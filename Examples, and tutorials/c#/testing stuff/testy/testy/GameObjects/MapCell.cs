﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectSteam.GameObjects
{
    // An instance of this class is a single cell in the isometric map
    // It can contain more than one tile in its position.
    class MapCell
    {
        internal List<int> BaseTiles = new List<int>();
        internal List<int> HeightTiles = new List<int>();
        internal List<int> TopperTiles = new List<int>();

        public int TileID
        {
            get { return BaseTiles.Count > 0 ? BaseTiles[0] : 0; }
            set
            {
                if (BaseTiles.Count > 0)
                    BaseTiles[0] = value;
                else
                    AddBaseTile(value);
            }
        }

        public void AddBaseTile(int tileID)
        {
            BaseTiles.Add(tileID);
        }

        public void AddHeightTile(int tileID)
        {
            HeightTiles.Add(tileID);
        }

        public void AddTopperTile(int tileID)
        {
            TopperTiles.Add(tileID);
        }

        public MapCell(int tileID)
        {
            TileID = tileID;
        }
    }
}
