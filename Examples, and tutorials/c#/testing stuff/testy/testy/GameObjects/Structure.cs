﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ProjectSteam.GameObjects
{
    abstract class Structure : ActiveObject
    {
        Inventory<Structure> stuff;

        internal Structure()
        {}

        internal Structure(int width, int height)
        {
            this.width = width;
            this.height = height;
        }

        internal override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            animation.Draw(spriteBatch, gameTime);
        }

        internal void setIconTexture(Texture2D txtr)
        {
            icon = txtr;
        }

        internal void setTexture(Texture2D txtr)
        {
            texture = txtr;
            animation = new Utils.SpriteAnimation(this, texture, width, height);
        }

        internal abstract void Build();
    }
}
