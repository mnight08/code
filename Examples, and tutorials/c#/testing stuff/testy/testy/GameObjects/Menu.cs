﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ProjectSteam.GameObjects
{
    class Menu
    {
        internal enum State { hidden, start, pause, quick, build }

        internal State state;

//        Inventory equipment;
        internal Inventory<GameObject> gear;
        internal Inventory<Structure> build;
//        Inventory current;

        internal Menu(GameObject owner)
        {
            gear = new Inventory<GameObject>(owner);
            build = new Inventory<Structure>(owner);
            state = State.hidden;
        }

        internal void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (state == State.build) // draw build menu
            {
                build.Draw(spriteBatch, gameTime);
            }
        }

        internal void AddBuildItem(Structure item)
        {
            build.Add(item);
        }
    }
}
