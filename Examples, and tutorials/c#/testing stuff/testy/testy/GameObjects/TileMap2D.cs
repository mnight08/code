﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ProjectSteam.GameObjects
{
    class TileMap2D : TileMap
    {
        internal TileMap2D(Texture2D image)
        {
            this.image = image;

            MapWidth = 30;
            MapHeight = 16;

            squaresAcross = 29;
            squaresDown = 16;

            for (int y = 0; y < MapHeight; y++)
            {
                MapRow thisRow = new MapRow();
                for (int x = 0; x < MapWidth; x++)
                {
                    thisRow.Columns.Add(new MapCell(1));
                }
                Rows.Add(thisRow);
            }

            // Create Sample Map Data
            Rows[0].Columns[0].TileID = 4;
            Rows[0].Columns[1].TileID = 4;
            Rows[0].Columns[2].TileID = 4;
            Rows[0].Columns[3].TileID = 4;
            Rows[0].Columns[4].TileID = 4;
            Rows[0].Columns[5].TileID = 4;
            Rows[0].Columns[6].TileID = 4;
            Rows[0].Columns[7].TileID = 4;
            Rows[0].Columns[8].TileID = 4;
            Rows[0].Columns[9].TileID = 4;
            Rows[0].Columns[10].TileID = 4;
            Rows[0].Columns[11].TileID = 4;
            Rows[0].Columns[12].TileID = 4;
            Rows[0].Columns[13].TileID = 4;
            Rows[0].Columns[14].TileID = 4;
            Rows[0].Columns[15].TileID = 4;
            Rows[0].Columns[16].TileID = 4;
            Rows[0].Columns[17].TileID = 4;
            Rows[0].Columns[18].TileID = 4;
            Rows[0].Columns[19].TileID = 4;
            Rows[0].Columns[20].TileID = 4;
            Rows[0].Columns[21].TileID = 4;
            Rows[0].Columns[22].TileID = 4;
            Rows[0].Columns[23].TileID = 4;
            Rows[0].Columns[24].TileID = 4;
            Rows[0].Columns[25].TileID = 4;
            Rows[0].Columns[26].TileID = 4;
            Rows[0].Columns[27].TileID = 4;
            Rows[0].Columns[28].TileID = 4;
            Rows[0].Columns[29].TileID = 4;
            Rows[1].Columns[0].TileID = 4;
            Rows[1].Columns[1].TileID = 4;
            Rows[1].Columns[2].TileID = 4;
            Rows[1].Columns[3].TileID = 4;
            Rows[1].Columns[4].TileID = 4;
            Rows[1].Columns[5].TileID = 4;
            Rows[1].Columns[6].TileID = 4;
            Rows[1].Columns[7].TileID = 4;
            Rows[1].Columns[8].TileID = 4;
            Rows[1].Columns[9].TileID = 4;
            Rows[1].Columns[10].TileID = 4;
            Rows[1].Columns[11].TileID = 4;
            Rows[1].Columns[12].TileID = 4;
            Rows[1].Columns[13].TileID = 4;
            Rows[1].Columns[14].TileID = 4;
            Rows[1].Columns[15].TileID = 4;
            Rows[1].Columns[16].TileID = 4;
            Rows[1].Columns[17].TileID = 4;
            Rows[1].Columns[18].TileID = 4;
            Rows[1].Columns[19].TileID = 4;
            Rows[1].Columns[20].TileID = 4;
            Rows[1].Columns[21].TileID = 4;
            Rows[1].Columns[22].TileID = 4;
            Rows[1].Columns[23].TileID = 4;
            Rows[1].Columns[24].TileID = 4;
            Rows[1].Columns[25].TileID = 4;
            Rows[1].Columns[26].TileID = 4;
            Rows[1].Columns[27].TileID = 4;
            Rows[1].Columns[28].TileID = 4;
            Rows[1].Columns[29].TileID = 4;
            Rows[2].Columns[0].TileID = 4;
            Rows[2].Columns[1].TileID = 4;
            Rows[2].Columns[2].TileID = 4;
            Rows[2].Columns[3].TileID = 4;
            Rows[2].Columns[4].TileID = 4;
            Rows[2].Columns[5].TileID = 4;
            Rows[2].Columns[6].TileID = 4;
            Rows[2].Columns[7].TileID = 4;
            Rows[2].Columns[8].TileID = 4;
            Rows[2].Columns[9].TileID = 4;
            Rows[2].Columns[10].TileID = 4;
            Rows[2].Columns[11].TileID = 4;
            Rows[2].Columns[12].TileID = 4;
            Rows[2].Columns[13].TileID = 4;
            Rows[2].Columns[14].TileID = 4;
            Rows[2].Columns[15].TileID = 4;
            Rows[2].Columns[16].TileID = 4;
            Rows[2].Columns[17].TileID = 4;
            Rows[2].Columns[18].TileID = 4;
            Rows[2].Columns[19].TileID = 4;
            Rows[2].Columns[20].TileID = 4;
            Rows[2].Columns[21].TileID = 4;
            Rows[2].Columns[22].TileID = 4;
            Rows[2].Columns[23].TileID = 4;
            Rows[2].Columns[24].TileID = 4;
            Rows[2].Columns[25].TileID = 4;
            Rows[2].Columns[26].TileID = 4;
            Rows[2].Columns[27].TileID = 4;
            Rows[2].Columns[28].TileID = 4;
            Rows[2].Columns[29].TileID = 4;

            Rows[3].Columns[12].AddBaseTile(0);
            Rows[3].Columns[12].AddBaseTile(38);
            Rows[3].Columns[13].AddBaseTile(3);
            Rows[3].Columns[14].AddBaseTile(3);
            Rows[3].Columns[15].AddBaseTile(3);
            Rows[3].Columns[16].AddBaseTile(0);
            Rows[3].Columns[16].AddBaseTile(39);
            Rows[4].Columns[12].AddBaseTile(0);
            Rows[4].Columns[12].AddBaseTile(38);
            Rows[4].Columns[13].AddBaseTile(3);
            Rows[4].Columns[14].AddBaseTile(3);
            Rows[4].Columns[15].AddBaseTile(3);
            Rows[4].Columns[16].AddBaseTile(0);
            Rows[4].Columns[16].AddBaseTile(39);
            Rows[5].Columns[12].AddBaseTile(0);
            Rows[5].Columns[12].AddBaseTile(38);
            Rows[5].Columns[13].AddBaseTile(3);
            Rows[5].Columns[14].AddBaseTile(3);
            Rows[5].Columns[15].AddBaseTile(3);
            Rows[5].Columns[16].AddBaseTile(0);
            Rows[5].Columns[16].AddBaseTile(39);
            Rows[6].Columns[12].AddBaseTile(0);
            Rows[6].Columns[12].AddBaseTile(38);
            Rows[6].Columns[13].AddBaseTile(3);
            Rows[6].Columns[14].AddBaseTile(3);
            Rows[6].Columns[15].AddBaseTile(3);
            Rows[6].Columns[16].AddBaseTile(0);
            Rows[6].Columns[16].AddBaseTile(39);
            Rows[7].Columns[12].AddBaseTile(0);
            Rows[7].Columns[12].AddBaseTile(38);
            Rows[7].Columns[13].AddBaseTile(3);
            Rows[7].Columns[14].AddBaseTile(3);
            Rows[7].Columns[15].AddBaseTile(3);
            Rows[7].Columns[16].AddBaseTile(0);
            Rows[7].Columns[16].AddBaseTile(39);
            Rows[8].Columns[12].AddBaseTile(0);
            Rows[8].Columns[12].AddBaseTile(38);
            Rows[8].Columns[13].AddBaseTile(3);
            Rows[8].Columns[14].AddBaseTile(3);
            Rows[8].Columns[15].AddBaseTile(3);
            Rows[8].Columns[16].AddBaseTile(0);
            Rows[8].Columns[16].AddBaseTile(39);
            Rows[9].Columns[12].AddBaseTile(0);
            Rows[9].Columns[12].AddBaseTile(38);
            Rows[9].Columns[13].AddBaseTile(3);
            Rows[9].Columns[14].AddBaseTile(3);
            Rows[9].Columns[15].AddBaseTile(3);
            Rows[9].Columns[16].AddBaseTile(0);
            Rows[9].Columns[16].AddBaseTile(39);
            Rows[10].Columns[12].AddBaseTile(0);
            Rows[10].Columns[12].AddBaseTile(38);
            Rows[10].Columns[13].AddBaseTile(3);
            Rows[10].Columns[14].AddBaseTile(3);
            Rows[10].Columns[15].AddBaseTile(3);
            Rows[10].Columns[16].AddBaseTile(0);
            Rows[10].Columns[16].AddBaseTile(39);
            Rows[11].Columns[12].AddBaseTile(0);
            Rows[11].Columns[12].AddBaseTile(38);
            Rows[11].Columns[13].AddBaseTile(3);
            Rows[11].Columns[14].AddBaseTile(3);
            Rows[11].Columns[15].AddBaseTile(3);
            Rows[11].Columns[16].AddBaseTile(0);
            Rows[11].Columns[16].AddBaseTile(39);
            Rows[12].Columns[12].AddBaseTile(0);
            Rows[12].Columns[12].AddBaseTile(38);
            Rows[12].Columns[13].AddBaseTile(3);
            Rows[12].Columns[14].AddBaseTile(3);
            Rows[12].Columns[15].AddBaseTile(3);
            Rows[12].Columns[16].AddBaseTile(0);
            Rows[12].Columns[16].AddBaseTile(39);
            Rows[13].Columns[12].AddBaseTile(0);
            Rows[13].Columns[12].AddBaseTile(38);
            Rows[13].Columns[13].AddBaseTile(3);
            Rows[13].Columns[14].AddBaseTile(3);
            Rows[13].Columns[15].AddBaseTile(3);
            Rows[13].Columns[16].AddBaseTile(0);
            Rows[13].Columns[16].AddBaseTile(39);
            Rows[14].Columns[12].AddBaseTile(0);
            Rows[14].Columns[12].AddBaseTile(38);
            Rows[14].Columns[13].AddBaseTile(3);
            Rows[14].Columns[14].AddBaseTile(3);
            Rows[14].Columns[15].AddBaseTile(3);
            Rows[14].Columns[16].AddBaseTile(0);
            Rows[14].Columns[16].AddBaseTile(39);
            Rows[15].Columns[12].AddBaseTile(0);
            Rows[15].Columns[12].AddBaseTile(38);
            Rows[15].Columns[13].AddBaseTile(3);
            Rows[15].Columns[14].AddBaseTile(3);
            Rows[15].Columns[15].AddBaseTile(3);
            Rows[15].Columns[16].AddBaseTile(0);
            Rows[15].Columns[16].AddBaseTile(39);

            Rows[3].Columns[0].AddBaseTile(18);
            Rows[3].Columns[1].AddBaseTile(13);
            Rows[3].Columns[2].AddBaseTile(13);
            Rows[3].Columns[3].AddBaseTile(13);
            Rows[3].Columns[4].AddBaseTile(13);
            Rows[3].Columns[5].AddBaseTile(13);
            Rows[3].Columns[6].AddBaseTile(13);
            Rows[3].Columns[7].AddBaseTile(13);
            Rows[3].Columns[8].AddBaseTile(13);
            Rows[3].Columns[9].AddBaseTile(13);
            Rows[3].Columns[10].AddBaseTile(13);
            Rows[3].Columns[11].AddBaseTile(19);
            Rows[4].Columns[0].AddBaseTile(15);
            Rows[4].Columns[11].AddBaseTile(14);
            Rows[5].Columns[0].AddBaseTile(15);
            Rows[5].Columns[11].AddBaseTile(14);
            Rows[6].Columns[0].AddBaseTile(15);
            Rows[6].Columns[11].AddBaseTile(14);
            Rows[7].Columns[0].AddBaseTile(15);
            Rows[7].Columns[11].AddBaseTile(14);
            Rows[8].Columns[0].AddBaseTile(15);
            Rows[8].Columns[11].AddBaseTile(14);
            Rows[9].Columns[0].AddBaseTile(15);
            Rows[9].Columns[11].AddBaseTile(14);
            Rows[10].Columns[0].AddBaseTile(15);
            Rows[10].Columns[11].AddBaseTile(14);
            Rows[11].Columns[0].AddBaseTile(15);
            Rows[11].Columns[11].AddBaseTile(14);
            Rows[12].Columns[0].AddBaseTile(15);
            Rows[12].Columns[11].AddBaseTile(14);
            Rows[13].Columns[0].AddBaseTile(15);
            Rows[13].Columns[11].AddBaseTile(14);
            Rows[14].Columns[0].AddBaseTile(15);
            Rows[14].Columns[11].AddBaseTile(14);
            Rows[15].Columns[0].AddBaseTile(16);
            Rows[15].Columns[1].AddBaseTile(12);
            Rows[15].Columns[2].AddBaseTile(12);
            Rows[15].Columns[3].AddBaseTile(12);
            Rows[15].Columns[4].AddBaseTile(12);
            Rows[15].Columns[5].AddBaseTile(12);
            Rows[15].Columns[6].AddBaseTile(12);
            Rows[15].Columns[7].AddBaseTile(12);
            Rows[15].Columns[8].AddBaseTile(12);
            Rows[15].Columns[9].AddBaseTile(12);
            Rows[15].Columns[10].AddBaseTile(12);
            Rows[15].Columns[11].AddBaseTile(17);

            Rows[3].Columns[17].AddBaseTile(18);
            Rows[3].Columns[18].AddBaseTile(13);
            Rows[3].Columns[19].AddBaseTile(13);
            Rows[3].Columns[20].AddBaseTile(13);
            Rows[3].Columns[21].AddBaseTile(13);
            Rows[3].Columns[22].AddBaseTile(13);
            Rows[3].Columns[23].AddBaseTile(13);
            Rows[3].Columns[24].AddBaseTile(13);
            Rows[3].Columns[25].AddBaseTile(13);
            Rows[3].Columns[26].AddBaseTile(13);
            Rows[3].Columns[27].AddBaseTile(19);
            Rows[3].Columns[28].AddBaseTile(0);
            Rows[4].Columns[17].AddBaseTile(15);
            Rows[4].Columns[27].AddBaseTile(14);
            Rows[4].Columns[28].AddBaseTile(0);
            Rows[5].Columns[17].AddBaseTile(15);
            Rows[5].Columns[27].AddBaseTile(14);
            Rows[5].Columns[28].AddBaseTile(0);
            Rows[6].Columns[17].AddBaseTile(15);
            Rows[6].Columns[27].AddBaseTile(14);
            Rows[6].Columns[28].AddBaseTile(0);
            Rows[7].Columns[17].AddBaseTile(15);
            Rows[7].Columns[27].AddBaseTile(14);
            Rows[7].Columns[28].AddBaseTile(0);
            Rows[8].Columns[17].AddBaseTile(15);
            Rows[8].Columns[27].AddBaseTile(14);
            Rows[8].Columns[28].AddBaseTile(0);
            Rows[9].Columns[17].AddBaseTile(15);
            Rows[9].Columns[27].AddBaseTile(14);
            Rows[9].Columns[28].AddBaseTile(0);
            Rows[10].Columns[17].AddBaseTile(15);
            Rows[10].Columns[27].AddBaseTile(14);
            Rows[10].Columns[28].AddBaseTile(0);
            Rows[11].Columns[17].AddBaseTile(15);
            Rows[11].Columns[27].AddBaseTile(14);
            Rows[11].Columns[28].AddBaseTile(0);
            Rows[12].Columns[17].AddBaseTile(15);
            Rows[12].Columns[27].AddBaseTile(14);
            Rows[12].Columns[28].AddBaseTile(0);
            Rows[13].Columns[17].AddBaseTile(15);
            Rows[13].Columns[27].AddBaseTile(14);
            Rows[13].Columns[28].AddBaseTile(0);
            Rows[14].Columns[17].AddBaseTile(15);
            Rows[14].Columns[27].AddBaseTile(14);
            Rows[14].Columns[28].AddBaseTile(0);
            Rows[15].Columns[17].AddBaseTile(16);
            Rows[15].Columns[18].AddBaseTile(12);
            Rows[15].Columns[19].AddBaseTile(12);
            Rows[15].Columns[20].AddBaseTile(12);
            Rows[15].Columns[21].AddBaseTile(12);
            Rows[15].Columns[22].AddBaseTile(12);
            Rows[15].Columns[23].AddBaseTile(12);
            Rows[15].Columns[24].AddBaseTile(12);
            Rows[15].Columns[25].AddBaseTile(12);
            Rows[15].Columns[26].AddBaseTile(12);
            Rows[15].Columns[27].AddBaseTile(17);
            Rows[15].Columns[28].AddBaseTile(0);

            Rows[3].Columns[0].AddBaseTile(97);
            Rows[15].Columns[0].AddBaseTile(100);
            Rows[3].Columns[27].AddBaseTile(103);
            Rows[15].Columns[27].AddBaseTile(99);
            Rows[3].Columns[17].AddBaseTile(105);

//            Rows[1].Columns[3].TileID = 3;
//            Rows[1].Columns[4].TileID = 1;
//            Rows[1].Columns[5].TileID = 1;
//            Rows[1].Columns[6].TileID = 1;
//            Rows[1].Columns[7].TileID = 1;

//            Rows[2].Columns[2].TileID = 3;
//            Rows[2].Columns[3].TileID = 1;
//            Rows[2].Columns[4].TileID = 1;
//            Rows[2].Columns[5].TileID = 1;
//            Rows[2].Columns[6].TileID = 1;
//            Rows[2].Columns[7].TileID = 1;

//            Rows[3].Columns[2].TileID = 3;
//            Rows[3].Columns[3].TileID = 1;
//            Rows[3].Columns[4].TileID = 1;
//            Rows[3].Columns[5].TileID = 2;
//            Rows[3].Columns[6].TileID = 2;
//            Rows[3].Columns[7].TileID = 2;

//            Rows[4].Columns[2].TileID = 3;
//            Rows[4].Columns[3].TileID = 1;
//            Rows[4].Columns[4].TileID = 1;
//            Rows[4].Columns[5].TileID = 2;
//            Rows[4].Columns[6].TileID = 2;
//            Rows[4].Columns[7].TileID = 2;

//            Rows[5].Columns[2].TileID = 3;
//            Rows[5].Columns[3].TileID = 1;
//            Rows[5].Columns[4].TileID = 1;
//            Rows[5].Columns[5].TileID = 2;
//            Rows[5].Columns[6].TileID = 2;
//            Rows[5].Columns[7].TileID = 2;

//            Rows[3].Columns[5].AddBaseTile(30);
//            Rows[4].Columns[5].AddBaseTile(27);
//            Rows[5].Columns[5].AddBaseTile(28);

//            Rows[3].Columns[6].AddBaseTile(25);
//            Rows[5].Columns[6].AddBaseTile(24);

//            Rows[3].Columns[7].AddBaseTile(31);
//            Rows[4].Columns[7].AddBaseTile(26);
//            Rows[5].Columns[7].AddBaseTile(29);

//            Rows[4].Columns[6].AddBaseTile(104);
            // End Create Sample Map Data
        }
        internal Point WorldToMapCell(Point worldPoint)
        {
            Point dummy = new Point(worldPoint.X / 48, worldPoint.Y / 48);
        //    return WorldToMapCell(worldPoint, out dummy);
            return dummy;
        }

        internal void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            Vector2 firstSquare = new Vector2(Camera.Location.X / Tile.TileWidth2D, Camera.Location.Y / Tile.TileHeight2D);
            int firstX = (int)firstSquare.X;
            int firstY = (int)firstSquare.Y;

            Vector2 squareOffset = new Vector2(Camera.Location.X % Tile.TileWidth2D, Camera.Location.Y % Tile.TileHeight2D);
            int offsetX = (int)squareOffset.X;
            int offsetY = (int)squareOffset.Y;

            for (int y = 0; y < squaresDown; y++)
            {
                for (int x = 0; x < squaresAcross; x++)
                {
                    foreach (int tileID in Rows[y + firstY].Columns[x + firstX].BaseTiles)
                    {
                        spriteBatch.Draw(
                            image,
                            new Rectangle(
                                (x * Tile.TileWidth2D) - offsetX, (y * Tile.TileHeight2D) - offsetY,
                                Tile.TileWidth2D, Tile.TileHeight2D),
                            Tile.GetSourceRectangle2D(tileID, image),
                            Color.White);
                    }
                }
            }

        }
    }
}