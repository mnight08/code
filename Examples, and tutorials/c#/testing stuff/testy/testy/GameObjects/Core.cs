﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectSteam.Utils;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;

namespace ProjectSteam.GameObjects
{
    class Core : Structure
    {
        internal Core(ContentManager content)
        {
            // setup collision box
            collisionBox = new Rectangle(0, 0, 64, 44);
            // default position
            SetPosition(new Vector2(100, 400));
            load(content);
            // setup animation; texture was added statically
            animation = new SpriteAnimation(this, texture, 100, 100);
            // scale the image down from 100px to 64
            animation.setScale((float)0.64);
            drawOffset.X = 0;
            drawOffset.Y = -20;
        }

        // This load function must be called before creating an instance of Core
        internal void load(ContentManager content)
        {
            // Load texture for the core
            texture = content.Load<Texture2D>(@"Textures\Structures\core");
        }

        internal override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            animation.Draw(spriteBatch, gameTime);
        }

        internal override void Build()
        {
            complete = true;
        }
    }

}
