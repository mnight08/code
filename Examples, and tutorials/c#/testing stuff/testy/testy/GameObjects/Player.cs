﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ProjectSteam.Utils;

namespace ProjectSteam.GameObjects
{
    class Player : Unit
    {
        // 8 directions going clockwise, North = 1
        int direction;

        internal Menu menu;

        internal KeyboardState prevKS;
        internal GamePadState prevGPS;
        private int currentBuildItemIndex = -1; // item selected from build menu
        private Structure currentBuildItem; // pointer to actual item player is building
        private Animation structurePlacementSelector; // animation for selector

        private Structure objectToMove; // object I'm currently pushing or pulling
        private int relativePositionOfGrabbedStructure; // position of structure relative to me. On top=1, on right=3, below=5, on left=7

        internal int speed;

        double Weight
        {
            get { return 0; }
        }

        internal Player(ContentManager content)
        {
            collisionBox = new Rectangle(0, 0, 50, 50);
            SetPosition(new Vector2(300, 300));
            animation = new Utils.SpriteAnimation(this, texture = content.Load<Texture2D>(@"Textures\Units\pred"), 95, 135);

            structurePlacementSelector = new Animation(content.Load<Texture2D>(@"Textures\Menus\selectMenuHover"), 64, 64, 100);
            structurePlacementSelector.setScale(0.8f);

            drawOffset.X = -22;
            drawOffset.Y = -78;
            direction = 5; // default facing down
            speed = 5;

            menu = new Menu(this);
        }

        internal override void Update(SpriteManager spriteManager, GameTime gameTime)
        {
            KeyboardState ks = Keyboard.GetState();
            GamePadState gps = GamePad.GetState(PlayerIndex.One);

            if (ks.IsKeyDown(Keys.G) || gps.IsButtonDown(Buttons.Y))
                MoveObject(spriteManager);
            else if (prevKS.IsKeyDown(Keys.G) || prevGPS.IsButtonDown(Buttons.Y))
                objectToMove = null;

            // move handles controls involving anything internal to player
            Vector2 moveVector = Move(ks, gps); // returns move vector

            // time to handle stuff that actually modifies the game
            // building structures
            // check if build key/button was pressed
            if ((ks.IsKeyDown(Keys.B) && !ks.IsKeyDown(Keys.M)) || (gps.IsButtonDown(Buttons.B) && !gps.IsButtonDown(Buttons.LeftShoulder)))
                BuildStructure(spriteManager, gameTime);

            // Update players position
            SetPosition(GetPosition() + moveVector);
            if (moveVector == Vector2.Zero)
                animation.idle = true;
            else
                animation.idle = false;
            updateImage();
        }

        private Vector2 Move(KeyboardState ks, GamePadState gps)
        {
            // vector to add to player's position
            Vector2 moveVector = Vector2.Zero;

            // keyboard
            if (ks.IsKeyDown(Keys.Left))
            {
                moveVector.X -= 2; ;
                if (ks.IsKeyDown(Keys.Up))
                {
                    moveVector.Y -= 2;
                    direction = 8;
                }
                else if (ks.IsKeyDown(Keys.Down))
                {
                    moveVector.Y += 2;
                    direction = 6;
                }
                else
                    direction = 7;
            }

            if (ks.IsKeyDown(Keys.Right))
            {
                moveVector.X += 2;
                if (ks.IsKeyDown(Keys.Up))
                {
                    moveVector.Y -= 2;
                    direction = 2;
                }
                else if (ks.IsKeyDown(Keys.Down))
                {
                    moveVector.Y += 2;
                    direction = 4;
                }
                else
                    direction = 3;
            }

            if (ks.IsKeyDown(Keys.Up))
            {
                moveVector.Y -= 2;
                if (ks.IsKeyDown(Keys.Left))
                {
                    moveVector.X -= 2;
                    direction = 8;
                }
                else if (ks.IsKeyDown(Keys.Right))
                {
                    moveVector.X += 2;
                    direction = 2;
                }
                else
                {
                    direction = 1;
                }
            }

            if (ks.IsKeyDown(Keys.Down))
            {
                moveVector.Y += 2;
                if (ks.IsKeyDown(Keys.Left))
                {
                    moveVector.X -= 2;
                    direction = 6;
                }
                else if (ks.IsKeyDown(Keys.Right))
                {
                    moveVector.X += 2;
                    direction = 4;
                }
                else
                {
                    direction = 5;
                }
            }

            if (ks.IsKeyDown(Keys.M))
            {
                // if just pressed M, set menu to build state
                if (!prevKS.IsKeyDown(Keys.M))
                {
                    currentBuildItemIndex = -1;
                    menu.state = Menu.State.build;
                }
                // move menu selector around
                if (ks.IsKeyDown(Keys.A) && !prevKS.IsKeyDown(Keys.A)) // left
                    menu.build.currentItem = -1;
                if (ks.IsKeyDown(Keys.D) && !prevKS.IsKeyDown(Keys.D)) // right
                    menu.build.currentItem = 1;

                if ((ks.IsKeyDown(Keys.B) && !prevKS.IsKeyDown(Keys.B)) || (prevGPS.IsButtonDown(Buttons.B) && !prevGPS.IsButtonDown(Buttons.B))) // add selected structure to currentBuildItem
                    currentBuildItemIndex = menu.build.currentItem;
            }
            else
            {
                if (prevKS.IsKeyDown(Keys.M))
                    menu.state = Menu.State.hidden;
            }

            // set previous keystate
            prevKS = ks;

            // gamepad
            // set direction
//            Console.Out.WriteLine(Math.Abs(gps.ThumbSticks.Left.X) - Math.Abs(gps.ThumbSticks.Left.Y) + " what " + (((double)(Math.Abs(gps.ThumbSticks.Left.Y) + Math.Abs(gps.ThumbSticks.Left.X))) / 4));
            if ((Math.Abs(Math.Abs(gps.ThumbSticks.Left.X)-Math.Abs(gps.ThumbSticks.Left.Y)) < ((double)(Math.Abs(gps.ThumbSticks.Left.Y)+Math.Abs(gps.ThumbSticks.Left.X)))/2) && !gps.IsButtonDown(Buttons.Y)) // going diagonal
            {
                Console.Out.WriteLine(gps.ThumbSticks.Left.X + " what " + gps.ThumbSticks.Left.Y);
                if(gps.ThumbSticks.Left.X > 0) // right
                    if(gps.ThumbSticks.Left.Y > 0) // up
                    {
                        direction = 2;
                        moveVector.X = speed*Math.Max(gps.ThumbSticks.Left.X, Math.Abs(gps.ThumbSticks.Left.Y));
                        if (moveVector.X > (double)speed / 2.0)
                            moveVector.X = (float)(speed / 2.0);
                        moveVector.Y = -moveVector.X;
                    }
                    else // down
                    {
                        direction = 4;
                        moveVector.Y = speed * Math.Max(gps.ThumbSticks.Left.Y, gps.ThumbSticks.Left.X);
                        if (moveVector.Y > (double)speed / 2.0)
                            moveVector.Y = (float)(speed / 2.0);
                        moveVector.X = moveVector.Y;
                    }
                else // left
                    if(gps.ThumbSticks.Left.Y > 0) // up
                    {
                        direction = 8;
                        moveVector.X = -speed * Math.Max(Math.Abs(gps.ThumbSticks.Left.X), Math.Abs(gps.ThumbSticks.Left.Y));
                        if (moveVector.X > (double)speed / 2.0)
                            moveVector.X = (float)(speed / 2.0);
                        moveVector.Y = moveVector.X;
                    }
                    else //down
                    {
                        direction = 6;
                        moveVector.Y = speed * Math.Max(gps.ThumbSticks.Left.Y, Math.Abs(gps.ThumbSticks.Left.X));
                        if (moveVector.Y > (double)speed / 2)
                            moveVector.Y = (float)(speed / 2);
                        moveVector.X = -moveVector.Y;
                    }
            }
            else // not moving in diagonal direction
            {
                if (Math.Abs(gps.ThumbSticks.Left.X) > Math.Abs(gps.ThumbSticks.Left.Y)) // X overpowers Y
                {
                    if (gps.IsButtonDown(Buttons.Y) && direction != 3 && direction != 7)
                    { }
                    else
                    {
                        if (gps.ThumbSticks.Left.X > 0) // to the right
                        {
                            direction = 3;
                            moveVector.X = speed * gps.ThumbSticks.Left.X;
                        }
                        else // to the left
                        {
                            direction = 7;
                            moveVector.X = speed * gps.ThumbSticks.Left.X;
                        }
                    }
                }
                else // Y overpowers X
                {
                    if (gps.IsButtonDown(Buttons.Y) && direction != 1 && direction != 5)
                    { }
                    else
                    {
                        if (gps.ThumbSticks.Left.Y < 0) // moving down
                        {
                            direction = 5;
                            moveVector.Y = -speed * gps.ThumbSticks.Left.Y;
                        }
                        else if(gps.ThumbSticks.Left.Y > 0) // moving up
                        {
                            direction = 1;
                            moveVector.Y = -speed * gps.ThumbSticks.Left.Y;
                        }
                    }
                }
            }

            if (ks.IsKeyDown(Keys.M) || gps.IsButtonDown(Buttons.LeftShoulder))
            {
                // if just pressed M, set menu to build state
                if (!prevKS.IsKeyDown(Keys.M) && !prevGPS.IsButtonDown(Buttons.LeftShoulder))
                {
                    currentBuildItemIndex = -1;
                    menu.state = Menu.State.build;
                }
                // move menu selector around
                if (((ks.IsKeyDown(Keys.A) && !prevKS.IsKeyDown(Keys.A))) || (gps.ThumbSticks.Right.X < -0.3 && !(prevGPS.ThumbSticks.Right.X < -0.3))) // left
                    menu.build.currentItem = -1;
                if (((ks.IsKeyDown(Keys.D) && !prevKS.IsKeyDown(Keys.D))) || (gps.ThumbSticks.Right.X > 0.3 && !(prevGPS.ThumbSticks.Right.X > 0.3))) // right
                    menu.build.currentItem = 1;

                if ((ks.IsKeyDown(Keys.B) && !prevKS.IsKeyDown(Keys.B)) || (gps.IsButtonDown(Buttons.B) && !prevGPS.IsButtonDown(Buttons.B))) // add selected structure to currentBuildItem
                    currentBuildItemIndex = menu.build.currentItem;
            }
            else
            {
                if (prevKS.IsKeyDown(Keys.M) || prevGPS.IsButtonDown(Buttons.LeftShoulder))
                    menu.state = Menu.State.hidden;
            }

            // set previous gamepad state
            prevGPS = gps;

            return moveVector;
        }

        internal override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            animation.Draw(spriteBatch, gameTime);
            // draw menu
            menu.Draw(spriteBatch, gameTime);
//            DebugDraw(spriteBatch, gameTime);
            // draw structure build square
            if (currentBuildItemIndex != -1)
            {
                Point pos = new Point(0, 0);
                switch(direction)
                {
                case 1:
                    pos.X = Xleft;
                    pos.Y = Yhigh-50;
                    break;
                case 3:
                    pos.X = Xright;
                    pos.Y = Yhigh;
                    break;
                case 5:
                    pos.X = Xleft;
                    pos.Y = Ylow;
                    break;
                case 7:
                    pos.X = Xleft-50;
                    pos.Y = Yhigh;
                    break;
                }
                if (direction % 2 == 1)
                    structurePlacementSelector.Draw(spriteBatch, gameTime, pos);
            }
        }

        // function to move(push/pull/rotate) a physical in game object
        internal void MoveObject(SpriteManager spriteManager)
        {
            // check if I'm already moving an object
            if (objectToMove == null)
            {
                // find object that you are facing and are near to
                objectToMove = GetStructureInFrontOfMe(spriteManager);
                if (objectToMove == null)
                    return;
            }
            if (direction == 1 || direction == 5)
            {
                if (objectToMove.Ylow > this.Ylow)
                    relativePositionOfGrabbedStructure = 5;
                else
                    relativePositionOfGrabbedStructure = 1;
            }
            else
            {
                if (objectToMove.Xright > this.Xright)
                    relativePositionOfGrabbedStructure = 3;
                else
                    relativePositionOfGrabbedStructure = 7;
            }

            if (relativePositionOfGrabbedStructure == 1) // pushing up or pulling down
            {
                // move object up
                objectToMove.SetPositionY((int)this.GetPosition().Y - objectToMove.height);
            }
            else if (relativePositionOfGrabbedStructure == 3) // pushing or pulling to the right
            {
                // move object to the right
                objectToMove.SetPositionX((int)this.GetPosition().X + this.width);
            }
            else if (relativePositionOfGrabbedStructure == 5) // pushing or pulling down
            {
                // move object down
                objectToMove.SetPositionY((int)this.GetPosition().Y + this.height);
            }
            else if (relativePositionOfGrabbedStructure == 7) // pushing or pulling left
            {
                // move object left
                objectToMove.SetPositionX((int)this.GetPosition().X - objectToMove.width);
            }
        }

        private void BuildStructure(SpriteManager spriteManager, GameTime gameTime)
        {
            if (currentBuildItem == null)
            {
                // find out what structure to build
                if (currentBuildItemIndex < 0) // get in game structure I'm standing in front of
                {
                    currentBuildItem = GetStructureInFrontOfMe(spriteManager);
                }
                else // structure is new and hasn't been placed yet
                {
                    // add new structure to objectlist in game
                    Structure newStructure = (Structure)spriteManager.GetStructure(menu.build.GetObjectIndex());

                    switch(direction)
                    {
                    case 1:
                        newStructure.SetPositionX( Xleft );
                        newStructure.SetPositionY( Yhigh-newStructure.height );
                        break;
                    case 3:
                        newStructure.SetPositionX( Xright );
                        newStructure.SetPositionY( Yhigh);
                        break;
                    case 5:
                        newStructure.SetPositionX( Xleft );
                        newStructure.SetPositionY( Ylow );
                        break;
                    case 7:
                        newStructure.SetPositionX( Xleft - newStructure.width);
                        newStructure.SetPositionY( Yhigh );
                        break;
                    }
                    spriteManager.newAdditionsList.Add(newStructure);
                    currentBuildItemIndex = -1;
                }
            }
            else // I have the pointer to the structure I'm building
            {
                // check if I moved too far away from structure, in which case halt building process
                if(IsStructureInFrontOfMe(currentBuildItem) == null)
                    currentBuildItem = null;
                else // perfect, continue building the structure
                {
                    // step through a round of the building process
                    currentBuildItem.Build();
                }
            }
        }
        private Structure GetStructureInFrontOfMe(SpriteManager spriteManager)
        {
            // check if I'm in front of object
            foreach( GameObject go in spriteManager.objectList)
            {
                if (go == this) // have player skip itself
                    continue;

                if (IsStructureInFrontOfMe((Structure)go) != null)
                    return (Structure)go;
            }
            return null;
        }

        private Structure IsStructureInFrontOfMe(Structure go)
        {
            if (direction == 1) // facing up
            {
                if (go.Xleft < Xright)
                    if (go.Xright > Xleft)
                        if (Yhigh - go.Ylow < 1 && Yhigh - go.Ylow >= -1)
                        {
                            return go;
                        }
            }
            else if (direction == 3) // facing to the right
            {
                if (go.Yhigh < Ylow)
                    if (go.Ylow > Yhigh)
                        if (go.Xleft - Xright < 1 && go.Xleft - Xright >= -1)
                        {
                            return go;
                        }
            }
            else if (direction == 5) // facing down
            {
                if (go.Xleft < Xright)
                    if (go.Xright > Xleft)
                        if (go.Yhigh - Ylow > -1 && go.Yhigh - Ylow <= 1)
                        {
                            return go;
                        }
            }
            else if (direction == 7) // facing left
            {
                if (go.Yhigh < Ylow)
                    if (go.Ylow > Yhigh)
                        if (Xleft - go.Xright > -1 && Xleft - go.Xright <= 1)
                        {
                            return go;
                        }
            }

            return null;
        }

        private void updateImage()
        {
            switch (direction)
            {
                case 1:
                    animation.currentImage = 4;
                    break;
                case 2:
                    animation.currentImage = 7;
                    break;
                case 3:
                    animation.currentImage = 6;
                    break;
                case 4:
                    animation.currentImage = 5;
                    break;
                case 5:
                    animation.currentImage = 0;
                    break;
                case 6:
                    animation.currentImage = 1;
                    break;
                case 7:
                    animation.currentImage = 2;
                    break;
                case 8:
                    animation.currentImage = 3;
                    break;
            }
        }
    }
}
