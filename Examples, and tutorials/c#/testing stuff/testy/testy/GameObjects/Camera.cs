﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ProjectSteam.GameObjects
{
    static class Camera
    {
        // X, Y Location of camera
        static public Vector2 location = Vector2.Zero;

        public static int ViewWidth { get; set; }
        public static int ViewHeight { get; set; }
        public static int WorldWidth { get; set; }
        public static int WorldHeight { get; set; }

        // Isometric tiles need an offset to avoid seeing choppy sides of the world
        public static Vector2 DisplayOffset { get; set; }

        public static Vector2 Location
        {
            get
            {
                return location;
            }
            set
            {
                // Only allow setting the camera location to a valid setting
                location = new Vector2(
                    MathHelper.Clamp(value.X, 0f, WorldWidth - ViewWidth),
                    MathHelper.Clamp(value.Y, 0f, WorldHeight - ViewHeight));
            }
        }

        // Maps a position to the actual position on screen
        public static Vector2 WorldToScreen(Vector2 worldPosition)
        {
            return worldPosition - Location + DisplayOffset;
        }
        // same as above but with a point instead of vector
        internal static Vector2 WorldToScreen(Point point)
        {
            return WorldToScreen(new Vector2(point.X, point.Y));
        }

        // Maps a position to the position in the world
        public static Vector2 ScreenToWorld(Vector2 screenPosition)
        {
            return screenPosition + Location - DisplayOffset;
        }

        // Moves the camera by the offset
        public static void Move(Vector2 offset)
        {
            Location += offset;
        }
    }
}
