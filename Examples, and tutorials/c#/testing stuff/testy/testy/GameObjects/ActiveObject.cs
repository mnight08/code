﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ProjectSteam.Utils;

namespace ProjectSteam.GameObjects
{
    class ActiveObject : GameObject
    {
        double hp, attack, defense, range;
        int exp;
        int _level;
        State state;
        
        int level   //getter setter for level
        {
            get { return _level; }
            set { _level = value; }
        }

        bool alive
        {
            get { return (state != State.dead); }
        }

        /// <summary>
        /// Constructor for Active Object
        /// </summary>
        public ActiveObject() : base()
        {

        }

        internal void Attack()
        {}

        internal override void Update(SpriteManager spriteManager, GameTime gametime)
        {
        }

        internal override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {}
    }

    enum State { dead, moving, attacking, waiting }
}
