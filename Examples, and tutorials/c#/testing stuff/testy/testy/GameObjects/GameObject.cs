﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using ProjectSteam.Utils;
using Microsoft.Xna.Framework.Graphics;

namespace ProjectSteam.GameObjects
{
    abstract class GameObject
    {
        internal static Texture2D debugTexture;
        internal int objectIndex; // index to indicate what exact object type it is.

        internal double weight;

        internal Vector2 position
        {
            get { return new Vector2(collisionBox.X, collisionBox.Y); }
            set { collisionBox.X = (int)value.X; collisionBox.Y = (int)value.Y; }
        }
        internal Vector2 drawOffset; // negative offset of image to draw
        internal float drawDepth = 0.0f;
        internal Rectangle collisionBox;

        // main texture to be used in animation
        internal Texture2D texture;
        internal SpriteAnimation animation;

        // icon texture used in menus etc.
        internal Texture2D icon;

        internal bool complete = true; // whether this object has been completely built. Used for building structures

        internal int width
        {
            get {return collisionBox.Width;}
            set { collisionBox.Width = value; }
        }
        internal int height
        {
            get { return collisionBox.Height; }
            set { collisionBox.Height = value; }
        }

        internal int Xleft
        {
            get { return collisionBox.X; }
            set { }
        }
        internal int Xright
        {
            get { return collisionBox.X + width; }
            set { }
        }
        internal int Yhigh
        {
            get { return collisionBox.Y; }
            set { }
        }
        internal int Ylow
        {
            get { return collisionBox.Y + height; }
            set { }
        }

        internal void SetPosition(Vector2 pos)
        {
            position = pos;
        }
        internal void SetPosition(Point point)
        {
            position = new Vector2(point.X, point.Y);
        }

        internal void SetPositionX(int x)
        {
            collisionBox.X = x;
        }
        internal void SetPositionY(int y)
        {
            collisionBox.Y = y;
        }
        internal Vector2 GetPosition()
        {
            return position;
        }

        internal void IsCollidingWith(GameObject gameObject)
        {
        }

        internal abstract void Update(SpriteManager spriteManager, GameTime gametime);

        internal abstract void Draw(SpriteBatch spriteBatch, GameTime gameTime);

        internal void DebugDraw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            Rectangle sourceRect = new Rectangle(collisionBox.X, collisionBox.Y, width, height);
            spriteBatch.Draw(debugTexture, Camera.WorldToScreen(GetPosition()), sourceRect, Color.White, 0f, Vector2.Zero, 1.0f, SpriteEffects.None, 0);
        }

        internal void DrawIcon(SpriteBatch spriteBatch, GameTime gameTime, Point point)
        {
            point.X -= icon.Width/2;
            point.Y -= icon.Height/2;
            spriteBatch.Draw(icon, Camera.WorldToScreen(point), new Rectangle(0, 0, icon.Width, icon.Height), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.1f);
        }
    }
}
