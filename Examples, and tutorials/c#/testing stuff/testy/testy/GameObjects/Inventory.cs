﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using ProjectSteam.Utils;

namespace ProjectSteam.GameObjects
{
    class Inventory<T>
    {
        // ring
        internal static Texture2D ring;
        static int ringRadius = 110;
        static Vector2 radiusVector = new Vector2(ringRadius+15, ringRadius+15);

        // selector
        internal static Texture2D selectorTexture;
        internal Animation selector;

        private List<GameObject>[] lists;
        private int currentList; // index of currentlist in lists

        private int CurrentItem;
        // setting currentItem to 1 increments it setting it to -1 decrements it
        internal int currentItem // index of currentItem in currentList
        {
            set { if (value > 0) { CurrentItem++; if (CurrentItem >= lists[currentList].Count) { CurrentItem = 0; } } else { CurrentItem--; if (CurrentItem < 0) { CurrentItem = lists[currentList].Count - 1; } } }
            get { return CurrentItem; }
        }

        // owner of inventory
        private GameObject owner;

        Vector2 drawOffset;

        internal Inventory(GameObject owner)
        {
            this.owner = owner;
            drawOffset = new Vector2(-80, -65);

            currentList = 0;
            lists = new List<GameObject>[4];
            for (int i = 0; i < 4; i++)
                lists[i] = new List<GameObject>();

            if (selectorTexture != null) // create animation
            {
                selector = new Animation(selectorTexture, 64, 64, 100);
                selector.setZIndex(0.11f);
            }
        }

        internal void Swap()
        {
        }

        internal void changeCurrentList(int n)
        {
            if (n < 0 || n > lists.Length - 1)
                return;
            currentList = n;
        }

        // draw inventory in ring form
        internal void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            // draw ring
            Vector2 pos = Camera.WorldToScreen(owner.GetPosition()) + owner.drawOffset + drawOffset;
            spriteBatch.Draw(ring, pos, new Rectangle(0, 0, 500, 500), Color.White, 0f, new Vector2(0f) , 0.5f, SpriteEffects.None, 0.2f);
            // draw inventory items
            int index = 0; // index of current item being drawn
            double angle = 0.0;
            foreach (GameObject item in lists[currentList])
            {
                if (index == currentItem) // current item is selected
                    selector.Draw(spriteBatch, gameTime, ItemPosition(pos + radiusVector, angle), true);
                item.DrawIcon(spriteBatch, gameTime, ItemPosition(pos + radiusVector, angle));
                angle += Math.PI * 2 / 10;
                index++;
            }
        }

        // add item to inventory
        internal void Add(GameObject item)
        {
            lists[currentList].Add(item);
        }

        // returns items position for drawing in inventory menu display circle
        internal Point ItemPosition(Vector2 center, double angle)
        {
            return new Point((int)(center.X+ringRadius*Math.Cos(angle)), (int)(center.Y+ringRadius*Math.Sin(angle)));
//            var randomAngle:Number = Math.random() * (Math.PI * 2);
//            var newX:Number = obj.fromX + (obj.radius*Math.cos(randomAngle));
//            var newY:Number = obj.fromY + (obj.radius*Math.sin(randomAngle));
//            return {x:newX,y:newY};
        }

        // returns object index of currently selected item
        internal int GetObjectIndex()
        {
            int index = 0;
            foreach (GameObject item in lists[currentList])
            {
                if (index == item.objectIndex)
                    return index;
                    
            }
            return -1;
        }
    }
}
