﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ProjectSteam.GameObjects
{
    class Tile
    {
        public Texture2D TileSetTexture;
        public static int TileWidth = 64;
        public static int TileHeight = 64;
        internal static int TileWidth2D = 48;
        internal static int TileHeight2D = 48;
        public static int TileStepX = 64;
        public static int TileStepY = 16;
        public static int OddRowXOffset = 32;
        public static int HeightTileOffset = 32;

        public Vector2 originPoint = new Vector2(19, 39);

        Tile()
        {}
        Tile(Texture2D texture, int tilewidth, int tileheight)
        {
            TileSetTexture = texture;
            TileWidth = tilewidth;
            TileHeight = tileheight;
        }

        // returns rectangle to then use when cropping the tileset texture
        public static Rectangle GetSourceRectangle(int tileIndex, Texture2D TileSetTexture, int tWidth, int tHeight)
        {
            int tileY = tileIndex / (TileSetTexture.Width / tWidth);
            int tileX = tileIndex % (TileSetTexture.Width / tWidth);

            return new Rectangle(tileX * tWidth, tileY * tHeight, tWidth, tHeight);
        }
        public static Rectangle GetSourceRectangle(int tileIndex, Texture2D TileSetTexture)
        {
            return GetSourceRectangle(tileIndex, TileSetTexture, TileWidth, TileHeight);
        }
        public static Rectangle GetSourceRectangle2D(int tileIndex, Texture2D TileSetTexture)
        {
            return GetSourceRectangle(tileIndex, TileSetTexture, TileWidth2D, TileHeight2D);
        }
    }
}
