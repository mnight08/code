﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using ProjectSteam.Utils;
using Microsoft.Xna.Framework.Graphics;

namespace ProjectSteam.GameObjects
{
    class StillCannon : Structure
    {
        internal int structureIndex = 1;

        internal StillCannon(ContentManager content)
        {
            load(content);
        }
        internal StillCannon(ContentManager content, int _x, int _y)
        {
            // default position
            SetPosition(new Vector2(_x, _y));
            load(content);
        }

        // This load function must be called before creating an instance of Core
        internal void load(ContentManager content)
        {
            texture = content.Load<Texture2D>(@"Textures\Structures\testBuildItemA");
            setIconTexture(content.Load<Texture2D>(@"Textures\Structures\Icons\testBuildItem"));

            // setup animation; texture was added statically
            animation = new SpriteAnimation(this, texture, 64, 64);
            // scale the image down from 100px to 64
            animation.setScale(1f);
            // setup collision box
            collisionBox = new Rectangle(0, 0, 64, 44);
            drawOffset.X = 0;
            drawOffset.Y = -20;
        }

        internal override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            animation.Draw(spriteBatch, gameTime);
        }

        internal override void Build()
        {
            if (complete)
                return;

            complete = true;
        }
    }
}
