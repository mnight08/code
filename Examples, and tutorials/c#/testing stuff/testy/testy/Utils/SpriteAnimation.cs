﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using ProjectSteam.GameObjects;

namespace ProjectSteam.Utils
{
    class SpriteAnimation
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        ActiveObject sprite;  // reference to owner object
        Texture2D image;      // Sprite Texture
        float timer = 0f;     // A Timer Variable
        float interval;       // Interval between frames
        int[] frameCount;     // array of framecounts for each line
        int currentFrame = 0;     // current frame holder

        // Current image from sprite sheet; One image per horizontal line
        public int currentImage = 0;

        int spriteWidth = 32; // Width of a single image
        int spriteHeight = 32; // Height of a single image

        float scale; // 1.0f is original size, 2.0 doubles.

        Rectangle sourceRect; // A rectangle to store which 'frame' is currently being shown
        Vector2 origin; // The center of the current 'frame'

        internal bool idle; // if animation should be idle

        // Series of constructors
        internal SpriteAnimation(ActiveObject sprite, Texture2D image, int sWidth, int sHeight)
        {
            this.sprite = sprite;
            SetTexture(image);
            interval = 100f;
            SetSpriteDimensions(sWidth, sHeight);
            scale = 1.0f;
            this.frameCount = new int[image.Height / sHeight];
            for (int i = 0; i < frameCount.Length; i++)
            {
                frameCount[i] = image.Width / sWidth;
            }
        }
        public SpriteAnimation(ActiveObject sprite, Texture2D image, int sWidth, int sHeight, float interval)
        {
            this.sprite = sprite;
            SetTexture(image);
            SetInterval(interval);
            SetSpriteDimensions(sWidth, sHeight);
            scale = 1.0f;
            this.frameCount = new int[image.Height / sHeight];
            for (int i = 0; i < frameCount.Length; i++)
            {
                frameCount[i] = image.Width / sWidth;
            }
        }
        public SpriteAnimation(ActiveObject sprite, Texture2D image, int sWidth, int sHeight, float interval, int[] frameCount)
        {
            this.sprite = sprite;
            SetTexture(image);
            SetInterval(interval);
            SetSpriteDimensions(sWidth, sHeight);
            scale = 1.0f;
            this.frameCount = frameCount;
        }
        // Set texture spritesheet image
        public void SetTexture(Texture2D image)
        {
            this.image = image;
        }
        // Set image interval
        public void SetInterval(float interval)
        {
            this.interval = interval;
        }
        // Dimensions of individual image in spritesheet
        public void SetSpriteDimensions(int sWidth, int sHeight)
        {
            spriteWidth = sWidth;
            spriteHeight = sHeight;
        }
        public void setScale(float scale)
        {
            this.scale = scale;
        }

        // Update loop
        private void Update(GameTime gameTime)
        {
            //Increase the timer by the number of milliseconds since update was last called
            timer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            //Check that the timer is more than the chosen interval
            if (timer > interval)
            {
                //Set the next frame
                currentFrame++;
                if (idle)
                    currentFrame--;
                //Reset the timer
                timer = 0f;
            }
            //If we are on the last frame, reset
            if (currentFrame == frameCount[currentImage])
            {
                currentFrame = 0;
            }

            sourceRect = new Rectangle(currentFrame * spriteWidth, currentImage * spriteHeight, spriteWidth, spriteHeight);
            origin = new Vector2(0f, 0f);
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            Update(gameTime);
            spriteBatch.Draw(image, Camera.WorldToScreen(sprite.GetPosition()) + sprite.drawOffset, sourceRect, Color.White, 0f, origin, scale, SpriteEffects.None, 0);
        }
    }
}
