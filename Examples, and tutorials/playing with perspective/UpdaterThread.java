public class UpdaterThread extends Thread{
    EPanel panel;
    UpdaterThread(EPanel p)
    {
        panel=p;
    }
    public void run()
    {
        while(true)
        {
            panel.update();
            panel.repaint();
            try
            {
                Thread.sleep(50);
            }catch( Exception e){}
        }

    }

}
