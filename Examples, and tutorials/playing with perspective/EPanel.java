import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.awt.event.*;
//so the general structure of the program will be to update the player list, and have input affect the player in the list that player reffers to.  this could be
//done with an index to the array, or something similar, but i thought this would be the least confusing.
//cycling through the array seems easier using an integer, so ill do that
public class EPanel extends JPanel implements KeyListener  {

    Random rand;
    Player view;
    Player currentplayer;//reffers to a particular map;
   
    Map[] maps;//a set of all the maps availible.

    EPanel() {
        UpdaterThread ut = new UpdaterThread(this);
        rand = new Random();

        initializemaps();
        initializequest();
        //what is displayed on screen is what ever the player view boxes in.
        //every object that is gonna be displayed will be zeroed, so that the
        //upper left corner of view is the upper left corner of what is
        //displayed
        view = new Player();
        ut.start();
        addKeyListener(this);
        setFocusable(true);
    }
    public void initializequest()
    {
        currentplayer.buildquestlog(this);
        add(new JLabel());
        Quest q= new Quest("welcome to the world");
        q.Q_desc="                  this is your first quest, have fun";
        q.x=40;
        currentplayer.pquestlog.Quests.add(q);
        q= new Quest(" this is your second quest, get to egle");
        q.x=40;
        q.Q_desc="                  this is your second quest, get to egle";
        currentplayer.pquestlog.Quests.add(q);
        currentplayer.pquestlog.addQuest();
    }

    public void initializemaps()
    {
        maps= new Map[2];

        maps[0]= new Map();
        maps[1]= new Map();

        maps[0].initializecharacters();

        maps[0].buildengineering();
        currentplayer= new Player();
        currentplayer.initializeplayer(maps[0]);
        currentplayer.updatepos(982, 507);
        maps[1].initializecharacters();
   //     maps[1].initializedoorsmap1(maps);
        maps[1].buildmazelevel();
        initializedoors(maps);
    }
    public void initializedoors(Map[] maps) {
        Player door,door2;

        door = new Player();
        door.currentmap=maps[0];

        door.initializedoor(maps[0].list, 1381, 63);

        door2= new Player();
        door2.currentmap=maps[1];
        door2.initializedoor(maps[1].list, 20, -20);

        door.otherside=door2;
        door2.otherside=door;
    }
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        currentplayer.currentmap.draw(g,view);
        currentplayer.pquestlog.draw(g);
    }

    public void update() {
        for(int i=0;i<maps.length;i++)
        {
            maps[i].update();
        }

        view.updatedim(getWidth(), getHeight());
        //view keep player in center.
        view.x = currentplayer.x + currentplayer.width / 2 - view.width / 2;
        view.y = currentplayer.y + currentplayer.height / 2 - view.height / 2;
    }

    public void keyPressed(KeyEvent k) {
        if (k.getKeyChar() == 'a') {
            currentplayer.xvel = -10;
        }
        if (k.getKeyChar() == 'w') {
           currentplayer.yvel = -10;
        }
        if (k.getKeyChar() == 'd') {
            currentplayer.xvel = 10;
        }
        if (k.getKeyChar() == 's') {
            currentplayer.yvel = 10;
        }
        //this would be the key to switch characters left
        if (k.getKeyChar() == 'q') {

        }
        if (k.getKeyChar() == ' ')
        {
            currentplayer.interact();
        }
        //this would be the key to switch characters right
        if (k.getKeyChar() == 'e') {

        }

    }

    public void keyReleased(KeyEvent k) {
       currentplayer.xvel = 0;
       currentplayer.yvel = 0;

    }

    public void keyTyped(KeyEvent k) {
    }
   
}
