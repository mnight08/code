/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mnight08
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Quest extends Rectangle implements ActionListener{
    JButton     Q_button;   //quests button: when pressed displays the description
    String      name;
    String      Q_desc;      //quests description
    boolean[]   conditions; //check quest completion
    boolean     isvisible;
        Quest(String n)
        {
            name=n;
            Q_button= new JButton(name);
            Q_button.addActionListener(this);
            isvisible=false;
        }

        public boolean isComplete()
        {   //check if all quest completion conditions are true
            for ( int i = 0; i < conditions.length; i++ )
            {   //if one is false they are all false
                if( conditions[i] == false )
                    return false;
            }
          return true;
        }

        public boolean draw( Graphics g , int x, int y)
        {
            if(isvisible)
            {
                g.setColor(Color.white);
                g.drawString(Q_desc,x,y);
                return true;
            }
            return false;
        }
        public void actionPerformed(ActionEvent e)
	{
            if(e.getSource()==Q_button)
            {
                    System.out.println("i felt that");
                   isvisible=!isvisible;
            }

	}

}
