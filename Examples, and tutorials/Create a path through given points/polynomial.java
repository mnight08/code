package pathmaking;

import java.lang.Math.*;
public class polynomial 
{
	int degree;
	double[] coefficients;
	
	polynomial()
	{
		degree=-1;
		coefficients=null;
	}
	
	polynomial( double[] coef)
	{
		degree=coef.length;
		coefficients=coef;
	
	}

        //return a polynomial that interpolatesthe points given
        polynomial (point[] points)
        {
            //make matrix with coefficients row one is 1, x1, x1^2... x1^n
            matrix m= new matrix(points.length,points.length);

            for(int i=0;i<points.length;i++)
            {
                for(int j=0; j<points.length;j++)
                {
                    m.setelement(i,j,Math.pow(points[i].x,j));
                }
                System.out.println("");
            }


            System.out.println(m);

            //make vector as y points.
            double[] in=new double[points.length];

            //apply linear matrix solver
            System.out.println(m);
           // coefficients = solver(matrix, in);

            //set degree to length of coeeficients
            degree=points.length;
        }

	double evaluate(double t)
	{
		double eval=0;
		for(int i=0; i<degree;i++)
		{
			eval=eval+coefficients[i]*Math.pow( t ,(double) i );
		
		}
                return eval;
	}
}