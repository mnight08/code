package pathmaking;
public class matrix {
    double[][] elements;
    int xdimension;
    int ydimension;
    
    matrix(int xdim, int ydim)
    {
        xdimension=xdim;
        ydimension=ydim;
        elements= new double[xdimension][];
        for( int i=0;i<xdimension;i++)
        {
            elements[i]= new double[ydimension];
        }
    }
    
    public vector solve(vector in)
    {
        vector v= new vector(xdimension);
        
        return v;
    }

    public void setelement(int i, int j, double value)
    {
        elements[i][j]=value;
    }
    
    public String toString()
    {
        String s=""; 
        for(int i=0;i<elements.length;i++)
        {
            for(int j=0;j<elements[0].length;j++)
            {
                s=s+" "+elements[i][j];
            }
            s=s+"\n";
        }
        return s;
    }
}
