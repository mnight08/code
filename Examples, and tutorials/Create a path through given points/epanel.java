package pathmaking;


import javax.swing.*;
import java.awt.*;
public class epanel extends JPanel{

    path p;
    double time;
    epanel()
    {
        polynomial x,y;
        double [] coeff1={300,20,0,-.5};
        double [] coeff2={0,2,4,0,0,-.001,.0000012};
        x= new polynomial(coeff1);
        y= new polynomial(coeff2);
        p=new path(x,y);
        time=0;

    }
    public void paintComponent(Graphics g)
    {
        g.setColor(Color.black);
        g.fillRect(0,0,600,600);
        g.setColor(Color.blue);
        g.fillRect(p.getposition(time).x, p.getposition(time).y, 30, 30);
        System.out.println(p.getposition(time).x+" " +p.getposition(time).y);
        if(time>100)time=0; else time=time+.01;
        repaint();

    }
}
