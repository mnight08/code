package lab3;

import org.lwjgl.input.Mouse;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.math.Matrix3;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;

import com.badlogic.gdx.graphics.Pixmap;

public class Game implements ApplicationListener {
	float rot=0;
    private Camera camera;
	private Mesh mesh;

	Vector3 cameradirection;
	Vector3 cameraup;
	float thetax;
	float thetay;
    @Override
    public void create() {
    	//cameradirection=new Vector3(0,1,0);
    	GL10 gl = Gdx.graphics.getGL10();

    	gl.glEnable(GL10.GL_LIGHTING);

    	gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_POSITION, new float[]{0, 5f, 0, 1}, 0);
    	gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_AMBIENT, new float[]{0.5f, 0.5f, 0.5f, 1f}, 0);
    	gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_DIFFUSE, new float[]{.9f, .9f, .9f, 1f}, 0);
    	gl.glEnable(GL10.GL_LIGHT0);
    	
    	gl.glEnable(GL10.GL_DEPTH_TEST);
    	gl.glShadeModel(GL10.GL_SMOOTH);
    
    	//gl.glPolygonMode(gl.GL_FRONT_AND_BACK,gl.GL_LINE);
    	gl.glEnable(GL10.GL_CULL_FACE);
    	
    	
    	mesh = new Mesh(true, 5, 7, 
    			new VertexAttribute(Usage.Position, 3, "a_position"),
    			new VertexAttribute( Usage.Normal, 3, "a_normal" ),
    			new VertexAttribute(Usage.ColorPacked, 4, "a_color"));
    	
    	mesh.setVertices(new float[] {
    			  0.0f, 1.0f,  0.0f,  0.0f,1.0f,0.0f,              Color.toFloatBits(255, 0, 0, 255),
    			 -1.0f, 0.0f,  1.0f,  -0.7071707f,0.0f,0.7071707f,  Color.toFloatBits(255, 0, 0, 255),
    			  1.0f, 0.0f,  1.0f,  0.7071707f,0.0f,0.7071707f,  			   Color.toFloatBits(255, 0, 0, 255),
    			  1.0f, 0.0f, -1.0f,  0.7071707f,0.0f,-0.7071707f,              Color.toFloatBits(255, 0, 0, 255),
    			 -1.0f, 0.0f, -1.0f,  -0.7071707f,0.0f,-0.7071707f,  			   Color.toFloatBits(255, 0, 0, 255)
    	});   
    	mesh.setIndices(new short[] { 0, 1, 2, 3,4,1});
    }

    @Override
    public void dispose() { }

    @Override
    public void pause() { }

    public void handleInput(float delta)
    {
    	if(Mouse.isButtonDown(0))
    	{
    		
    		camera.direction.set(cameradirection);
    		camera.up.set(cameraup);
    		thetax=(camera.viewportWidth/2-Mouse.getX())/10;
    		thetay=(camera.viewportHeight/2-Mouse.getY())/10;
    	
    		camera.rotate(thetax, camera.up.x,camera.up.y,camera.up.z);
    		Vector3 axis=new Vector3(camera.direction);
    		axis.crs(camera.up);
    		axis.mul(-1);
    		System.out.println(axis);
    		camera.rotate(thetay,axis.x,axis.y,axis.z);
    		//sorta works
    		//camera.direction.set(cameradirection);
    		//	Vector3 vec= new Vector3(cameradirection);
    		//	vec.add((new Vector3(camera.viewportWidth/2-Mouse.getX(),camera.viewportHeight/2-Mouse.getY(),0)).mul(1/(new Vector3(camera.viewportWidth/2-Mouse.getX(),camera.viewportHeight/2-Mouse.getY(),0)).len2()));
    		//	camera.direction.set(vec);

    
    	}
    	else 
    	{
    		camera.direction.set(cameradirection);
    		camera.up.set(cameraup);
    	}
    }

    @Override
    public void render() {
    	float delta=Gdx.graphics.getDeltaTime();
    	handleInput(delta);
        camera.update();
        camera.apply(Gdx.gl10);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        Gdx.gl.glClear(GL10.GL_DEPTH_BUFFER_BIT);
        //does not actually clear anything, only sets the clear value.
        //Gdx.gl.glClearColor(0, 0, 0, 0);
        //Gdx.gl.glClearDepthf(1.0f);

    	rot+=delta*100%90;
    	Gdx.gl10.glRotatef(rot,0,1.0f,0);
    	
        mesh.render(GL10.GL_TRIANGLE_FAN);
    }

    @Override
    public void resize(int width, int height) {
        float aspectRatio = (float) width / (float) height;
        camera = new PerspectiveCamera(67,2f * aspectRatio, 2f);
        camera.position.set(0,2.0f,3.0f);
        camera.lookAt(0, 0, 0);
        if(cameradirection==null)cameradirection=new Vector3(camera.direction);
        if(cameraup==null)cameraup=new Vector3(camera.up);
    }
    
    
    public void updateview()
   {	

    	
    
    }

    @Override
    public void resume() { }
}
