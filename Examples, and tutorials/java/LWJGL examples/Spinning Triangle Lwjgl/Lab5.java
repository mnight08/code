/*************************************************************
 * Basic 3d lab
 *
 * Based on code created by Jeff Molofee (nehe.gamedev.net)
 * and Kevin Glass (cokeandcode.com).
 *
 * Port of NeHe's tutorials to LWJGL by Mark Bernard.
 *
 */
import org.lwjgl.Sys;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.input.Keyboard;

import java.io.IOException;

public class Lab5 {

    private boolean done = false;
    private boolean fullscreen = false;
    private final String windowTitle = "Basic 3d w/ LWJGL";
    private boolean f1 = false;
    private DisplayMode displayMode;

	int prevtime;
	int theta;
	Texture texture;
	TextureLoader tl;
    public static void main( String args[] ) {
        
        boolean fullscreen = false;
        if( args.length > 0 ) {
            if( args[0].equalsIgnoreCase( "fullscreen" ) ) {
                fullscreen = true;
            }
        }

        Lab5 l5 = new Lab5();
        l5.run( fullscreen );
    }

    /**
     * Launch point
     * @param fullscreen boolean value, set to true to run in fullscreen mode
     */
    public void run( boolean fullscreen ) {
        
        this.fullscreen = fullscreen;
        try {
            init();
            while (!done) {
                handleInput();
                render();
                Display.update();
            }
            cleanup();
        }
        catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

	
    /**
     * Very basic for first lesson.  Just check for escape key or user
     * clicking to close the window.
     */
    private void handleInput() {
        if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {       // Exit if Escape is pressed
            done = true;
        }
        if(Display.isCloseRequested()) {                     // Exit if window is closed
            done = true;
        }
        if(Keyboard.isKeyDown(Keyboard.KEY_F1) && !f1) {    // Is F1 Being Pressed?
            f1 = true;                                      // Tell Program F1 Is Being Held
            switchMode();                                   // Toggle Fullscreen / Windowed Mode
        }
        if(!Keyboard.isKeyDown(Keyboard.KEY_F1)) {          // Is F1 Being Pressed?
            f1 = false;
        }
    }

    private void switchMode() {
        fullscreen = !fullscreen;
        try {
            Display.setFullscreen(fullscreen);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * For rendering all objects to the screen
     * @return boolean for success or not
     */
    private void render() {
		texture.bind();
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
        GL11.glMatrixMode( GL11.GL_MODELVIEW );
		GL11.glLoadIdentity();
		Sys.getTime();
		
		//move 
		GL11.glTranslatef(-1.5f,0.0f,-6.0f);
		
		triangulate();
				
		//move 
		GL11.glTranslatef(3.0f,0.0f,0.0f);
		theta=(theta+getDelta())%90;
		GL11.glRotatef(theta,0,0,1);
		triangulate();

		//draw shifted triangle
		//triangulate();
		//GL11.glRotatef( 74, 0, 1, 0 );
		//GL11.glTranslatef(4.0f,0.0f,-6.0f);

    }
	
	private int getDelta()
	{
		int delta=(int)(Sys.getTime()-prevtime);
		prevtime=(int)(Sys.getTime());
		return delta;
	}
	
	private void triangulate()
	{
		//draw triangle
		GL11.glBegin( GL11.GL_TRIANGLES );
			GL11.glColor3f(255,0,0);
			GL11.glTexCoord2f(0.5f,0.0f);
			GL11.glVertex3f( 0.0f, 1.0f, 0.0f);
			
			GL11.glColor3f(0,255,0);
			GL11.glTexCoord2f(0.0f,1.0f);
			GL11.glVertex3f(-1.0f,-1.0f, 0.0f);
						
			GL11.glColor3f(0,0,255);
			GL11.glTexCoord2f(1.0f,1.0f);
			GL11.glVertex3f( 1.0f,-1.0f, 0.0f);
		GL11.glEnd();
	}



    /**
     * Initialize the environment
     * @throws Exception
     */
    private void init() throws Exception {

        createWindow();
        initGL();
		prevtime=(int)Sys.getTime();
		theta=0;
		tl=new TextureLoader();
		try{
			texture=tl.getTexture("jim.png");
		}catch(IOException ex){}
    }

    /**
     * Create a window for all display
     * @throws Exception
     */
    private void createWindow() throws Exception {
        Display.setFullscreen(fullscreen);
        DisplayMode d[] = Display.getAvailableDisplayModes();
        for (int i = 0; i < d.length; i++) {
            if (d[i].getWidth() == 640
                && d[i].getHeight() == 480
                && d[i].getBitsPerPixel() == 32) {
                displayMode = d[i];
                break;
            }
        }
        Display.setDisplayMode(displayMode);
        Display.setTitle(windowTitle);
        Display.create();
    }

    /**
     * Initialize OpenGL
     *
     */
    private void initGL() {
        
        // some typical gl parameters
        GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // Black Background
        GL11.glClearDepth(1.0); // Depth Buffer Setup
		GL11.glEnable(GL11.GL_TEXTURE_2D); // Enable Texture Mapping
        GL11.glEnable(GL11.GL_DEPTH_TEST); // Enables Depth Testing
		GL11.glShadeModel(GL11.GL_SMOOTH); // Enable Smooth Shading
        GL11.glDepthFunc(GL11.GL_LEQUAL); // The Type Of Depth Testing To Do

        // camera setup goes here
		GL11.glMatrixMode( GL11.GL_PROJECTION );
		GL11.glLoadIdentity();
		GLU.gluPerspective( 45.0f,
                    (float) displayMode.getWidth() / (float) displayMode.getHeight(),
                    0.1f,
                    100.0f);
		GL11.glHint(GL11.GL_PERSPECTIVE_CORRECTION_HINT, GL11.GL_NICEST);

    }

    /**
     * Cleanup all the resources.
     *
     */
    private void cleanup() {
        Display.destroy();
    }

}
