/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author afmartinez4
 */
import javax.swing.*;
import java.awt.*;

public class bricks {

    int[] xpos;
    int[] ypos;
    int length;
    int width;

    bricks(int n, int w, int l) {
        xpos = new int[n];
        ypos = new int[n];
        width = w;
        length = l;
        for (int i = 0; i < n; i++) {
            xpos[i] = -1;
        }
    }
    public boolean pointincircle(int xp, int yp, int x, int y, int r)
    {
        if(((xp-x)*(xp-x)+(yp-y)*(yp-y)<=r*r))
                return true;
        else return false;
    }

    public boolean pointinrect(int xp, int yp, int x, int y)
    {
        if((xp-width/2<=x&&x<=xp+width/2)&&(yp-length/2<=y&&y<=yp+length/2))
            return true;
        else return false;
    }
    public boolean checkcollisionrect(int xp, int yp, int x, int y, int r)
    {
        if(pointinrect(xp,yp,x,y)||pointinrect(xp,yp,x-r,y)||pointinrect(xp,yp,x,y-r)||pointinrect(xp,yp,x,y+r)||pointinrect(xp,yp,x+r,y)||pointincircle(xp-width/2,yp-length/2,x,y,r)||pointincircle(xp+width/2,yp-length/2,x,y,r)||pointincircle(xp+width/2,yp+length/2,x,y,r)||pointincircle(xp-width/2,yp+length/2,x,y,r))
        return true;
        else return false;
    }
    public int checkcollisionball(int x, int y, int r)
    {
        //just does one collision at a time
        for( int i=0;i<xpos.length;i++)
        {
            if(xpos[i]!=-1)
                if(checkcollisionrect(xpos[i],ypos[i],x,y,r))
                    return i;
        }
        return -1;
    }
    public void insert(int x, int y) {
        for (int i = 0; i < xpos.length; i++) {
            if (xpos[i] == -1) {
                xpos[i] = x;
                ypos[i] = y;
                break;
            }

        }
    }

    public int getsize()
    {
        
        return xpos.length;
    }

    public int getwidth() {

        return width;
    }

    public int getlength() {

        return length;
    }

    public int getxn(int n) {
        if (n < xpos.length) {
            return xpos[n];
        } else {
            return -1;
        }
    }

    public int getyn(int n) {
        if (n < ypos.length) {
            return ypos[n];
        } else {
            return -1;
        }
    }

    public void remove(int i) {
        xpos[i]=-1;ypos[i]=-1;
    }

    public void ivebeenhit() {
    }
}
