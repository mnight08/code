package openglWaterSimulation;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;

public class Game implements ApplicationListener {

	//camera and its properties
	OrthographicCamera camera;
	Texture img;
	SpriteBatch batch;

	//Meters per second
	float camera_speed=1;

	//Degrees per second
	float camera_rotation_speed=1;



	@Override
	public void create() {
		batch=new SpriteBatch();
		img=new Texture("resources/TheWorld.png");
		camera = new OrthographicCamera(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		Initialize_Lighting();
		

	}


	@Override
	public void resize(int width, int height) {
		//float aspectRatio = (float) width / (float) height;
		//camera = new PerspectiveCamera( 67, 2f * aspectRatio, 2f );
		//camera.near = 0.1f;
		//camera.far = 500;
	
		// set a nice initial view
		//camera.position.set(0,0,10);
	}


	public void update()
	{
		handleInput(Gdx.input, Gdx.graphics.getDeltaTime());

		camera.update();
	}

	@Override
	public void render() {
		
		update();
		Gdx.gl.glClearColor(1,0,0,1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		camera.update();
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		batch.draw(img,0,0);
		batch.end();
        	//gl.glClear( gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT );
		

		
		//lake.render();
		
	}
	
	@Override
	public void pause() {
		// TODO Auto-generated method stub
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	public void Initialize_Lighting()
	{

/*		gl.glEnable(GL20.GL_LIGHTING);
		// Character lighting
		gl.glEnable(GL20.GL_LIGHT0);
		// Sun
		gl.glEnable(GL20.GL_LIGHT1);

		// Color is red to simulate torch light
		gl.glLightfv(GL20.GL_LIGHT0, GL20.GL_DIFFUSE, new float[]{ 2f, 1f, 1f, 3f }, 0);
	
		// Set up sun
		gl.glLightfv(GL20.GL_LIGHT1, GL20.GL_DIFFUSE, new float[]{ 1.6f, 1.6f, 1.9f, 130f }, 0);
		
		gl.glEnable(GL20.GL_BLEND);
*/
	}

	//This should be broken down to a set of specific functions that specify what is begin handled.
	public void handleInput(Input input, float delta){


		//for these rotations we want to rotate the camera
		//around the axis 
		//rotate up
		if( input.isKeyPressed( Keys.UP ) ){
			Vector3 axis=camera.up.crs(camera.direction);
			camera.rotate(axis, camera_rotation_speed*delta);
			}
		
		//rotate left
		if( input.isKeyPressed( Keys.LEFT ) ){

			camera.rotate(camera.up, camera_rotation_speed*delta);
		}
		
		//rotate right
		if( input.isKeyPressed( Keys.RIGHT) ){
			camera.rotate(camera.up, camera_rotation_speed*delta);
		}
		//rotate down
		if( input.isKeyPressed( Keys.DOWN ) ){
			Vector3 axis=camera.up.crs(camera.direction);
			camera.rotate(axis, -camera_rotation_speed*delta);
		}


		//move forward
		if( input.isKeyPressed( Keys.W ) ){
			camera.translate(camera.direction.scl(camera_speed*delta));
		}
		//move back
		if( input.isKeyPressed( Keys.S ) ){
			camera.translate(camera.direction.scl(-camera_speed*delta));
		}
		//move left
		if( input.isKeyPressed( Keys.A ) ){
			Vector3 left =camera.direction.crs(camera.up);
			camera.translate(left.scl(camera_speed*delta));
		}
		//move right
		if( input.isKeyPressed( Keys.D ) ){
			Vector3 right=camera.up.crs(camera.direction);
			camera.translate(right.scl(camera_speed*delta));
		}
		//move up
		if( input.isKeyPressed( Keys.Q ) ){
			camera.translate(camera.up.scl(camera_speed*delta));
		}
		//move down
		if( input.isKeyPressed( Keys.E ) ){
			camera.translate(camera.up.scl(-camera_speed*delta));
		}
		
		
		
	}



	

	

}
