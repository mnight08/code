package com.mygdx.game
import com.badlogic.gdx.Input.TextInputListener;


class BodyOfWater{
	Vector3 center;
	Vector2 size;
	Vector2 dimension;
	Vector2 resolution;
	mesh water;
	float[] vertices;
	float[] indices;
	float currentTime;
	float vertexSize;
	float numVertices;
	public BodyOfWater()
	{

		currentTime=0;
		center=Vector3(0,0,0);
	 	size=Vector2(100,100);
		dimension=Vector2(100,100);
		resolution=Vector2(size[0]/dimension[0],size[1]/dimension[1]);
		mesh=new mesh();
		vertexSize=water.getVertexSize();
		numVertices=water.getNumVertices();
		float[] vertices=new float[dimension[0]*dimension[1]*water.getVertexSize()];

		for(int i=0;i<dimension[0];i++)
			for(int j=0;j<dimension[1];j++){

				vertices[dimension[1]*i+j]=center[0]-size[0]/2*j*resolution[0];
				vertices[dimension[1]*i+j+2]=0;
				vertices[dimension[1]*i+j+2]=center[1]-size[1]/2*i*resolution[1];

			}
		water.setVertices(vertices);	
		float[] indices=new float[dimension[0]*2+1];
		indices[0]=0;
		indices[1]=1;

		boolean goingRight=true;
		boolean moveDown=true;
		boolean moveUp=true;
		boolean moveLeft=true;
		boolean moveRight=true;


		for(int i=2;i<dimension[0]*2+1];i++)
		{
			if(goingRight)
			{
				if(moveDown){
					indices[i]=indices[i-1]+dimension[1]-1;
					goingDown=false;}
				else if(moveRight){

					goingDown=true;
				}
				else if(moveLeft){

					goingDown=true;
				}
	
			}
			else if(!goingRight)
			{

				if(moveDown){
					indices[i]=indices[i-1]+dimension[1];
					goingDown=false;}
				else if(moveLeft){

					goingDown=true;
				}
				else if(moveLeft){

					goingDown=true;
				}
			}
				
		}


	}

	public void update(camera cam, float delta)
	{
		//go through all visible vertices and update heights.
		water.getVertices(0,vertices);
		for(int i=0;i<numVertices;i++)
		{
			if(isVisible(vertices[i*vertexSize],vertices[i*vertexSize+1],vertices[i*vertexSize+2],cam))
			{
				vertices[i*vertexSize+1]=h(vertices[i*vertexSize],vertices[i*vertexSize+2], currentTime+delta);
			}
		}
		water.setVertices(vertices);		
		currentTime+=delta;
	}

	public void render()
	{
	}

	public float h(x,z,t)
	{
		x+z-t;
	}

	public boolean isVisible(float x, float y, float z, camera cam)
	{
		return true;
	}



}
