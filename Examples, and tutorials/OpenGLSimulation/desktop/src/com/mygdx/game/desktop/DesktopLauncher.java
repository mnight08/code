package openglWaterSimulation;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;

public class GameDesktop{
	 public static void main (String[] argv) {
         
		 new LwjglApplication(new Game(), "Water Simulation", 1024, 768);               
 }
}
