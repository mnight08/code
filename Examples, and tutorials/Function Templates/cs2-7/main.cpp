//-----------------------------------------------------------------
// Author:	Zhixiang Chen
// Class:	CSCI/CMPE 2380.01, Fall, 2008
// Lab 7:	Function templates
// Date:	September 21, 2008
// Comment: The code here is meant to be revised.
//
//-----------------------------------------------------------------
// Lab Exercise 7: 
//
// This lab exercise is practice function template
//
// Compile and run your program. When everything is fine,
// print your .cpp and .h files and turn it to me.
// *****************************************************************


#include "headers.h"

using namespace std; 

int main()
{
	vector<int>  cat(5); 
	int  dog[5]; 
	int i, k, largest; 
	unsigned int seed; 
	bool flag; 

	//create a vector and an arrayy of 20 random int values
	cout<<"Enter a random seed => "; 
	cin >>seed; 
	srand(seed); 
	for (i=0; i<5; i++)
	{
		cat[i]=rand()%101; 
		dog[i]=rand()%101;
		//cout<<cat[i]<<"  "; 
		//if ((i+1)%10 ==0)
		//	cout<<endl; 
	}
	//cout cat
	cout<<"cat elements are ==> " <<endl; 
	for (i=0; i<5; i++)//because it is only doing 5 num bers, i+1%10  will never be zero
	{
		cout<<cat[i]<<"  "; 
		if ((i+1)%10 ==0)
			cout<<endl; 
	}
	cout<<endl; 
	//cout dog elements
	cout<<" dog elements are => "<<endl; 
	for (i=0; i<5; i++)
	{
		cout<<dog[i]<<"  "; 
		if ((i+1)%10 ==0)
			cout<<endl; 
	}
	cout<<endl; 

	/************************************************************************
	 * Part One: practice findMax
	 ************************************************************************/
	
	cout<<" the max in cat is " <<findMax<int>(cat)<<endl; 

	/*************************************************************************************
	 * Part Two: Practice finKthMax
      ************************************************************************************/
	   
	cout<<"Enter a k value => "; 
	cin>>k; 
	flag = findKthMax<int>(dog, 5, k, largest); 
	if (flag)						//find the kth-largest
		cout<<"the "<<k<<"-th largest element in dog is " <<largest <<endl; 
	

	/***********************************************************************************************
	 Part C: for you to complete
	 (1) Write a function template to print any given list of elements in reverse
		 order. Assume that the list of element is stored in an array. 
	 (2) Test your function template with (1) a list of integers; (2) a list a string type words; 
	     and (3) a list a nameType names as done in lab2380_5.cpp, lab2380_5_head.h
	 ***********************************************************************************************/
	const int size=5;
	int list[size]={1,2,3,4,5};
	nameType name[size];
	
	name[0].setName("abc", "def");		//should try and think of a better way to initialize this list
	name[1].setName("ghi", "jkl");
	name[2].setName("mno", "pqr");
	name[3].setName("stu", "vwx");
	name[4].setName("y", "z");


	string strings[size]={"abc", "def","ghi", "jkl", "mno"};
	
	//template seems to work just fine
	reverse<int> (list,size);
	reverse<string>(strings, size);
	reverse<nameType> (name, size);	

	//complete the program 
	return 0; 
}
