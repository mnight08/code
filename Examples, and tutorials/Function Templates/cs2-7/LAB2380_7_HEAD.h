// *****************************************************************
// Author:	Zhixiang Chen
// Class:	CSCI/CMPE 2380.01, Fall, 2008
// Lab 7:	Function template
// Date:	September 21, 2008
// Comment: The code here is meant to be revised.
// *****************************************************************



using namespace std; 

#ifndef		LAB2380_7_HEAD_H		
#define		LAB2380_7_HEAD_H
#include"headers.h"
/*************************************************************
  * declarae a valueType class template
  * the data member value is of generic type 
  ************************************************************/

/***************************************************************
 Delcare a function template to findMax: 
 This fucntion template find the largest element from any given
 list of elements. Assume a vector is used to store the elements
 ***************************************************************/
template <class type>
const type& findMax(const vector<type>& list) 
{
	int maxIndex=0;
	for (int i=0; i<list.size(); i++)
	{
		if (list[maxIndex]<list[i])				//this implies that type must support < operator
			maxIndex=i;
	}
	return list[maxIndex];
}

/***************************************************************
 Delcare a function template to findKthMax: 
 This fucntion template find the k-th largest element from any given
 list of element. The 1-th (or first) largest is the smallest, 
 the 2-th (second) largest element is the second smallest. 
 Assume an array is used to store the list of element. 
 ***************************************************************/
template <class type>
bool findKthMax(const type list[], const int size, const int k, type& element) 
{
	int i, j;							//loop vars
	type tmp;							//for swap
	type * buff;						//for buffering

	//check the range of k
	if (k <=0 || k > size)
	{
		cout<< k << " is out of the range of the list. " <<endl; 
		return false; 
	}
	
	//create a buffer of k elements
	buff = new type[k+1];				//add one more for easy coding

	//get the first element
	buff[0]=list[0];					
	
	//insert the first k elements of the list into the buffer in increasing order
	for (i=1; i<k; i++)
	{
		tmp = list[i];					//need to insert list[i+1] to buff
		for (j=i; j>=0;  j--)
		{
			if (tmp < buff[j-1])		//insert from the back. If smaller than move forward
			{
				buff[j]= buff[j-1];
			}
			else 
			{							//find the position to insert list[j]
				buff[j]=tmp;			//insert list[i+1]
				break;					
			}
		}
	}

	//insert the next size - k elements 
	for (i=k; i< size; i++)
	{
		tmp = list[i]; 

		for (j=k; j>=0;  j--)
		{
			if (tmp < buff[j-1])		//insert from the back. If smaller than move forward
			{
				buff[j]= buff[j-1];
			}
			else 
			{	
				buff[j]=tmp;		//insert list[i+1]
				break;					
			}
		}
	}

	//find the k-th largest element
	element = buff[k-1]; 
	
	delete buff;						//de-allocate memory		

	return true; 
}

#endif 
