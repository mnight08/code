/****************************************************************************************
*  Program Name:		recursionTwo_head.h
*  Lab 17:				Header file for Lab 17
*  Date:				01/15/2009
*
*  Description: This lab practice 
*  (1) Euclid algorithm: It was a well-known algorithm for finding the great 
*  common divisor (gcd) of two integers. The algorithm is very efficient with logarithmic 
*  time complexity. 
*  The Euclid algorithm gcd works as follows: Given any two integers x and y, if y=0; then 
*  gcd(x,y) = x. Otherwise, gcd(x,y) = gcd(y, x%y).
*  (2) Print a char string in reverse order
*****************************************************************************************/

#ifndef H_recrusionTwo_head
#define H_recrusionTwo_head

#include "headers.h"
using namespace std; 

// this function shows the user menu
void showMenu()
{
	cout<<"1: Play with the recursive gcd algrithm"<<endl
		<<"2: Play with the iterative gcd algorithm"<<endl
		<<"3: Print a char string in reverse order"<<endl
		<<"0: To stop "<<endl
		<<"Enter your option => "; 
}

//the interative gcd glorithm 
int gcdIterative( int x, int y)
{
	int left, right, tmp; 

	left = x; 
	right = y; 
	
	while (right!=0)
	{
		tmp = left; 
		left = right; 
		right = tmp%left; 
	}
	return left; 
}

//the recursive gcd algorithm
int gcdRecursive (int x, int y)
{
	//your code here
	if(x%y==0)
		return y;
	else
		gcdRecursive(y, x%y);
	
	//return 10;//10 is a placehold value//this causes errors if done like this
}

//print a string of chars in reverse order 
//print("abcdefg") outputs "gfedcba"
void printReverse(char *p)
{	
	char *end;
	//your code here
	end=(p+(strlen(p)-1));
	for(;end>=p;end--)
	{
	cout<<*end;
	}
}


#endif
