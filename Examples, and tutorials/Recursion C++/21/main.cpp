// *****************************************************************
// Progran name:	lab2380_17.cpp
// Author:			alex martinez
// Class:			CSCI/CMPE 2380, Spring 2009
// Lab 17:			More recursions
// Date:			January 15, 2009
// Comment: The code here is meant to be revised.
//
//-----------------------------------------------------------------
//
// This lab exercise is practice more recursions. 
// 
// Compile and run your program. When everything is fine,
// print your .h and .cpp files and turn them to me or the TA.
// *****************************************************************

#include "headers.h" 

using namespace std; 

int main()
{
	//var declaration
	int x, y, gcd; 
	char str[1000]; 
	int option; 

	//loop forever until the user enter an option to stop
	do 
	{
		cout<<endl; 
		showMenu();					//display user menu
		cin>>option;				//get option from the user

		switch(option)				//use switch to deal with different options
		{
		case 1:						//play with iteratice Euclidean algorithm to find gcd
				cout<<"\nplay with iteratice Euclidean algorithm to find gcd ... "<<endl; 
				cout<<"enter an integer => "; 
				cin>>x; 
				cout<<"enter another integer => "; 
				cin>>y; 
				gcd = gcdIterative(x, y); 
				cout<<"the gcd of "<<x <<" and " <<y <<" is " << gcd<<endl; 
				break;
		case 2:						//play with recursive Euclidean algorithm to find gcd	
				cout<<"\nplay with recursive Euclidean algorithm to find gcd ... "<<endl; 
				cout<<"enter an integer => "; 
				cin>>x; 
				cout<<"enter another integer => "; 
				cin>>y; 
				gcd = gcdRecursive(x, y); 
				cout<<"the gcd of "<<x <<" and " <<y <<" is " << gcd<<endl; 
				break;
		case 3:						//play with haveFunThree	
				cin.ignore();		//this is needed if getline is to be used
				cout<<"\nPrint a char string in reverse order ... "<<endl; 
				cout<<"enter a string => "; 
				cin.getline(str, 999, '\n'); 
				printReverse(str); 
				break;
		
		default: 
				cout<<"\nWrong option. Try again. " <<endl;
		} 
	}while (option != 0);			//stop when option is 0

	//complete the program
	return 0; 
}
