// *****************************************************************
// Progran name:	lab2380_18.cpp
// Author:			alex martinez
// Class:			CSCI/CMPE 2380, Spring 2009
// Lab 18:			Additional recursion practice
// Date:			January 15, 2009
// Comment: The code here is meant to be revised.
//
//-----------------------------------------------------------------
//
// This lab exercise is to do recursion practice.
// 
// Compile and run your program. When everything is fine,
// print your .h and .cpp files and turn them to me or the TA.
// *****************************************************************

#include "headers.h" 

using namespace std; 

int main()
{
	//var declaration
	int a, b; 
	int option; 

	//loop forever until the user enter an option to stop
	do 
	{
		cout<<endl; 
		showMenu();					//display user menu
		cin>>option;				//get option from the user

		switch(option)				//use switch to deal with different options
		{
		case 1:						//play with iteratice game
				cout<<"\nplay with iteratice Euclidean algorithm to find gcd ... "<<endl; 
				getInterval(a,b); 
				cout<<"now guess a number between " <<a <<" and " <<b <<endl
					<<" and hide the number from me .... \n" <<endl; 
				gameIterative(a, b);//play the game
				break;
		case 2:						//play with recursive game	
				cout<<"\nplay with iteratice Euclidean algorithm to find gcd ... "<<endl; 
				getInterval(a,b); 
				cout<<"now guess a number between " <<a <<" and " <<b <<endl
					<<" and hide the number from me .... \n" <<endl; 
				gameRecursive(a, b);//play the game
				break;
		case 0:						//stop	
				break; 
		default: 
				cout<<"\nWrong option. Try again. " <<endl;
		} 
	}while (option != 0);			//stop when option is 0

	//complete the program
	return 0; 
}
