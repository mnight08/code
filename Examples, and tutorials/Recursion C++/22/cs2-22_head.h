/****************************************************************************************
*  Program Name:		lab2380_18.cpp
*  Lab 18:				Header file for Lab 18
*  Date:				01/15/2009
*
*  Description: This lab practices more recursion.
*		A game of finding a hidden number in a given interval. The user guesses a number 
*		x between two numbers, say, 1 and 100. x is hidden from the program. The program can
*		find this hidden number as follows: 
*		(a) Guess a number y, ask the user if y is x. If, yes, the user says so, hence 
*			the game ends. 
*		(b) If x < y, the user says too high. 
*		(c) If x > y, the user says too low.
*		Repeat (a) to (c) until x is found. 
*
*		I have an iterative function to play the game. You need to do a recursive function
*		to play the game.
*****************************************************************************************/

#ifndef H_lab2380_18_head
#define H_lab2380_18_head

#include "headers.h"
using namespace std; 

// this function shows the user menu
void showMenu()
{
	cout<<"1: Play with the Iterative Game of Finding a Hidden Number"<<endl
		<<"2: Play with the Recursive Game of Finding a Hidden Number"<<endl
		<<"0: To stop "<<endl
		<<"Enter your option => "; 
}

//this function get the interval end points a and b
void getInterval(int & a, int & b) 
{
	cout<<"what range do you want the hidden number lower bound >= "; 
	cin>>a; 
	cout<<"what range do you want the hidden number upper bound <="; 
	cin>>b; 
	cin.ignore(200, '\n');
}


//This function find the hidden number between a nd b iteratively
void gameIterative(int a, int b)
{
	assert(a<=b);				//make sure a <= b 

	int middle, 
		left, 
		right; 
	string answer; 

	left = a; 
	right = b; 
	while (left<=right)
	{
		middle = (left + right)/2;  
	
		cout<<"I think your hidden is "<<middle<<".\n"; 
		cout<<"Tell me your answer please (yes; too high; too low) => ";  
		getline(cin, answer, '\n');
		cout<<" anwer is " <<answer <<endl; 
		//cin.ignore(200, '\n');
		if (answer == "yes")
		{
			cout<<"Wow, I found your hidden number "<< middle <<endl;
			break; 
		}
		else if (answer == "too high")
		{
			right = middle - 1; 
		}
		else 
		{
			left = middle + 1; 
		}
	}
}
//This function find the hidden number between a nd b recursively
void gameRecursive(int a, int b)
{
	assert(a<=b);				//make sure a <= b 

	int middle, 
		left, 
		right; 
	string answer; 

	left = a; 
	right = b; 

		middle = (left + right)/2;  
	
		cout<<"I think your hidden is "<<middle<<".\n"; 
		cout<<"Tell me your answer please (yes; too high; too low) => ";  
		getline(cin, answer, '\n');
		cout<<" anwer is " <<answer <<endl; 
		//cin.ignore(200, '\n');
		if (answer == "yes")
		{
			cout<<"Wow, I found your hidden number "<< middle <<endl;
			return; 
		}
		else if (answer == "too high")
		{
			right = middle - 1; 
		}
		else 
		{
			left = middle + 1; 
		}
		gameRecursive(left, right);
	
}

#endif
