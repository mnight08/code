// *****************************************************************
// Progran name:	lab2380_16.cpp
// Author:			alex martinez
// Class:			CSCI/CMPE 2380, Spring 2009
// Lab 16:			Simple recursions
// Date:			January 15, 2009
//
//Comment: The code here is meant to be revised.
//-----------------------------------------------------------------
//
// This lab exercise is practice simple recursions
// 
// Compile and run your program. When everything is fine,
// print your .h and .cpp files and turn them to me or the TA.
// *****************************************************************
#include "headers.h" 

using namespace std; 

int main()
{
	//var declaration
	int size = 100;					//array size
	int numbers[100];				//an array for storing int values
	int option;						//for user option
	int num;						//an int value to play recursions
	int largest;					//store the largest int value in the array

	/*************************************************************************
	 Problem for you: 
	 (1)  As you can see, haveFunOne and haveFunTwo run forever, try to use 
		  static local variable to prevent the function from 
		  running forever. Say, once it runs 1000 times, then stop.
	 (2)  recursiveMaxInt and iterativeMaxInt may not work correctly to find 
		  the largest int value in the array. If this is the case, try to 
		  correct the function(s).
	 *************************************************************************/

	//loop forever until the user enter an option to stop
	do 
	{
		showMenu();					//display user menu
		cin>>option;				//get option from the user

		switch(option)				//use switch to deal with different options
		{
		case 1:						//play with haveFunOne	
				cout<<"enter number => "; 
				cin>>num; 
				haveFunOne(num); 
				break;
		case 2:						//play with haveFunTwo	
				cout<<"enter number => "; 
				cin>>num; 
				haveFunTwo(num); 
				break;
		case 3:						//play with haveFunThree	
				cout<<"enter number => "; 
				cin>>num; 
				haveFunThree(num); 
				break;
		case 4:						//load int values	
				loadIntegers(numbers, size); 
				break;

		case 5:						//recursively find the largest int value	
				largest = recursiveMaxInt(numbers, size);
				cout<<"the largest int value is " <<largest<<endl; 
				break;
		case 6:						//iteratively find the largest int value	
				largest = iterativeMaxInt(numbers, size);
				cout<<"the largest int value is " <<largest<<endl; 
				break;
		case 0:						//stop the loop
				break; 
		default: 
				cout<<"Wrong option. Try again. " <<endl;
		} 
	}while (option != 0);			//stop when option is 0

	//complete the program
	return 0; 
}
