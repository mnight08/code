/****************************************************************************************
*  Program Name:		lab2380_16_head.h
*  Lab 16:				Header file for Lab 16
*  Date:				01/15/2009
*  Description: We pratice basic recursions in this lab exercise 
*****************************************************************************************/

#ifndef H_lab2380_20_head
#define H_lab2380_20_head

#include "headers.h"
using namespace std; 

// this function shows the user menu
void showMenu()
{
	cout<<"1: Play recursion without proper base case"<<endl
		<<"2: Play recrusion with proper base case but bad general case"<<endl
		<<"3: Play recursion with proper base and general case"<<endl
		<<"4: Load an array of int values " <<endl
		<<"5: Call recusive function to find the the largest int in the array" <<endl
		<<"6: Call iterative function to find the largest int in the array" <<endl
		<<"0: To stop "<<endl
		<<"Enter your option => "; 
}

//this function has no proper base case
void haveFunOne( int n)
{
	//add a part to prevent the function from running forever
	
	if(n>0)
		n--;
	else 
		return;
	//the real body of the function
	cout<<"Hehe, see, I have no base case." <<endl; 
	haveFunOne(n);					//the general case
}

//this function has no proper general case
void haveFunTwo( int n)
{
	//add a part to prevent the function from running forever


	//the real body of the function
	if (n<=0)						//base case
	{
		cout<< n <<" <= 0 is the base case. Here, I stop. " <<endl;
		return;
	}
	else
	{
		cout<< n << " >0 is a general case. "<<endl
			<< "Here, I should have done reduction to a simpler case "
			<< " but, guess what, I didn't." <<endl; 
		n--;
		haveFunTwo(n);				//the general case without reduction to a simpler case
	}
}

//this function has proper base case and general case
void haveFunThree( int n)
{
	if (n<=0)						//base case
	{
		cout<< n <<" <= 0 is the base case. Here, I stop. " <<endl;
		return;
	}
	else
	{
		cout<< n << " >0 is a general case. "<<endl
			<< "Here, I DID reduction to a simpler case "<<endl
			<< n << " is changed to " << n-1 <<endl;
		haveFunThree(n-1);			//the general case without reduction to a simpler case
	}
}

//this functions load an array of integers
void loadIntegers(int cat[], int size)
{
	unsigned seed;					//to store a random seed

	cout<<"enter a random seed => ";//get a radom seed 
	cin>>seed; 

	srand(seed);					//re-set random seed

	for (int i=0; i< size; i++)
		cat[i]=rand()%1001;			//get random integers from 0 to 1000
}

//this recursive function finds the largest int in an array
int recursiveMaxInt(int cat[], int size)
{
	size--;	
	//problem with it, large  must be static, or passed to the function
	static int large=cat[size]; 

	if (size<= 0)					//case case
		return large; 
	else							//general case
	{	
		if (large <= cat[size-1])
		large = cat[size-1];

		recursiveMaxInt(cat, size ); //reduction
		 
	}

}

//this iterative function finds the largest int in an array
int iterativeMaxInt(int cat[], int size)
{
	assert(size >= 1);				//make sure the array is not empty
	
	int large = cat[0];				//local var to store the largest

	for (int i=0; i<size; i++)		//iterative loop to find the largest
	{
		if (cat[i] > large)
			large = cat[i]; 
	}
	return large; 
}
#endif
