
%create n random mxr matrices.  Perform 
%SVD
n=600;
m=100;
r=1000;
i=1;
a=-1000;
b=1000;
error=zeros(n,1);
residual=zeros(n,1);
while i<=n
    A=a+(b-a)*rand(m,r);    
    x=rand(r,1);
    b=A*x;
    xrec=A\b;
    error(i)=norm(xrec-x);
    residual(i)=norm(A*xrec-b);
    i=i+1;    
end
plot(residual);
    
    