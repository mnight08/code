#include<glut.h>

void init(void) 
{
	glClearColor (0.0, 0.0, 0.0, 0.0);
	glShadeModel (GL_FLAT);
}

void display()
{
	glClear (GL_COLOR_BUFFER_BIT);
	glColor3f (1.0, 1.0, 1.0);
	glLoadIdentity ();
	gluLookAt (0.0, 0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
	glScalef (1.0, 2.0, 1.0); /* modeling transformation */
	glutWireCube (1.0);
	glFlush ();
}
void reshape (int w, int h)
{
	glViewport (0, 0, (GLsizei) w, (GLsizei) h); 
	glMatrixMode (GL_PROJECTION); 
	glLoadIdentity ();
	glFrustum (-1.0, 1.0, -1.0, 1.0, 1.5, 20.0);
	glMatrixMode (GL_MODELVIEW);
}

void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		exit(0);
		break;
	case 'a':
		reshape(x=x+10,y=y+10);
		display();
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	
	switch(button)
	{
	case 0:

		x=0; y=0;
		break;
	}

}


int main(int argc, char** argv)
{
	// initialize GLUT
	glutInit(&argc, argv);
	/*This is where you tell glut if you want a single buffer or a double buffered window.
	GLUT_RGB tells what kind of color system you want. We could have chosen GLUT_RGBA too if we needed the alpha component of the color.*/
	glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);
	// These are the coordinates and size of the window that will be opened.
	glutInitWindowSize (1600, 900); 
	glutInitWindowPosition (0, 0);
	// Create the window now!
	glutCreateWindow (argv[0]);
	init ();
	/*Glut is an event based framework. You don't actually control anything. You tell GLUT where and when to call your functions to do your stuff. It's like "Don't call us...we'll call you :D". All you have to do is have the right signature for a function (the right parameters) and give GLUT the pointer to it. It will call it at the right time with the right arguments
	This is how you register a display function. Actualy this is how you register all functions...by passing a pointer to opengl saying: this is the function you need to call to render my scene. This goes for all other events like mouse, keyboard and reshape.*/
	glutDisplayFunc(display); 
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	// This call will give the control to glut. It will loop infinitly and call your functions when the events you registered with happen.
	glutMainLoop();
	return 0;
}