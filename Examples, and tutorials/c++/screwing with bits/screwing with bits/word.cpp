#include "word.h"

word::word(string& s)
{
	state=0;
	for(int i=0;i<s.size();i++)
	{
		switch(s[i])
		{

		case 'a':
		case 'A':
			state+=A;
			break;
		case 'b':
		case 'B':
			state+=B;
			break;

		case 'c':
		case 'C':
			state+=C;
			break;

		case 'd':
		case 'D':
			state+=D;
			break;

		case 'e':
		case 'E':
			state+=E;
			break;

		case 'f':
		case 'F':
			state+=F;
			break;
		case 'g':
		case 'G':
			state+=G;		
			break;

		case 'h':
		case 'H':
			state+=H;
			break;
		case 'i':
		case 'I':
			state+=I;
			break;
		case 'j':
		case 'J':
			state+=J;
			break;
		case 'k':
		case 'K':
			state+=K;
			break;

		case 'l':
		case 'L':
			state+=L;
			break;
		case 'm':
		case 'M':
			state+=M;
			break;
		case 'n':
		case 'N':
			state+=N;
			break;
		case 'o':
		case 'O':
			state+=O;
			break;
		case 'p':
		case 'P':
			state+=P;
			break;
		case 'q':
		case 'Q':
			state+=Q;
			break;
		case 'r':
		case 'R':
			state+=R;
			break;
		case 's':
		case 'S':
			state+=S;
			break;
		case 't':
		case 'T':
			state+=T;
			break;
		case 'u':
		case 'U':
			state+=U;
			break;
		case 'v':
		case 'V':
			state+=V;
			break;

		case 'w':
		case 'W':
			state+=W;

			break;

		case 'x':
		case 'X':
			state+=X;
			break;


		case 'y':
		case 'Y':
			state+=Y;
			break;

		case 'z':
		case 'Z':
			state+=Z;
			break;

		default: 
			this->state+=0;
			break;
		}
	}
	str=s;
}

word::~word(void)
{
}

//working with a base 4 right now, level counting starts at 0
unsigned int word::getstate(int &level)
{
	return (state<<(4*level))%16;
}