// Author:	Zhixiang Chen
// Class:	CSCI 1380.01, Summer I, 2008
// Lab 40:	More classes, a bank exmaple
// Date:	July 2, 2008
// Comment: The code here is meant to be revised.
//
//-----------------------------------------------------------------
// Lab Exercise 40: 
//
// This lab exercise is to more class exercise. We shall define a 
// bank account class along with necessary methods. We then write a 
// driver program to manage bank accounts for customers.
//
// The program is not complete. Your job to complete this progam for all options
// and add as many features as you can.
//
// Compile and run your program. When everything is fine,
// print your .cpp file and turn it to me or Sergio
// *****************************************************************


//i have to create a function to load previous bank accounts from a file.
//i have to add function to save the changes of this bank account
#include < iostream >
#include <fstream >
#include < string >
#include < cstring >
#include < cmath> 
#include <cstdlib>
#include <iomanip>
#include <vector>

#include "lab_40_head.h" 

using namespace std; 

int main( )
{
	//declare a vector of accounts
	vector<accountType> bank(0); 

	accountType acount;					//an account object
	transactionType tans;				//a transaction object	
	string fn, ln, id, pwdStr;			//vars for names, IDs, and passwords
	double amt;							//var to amount

	char option;						//switch option
	int i;								//loop vars
	bool found;							//for searching
	bool isid=true;						//check if id or name
	int position;						//for position of account in vector
	int size;
	do 
	{
		load(bank);						//load previous bank accounts if they exist
		showMenu();						//show menu

		cout << " Enter your option => "; 
		cin >> option; 

		switch(option)
		{
		case 'e':
			exit(0);
			break;

		case 'o'://open an account
			//get necessary info

			cout<<"Enter your first name => ";	
			cin>>fn; 
			cout<<"Enter your last name => "; 
			cin>>ln; 
			cout<<"Enter your account ID => "; 
			cin>>id; 
			cout<<"Enter your initial deposit => "; 
			cin>>amt; 
			cout<<"Enter your password => "; 
			cin>>pwdStr; 

			//add the account into the bank
			bank.push_back(accountType(fn, ln, id, amt, pwdStr)); 
			break; 

		case 'd'://make a deposit

			size=bank.size();
			if(size<=0)
			{
				cout<<"there are no bank accounts created yet\n";
				break;
			}

			position=(search(isid,size,bank));
			//test if bank account is open
			//might wanna change implimentation of this
			bank[position].activeorclosed(bank);
			if (position<0)
			{
				cout<<"your account does not exist\n";
				break;
			}
			cout<<"please enter your password=>";
			cin>>pwdStr;
			if(bank[position].checkPwd(pwdStr)==true)
			{
				cout<<"how much would you like to deposit??\n";
				cout<<"->";
				cin>>amt;
				bank[position].deposit(amt);


			}

			//get necessary info
			break; 

		case 'w'://make a withdraw
			size=bank.size();
			if(size<=0)
			{
				cout<<"there are no bank accounts created yet\n";
				break;
			}
			position=(search(isid,size,bank));
			if (position<0)
			{
				cout<<"your account does not exist ";
				break;
			}
			cout<<"please enter your password=>";
			cin>>pwdStr;
			if(bank[position].checkPwd(pwdStr)==true)
			{

				cout<<"how much would you like to withdraw??";
				cout<<"->";
				cin>>amt;
				bank[position].withdraw(amt);
			}
			//get necessary info
			break; 


		case 't'://make a transfer

			int positionto;
			size=bank.size();
			if(size<=0)
			{
				cout<<"there are no bank accounts created yet\n";
				break;
			}
			else if (size==1)
			{
				cout<<"there is no bank accounts to transfer to\n";
				break;
			}
			position=(search(isid,size,bank));
			cout<<"what account will you transfer to?\n";
			positionto=(search(isid,size,bank));
			if (position<0||positionto<0)
			{
				cout<<"one of the accounts does not exist )->";
				break;
			}
			cout<<"please enter your password=>";
			cin>>pwdStr;
			if(bank[position].checkPwd(pwdStr)==true)
			{

				cout<<"how much would you like to transfer??";
				cout<<"->";
				cin>>amt;
				bank[position].transfer(amt,bank[positionto]);
			}



			//get necessary info
			break; 

		case 'p'://print account report
			//get necessary info
			size=bank.size();
			if(size<=0)
			{
				cout<<"there are no bank accounts created yet\n";
				break;
			}
			position=(search(isid,size,bank));
			if (position<0)
			{
				cout<<"your account does not exist ";
				break;
			}
			cout<<"please enter your password=>";
			cin>>pwdStr;
			if(bank[position].checkPwd(pwdStr)==true)
				bank[position].print();
			break; 

		case 'c'://close an account
			//get necessary info
			size=bank.size();
			if(size<=0)
			{
				cout<<"there are no bank accounts created yet\n";
				break;
			}
			cout<<"Enter an account ID => "; 
			cin>>id; 
			//search for the account in the bank
			found = false; 
			for (i=0; i<size; i++)
			{
				if (bank[i].getID() == id)
				{
					found = true; 
					break; 
				}
			}
			if (found)						//found the account
			{
				cout<<"Enter your password => "; 
				cin>>pwdStr; 
				if(pwdStr == bank[i].getPwd())	//password verified
				{
					//bank[i].print(); 
					bank[i].close();
				}
				else
				{
					cout<<"Password is not right. Request is denied. " <<endl; 
				}
			}
			else
			{
				cout<<"We don't have an  account with ID " << id <<endl; 
			}

			break; 
		default:
			cout<<"invalid option\n"; 
		}

		save(bank);
	} while (toupper(option)!='E'); 

	//well done and exit
	return 0; 
}

