// *****************************************************************
// Author:	Zhixiang Chen
// Class:	CSCI 1380.01, Summer I, 2008
// Lab 40:	More classes, a bank example
// Date:	July 3, 2008
// Comment: The code here is meant to be revised.
// *****************************************************************

#include <iostream>
#include <cstring>
#include <string>
#include <vector> 

using namespace std; 

#ifndef		LAB_40_HEAD_H		
#define		LAB_40_HEAD_H

struct transactionType
{
	string type;							//"deposit" or "withdraw", or "transfer"
	double amount;							//amount in the transaction
};

//define studentClass class
class accountType 
{
private: 	
	string firstName,							//first name of customer 
		lastName;							//last name of customer
	string ID;									//account ID
	double balance;								//account balance
	string pwd;									//password for the account
	bool   activeOrClosed;						//status of the account
	vector<transactionType> activities;			//activity log of the account

public: 
	accountType();								//default constructor
	accountType(const string fn, const string ln, //another constructor
		const string id, const double bal,
		const string pwdStr); 
	void deposit(const double amount);			//method to make a deposit
	bool withdraw(const double amount);			//make a withdraw
	bool transfer(const double amount, accountType & dest); 
	string getName() {return firstName + " " + lastName;} //inline method to get the name
	string getID(){return ID;}					//inline method to get the ID
	string getPwd(){return pwd;}				//inline method to get the pwd
	double getBalance(){return balance;}		//inline method to get the balance
	bool checkPwd(const string str){return pwd == str;} //inline method to check password
	void print();								//print account report
	void save(ofstream &save);					//save changes to a file
	void open(const string fn, const string ln, //method to open an account
		const string id, const double bal,
		const string pwdStr); 
	void loadtranshistory(vector <transactionType> loadtrans, bool aOc);
	//originally had all those variables, but i think you only need to change one thing
	void close(/*const string fn, const string ln,//close the account
			   const string id, const string pwdStr*/); 	
	void  activeorclosed(vector <accountType> bank);
}; 

//default constructor
accountType:: accountType()
{
	firstName = "";								//first name of customer 
	lastName = "";								//last name of customer
	ID = -1;									//account ID
	balance = 0; 								//account balance
	pwd = "";									//password for the account
	activeOrClosed = true; 						//status of the account
	activities.clear(); 						//clear the activity vector
}

//another constructor
accountType:: accountType(const string fn, const string ln, 
						  const string id, const double bal,
						  const string pwdStr)
{
	open(fn, ln, id, bal, pwdStr);				//call open method to open the account
}

//method to make a deposit
void accountType:: deposit(const double amount)
{
	transactionType tmp;						//var to log the transaction type

	balance += amount;							//make the deposit

	tmp.type = "deposit";								//log the transaction
	tmp.amount = amount; 
	activities.push_back(tmp); 
}

//make a withdraw
bool accountType:: withdraw(const double amount)
{
	transactionType tmp;						//var to log the transaction type

	if (balance < amount)
	{
		cout<<" Your balance is "<<setprecision(2)<<fixed<<showpoint<<balance<<endl; 
		cout<<" You are trying to overdraw.  Request is denied. Bybye." <<endl; 
		return false;							//unsuccessful withdraw
	}
	balance -= amount;							//make the withdraw

	tmp.type = "withdraw";								//log the transaction
	tmp.amount = amount; 
	activities.push_back(tmp); 

	return true;								//successful withdraw
}


bool accountType::transfer(const double amount, accountType & dest)
{
	transactionType tmp;						//var to log the transaction type

	if (balance < amount)						//unsuccessful transfer
	{
		cout<<" Your balance is "<<setprecision(2)<<fixed<<showpoint<<balance<<endl; 
		cout<<" You do not have enough fund to transfer.  Request is denied. Bybye." <<endl; 
		return false;							
	}

	balance -= amount;							//make transfer
	dest.deposit(amount); 

	tmp.type = "transfer";								//log the transaction
	tmp.amount = amount; 
	activities.push_back(tmp); 

	return true;								//successful withdraw
}

//print account report
void accountType::print()
{
	cout<<endl<<" Benny's World Bank Account Report"<<endl<<endl; 
	cout<<"Customer Name: " <<setw(16)<<right<< firstName + " " + lastName <<endl; 
	cout<<"Account ID   : " <<setw(16)<<right<< ID<<endl; 
	cout<<"Balance      : " <<setw(16)<<right<< balance<<endl; 
	cout<<"Status       : " <<setw(16)<<right<<  (activeOrClosed? "active" : "closed") <<endl;
	cout<<"Transaction Records ...... "<<endl; 
	cout<<"Total Transactions is " <<activities.size()<<endl; 
	for (int i=0; i<activities.size(); i++)
	{
		cout<<"Transaction " <<i <<" is a " <<activities[i].type <<" of amount " <<activities[i].amount<<endl; 
	}
}
//save account info to file
void accountType::save(ofstream& save)
{

	save<<firstName<<" "<<lastName<<" "<<ID<<" "//output first, last name, and id
		<<balance<<" "<<pwd<<" "<<activeOrClosed<<endl;	//output balance, and whether active or closed
	save<<activities.size()<<endl;				//output total number of transactions
	for (int i=0;i<activities.size();i++)
	{
		save<<i<<" "<<activities[i].type<<" "<<activities[i].amount<<endl;
	}
	save<<"\n\n\n\n\n";
}

//method to open an account
void accountType::open(const string fn, const string ln, 
					   const string id, const double bal,
					   const string pwdStr)
{
	firstName = fn; 
	lastName = ln; 
	ID = id; 
	balance = bal; 
	pwd = pwdStr; 
	activeOrClosed = true; 
	activities.clear(); 
}

void accountType::loadtranshistory(vector <transactionType> loadtrans, bool aOc)
{
	//transactionType temp;
	for (int i =0;i<loadtrans.size();i++)
	{
		//temp.amount=loadtrans[i].amount;
		//temp.type=loadtrans[i].type;
		activities.push_back(loadtrans[i]/*temp*/);
	}
	activeOrClosed=aOc;

}

//close the account
void accountType:: close(/*const string fn, const string ln,
						 const string id, const string pwdStr)*/)
{
	//was here origionaly, not sure if its needed
	//if (firstName != fn || lastName != ln || ID != id || pwd != pwdStr)
	//{
	//	cout<<" Information is right. Request is denied. Byebye."<<endl;
	//	return; 
	//}
	activeOrClosed = false; 
}

//method to load all previously saved bank accounts
void load(vector <accountType> &bank)
{
	string fn, ln, id;
	string trash="+";
	double bal;
	string pwdstr;
	bool aOc;
	int size;

	bank.erase(bank.begin(),bank.end());

	ifstream load;
	load.clear();
	load.open("savedaccounts.txt");
	if(!load.is_open()||load.peek()==char(32))
	{
		cout<<"file not found, or failed to open\none will be created on exit\n";
		return;
	}
	load.seekg(0,ios::beg);
	for (int i=0;true;i++)
	{

		load>>fn>>ln>>id
			>>bal>>pwdstr>>aOc
			>>size;
		if(fn[0]=='!')
			break;
		bank.push_back(accountType(fn,ln,id,bal,pwdstr));

		vector <transactionType> activ(size);
		for (int j=0;j<size;j++)
		{
			//load.ignore();	//ignore transaction number
			load>>trash;
			load>>activ[j].type;
			load>>activ[j].amount;
		}
		//for(int j=0;j<activ.size();j++)
		//{
			bank[i].loadtranshistory(activ,aOc);
		//}
		//load>>trash;

	}
	load.close();
}

//saves bank accounts
void save(vector <accountType> bank)
{
	ofstream save;
	save.open("savedaccounts.txt");
	for (int i=0;i<bank.size();i++)
	{	
		bank[i].save(save);
	}
	save<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
	save.close();
}
//show user menu
void showMenu()
{
	cout<<" Enter 'o' to open an account. "<<endl
		<<" Enter 'd' to make a deposit. "<<endl
		<<" Enter 'w' to make a withdraw. "<<endl
		<<" Enter 't' to make a transfer. "<<endl
		<<" Enter 'p' to print an account report "<<endl
		<<" Enter 'c' to close an accont."<<endl
		<<" Enter 'e' to exit"<<endl; 
}

int search(bool isid, const int size,vector <accountType> csci1380)
{	
	int position=0;
	string temp;
	string entry;	

	cout<<"enter an id, or a first and last name please->";

	cin>>entry;


	//test to see if there is still input
	if(entry[0]>64)
	{
		isid=false;
		cin>>temp;
		entry=entry+temp+"\0";
	}


	if (isid)
	{

		for (int i=0;i<size;i++)
		{
			position=i;
			temp=csci1380[i].getID();
			if (temp==entry)
				return position;
		}
	}

	//could be seperate function
	else
	{


		//i need to get rid of the spaces when loading name into temp
		for (int i=0;i<size;i++)
		{
			position=i;
			temp=csci1380[i].getName();

			int length=temp.length();

			//variable for the loop that finds out number of spaces
			int move=0, start=-1;


			//this is a loop count number of spaces, and where they are
			for (int pos=0;pos<length;pos++)
			{
				//this will be so i know how many spaces are in a row

				//this test if there is no more characters
				if (temp[pos]==0)
					break;
				else if(temp[pos]==32)
				{
					move++;
					if(move==1)
						start=pos;
				}


			}


			//this moves the characters back, replacing spaces
			for (int pos=(start+move);pos< length;pos++, start++)
			{
				temp[start]=temp[pos];
			}

			//gets rid of excess characters
			temp.erase(start);

			if (temp==entry)
				return position;


		}
	}

	return -1;

}


void accountType::activeorclosed(vector <accountType> bank)
{
	//prototype for main function
	int main();
	if (activeOrClosed==true)
		return;
	if (activeOrClosed==false)
	{
	cout<<"the account is closed"<<endl;
	//runs main function
	main();
	}
	
}


#endif
