//-----------------------------------------------------------------
// Author:	alex martinez
// Class:	CSCI/CMPE 2380.01, Fall, 2008
// Lab 8:	Inheritance - understanding member status
// Date:	4/17/09
// Comment: The code here is meant to be revised.
//
//-----------------------------------------------------------------
// Lab Exercise 8: 
//
// This lab exercise is practice class inheritance and to understand
// member status under inheritance
//
// Compile and run your program. When everything is fine,
// print your .cpp and .h files and turn it to me.
// *****************************************************************

#include "headers.h"

using namespace std; 

int main()
{

	/************************************************************************
	Part A: 
	(1) Create local blocks to see the creation and destruction 
	of class objects.
	(2) You shall check the output to make sure you understand 
	why it is so
	(3) Pay close attention to member status 
	(4) Pay close attention to the chain reaction of constructor calling
	the chain reaction of destructor calling
	************************************************************************/

	//a local block to see A's objects
	{
		A cat;				//create cat
		cat.printA(); 
	}						//cat dies when out of this block
	cout<<"A dies here********"<<endl<<endl; 

	//a local block to see B's objects
	{
		B dog;				//create dog
		dog.printB(); 
	}						//dog dies at this point
	cout<<"B dies here********"<<endl<<endl; 

	//another local block to see B's objects
	{
		B dog("I like ", "C++ ", "very much!");				//create dog
		dog.printB(); 
	}						//dog dies at this point
	cout<<"second B dies here********"<<endl<<endl; 

	//a local block to see C's object
	{
		C rat; 
		rat.printC(); 
	}						//rat dies at this point
	cout<<"C dies here********"<<endl<<endl; 

	//anotrher local block to see C's object
	{
		C rat("They don't ", "like ", "C++ at all!"); 
		rat.printC(); 
	}						//rat dies at this point
	cout<<"second C dies here********"<<endl<<endl; 


	/***********************************************************************************************
	Part B: For you to complete
	(1) Re-do part A by changing B and C inheritance to public and protected respectively. 
	(2) Re-do part A by changing B and C inheritance to public and private respectively. 
	(3) Re-do part A by changing B and C inheritance to protected and public respectively.
	(4) Re-do part A by changing B and C inheritance to protected and protected respectively. 
	(5) Re-do part A by changing B and C inheritance to protected and private respectively.
	(6) Re-do part A by changing B and C inheritance to private and public respectively. 
	(7) Re-do part A by changing B and C inheritance to private and protected respectively.
	(8) Re-do part A by changing B and C inheritance to private and private respectively.

	How to submit this lab?
	Just play with those 8 options by changing the given code and compile and run your changed code. 
	Print out the orginial version of the code, and written for each option what happens. 
	Turn in the orginal version plus the written observations for those 8 options. 
	***********************************************************************************************/



	//complete the program 
	return 0; 
}
