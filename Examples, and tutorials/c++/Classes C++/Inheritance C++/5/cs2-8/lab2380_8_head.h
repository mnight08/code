// *****************************************************************
// Lab 8:	Inheriance - understand member status, chain reactions 
//			of constructor / destructor calling
// *****************************************************************




using namespace std; 

#ifndef		LAB2380_8_HEAD_H		
#define		LAB2380_8_HEAD_H

#include "headers.h"
//A simple base class
class A
{
public: 
	int a;
	A();				//default constructor
	A(int, int, int);	//another constructor
	~A();				//destructor
	void printA();		//printA
protected: 
	int b;				
private:					
	int c; 
}; 

//A's deafult constructor
A::A()
{
	cout<<"Inside A's constructor" <<endl; 
	a=10; 
	b=15; 
	c=20; 
}

//A's another constructor
A::A(int u, int v, int w)
{
	cout<<"Inside A's constructor" <<endl; 
	a=u; 
	b=v; 
	c=w; 
}
//A's desctructor
A::~A()
{
	cout<<"Inside A's destructor" <<endl; 
}

//print in A
void A::printA()
{
	cout<<"printA prints ..." <<endl; 
	cout<< "a is " <<a <<" b is " <<b <<" c is " <<c <<endl;  
}

//derive B from A
class B: public A 
{
public: 
	string e;
	B();						//default constructor
	B(string, string, string);	//another constrctor
	~B();						//destructor
	void printB();				//printB
protected: 
	string f;				
private:					
	string g; 
};

//B's default constructor
B::B()
{
	cout<<"Inside B's constructor" <<endl; 
	e="We "; 
	f="Like "; 
	g="C++ inheritance!"; 
}

//B's another constructor
B::B(string u, string v, string w):A(111,222,333)
{
	cout<<"Inside B's constructor" <<endl; 
	e=u; 
	f=v; 
	g=w; 
}

//B's desctructor
B::~B()
{
	cout<<"Inside B's destructor" <<endl; 
}	

//printB
void B::printB()
{
	cout<<"printB prints ......... " <<endl; 
	cout<<"a is " <<a <<" b is " <<b <<endl; 
	cout<< e+f+g <<endl;  
}

//dedrive C from B
class C: public B 
{
public: 
	string x;
	C();						//default constructor
	C(string, string, string);	//another constructor
	~C();						//destructor
	void printC();				//printB
protected: 
	string y;				
private:					
	string z; 
};

//C's default constructor
C::C()
{
	cout<<"Inside C's constructor" <<endl; 
	x="Hello, "; 
	y="Tom "; 
	z="Jerry!"; 
}

//C's another constructor
C::C(string u, string v, string w):B("I am ", "very ", "confused!")
{
	cout<<"Inside C's constructor" <<endl; 
	x=u; 
	y=v; 
	z=w; 
}
//C's desctructor
C::~C()
{
	cout<<"Inside C's destructor" <<endl; 
}	

//printC
void C::printC()
{
	cout<<"printC prints ......... " <<endl; 
	cout<<"a is " <<a <<" b is " <<b <<endl; 
	cout<< e+f <<endl;
	cout<<x+y+z <<endl; 
}
#endif 
