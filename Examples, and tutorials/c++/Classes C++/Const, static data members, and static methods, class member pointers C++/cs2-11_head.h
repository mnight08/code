#ifndef		LAB2380_11_HEAD_H		
#define		LAB2380_11_HEAD_H
#include "headers.h"
using namespace std; 

/*************************************************************
Part A: practice const data members
*************************************************************/
class  A
{
private: 
	const int size;			//a const data member
	char *cat; 
public: 
	A();					//default constructor
	A(const int n);			//constrcutor with parameter
	void print();			//print A
	~A(){delete[] cat;};	//destructor
}; 

A::A():size(100)			//head initialization of size
{
	cat = new char[size]; 
	strcpy(cat, "I see a head initialization."); 
}

A::A(const int n): size(n)
{
	cat = new char[n]; 
	if (n>50)
		strcpy(cat, "Meow, meow, another head initialization.");
	else
		cat[0]='\0';
}

void A::print()
{
	cout<<"size is => "<< size<<endl; 
	cout<<"cat is => " <<cat<<endl; 
}


/*************************************************************
Par B: Practice static data member and static methods
Note: A static method cannot access any non-static data
members.
*************************************************************/
class  B
{
private: 
	static int lifeCount;	//a static data member	
	static int deathCount;	//a static data member	
	const int size;			//a static data member
	char *cat; 
public: 
	B();					//default constructor
	B(const int n);			//constrcutor with parameter
	static void resetcount();
	static int returncount(){return lifeCount;}
	void print();			//print A
	static void printStatic();//a static method
	~B();					//destructor
}; 

//this is a key part: A static data member much declared out the class
//the purpose for this is to gain memory allocation
int B:: lifeCount=0;				
int B:: deathCount=0; 

void B::resetcount()
{
	lifeCount=0;
	deathCount=0;
}
B::B():size(100)			//head initialization of size
{
	lifeCount++;			//increase by one
	cat = new char[size]; 
	strcpy(cat, "I see a head initialization."); 
}

B::B(const int n): size(n)
{
	lifeCount++;			//increase by one
	cat = new char[n]; 
	if (n>50)
		strcpy(cat, "Meow, meow, another head initialization.");
	else
		cat[0]='\0';
}

void B::print()
{
	cout<<"size is => "<< size<<endl; 
	cout<<"cat is => " <<cat<<endl; 
}

B::~B()
{
	deathCount++;				//increase by one
	printStatic();			//call static method
	delete[] cat; 
}

void B::printStatic()
{
	cout<<lifeCount<<"  many class B objects have been created up to this point."<<endl; 
	cout<<deathCount<<"  many class B objects have died up to this point."<<endl; 
}

/*************************************************************
Par C: Pointers to class members (data members or methods).
*************************************************************/
class  C
{
public:  
	string cat; 
	string dog;  
	C();					//default constructor
	C(const string &, const string &);//constrcutor with parameter
	void printOne(int n);	//print A
	void printTwo(int n);	//a static method
	~C();					//destructor
}; 

C::C()
{
	cat = "Tom "; 
	dog ="Jerry ";
}

C::C(const string & first, const string & last)
{
	cat = first; 
	dog = last; 
}

void C::printOne(int n )
{
	/*gets annoying
	for (int i=0; i<n; i++)
	cout<<"\a"; */
	cout<<"Okay, "<<  cat<<" likes cats." <<endl; 

}

C::~C()
{
}

void C::printTwo(int n)
{
	/*gets annoying
	for (int i=0; i<n; i++)
	cout<<"\a"; */
	cout<<"Okay, "<< dog<<" likes dogs." <<endl; 
}

/************************************************************************/
class  C2
{	
private: 
	string cat; 
	string dog; 
public:  

	C2();					//default constructor
	C2(const string &, const string &);//constrcutor with parameter
	void printOne(int n);	//print A
	void printTwo(int n);	//a static method
	string* catlocation();	//this probably shouldnt work
	string* doglocation();	//this probably shouldnt work
	~C2();					//destructor
}; 

//kinda strange that you can return the memory location, but not pass it by 
string* C2::catlocation()
{
	///cout<<"\a\a\a\a\a\a\a\a\athis is not the smartest thing to do";
	return &cat;
}


string* C2::doglocation()
{
	//cout<<"\a\a\a\a\a\a\a\a\athis is not the smartest thing to do";
	return &dog;

}

C2::C2()
{
	cat = "Tom "; 
	dog ="Jerry ";
}

C2::C2(const string & first, const string & last)
{
	cat = first; 
	dog = last; 
}

void C2::printOne(int n )
{
	/*for (int i=0; i<n; i++)
	cout<<"\a"; *///this was getting very anoying
	cout<<"Okay, "<<  cat<<" likes cats." <<endl; 

}

C2::~C2()
{
}

void C2::printTwo(int n)
{
	/*for (int i=0; i<n; i++)
	cout<<"\a"; 
	*///this was getting very anoying
	cout<<"Okay, "<< dog<<" likes dogs." <<endl; 
}

#endif
