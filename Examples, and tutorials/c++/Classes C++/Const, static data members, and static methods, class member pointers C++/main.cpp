//-----------------------------------------------------------------
// Author:	alex martinez
// Class:	CSCI/CMPE 2380.01, Fall, 2008
// Lab 11:	Const, static data members, and static methods
//			class member pointers
// Date:	September 24, 2008
// Comment: The code here is meant to be revised.
//
//-----------------------------------------------------------------
// Lab Exercise 11: 
//
// This lab exercise is practice const, static data members, and static methods
//			class member pointers
//
// Compile and run your program. When everything is fine,
// print your .cpp and .h files and turn it to me.
// *****************************************************************

#include "headers.h"
using namespace std; 


int main()
{	

	/******************************************************
	Part A: practice a const data member
	******************************************************/
	cout<<"Part A: "<<endl;
	A rat; 
	A bat(200); 
	rat.print(); 
	bat.print(); 

	/******************************************************
	Part B: If you change the head initialization to body 
	initialization, then compile your program to see 
	what will happen.
	******************************************************/
	//your code is here
	//the program wont run

	/******************************************************
	Part C: Test static data memebrs and methods.
	******************************************************/
	cout<<endl<<"Part C: "<<endl;
	int i, j;  
	for (i=0; i<3; i++)
	{
		for (j=0; j<5; j++)
		{
			B tom; 
		}
		cout<<"i = "<< i<<endl<<endl; 

		B jerry(100); 
	}

	/***********************************************************************
	Part D: Re-do part C. This time, you shall use static methods
	Using static member(s) to control that the number of class B objects 
	created in the program cannot be greater than 10. 
	If the number is 10, then no more class B objects can be created.  
	***********************************************************************/
	//Your code is here. 
	{

		B::resetcount();

		cout<<endl<<"Part D: "<<endl;
		int i, j;  
		for (i=0; i<3; i++)
		{
			for (j=0; j<5; j++)
			{		
				if(B::returncount()>9)
					break;
				B tom; 

			}
			cout<<"i = "<< i<<endl<<endl; 
			if (B::returncount()>9)
				break;
			B jerry(100); 
		}
	}

	/******************************************************
	Part E: Test pointers to class members
	******************************************************/

	cout<<endl<<endl<<"Part E ...." <<endl; 

	int option; 
	C hat; 
	C fat("Joe", "Blum"); 

	string C::*ptr;				//ptr is a pointer to string type data member in C

	void (C::*ptrTwo)(int);		//ptrTwo is a pointer to a method in C 
	//with value-returnning type void and parameter list (int)

	ptr = & C::cat;				//ptr points to member cat in C

	cout<<" cat in hat is " << hat.*ptr <<endl; //access cat in hat

	ptr = & C::dog;				//ptr points to member dog in C

	cout<<" dog in bat is " << fat.*ptr <<endl; //access dog in hat

	cout<<"Enter 1 to call print() in class C \n"
		<<"Enter 2 to call printTwo() in class C\n"
		<<"Give me your choice => "; 
	cin >> option; 

	if (option == 1)
	{
		ptrTwo = & C:: printOne;//ptrTwo points to method printOne
		(fat.*ptrTwo)(20);			//call printOne in bat
	}
	else 
	{
		ptrTwo = & C:: printTwo;//ptrTwo points to method printOne
		(fat.*ptrTwo)(30);			//call printTwo in bat
	}

	/*******************************************************************
	Part F: This part is for you. 
	Re-define class C so that cat and dog are both private. 
	Re-do Part F. Feel free to add more members to class C.
	*******************************************************************/
	/*this is a cheat, in that i made a function to pass the location of cat, and dog, 
	this completely defeats the purpose of making them private, if they are accesible like that
	*/
	{
		cout<<endl<<endl<<"Part F ...." <<endl; 

		int option; 
		C2 hat; 
		C2 fat("Joe", "Blum"); 

		//i dont think this should work, seems like a flaw 
		//because the member is private, you can only access it indirectly
		string *ptr=NULL;				//ptr is a pointer to string type data member in C



		void (C2::*ptrtwo)(int n);

		ptr=hat.catlocation();
		cout<<" cat in hat is " << *ptr <<endl; //access cat in hat

		//C2::doglocation(ptr);				//ptr points to member dog in C

		ptr=fat.doglocation();				//but apparently you cant pass the memory location by refference
		//fails on next line at fat.*ptr,  ptr is not taking a value
		cout<<" dog in fat is " << *ptr<<endl; //access dog in fat

		cout<<"Enter 1 to call print() in class C \n"
			<<"Enter 2 to call printTwo() in class C\n"
			<<"Give me your choice => "; 
		cin >> option; 


		if (option == 1)
		{
			ptrtwo = & C2:: printOne;//ptrTwo points to method printOne
			(fat.*ptrtwo)(20);			//call printOne in bat
		}
		else 
		{
			ptrtwo = & C2:: printTwo;//ptrTwo points to method printOne
			(fat.*ptrtwo)(30);			//call printTwo in bat
		}
	}
	return 0; 
}


