#include <iostream>
#include <cstring>
#include <string>
#include <vector> 

using namespace std; 

#ifndef		LAB2380_5_HEAD_H		
#define		LAB2380_5_HEAD_H

/*************************************************************
  * declarae a nameType class
  ************************************************************/
class nameType
{	  
	friend ostream& operator<<(ostream&, const nameType&);
	friend istream& operator>>(istream&, nameType&);
	friend void printName(const nameType &);  

public:
	bool operator==(const nameType&) const;		// == overloading
	bool operator<(const nameType&) const;		// > overloading
	bool operator>(const nameType&) const;		// < overloading
	const nameType & operator=(const nameType&);// = overloading
	void setName(char *, char *);				// set the full name
	void getName(char *, char *);				// get the full name
	nameType();									// default constructor
	nameType(char *, char *);					// a constructor with parameters
	nameType(nameType&);						// copy constructor
	~nameType();								//destructor

private:
	char *firstName;							// store the first name
	char *lastName;								// store the last name
};


//overload operator<<, which is not a member of nameType, but is a friend to it.
ostream& operator<<(ostream& os, const nameType& aName)
{
	os.setf(ios::left, ios::adjustfield);

	string fn = aName.firstName; 
	string ln = aName.lastName;
	os<<fn + "  " + ln <<endl; 

	return os;
}


//overload operator>>, which is not a member of nameType, but is a friend to it.
istream& operator>>(istream& is, nameType& aName)
{
	cout<<"Please enter the name (first last): ";
    is>>aName.firstName;
	is>>aName.lastName;
	return is;
}


//define printName, which is not a member of nameType, but is a friend to it.
void printName(const nameType & aName)
{
	//use C string functions
	char *str = new char[strlen(aName.firstName) + strlen(aName.lastName) + 3]; 
	strcpy (str, aName.firstName); 
	strcat(str, "  "); 
	strcat(str, aName.lastName); 
	cout<<str<<endl; 
	delete[] str; 
}

//overload == operator
bool nameType::operator==(const nameType& aName) const
{
	return(strcmp(firstName, aName.firstName) == 0
		&& strcmp(lastName, aName.lastName) == 0);
}



//overload < operator
bool nameType::operator<(const nameType& aName) const
{
	if (strcmp(lastName, aName.lastName)<0)
		return true; 
	else 
		return strcmp(lastName, aName.lastName)==0 
					  && strcmp(firstName, aName.firstName)<0; 
}


//overload > operator
bool nameType::operator>(const nameType& aName) const
{
	if (strcmp(lastName, aName.lastName)>0)
		return true; 
	else 
		return strcmp(lastName, aName.lastName)==0 
					  && strcmp(firstName, aName.firstName)>0; 
}


//copy constructor
const nameType & nameType::operator=(const nameType & aName)
{
	if (this != & aName)
	{
		delete[] firstName; 
		delete[] lastName; 
		firstName = new char[strlen(aName.firstName)+1]; 
		lastName = new char[strlen(aName.lastName)+1]; 
		strcpy(firstName, aName.firstName);	// set the first name
		strcpy(lastName, aName.lastName);	// set the last name
	}
	return *this; 
}



//setName
void nameType::setName(char *first, char *last)
{
	delete[] firstName; 
	delete[] lastName; 
	firstName = new char[strlen(first)+1]; 
	lastName = new char[strlen(last)+1]; 
	strcpy(firstName, first);	// set the first name
	strcpy(lastName, last);		// set the last name
}

//getName
void nameType::getName(char *first, char *last)
{
	strcpy(first, firstName);	// get the first name
	strcpy(last, lastName);		// get the last name
}


//default constructor
nameType::nameType()			// default constructor
{
	firstName = new char[100];	// set the first name to null string
	lastName = new char[100];	// set the second name to null string
}


//another constructor
nameType::nameType(char *first, char *last) // a constructor 
{
	firstName = new char[strlen(first)+1]; 
	lastName = new char[strlen(last)+1]; 
	strcpy(firstName, first);	// set the first name
	strcpy(lastName, last);		// set the last name
}


//copy constructor
nameType::nameType(nameType & aName)
{
	if (this != & aName)
	{
		firstName = new char[strlen(aName.firstName)+1]; 
		lastName = new char[strlen(aName.lastName)+1]; 
		strcpy(firstName, aName.firstName);		// set the first name
		strcpy(lastName, aName.lastName);		// set the last name
	}
}



// desconstructor
nameType::~nameType( )
{
	delete[] firstName; 
	delete[] lastName; 
}


#endif 
