//-----------------------------------------------------------------
// Author:	alex martinez
// Class:	CSCI/CMPE 2380.01, Fall, 2008
// Lab 3:	Simple excercises about classes
// Date:	2/19/09
// Comment: The code here is meant to be revised.
//
//-----------------------------------------------------------------
// Lab Exercise 3: 
//
// This lab exercise is practice class declaration and implementation
//
// Compile and run your program. When everything is fine,
// print your .cpp and .h files and turn it to me.
// *****************************************************************

#include < iostream >
#include <fstream >
#include < string >
#include < cstring >
#include < cmath> 
#include <cstdlib>
#include <iomanip>

#include "lab2380_5_head.h"
#include "lab_5b_head.h"

using namespace std; 

int main()
{
	/************************************************************************
	* Part One: class object creation, class pointers, member calling, etc.
	************************************************************************/

	//create nameType objects
	nameType cat;							//use default constructor
	nameType dog("Tom", "Jerry");			//use another constructor
	nameType rat(dog);						//use copy constructor

	cin>>cat;								//use overloaded >> to get cat
	cout<<"cat's name is => " <<cat<<endl;	//use overloaded << to output cat

	cout<<"dog's name is => "; 
	printName(dog);							//use friend function printName to print dog
	cout<<"rat's name is => "; 
	printName(rat);							//use friend function printName to print rat
	rat = cat;								//use overloaded = 
	cout<<"rat's name now is => " <<rat<<endl;	
	//use overloaded << to output cat
	//play with pointer
	nameType * ptr = new nameType;			//ptr is nameType pointer
	cin>>*ptr; 
	cout<<"the name of *ptr is => " <<*ptr; 
	ptr->setName("Blabla", "Wow"); 
	cout<<"the name of *ptr is now => "; 
	printName(*ptr); 


	/*************************************************************************************
	* Part Two: For you to complete
	(1) Declare a bookType class with at least the following data members: 
	bookTile, ISBN, Author, etc.
	(2) Declare a similar set of methods like those in nameType
	(3) Add the declarations and implementation in the head file "lab2380_5_b_head.h"
	(4) Include this header file in "lan2380_5.cpp and carry out similar 
	tests as in Part One.
	************************************************************************************/


	// i need to move the second class to other header file, didnt read that.
	bookType novel;
	bookType shortstory("w","ts.","ol");
	bookType three(shortstory);

	cin>>novel;
	cout<<"the novel is "<<novel<<endl;

	cout<<"shortstories is => "; 
	printBook(shortstory);						//use friend function printName to print dog
	cout<<"three is => "; 
	printBook(three);							//use friend function printName to print rat
	novel = shortstory;							//use overloaded = 
	cout<<"novel is now => " <<novel<<endl;		//use overloaded << to output cat

	//play with pointer
	bookType * ptrb = new bookType;				//ptr is bookType pointer
	cin>>*ptrb; 
	cout<<"the book of *ptr is => " <<*ptrb; 
	ptrb->setbook("Blabla","giberish", "Wow"); 
	cout<<"the name of *ptr is now => "; 
	printBook(*ptrb); 


	//complete the program 
	return 0; 
}
