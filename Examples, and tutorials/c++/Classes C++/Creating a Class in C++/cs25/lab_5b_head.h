#include "lab2380_5_head.h"
#ifndef lab_5b_head_h
class bookType
{
	friend ostream& operator<<(ostream&, const bookType&);
	friend istream& operator>>(istream&, bookType&);
	friend void printBook(const bookType &);  

public:
	bool operator==(const bookType&) const;
	bool operator <(const bookType&) const;
	bool operator >(const bookType&) const;
	const bookType &operator=(const bookType&);
	void getbook(char *, char *, char *);
	void setbook(char *, char *, char *);
	bookType(bookType&);					//copy constructor
	bookType(char*  ,char*  , char*  );		//constructor with parameters
	bookType();								//default constructor
	~bookType();
private:
	char * Title;
	char * Isbn;
	char * Author;
};




//same as previousm except for different class
ostream& operator <<(ostream& os, const bookType& book)
{
	os.setf(ios::left, ios::adjustfield);

	string title=book.Title;
	string isbn=book.Isbn;
	string author=book.Author;

	os<<title + " " + isbn + " " + author<<endl;
	return os;
}

//overload >>
istream& operator>>(istream& is, bookType& book)
{
	cout<<"please enter the book (title isbn author): ";
	is>>book.Title;
	is>>book.Isbn;
	is>>book.Author;
	return is;
}

//print contents of book object
void printBook(const bookType&book)
{

	//if i counted spaces wrong, this will probably have errors
	char *str=new char[strlen(book.Title)+strlen(book.Isbn)+strlen(book.Author)+6];
	strcpy(str, book.Title);
	strcat(str, "  ");
	strcat(str, book.Isbn);
	strcat(str, "  ");
	strcat(str, book.Author);
	//this line will make an extra line appear even when there is no object
	//it looks cleaner with it though
	cout<<str<<endl;
	delete[] str;

}

//overload == test
bool bookType::operator ==(const bookType& book) const
{
	return(strcmp(Title, book.Title)==0&&
			strcmp(Isbn, book.Isbn)==0&&
			strcmp(Author, book.Author)==0);
}

//overload < operator
bool bookType::operator <(const bookType& book) const
{
	if (strcmp(Title, book.Title)<0)
		return true;
	else if(strcmp(Title, book.Title)==0&&
		strcmp(Isbn, book.Isbn)<0)
		return true;
	else
		return strcmp(Title, book.Title)==0&&
		strcmp(Isbn, book.Isbn)==0&&strcmp(Author, book.Author)<0;
}

//overload > operator
bool bookType::operator >(const bookType& book) const
{
	if (strcmp(Title, book.Title)>0)
		return true;
	else if(strcmp(Title, book.Title)==0&&
		strcmp(Isbn, book.Isbn)>0)
		return true;
	else
		return strcmp(Title, book.Title)==0&&
		strcmp(Isbn, book.Isbn)==0&&strcmp(Author, book.Author)>0;
}

//overload = operator
const bookType &bookType::operator=(const bookType& book)
{
	if (this != &book)
	{
		delete[] Title;
		delete[] Isbn;
		delete[] Author;
		Title=new char [strlen(book.Title)+1];
		Isbn= new char [strlen(book.Isbn)+1];
		Author=new char [strlen(book.Author)+1];
		strcpy(Title, book.Title);
		strcpy(Isbn, book.Isbn);
		strcpy(Author, book.Author);
	}
	return *this;

}

//set values of book
void bookType::setbook(char* title, char*isbn, char*author)
{
	delete[] Title;
	delete[] Isbn;
	delete[] Author;
	Title=new char [strlen(title)+1];
	Isbn=new char [strlen(isbn)+1];
	Author=new char [strlen(author)+1];
	strcpy(Title, title);
	strcpy(Isbn, isbn);
	strcpy(Author, author);
}

//get values book has
void bookType::getbook(char *title, char *isbn, char *author)
{
	strcpy(title, Title);
	strcpy(isbn, Isbn);
	strcpy(author, author);

}

//defaul constructor
bookType::bookType()
{
	Title=new char[100];
	Isbn=new char[100];
	Author=new char[100];
}

//constructor with parameters
bookType::bookType(char *title, char*isbn, char*author)
{
	Title=new char [strlen(title)+1];
	Isbn=new char [strlen(isbn)+1];
	Author=new char[strlen(author)+1];
	strcpy (Title, title);
	strcpy (Isbn, isbn);
	strcpy (Author, author);
}

//copy constructor
bookType::bookType(bookType& book)
{
	if (this != &book)
	{
		Title=new char [strlen(book.Title)+1];
		Isbn= new char [strlen(book.Isbn)+1];
		Author=new char [strlen(book.Author)+1];
		strcpy(Title, book.Title);
		strcpy(Isbn, book.Isbn);
		strcpy(Author, book.Author);
	}
}

//destructor
bookType::~bookType()
{
	delete[] Title;
	delete[] Isbn;
	delete[] Author;
}
#endif