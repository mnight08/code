//-----------------------------------------------------------------
// Author:	alex martinez
// Class:	CSCI/CMPE 2380.01, spring, 2009
// Lab 6:	Virtual methods vs. polymorphism
// Date:	2/19/2009
// Comment: The code here is meant to be revised.
//
//-----------------------------------------------------------------
// Lab Exercise 6: 
//
// This lab exercise is practice virtual methods vs. polymorphism
//
// Compile and run your program. When everything is fine,
// print your .cpp and .h files and turn it to me.
// *****************************************************************

#include "headers.h" 

using namespace std; 

int main()
{	
	/************************************************************
	Part One: Watch the output. Try to understand: 
	No virtual methods, no polymorphism.
	************************************************************/
	One dog; 
	Two cat; 
	Three rat;

	One * objPtr[3];					//base class pointer
	objPtr[0]=&dog;						//normal binding to a base class object
	objPtr[1]=&cat;						//dynamic binding to a derived class object
	objPtr[2]=&rat;						//dynamic binding to a derived class object

	for(int i=0; i<3; i++)
		objPtr[i]->whoami();			//no virtual function, no polymorphism.

	cout<<" this is the end of Part one." <<endl<<endl; 

	/************************************************************
	Part Two: Watch the output. Try to understand:
	Virtual methods vs. polymorphism.
	************************************************************/
	MyOne myDog;						
	MyTwo myCat; 
	MyThree myRat;

	MyOne * myObjPtr[3];				//base class pointer	
	myObjPtr[0]=&myDog;					//normal binding to a base class object
	myObjPtr[1]=&myCat;					//dynamic binding to a derived class object
	myObjPtr[2]=&myRat;					//dynamic binding to a derived class object

	for(int i=0; i<3; i++)
		myObjPtr[i]->whoami();			//polymorphism is realized via virtual functions

	cout<<" this is the end of Part Two." <<endl<<endl; 

	/************************************************************************************
	Part Three: Now you shall play with virtual methods vs. polymorphism:
	(1)	Define an abstract base class Write via defining a pure virtual  
	method 	void print().
	(2) Derive three classes MyWrite, YourWrite, HerWrite from the base
	class Write. 
	Redefine void print() in MyWrite to print "I like C++.". 
	Redefine void print() in YourWrite to print "You don't like C++.". 
	Redefine void print() in TheirWrite to print "They don't care about C++.". 
	(3) Use a base class Write pointer p and call p->print() to invoke different 
	definition of print in MyWrite, YourWrite and TheirWrite. 
	That is, use the same call p->print() to realize different functionalities
	-- polymorphisam. 
	*************************************************************************************/

	write *pointer[3];	//using a pointer of the abstract class type

	mywrite my;			//mywrite object
	yourwrite your;		//yourwrite object
	theirwrite their;	//theirwrite object

	pointer[0]=&my;		//pointer to my
	pointer[1]=&your;	//pointer to your
	pointer[2]=&their;	//pointer their


	//output same function from three different classes
	for (int i=0;i<3;i++)
	{	
		pointer[i]->print();

	}

	//complete the program 
	return 0; 
}
