#ifndef lab2380_9b_head_h

#define	lab2380_9b_head_h
#include "headers.h"

//base class
class write
{
public:
	void virtual print(void)=0;
};

//derived from write
class mywrite: public write
{
public: 
	void print(){cout<<"I like C++."<<endl;}
};

//derived from write
class yourwrite: public write
{
public: 
	void print(){cout<<"You don't like C++."<<endl;}
};

//derived from write
class theirwrite: public write
{
public: 
	void print(){cout<<"They don't care about C++."<<endl;}
};
#endif