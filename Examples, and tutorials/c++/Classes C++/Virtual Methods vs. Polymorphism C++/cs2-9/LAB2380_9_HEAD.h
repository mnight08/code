using namespace std; 

#ifndef		LAB2380_9_HEAD_H		
#define		LAB2380_9_HEAD_H
#include	"headers.h"
/***************************************************************
  Part One: Straight inheritance with virtual memthods
 ***************************************************************/
//base class One
class One
{	
public: 
	void whoami(){cout<<"This is One."<<endl;};
};

//derive Two from One
class Two: public One
{
public:
	void whoami(){cout<<"This is Two."<<endl;};
};

//derive three from Two
class Three: public Two
{
public:
	void whoami(){cout<<"This is Three."<<endl;};
};

/***************************************************************
  Part Two: Virtual memthods and Polymorphism
 ***************************************************************/
//base class MyOne
class MyOne
{	
public: 
	void virtual whoami(){cout<<"This is MyOne."<<endl;};	//this is virtual
};

//derive Two from One
class MyTwo: public MyOne									//this is virtual
{
public:
	void virtual whoami(){cout<<"This is MyTwo."<<endl;};
};

//derive three from Two
class MyThree: public MyTwo									//this is virtual
{
public:
	void virtual whoami(){cout<<"This is MyThree."<<endl;};
};



#endif
