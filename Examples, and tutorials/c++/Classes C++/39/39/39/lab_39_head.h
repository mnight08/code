// *****************************************************************
// Author:	Zhixiang Chen
// Class:	CSCI 1380.01, Summer I, 2008
// Lab 30:	Arrays of structures
// Date:	June 24, 2008
// Comment: The code here is meant to be revised.
// *****************************************************************

#include <iostream>
#include <cstring>
#include <string>


using namespace std; 

#ifndef		LAB_30_HEAD_H		
#define		LAB_30_HEAD_H


//define studentClass class
class studentClass 
{
private: 	
	string firstName,			//firstnames for students
		lastName,				//last names for students
		ID;					//IDs for students
	double record[COLUMN];		//record of attendance, hw1, ..., hw6, quiz, test1, test2, test3
	double score;				//scores for students
	char   grade;				//letter grades
public: 
	//default constructor
	studentClass(); 
	//another constructor
	studentClass(const string fn, const string ln, 
		const string ID, const double pts, 
		const double scr, const char gr); 
	//method to access private members
	string getName(){ return firstName + "  " + lastName;}	//inline method
	string getID(){return ID;}								//inline method
	double getScore(){ return score;}						//inline method
	char getGrade(){return grade;}							//inline method
	void getRecord(double rcd[ ], const int s); 
	//methods to set or process private data members
	void setName(const string fn, const string ln);
	void setID(const string id); 
	void setRecord( const double rcd[ ], const int s, int i); 
	void computeScoreGrade(); 
	//method to show show the result
	void printStudent(); 
	void printStudentToFile(ofstream & output);
	//destructor 
	~studentClass(); 
}; 

/********************************************************************************************
The following are implementation of class methods
********************************************************************************************/
//default constructor
studentClass::studentClass()
{
	firstName = ""; 
	lastName = ""; 
	ID = ""; 
	for (int i=0; i<COLUMN; i++)
		record[i]=0; 
	score = 0; 
	grade = ' '; 
}

//another constructor
studentClass:: studentClass(const string fn, const string ln, 
							const string ID, const double pts, 
							const double scr, const char gr)
{
	double att=0, hw=0, test=0, quiz=0; 
	int i;

	firstName = fn; 
	lastName = ln; 
	for (int i=0; i<COLUMN; i++)
		record[i] = pts; 
	//compute score
	att = record[0]*0.05; 
	for (i=1; i<=6; i++)
		hw += record[i]; 
	hw = (hw/6)*0.3; 
	test = record[7]+record[8]+record[9]; 
	test *= 0.6; 
	quiz = record[10]*0.05; 
	score = att + hw + test + quiz; 
	if (score >= 90)
		grade = 'A'; 
	else if (grade >= 80)
		grade = 'B'; 
	else if (grade >= 70)
		grade = 'C'; 
	else if (grade >= 60)
		grade = 'D'; 
	else
		grade = 'F'; 
}

//getrecod
void studentClass:: getRecord(double rcd[ ], const int s)
{
	for( int i=0; i<s; i++)
		rcd[i]=record[i]; 
}

//method setName
void studentClass:: setName(const string fn, const string ln)
{
	firstName = fn; 
	lastName = ln; 
}

//method setID
void studentClass:: setID(const string id)
{
	ID = id;  
}
//method setRecord

//by adding the int i as a parameter, i can allow other parts of program where to have it stop putting numbers
void studentClass:: setRecord( const double rcd[ ], const int s, int i)
{
	for (; i<s; i++)
		record[i]=rcd[i]; 
}

//method ComputerScoreGrade
void studentClass:: computeScoreGrade()
{
	int att=0, hw=0, test=0, quiz=0, i;

	//compute score
	att = record[0]*0.05; 
	for (i=1; i<=6; i++)
		hw += record[i]; 
	hw = (hw/6)*0.3; 
	test = record[7]+record[8]+record[9]; 
	test =(test/3)* 0.6; 
	quiz = record[10]*0.05; 

	score = att + hw + test + quiz; 

	//compute grade
	if (score >= 90)
		grade = 'A'; 
	else if (score >= 80)
		grade = 'B'; 
	else if (score >= 70)
		grade = 'C'; 
	else if (score >= 60)
		grade = 'D'; 
	else if(score<60)
		grade = 'F'; 
}

//method to show show the result
void studentClass:: printStudent()
{	
	//it should display decent now,dont forget to look at grading, score is way to high
	cout << left<<setw(8)<<firstName <<setw(8)<< lastName
		<< left<<setw(8) <<ID; 
	cout << setprecision(1)<<fixed<<showpoint<<left;
	for (int i=0; i<COLUMN; i++)
		cout << setw (4) <<int(record[i]); 
	cout<<setw(4)<<right<<int(score) 
		<<setw(5)<<right<< grade;
	cout<<endl; 
}

//this is to save changes to file
void studentClass::printStudentToFile(ofstream &output)
{

	//it should display decent now
	output << left<<setw(8)<<firstName <<setw(8)<< lastName
		   << left<<setw(8) <<ID; 
	output << setprecision(1)<<fixed<<showpoint<<left;
	for (int i=0; i<COLUMN; i++)
		output << setw (4) <<int(record[i]);


	output	<<endl; 

}

//destructor 
studentClass::~studentClass()
{
	firstName = ""; 
	lastName = ""; 
	ID = ""; 
	for (int i=0; i<COLUMN; i++)
		record[i]=0; 
	score = 0; 
	grade = ' '; 
}

//function to print new info to file


void label()
{
		cout<<"-------------------------------------------------------------------------------\n"
			<<"firt	last	ID	att hw1 hw2 hw3 hw4 hw5 hw6 T1  T2  T3 Quiz Score Grade\n"
			<<"-------------------------------------------------------------------------------\n";
}
void save(studentClass csci1380[], 
			const int SIZE)
{
	//your code is here.	
	//using size, limits the number of students

	ofstream output;
	output.open("lab_29_data.txt");
	//clear contents of file
	output.clear();

	output	<<"-------------------------------------------------------------------------------\n"
			<<"firt	last	ID	att hw1 hw2 hw3 hw4 hw5 hw6 T1  T2  T3 Quiz Score Grade\n"
			<<"-------------------------------------------------------------------------------\n";
	for (int i=0;i<SIZE;i++)
	{
		csci1380[i].printStudentToFile(output);

	}
	output.clear();
	output.close();
}

/****************************************************************
This function gets class information from the input file and store
the info to an array studentClass
inFile: input file
csci1380: an array of studentClass
SIZE, COLUMN: two input parameters
****************************************************************/
void loadData(ifstream & inFile, 
			  studentClass csci1380[], const int SIZE, const int COLUMN)
{
	char  str[1024];			//to get a line
	string fn, ln, id;			//to get firstName, lastName, ID
	double * rcd;					//int pointer
	rcd = new double[COLUMN];		//rcd is an array of COLUMN size to help get record

	//skip the first three lines
	inFile.getline(str, 1080, '\n'); 
	cout<<str<<endl; 
	inFile.getline(str, 1080, '\n'); 
	cout<<str <<endl;
	inFile.getline(str, 1080, '\n'); 
	cout<< str<<endl; 


	for (int row = 0; row < SIZE;row++)
	{
		inFile	>> fn					//get first name
			>> ln					//get last name
			>> id;					//get ID

		for (int col = 0; col <COLUMN; col++)
		{
			inFile >> rcd[col];	//get a row for csci1380
		}

		csci1380[row].setName(fn, ln);			//set name
		csci1380[row].setID(id);				//set ID
		csci1380[row].setRecord(rcd, COLUMN, 0);	//set record
		csci1380[row].computeScoreGrade();		//compute score and grade

	}
	inFile.close();						//close file
}



/****************************************************************
This is for you to complete: 
This function displays class information for CSCI 1380 
in some nice format.
****************************************************************/
void showGrades( studentClass csci1380[],const int SIZE)
{
	//your code is here.	
	for (int i=0;i<SIZE;i++)
	{
		csci1380[i].printStudent();

	}
}



int search(bool isid, const int size,studentClass csci1380[])
{	
	int position=0;
	string temp;
	string entry;	

	cout<<"enter a student id, or a name please->";
	
	cin>>entry;


	//test to see if there is still input
	if(entry[0]>64)
	{
		isid=false;
		cin>>temp;
		entry=entry+temp+"\0";
	}

	//if changed all the ids to make them each unique
	if (isid)
	{

		for (int i=0;i<size;i++)
		{
			position=i;
			temp=csci1380[i].getID();
			if (temp==entry)
				return position;
		}
	}

	//could be seperate function
	else
	{


		//i need to get rid of the spaces when loading name into temp
		for (int i=0;i<size;i++)
		{
			position=i;
			temp=csci1380[i].getName();

			int length=temp.length();

			//variable for the loop that finds out number of spaces
			int move=0, start=-1;


			//this is a loop count number of spaces, and where they are
			for (int pos=0;pos<length;pos++)
			{
				//this will be so i know how many spaces are in a row

				//this test if there is no more characters
				if (temp[pos]==0)
					break;
				else if(temp[pos]==32)
				{
					move++;
					if(move==1)
						start=pos;
				}


			}


			//this moves the characters back, replacing spaces
			for (int pos=(start+move);pos< length;pos++, start++)
			{
				temp[start]=temp[pos];
			}
			
			//gets rid of excess characters
			temp.erase(start);

			if (temp==entry)
				return position;
			

		}
	}

	return -1;

}

void pick(int&choice)
{
	choice=0;


	//i should probably add option to go back
	//i could group them to make this look nicer
	cout<<"enter the number that is next to what you would like to change??\n"
		<<"1 :name\n"
		<<"2 :ID\n"
		<<"3 :attendance\n"
		<<"5 :homework \n"
		<<"6 :test \n"
		<<"7 :quiz\n"
		<<"type here:";

	cin>>choice;
}
//i need to make this so changes occur in file also.

void alter(int position, studentClass csci1380[])
{		
	string temp, temp2;
	int gradetemp;
	char option='Y';
	while(true)
	{
		int choice=0;

		pick(choice);

		//for this to work i need to have a check for 
		//used ids, ids must be unique, or the whole 
		//concept of an id number is as pointless
		//as a name.
		switch(choice)
		{
			double rcdtemp[COLUMN];
		case 1:

			cout<<"enter a first name :";
			cin>>temp;
			cout<<"and now a last name";
			cin>>temp2;
			csci1380[position].setName(temp, temp2);
			cout<<"\n";
			label();
			csci1380[position].printStudent();
			break;

		case 2:
			cout<<"enter a new six digit id please :";
			cin>>temp;
			csci1380[position].setID(temp);
			label();
			csci1380[position].printStudent();
			break;

		case 3:
			cout<<"enter a new grade for attendence";
			cin>>gradetemp;	

			rcdtemp[position]=gradetemp;

			csci1380[position].setRecord(rcdtemp, position,position);
			csci1380[position].printStudent();
			break;

		case 4:

		case 5:

		case 6:

		case 7:


		default:
			cout<<"please enter the number between 1 and 7 next to what you would like to change";
			pick(choice);
		}
		cout<<"would you like to change anything else?(y/n)";
		cin>>option;
		if(toupper(option)!='Y'){
			option='N';
			cout<<"would you like to save changes to file?(y/n)";
			cin>>option;
			if (toupper(option)=='Y')
				save(csci1380, SIZE);
			break;
		}

	}


}

void driver(studentClass csci1380[], const int size)
{
	while(true)
	{
		string temp;
		bool isid=true;
		char option;
		int position=0;
		cout<< "do you wanna look for a student?(y/n)";
		cin>> option;

		if (toupper(option)!='Y')
			break;

		position=search(isid,size,csci1380);

		if (position<0){
			cout<<"the name could be not found, the search is case sensitive \n";
			continue;
		}

		//labels so user doesnt have to scroll up to see what is what
		label();
		//prints out student info
		csci1380[position].printStudent();

		
		//give option to change info
		cout<<"would you like to change a name, id, or one of the records?(y/n)";
		cin>>option;

		if (toupper(option)=='Y')
		alter(position, csci1380);				//i need to create this funtion.



	}

}

#endif
