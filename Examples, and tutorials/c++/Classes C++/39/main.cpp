// Author:	Zhixiang Chen
// Class:	CSCI 1380.01, Summer I, 2008
// Lab 39:	Classes
// Date:	July 2, 2008
// Comment: The code here is meant to be revised.
//
//-----------------------------------------------------------------
// Lab Exercise 30: 
//
// This lab exercise is to show how to process an array of classes.
// The problem is to define a strucure type to represent a students in class CSCI 1380
// and use an array of the structure to represent all students in the class. 
// 
// The class studentClass consists of the following members( or fields): 
// (1) studentFirstName, (2) studentLastName, (3) studentID, (4) a double array to 
// store value of attendance, hw1, hw2, hw3, hw4, hw5, hw6, quiz, test1, test2, test3, 
// (5) score for the class, and (6)  letter grade. 
//
// Once the studentClass has been defined, we shall declare an array of studentClass to represent all 
// students in the class. 
//
// The data is stored in file "lab_29_data.txt", and shall be loaded into the array.
//
// The formula to calculate the scores of attendance 5%, quiz 5%, homework 30%, tests 60%. 
// The grade is decided as follows: A if score >= 90, B if 80<= score <90, C if 70<= score < 80, 
// D if 60 <= score <70, and F otherwise.
//
// Compile and run your program. When everything is fine,
// print your .cpp file and turn it to me or Sergio
// *****************************************************************

#include < iostream >
#include <fstream >
#include < string >
#include < cstring >
#include < cmath> 
#include <cstdlib>
#include <iomanip>


//define a const array size
const int SIZE = 35;				//class size				
const int COLUMN = 11;				//column size
#include "lab_39_head.h" 


using namespace std; 

int main( )
{
	

	//declare an array of studentType to class CSCI 1380
	studentClass csci1380[SIZE];		//for att, quiz, hw, tests and score

	//file var
	ifstream inFile;					//input file

	//open file
	inFile.open("lab_29_data.txt"); 
	if(!inFile)
	{
		cout<<"Fail to open lab_28_data.txt, and stop ."<<endl;
		return 1; 
	}

	//initialize array letterStat
	loadData(inFile, csci1380, SIZE, COLUMN);

	 
	//show grades
	showGrades(csci1380, SIZE); 
	

	/****************************************************************************
	 This part is for you: 
		Design a loop to do the following: 
			(1) ask the user to either a student ID, or name. 
			(2) search for students and report his/her grade
			(3) you also ask the user to change the student name, ID, records
		Repeat the loop until the user chooses to quit.
	 *****************************************************************************/
	//Your code is here
	
	driver(csci1380, SIZE);

	//well done and exit
	return 0; 
}

