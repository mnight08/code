//-----------------------------------------------------------------
// Author:	alex martinez
// Class:	CSCI/CMPE 2380.01, spring, 2009
// Lab 7:	Destructor under inheritance vs. virtual destructor
//			Also, watch the chain reaction of constructor/desctructor calling
// Date:	2/19/09
// Comment: The code here is meant to be revised.
//
//-----------------------------------------------------------------
// Lab Exercise 7: 
//
// This lab exercise is practice virtual destructor
//
// Compile and run your program. When everything is fine,
// print your .cpp and .h files and turn it to me.
// *****************************************************************
#include "headers.h"


using namespace std; 

bool fun();					//define function fun
bool fun2();

int main()
{	
	bool tag = false; 
	int i=0;

	/*****************************************************************************
	Part One: Test non-virtual destructor
	*****************************************************************************/

	while(i<10)
	{
		tag = fun();		//inside fun, a class D object is created.
		//once outside fun, this object dies and the destructor 
		//is called to deallocated memory 
		i++;
		cout<<"This is the end of iteration "<< i <<endl
			<<"Can you see D's destructor was called? "<<endl<<endl; 

		if(!tag)
		{
			cout<<"Test is done. Memory is running out."<<endl; 
			break; 
		}
	}
	//a pause for the output
	cout<<"enter anything to continue";
	string x;
	cin>>x;
	/*************************************************************************
	Part Two: For you to complete.
	When polymorphism comes into the play, you got to be very careful about 
	the destructors to deal with dynamic memeory allocation. 
	The key point is that when a base class pointer is dynamically bound to 
	a derived class object the destructor of the derived class is not bound
	to the pointer, UNLESS virtual desctructor is used. 

	Try to use virtual destructor to re-do Part One and test whether you can
	see D's destructor.
	*************************************************************************/
	i=0;
	while(i<10)
	{
		tag = fun2();		//inside fun, a class za object is created.
		//once outside fun, this object dies and the destructor 
		//is called to deallocated memory 
		i++;
		cout<<"This is the end of iteration "<< i <<endl
			<<"Can you see z's destructor was called? "<<endl<<endl; 

		if(!tag)
		{
			cout<<"Test is done. Memory is running out."<<endl; 
			break; 
		}

	}
	cout<<"enter anything to continue";
	cin>>x;

	//complete the program 
	return 0; 
}

bool fun2()
{
	B* p;					//base class pointer p
	p=new z;				//dynmic binding of p to derived class object

	if (p)					//make sure p got memory allocated
	{
		delete p;
		cout<<"Yes, p got memory .... Alive ....." <<endl; 
		return true; 
	}
	else 
	{
		cout<<"Running out memory .... Die...."<<endl; 
		return false; 
	}
}		

bool fun()
{
	B* p;					//base class pointer p
	p=new D;				//dynmic binding of p to derived class object

	if (p)					//make sure p got memory allocated
	{
		delete p;
		cout<<"Yes, p got memory .... Alive ....." <<endl; 
		return true; 
	}
	else 
	{
		cout<<"Running out memory .... Die...."<<endl; 
		return false; 
	}
}	




