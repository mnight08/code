#ifndef lab10_head_h
#define lab10_head_h


#include "headers.h"


using namespace std; 

//base class B
class B 
{
private:
	char *ptrB;
public:
	B()
	{
		ptrB=new char[16];
		cout<<"B allocates 16 bytes"<<endl;
	};
	virtual ~B()
	{
		delete[] ptrB;
		cout<<"B frees 16 bytes"<<endl;
	};
};

//derived class D
class D: public B
{
private:
	char *ptrD;
public:
	D()
	{
		ptrD=new char[1000];
		cout<<"D allocates 1000 bytes"<<endl;
	};
	~D()
	{
		delete[] ptrD;
		cout<<"This is D's destructor. If you see this, then you shall be happy." <<endl
			<<"D free 1000 bytes"<<endl;
	};
};


//derived class z
class z: public B
{
private:
	char *ptrz;
public:
	z()
	{
		ptrz=new char[1000];
		cout<<"z allocates 1000 bytes"<<endl;
	};
	//define base as virtual, this replaces it 
	~z()
	{
		delete[] ptrz;
		cout<<"This is z's destructor. If you see this, then you shall be happy." <<endl
			<<"z free 1000 bytes"<<endl;
	};
};
#endif

