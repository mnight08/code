//-----------------------------------------------------------------
// Author:	alex martinez
// Class:	CSCI/CMPE 2380.01, spring, 2009
// Lab 10:	Play more with iterator methods for a class (or container)
//			via the implementation of the list class template
// Date:	2/19/09
// Comment: The code here is meant to be revised.
//
//-----------------------------------------------------------------
// Lab Exercise 10: 
//
// This lab exercise is to do more practice about iterator methods 
// for a class (or container). An interator is just like a pointer to 
// the class object, which can move via 
// address operations such as ++ and --, access the content pointed to by it via
// dereference operator *, and do comparisons, etc. With with help of interator 
// method, it is inconvenient to move around a "list" of objects and to do insertion, 
// deletion operatioms, etc. 
//
// Compile and run your program. When everything is fine,
// print your .cpp and .h files and turn it to me.
// *****************************************************************

#include "headers.h"

using namespace std; 


int main()
{	
	/*********************************************************
	Part A: Test interators
	*********************************************************/
	List<int>	cat,				//create a vector of size 10
		dog;						//create another vector of size 10

	cat.push_back(20);				//add 20 to the back
	cat.push_back(30);				//add 30 to the back; 
	cat.push_back(40);				//add 40 to the back

	List<int>::const_iterator itrOne;	
	List<int>::iterator itrTwo; 

	itrOne = cat.begin();			//itrOne points to dog for changing
	itrTwo = dog.begin();			//itrTwo points to cat for accessing with changing

	for ( ; itrOne != cat.end(); itrOne++)
		dog.push_back(*itrOne);

	cout<<"after copying cat to dog, the dog is .... " <<endl; 
	for (itrTwo=dog.begin(); itrTwo!=dog.end(); itrTwo++)
	{
		cout<<*itrTwo <<"  ";		//cout the current value in the list
		*itrTwo = *itrTwo + 100;	//change the current value in the list		
	}
	cout<<endl; 

	cout<<"after chaning dog, dog becomes.... " <<endl; 
	for (itrTwo=dog.begin(); itrTwo!=dog.end(); itrTwo++)
	{
		cout<<*itrTwo <<"  "; 
	}
	cout<<endl; 

	/*****************************************************************
	Part B: This part is for you. 
	Use interator and const_interator to reverse the elements in 
	the list member of cat and print the reversed list of cat.
	*****************************************************************/

	List<int>::iterator itrone;
	List<int>::iterator itrtwo;

	itrone=--cat.end();
	itrtwo=dog.begin();

	//copy cat into dog, backwards
	for(;itrone!=--cat.begin();itrone--, itrtwo++)	//was not sure on how to define >, 
													//and < operators, so i just defined --, 
	{												//and used dog as a swap area
		*itrtwo=*itrone;
	}

	//copy dog back into cat
	for (itrone=cat.begin(), itrtwo=dog.begin();itrtwo!=dog.end();itrone++, itrtwo++)
	{
		*itrone=*itrtwo;
	}

	itrone=cat.begin();

	cout<<"the backwards list is:"<<endl;
	//output cat
	for(;itrone!=cat.end();itrone++)
		cout<<*itrone<<" ";
	cout<<endl;	

	//complete the program
	return 0; 
}
