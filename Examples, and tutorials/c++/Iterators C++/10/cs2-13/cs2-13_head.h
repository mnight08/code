using namespace std; 

#ifndef		cs2_13_HEAD_H		
#define		cs2_13_13_HEAD_H

#include "headers.h"

//declare myVecror class template
template <class type>
class List
{
private: 
	//define private node type 
	struct Node								//this a node type for double linked list
	{
		type data; 
		Node *prev; 
		Node *next; 
		Node(const type& d = type(), Node* p = NULL, Node* n=NULL)
			:data(d), prev(p), next(n){ };	//constructor
	}; 

public: 
	//define const_iterator class
	class const_iterator
	{
		friend class List<type>;			//List is a friend to const_iterator	

	public: 
		const_iterator(): current(NULL){ }; 
		
		const type& operator* ( ) const		//overload rhe dereference operator *
		{
			return retrieve( ); 
		}; 

		const_iterator& operator++( )		//prefix increment operator
		{
			current = current->next; 
			return *this; 
		};

		const_iterator& operator++(int)		//postfix increment operator. The parameter (int) is
		{									// for differentiating from 
											//the prefix increment operator
			const_iterator old = *this; 
			++(*this);  
			return old; 
		};

		bool operator==(const const_iterator &rhs) const
		{ 
			return current == rhs.current;
		};

		bool operator!=(const const_iterator &rhs) const
		{ 
			return !(*this == rhs);
		};

	protected: 
		Node * current; 

		type & retrieve( ) const
		{
			return current->data;
		};

		const_iterator(Node *p):current(p){ };
	}; 
	
	//define iterator class, which is derived from const_iterator
	class iterator: public const_iterator
	{
		friend class List<type>;		//List is a friend 
	
	public: 
		iterator(){ };					//default constructor

		type & operator*( )				//overload deference operator
		{								//the returned value can be changed
			return retrieve(); 
		}; 

		const type& operator*( ) const	//another overloading of deference operator
		{								//the returned value cannot be changed
			return const_iterator::operator*();
		}; 

		iterator & operator++( )		//overload prefix increment operator
		{
			current = current ->next; 
			return *this; 
		}; 

		iterator & operator++( int )	//overload postfix increment operator
		{
			iterator old = *this; 
			++(*this); 
			return old; 
		}; 

		
		iterator & operator--()			//overload postfix decrement operator
		{
			current=current->prev;  
			return *this; 
		};

		iterator & operator--( int )	//overload postfix increment operator
		{
			iterator old = *this; 
			--(*this); 
			return old; 
		}; 


	protected: 
		iterator (Node *p):const_iterator(p){ }
	}; 

	//List methods
	List();								//default constructor
	List(const List& rhs);				//copy constructor
	~List();							//destructor
	const List & operator= (const List& rhs); //overload =

	iterator begin()					//get the position of the first node
	{
		return iterator(head->next); 
	};

	const_iterator begin() const		//overload begin()
	{
		rerurn const_iterator(head->next);
	};

	iterator end()						//get the end position 
	{
		return iterator(tail); 
	};

	const_iterator end() const			//overload end()
	{
		return const_iterator(tail);
	};
	
	int size()const {return theSize; };	//get the size
	bool empty()const {return size()==0;};//check if empty
	void clear();						//delete the list
	type & front(){return *begin(); }	//get the value of the first node
	
	const type & front() const			//overload front
	{
		return *begin(); 
	};

	type & back(){return *--end();};	//get the value of the last node and take it off
	
	const type & back() const			//overload back
	{
		return *--end(); 
	};

	void push_front(const type &x)		//add x to the front
	{
		insert(begin(), x); 
	};

	void push_back(const type &x)		//add to the back
	{
		insert(end(), x); 
	};

	void pop_front()					//delete the front node without returning value
	{
		erase(begin()); 
	};

	void pop_back()						//delete the end node without returning value
	{
		erase(--end()); 
	};
	
	iterator insert(iterator itr, const type & x) //insert x  at position itr
	{
		Node *p=itr.current; 
		theSize++; 
		return iterator(p->prev = p->prev->next = 
						new Node(x, p->prev, p)); 
	};

	iterator erase(iterator itr)			//erase node at itr
	{
		Node *p = itr.current;				//get the current node to be deleted
		iterator retVal(p->next);			//get the next node to be returned
		p->prev->next = p->next;			//previous node of the current node points to 
											//the next node of the current node
		p->next->prev = p->prev;			//next node of the current node points to 
											//the previous node of the current node

		delete p;							//delete the current
		theSize--;							//decrease the size
		
		return retVal;						//the next node to the current node just deleted
	};
		
	iterator erase(iterator start, iterator end) //erase nodes between start and end								
	{
		for (iterator itr = start; itr != end; )
		itr = erase(itr); 
		
		return end; 
	};		


private: 
	int theSize; 
	Node * head; 
	Node * tail; 

	void init( );						//private init() method 
}; 

//default constructor
template<class type>
List<type>:: List()								
{
	init(); 
}

//destructor
template <class type>
List<type>::~List()								
{
	clear(); 
	delete head; 
	delete tail; 
}

//copy constructor
template<class type>
List<type>:: List(const List& rhs)				
{
	init(); 
	*this = rhs; 
}

//overlaod =
template <class type>
const List<type>& List<type>:: operator=(const List & rhs)
{
	if (this == &rhs)
		return *this; 

	clear(); 
	for (cons_iteraror itr = rhs.begin(); 
		 itr != rhs.end(); itr++)
	{
		push_back(*itr); 
	}

	return *this; 
}

//delete the list
template<class type>
void List<type>::clear()						//delete the list
{
	while(!empty())
		pop_front(); 
}

//init()
template<class type>
void List<type>::init()
{
	theSize =  0; 
	head = new Node; 
	tail = new Node; 
	head->next = tail; 
	tail->prev = head; 
}



#endif
