using namespace std; 

#ifndef		LAB2380_12_HEAD_H		
#define		LAB2380_12_HEAD_H

#include "headers.h"
//declare myVecror class template
template <class type>
class myVector
{
public: 
	//constructors
	explicit myVector (int initSize=0);			//constructor 
	myVector(const myVector<type> & rhs);		//copy constructor
	~myVector();								//destructor

	//operator overloading
	const myVector & operator= (const myVector<type> & rhs); 
	type & operator[ ]( int index);				//for change the value
	const type & operator[ ] (int index) const; //for get the value without changing it

	//methods
	void resize(int newSize); 
	void reserve(int newCapacity); 
	bool empty() const; 
	int size() const; 
	int capacity() const; 
	void push_back(const type& x); 
	void pop_back(); 
	const type& back() const; 

	//iterator type and iterator methods
	typedef type* iterator;						//define iterator type for changing values
	typedef const type * const_iterator;		//define const iterator type for accessing 
												//without changing values
	
	iterator begin(){return &list[0];};							//for changing
	const_iterator begin() const{return &list[0];};				//without changing
	iterator end(){return &list[theSize];};						//for chaning					
	const_iterator end() const{return &list[theSize];};			//without chaning

	//auxillary member
	enum {SPARE_CAPACITY = 16};					//for additional capacity

private: 
	int theSize; 
	int theCapacity; 
	type *list;									//for dynamic array
}; 

//method implementations
//constructors
template <class type>
myVector<type> ::myVector (int initSize=0): theSize(initSize), 
				theCapacity(theSize + SPARE_CAPACITY)
{	
	list = new type[theCapacity]; 
}

//copy constructor
template <class type> 
myVector<type>:: myVector(const myVector<type> & rhs):list(NULL)
{
	*this = rhs;								//use operator= to copy
}

//destructor
template<class type>
myVector<type>::~myVector()
{
	delete[] list; 
}

//operator= overloading
template<class type>
const myVector<type>& myVector<type>:: operator= (const myVector<type> & rhs)
{
	if(this!=&rhs)
	{	
		delete[] list; 
		theSize = rhs.theSize; 
		theCapacity = rhs.theCapacity; 

		list = new type[theCapacity]; 
		for (int i = 0; i < theSize; i++)
			list[i] = rhs.list[i]; 
	}

	return *this; 
}

//operator[ ] overloading
template<class type>
type& myVector<type>::operator[ ]( int index)//to change the value
{
	return list[index]; 
}

//operator[ ] overloading
template<class type>						//to get the value without changing it
const type & myVector<type>::operator[ ] (int index) const 
{
	return list[index]; 
}

//reseize method
template<class type>
void myVector<type>::resize(int newSize)
{
	if(newSize>theCapacity)
		reserve(newSize*2 + 1); 
	theSize = newSize; 
}

//reserve
template<class type>
void  myVector<type>::reserve(int newCapacity)
{
	if(newCapacity < theSize)
		return; 

	type * oldList = list; 
	list = new type[newCapacity];
	for (int i=0; i<theSize; i++)
		list[i]=oldList[i]; 

	theCapacity = newCapacity; 
	delete[] oldList; 
}

//empty
template<class type>
bool myVector<type>::empty() const
{
	return theSize==0; 
}

//size
template<class type>
int myVector<type>::size() const
{
	return theSize; 
}

//capacity
template<class type>
int myVector<type>::capacity() const
{
	return theCapacity; 
}

//push_back
template<class type>
void myVector<type>::push_back(const type& x)
{
	if(theSize == theCapacity)
		reserve(2*theCapacity + 1); 
	list[theSize++] =  x; 
}

//pop_back
template<class type>
void myVector<type>::pop_back()
{
	theSize--; 
}

//back
template<class type>
const type& myVector<type>::back() const
{
	return list[theSize - 1]; 
}
#endif
