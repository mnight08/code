//-----------------------------------------------------------------
// Author:	alex martinez
// Class:	CSCI/CMPE 2380.01, spring, 2009
// Lab 9:	Play with iterator methods for a class (or container)
//			via the implementation of vector class template
// Date:	September 28, 2008
// Comment: The code here is meant to be revised.
//
//-----------------------------------------------------------------
// Lab Exercise 9: 
//
// This lab exercise is practice iterator method for a class (or container). 
// An interator is just like a pointer to the class object, which can move via 
// address operations such as ++ and --, access the content pointed to by it via
// dereference operator *, and do comparisons, etc. With with help of interator 
// method, it is inconvenient to move around a "list" of objects and to do insertion, 
// deletion operatioms, etc. 
//
// Compile and run your program. When everything is fine,
// print your .cpp and .h files and turn it to me.
// *****************************************************************

#include "headers.h"

using namespace std; 


int main()
{	
	/*********************************************************
	Part A: Test interators
	*********************************************************/
	myVector<int> cat,				//create a vector of size 10
		dog;						//create another vector of size 10

	cat.push_back(20);				//add 20 to the back
	cat.push_back(30);				//add 30 to the back; 
	cat.push_back(40);				//add 40 to the back

	myVector<int>::iterator itrOne;	
	myVector<int>::iterator itrTwo; 

	itrOne = cat.begin();			//itrOne points to dog for changing
	itrTwo = dog.begin();			//itrTwo points to cat for accessing with changing

	for ( ; itrOne < cat.end(); itrOne++, itrTwo++)
		dog.push_back(*itrOne);		//copy contents of cat to dog

	cout<<"after copying cat to dog, the dog is .... " <<endl; 
	for (itrTwo=dog.begin(); itrTwo<dog.end(); itrTwo++)
	{
		cout<<*itrTwo <<"  ";		//cout the current value in the list
		*itrTwo = *itrTwo + 100;	//change the current value in the list		
	}
	cout<<endl; 

	cout<<"after chaning dog, dog becomes.... " <<endl; 
	for (itrTwo=dog.begin(); itrTwo<dog.end(); itrTwo++)
	{
		cout<<*itrTwo <<"  "; 
		*itrTwo = *itrTwo + 100; 
	}
	cout<<endl; 

	/*****************************************************************
	Part B: This part is for you. 
	Use interator and const_interator to reverse the elements in 
	the list member of cat and print the reversed list of cat.
	*****************************************************************/
	myVector<int>::iterator itrone;
	myVector<int>::iterator itrtwo;
	int temp;

	itrone=cat.begin(); 
	itrtwo=(cat.end()-1);			//might want to make .end() point to 
									//end as seen by array, not the size.
	
	cout<<"the backwards list is:"<<endl;
	//reverse the list
	for(;itrone<itrtwo&&itrone!=itrtwo;itrone++,itrtwo--)
	{
		temp=*itrone;
		*itrone=*itrtwo;
		*itrtwo=temp;
	}

	//output the list
	for (itrone=cat.begin();itrone<cat.end();itrone++)
		cout<<*itrone<<" ";
	cout<<endl;


	//complete the program
	return 0; 
}
