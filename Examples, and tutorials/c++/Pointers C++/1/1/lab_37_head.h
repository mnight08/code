#include <iostream>
#include <cstring>
#include <string>
#include <vector> 

using namespace std; 

#ifndef		LAB_30_HEAD_H		
#define		LAB_30_HEAD_H


//define studentType structure
struct booktype 
{
	string title,					//firstnames for students
		   author,
		   isbn;					//last names for students
	int	price;						//letter grades
}; 

//define studentType structure
struct studentType 
{
	string firstName,				//firstnames for students
		   lastName;				//last names for students
	char   grade;					//letter grades
}; 

//swapOne takes two pointer type parameters 
void swapOne(int * x, int * y)
{
	//watch carefully how we do the swaping. Compare this with swapTwo
	int temp; 
	temp = *x;					
	*x = *y; 
	*y = temp; 
}

//swapTwo takes two reference type parameters 
void swapTwo(int & x, int & y)
{
	//watch carefully how we do the swaping. Compare this with swapOne
	int temp; 
	temp = x;					
	x = y; 
	y = temp; 
}
//swapOne takes two int type parameters 
void swapThree(int x, int y)
{
	//watch carefully how we do the swaping
	int temp; 
	temp = x;					
	x = y; 
	y = temp; 
}

//swapOne takes two pointer type parameters 
void swapOne(booktype * x, booktype * y)
{
	//watch carefully how we do the swaping. Compare this with swapTwo
	booktype temp; 
	temp = *x;					
	*x = *y; 
	*y = temp; 
}

//swapTwo takes two reference type parameters 
void swapTwo(booktype & x, booktype & y)
{
	//watch carefully how we do the swaping. Compare this with swapOne
	booktype temp; 
	temp = x;					
	x = y; 
	y = temp; 
}
//swapOne takes two pointer type parameters 
void swapThree(booktype x, booktype y)
{
	//watch carefully how we do the swaping
	booktype temp; 
	//shouldnt i have had to overload = for this to work?
	temp = x;					
	x = y; 
	y = temp; 
}

#endif
