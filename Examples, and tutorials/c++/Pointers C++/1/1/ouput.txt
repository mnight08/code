cat is     10,  dog is    20,  intPtr is 0012FF50,  *intPtr is 10
cat is     55,  dog is    10,  intPtr is 0012FF50,  *intPtr is 55
Alice  Smith got A
Alex  Black got B
use array name ............
10 20 30 40 50
*myPtr = *a = a[0] = 10
*myPtr = *(a+1) = a[1] = 20
What is *&a[2]? 30
Enter a first name => alex
Enter a last name => martinez
Enter letter a grade => B
Enter a first name => jo
Enter a last name => jc
Enter letter a grade => A
Enter a first name => jol
Enter a last name => lok
Enter letter a grade => C
alex  martinez got B
jo  jc got A
jol  lok got C
before calling swapOne(...), x is 10 and y is 20
after calling swapOne(...), x is 20 and y is 10
before calling swapTwo(...), u is 30 and v is 40
after calling swapTwo(...), u is 40 and v is 30
before calling swapThree(...), w is 50 and z is 60
after calling swapThree(...), w is 50 and z is 60
cat is     5,  dog is    2.5,  intPtr is 12fd28,  *intPtr is 5
cat is     104,  dog is    5,  intPtr is 12fd28,  *intPtr is 104
Alice  Smith got 1234A 100
Alex  Black got 1245d 2
use array name ............
F K P U Z
*mycharPtr = *a = a[0] = F
*mycharPtr = *(a+1) = a[1] = K
What is *&a[2]? P
Enter a first name => ok
Enter a last name => here
Enter a letter grade => A
Enter a first name => jon
Enter a last name => doe
Enter a letter grade => B
Enter a first name => jane
Enter a last name => doe
Enter a letter grade => A
ok  here got A
jon  doe got B
jane  doe got A
before calling swapOne(...), x is a b 30 1232 and y is as wsx 30 12342
after calling swapOne(...), x is as wsx 30 12342 and y is a b 30 1232
before calling swapTwo(...), u is qwsd hhgy 30 23433 and v is werg dfsb 30 233e
after calling swapTwo(...), u is werg dfsb 30 233e and v is qwsd hhgy 30 23433
before calling swapThree(...), w is bgf ret 30 2311 and z is jup re 30 2342
after calling swapThree(...), w is bgf ret 30 2311 and y is jup re 30 2342
Press any key to continue . . .