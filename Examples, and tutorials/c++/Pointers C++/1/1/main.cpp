// Author:	alex martinez
// Class:	CSC2  spring I, 2009
// Lab 1:	Pointers, pointer arithmetic, pointers vs. arrays, 
//			pointer parameters
// Date:	2/19/09
// Comment: The code here is meant to be revised.
//
//-----------------------------------------------------------------
// Lab Exercise 1: 
//
// This lab exercise is to practice pointers, pointer arithmetic, pointers vs. arrays, 
//			pointer parameters
// 
//
// Compile and run your program. When everything is fine,
// print your .cpp file and turn it to me or Sergio
// *****************************************************************

#include < iostream >
#include <fstream >
#include < string >
#include < cstring >
#include < cmath> 
#include <cstdlib>
#include <iomanip>
#include <vector>					//include vector class template

//define a const array size
const int SIZE = 35;				//class size				
const int COLUMN = 11;				//column size

#include "lab_37_head.h" 

using namespace std; 


int main( )
{


	/************************************************************************
	Part A: pointer declarations, operators:
	* : dereference operator
	& : address operator
	. : member selection operator
	->: dereference member selection operator 
	************************************************************************/
	//simple pointers
	int cat, dog;						//two int vars
	int *intPtr;						//an int pointer
	cat = 10; 
	dog = 20; 
	intPtr = &cat;						//intPtr points to cat, i.e., intPtr has the address of cat
	cout << "cat is	   " <<cat <<",  "
		<< "dog is    " <<dog <<",  "
		<< "intPtr is " <<intPtr<<",  "
		<<"*intPtr is " <<*intPtr<<endl; 

	dog = *intPtr;						//get what is pointed by intPtr to dog
	*intPtr = 55;						//change what is pointed by intPtr to 55

	intPtr = &cat;						//intPtr points to cat, i.e., intPtr has the address of cat
	cout << "cat is	   " <<cat <<",  "
		<< "dog is    " <<dog <<",  "
		<< "intPtr is " <<intPtr<<",  "
		<<"*intPtr is " <<*intPtr<<endl; 

	//structure/class pointers
	studentType jane, joe;				//two students
	studentType *ptr;					//student pointer

	//using member selection operator . to access members of a structure/class
	jane.firstName="Alice";				//initilization
	jane.lastName="Smith";
	jane.grade='A'; 

	//using member selection operator . to access members of a structure/class
	joe.firstName="Alex";				//initilization
	joe.lastName="Black";
	joe.grade='B'; 

	ptr = &jane;						//ptr points to jane

	//use dereference member selection operator . to access members of a structure/class
	cout << ptr->firstName <<"  " 
		<< ptr->lastName  <<" got "
		<< ptr->grade << endl; 

	ptr = &joe;							//ptr points to joe

	//use dereference operator *, member selection operator ., 
	//and dereference member selection operator 
	//to access members of a structure/class
	cout << (*ptr).firstName <<"  " 
		<< (*ptr).lastName  <<" got "
		<< ptr->grade << endl; 


	/************************************************************************
	Part B: Pointer arithemtic, arrays vs. pointers
	/* ************************************************************************/
	//var declaration
	int a[5]={10, 20, 30, 40, 50};		//an int array of size 5
	studentType fun[3];					//a studentType array of size 3
	int *myPtr;							//an int pointer
	studentType *yourPtr;				//a studentType point
	int i;								//loop var

	//array name is a pointer pointing to the first element of the array
	//use array name pointer to access array elements
	cout<<"use array name ............ "<<endl; 
	for (i=0; i<5; i++)
	{
		cout<< *(a+i) <<" ";			//a+i is &a[i], *(a+i) is a[i]
	}
	cout<<endl; 

	myPtr = a; 
	cout<<"*myPtr = *a = a[0] = " << *myPtr<<endl; 
	myPtr = &a[1]; 
	cout<<"*myPtr = *(a+1) = a[1] = " << *myPtr<<endl;
	cout<<"What is *&a[2]? "         <<*&a[2]<<endl; 

	yourPtr = fun;						//youPtr points to the first element of fun array
	for (i=0; i<3; i++)
	{
		cout<<"Enter a first name => "; 
		cin>> (yourPtr+i)->firstName;  
		cout<<"Enter a last name => "; 
		cin>> (*(yourPtr+i)).lastName; 
		cout<<"Enter letter a grade => "; 
		cin>> yourPtr[i].grade;
		cin.ignore();
	}
	//cout students
	yourPtr = fun; 
	for (i=0; i<3; i++)
	{
		cout << (yourPtr+i)->firstName <<"  "
			<< (*(yourPtr+i)).lastName <<" got " 
			<<  yourPtr[i].grade <<endl; 					
	}


	/************************************************************************
	Part C: Pointer parameter vs reference parameters vs value parameter
	************************************************************************/
	// var declaration
	int x =10, y=20; 
	int u=30, v=40; 
	int w=50, z=60; 
	//call swapOne with pointer type paramters
	cout<<"before calling swapOne(...), x is " << x << " and y is " <<y <<endl; 
	swapOne(&x, &y);					//watch, we pass &x and &y to this function!!!!!! 
	cout<<"after calling swapOne(...), x is " << x << " and y is " <<y <<endl; 

	//call swapTwo with reference type paramters
	cout<<"before calling swapTwo(...), u is " << u << " and v is " <<v <<endl; 
	swapTwo(u, v);						//watch, we pass u and v to this function!!!!!! 
	cout<<"after calling swapTwo(...), u is " << u << " and v is " <<v <<endl; 

	//call swapThree with value type paramters
	cout<<"before calling swapThree(...), w is " << w << " and z is " <<z <<endl; 
	swapThree(w, z);					
	cout<<"after calling swapThree(...), w is " << w << " and z is " <<z <<endl; 


	/*******************************************************************************
	Part D: This part is for you to complete.
	Repeat Parts A to C with differenet type of variables, arrays, structures.
	*******************************************************************************/
	//Your code  is here


	/************************************************************************
	Part A: pointer declarations, operators:
	* : dereference operator
	& : address operator
	. : member selection operator
	->: dereference member selection operator 
	************************************************************************/
	//simple pointers
	{	
		double cat, dog;						//two int vars
		double *doublePtr;						//an int pointer
		cat = 5; 
		dog = cat/2; 
		doublePtr = &cat;						//intPtr points to cat, i.e., 
		//intPtr has the address of cat
		cout << "cat is	   " <<cat <<",  "
			<< "dog is    " <<dog <<",  "
			<< "intPtr is " <<hex<<int(doublePtr)<<",  "
			<<"*intPtr is " <<*doublePtr<<endl; 

		dog = *doublePtr;						//get what is pointed by intPtr to dog
		*doublePtr = 'h';						//change what is pointed by intPtr to 55

		doublePtr = &cat;						//intPtr points to cat, i.e., intPtr has the address of cat
		cout << "cat is	   " <<cat <<",  "
			<< "dog is    " <<dog <<",  "
			<< "intPtr is " <<hex<<int(doublePtr)<<",  "
			<<"*intPtr is " <<*doublePtr<<endl; 

		//structure/class pointers
		booktype one, two;						//two students
		booktype *ptr;							//student pointer

		//using member selection operator . to access members of a structure/class
		one.title="Alice";						//initilization
		one. author="Smith";
		one.isbn="1234A";
		one.price=100;

		//using member selection operator . to access members of a structure/class
		two.title="Alex";						//initilization
		two.author="Black";
		two.isbn="1245d"; 
		two.price=2;

		ptr = &one;								//ptr points to jane

		//use dereference member selection operator . to access members of a structure/class
		cout << ptr->title <<"  " 
			<< ptr->author  <<" got "
			<< ptr->isbn << " "
			<< dec<<ptr->price <<endl; 

		ptr = &two;								//ptr points to joe

		//use dereference operator *, member selection operator .,
		//and dereference member selection operator 
		//to access members of a structure/class
		cout << (*ptr).title <<"  " 
			<< (*ptr).author  <<" got "
			<< ptr->isbn <<" "
			<< ptr->price << endl; 

	}
	/************************************************************************
	Part B: Pointer arithemtic, arrays vs. pointers
	************************************************************************/
	{
		//var declaration
		char a[5]={70, 75, 80, 85, 90};		//an int array of size 5
		studentType fun[3];					//a studentType array of size 3
		char *mycharPtr;					//an int pointer
		studentType *yourstPtr;				//a studentType point
		char i;								//loop var

		//array name is a pointer pointing to the first element of the array
		//use array name pointer to access array elements
		cout<<"use array name ............ "<<endl; 
		for (i=0; i<5; i++)
		{
			cout<< *(a+i) <<" ";			//a+i is &a[i], *(a+i) is a[i]
		}
		cout<<endl; 

		mycharPtr = a; 
		cout<<"*mycharPtr = *a = a[0] = " << *mycharPtr<<endl; 
		mycharPtr = &a[1]; 
		cout<<"*mycharPtr = *(a+1) = a[1] = " << *mycharPtr<<endl;
		cout<<"What is *&a[2]? "         <<*&a[2]<<endl; 

		yourstPtr = fun;					//youPtr points to the first element of fun array
		for (i=0; i<3; i++)
		{
			cout<<"Enter a first name => "; 
			cin>> (yourstPtr+i)->firstName;  
			cout<<"Enter a last name => "; 
			cin>> (*(yourstPtr+i)).lastName; 
			cout<<"Enter a letter grade => "; 
			cin>> yourstPtr[i].grade;
			cin.ignore();
		}
		//cout students
		yourstPtr = fun; 
		for (i=0; i<3; i++)
		{
			cout << (yourstPtr+i)->firstName <<"  "
				<< (*(yourstPtr+i)).lastName <<" got " 
				<<  yourstPtr[i].grade <<endl; 					
		}
	}
	/************************************************************************
	Part C: Pointer parameter vs reference parameters vs value parameter
	************************************************************************/
	{	// var declaration
		booktype x, y, z, w, u, v;
		x.title="a"; x.author="b"; x.isbn="1232"; x.price=30; 
		y.title="as"; y.author="wsx"; y.isbn="12342"; y.price=30; 
		u.title="qwsd"; u.author="hhgy"; u.isbn="23433"; u.price=30; 
		v.title="werg"; v.author="dfsb"; v.isbn="233e"; v.price=30; 
		w.title="bgf"; w.author="ret"; w.isbn="2311"; w.price=30; 
		z.title="jup"; z.author="re"; z.isbn="2342"; z.price=30; 


		//call swapOne with pointer type paramters
		cout<<"before calling swapOne(...), x is " << x.title<<" "
			<<x.author<<" "<<x.price<<" "<<x.isbn
			<< " and y is " << y.title<<" "<<y.author<<" "
			<<y.price<<" "<<y.isbn <<endl; 
		swapOne(&x, &y);					//watch, we pass &x and &y to this function!!!!!! 
		cout<<"after calling swapOne(...), x is "  << x.title<<" "
			<<x.author<<" "<<x.price<<" "<<x.isbn
			<< " and y is " << y.title<<" "<<y.author<<" "
			<<y.price<<" "<<y.isbn <<endl; 

		//call swapTwo with reference type paramters
		cout<<"before calling swapTwo(...), u is "  
			<< u.title<<" "<<u.author<<" "<<u.price<<" "<<u.isbn
			<< " and v is " << v.title<<" "
			<<v.author<<" "<<v.price<<" "<<v.isbn <<endl; 
		swapTwo(u, v);						//watch, we pass u and v to this function!!!!!! 
		cout<<"after calling swapTwo(...), u is "  << u.title
			<<" "<<u.author<<" "<<u.price<<" "<<u.isbn
			<< " and v is " << v.title<<" "
			<<v.author<<" "<<v.price<<" "<<v.isbn <<endl;  

		//call swapThree with value type paramters
		cout<<"before calling swapThree(...), w is "  
			<< w.title<<" "<<w.author<<" "<<w.price<<" "<<w.isbn
			<< " and z is " << z.title<<" "<<z.author<<" "
			<<z.price<<" "<<z.isbn <<endl; 
		swapThree(w, z);					//doesnt affect outside of itself				
		cout<<"after calling swapThree(...), w is "
			<< w.title<<" "<<w.author<<" "<<w.price<<" "<<w.isbn
			<< " and y is " << z.title<<" "<<z.author<<" "
			<<z.price<<" "<<z.isbn <<endl; 


	}	//well done and exit
	return 0; 
}

