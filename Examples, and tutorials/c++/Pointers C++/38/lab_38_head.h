
#include <iostream>
#include <cstring>
#include <string>
#include <vector> 

using namespace std; 

#ifndef		LAB_38_HEAD_H		
#define		LAB_38_HEAD_H


//this function reverse a char string via pointer operations
char * reverse(char * s )
{
	char *p, *q, tmp; 
	int n; 

	n=strlen(s);						//get the length of string s
	q = (n > 0)? s + n - 1 : s;			//see what this do?
	//p starts at the beginning and move to right
	//q starts at the end and move to left
	for (p = s; p < q; ++p, --q)
	{
		//swap the char pointed by p with the char pointed by q 
		tmp = *p;						
		*p = *q; 
		*q = tmp; 
	}

	return s; 
}


//i dont think this was needed
//define studentType structure
struct studentType 
{
	string firstName,					//firstnames for students
		lastName;						//last names for students
	char   grade;						//letter grades
}; 

//swapOne takes two pointer type parameters 
void swapOne(int * x, int * y)
{
	//watch carefully how we do the swaping. Compare this with swapTwo
	int temp; 
	temp = *x;					
	*x = *y; 
	*y = temp; 
}

//swapTwo takes two reference type parameters 
void swapTwo(int & x, int & y)
{
	//watch carefully how we do the swaping. Compare this with swapOne
	int temp; 
	temp = x;					
	x = y; 
	y = temp; 
}
//swapOne takes two pointer type parameters 
void swapThree(int x, int y)
{
	//watch carefully how we do the swaping
	int temp; 
	temp = x;					
	x = y; 
	y = temp; 
}

//need to see what he wants diff from other reverse function.
void reversep(char* ch)
{
	char *a, *b, tmp;
	int length=0;
	length= strlen(ch);

	a=ch+(length-1);
	//this reverses it in output, no swaping involved
	for(int i=0;i<(strlen(ch)-1);i++)
	{
		cout<<*(a--);
	}

	cout<<endl;



}
void list()
{
	//should i use strings, or characters?
	int size;
	string *p;
	cout<<"how big of a list do you wanna enter??->";
	cin>>size;
	//creates array of strings 
	p=new string[size];
	for(int i =0;i<size;i++)
	{
		cout<<"enter a word->";
		//inputs a word into memory location p+i is at
		cin>>p[i];
		for (int j=0;j<p[i].length();j++)
		{
			p[i][j]=toupper(p[i][j]);
		}

	}
	for (int i =0;i<size;i++)
	{
		cout<<"the upper case of word "<<i+1<<" of the list is :";
		cout<<p[i]<<endl;
	}



}
#endif
