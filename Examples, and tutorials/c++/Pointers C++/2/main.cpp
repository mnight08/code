// *****************************************************************
// Author:	alex martinez 
// Class:	CSC2 spring 2009
// Date:	2/19/09
// Lab 2:	More pointer practice
//
//-----------------------------------------------------------------
// Lab Exercise 2: 
//
// This lab exercise is to practice more about pointers.
//
// There are some running-time bugs in this program. Try to fix the bugs.
// 
// Compile and run your program. When everything is fine,
// print your .cpp file and turn it to me or Sergio
// *****************************************************************

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath> 
#include <cstdlib>
#include <iomanip>
#include <vector>						//include vector class template

//define a const array size
const int SIZE = 35;					//class size				
const int COLUMN = 11;					//column size

#include "lab_38_head.h" 

using namespace std; 


int main( )
{

	/************************************************************************
	Part A: Use pointer operation to revser a string of chars
	************************************************************************/
	char str[]="abcdefghijklmnopqrstuvwxyz";//a string of letters
	char *ptr;								//char ptr

	cout<<"befor calling reverse( )... " <<endl; 
	cout<<" the string is " <<str<<endl; 
	ptr = reverse(str); 
	cout<<"after calling reverse( )... " <<endl; 
	cout<<" the string is " <<ptr<<endl; 

	/************************************************************************
	Part B: Dynamic arrays: new, delete, and delete[]
	************************************************************************/
	int * intPtr;							//int ptr


	int i;									//loop var

	intPtr=&i;

	//Do this, see what will happen
	*intPtr = 2222;							//assignment without memory allocation

	intPtr = new int;						//dynamically allocate 1 int for intPtr
	*intPtr = 222; 
	delete intPtr;							//deallocated what was allocated for intPtr	

	intPtr = new int[10];					//dynamically allocated 10 elements for intPtr

	//get ASCII value of chars in ptr to intPtr
	for (i=0; i<10; i++)
		intPtr[i] = ptr[i]; 
	//print intPtr
	for (i=0; i<10; i++)
		cout<<*(intPtr+i) <<" "; 
	cout<<endl; 
	delete[] intPtr;						//deallocated an array of elements allocated for intPtr

	//see what will happen
	*intPtr = 4444; 

	/************************************************************************
	Part C: Arrays of pointers
	************************************************************************/
	char * chPtrs[5];						//chPtrs is an array of pointers
	int j;									//loop var
	char tmpStr[1000];						//use a temp str to get a word

	//get an array of words with variable length
	for (j=0; j<5; j++)
	{
		cout<<"enter a word => ";			//get a word
		cin>>tmpStr;									
		i=strlen(tmpStr);

		chPtrs[j] = new(nothrow) char[i];	//allocate memory for charPtr[i]
		strcpy(chPtrs[j], tmpStr);			//copy the strin
	}
	//show the words
	for (j=0; j<5; j++)
		cout<<chPtrs[j]<<endl; 

	//print the second word char by char
	cout <<"the second word chPtrs[1] is ...."<<endl; 
	for (j=0; j<strlen(chPtrs[1]); j++)
		cout<<*(chPtrs[1] + j);				//Do you understand this?
	cout<<endl; 

	//another way to print the second word
	cout<<"another way to print the second word ..."<<endl; 
	//i think the loop is crashing
	for (j=0; j<strlen(chPtrs[1]); j++)
		cout<<*(*(chPtrs + 1) + j);			//Do you understand this?
	cout<<endl; 


	/************************************************************************
	Part D: This part is for you to complete.
	(1) Implement and test a function to print a string of chars
	in reverse order. You cannot use the reverse function. You shall
	use pointer operation
	(2) Implement and test a function to read a list of words of variable lengths 
	and convert all letters to upper case. You can ask the user to enter the size of the 
	list. You shall use an array os pointers. 
	************************************************************************/
	//your code to test the functions is here. Your function definitions shall be in the header file.

	cout<<"the reverse order of str is:\n";
	//reverse ouput with pointer operation
	reversep(str);

	//creates a new list, with pointers
	list();
	return 0; 
}
