import pygame
from pygame import Rect
import random
from constants import *
from vector import Vector2



class Sprite:   
     def __init__(self,image):
          self.pos=Vector2(0,0)
          self.velocity=Vector2(0,0)
          self.oldvelocity=Vector2(0,0)
          self.image=pygame.image.load( image ).convert()
          self.width=self.image.get_width()
          self.height=self.image.get_height()
          self.radius=Vector2(self.width/2,self.height/2).norm()
          self.setrectanglecollisiontest()
          self.error=0
          self.setrandommass()
          self.drawbounds=True
     #Check if self contains any of the corners of test, and vice versa.
     #if they do not, they are not colliding
     def rectangletest(self,other):
          #if the range of numbers the x values of the rects have is less than thier widths, then they must have some intersection
          if (self.pos.x<other.pos.x and abs(self.pos.x-(other.pos.x+other.width))>self.width+other.width):
               return False
          elif (other.pos.x<self.pos.x and abs(other.pos.x-(self.pos.x+self.width))>other.width+self.width):
               return False
          elif (other.pos.y<self.pos.y and abs(other.pos.y-(self.pos.y+self.height))>other.height+self.height):
               return False
          elif (self.pos.y<other.pos.y and abs(self.pos.y-(other.pos.y+other.height))>self.height+other.height):
               return False
          else:
               return True
     def movetoboundarycircle(self,other):
          other.pos=(other.pos-self.pos).normalize()*self.radius-(self.pos-other.pos).normalize()*other.radius+self.pos
     def movetoboundaryrectangle(self,other):
          #closestcorner=min((self.pos-other.pos).normalize(),(self.pos-(other.pos+Vector2(other.width,0))).normalize(),(self.pos-(other.pos+Vector2(0,other.height))).normalize(),(self.pos-(other.pos+Vector2(other.width,other.height))).normalize())
          '''if(closestcorner==(self.pos-other.pos).normalize()):
               other.pos=Vector2(other.pos.x,self.pos.y+height)
          elif(closestcorner==(self.pos-(other.pos+Vector2(other.width,0))).normalize()):
               other.pos=Vector2(self.pos.x-other.width,other.pos.y)
          elif(closestcorner==(self.pos-(other.pos+Vector2(0,other.height))).normalize()):
               other.pos=Vecotr2(other.pos.x,self.pos.y-other.height)
          else:
               other.pos=Vector2(self.pos.x+self.width,other.pos.y)
          '''
          print "intersecting"
     def drawcollisionrectangle(self,screen):
          pygame.draw.rect(screen,(100,100,100),Rect(self.pos.x,self.pos.y,self.width,self.height))
     def drawcollisioncircle(self,screen):
          pygame.draw.circle(screen,(0,0,230),self.pos.tointegertuple(),int(self.radius))
     def circletest(self,test):
          if(self.radius+test.radius>=(self.pos-test.pos).norm()):
               return True
          else:
               return False
     def reacttocollision(self,other):
          other.oldvelocity=other.velocity
          other.velocity=(other.oldvelocity*(other.mass-self.mass)+2*other.mass*self.oldvelocity)/(self.mass+other.mass)
          self.movetoboundary(other)
     def setcirclecollisiontest(self):
          self.collisiontest=self.circletest
          self.drawcollisionbounds=self.drawcollisioncircle
          self.movetoboundary=self.movetoboundarycircle
          
     def setrectanglecollisiontest(self):
          self.collisiontest=self.rectangletest
          self.drawcollisionbounds=self.drawcollisionrectangle
          self.movetoboundary=self.movetoboundaryrectangle
     def setrandommass(self):
          self.mass=random.randrange(1,100,1)
     def setrandomtarget(self):
         self.target=Vector2(random.randrange(0,SCREENWIDTH,1),random.randrange(0,SCREENHEIGHT,1))
     def setrandomposition(self):
         self.pos=Vector2(random.randrange(0,SCREENWIDTH,1),random.randrange(0,SCREENHEIGHT,1))
     def setrandomvelocity(self):
         self.velocity=Vector2(random.uniform(0,1),random.uniform(0,1))
         self.oldvelocity=self.velocity
     def update(self,changeintime):
         self.pos=self.pos+changeintime*self.velocity
         self.velocity=self.velocity+GRAVITY
     def settargetvector(self,target):
         self.target=target
         self.velocity=self.gettargetvector().normalize()*self.velocity.norm()
     def settargettuple(self,target):
         self.settargetvector(Vector2(target[0],target[1]))
     def gettargetvector(self):
         return (self.target-self.pos).normalize()
     def draw(self,screen):
          if(self.drawbounds):
               self.drawcollisionbounds(screen)
          screen.blit( self.image, (self.pos.x,self.pos.y) )
