import pygame
import sys
import math
from pygame.locals import *
from vector import Vector2
from sprite import Sprite
from constants import *
#file names are all lowercase and only appear at top of file
#class names start with uppercase letter
#constants are all caps
def handlecollisions(carlandhisfriends,changeintime):
    handleboundarycollisions(carlandhisfriends,changeintime)
    handleobjectcollisions(carlandhisfriends,changeintime)
    
def handleboundarycollisions(carlandhisfriends,changeintime):
    for carlorafriend in carlandhisfriends:
        #check if you are gonna go through the floor
        if (carlorafriend.pos.y+carlorafriend.image.get_height()+(changeintime*carlorafriend.velocity).y>=SCREENHEIGHT):
            carlorafriend.dontupdate=True
            if(carlorafriend.velocity.y>MINIMUMVELOCITY.y):
                carlorafriend.velocity=Vector2(carlorafriend.velocity.x*.93,-carlorafriend.velocity.y*.7)
            else:
                carlorafriend.velocity=Vector2(carlorafriend.velocity.x*.93,0)
            carlorafriend.pos.y=SCREENHEIGHT-carlorafriend.image.get_height()
        #check if you are gonna go through the ceiling
        elif (carlorafriend.pos.y+(changeintime*carlorafriend.velocity).y<=0):
            carlorafriend.dontupdate=True
            if(math.fabs(carlorafriend.velocity.y)>MINIMUMVELOCITY.y):
                carlorafriend.velocity=Vector2(carlorafriend.velocity.x,-carlorafriend.velocity.y*.7)
            else:
                carlorafriend.velocity=Vector2(carlorafriend.velocity.x,0)
            carlorafriend.pos.y=0
        #check if you are gonna pass through right wall         
        elif (carlorafriend.pos.x+carlorafriend.image.get_width()+(changeintime*carlorafriend.velocity).x>=SCREENWIDTH):
            carlorafriend.dontupdate=True
            carlorafriend.velocity=Vector2(-carlorafriend.velocity.x,carlorafriend.velocity.y)
            carlorafriend.pos.x=SCREENWIDTH-carlorafriend.image.get_width()
        #check if you are gonna go through the left wall
        elif (carlorafriend.pos.x+(changeintime*carlorafriend.velocity).x<=0):
            carlorafriend.dontupdate=True
            carlorafriend.velocity=Vector2(-carlorafriend.velocity.x,carlorafriend.velocity.y)
            carlorafriend.pos.x=0
        else:
            carlorafriend.dontupdate=False
def handleobjectcollisions(carlandhisfriends,changeintime):
    for carlorafriend in carlandhisfriends:
        for afriend in carlandhisfriends:
            if(afriend!=carlorafriend and carlorafriend.collisiontest(afriend)):
                afriend.reacttocollision(carlorafriend)
                carlorafriend.dontupdate=True
            else:
                carlorafriend.dontupdate=False
def update(carlandhisfriends,changeintime):
    for carlorafriend in carlandhisfriends:
        carlorafriend.update(changeintime)

def draw(carlandhisfriends,screen):
    screen.fill((255,255,0))
    for carlorafriend in carlandhisfriends:
       carlorafriend.draw(screen)
    pygame.display.flip()       
def handleevents(carlandhisfriends):
    pygame.event.pump()
    for evt in pygame.event.get():
        if evt.type == QUIT:
            pygame.quit()
            sys.exit()
        elif evt.type == KEYDOWN and evt.key == K_ESCAPE:
            pygame.quit()
            sys.exit()
        elif evt.type == MOUSEBUTTONDOWN:
            for carlorafriend in carlandhisfriends:
                carlorafriend.settargettuple(pygame.mouse.get_pos())

def gameloop(carlandhisfriends,screen):
    starttime=pygame.time.get_ticks()
    changeintime=0
    while True:
        changeintime=pygame.time.get_ticks()-starttime
        starttime=pygame.time.get_ticks()
        handleevents(carlandhisfriends)
        handlecollisions(carlandhisfriends,changeintime)
        update(carlandhisfriends,changeintime)
        draw(carlandhisfriends,screen)



#initialize things
pygame.init()
screen = pygame.display.set_mode( (SCREENWIDTH,SCREENHEIGHT) )
pygame.display.set_caption( "hi there" )
carlandhisfriends = [];
i=0
while i<FRIENDCOUNT:
    carlandhisfriends.append(Sprite("img/snowman.jpg"))
    i=i+1

for carlorafriend in carlandhisfriends:
    carlorafriend.error=5
    carlorafriend.setrandomposition();
    carlorafriend.setrandomvelocity();
#done setting up, start game
    
gameloop(carlandhisfriends,screen)




