import math
class Vector2:
    def __init__(self,x,y):
        self.x=x
        self.y=y
    def __add__(self, argument):
        x=self.x+argument.x
        y=self.y+argument.y
        return Vector2(x,y)
    def __neg__(self):
        return Vector2(self.x*-1,self.y*-1)
    def __mul__(self,argument):
        return Vector2(self.x*argument,self.y*argument)
    def __rmul__(self,argument):
        return Vector2(self.x*argument,self.y*argument)
    def __div__(self,argument):
        return (1/float(argument))*self
    def __repr__(self):
        return str((self.x,self.y))
    def __sub__(self,argument):
        return self+-1*argument
    def norm(self):
        return math.sqrt(self.x*self.x+self.y*self.y)
    def normalize(self):
        return self/self.norm()
    def totuple(self):
        return (self.x,self.y)
    def tointegertuple(self):
        return (int(self.x),int(self.y))
