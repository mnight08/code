import pygame
import math
import sys
from pygame.locals import *
import random
SCREENWIDTH=1024
SCREENHEIGHT=786
class Vector2:
    def __init__(self,x,y):
        self.x=x
        self.y=y
    def __add__(self, argument):
        x=self.x+argument.x
        y=self.y+argument.y
        return Vector2(x,y)
    def __neg__(self):
        return Vector2(self.x*-1,self.y*-1)
    def __mul__(self,argument):
        return Vector2(self.x*argument,self.y*argument)
    def __rmul__(self,argument):
        return Vector2(self.x*argument,self.y*argument)
    def __div__(self,argument):
        return (1/float(argument))*self
    def __repr__(self):
        return str((self.x,self.y))
    def __sub__(self,argument):
        return self+-1*argument
    def norm(self):
        return math.sqrt(self.x*self.x+self.y*self.y)
    def normalize(self):
        return self/self.norm()
    
class Sprite:   
     def __init__(self,image):
         self.image=pygame.image.load( image ).convert()
         self.pos=Vector2(0,0)
         self.setrandomtarget();
         self.speed=1
         self.error=0
     def setrandomtarget(self):
         self.target=Vector2(random.randrange(0,SCREENWIDTH,1),random.randrange(0,SCREENHEIGHT,1))
     def update(self,changeintime):
         if (self.target-self.pos).norm()>self.error:
             self.pos=self.pos+changeintime*self.speed*self.gettargetvector()
         else :
             self.setrandomtarget();
     def settargetvector(self,target):
         self.target=target
     def setspeed(self,speed):
         self.speed=speed
     def settargettuple(self,target):
         self.settargetvector(Vector2(target[0],target[1]))
     def gettargetvector(self):
         return (self.target-self.pos).normalize()
     def draw(self,screen):
         screen.blit( self.image, (self.pos.x,self.pos.y) )


#lasdlfajsdlfjalskdfjlskjlfjlkjlk
pygame.init()
screen = pygame.display.set_mode( (SCREENWIDTH,SCREENHEIGHT) )
pygame.display.set_caption( "hi there" )
carlandhisfriends = [Sprite("img/brave.jpg"),Sprite("img/brave.jpg"),Sprite("img/brave.jpg")]
for carlorafriend in carlandhisfriends:
    i=1
    carlorafriend.error=5
    carlorafriend.speed=i
    starttime=pygame.time.get_ticks()
    changeintime=0
while True:
    changeintime=pygame.time.get_ticks()-starttime
    starttime=pygame.time.get_ticks()
    screen.fill((255,255,0))
    
    for carlorafriend in carlandhisfriends:
        carlorafriend.update(changeintime)
        
    for carlorafriend in carlandhisfriends:
        carlorafriend.draw(screen)
        
    pygame.display.flip()
    pygame.event.pump()
    
    for evt in pygame.event.get():
        if evt.type == QUIT:
            pygame.quit()
            sys.exit()
        elif evt.type == KEYDOWN and evt.key == K_ESCAPE:
            pygame.quit()
            sys.exit()
        elif evt.type == MOUSEBUTTONDOWN:
            for carlorafriend in carlandhisfriends:
                carlorafriend.settargettuple(pygame.mouse.get_pos())
