
this gets the steps for recipe id =1, sorted by stepno.
select s.stepno,s.text from steps as s where s.recipe_id=1 order by stepno;

this gets ingredients
select ri.amount,u.name,i.name 
from 
	ingredients as i, recipe_ingredient as ri, units_of_measure as u
	where i.id=ri.ingredient_id AND ri.recipe_id=1 and u.id=i.unit_id ;
