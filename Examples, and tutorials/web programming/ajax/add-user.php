<?
include( "db.php" );
include( "User.php" );

// POST handler
$added = false;
if( $_POST )
{
   // instantiate data class
   $postdata = new User( $_POST );

   if( $postdata->validate() )
   {
      $postdata->insert();

      $added = $postdata->user;
      $postdata = NULL;
   }
}

?>
<html>

<head>

<title>Add User</title>

<script src="add-user.js"></script>

</head>

<body>

<h1>Add User</h1>

<? if( $added ) { ?>
      <h3>User <?=$added?> successfully added</h3>
<? } ?>

<form action="add-user.php" method=POST>

<table>

  <tr><td>username:</td>
      <td><input type="textbox" name="username" id="username" value="<?=$postdata->user?>">
      <? if( $postdata->user_err ) { ?>
            <div style="color:red;" id="usernameerr"><?=$postdata->user_err?>
      <? } ?>
  </td></tr>

  <tr><td>password:</td>
      <td><input type="password" name="password" id="pw1"></td></tr>

  <tr><td>re-type password:</td>
      <td><input type="password" name="password2" id="pw2">
      <? if( $postdata->pass_err ) { ?>
            <div style="color:red;" id="pw2err"><?=$postdata->pass_err?>
      <? } ?>
	  </td>
	 
  </tr>
	  <tr id="errorrow" style="display:none;">
	  <td></td>
	  <td><div id="pw1err2" style="color:red;display:none;"> Password Must be longer than 6 characters</div></td>
	    
	  <td><div id="pw2err2" style="color:red;display:none;"> The passwords you entered do not match.  Please enter Matching passwords.</div></td>
	  </tr>
  <tr><td colspan="2"><hr><h4>User Info:</h4></td></tr>

  <tr><td>First Name:</td>
      <td><input type="textbox" name="first_name" id="fname" value="<?=$postdata->fname?>"></td></tr>

  <tr><td>Last Name:</td>
      <td><input type="textbox" name="last_name" id="lname" value="<?=$postdata->lname?>">
      <? if( $postdata->name_err ) { ?>
            <div style="color:red;" id="lnameerr"><?=$postdata->name_err?>
      <? } ?>
  </td></tr>

  <tr><td>Birthday:</td>
      <td><input type="textbox" name="birthday" id="bday" value="<?=$postdata->bday?>"></td></tr>

  <tr><td>&nbsp;</td>
      <td><input type="submit" value="Submit"></td></tr>

</form>

</body>

</html>
