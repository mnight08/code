<?
// a class
class User
{
   public $user, $pass, $fname, $lname, $bday;
   public $user_err, $pass_err, $name_err;

   public function __construct( $post_array ) {
      $this->user = $_POST['username'];
      $this->pass = $_POST['password'];
      $this->pass2 = $_POST['password2'];
      $this->fname = $_POST['first_name'];
      $this->lname = $_POST['last_name'];
      $this->bday = $_POST['birthday'];

      $this->user_err = NULL;
      $this->pass_err = NULL;
      $this->name_err = NULL;
   }

   public function validate() {

      // username isn't blank or a duplicate
      if( !$this->user ) {
         $this->user_err = "Please specify username";
      } else if( duplicate( $this->user ) ) {
         $this->user_err = "That username is already in use";
      }

      // passwords match and are at least 6 chars
      if( !$this->pass || strlen( $this->pass ) < 6 ) {
         $this->pass_err = "Password must be at least 6 characters";
      } else if( $this->pass != $this->pass2 ) {
         $this->pass_err = "Passwords do not match";
      }

      // first/last name aren't blank
      if( !$this->fname || !$this->lname ) {
         $this->name_err = "Please provide a first and last name";
      }

      return !$this->has_errors();
   }

   public function has_errors() {
      return $this->user_err || $this->pass_err || $this->name_err;
   }

   public function insert()
   {
      $sql = "
INSERT INTO users 
(username, password, first_name, last_name, birthdate)
VALUES ( '$this->user', aes_encrypt( 'The Secret Phrase', '$this->pass' ), 
'$this->fname', '$this->lname', '$this->bday' );";

      mysql_query( $sql ) or die( "Error( $sql): " . mysql_error() );
   }
}

function duplicate( $username )
{
   $sql = "SELECT id FROM users WHERE username = '$username'";

   $result = mysql_query( $sql ) or die( "Error( $sql): " . mysql_error() );

   return mysql_num_rows( $result ) > 0;
}

?>
