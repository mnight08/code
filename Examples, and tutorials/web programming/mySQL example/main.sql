



insert into recipes values (1,'Thai Shrimp and Asparagus','A simple, classy stir-fry that makes  great  use of a few high-quality ingredients.    Quick to make and surprisingly flavorful',15,20,4.5);
insert into recipes values (2,'Chicken Nepiev','Chicken, cream cheese, and garlic unite in this marriage of Chicken Neptune, Chicken Kiev, and a shorthanded pantry! Takes less fuss than either of its inspirations, and recipe can easily be increased for larger households.',40 ,65,4.2);
insert into recipes values (3,'Slow Cooker Texas Pulled Pork','Slow cooked, Texas-style pulled pork that is served on a buttered and toasted roll. My familys favorite.',15 ,315,4.6);
insert into recipes values (4,'Jessicas Steak Oscar','This is a fabulous recipe that I tried in a steakhouse and re-created the recipe. It is very rich and absolutely divine!',20,40,4.7);


insert into units_of_measure values(0,'whole');
insert into units_of_measure values(1,'pound');
insert into units_of_measure values(2,'ounce');
insert into units_of_measure values(3,'tablespoon');
insert into units_of_measure values(4,'teaspoon');
insert into units_of_measure values(5,'cup');
insert into units_of_measure values(6,'to taste');
insert into units_of_measure values(7,'packet');

insert into ingredients values(1,'shrimp',1);
insert into ingredients values(2,'asparagus',1);
insert into ingredients values(3,'dried Chinese black mushrooms',2);
insert into ingredients values(4,'vegetable oil',3);
insert into ingredients values(5,'cloves garlic',0);
insert into ingredients values(6,'oyster sauce',3);
insert into ingredients values(7,'red chili pepper',0);
insert into ingredients values(8,'Italian seasoning',3);
insert into ingredients values(9,'onion powder',3);
insert into ingredients values(10,'garlic powder',3);
insert into ingredients values(11,'skinless, boneless chicken breast halves - pounded to 1/4 inch thickness',0);
insert into ingredients values(12,'garlic flavored cream cheese spread',3);
insert into ingredients values(13,'garlic and herb seasoned dry bread crumbs',5);
insert into ingredients values(14,'butter',3);
insert into ingredients values(15,'salt and pepper',6);
insert into ingredients values(16,'(4 pound) pork shoulder roast',0);
insert into ingredients values(17,'barbeque sauce',5);
insert into ingredients values(18,'apple cider vinegar',5);
insert into ingredients values(19,'chicken broth',5);
insert into ingredients values(20,'light brown sugar',5);
insert into ingredients values(21,'prepared yellow mustard',3);
insert into ingredients values(22,'Worcestershire sauce',3);
insert into ingredients values(23,'chili powder',3);
insert into ingredients values(24,'extra large onion, chopped',0);
insert into ingredients values(25,'large cloves garlic, crushed',0);
insert into ingredients values(26,'dried thyme',4);
insert into ingredients values(27,'hamburger buns, split',0);
insert into ingredients values(28,'(7 ounce) beef tenderloin steaks',0);
insert into ingredients values(29,'butter',5);
insert into ingredients values(30,'(6 ounce) can crab, drained',0);
insert into ingredients values(31,'dry Bearnaise sauce mix',7);
insert into ingredients values(33,'whole milk',5);
insert into ingredients values(32,'vegetable oil',4);

insert into steps values(1,1,1,'Soak dried mushrooms in warm water for 15 minutes');
insert into steps values(2,1,2,'Drain mushrooms, remove stems and slice caps into thin slices');
insert into steps values(3,1,3,'Shell and devein shrimp');
insert into steps values(4,1,4,'Rinse asparagus, peel and trim stems, cut into 3-inch lengths');
insert into steps values(5,1,5,'Seed and slice red peppers');
insert into steps values(6,1,6,'Heat oil in a skillet, add garlic and cook until light brown');
insert into steps values(7,1,7,'Stir in mushroom and cook, stirring constantly, for 1 minute');
insert into steps values(8,1,8,'Add shrimp, asparagus, oyster sauce and chili peppers, and stir-fry for 3 minutes.');
insert into steps values(9,1,9,'Serve hot with steamed rice');
insert into steps values(10,2,1,'In a cup or small bowl, stir together the Italian seasoning, onion powder and garlic powder. Sprinkle over both sides of the chicken. Place 1 tablespoon of cream cheese on the center of each piece, and spread slightly. Tuck in the sides, and roll up tightly. Secure with toothpicks.');
insert into steps values(11,2,2,'Place the bread crumbs on a plate or in a shallow bowl. Roll the chicken rolls in the bread crumbs to coat. Place on a plate, cover, and freeze for about 30 minutes.');
insert into steps values(12,2,3,'Preheat the oven to 400 degrees F (200 degrees C).');
insert into steps values(13,2,4,'Heat the butter and oil in a skillet over medium-high heat. Brown the chicken rolls on all sides, this should take about 5 minutes. Transfer the rolls to a baking dish.');
insert into steps values(14,2,5,'Bake for 20 minutes in the preheated oven, or until chicken is no longer pink and the juices run clear. Spoon drippings from the dish over the rolls before serving.');
insert into steps values(15,3,1,'Pour the vegetable oil into the bottom of a slow cooker. Place the pork roast into the slow cooker; pour in the barbecue sauce, apple cider vinegar, and chicken broth. Stir in the brown sugar, yellow mustard, Worcestershire sauce, chili powder, onion, garlic, and thyme. Cover and cook on High until the roast shreds easily with a fork, 5 to 6 hours.');
insert into steps values(16,3,2,'Remove the roast from the slow cooker, and shred the meat using two forks. Return the shredded pork to the slow cooker, and stir the meat into the juices.');
insert into steps values(17,3,3,'Spread the inside of both halves of hamburger buns with butter. Toast the buns, butter side down, in a skillet over medium heat until golden brown. Spoon pork into the toasted buns.');
insert into steps values(18,4,1,'Preheat the ovens broiler and set the oven rack about 6 inches from the heat source. Sprinkle the filets with salt and pepper.');
insert into steps values(19,4,2,'In a small saucepan over low heat, melt 1/4 cup of butter; gently stir in the crab meat, and simmer while you broil the filets.');
insert into steps values(20,4,3,'Broil the filets until they reach the desired stage of doneness; for medium (slightly pink in the center), broil 10 to 12 minutes on the first side and 6 minutes on the second side. Set the filets aside to rest.');
insert into steps values(21,4,4,'Place a steamer insert into a skillet, and fill with water to just below the bottom of the steamer. Cover, and bring the water to a boil over high heat. Add the asparagus, recover, and steam until just tender, 2 to 6 minutes depending on thickness.');
insert into steps values(22,4,5,'Whisk Bearnaise sauce mix with milk in a saucepan. Add 1/2 cup of butter, and bring the sauce to a boil over medium heat, whisking constantly; reduce heat to a simmer, and whisk until sauce has thickened, about 1 minute.');
insert into steps values(23,4,6,'To assemble, top each broiled filet with crab meat, then half the asparagus; spoon Bearnaise sauce to taste over the asparagus.');



insert into recipe_ingredient values(1,1,.5);
insert into recipe_ingredient values(1,2,1);
insert into recipe_ingredient values(1,3,1);
insert into recipe_ingredient values(1,4,2);
insert into recipe_ingredient values(1,5,2);
insert into recipe_ingredient values(1,6,3);
insert into recipe_ingredient values(1,7,2);
insert into recipe_ingredient values(2,8,1);
insert into recipe_ingredient values(2,9,1);
insert into recipe_ingredient values(2,10,1);
insert into recipe_ingredient values(2,11,4);
insert into recipe_ingredient values(2,12,4);
insert into recipe_ingredient values(2,13,.25);
insert into recipe_ingredient values(2,14,1);
insert into recipe_ingredient values(2,4,1);
insert into recipe_ingredient values(2,15,1);
insert into recipe_ingredient values(3,32,1);
insert into recipe_ingredient values(3,16,1);
insert into recipe_ingredient values(3,17,1);
insert into recipe_ingredient values(3,18,.5);
insert into recipe_ingredient values(3,19,.5);
insert into recipe_ingredient values(3,20,.25);
insert into recipe_ingredient values(3,21,1);
insert into recipe_ingredient values(3,22,1);
insert into recipe_ingredient values(3,23,1);
insert into recipe_ingredient values(3,24,1);
insert into recipe_ingredient values(3,25,2);
insert into recipe_ingredient values(3,26,1.5);
insert into recipe_ingredient values(3,27,8);
insert into recipe_ingredient values(3,14,2);
insert into recipe_ingredient values(4,28,2);
insert into recipe_ingredient values(4,15,1);
insert into recipe_ingredient values(4,29,.75);
insert into recipe_ingredient values(4,30,1);
insert into recipe_ingredient values(4,2,1);
insert into recipe_ingredient values(4,31,1);
insert into recipe_ingredient values(4,33,1);


