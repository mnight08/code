req= new XMLHttpRequest();

function generatetable()
{
	document.getElementById("table");
}

function searchdatabase()
{
	val=document.getElementById("inputstring").value;

	if( val )
	{
		req.open( "GET", "databasesearch.php?" + "partialname" + "=" + val, true );
		req.onreadystatechange = function() { respond( req ); }
		req.send( null );
	}
}

function respond( req )
{
    if( req.readyState == 4 ) {
        // data is back
        if( req.status == 200 ) {
            // successful return
	    table=document.getElementById("resultstable");

	    //empty the table
	    while(table.hasChildNodes())
	    {
		table.removeChild(table.firstChild);
	    }

	    rowtemplate=document.createElement('tr');
	    datatemplate=document.createElement('td');
	    rowtemplate.appendChild(datatemplate);
	    datatemplate=datatemplate.cloneNode(true);
	    rowtemplate.appendChild(datatemplate);
	    datatemplate=datatemplate.cloneNode(true);
	    rowtemplate.appendChild(datatemplate);

	    table.appendChild(rowtemplate);

            firstnames=req.responseXML.firstChild.getElementsByTagName("firstname");
	    lastnames=req.responseXML.firstChild.getElementsByTagName("lastname");
	    usernames=req.responseXML.firstChild.getElementsByTagName("username");

	    newitem=rowtemplate.cloneNode(true);
	    newitem.getElementsByTagName("td")[0].innerHTML="USERNAME";
	    newitem.getElementsByTagName("td")[1].innerHTML="First Name";
	    newitem.getElementsByTagName("td")[2].innerHTML="Last Name";
	    
	    document.getElementById("resultstable").appendChild(newitem);
	    for(i=0;i<firstnames.length;i++)
	    {
	
	    	newitem=rowtemplate.cloneNode(true);
		newitem.getElementsByTagName("td")[1].innerHTML=firstnames[i].firstChild.nodeValue;
		newitem.getElementsByTagName("td")[2].innerHTML=lastnames[i].firstChild.nodeValue;
		newitem.getElementsByTagName("td")[0].innerHTML=usernames[i].firstChild.nodeValue;

	   	document.getElementById("resultstable").appendChild(newitem);
	    }			
			
        }
    }
}


window.onload = function() {

	document.getElementById( "inputstring" ).focus();

	document.getElementById( "inputstring" ).onkeyup = searchdatabase;
}


