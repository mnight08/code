package com.mygdx.game;
import com.badlogic.gdx.Input.TextInputListener;
import com.badlogic.gdx.math.MathUtils;

class BodyOfWater{
	Vector3 center;
	Vector2 size;
	Vector2 dimension;
	Vector2 resolution;
	mesh water;
	float[] vertices;
	short[] indices;
	float previousTime;
	float vertexSize;
	float numVertices;
	float numIndices;
	MathUtils math;
    	ShaderProgram shader;
	public BodyOfWater()
	{

		math=new MathUtils();
		previousTime=0;
		center=Vector3(0,0,0);
	 	size=Vector2(100,100);
		dimension=Vector2(10,10);
		resolution=Vector2(size[0]/dimension[0],size[1]/dimension[1]);
		numVertices=dimension[0]*dimension[1];
		numIndices=6*(n-1)*(m-1);
		mesh=new mesh(false,numVertices,numIndices,);
		vertexSize=water.getVertexSize();


		vertices=new float[dimension[0]*dimension[1]*water.getVertexSize()];

		int n=dimension[0];
		int m=dimension[1];

		//create the vertex positions.
		for(int i=0;i<dimension[0];i++)
			for(int j=0;j<dimension[1];j++){
				vertices[dimension[1]*i+j]=center[0]-size[0]/2+j*resolution[0];
				vertices[dimension[1]*i+j+1]=0;
				vertices[dimension[1]*i+j+2]=center[1]-size[1]/2+i*resolution[1];

			}

		water.setVertices(vertices);	
		
		//row and column of current box
		int row=0;
		int col=0;
		int box=0;


		//three indices for each triangle.  Mesh is made up of several 
		//squares.  Each row contains dimension[0]-1 square strips.
		//Each strip has dimension[1]-1 squares.  Each square has two triangles.
		indices=new short[numIndices];

		



		//build indices. Mesh is a collection of boxes with two triangles each.
		//There are (n-1)*(m-1) boxes since they are between the vertices.
		//There are 2(n-1)*(m-1) triangles, and so 6(n-1)*(m-1) indices needed.
		//Start counting the boxes.  from left to right at the top left.  Once you
		//hit the end, start again at the left side.  
		for(int i=0;i<numIndices;i+=3)
		{
			box=i/6;
			row=box/(m-1);
			col=box%(n-1);

			//top triangle
			indices[i]=row*(m-1)+column;
			indices[i+1]=row*(m-1)+column+1;
			indices[i+2]=(row+1)*(m-1)+column;
		
			//bottom triangle
			indices[i+3]=row*(m-1)+column+1;
			indices[i+4]=(row+1)*(m-1)+column;
			indices[i+5]=(row+1)*(m-1)+column+1;
		}


		water.setIndices(indices);


		//load our shader program and sprite batch
		try {
		//read the files into strings
    		final String VERTEX = Util.readFile(Util.getResourceAsStream("assets/vertex_shader.vert"));
    		final String FRAGMENT = Util.readFile(Util.getResourceAsStream("assets/fragment_shader.frag"));

    		//create our shader program -- be sure to pass SpriteBatch's default attributes!
		shader = new ShaderProgram(VERTEX, FRAGMENT, SpriteBatch.ATTRIBUTES);

		//Good idea to log any warnings if they exist
    		if (program.getLog().length()!=0)
        		System.out.println(program.getLog());

	    	//create our sprite batch
    		batch = new SpriteBatch(program);
		} catch (Exception e) { 
    		// ... handle the exception ... 
		}



	}

	public void update(camera cam, float delta)
	{

		//go through all visible vertices and update heights(y coordinate).
		for(int i=0;i<numVertices;i++)
		{
			if(isVisible(vertices[i*vertexSize],vertices[i*vertexSize+1],vertices[i*vertexSize+2],cam))
			{
				//update height of visible vertices.
				vertices[i*vertexSize+1]=h(vertices[i*vertexSize],vertices[i*vertexSize+2], previousTime+delta);;
			}
		}
		
		water.setVertices(vertices);
		currentTime+=delta;
	}

	public void render()
	{
		//start the shader before setting any uniforms
    		shader.begin();

    		//update the projection matrix so our triangles are rendered in 2D
    		shader.setUniformMatrix("u_projTrans", cam.combined);

    		//render the mesh
    		mesh.render(shader, GL20.GL_TRIANGLES, 0, vertexCount);

    		shader.end();



	}

	public float h(x,z,t)
	{
		return math.sin(x+z-t);
	}

	public boolean isVisible(float x, float y, float z, camera cam)
	{
		return true;
	}



}
