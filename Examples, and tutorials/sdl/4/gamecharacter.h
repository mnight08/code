#include "SDL.h"
#include "SDL_image.h"
#include <string>


//Screen attributes
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int SCREEN_BPP = 32;

//The surfaces
SDL_Surface *image = NULL;
SDL_Surface *screen = NULL;
SDL_Surface *background=NULL;
SDL_Surface *column=NULL;
SDL_Surface *verticalwall=NULL;
SDL_Surface *horizontalwall=NULL;
SDL_Surface *character=NULL;
SDL_Surface *ground=NULL;

SDL_Event event;

void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination )
{
	//Temporary rectangle to hold the offsets
	SDL_Rect offset;

	//Get the offsets
	offset.x = x;
	offset.y = y;

	//Blit the surface
	SDL_BlitSurface( source, NULL, destination, &offset );
}

bool setbackground()
{
	//setup  background
	apply_surface( 0, 0, background, screen );


	apply_surface( 50, 50, ground, screen );

	//load more of the background
	apply_surface( 20,20, column, screen );
	apply_surface( 590, 20, column, screen );
	apply_surface( 590,430, column, screen );
	apply_surface( 20, 430, column, screen );

	//load walls
	//left/right
	apply_surface( 50, 20, horizontalwall, screen );
	apply_surface( 50,430 , horizontalwall, screen );

	//top/bottom
	apply_surface( 20, 50, verticalwall, screen );
	apply_surface( 590, 50, verticalwall, screen );

	return true;
}


class gamecharacter
{
public:
	int xpos;
	int ypos;
	int mass;
	int velocityx;
	int velocityy;
	SDL_Surface* looks;
	bool movement(int x, int y,SDL_Surface* background);
	bool updateposition(SDL_Surface* background);
	gamecharacter(int x, int y, SDL_Surface* image);
};

bool gamecharacter::updateposition( SDL_Surface*background)
{
	apply_surface(xpos,ypos,looks,background);	
	
	SDL_Flip(background);
return true;
}
bool gamecharacter::movement(int x, int y, SDL_Surface* background)
{
	velocityx=x;
	velocityy=y;

	int xtemp=xpos+velocityx;
	int ytemp=ypos+velocityy;

	while(SDL_PollEvent(&event ))
	{
		//this doesnt work right.
		if(event.key.type==SDL_KEYDOWN)
		{
			xtemp=xpos+velocityx;
			ytemp=ypos+velocityy;

			while(xpos !=xtemp||ypos !=ytemp)
			{
				if(xpos!=xtemp)
					if(xpos>xtemp)
					--xpos;
					else
					++xpos;
				if(ypos!=ytemp)
					if(ypos>ytemp)
					--ypos;
					else
					++ypos;
				updateposition(background);

			}
			xpos=xpos+velocityx;
			ypos=velocityy+ypos;

			SDL_Delay(10);
			setbackground();
			updateposition(background);
	
		}
	}


	//slowdown
	while(velocityy!=0||velocityx!=0)
	{

		if(velocityx!=0)
			if(velocityx>0)
				velocityx--;
			else
				velocityx++;
		if(velocityy!=0)
				if(velocityy>0)
				velocityy--;
				else
				velocityy++;
		
		xtemp=xpos+velocityx;
		ytemp=ypos+velocityy;
		while(xpos !=xtemp||ypos !=ytemp)
		{
			if(xpos!=xtemp)
				if(xpos>xtemp)
				--xpos;
				else
					++xpos;
			if(ypos!=ytemp)
				if(ypos>ytemp)
					--ypos;
				else
					++ypos;
			updateposition(background);

		}
		xpos=xpos+velocityx;
		ypos=velocityy+ypos;

		SDL_Delay(50);
		setbackground();
		updateposition(background);


	}

	return true;
}
gamecharacter::gamecharacter(int x, int y, SDL_Surface* image)
{
	xpos=x;
	ypos=y;
	looks=image;
	velocityy=0;
	velocityx=0;

}