/*This source code copyrighted by Lazy Foo' Productions (2004-2009) and may not
be redestributed without written permission.*/

//The headers
#include "SDL.h"
#include "SDL_image.h"
#include <string>
#include "gamecharacter.h"


//The event structure that will be used


SDL_Surface *load_image( std::string filename )
{
	//The image that's loaded
	SDL_Surface* loadedImage = NULL;

	//The optimized image that will be used
	SDL_Surface* optimizedImage = NULL;

	//Load the image
	loadedImage = IMG_Load( filename.c_str() );

	//If the image loaded
	if( loadedImage != NULL )
	{
		//Create an optimized image
		optimizedImage = SDL_DisplayFormat( loadedImage );

		//Free the old image
		SDL_FreeSurface( loadedImage );
	}

	//Return the optimized image
	return optimizedImage;
}



bool init()
{
	//Initialize all SDL subsystems
	if( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
	{
		return false;
	}

	//Set up the screen
	screen = SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE );
	//staticbackground=SDL_SetVideoMode(SCREEN_WIDTH,SCREEN_HEIGHT,SCREEN_BPP,SDL_SWSURFACE);

	//If there was an error in setting up the screen
	if( screen == NULL )
	{
		return false;
	}

	//Set the window caption
	SDL_WM_SetCaption( "Event test", NULL );

	//If everything initialized fine
	return true;
}



bool load_files()
{
	//Load the image
	image = load_image( "x.png" );

	ground= load_image( "ground.png");
	background=load_image("background.png");
	column=load_image("collumns.png");
	verticalwall=load_image("left and right wall.png");
	horizontalwall=load_image("top and bottom wall.png");
	character=load_image("character.png");

	//If there was an error in loading the image
	if( image == NULL ||ground==NULL||background==NULL||column==NULL||verticalwall==NULL||horizontalwall==NULL||character==NULL)
	{
		return false;
	}

	//If everything loaded fine
	return true;
}

void clean_up()
{
	//Free the surface
	SDL_FreeSurface( image );
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!11111
	//add cleanup for other images
	//Quit SDL
	SDL_Quit();
}

int main( int argc, char* args[] )
{
	//Make sure the program waits for a quit
	bool quit = false;

	//Initialize
	if( init() == false )
	{
		return 1;
	}

	//Load the files
	if( load_files() == false )
	{
		return 1;
	}

	setbackground();

	//apply_surface(0,0,staticbackground,screen);	

	//	SDL_Flip(staticbackground);


	gamecharacter hero(305,225,character);

	apply_surface(hero.xpos,hero.ypos,hero.looks,screen);
	//Update the screen
	if( SDL_Flip( screen ) == -1 )
	{
		return 1;
	}

	

	//While the user hasn't quit
	while( quit == false )
	{
		int movementspeed=10;
		//While there's an event to handle
		while( SDL_PollEvent( &event ) )
		{
			//If the user has Xed out the window
			if( event.type == SDL_QUIT )
			{
				//Quit the program
				quit = true;
			}
			if(event.key.type==SDL_KEYDOWN)
			{
			switch( event.key.keysym.sym)
			{
			case SDLK_w:
				setbackground();
				hero.movement(0*movementspeed,-1*movementspeed,screen);
				
				//load character guy

				//hero.updateposition(screen);

				break;
			case SDLK_a:
								//setup  background
				setbackground();
				hero.movement(-1*movementspeed,0*movementspeed,screen);

				//load character guy

			//hero.updateposition(screen);
				break;
			case SDLK_s:
				setbackground();
				hero.movement(0*movementspeed,1*movementspeed,screen);

				//load character guy

				//apply_surface(hero.xpos,hero.ypos,hero.looks,screen);

				break;
			case SDLK_d:
				setbackground();
				hero.movement(1*movementspeed,0*movementspeed,screen);

				//load character guy

			//	apply_surface(hero.xpos,hero.ypos,hero.looks,screen);
				break;
			case SDLK_q:
				quit=true;
				break;
			default:
				break;

			}
			}
			SDL_Flip( screen ) ;
		}
	}

	//Free the surface and quit SDL
	clean_up();

	return 0;
}
