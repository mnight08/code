#ifndef __CONST_H__
#define __CONST_H__
//This is the constants page
//Use this page if you are going to have something that is constant and if changed, should be changed in many locations
//This prevents a lot of errors from occurring

//Screen Bits Per Pixel
const int SCREEN_BPP = 32;

//For Frame Independent Movement
const int CHARACTER_VELOCITY = 12;

//Widths and Heights
const int WORLD_WIDTH = 1080;
const int WORLD_HEIGHT = 720;
const int SCREEN_WIDTH = 720;
const int SCREEN_HEIGHT = 504;
const int IMAGE_WIDTH = 50;
const int IMAGE_HEIGHT = 85;

//Enumerators

//Direction
enum dir_t {
	DIR_NORTH = 0,
	DIR_EAST = 1,
	DIR_SOUTH = 2,
	DIR_WEST = 3
};

//Errors
enum error_t {
	ERROR_LOAD = 0,
	ERROR_MAPLOAD = 1
};
#endif
