#ifndef __CHARACTER_H__
#define __CHARACTER_H__

#include <string>
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "const.h"
#include "timer.h"

class Character 
{
public:
    //Constructor
    Character();
	//Getters/Setters
	SDL_Surface* getImage() {return images[(int)direction];};
	SDL_Surface* getImage(int dir) {return images[dir];};
	int getX() {return (int)x;};
	int getY() {return (int)y;};
	void setImage(int dir, SDL_Surface *img) {images[dir] = img;};

	//Checks if character can move, if it can, return true, otherwise false
	bool canMove() const {return (changeImage->get_ticks() > 300 ? true : false);};
	//called to destroy character
	void clean_up();

	//Handles all Input for character
	void input_handler(SDL_Event event);
	//Moves the character
    void move();
	//Gets the correct image to display
    SDL_Rect* getClip();

private:
	//Character's location (x,y)
	//Character's movement speed (xVel,yVel)
	//Character's image frame (frame)
    float x, y, xVel, yVel, frame;
	//flag for if character is moving
	bool moving;
	//Direction character is facing
	dir_t direction;
	//array[4] of SDL_Surfaces*
	SDL_Surface **images;
	//loc of image	
	SDL_Rect* clip;
	//Changing image timer
	Timer *changeImage;
};

//Handles all input that affects the character
void Character::input_handler(SDL_Event event)
{
		//If key is pressed
		if(event.type == SDL_KEYDOWN)
		{
			//Check to see if it has an effect
		    switch(event.key.keysym.sym)
		    {
		        case SDLK_UP:
						//Make character go up (negative)
						yVel -= CHARACTER_VELOCITY;
						//set direction to be north
						direction = DIR_NORTH;
						//set moving to true because he will be moving until released
						moving = true;
						//start the timer for displaying the proper image
						changeImage->start();
						break;
		        case SDLK_DOWN:
						//Make character go down (positive)
						yVel += CHARACTER_VELOCITY;
						//set direction to be south
						direction = DIR_SOUTH;
						//set moving to true because he will be moving until released
						moving = true;
						//start the timer for displaying the proper image
						changeImage->start();
						break;
		        case SDLK_LEFT:
						//Make character go left (negative)
						xVel -= CHARACTER_VELOCITY;
						//set direction to be west
						direction = DIR_WEST;
						//set moving to true because he will be moving until released
						moving = true;
						//start the timer for displaying the proper image
						changeImage->start();
						break;
		        case SDLK_RIGHT:
						//Make character go right (positive)
						xVel += CHARACTER_VELOCITY;
						//set direction to be east
						direction = DIR_EAST;
						//set moving to true because he will be moving until released
						moving = true;
						//start the timer for displaying the proper image
						changeImage->start();
						break;
		    }
		}
		//If a key was released
		else if(event.type == SDL_KEYUP)
		{
			//Check to see if it has an effect
		    switch(event.key.keysym.sym)
		    {
		        case SDLK_UP:
					//set velocity back to its original number
					yVel += CHARACTER_VELOCITY;
					//set the frame back to first image
					frame = 0;
					//character is no longer moving
					moving = false;
					break;
		        case SDLK_DOWN:
					//set velocity back to its original number
					yVel -= CHARACTER_VELOCITY;
					//set the frame back to first image
					frame = 0;
					//character is no longer moving
					moving = false;
					break;
		        case SDLK_LEFT:
					//set velocity back to its original number
					xVel += CHARACTER_VELOCITY;
					//set the frame back to first image
					frame = 0;
					//character is no longer moving
					moving = false;
					break;
		        case SDLK_RIGHT:
					//set velocity back to its original number
					xVel -= CHARACTER_VELOCITY;
					//set the frame back to first image
					frame = 0;
					//character is no longer moving
					moving = false;
					break;
		    }
		}
}

void Character::move()
{
	//If the character can move
	if(canMove()){
		//Move the character WEST-EAST
		x += xVel;

		//WEST-EAST Bound
		if(x < 0)
		    x = 0;
		else if(x + IMAGE_WIDTH > SCREEN_WIDTH)
		    x = SCREEN_WIDTH - IMAGE_WIDTH;

		//Move the character NORTH-SOUTH
		y += yVel;
		//NORTH-SOUTH Bound
		if(y < 0)
		    y = 0;
		else if(y + IMAGE_HEIGHT > SCREEN_HEIGHT)
		    y = SCREEN_HEIGHT - IMAGE_HEIGHT;

		//If character is moving
		if(moving){
			//change the image to give illusion of movement
			frame = ++frame % 4; //Thanks for pointing this out in class guys ;]
			//restart timer
			changeImage->start();
		}
	}
}

//get proper section of image based on frame
SDL_Rect* Character::getClip()
{
	clip->x = 0 + frame*IMAGE_WIDTH;
	clip->y = 0;
	clip->w = IMAGE_WIDTH;
	clip->h = IMAGE_HEIGHT;
	return clip;
}

void Character::clean_up()
{
	//Free surfaces in array
	for(int i=0;i<4;i++)
		SDL_FreeSurface(images[i]);
	//Delete pointers
	delete images;
	delete clip;
	delete changeImage;
}

//Set everything to a nice defaul to prevent segmentation faults
Character::Character()
{
	x = y = xVel = yVel = frame = 0;
	moving = false;
	direction = DIR_SOUTH;
	images = new SDL_Surface*[4];
	for(int i=0;i<4;i++)
		images[i] = NULL;
	clip = new SDL_Rect;
	changeImage = new Timer;
	changeImage->start();
}

#endif
