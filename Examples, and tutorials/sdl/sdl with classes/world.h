#ifndef __WORLD_H__
#define __WORLD_H__

#include <string>
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_ttf.h"
#include "const.h"
#include "genericsdl.h"
#include "character.h"
#include "timer.h"

class World : protected GenericSDL
{
public:
	//Constructor
	World();

	//Event Handlers
	void handle_mouse_events();

	//Initializer/Remover
	bool ini();
	void clean_up();
	//Load Files 
	bool load_files();

	//Error Handler
	void showErr(error_t);
	//runs the initializer and prepatory functions
	void start();
	//Main Engine [contains game loop]
	void startEngine();


protected:
	Character character;
	Timer delta;
	bool isRunning, hasError;
};

//Main Engine
//(The heart of it if you will)
void World::startEngine(){
	//If the world is running, place the background and character
	//This is to prevent the screen from being black for the first loop
	if(isRunning){
		apply_surface( 0, 0, background, screen );
		apply_surface(character.getX(), character.getY(), character.getImage(), screen, character.getClip());
	}
	//game loop quit
	bool quit = false;
	//Game Loop when there is an error
	//If there is an error, only allow user to exit the program
	if(hasError){
		while( quit == false ){
			while( SDL_PollEvent( &event )){
				//If the window is closed, exit game
				//Remember, SDL_QUIT is the x at the top right of the screen
		        if( event.type == SDL_QUIT)
		            quit = true;
		    }
			//This gets all the possible key states
			//Key states contains all the keys that can be pressed on the keyboard
			Uint8 *keystates = SDL_GetKeyState( NULL );
			//If ctrl+q is pressed, the user wants to end the game, so quit
			if( keystates[ SDLK_LCTRL ] && keystates[ SDLK_q ])
			    quit = true;
		}
		return;
	}
	
	//While the user hasn't quit
    while( quit == false )
    {
		delta.start();
        //while there are events
        while( SDL_PollEvent( &event ))
        {
			//If there is an event, run it through the character's input handler
			character.input_handler(event);
			//If the window is closed, exit game
			//Remember, SDL_QUIT is the x at the top right of the screen
            if( event.type == SDL_QUIT)
                quit = true;
        }
		
		//This gets all the possible key states
		//Key states contains all the keys that can be pressed on the keyboard
	    Uint8 *keystates = SDL_GetKeyState( NULL );
	    //If ctrl+q is pressed, the user wants to end the game, so quit
	    if( keystates[ SDLK_LCTRL ] && keystates[ SDLK_q ])
	        quit = true;

		//Movement for the character is dealt with in the character's move() function
		character.move();
		//Paint the Screen
		//First the background, then the character
		apply_surface( 0, 0, background, screen );
		apply_surface(character.getX(), character.getY(), character.getImage(), screen, character.getClip());

		//update the screen
		if( SDL_Flip( screen ) == -1 )
			return;
    }
}

//This starts the world
void World::start()
{
	//Make sure everything initializes
	//If not, end the program
	if(ini() == false)
		return;
	//Load all the files
	//If it fails, show the errors
	if(load_files() == false)
		showErr(ERROR_LOAD);
	//Start the game loop
	startEngine();
}

//This will print an error on a white background
//This helps you understand why it won't run
void World::showErr(error_t err)
{
	SDL_Surface *error = NULL;
	if(err == ERROR_LOAD)
	    error = TTF_RenderText_Solid(font, "World failed to load all files. Please uninstall then reinstall software.", textColor);
	else
		return;
	//Fill the screen white
	SDL_FillRect( screen, &screen->clip_rect, SDL_MapRGB( screen->format, 0xFF, 0xFF, 0xFF ) );
	//Apply the error message
	apply_surface( 210, 345, error, screen );
	//Set boolean so the proper game loop is run
	hasError = true;
	//release the surface used for the error
	SDL_FreeSurface(error);
}

bool World::load_files()
{
    //Load the background image
    background = load_image("img/dayBattle.jpg");

    //Check to make sure background is loaded properly
	//If not, return false
    if( background == NULL )
		return false;

	//Load character images
	character.setImage(0, load_image("img/char_up.png"));
	character.setImage(1, load_image("img/char_right.png"));
	character.setImage(2, load_image("img/char_down.png"));
	character.setImage(3, load_image("img/char_left.png"));

	//Check to make sure all the images were loaded properly
	//If not, return false
	for(int i=0;i<4;i++)
	    if(character.getImage(i) == NULL)
			return false;

	//Open the font that will be used for errors
    font = TTF_OpenFont("HappyPhantom.ttf", 24);

    //If everything loaded properly
    return true;
}

void World::clean_up()
{
	//Call character clean up (character.h)
	character.clean_up();
    //Call genericSDL clean up (genericsdl.h)
	GenericSDL::clean_up();
	//Quit SDL
    SDL_Quit();
}

bool World::ini()
{
	//Call the genericSDL initializer (genericsdl.h)
	//If it returns false, return false
	if(GenericSDL::ini(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP) == false)
		return false;
    //Set the window caption
    SDL_WM_SetCaption( "Walking Man With Classes", NULL );

	
    //If everything initialized fine
    return true;
}

World::World() : GenericSDL()
{
	//The world should start as running
	isRunning = true;
	//The world should start as not having an error
	hasError = false;
}
#endif
