//GenericSDL.h created by Alexander Wilson
//This is a generic SDL class that contains everything you need to create a simple screen with a background
//This class requires other classes to actually function how it was designed
//GenericSDL is usually inherited by another class for complete functionality
//Suggestion: inherit as protected, since all aspects are public

#ifndef __GENERIC_SDL_H__
#define __GENERIC_SDL_H__

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_ttf.h"

class GenericSDL
{
public:
	//Constructor/Destructor
	GenericSDL();
	~GenericSDL();

	//Initializer/Clean Up
	bool ini(int SCREEN_WIDTH, int SCREEN_HEIGHT, int SCREEN_BPP);
	void clean_up();

	//Image based Functions
	void apply_surface(int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip = NULL);
	SDL_Surface* load_image(std::string filename);

	//Event to track all interactions
	SDL_Event event;

	//Default Screen and generic background
	SDL_Surface *background;
	SDL_Surface *screen;

	//Font and related text color
	TTF_Font *font;
	SDL_Color textColor;
};

bool GenericSDL::ini(int SCREEN_WIDTH, int SCREEN_HEIGHT, int SCREEN_BPP)
{
	//Initialize all SDL subsystems
    if(SDL_Init(SDL_INIT_EVERYTHING) == -1)
        return false;

    //Set up the screen
    screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE);

    //If screen failed to initialize, return false
    if(screen == NULL)
        return false;	//If initialization of the font failed, return false
    if(TTF_Init() == -1)
        return false;

	return true;
}

//Apply Surface places an image (source) on another surface (destination)
//Originally created on lazyfoo.net
void GenericSDL::apply_surface(int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip)
{
    //Holds starting points
    SDL_Rect offset;

    //Set points
    offset.x = x;
    offset.y = y;

    //Add to Screen
    SDL_BlitSurface( source, clip, destination, &offset );
}

//This function loads an image, optimizes it, then returns the optimized image
//Originally created on lazyfoo.net
SDL_Surface* GenericSDL::load_image(std::string filename)
{
	//declare variables used to optimize the image
    SDL_Surface* loadedImage = NULL;
    SDL_Surface* optimizedImage = NULL;

	//Load initial image
    loadedImage = IMG_Load(filename.c_str());

	//If image loaded
    if(loadedImage != NULL){
        //Optimize The Image (All the magic is HERE)
        optimizedImage = SDL_DisplayFormat(loadedImage);
		//Release the old image
        SDL_FreeSurface(loadedImage);
		//If the optimization succeeded
		//Set the white background to be see through
        if(optimizedImage != NULL)
            SDL_SetColorKey(optimizedImage, SDL_SRCCOLORKEY, SDL_MapRGB(optimizedImage->format, 255, 255, 255));
    }
	//send back the optimized image
    return optimizedImage;
}

void GenericSDL::clean_up()
{
    //release surfaces
	SDL_FreeSurface(screen);
    SDL_FreeSurface(background);
	//Release Font
    TTF_CloseFont(font);
}

//Constructor
GenericSDL::GenericSDL()
{
	//set everything so there are no stray pointers
	background = NULL;
	screen = NULL;
	font = NULL;
	textColor = (SDL_Color){0, 0, 0};	
}

GenericSDL::~GenericSDL()
{
	//
}

#endif
