#include <string>
#include "world.h"

//This is the standard main function
//The arguments are not used, and you don't need to worry about them
int main( int argc, char* args[] )
{
	//World is where everything is going to be happening
	World world;
	//Start the world
	//If you are looking for the game loop, go to world.h and look for this line
	//World::startEngine()
	world.start();
	return 0;
}
