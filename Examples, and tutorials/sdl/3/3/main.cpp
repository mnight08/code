#include "SDL.h"
#include <string>

const int screenwidth=640;
const int screenheight=480;
const int screenbpp=32;
	
SDL_Surface *message=NULL;
SDL_Surface *background=NULL;
SDL_Surface *screen=NULL;

SDL_Surface *load_image(std::string filename)
{
	SDL_Surface *loadedimage=NULL;

	SDL_Surface* optimizedimage=NULL;

	loadedimage=SDL_LoadBMP(filename.c_str());

	if(loadedimage!=NULL)
	{
		optimizedimage=SDL_DisplayFormat(loadedimage);

		SDL_FreeSurface(loadedimage);


	}

	return optimizedimage;

}

void apply_surface(int x, int y, SDL_Surface* source, SDL_Surface* destination)
{
	SDL_Rect offset;

	offset.x=x;
	offset.y=y;
	
	SDL_BlitSurface(source, NULL, destination,&offset);

}



int main(int argc, char* args[])
{

	
if(SDL_Init( SDL_INIT_EVERYTHING)==-1) 
{
	return 1;
}
	
	screen=SDL_SetVideoMode(screenwidth,screenheight, screenbpp,SDL_SWSURFACE);
	if(screen==NULL)
		return 1;

	SDL_WM_SetCaption("Hello world sucks", NULL);

	message=load_image("image1.bmp");
	background=load_image("background.bmp");

	apply_surface(0,0,background,screen);
	apply_surface(320,0,background,screen);
apply_surface(320,240,background,screen);
apply_surface(0,240,background,screen);

apply_surface(180,140, message,screen);

if(SDL_Flip(screen)==-1)
return 1;

SDL_Delay(20000);

SDL_FreeSurface(message);
SDL_FreeSurface(background);
SDL_FreeSurface(screen);

SDL_Quit();

return 0;

}