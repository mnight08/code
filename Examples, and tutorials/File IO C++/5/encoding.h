#ifndef encoding
#define encoding
#include "linkedlist.h"
#include "stack.h"
#include <cstring>
#include <fstream>
#include <iostream> 
//very basic class, havent thought of what would be nice to add, so at this point it will only do what is needed
//this wont work so well as an independent class unless i actualy derive from the fstream, which would be overkill for what is really needed
//it would be easy to add this as a member of fstream, ill just define most of the stuff in constructor
class encoded
{
private :
	//a stack to reverse word,  i think this could have been done with a character array, and a few pointers, but this was cleaner, and suggested
	linkedListStackType<char> reverse;
	//the file to output to
	ofstream file;
	//file to get stuff from
	ifstream f;
public:

	//default constructor
	encoded();
};

encoded::encoded()
{	
	//it took me a while to realize you could do this
	char* filen=new char[100];
	cout<<"enter a filename, only text files, no extension please"<<endl;



	cin>>filen;


	//add .txt exension
	f.open(strcat(filen, ".txt"));
	//get rid of txt extension
	filen[strlen(filen)-3]=0;

	//check to see if you were passed a file, if not, then exit
	if(!f.is_open())
	{
		cout<<"i need to know which file to work with"<<endl;
		return;
	}

	//open file with same name, but .ecd instead,  ecd stands for encoded.
	file.open(strcat(filen,"ecd"), ios::trunc);


	//to initialize g
	char g;
	f.get(g);

	//loop through file
	for(;f; f.get(g))
	{

		//manage characters
		if(int(g)>64&&int(g)<91||int(g)>96&&int(g)<123)
		{
			//probably not the most elegant way to get this done, but it should work
			//test to see if last character in range
			if(g==char(90))
				reverse.push(char(65));
			else if(g==char(122))
				reverse.push(char(97));
			else
				reverse.push(char(g+1));
		}
		//manage numbers
		else if(int(g)>47&&int(g)<58)
		{	
			//check if number is at edge, to you dont go into wrong characters
			if(g==char(48))
				reverse.push(char (57));
			else
				reverse.push(char(g-1));

		}
		//just ouput everything else straight to the file
		else
		{
			while(!reverse.isEmpty())
			{
				file<<reverse.pop();
			}
			file<<g;
		}
	}

	//output any remaining words in stack
	while(!reverse.isEmpty())
	{
		file<<reverse.pop();
	}

	//close files, i hadnt realized you could have two files open at same time til this.
	f.close();
	file.close();

	//reallocate char array.
	delete[] filen;
}

#endif