//-----------------------------------------------------------------
// Author:	alex martinez
// Class:	CSCI/CMPE 2380.01, spring, 2009
// hwk 4:	encoding/decoding a file
// Date:	4/16/09
//------------------------------------------------------------------
#include <iostream> 
#include "encoding.h"
#include "decoding.h"
#include <string>
using namespace std;



//i dont know whethere i should make a encoding/ decoding class, or just main
int main()
{
	//this is really pointless to have as a class, considering it does not even create an object, only calls one function, 
	//but it felt cleaner having this one have total control over stack, and such.  ideally i would want to derive this from fstream, or just define it as a member function, but it seems like too much
	//trouble, it may have been easier to just make this  function, but i was already close to finishing the class as it is.  also it hides the files, and stack.  i tried to
	//pass  ifstream &f  as a parameter for a constructor, but it gave me an error saying i was redefining f, not sure why though, so i just gave the class full control over the files
	//it seems to work well, maybe i should get a list of all characters, and have it put out again
	//this will probably be hard to read. after each call, it goes to default constructor
	encoded();
	decoded();
	//in case you were confused, the input file is the hello world lab
	//fileout.txt is the input file,  fileout.ecd is the encoded file,  output.txt is the output to screen

	return 0;
}

