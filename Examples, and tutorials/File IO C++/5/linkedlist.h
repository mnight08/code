/****************************************************************************************
*  Program Name:		lab2380_20_head.h
*  Lab 20:				Header file for Lab 20
*						- defining the linked list based list
*  Date:				01/15/2009
*
*  Description: This file contains the prototype of the class linkedListType
*  and the linked-list's node struct definition.
*****************************************************************************************/
#include <iostream>
#include <fstream>
#include <cassert>

using namespace std; 

#ifndef H_linkedListType
#define H_linkedListType

template <class Type> 
struct nodeType
{
	Type info;
	nodeType<Type> *link;
};

template <class Type> 
class linkedListType
{
public:
	const linkedListType<Type> & operator=	// overloading assignment operator
		(const linkedListType<Type>&); 
	void initializeList();					// initialize the list to an empty list
	bool isEmpty();							// chech whether list is empty
	bool isFull(){return false;}			// assume memory is always available
	int  length();							// read the number of nodes in list
	void destroyList();						// to delete all nodes from the list
	Type front();							// return the first element in the list
	Type back();							// return the last element in the list
	bool search(const Type & searchItem);	// to determine the searchItem is in the list
	// return true if yes or no otherwise
	void insertFirst(const Type & newItem);	// insert the newItem to the beginning of the list
	void insertLast(const Type & newItem);	// insert the newItem to the end of the list
	void deleteNode(const Type & deleteItem); // delete the node containing the input item from the list
	void print();							// print the list  

	linkedListType();						// default constructor
	linkedListType(const linkedListType<Type>&);// copy constructor 
	~linkedListType();						// the destructor

protected: 
	int count;								// store the number of nodes in the list
	nodeType<Type> *first;					// the pointer to the first node
	nodeType<Type> *last;					// the pointer to the last node

private: 
	void copyList(const linkedListType<Type> & otherList); 
	//copy other list to the invoking list
};

// print the list
template <class Type>
void linkedListType<Type>::print( )
{
	nodeType<Type> *current;				//point to current node
	current = first; 
	int flag = 1; 

	while(current != NULL)
	{
		cout<<setw(10) <<left <<current->info<<"  "; 
		if (flag % 5 == 0)					//print five element per line
			cout << endl; 
		flag++; 
		current = current->link; 
	}

	cout<<endl; 
}

// initialize the list to an empty list
template <class Type> 
void linkedListType<Type> :: initializeList()
{
	destroyList();							//destroy the list to an empty one
}

// chech whether list is empty
template <class Type> 
bool linkedListType<Type>::isEmpty()
{
	return count == 0; 
}

// read the number of nodes in list
template <class Type>
int  linkedListType<Type>::length()
{
	return count; 
}

// to delete all nodes from the list
template <class Type>
void linkedListType<Type>::destroyList()
{
	nodeType<Type> *tmp;			//local node ptr to help to delete a node

	while (first!=NULL)				//loop until no more nodes are in the list
	{
		tmp = first;				//get the current node
		first = first->link;		//move to the next node
		delete tmp;					//delete the current node
	}

	first = last = NULL; 
	count = 0; 
}

// return the first element in the list
template <class Type> 
Type linkedListType<Type>::front()
{
	assert(first!=NULL);		//to make sure the list is not empty
	return first->info; 
}

// return the last element in the list
template <class Type> 
Type linkedListType<Type>::back()
{
	assert(last!=NULL);			//to make sure the list is not empty
	return last->info; 
}

// to determine the searchItem is in the list
// return true if yes or no otherwise
template <class Type> 
bool linkedListType<Type>::search(const Type & searchItem)
{
	nodeType<Type> *current;	//ptr to current node 
	bool found = false;			//flag for founding status

	current = first;			//point to first
	while (!found && current!=NULL)		
	{
		if (current->info == searchItem)
			found = true;		//find the searchItem
		else					//otherwise move to the next node
			current = current->link; 
	}

	return found; 
}

// insert the newItem to the beginning of the list
template <class Type>
void linkedListType<Type>::insertFirst(const Type & newItem)
{
	nodeType<Type> *newNode;		//for creating a new new
	newNode = new nodeType<Type>;	//create a new node

	assert(newNode != NULL);		//make sure the new node in indeed created

	newNode->info = newItem;		//load info to new node	 
	newNode->link = first;			//add new node the front of the list
	first = newNode;				//reset first 
	count++;						//increase the count 
	if (last == NULL)				//reset last if it is empty
		last = newNode; 
}

// insert the newItem to the end of the list
template <class Type> 
void linkedListType<Type>::insertLast(const Type & newItem)
{
	nodeType<Type> *newNode;		//for creating a new new
	newNode = new nodeType<Type>;	//create a new node

	assert(newNode != NULL);		//make sure the new node in indeed created

	newNode->info = newItem;		//load info to new node	 
	newNode->link = NULL;			//make sure the new node is the last one
	count++;						//increase the count 
	if (first == NULL)				//when the list is empty
	{
		first = last = newNode; 
	}
	else 
	{
		last->link = newNode; 
		last = newNode;
	}
}

// delete the node containing the input item from the list	
template <class Type>
void linkedListType<Type>::deleteNode(const Type & deleteItem)
{
	nodeType<Type>	*current,			//point to current node
		*previous;			//point to next node
	bool found;							//flag for finding the node containing deleteItem

	//case 1: the list is empty
	if(isEmpty())
		return; 
	//case 2: the first is to be deleted
	else if (first->info == deleteItem)
	{
		current = first->link;			//get the second node if there is one
		delete first;					//delete the first node
		if (current == NULL)			//there is only one node
		{
			first = last = NULL; 
		}	
		else
		{
			first = current; 
		}
		count--; 
		return; 
	}
	//case 3: find the node after the first one
	else 
	{
		previous = first; 
		current = first->link;  
		found = false; 
		while (!found && current!=NULL)
		{
			if (current->info == deleteItem)
			{
				found = true; 
			}
			else
			{
				previous = current; 
				current = current->link; 
			}
		}
		if (found)
		{
			previous->link = current->link;	//skip the current node
			count--; 
			if (last == current)			//delete the last node
				last = previous; 
			delete current; 
		}	
	}
}

// default constructor	
template <class Type>
linkedListType<Type>::linkedListType()
{
	count = 0; 
	first = last = NULL;
}

// copy constructor 
template <class Type>
linkedListType<Type>::linkedListType(const linkedListType<Type>& otherList)
{
	first = NULL; 
	copyList(otherList); 
}

//destructor
template <class Type>
linkedListType<Type>::~linkedListType()
{
	destroyList(); 
}

//copy list
template <class Type>
void linkedListType<Type>::copyList(const linkedListType<Type> & otherList)
{
	nodeType<Type>	newNode,	//for creating a new node 
		current;	//point to current node


	if (!isEmpty())				//if list is not empty then destroy it
		destroyLisy(); 

	if (otherList.isEmpty())	//otherList is empty
	{
		first = last = NULL; 
		count = 0; 
		return; 
	}
	count = otherList.count;	//copy the count
	//otherList is not empty. first take care of the first node	
	first = new nodeType<Type>;
	first->info = otherList.first->info; 
	first->link = NULL; 
	last = first; 
	//now take care of additional nodes if any
	current = otherList.first->link;  
	while (current != NULL)
	{
		newNode = new nodeType<Type>; 
		newNode -> info = current->info; 
		newNode -> link = NULL; 
		last->link = newNode; 
		last = newNode; 

		current = current->link; 
	}
}

// overloading assignment operator
template <class Type>
const linkedListType<Type>& 
linkedListType<Type>::operator=(const linkedListType<Type>& otherList)
{
	if (this != & otherList)
	{
		copyList(otherList);
	}
	return *this; 
}

#endif
