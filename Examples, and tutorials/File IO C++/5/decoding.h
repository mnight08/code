#ifndef decoding
#define decoding
#include <fstream>
#include <iostream> 
//very basic class, havent thought of what would be nice to add, so at this point it will only do what is needed
//this is actually a copy of encoding class, just changed a few - to +, and whihc item to insert if at bounds, and stuff like that
class decoded
{
private :
	//here for the same reasons as in encoding.h
	linkedListStackType<char> reverse;
	ofstream file;
	ifstream f;
public:
	//default constructor
	decoded();
};

decoded::decoded()
{

	char* filen=new char[100];
	cout<<"enter a file name, only encoded files, no extensions please."<<endl;



	cin>>filen;
	f.open(strcat(filen, ".ecd"));	

	if(!f.is_open())
	{
		cout<<"i need to know which file to work with"<<endl;
		return;
	}

	filen[strlen(filen)-3]=0;		//this might work better if i put a loop to delete from back til at the '.'
	file.open(strcat(filen, "txt"), ios::trunc);
	//check to see if you were passed a file, if not, then exit

	char g;
	f.get(g);
	for(;f; f.get(g))
	{

		//manage characters
		if(int(g)>64&&int(g)<91||int(g)>96&&int(g)<123)
		{
			//probably not the most elegant way to get this done, but it should work
			//test to see if last character in range
			if(g==char(65))
				reverse.push(char(90));
			else if(g==char(97))
				reverse.push(char(122));
			else
				reverse.push(char(g-1));
		}
		//manage numbers
		else if(int(g)>47&&int(g)<58)
		{	
			//check if number is at edge, to you dont go into wrong characters
			if(g==char(57))
				reverse.push(char (48));
			else
				reverse.push(char(g+1));

		}
		//just ouput everything else straight to the file
		else
		{
			while(!reverse.isEmpty())
			{
				file<<reverse.pop();
			}
			file<<g;
		}
	}
	while(!reverse.isEmpty())
	{
		file<<reverse.pop();
	}

	f.close();
	file.close();
}

#endif