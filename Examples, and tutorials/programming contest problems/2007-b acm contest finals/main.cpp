#include <iostream>
using namespace std;
#include <vector>
#include <stack>
#include <string>
#include <algorithm>
#include <list>

//quicksort
void sortstacks(int begin, int end, vector<stack<char>> &stacks)
{
	if(end-begin<=1)
		return;
	else
	{
		list<stack<char>>	temps;
		list<stack<char>>	templ;
		int pivot= (end +begin)/2;
		for (int i=begin;i<end;i++)
		{
			if(stacks[i].top()<stacks[pivot].top())
			{
				temps.push_back(stacks[i]);
			}
			else
			{
				templ.push_back(stacks[i]);
			}
		}
		int i=0;
		for(std::list<stack<char>>::iterator itr=temps.begin();itr!=temps.end();itr++,i++)
		{
			stacks[i]=(*itr);
		}

		for(std::list<stack<char>>::iterator itr=templ.begin();itr!=templ.end();itr++,i++)
		{
			stacks[i]=(*itr);
		}

		sortstacks(begin,begin +temps.size(),stacks);
		sortstacks(begin+temps.size()+1,end,stacks);
	}

}

int main()
{
	vector<string> containers;
	containers.resize(1000);
	for(int i=0;i<1000;i++)
	{
		containers[i]="ACMICPC";
	}


	bool found=false;
	for(int k=0;k<containers.size();k++){
		{
			vector<stack<char>> stacks;
			for(int i=0;i<containers[k].size();i++)
			{
				//sort according to the top of each stack
				sortstacks(0,stacks.size(),stacks);
				//iterate though the vector til it finds a letter that is greater than or equal to it, the sorting is neccesary to make sure this works right.
				for( int j=0;j<stacks.size();j++)
				{
					if(containers[k][i]<=stacks[j].top()){
						stacks[j].push(containers[k][i]);
						found =true;
						break;
					}

				}
				if(found==false)
				{
					stack<char> temp;
					temp.push(containers[k][i]);
					stacks.push_back(temp);
				}
				else found=false;
			}
			cout<<stacks.size();
		}
	}

}