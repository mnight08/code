// generate anagrams.cpp : main project file.
#include <iostream>
#include <string>

using namespace std;

void generateanagrams(string s, string prefix)
{
	if(s.size()==0)
	{
		prefix+=s;
		std::cout<<prefix<<endl;
	}
	else
	{
		string temp1,temp2;
		for(int i=0;i<s.size();i++)
		{
			temp1=prefix;
			temp1+=s[i];
			temp2=s;
			temp2.erase(i,1);
			generateanagrams(temp2,temp1);

		}

	}
}

int main()
{

	string input="cake";
	generateanagrams(input,"");
	cin>>input;
    return 0;
}
