#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

struct word
{
	string w;
	int weight;

};


struct query
{
	vector<word> words;

};

struct page
{
	vector<word> words;

};
//linear search
int search(vector<word> words, word target)
{
	for(int i=0;i<words.size();i++)
	{
		if(words[i].w==target.w)
			return i;
	}
	return -1;
}

int strength(page& p, query& q, int N)
{	
	vector<int> temp;
	temp.resize(N);
	for(int i=0;i<N;i++)
	{
		if(!(search(p.words,q.words[i])<0))
			temp[i]=(N-i)*p.words[search(p.words,q.words[i])].weight;
	}

	for(int i=0;i<N;i++)
	{
		int sum=0;
		sum=temp[i]+sum;
	}
	return sum;
}

int main()
{
	
	return 0;
}