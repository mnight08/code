#include <vector>
#include <iostream>
#include <math.h>
#include <fstream>
#include <algorithm>
using namespace std;

//if the line is horizontal then l is the y coordinate else l is the x coordinate
struct line
{

	double/* sx,sy,*/ex,ey;
	bool operator <(line t){if(ex<t.ex)return true; else if(ex>t.ex) return false; else if(ey<t.ey) return true; else return false;}
};

//some errors wheere dividing by zero
void normalizelines(vector<line> &lines)
{
	if(lines.size()<=0)return;
	double minx=/*min((*lines.element()).sx,*/lines[0].ex;
	double miny=/*min((*lines.element()).sy,*/lines[0].ey;

	for(int i= 0;i<lines.size();i++)
	{
		minx=min(/*min((*/minx, lines[i].ex/*),(*it).sx*/);
	}

	for(int i= 0;i<lines.size();i++)
	{
		miny=min(/*min((*/miny, lines[i].ey/*),(*it).sy*/);
	}
	for(int i= 0;i<lines.size();i++)
	{
		/*(*it).sx=((*it).sx+101)/(minx+101);*/lines[i].ex=(lines[i].ex+101)/(minx+101);
		/*(*it).sy=((*it).sy+101)/(miny+101);*/lines[i].ey=(lines[i].ey+101)/(miny+101);
	}
}
/*
double getdistance(vector<line>::Iterator it, vector<line>::Iterator c)
{

if((*it).ishorizontal&&(*c).ishorizontal||!(*it).ishorizontal&&!(*c).ishorizontal)
{
return abs((*it).l-(*c).l);
}
else return 0;

}

void getdistances(vector<line> &lines,vector<double> & distances,vector<line>::Iterator c)
{
//iterate through the vector of lines, and put the distances into distances
for(vector<line>::Iterator it= lines.element();it!=lines.end();it++)
{
if(it!=c)
{
distances.push_back(getdistance(it, c));
}
}
}*/
void sortlines(vector<line>& lines)
{
	sort(lines.begin(),lines.end());
}

bool issubset(vector<line> &imagelines,vector<line> &subimagelines)
{
	return false;
}
void loadimage(vector<line> &imagelines, int numberoflines,ifstream &input)
{
	imagelines.clear();
	line temp;
//	ifstream input;input.open("input.txt");
	for(int i=0;i<numberoflines;i++)
	{
		input>>temp.ex>>temp.ey;
		imagelines.push_back(temp);
	}
}
//this is a decision problem, might be able to just normalize the line positions, and 
int main(int argc, char** argv)
{
	int k=0;
	int numberofimagelines=0;
	int numberofsubimagelines=0;
	//
	vector<line> imagelines;
	vector<line> subimagelines;
	ifstream input;input.open("input.txt");
	while(true)
	{
		k++;
		input>>numberofimagelines;
		if(numberofimagelines==0)
			break;
		input>>numberofsubimagelines;


		loadimage(imagelines,numberofimagelines,input);
		loadimage(subimagelines,numberofsubimagelines,input);

		normalizelines(imagelines);
		normalizelines(subimagelines);

		cout<<"Case "<<k<<": ";
		if(issubset(imagelines,subimagelines))
		{
			cout<<"Possible"<<endl<<endl;
		}
		else
		{
			cout<<"IMPOSSBLE!!!!"<<endl<<endl;
		}
	}
	return 0;
}