#pragma once
#include "card.h"
#include <algorithm>

const int ace=1;
const int two=2;
const int three=3;
const int four=4;
const int five=5;
const int six=6;
const int seven=7;
const int eight=8;
const int nine=9;
const int ten=10;
const int jack=11;
const int queen=12;
const int king=13;
const int spade=0;
const int clover=1;
const int diamond=2;
const int heart=3;

class hand
{
public:

	bool suits[4];	//0=spades, 1=diamonds, 2=hearts, 3=clovers //if more than one is true, that means no flush
	card cards[5];
	//the type of hand ie flush, straight
	int absoluterank;	
	//the relative rank within its own type
	int relativerank;

	bool isroyalflush();	//10
	bool isstraightflush();	//9
	bool isfourofakind();	//8
	bool isfullhouse();		//7
	bool isflush();		//6
	//there will need to be a special case for the a
	bool isstraight();		//5
	bool isthreeofakind();	//4
	bool istwopair();		//3
	bool isonepair();		//2
	bool isnothing();		//1
	bool isroyal();
	bool isroyal(card);
	void setrank();
	hand(void);
	~hand(void);
};
