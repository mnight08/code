#pragma once
#include "player.h"
#include "hand.h"
#include "card.h"
#include <vector>

//only works for 52 card deck game
class game
{
public:
	int playercount;		//player count will be from 2 to 5, one will always be the dealer.//for now i will focus on one player
	vector<player> players;	//im not sure if i should allow betting at this point, doesnt seem like it would be hard to do, but might make a mess of other things
//	int pool;
	//	bool placebet(int amount);
//	card deck[52];
	bool used[52];		//this list the cards that have already been used, and as such, the cards that havent 
	bool swapped[52];	//list the cards that have been swapped out of a players hand;
	int cardsindeck;
	void builddeck();
	void startgame();
	void deal();
	

	game(void);
	~game(void);
};
