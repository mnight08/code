#include "hand.h"
bool hand::isstraight()
{
	card set[5];
	for(int i=0;i<5;i++)
		set[i]=cards[i];
	std::sort(set,set+5);
	for(int i=0;i<4;i++)
	{
		if((set[i+1].type-set[i].type)!=1)
			return false;
	}
	relativerank=set[4].type;
	return true;
}
bool hand::isroyal()
{
	card set[5];
	for(int i=0;i<5;i++)
		set[i]=cards[i];
	std::sort(set,set+5);
	return ((set[0].type==1)&&(set[1].type=10)&&(set[2].type==11)&&(set[3].type==12)&&(set[4].type==13));
}

bool hand::isstraightflush()
{
	card set[5];
	for(int i=0;i<5;i++)
		set[i]=cards[i];
	std::sort(set,set+5);
	if(isstraight()&&isflush())
	{
		relativerank=set[4].type;
		return true;
	}
	return false;
}
bool hand::isroyalflush()
{
	
	if( (isflush()&&isstraight()&&isroyal()))
	{
		relativerank=1;//all royal flushes are equal
		return true;
	}
	return false;
}
bool hand::isflush()
{
	card set[5];
	for(int i=0;i<5;i++)
		set[i]=cards[i];
	std::sort(set,set+5);

	for(int i=0;i<5;i++)
		suits[cards[i].suit]=true;
	if ((suits[0]&&!suits[1]&&!suits[2]&&!suits[3])||(!suits[0]&&suits[1]&&!suits[2]&&!suits[3])||(!suits[0]&&!suits[1]&&suits[2]&&!suits[3])||(!suits[0]&&!suits[1]&&!suits[2]&&suits[3]));	
	{
		relativerank=set[4].type*10000000+set[3].type*1000000+set[2].type*10000+set[1].type*100+set[0].type;
		return true;
	}
	else return false;
}


bool hand::isfourofakind()
{
	card set[5];
	for(int i=0;i<5;i++)
		set[i]=cards[i];
	std::sort(set,set+5);
	if((set[0].type==set[1].type&&set[1].type==set[2].type&&set[2].type==set[3].type&&set[3].type==set[4].type||set[1].type==set[2].type&&set[2].type==set[3].type&&set[3].type==set[4].type&&set[4].type))
	{
		relativerank=set[3].type;
		return true;
	}
	return false;
}

bool hand::isfullhouse()
{
	card set[5];
	for(int i=0;i<5;i++)
		set[i]=cards[i];
	std::sort(set,set+5);
	
	if(set[0].type==set[1].type&&set[1].type==set[2].type&&set[3].type==set[4].type)
	{
		relativerank=set[0].type*100+set[4].type;
		return true;
	}
	else if(set[0].type==set[1].type&&set[2].type==set[3].type&&set[3].type==set[4].type)
	{
		relativerank=set[4].type*100+set[0].type;
		return true;
	}
	else return false;
}

bool hand::isonepair()
{
	card set[5];
	for(int i=0;i<5;i++)
		set[i]=cards[i];
	std::sort(set,set+5);

	if(set[0].type==set[1].type){
		relativerank=set[0].type*1000000+set[4].type*10000+set[3].type*100+set[2].type;
		return true;
	}
	else if(set[1].type==set[2].type){
		relativerank=set[1].type*1000000+set[4].type*10000+set[3].type*100+set[0].type;
		return true;
	}
	else if(set[2].type==set[3].type){
		relativerank=set[2].type*1000000+set[4].type*10000+set[1].type*100+set[0].type;
		return true;
	}
	else if(set[3].type==set[4].type){
		relativerank=set[3].type*1000000+set[2].type*10000+set[1].type*100+set[0].type;
		return true;
	}
	else return false;
}

bool hand::isthreeofakind()
{
	card set[5];
	for(int i=0;i<5;i++)
		set[i]=cards[i];
	std::sort(set,set+5);
	if(set[0].type==set[1].type&&set[1].type==set[2].type||set[1].type==set[2].type&&set[2].type==set[3].type||set[2].type==set[3].type&&set[3].type==set[4].type)
	{
		relativerank=set[2].type;
		return true;
	}
	else return false;
}

hand::hand(void)
{
}

hand::~hand(void)
{
}

bool hand::istwopair()
{
	card set[5];
	for(int i=0;i<5;i++)
		set[i]=cards[i];
	std::sort(set,set+5);

	if(set[0].type==set[1].type&&set[2].type==set[3].type){
		relativerank=set[2].type*10000+set[0].type*100+set[4].type;
		return true;
	}
	else if(set[1].type==set[2].type&&set[3].type==set[4].type){
		relativerank=set[4].suit*10000+set[2].type*100+set[0].type;
		return true;
	}
	return false;
}

bool hand::isnothing()
{
	card set[5];
	for(int i=0;i<5;i++)
		set[i]=cards[i];
	std::sort(set,set+5);
	relativerank=set[4].type*100000000+set[3].type*1000000+set[2].type*10000+set[1].type*100+set[0].type;
	return true;
}


void hand::setrank()
{
	if(isroyalflush())
	absoluterank=10;
	else if(isstraightflush())
		absoluterank=9;
	else if(isfourofakind())
		absoluterank=8;
	else if(isfullhouse())
		absoluterank=7;
	else if(isflush())
		absoluterank=6;
	else if(isstraight())
		absoluterank=5;
	else if(isthreeofakind())
		absoluterank=4;
	else if(istwopair())
		absoluterank=3;
	else if(isonepair())
		absoluterank=2;
	else if(isnothing())
		absoluterank=1;
}