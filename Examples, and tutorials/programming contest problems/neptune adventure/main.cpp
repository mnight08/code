#include <iostream>
#include <fstream>
#include <vector>
using namespace std;
//int smaller( smallest &list, int index1, int index2);


class smallest
{

public:

	vector<bool> used;	
	vector<int> small;
	void enqueue( int x)
	{
		small.push_back(x);
	}
	int getmin()
	{
		int min=0;
		for(int i=1;i<small.size();i++)
		{
			if(!used[smaller(min, i)])
				min=smaller(min, i);

		}
		used[min]=true;
		return min;
	}
	smallest(int x)
	{
		small.resize(x);
		used.resize(x);
		for( int i=0;i< x;i++)
		{
			small[i]=-1;
			used[i]=false;
		}
	}

	int smaller(int index1, int index2)
	{
		if((small[index1]==-1&&small[index2]==-1)||(small[index1]!=-1&&small[index2]==-1)||(small[index1]<small[index2]))
		{
			return index1;
		}
		else if(((small[index1]==-1)&&!(small[index2]==-1))||!(small[index1]<small[index2]))
		{
			return index2;
		}
	}
};



void update(smallest& shortestpath, int current,vector<vector<int>> &graph, vector<int> &floodtime)
{
	int newcost;
	if(shortestpath.small[current]==-1)
		return;
	else
		for( int i=0;i<graph.size();i++)
		{	if(graph[current][i]!=0)
		{
			newcost=shortestpath.small[current]+graph[current][i];
			if((shortestpath.small[i]==-1)||(newcost<shortestpath.small[i]))
				if(!(floodtime[i]<newcost))
					shortestpath.small[i]=newcost;
		}
		}
}

//this started as dijkstra's but somehow turned into the bellman-ford algorithm i think
int shortestpath(vector<vector<int>> &graph, int start, int end, vector<int> &floodtime)
{
	//smallest temp(graph.size());
	smallest shortestpath(graph.size());//defaults to -1, update position of minimum value in temp to be the shortest path in this one.
	//first update
	//get first node
	int current=start;
	shortestpath.small[start]=0;
	shortestpath.used[start]=true;
	//iterate through rest of unvisited nodes

	for( int i=0;i< graph.size();i++)
	{
		update(shortestpath,current, graph,floodtime);
		shortestpath.used[current]=true;
		current=shortestpath.getmin();
		if(shortestpath.small[current]==-1||shortestpath.used[current])
			break;
	}
	return shortestpath.small[end];
}

int main()
{
	int datasets=0;
	int graphsize=0;
	int l, s, r =0;
	int swap=0;
	fstream file;
	file.open("input.txt");
	while(cin)
	{
		cin>>file;
	}
	ofstream file2;
	file2.open("output.txt");
	file.open("input.txt");
	file>>datasets;
	//handle input
	vector<vector<int>> graph;
	vector<int> floodtime;

	for(int i=0;i<datasets;i++)
	{
		file>>l>>s>>r;
		graph.resize(l);
		floodtime.resize(l);
		//make an lxl matrix
		for(int j=0; j<l;j++)
		{	
			graph[j].resize(l);
		}


		for(int j=0; j<l;j++)
		{
			file>>floodtime[j];
			if(floodtime[j]==0)
				floodtime[j]=10001;
			for( int k=0;k<l;k++)
			{
				file>>graph[j][k];
			}

		}
		int temp=shortestpath(graph,s-1,r-1,floodtime);
		if(temp!=-1)
			file2<< temp<<endl;
		else
			file2<<"GENE HACKMAN"<<endl;
		graph.clear();
	}

	return 0;
}