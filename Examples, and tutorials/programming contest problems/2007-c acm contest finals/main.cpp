#include <iostream>
#include <list>
#include <math.h>
#include <vector>
#include <fstream>

using namespace std;
class point
{
public:
	double x;
	double y;

};


//adjust points by some agnle
void correct(vector<point>& points, double angle)
{
	double newy;
	double newx;
	double r;

	for(int i=0;i<points.size();i++)
	{
		r=pow(pow(points[i].x,2)+pow(points[i].y,2),.5);
		newy=r*sin(angle);
		newx=r*cos(angle);
		points[i].x=newx;
		points[i].y=newy;
	}
}

double getangle(vector<point>& points,int first, int next)
{
	double dy=points[next].y-points[first].y;
	double dx=points[next].x-points[first].y;
	dy=pow(dy,2);
	dx=pow(dy,2);

	dy=dy/(dy+dx);
	
	return pow(dy,.5);
}

int main()
{

	ifstream input;
	input.open("in.txt",ios::in);

	double angle=0;
	double offset=0;
	
	vector<point> points;

	int x,y;
	for(;!input.eof();)
	{
		int n;
		input>>n;
		input>>angle;
		for(int i=0;i<n;i++){
		point temp;
		input>>x;
		input>>y;
		temp.x=x;
		temp.y=y;
		points.push_back(temp);
		}

		for(;true;)
		{
			
			for(int i=0;i<n-1;i++)
			{
				double dy=points[i+1].y-points[i].y;
				if(dy<0)
				{
					double angle=getangle(points,i, i+1);
					correct(points,angle);
				}
				
			}

		}
	}


	return 0;
}