% makephantom
%--------------------------------------------------------------------
% This script generates a function to be used as a phantom in
% tomography simulations. The values are computed on two grids,
% a fine grid and a coarse one. The values on the fine grid
% will be used to accurately compute projections. The coarse grid
% representation will be used to compare the reconstruction
% with the phantom.
% The size of the grids is  ndim x ndim  for the coarse grid,
% and  ndimphan x ndimphan  for the fine grid. Parameters ndim and
% ndimphan are read from the file config.in. The latter file
% also contains the name of the phantom description file.
%
% The phantom  consists of several circular "bumps"
% defined by the following parameters:
%  rcx, rcy = the arrays of length nbump, corresponding to the
%       x and y coordinates of the "bumps'" centers
%       (the grid is defined within the unit square [-1,1] x [-1,1] )
%  radouter,radinner = arrays defining the outer and inner radii
%       of each of the bumps
%  amp = array with the amplitudes of each bump (can of any sign).
%
%  The output of this script is written into four output files.
%  Files "phantom.out" and "phantom.d" will contain the function
%  on the coarse grid. The former file is in a text format,
%  loadable into Matlab using "load ... -ascii" command.
%  File "phantom.d" is a binary dump of the same file in the
%  32-bit IEEE single precision format, in the row-by-row (!!!) order.
%  Files "phantom_fine.out" and  "phantom_fine.d" will contain
%  values of the phantom on the fine grid, in the similar formats.
%
%--------------------------------------------------------------------
%   Leonid Kunyansky, U. of Arizona, Tucson, leonk@math.arizona.edu
%
%   Written for MSRI Graduate Workshop on Inverse Problems, 2009
%===================================================================

clear all;
clf;

%-------------- read desired sizes of grids and the phantom file name

[nproj,a0,a1,nlines,ndimphan,ndim,phan_name,dummy,dummy1] = readconfig;


% ------------ read phantom description ------------------
fid = fopen(phan_name);

numb_of_bumps = fscanf(fid,'%d',10);
fgets(fid);

for j = 1:numb_of_bumps
    array  = (fscanf(fid,'%f'));
    rcx(j)    = array(1);
    rcy(j)    = array(2);
    radouter(j) = array(3);
    radinner(j) = array(4);
    amp(j)    = array(5);

    fgets(fid);
end

fclose(fid);




%-------------- generate coarse-grid phantom -----------------------------

f = sumbump(ndim,rcx,rcy,radouter,radinner,amp);
mywrite(f,'phantom.d');

% f = flip(f);
save phantom.out f -ascii;


%-------------- generate fine-grid phantom -------------------------------

f = sumbump(ndimphan,rcx,rcy,radouter,radinner,amp);
mywrite(f,'phanfine.d');

% f = flip(f);
save phantom_fine.out f -ascii;

display('Done !!!');

myimage('phantom_fine.out');
