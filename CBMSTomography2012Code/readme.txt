                Tomography subroutines,  MATLAB
             -----------------------------------

                      Leonid Kunyansky

                   U. of Arizona,  Tucson

                   leonk@math.arizona.edu




Section 1. Introduction
=======================

This set of Matlab scripts and function allows one to do some basic
experiments with direct and inverse Radon transform.
In particular, the main scripts of this package can be used to generate
some simple model density distributions (phantoms), compute Radon
projections (with an option to add noise and/or to model the exponential
decay present in emission tomography), and to implement the Filtration-
Backprojection algorithm.

The package consists of three scripts:
makehantom.m, makeprojections.m, and makeinversion.m,
and several subroutines called by the scripts.
The names of the scripts are self-explanatory, and in general, the scripts
should be ran in that order.

It is recommended to place all the subroutines in a subdirectory
(call it, say, "routines") and adding the name of this subdirectory
to the current path, e.g. by the command    path(path,"routines")  .

The parameters determning the geometry of the simulation (number of
projections, angular range etc) are placed in the file config.in, and are
read by the scripts. These parameters are:

   nproj    = number of projections
   a0, a1   = angles in degrees, the start and the end of the angular range
              covered by projections
   nlines   = number of measurements (line integrals) within one projection
   ndimphan = size of the fine grid used to discretize the phantom and to
              compute projections
   ndim     = size of the (coarse) reconstruction grid;
              typically, ndim < ndimphan
   phantom_name = file name of the phantom description file,
                  see function makephantom for details
   atten    = constant attenuation coefficient, if one wants to model ERT/SPECT
   want_noise = desired level of noise in projections, in percents


Section 2. Makephantom
======================

This script generates a function to be used as a phantom in
tomography simulations. The values are computed on two grids,
a fine grid and a coarse one. The values on the fine grid
will be used to accurately compute projections. The coarse grid
representation will be used to compare the reconstruction
with the phantom.
The size of the grids is  ndim x ndim  for the coarse grid,
and  ndimphan x ndimphan  for the fine grid. Parameters ndim and
ndimphan are read from the file config.in. The latter file
also contains the name of the phantom description file.

The phantom  consists of several circular "bumps"
defined by the following parameters:
   rcx, rcy = the arrays of length nbump, corresponding to the
        x and y coordinates of the "bumps'" centers
        (the grid is defined within the unit square [-1,1] x [-1,1] )
   radouter,radinner = arrays defining the outer and inner radii
        of each of the bumps
   amp = array with the amplitudes of each bump (can of any sign).

Having phantom desription in a separate file (or files), allows
one to keep a library of phantoms, and utilize the desired one
simply by changing the name in the config.in

The output of this script is written into four output files.
Files "phantom.out" and "phantom.d" will contain the function
on the coarse grid. The former file is in a text format,
loadable into Matlab using "load ... -ascii" command.
File "phantom.d" is a binary dump of the same file in the
32-bit IEEE single precision format, in the row-by-row (!!!) order.
Files "phantom_fine.out" and  "phantom_fine.d" will contain
values of the phantom on the fine grid, in the similar formats.


Section 3. Makeprojections
==========================
This script loads a 2-D phantom from the file phantom_fine.out,
and computes the Radon projections (or X-ray transform).
Can also compute the attenuated Radon with a constant attenuation
coefficient.
In any case the result is placed into the file projections.out.
If requested, adds to the projections a normally distributed random
variable to model noise of measurements.
All the parameters are read from the file config.in; they were described
in Section 1.

Section 4. Makeinversion
========================
This script implements filtration/backprojection algorithm;
if desired, filtration can be skipped, or replaced by the second
derivative corresponding to "local tomography" type of reconstruction.

The filtration mode is detemined through a short dialog; the
user has to enter one of the symbols "f","t","s","l", or "0",
corresponding to FBP, FBP with FFT filtration, FBP with FFT filtration
and additional smoothing filter, local filtration, or no filtration.

Projections are read from the file projections.out.

The dimensions of the reconstruction grid and of the projection
data are read from the file config.in.

The resulting reconstruction is saved into files reconstruction.out
and recon.d. The former file is in the Matlab text format,
loadable by command  load ... -ascii. File recon.d is in the
IEEE 32-bit single precision format; the matrix is written row-by-row.


Section 5. General structure
============================
It is recommended to keep different simulations in different directories.
Also, we prefer to keep main scripts, such as makeinversion.m and
makeprojections.m separately from smaller subroutines.
To this end the larger scripts are put on the upper level directory,
the subroutines are in the subdirectory "subroutines", and examples
are in the "examples" subdirectory. The latter contains several subdirectories
with examples, i.e., "exterior", "fewlines", etc.

Each of the lower level example directories contains it's own "config.in" plus
a file with the phantom description suitable for this simulation, and in some
cases, additional scripts or subroutines specific for this example.
Each subdirectory also contains a "runall.m" or similarly named script
that runs consequitevely all the steps of the simulation. The first
line of these "runall" scripts appends the path (system list of "visible"
subdirectories with "../../" and "../subroutines", so that all the necessary
scripts located in the directories other that the current can be automatically
found by MATLAB.





