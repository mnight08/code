% makeinversion
%--------------------------------------------------------------------
% This script implements filtration/backprojection algorithm;
% if desired, filtration can be skipped, or replaced by the second
% derivative corresponding to "local tomography" type of reconstruction.
% The filtration mode is detemined through a short dialog; the
% user has to enter one of the symbols "f","t","s","l", or "0",
% corresponding to FBP, FBP with FFT filtration, FBP with FFT filtration
% and additional smoothing filter, local filtration, or no filtration.
%
% Projections are read from the file projections.out.
%
% The dimensions of the reconstruction grid and of the projection
% data are read from the file config.in.
%
% The resulting reconstruction is saved into files reconstruction.out
% and recon.d. The former file is in the Matlab text format,
% loadable by command  load ... -ascii. File recon.d is in the
% IEEE 32-bit single precision format; the matrix is written row-by-row.
%
%--------------------------------------------------------------------
%   Leonid Kunyansky, U. of Arizona, Tucson, leonk@math.arizona.edu
%====================================================================

clear;

mode = ' ';
while (mode ~= 'f' & mode ~= 'l' & mode ~= 'n' & mode ~= 't' & mode ~= 's' & mode ~= 'e')
    display('Filtration mode?' );
    display('f = FBP, n = none, l = local tomogrfy, t = FFT, s = FFT + smoothing' );

%    display('f = FBP, t = FFT filtration, s FFT filtration with smoothing' );
%    display('n = none, l = local tomogrfy' );
    display('Filtration mode?' );
  mode = input('Enter one character :','s');
end %while

if(mode == 't' | mode == 's')
	display('Extent of zero padding, M = 2^n')
  M = input('Enter M :');
end


%-------- Read the parameters

[nproj,a0,a1,nlines,ndimphan,ndim,dummy,atten,dummy2] = readconfig;

%-------- Read the projections

load projections.out projections -ascii;

stepx      = 2/(ndim - 1);
stepy      = stepx;

anglestep = (a1 - a0)/(nproj-1);
sstep     = 2/(nlines-1);

%--------------------------------------- partial correction for attenuation -------------
if(atten > 0.001)
   attcor = ones(nlines,1);

   for nl = 2: nlines-1             %--- ... for each detector in a projection ....
       s = - 1.0 + sstep*(nl-1);
       halfchord = sqrt(1-s*s);
       attcor(nl) = exp(halfchord*atten);
   end

   for npr = 1:nproj                   %--- ... for each projection direction ...
      projections(:,npr) = projections(:,npr).*attcor;
   end
end
%--------------------------------------- Filtration of projections ----------------------

if mode == 't'
   filtproj = fftfiltration(projections,0,M);
end


if mode == 's'
   filtproj = fftfiltration(projections,1,M);
end

if mode == 'e'
   filtproj = spectfiltration(projections,0,atten);
end


if (mode == 'n' | mode == 'l' | mode == 'f')

   filtproj = filtration(projections,mode);

end

save filtproj.out filtproj -ascii;

%--------------------------------------- Backprojection ---------------------------------

reconst = zeros(ndim,ndim);

for ix = 1:ndim
    disp(sprintf('Row # %d  out of %d',ix,ndim));
    x = -1.0 + stepx*(ix-1);
    for iy = 1:ndim
        y = -1.0 + stepy*(iy-1);
        if( x*x + y*y < 0.97)

           for npr = 1:nproj
               angle = a0 + (npr-1)*anglestep;
               co = cos(angle);
               si = sin(angle);

               s = x*co + y*si;
               t = x*si - y*co;

               position = (nlines+1)/2 + s/sstep ;
               nline0 = floor(position);
               nline1 = nline0 + 1;
               alpha1 = position - nline0;
               alpha0 = 1 - alpha1;

               if(alpha1 > 1 | alpha1 < 0 | nline1 > nlines | nline0 < 1)
                   display('linear interpolation is not working');
                   return
               end % if

               value = alpha0*filtproj(nline0,npr) + alpha1*filtproj(nline1,npr);

               if(atten >0.001)
                  value = value*exp(-t*atten);
               end

               trapweight = anglestep;
               if(npr == 1 | npr == nproj)
                  trapweight = 0.5*anglestep;
               end % if

               reconst(iy,ix) = reconst(iy,ix) + value*trapweight /(2.0*(a1-a0));

            end % for
        end %if
    end % for
end % for

mywrite(reconst,'reconst.d');

save reconstruction.out reconst -ascii;

if(mode == 'l' | mode == 'n')
    makepics(0);
else
    makepics(1);
end % if

display('Done !!!');

