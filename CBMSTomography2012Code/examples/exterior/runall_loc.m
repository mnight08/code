close all
clear all
path('..\..\;..\..\subroutines\',path)

display('                ');
display('Making a phantom');
display('================');
makephantom

display('                  ');
display('Making projections');
display('==================');
makeprojloc

display('         ');
display('Inversion');
display('=========');
makeinversion

display('                    ');
display('Completely finished!');
display('====================');
