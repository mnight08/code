% makeprojloc
%------------------------------------------------------------------------------
%  This script loads a 2-D phantom from the file phantom_fine.out,
%  and computes the Radon projections (or X-ray transform).
%  Can also compute the attenuated Radon with a constant attenuation
%  coefficient.
%  In any case the result is placed into the file projections.out.
%  If requested, adds to the projections a normally distributed random
%  variable to model noise of measurements.
%  All the parameters are read from the file config.in.
%
%  The parameters are:
%
%   nproj    = number of projections
%   a0, a1   = angles in degrees, the start and the end of the angular range
%              covered by projections
%   nlines   = number of measurements (line integrals) within one projection
%   ndimphan = size of the fine grid used to discretize the phantom and to
%              compute projections
%   ndim     = size of the (coarse) reconstruction grid; ndim <= ndimphan
%   phantom_name = file name of the phantom description file, see makephantom()
%   atten    = constant attenuation coefficient, if one wants to model ERT/SPECT,
%              0 otherwise
%   want_noise = desired level of noise in projections, in percents.
%
%------------------------------------------------------------------------------
%   Leonid Kunyansky, U. of Arizona, Tucson, leonk@math.arizona.edu
%
%   Written for MSRI Graduate Workshop on Inverse Problems, 2009
%==============================================================================

clear all;
clf;

%------------------------------------- Read the parameters --------------------

[nproj,a0,a1,nlines,ndimphan,ndim,dummy,atten,want_noise] = readconfig;

%------------------------------------- Read the phantom on the fine grid ------

phantom = load('phantom_fine.out','ascii');
% phantom = flip(phantom);

                                       %-------- Foolproofing things ----------
[ndim1,ndim2] = size(phantom);
if (ndim1 ~= ndimphan |ndim1 ~= ndimphan)
    display('Check the dimensions!!!');
    return;
end

%-------------------------------------- Compute projections -------------------
figure(1);

projections = zeros(nlines,nproj);

stepx      = 2/(ndimphan-1);
stepy      = stepx;

anglestep = (a1 - a0)/(nproj-1);
sstep     = 2/(nlines-1);
tstep     = stepx;

for npr = 1:nproj                   %--- ... for each projection direction ...
   angle = a0 + (npr-1)*anglestep;

   co = cos(angle);
   si = sin(angle);

   proj = zeros(nlines,1);
   disp(sprintf('Proj # %d  out of %d',npr,nproj));

   for nl = 2: nlines-1             %--- ... for each detector in a projection ....
       s = - 1.0 + sstep*(nl-1);

       total = 0.0;

       halfchord = sqrt(1-s*s);
       sco = s*co;
       ssi = s*si;

       for t = -halfchord:tstep: halfchord         %--------- going along the line of integration

           x =  t*si + sco;
           y = -t*co + ssi;

                              %--------- bilinear interpolation ....

           rx = (x + 1.0)/stepx+1;
           ry = (y + 1.0)/stepy+1;
           nx = floor(rx);
           ny = floor(ry);

           if(nx > 0 & nx < ndimphan & ny > 0 & ny < ndimphan)

              ax1 = rx-nx;
              ay1 = ry-ny;
              ax0 = 1.0-ax1;
              ay0 = 1.0-ay1;

              value = ax0*(ay0*phantom(ny,nx)  + ay1*phantom(ny+1,nx) )+...
     +                ax1*(ay0*phantom(ny,nx+1)+ ay1*phantom(ny+1,nx+1));

              if(atten > 0.00001)             %  modeling exponential decay in emission tomography (SPECT)
                 dist_to_det = halfchord - t;
                 value = value * exp(- dist_to_det*atten);
              end % if

              total = total + value*tstep;  %   integration

           end % if
       end
       proj(nl) = total;
   end

%-------------------------  modeling random noise in the measurements ---------------
   if(want_noise > 0.00001)            % if noise
      noise = randn(size(proj));

      ampsignal = sqrt(proj'*proj);
      ampnoise  = sqrt(noise'*noise);
      coef = ampsignal/ampnoise*want_noise;
      proj1 = proj + coef*noise;
      plot(1:nlines,proj,1:nlines,proj1);
      projections(:,npr) = proj1;
   else                                % if no noise
      plot(proj);
      projections(:,npr) = proj;
   end % if
%------------------------------------------------------------------------------------

   drawnow;

end

%   angleint = a1-a0;
%   anglesh = 0.20*angleint;
%   anstart = a0;
%   anhigh1 = a0 + anglesh;
%   anhigh2 = a1 - anglesh;
%   anfin   = a1;


 sstart = -0.4;
 sfin = 0.4;
 shigh1 = -0.3;
 shigh2 = 0.3;
 smooth = zeros(nlines,1);

 for nl = 1:nlines
    s = -1.0 + sstep*(nl-1);
    smooth(nl) = hat(sstart,shigh1,shigh2,sfin,s);
 end

 smooth = ones(nlines,1)- smooth;
 figure(3);
 plot(smooth);

 for npr = 1:nproj                   %--- ... for each projection direction ...
    projections(:,npr) = projections(:,npr).*smooth;
 end


save projections.out projections -ascii;

figure(2);
myimage('projections.out');


%---------------- old stuff
%
%             if(ax > 1.0 | ax1 > 1.0 | ay > 1.0 |ay1 > 1.0)  % should never happen
%                 display('ax,ax1,ay,ay1');
%                 display(ax,ax1,ay,ay1);
%                 display('xp1,yp1,nx,ny');
%                 display(xp1,yp1,nx,ny);
%              end
