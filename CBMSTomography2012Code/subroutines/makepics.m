function makepics(varargin);
%--------------------------------------------------------------------
% This subroutine will load files phantom_fine.out
% and reconstruction.out, and make plots of the phantom 
% and the reconstructed image side-to-side.
% Can be called as makepic(0) or just makepics in which
% case plots will be on the individual color scales.
% If called as makepic(1), the plots will be on the same 
% color scale
%
%--------------------------------------------------------------------
%   Leonid Kunyansky, U. of Arizona, Tucson, leonk@math.arizona.edu
%
%   Written for MSRI Graduate Workshop on Inverse Problems, 2009
%====================================================================

equal = 0;
par = cell2mat(varargin);

if(size(par) == [1,1])
   if(par == 1 | par == 0)
       equal = par;
   end % if
end % if

name = 'phantom_fine.out';
phan = load(name,'ascii');

name = 'reconstruction.out';
rec  =  load(name,'ascii');

phanmax  = max(max(abs(phan)));
recmax   = max(max(abs(rec)));

if(equal == 1)
   totalmax = max(phanmax,recmax);
   phanmax = totalmax;
   recmax  = totalmax;
end % if

if(phanmax < 1e-35)
    display('Empty phantom');
    return;
end

if(recmax < 1e-35)
    display('Empty reconstruction');
    return;
end


mycolormap = zeros(64,3);

graybkg = 0.12;

for j = 1: 64
   t = -1 + 2*(j-1)/63; % t will run from -1 to 1
%   g = max( 0, graybkg + 0.75*(1 - graybkg) *(t*t-0.3*t) );
   g = graybkg + (1 - graybkg ) *t*t*t*t;
   r = min ( max(t + graybkg ,0), 1 );
   b = min ( max(-t + graybkg,0), 1 );
   mycolormap(j,:) = [r,g,b];
end


figure(1);
clf;
subplot(1,2,1);
hhh = image(32*(1 + phan/phanmax));
title('Phantom')
colormap(mycolormap);

axis xy;
axis equal;
axis manual;

subplot(1,2,2);
hhh = image(32*(1 + rec/recmax));
title('Reconstruction');
axis xy;
axis equal;
axis manual;

if(equal == 1)
   axes('position',[0.90,0.3,0.15, 0.5]);
   a = 1/64*totalmax*(64:-1:1)';
   hhh = imagesc(a,[0,totalmax]);
   title('Scale');
   axis fill;
   axis equal;
   axis manual;
   axis off;

end % if    

return
end
