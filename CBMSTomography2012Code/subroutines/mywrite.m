function mywrite(array,name);
%--------------------------------------------------------------------
% This subroutine writes input square array to the file with
% the name specified in the input parameters.
% The info is written in the IEEE 32-bit single precision format,
% !!!! row by row !!!!
%
%--------------------------------------------------------------------
%   Leonid Kunyansky, U. of Arizona, Tucson, leonk@math.arizona.edu
%====================================================================

[ny,nx] = size(array);
if nx ~= ny
    display('cant write');
    return
end % if

fid = fopen(name,'w');

fwrite(fid,array','single');

fclose(fid);
end
