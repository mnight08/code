function foutput = fftfiltration(finput,ifilter,M);
%--------------------------------------------------------------------
%  This function performs linear filtration of the "finput"
%  in the first variable (along the columns of the array)
%  in the frequency domain, i.e. using the FFT.
%  input parameter "ifilter" determines the use of an additional
%  smoothing filter
%    ifilter = 0  -> no additional filtration
%    ifilter = 1  -> additional cosine filter is applied
%    parameter M defines the extent of zeropadding
%--------------------------------------------------------------------
%   Leonid Kunyansky, U. of Arizona, Tucson, leonk@math.arizona.edu
%====================================================================

[nlines,nproj] = size(finput);

foutput = zeros(nlines,nproj);

nextended = (nlines-1)*M+1;

inpu = zeros(nextended-1,1);

for npr = 1:nproj
   inpu(1:nlines-1,1) = finput(1:nlines-1,npr);
   outp = fourierfilt(inpu,nextended,ifilter)/M ;
   foutput(1:nlines-1,npr) = outp(1:nlines-1);

end % for

end
