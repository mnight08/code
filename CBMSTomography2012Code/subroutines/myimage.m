function myimage(name);
%--------------------------------------------------------------------
%  This subroutine reads the file with a given filename,
%  containing a rectangular array in the text format readable by
%  Matlab function load ... -ascii. If at least one of the array entries
%  is positive, the function will draw a colormap of the array.
%  Otherwise, the error message is printed.
%
%--------------------------------------------------------------------
%   Leonid Kunyansky, U. of Arizona, Tucson, leonk@math.arizona.edu
%====================================================================


a = load(name,'ascii');

rmax = max(max(a));
rmin = min(min(a));

if(rmax <= 0)
    display('Cant scale nicely');
    return;
end

hhh = imagesc(a,[rmin, rmax]);

colormap('copper');

axis xy;
axis equal;
axis manual;

return
end
