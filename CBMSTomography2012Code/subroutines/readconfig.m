function [nproj,a0,a1,nlines,ndimphan,ndim,phantom_name,atten,want_noise] =readconfig;
%--------------------------------------------------------------------------
% This subroutine reads the configuration file config.in with the
% parameters of the current simulation.
% The parameters are:
%
%   nproj    = number of projections
%   a0, a1   = angles in degrees, the start and the end of the angular range
%              covered by projections
%   nlines   = number of measurements (line integrals) within one projection
%   ndimphan = size of the fine grid used to discretize the phantom and to
%              compute projections
%   ndim     = size of the (coarse) reconstruction grid; typically, ndim < ndimphan
%   phantom_name = file name of the phantom description file, see function makephantom
%   atten    = constant attenuation coefficient, if one wants to model ERT/SPECT
%   want_noise = desired level of noise in projections, in percents
%
%--------------------------------------------------------------------
%   Leonid Kunyansky, U. of Arizona, Tucson, leonk@math.arizona.edu
%====================================================================

clear;
fid = fopen('config.in');

nproj   = fscanf(fid,'%d',10);
fgets(fid);

a0      = fscanf(fid,'%f',20);
fgets(fid);
a0 = a0/180*pi;

a1      = fscanf(fid,'%f',20);
fgets(fid);
a1 = a1/180*pi;

nlines  = fscanf(fid,'%d',10);
fgets(fid);

ndimphan= fscanf(fid,'%d',10);
fgets(fid);

ndim    = fscanf(fid,'%d',10);
fgets(fid);

phantom_name    = fscanf(fid,'%c',20);
fgets(fid);

atten       = fscanf(fid,'%d',10);
fgets(fid);

want_noise  = fscanf(fid,'%d',10);
fgets(fid);
want_noise = want_noise/100.0;

fclose(fid);

end
