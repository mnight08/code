function res = bump(rmax,rmin,r);
%--------------------------------------------------------------------
% This subroutine returns a value of an infinetely smooth function
% that equals 0 for r > rmax, equals 1 for r < rmin, and decreases
% smoothly between rmin and rmax.
% It is assumed that rmin is either lesser than rmax (smooth transition),
% or equal to rmax, in which case this is a step function.
%
%--------------------------------------------------------------------
%   Leonid Kunyansky, U. of Arizona, Tucson, leonk@math.arizona.edu
%
%   Written for MSRI Graduate Workshop on Inverse Problems, 2009
%====================================================================

% [ndim,ndim1] = size(r);

if(rmax < rmin)
    display('no can do');
    return
end

if(rmax == rmin)
    rmin = rmax-0.00001;
end

relx = (r-rmin)./(rmax-rmin);

relx(relx>0.99999999) = 0.99999999;
relx(relx<0.00000001) = 0.00000001;

% relx = reshape(relx,ndim,ndim);

res = exp(2.0*exp( -1.0./relx )./(relx -1.0));

end

