function outpu = fourierfilt(inpu,nlines,ifilter);
%--------------------------------------------------------------------
%  This function performs linear filtration of the input
%  in the first variable (along the columns of the array)
%  in the frequency domain, i.e. using the FFT.
%  Input parameter "fftmode" determines the use of an additional
%  smoothing filter
%    ifilter = 0  -> no additional filtration
%    ifilter = 1  -> additional cosine filter is applied
%--------------------------------------------------------------------
%   Leonid Kunyansky, U. of Arizona, Tucson, leonk@math.arizona.edu
%====================================================================

nmed = (nlines-1)/2+1;

fftfilt = zeros(nlines-1,1);

for j = 1:nlines-1
   fftfilt(j) = pi*abs(j-nmed);
end


if(ifilter == 1)
   for j = 1:nlines-1
       fftfilt(j) = fftfilt(j) * cos((j-nmed)/nmed*pi*0.5);
   end
end

plot(fftfilt);
%pause

fftinpu = fftshift( fft(fftshift(inpu)) );


outpu   = fftshift( ifft(fftshift(fftfilt.*fftinpu)) );


end
