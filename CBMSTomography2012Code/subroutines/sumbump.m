function a = sumbump(ndim,rcx,rcy,radouter,radinner,amp);
%--------------------------------------------------------------------
% This subroutine will generate an array of the size ndim x ndim
% with values of the function consisting of several circular "bumps"
% defined by the following parameters:
%  rcx, rcy = the arrays of length nbump, corresponding to the
%       x and y coordinates of the "bumps'" centers
%       (the grid is defined within the unit square [-1,1] x [-1,1] )
%  radouter,radinner = arrays defining the outer and inner radii
%       of each of the bumps
%  amp = array with the amplitudes of each bump (can be of any sign).
%
%--------------------------------------------------------------------
%   Leonid Kunyansky, U. of Arizona, Tucson, leonk@math.arizona.edu
%====================================================================

[ndummy,nbump] = size(rcx);

a    = zeros(ndim,ndim);
step = 2.0/(ndim-1);

xx = linspace(-1.0,1.0,ndim);
[x,y] = meshgrid(xx,xx);

for k = 1: nbump
    distance = sqrt( (x-rcx(k)).^2 + (y-rcy(k)).^2 );
    a = a + amp(k)*bump(radouter(k),radinner(k),distance);
end


end
