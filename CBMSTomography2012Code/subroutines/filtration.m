function result = filtration(input,mode);
%--------------------------------------------------------------------
%  This function performs linear filtration of the input
%  in the first variable (along the columns of the array).
%  Input parameter mode determines the type of filtration:
%    'f' = lambda filtration (differentiation combined with the Hilbert
%          transform) --- this filter is used in the FBP algorithm
%    'l' = double differetiation, used for the "local" tomography.
%    'n' = no filtration performed
%
%--------------------------------------------------------------------
%   Leonid Kunyansky, U. of Arizona, Tucson, leonk@math.arizona.edu
%
%   Written for MSRI Graduate Workshop on Inverse Problems, 2009
%====================================================================

[nlines,nproj] = size(input);

sstep = 2/(nlines-1);

%% ========================================================================
if(mode == 'f') % Standard FBP filtration

   %-------- differentiation --------------
   deriv  = zeros([nlines,nproj]);

   for npr = 1:nproj
      for j = 2:nlines-1
         deriv(j,npr) = (input(j+1,npr) - input(j-1,npr))/(2*sstep);
      end % for
   end % for

   %-------- Hilbert transform -----------

   result = zeros([nlines,nproj]);

   filtr = zeros(1,nlines);

   filtr(1) = 0;
   filtr(2) = 2/pi*log(2);

   for j = 3:nlines
       n = j-1;
       filtr(j) = 1/pi*( n*log(1 - 1/(n*n) ) + log( (n+1)/(n-1)) );
   end % for

   %----------------- convolution

   for npr = 1:nproj
      for j = 1:nlines
         for k = 1:nlines
             jmk = j-k;
             if(jmk >= 0)
                filt =  filtr(jmk+1);
             else
                filt = -filtr(-jmk+1);
             end % if
             result(j,npr) = result(j,npr) + filt*deriv(k,npr);
         end % for
      end % for
   end
   return
end % if

%% ========================================================================
if(mode == 'n')   % no filtration at all
    result = input;
    return
end % if

%% =========================================================================
if(mode == 'l')   % local tomography, filtration = second derivative

   %-------- differentiation --------------
   result  = zeros([nlines,nproj]);

   for npr = 1:nproj
      for j = 2:nlines-1
         result(j,npr) = -(input(j+1,npr) - 2.0*input(j,npr)...
                         + input(j-1,npr) )/(sstep*sstep);
      end % for
   end % for

    return
end % if

end % function
