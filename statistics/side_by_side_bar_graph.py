import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
###############################################################################
#input frequency distribution
colors=('brown', 'green', 'grey', 'blue' )
frequency_1342_12=[14,1,0,0]
frequency_1342_23=[20,3,2,4]


#Get total frequency
total_frequency_1342_12=sum(frequency_1342_12)
total_frequency_1342_23=sum(frequency_1342_23)

#Set the positions of the bars.  The .5 will offset the bar so it is not aligned
#on the left.
x_pos_ticks=[x+.5 for x in np.arange(len(colors))]
x_pos_1342_12=[x+.25 for x in np.arange(len(colors))]
x_pos_1342_23=[x+.75 for x in np.arange(len(colors))]

#set the axis so that the tallest a bar can be is thte total frequency
plt.axis([0,len(colors),0, max(total_frequency_1342_12,total_frequency_1342_23)])

#create the bars in the plot.
plt.bar(left=x_pos_1342_12,height =frequency_1342_12, align='center', alpha=0.5, width=.25, color='red')
plt.bar(left=x_pos_1342_23,height =frequency_1342_23, align='center', alpha=0.5, width=.25, color='blue')
#Add labels and title.
plt.xlabel('Eye Color')
plt.ylabel('Freqency')
plt.title('Side by Side Bar Graph of Eye Color')
plt.xticks(x_pos_ticks, colors);





#output to file.
plt.savefig("eye_color_frequency_side_by_side_bar_graph.png")

