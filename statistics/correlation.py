# -*- coding: utf-8 -*-
"""
Created on Wed Mar  1 15:15:03 2017

@author: vpx365
"""
import numpy as np

x=[.8,1.7,2,3.8,4.8,5.8,
   6.7,7.6,8.3,9.2,10.6,   
   11.5,12.3, 13.4,14.2]
   
y=[5.9,14.9,4.9,0.9,13.9,
   7.8,8.8,4.4,14,15,7.3,
   4.7,11.3,14.6,9.3]
   
print(np.corrcoef(x,y))


#help(np.corrcoef)
