import numpy as np
import matplotlib.pyplot as plt
import statistics as s

#input our data
x=[1,4,4,7,5,9]
y=[3,21,36,40,45,46]


#Create scatter plot
plt.scatter(x, y, s=40)
plt.show()


#find mean and standard deviation of x
print(s.mean(x))
print(s.stdev(x))

#find mean and standard deviation of y
print(s.mean(y))
print(s.stdev(y))

#Find correlation coefficient of x and y.
print(np.corrcoef(x, y)[0, 1])
