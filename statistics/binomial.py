# -*- coding: utf-8 -*-
"""
Created on Wed Mar 29 12:35:59 2017

@author: vpx365
"""

from scipy.stats import binom as bi;
import matplotlib.pyplot as plt;
import numpy as np;

#create a figure that contains two sub plots.  Changing the 2 to a 3 will give 
#subplots.  First number is number of rows and sencond is number of columns in
#table of subplots.
fig,ax=plt.subplots(2,1);


#parameters for the binomial distribution
n,p=10,.5;



#create a plot of the pmf(pdf)
#create the x axis
x=np.arange(bi.ppf(0.0001,n,p),bi.ppf(0.99999,n,p))

#plot dots at height of probability mass above each point.
ax[0].plot(x,bi.pmf(x,n,p), 'bo', ms=8, label='binom pmf')


#create vertical lines up to the dots
ax[0].vlines(x,0, bi.pmf(x,n,p),colors='b', lw=5, alpha=0.5)



#create a plot of cdf
ax[1].plot(x,bi.cdf(x,n,p), 'bo', ms=8, label='binom cdf')


#create vertical lines up to the dots
ax[1].vlines(x,0, bi.cdf(x,n,p),colors='b', lw=5, alpha=0.5)


#print probability mass for all x.
print("probability mass",[bi.pmf(i,n,p) for i in range(0,n+1)])


#print cummulative probability for all x.
print("cummulative probability", [bi.cdf(i,n,p) for i in range(0,n+1)])

