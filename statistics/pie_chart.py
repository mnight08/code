import numpy as np
import matplotlib.pyplot as plt
###############################################################################
#input frequency distribution
colors=('brown', 'green')
frequency=[14,1]

#Get total frequency
total_frequency=sum(frequency)
relative_frequency=[x/total_frequency for x in frequency]

#Set the positions of the bars.  The .5 will offset the bar so it is not aligned
#on the left.
sizes=[x*360 for x in relative_frequency]


#create the the plot.
plt.pie(sizes, labels=colors,autopct='%1.1f%%', colors=colors)
plt.axis('equal')

#Add labels and title.
plt.title('Pie Chart of Eye Color')


#output to file.
plt.savefig("eye_color_Pie_Chart.png")


