import numpy as np
import matplotlib.pyplot as plt


#input our data.  x is where the data was observed
#y is the observed value.
x=[178,176,174,168,166,178,172]
y=[58,56,55,50,47,57,53]
#these are the numbers on the x-axis that
#we will predict y at.
axis=np.arange(min(x), max(x), 0.1);


#Create scatter plot of the data
plt.scatter(x, y, s=40)

#The line of best fit
line=np.poly1d(np.polyfit(x, y, 1))

#"With four parameters I can fit an elephant, 
#and with five I can make him wiggle his trunk.
quartic=np.poly1d(np.polyfit(x, y, 4))
quintic=np.poly1d(np.polyfit(x, y, 5))


#plot the line of best fit
plt.plot(axis, line(axis))


#Add labels
plt.xlabel('x')
plt.ylabel('y')
plt.title('Line of best fit')

#Show the plot
plt.show()

#Find correlation coefficient of x and y.
print(np.corrcoef(x, y)[0, 1])

