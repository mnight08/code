import numpy as np
import matplotlib.pyplot as plt



#My variables
x=[4000,6000,8000,12000,16000,20000,30000,45000,60000]
y=[13,12.4,12.6,12.2,11.6,11.6,10.4,9.2,8]

axis=np.arange(min(x), max(x), 0.1);

print "Mean of x:", np.mean(x)
print "Standard Deviation of x:", np.std(x,ddof=1)

print "Mean of y:", np.mean(y)
print "Standard Deviation of y:", np.std(y,ddof=1)
print "Correlation Coefficient:", np.corrcoef(x, y)[0, 1]


#create and show scatter plot
plt.scatter(x, y, s=40)


#The line of best fit
line=np.poly1d(np.polyfit(x, y, 1))

#plot the line of best fit
plt.plot(axis, line(axis))


#Add labels
plt.xlabel('x')
plt.ylabel('y')
plt.title('Line of best fit')



plt.show()



