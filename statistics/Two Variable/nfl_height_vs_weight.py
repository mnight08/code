import numpy as np
import matplotlib.pyplot as plt

#My variables
height=[77,76,78,72,74,77,76,72,69,74,77,77,76,66,78]
weights=[240,220,320,215,234,257,225,209,221,223,309,230,247,181,321]


print(np.corrcoef(height, weights)[0, 1])


#create and show scatter plot
plt.scatter(height, weights, s=40)
plt.show()
line=np.poly1d(np.polyfit(height, weights, 1))


