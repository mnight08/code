import numpy as np
import matplotlib.pyplot as plt


#input our data.  x is where the data was observed
#y is the observed value.
x=[100,75,150,150,250,600,800,1200,1500,2000]
y=[6.6,7.3,10.4,20.5,40.8,93.5,131.8,180.8,206.1,223]

#these are the numbers on the x-axis that
#we will predict y at.
axis=np.arange(min(x), max(x), 0.1);


#Create scatter plot of the data
plt.scatter(x, y, s=40)

#The line of best fit
line=np.poly1d(np.polyfit(x, y, 1))
quadratic=np.poly1d(np.polyfit(x, y, 2))
cubic=np.poly1d(np.polyfit(x, y, 3))
#"With four parameters I can fit an elephant, 
#and with five I can make him wiggle his trunk.
quartic=np.poly1d(np.polyfit(x, y, 4))
quintic=np.poly1d(np.polyfit(x, y, 5))


#plot the line of best fit
plt.plot(axis, line(axis))
#plt.plot(axis, quadratic(axis))
#plt.plot(axis, cubic(axis))
#plt.plot(axis, quartic(axis))
#plt.plot(axis, quintic(axis))

#Add labels
plt.xlabel('x')
plt.ylabel('y')
plt.title('Polynomials of best fit')

#Show the plot
plt.show()

#Find correlation coefficient of x and y.
#print(np.corrcoef(x, y)[0, 1])

