import numpy as np
import matplotlib.pyplot as plt


#input our data
x=[65,60,43,48,66,75,69,61,37,66]
y=[76,76,54,60,83,91,84,72,46,82]

#The line of best fit
predicted_y=np.poly1d(np.polyfit(x, y, 1))


#Create scatter plot of the data
plt.scatter(x, y, s=40)

#plot the line of best fit
plt.plot(x, predicted_y(x))

#Add labels
plt.xlabel('x')
plt.ylabel('y')
plt.title('Line of best fit for height')

#Show the plot
plt.show()

#Find correlation coefficient of x and y.
print(np.corrcoef(x, y)[0, 1])

