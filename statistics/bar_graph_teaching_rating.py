import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
###############################################################################
"""
Create a bar graph for nursing professions.  High school teacher is also listed to give a baseline  
"""

#input frequency distribution
semester_ratings={"F14": [4.44,  3.95, 4.09, 4.12, 4.14],
"S15": [4.44, 4.23, 4.58, 4.25, 4.18],
"SU15": [4.02, 3.60],
"F15": [4.46, 3.96, 4.01, 4.08, 4.17],
"S16": [3.56, 3.94, 3.59, 4.22],
"SU16": [3.96, 4.49],
"F16": [3.81, 4.49, 4.23, 4.22, 3.59],
"S17": [4.51, 4.52, 4.93, 4.40, 4.28],
"SU17": [4.59, 4.39],
"F17": [4.19, 4.41, 4.43]}
num_columns=len(semester_ratings.keys())


semester_ratings_avg={key: np.median(semester_ratings[key]) for key in semester_ratings}

plt.figure()



#Set the positions of centers of the bars.
x_pos=[1+x for x in np.arange(num_columns)]

#rating vs semester graph
#set the axis so that the tallest a bar can be is thte total frequency
plt.axis([0,num_columns+1,3, 1.1*max(semester_ratings_avg.values())])


#create the bars in the plot.
plt.bar(x=x_pos,height =list(semester_ratings_avg.values()), align='center', alpha=0.5, width=.8)


#Add labels and title.
plt.xlabel('Semester')
plt.ylabel('Average Rating ')
plt.title('UTRGV Student Ratings')
plt.xticks(x_pos, list(semester_ratings.keys()));

#output to file.
#plt.savefig("eye_color_frequency_bar_graph.png")




