# -*- coding: utf-8 -*-
"""
Created on Wed Mar 29 12:35:59 2017

@author: vpx365
"""

from scipy.stats import norm as norm;
from scipy.stats import binom as bi;
import matplotlib.pyplot as plt;
import numpy as np;


#Pick random samples of the same size for binomial random variable. 
#create a figure that contains two sub plots.  Changing the 2 to a 3 will give 
#subplots.  First number is number of rows and sencond is number of columns in
#table of subplots.
fig,ax=plt.subplots(3,1);

n,p=10,.7;
#create a plot of the pmf(pdf)
#create the x axis
x=np.linspace(bi.ppf(0.0001,n,p),bi.ppf(0.99999,n,p),100)

#plot dots at height of probability mass above each point.
ax[0].plot(x,bi.pmf(x,n,p), 'bo', ms=2, label='binom pmf')


#histogram of random data.
data=bi.rvs(size=10000,n=n,p=p)

ax[1].hist(data, normed= True, histtype='stepfilled', alpha=.2)# -*- coding: utf-8 -*-

#histogram of sample means of size k
k=100
sample_means=[np.mean(bi.rvs(size=k,n=n,p=p)) for i in range(1,1000)]
ax[2].hist(sample_means, normed= True, histtype='stepfilled', alpha=.2)# -*- coding: utf-8 -*-






