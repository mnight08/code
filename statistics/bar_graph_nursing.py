import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
###############################################################################
"""
Create a bar graph for nursing professions.  High school teacher is also listed to give a baseline  
Three graphs:  median pay vs profession, number of jobs vs professesion, and job outlook vs profession
Categories: RN, LPN/LVN, APRN
Data Source: https://www.bls.gov/ooh/healthcare/registered-nurses.htm
             https://www.bls.gov/ooh/healthcare/nurse-anesthetists-nurse-midwives-and-nurse-practitioners.htm
             https://www.bls.gov/ooh/healthcare/licensed-practical-and-licensed-vocational-nurses.htm
             https://www.bls.gov/ooh/education-training-and-library/high-school-teachers.htm
"""

#input frequency distribution
professions=['RN', 'LPN/LVN', 'APRN','High School\n Teacher']


median_salaries_per_year=[68450,44090,107460,58030]
number_of_jobs=[2751000,719900,170400,961600]
job_outlook=[16,16,31,6]
plt.figure()



#Set the positions of centers of the bars.  The x axis goes to len(professions)+1
x_pos=[1+x for x in np.arange(len(professions))]

#create the pay vs profession graph
#set the axis so that the tallest a bar can be is thte total frequency
plt.axis([0,len(professions)+1,0, 1.1*max(median_salaries_per_year)])


#create the bars in the plot.
plt.bar(left=x_pos,height =median_salaries_per_year, align='center', alpha=0.5, width=.8)


#Add labels and title.
plt.xlabel('Profession')
plt.ylabel('Median Pay in 2016')
plt.title('Comparison of Profession Median Salary')
plt.xticks(x_pos, professions);

#output to file.
#plt.savefig("eye_color_frequency_bar_graph.png")



plt.figure()

#create the jobs vs profession graph
#set the axis so that the tallest a bar can be is thte total frequency
plt.axis([0,len(professions)+1,0, 1.1*max(number_of_jobs)])


#create the bars in the plot.
plt.bar(left=x_pos,height =number_of_jobs, align='center', alpha=0.5, width=.8)


#Add labels and title.
plt.xlabel('Profession')
plt.ylabel('Number of Jobs in 2014')
plt.title('Comparison of Profession Numbers')
plt.xticks(x_pos, professions);

#output to file.
#plt.savefig("eye_color_frequency_bar_graph.png")


plt.figure()

#create the jobs vs profession graph
#set the axis so that the tallest a bar can be is thte total frequency
plt.axis([0,len(professions)+1,0, 1.1*max(job_outlook)])


#create the bars in the plot.
plt.bar(left=x_pos,height =job_outlook, align='center', alpha=0.5, width=.8)


#Add labels and title.
plt.xlabel('Profession')
plt.ylabel('Expected Percent Growth 2014-2024')
plt.title('Comparison of Proffession Growth')
plt.xticks(x_pos, professions);

#output to file.
#plt.savefig("eye_color_frequency_bar_graph.png")