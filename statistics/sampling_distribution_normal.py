from scipy.stats import norm as n;
import matplotlib.pyplot as plt;
import numpy as np;


#Pick random samples of the same size for binomial random variable. 
#create a figure that contains two sub plots.  Changing the 2 to a 3 will give 
#subplots.  First number is number of rows and sencond is number of columns in
#table of subplots.
fig,ax=plt.subplots(3,1);

mu,sigma=3,10;
#create a plot of the pmf(pdf)
#create the x axis
x=np.linspace(norm.ppf(0.0001,mu,sigma),norm.ppf(0.99999,mu,sigma),100)

#plot dots at height of probability mass above each point.
ax[0].plot(x,norm.pdf(x,mu,sigma), 'bo', ms=2, label='binom pmf')


#histogram of random data.
data=norm.rvs(mu,sigma,10000)

ax[1].hist(data, normed= True, histtype='stepfilled', alpha=.2)# -*- coding: utf-8 -*-

#histogram of sample means of size k
k=100
sample_means=[np.mean(norm.rvs(mu,sigma,k)) for i in range(1,1000)]
ax[2].hist(sample_means, normed= True, histtype='stepfilled', alpha=.2)# -*- coding: utf-8 -*-






