import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
###############################################################################
#input frequency distribution
data=['C','B','A','B','D','D','B','C','B','A','D','C','B','D','A','B','E','A','A','B']


frequency=[14,1]

#Get total frequency
total_frequency=sum(frequency)

#Set the positions of the bars.  The .5 will offset the bar so it is not aligned
#on the left.
x_pos=[x+.5 for x in np.arange(len(colors))]

#set the axis so that the tallest a bar can be is thte total frequency
plt.axis([0,len(colors),0, total_frequency])

#create the bars in the plot.
plt.bar(left=x_pos,height =frequency, align='center', alpha=0.5, width=.8)

#Add labels and title.
plt.xlabel('Eye Color')
plt.ylabel('Freqency')
plt.title('Bar Graph of Eye Color')
plt.xticks(x_pos, colors);

#output to file.
plt.savefig("eye_color_frequency_bar_graph.png")



###############################################################################
#Do the same for relative frequency.

relative_frequency=[x/total_frequency for x in frequency]

plt.cla()

#set the axis so that the tallest a bar can be is thte total frequency
plt.axis([0,len(colors),0, 1])
#Add labels and title.
plt.xlabel('Eye Color')
plt.ylabel('Freqency')
plt.title('Bar Graph of Eye Color')
plt.xticks(x_pos, colors);

#create the bars in the plot.
plt.bar(left=x_pos,height =relative_frequency, align='center', alpha=0.5, width=.8)



#output to file.
plt.savefig("eye_color_relative_frequency_bar_graph.png")
###############################################################################
