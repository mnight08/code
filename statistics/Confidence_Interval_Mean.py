# -*- coding: utf-8 -*-
"""
Created on Fri Jan 27 16:28:34 2017

@author: vpx365
"""

import scipy.stats as stats;
import math
#Using sample standard deviation.  Use t distribution
xbar=151.4;
s=1.2;
n=24;
alpha=.05;
df=n-1;

#critical value.
talpha2=stats.t.ppf(1-alpha/2,df)

#Confidence interval
CI=[xbar-s/math.sqrt(n)*talpha2,xbar+s/math.sqrt(n)*talpha2]


#Using sample standard deviation.  Use t distribution
xbar=106;
s=10;
n=20;
alpha=.05;
df=n-1;

#critical value.
talpha2=stats.t.ppf(1-alpha/2,df)

#Confidence interval
CI=[xbar-s/math.sqrt(n)*talpha2,xbar+s/math.sqrt(n)*talpha2]


#Find critical value and margin of error.
#Using sample standard deviation.  Use t distribution
xbar=2.76;
s=1.24;
n=14;
alpha=.01;
df=n-1;

#critical value.
talpha2=stats.t.ppf(1-alpha/2,df)

#Margin of error
MOE=s/math.sqrt(n)*talpha2


#Confidence interval from data
data=[31960,24239,25773,23057,35686,
      35461,32257,20748,33659,36472,
      25596,26252,32336,28181,19532]
[n, minmax,xbar, var, skew, kurt]= stats.describe(data)
s=math.sqrt(var);

alpha=.04;
df=n-1;

#critical value.
talpha2=stats.t.ppf(1-alpha/2,df)

#Confidence interval
CI=[xbar-s/math.sqrt(n)*talpha2,xbar+s/math.sqrt(n)*talpha2]





#Confidence intervals, z and t percentiles comparison.
n=15

df=n-1;

alpha=.02;
#critical value.
talpha2=stats.t.ppf(1-alpha/2,df)
#critical value.
zalpha2=stats.norm.ppf(1-alpha/2,0,1)
print("z/t "+str(zalpha2)+"/"+str(talpha2))

alpha=.01;
#critical value.
talpha2=stats.t.ppf(1-alpha/2,df)
#critical value.
zalpha2=stats.norm.ppf(1-alpha/2,0,1)
print("z/t "+str(zalpha2)+"/"+str(talpha2))


alpha=.20;
#critical value.
talpha2=stats.t.ppf(1-alpha/2,df)
#critical value.
zalpha2=stats.norm.ppf(1-alpha/2,0,1)
print("z/t "+str(zalpha2)+"/"+str(talpha2))

#Sample size needed for MOE
MOE=1;
alpha=.05;
#critical value.
zalpha2=stats.norm.ppf(1-alpha/2,0,1)
Sigma=10;

n=math.ceil((Sigma*zalpha2)**2/MOE**2)



#critical value and confidence interva;

#Using sample standard deviation.  Use t distribution
xbar=363.24;
s=26.14;
n=8;
alpha=.05;
df=n-1;

#critical value.
talpha2=stats.t.ppf(1-alpha/2,df)

#Confidence interval
CI=[xbar-s/math.sqrt(n)*talpha2,xbar+s/math.sqrt(n)*talpha2]



#Critical Value
n=18;
alpha=.01;
df=n-1;

#critical value.
talpha=stats.t.ppf(1-alpha/2,df)





