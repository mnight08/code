import numpy as np
import matplotlib.pyplot as plt


#input our data.  x is where the data was observed
#y is the observed value.
#linearly correlated data. 
data=[59,31,33,87,99,87,81,43,76,75,22,41,67,29,62]

#vertical wiht diamond outliers
plt.boxplot(data, 0, 'D')

plt.figure()

plt.boxplot(data,notch=None, vert=False, showfliers=True, showmeans=True, sym='rs')



plt.show()









