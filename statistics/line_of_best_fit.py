import numpy as np
import matplotlib.pyplot as plt

#######################################################
#input our data.  x is where the data was observed
#y is the observed value.
#linearly correlated data. 
x=[-1,1,5]
y=[0,9,13]

#The line of best fit
line=np.poly1d(np.polyfit(x, y, 1))


#######################################################
#create scatter plot
num_ticks=8
buffer=.5
step=(max(x)-min(x)+2*buffer)/(num_ticks-1)


#these are the numbers on the x-axis that
#we will predict y at.
xticks=np.arange(min(x)-buffer, max(x)+buffer+step, step);


#Create scatter plot of the data
plt.scatter(x, y, s=25, alpha=.5)



plt.axis([min(x)-buffer, max(x)+buffer, min(y)-buffer,max(y)+buffer])

#plot the line of best fit
plt.plot(xticks, line(xticks))


#Add labels
plt.xlabel('x')
plt.ylabel('y')
plt.title('Line of best fit')

plt.xticks(xticks)


plt.grid(True)

#Show the plot
plt.show()



###########################################################
#Find correlation coefficient of x and y.
print("Correlation is : "+  str(np.corrcoef(x, y)[0, 1]))



###########################################################
#Residual plot.
yhat=line(x)
res=y-yhat


plt.scatter(x, res)

plt.axis([min(x)-buffer, max(x)+buffer, min(res)-buffer,max(res)+buffer])

plt.plot(xticks,np.zeros(len(xticks)))



#Add labels
plt.xlabel('x')
plt.ylabel('residual')
plt.title('Residual Plot')

plt.xticks(xticks)


plt.grid(True)



##############################################################

print(line)





#########Sum of Squared Residuals.
print("SSR",sum(res**2))





