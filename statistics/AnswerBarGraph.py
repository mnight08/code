# -*- coding: utf-8 -*-
"""
Created on Wed May 10 14:37:27 2017

@author: vpx365
"""

from collections import Counter
import matplotlib.pyplot as plt


data=['C','B','A','B','D','D','B','C','B','A','D','C','B','D','A','B','E','A','A','B']

c=Counter(data)
A=c['A']
B=c['B']
C=c['C']
D=c['D']
E=c['E']

bar_heights=(A,B,C,D,E)

x=(1,2,3,4,5)
width=.4
fig,ax=plt.subplots()
ax.bar(x,bar_heights,width)

