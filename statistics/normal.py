from scipy.stats import norm as n;
import matplotlib.pyplot as plt;
import numpy as np;
#create a figure that contains two sub plots.  Changing the 2 to a 3 will give 
#subplots.  First number is number of rows and sencond is number of columns in
#table of subplots.
fig,ax=plt.subplots(3,1);


#parameters for the normal distribution
mean,stdv=3,1;



#create a plot of pdf
#create the x axis
x=np.linspace(n.ppf(0.0001,mean,stdv),n.ppf(0.99999,mean,stdv),100)

#plot dots at height of probability mass above each point.
ax[0].plot(x,n.pdf(x,mean,stdv), 'bo', ms=2, label='binom pmf')

#create a plot of cdf
ax[1].plot(x,n.cdf(x,mean,stdv), 'bo', ms=2, label='binom cdf')


#create some random data and plot.
data=n.rvs(mean,stdv,size=1000)

ax[2].hist(data, normed= True, histtype='stepfilled', alpha=.2)
