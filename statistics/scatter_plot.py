# -*- coding: utf-8 -*-
"""
Created on Wed Mar  1 15:28:27 2017

@author: vpx365
"""
import numpy as np
import matplotlib.pyplot as plt
#input our data.  x is where the data was observed
#y is the observed value.
x=[-6,-5,-4,-3,-2,0,1,2,3,4,5,6]
y=[36,25,16,9,4,0,1,4,9,16,25,36]


num_ticks=8
buffer=.5
step=(max(x)-min(x)+2*buffer)/(num_ticks-1)

#Create scatter plot of the data
plt.scatter(x, y, s=25, alpha=.5)

plt.axis([min(x)-buffer, max(x)+buffer, min(y)-buffer,max(y)+buffer])


#Add labels
plt.xlabel('x')
plt.ylabel('y')
plt.title('Scatter Plot')


#these are the numbers on the x-axis that
#we will predict y at.
xticks=np.arange(min(x)-buffer, max(x)+buffer+step, step);

plt.xticks(xticks)
plt.grid(True)

#Show the plot
plt.show()
