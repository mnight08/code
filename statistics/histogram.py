import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt

##############################################################################
#Continuous histogram
mu, sigma = 100, 15
x = mu + sigma*np.random.randn(50)

# the histogram of the data
n, bins, patches = plt.hist(x, 5, facecolor='green', alpha=0.75, align='mid')


plt.xticks(bins)
plt.ylabel('Frequency')
plt.title(r'$\mathrm{Histogram\ of\ IQ:}\ \mu=100,\ \sigma=15$')
plt.axis([40, 160, 0,20])
plt.grid(True)

plt.savefig("continuous_hist.png")


#############################################################################
#Discrete Histogram
plt.cla()
labels=[2,3,4,5,6]
data=[2,2,2,2,3,3,3,4,5,5,6]



n, bins, patches = plt.hist(data, 6, facecolor='green', alpha=0.75, align='left')


plt.xticks(labels)
plt.ylabel('Frequency')
plt.title(r'$\mathrm{Histogram\ of\ Siblings}$')
plt.axis([0, 7, 0,5])
plt.grid(True)


plt.savefig("discrete_hist.png")
