import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import scipy.stats as stats
##############################################################################
#Continuous histogram
mu, sigma = 100, 15
x = stats.norm.rvs(mu, sigma, size=100)

# the histogram of the data
n, bins, patches = plt.hist(x, 5, facecolor='green', alpha=0.75, align='mid')


plt.xticks(bins)
plt.ylabel('Frequency')
plt.title(r'$\mathrm{Histogram\ of\ IQ:}\ \mu=100,\ \sigma=15$')
plt.axis([40, 160, 0,20])
plt.grid(True)

plt.show()

