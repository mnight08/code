# -*- coding: utf-8 -*-
"""
Created on Wed Mar  1 15:31:05 2017

@author: vpx365
"""
import numpy as np
import matplotlib.pyplot as plt

x=[-6,-5,-4,-3,-2,0,1,2,3,4,5,6]
y=[36,25,16,9,4,0,1,4,9,16,25,36]




#The line of best fit
line=np.poly1d(np.polyfit(x, y, 1))

#Residual plot.
yhat=line(x)
res=yhat-y


plt.scatter(x, res)

num_ticks=8
buffer=.5
step=(max(x)-min(x)+2*buffer)/(num_ticks-1)



plt.axis([min(x)-buffer, max(x)+buffer, min(res)-buffer,max(res)+buffer])

plt.plot(xticks,np.zeros(len(xticks)))


#Add labels
plt.xlabel('x')
plt.ylabel('residual')
plt.title('Residual Plot')


xticks=np.arange(min(x)-buffer, max(x)+buffer+step, step);

plt.xticks(xticks)
plt.grid(True)

#Show the plot
plt.show()


