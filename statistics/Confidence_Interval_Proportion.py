# -*- coding: utf-8 -*-
"""
Created on Fri Jan 27 16:28:34 2017

@author: vpx365
"""

import scipy.stats as stats;
import math
#Using sample proportion
n=1800
p=486/n;

alpha=.05;


#critical value.
zalpha2=stats.norm.ppf(1-alpha/2,0, 1)
MOE=math.sqrt(p*(1-p)/n)*zalpha2
#Confidence interval
CI=[p- MOE,p+MOE]


#Using sample standard deviation.  Use t distribution
n=550
p=340/n;

alpha=.05;


#critical value.
zalpha2=stats.norm.ppf(1-alpha/2,0, 1)
MOE=math.sqrt(p*(1-p)/n)*zalpha2
#Confidence interval
CI=[p- MOE,p+MOE]

