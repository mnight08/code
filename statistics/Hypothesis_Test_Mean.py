# -*- coding: utf-8 -*-
"""
Created on Fri Jan 27 17:54:04 2017

@author: vpx365
"""
import math
import scipy.stats as stats
#Right tailed
mu0=77
xbar=78.14;
s=1.11;
n=10;
alpha=.01
df=n-1;


#test statistic
t0=(xbar-mu0)/(s/math.sqrt(n))

#critical value
tcrit=stats.t.ppf(1-alpha,df)



#p value
p=1-stats.t.cdf(t0,df)


#Right tailed
mu0=21
data=[21,20,23,25]
[n, minmax,xbar, var, skew, kurt]=stats.describe(data)

s=math.sqrt(var);
alpha=.01
df=n-1;


#test statistic
t0=(xbar-mu0)/(s/math.sqrt(n))

#critical value
tcrit=stats.t.ppf(1-alpha,df)



#p value
p=1-stats.t.cdf(t0,df)


#Right tailed
mu0=62.8
data=[65.4,66.2,62.1,62.9,67.3,66.7,64.6,65.1,63.1,65.7]
[n, minmax,xbar, var, skew, kurt]=stats.describe(data)

s=math.sqrt(var);
alpha=.05
df=n-1;


#test statistic
t0=(xbar-mu0)/(s/math.sqrt(n))

#critical value
tcrit=stats.t.ppf(1-alpha,df)



#p value
p=1-stats.t.cdf(t0,df)


