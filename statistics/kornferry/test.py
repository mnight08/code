# -*- coding: utf-8 -*-
"""
Created on Mon Nov 26 16:56:02 2018

@author: vpx365
"""

"""Problem: A study was conducted to see the effect of coupons on purchasing 
habits of potential customers. In the study, 1,000 homes were selected and a 
coupon and advertising material for a particular product was sent to each home. 
The advertising material was the same but the amount of the discount on the 
coupon varied from 5% to 30%. The number of coupons redeemed was counted. 
Below is the data. 
 

Price Reduction (X), Number of Coupons (n), Number  Redeemed (Y), Proportion Redeemed (p)

5, 200, 32, 0.160

10, 200, 51, 0.255

15, 200, 70, 0.350

20, 200, 103, 0.515

30, 200, 148, 0.740


 

a). Fit a simple linear regression to the observed proportions.
   
    Here the predictor is X, and the predicted is p.


b). Fit a simple linear regression of the logit transformed proportions on the price reduction.

   
    Here the predictor is X, and the predicted is  odds ratio p/(1-p).

c). Fit a logistic regression of the proportion redeemed on the price reduction.

   
    Here the predictor is the proportion redeemed and, and the predicted is 
    price reduction.


d). Compare the 3 regression equations and price reductions to get a 25% redemption rate.
 for the first two, take the model equation p=mx+b, then plug in .25 to p, and solve
 x=(0.25-b)/m
    
"""


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from sklearn.linear_model import LinearRegression
from sklearn.linear_model import LogisticRegression


df_raw=pd.DataFrame(data={"X":[5,10,15,20,30], 
                 "n":[200,200,200,200,200], 
                 "Y":[32,51,70,103,148], 
                 "p":[0.160,0.255,0.350,0.515,0.740]})


x_pred=df_raw["X"].values.reshape(-1,1)
x_train=x_pred.copy()

simple_linear_raw=LinearRegression().fit(x_train,df_raw["p"])

plt.plot(df_raw["X"], df_raw["p"], 'o', color='black')

plt.plot(df_raw["X"], simple_linear_raw.predict(x_pred))

plt.figure()



#Transform p to odds ratio
df_logit=df_raw.copy()
df_logit['p']=df_logit['p']/(1-df_logit['p'])
simple_linear_logit=LinearRegression().fit(x_train,df_logit[["p"]])
plt.plot(x_train, df_logit["p"], 'o', color='black')
plt.plot(x_pred, simple_linear_logit.predict(x_pred))
plt.figure()


#Fit p on price reduction X.
logistic=LogisticRegression().fit(df_raw[["p"]],df_logit[["X"]], 
                           sample_weight=[200, 200, 200, 200, 200])
p_pred=np.linspace(.160,.740, 200).reshape(-1,1)



plt.plot(p_pred, logistic.predict(p_pred), 'o')
plt.plot(df_raw["p"], df_raw["X"], 'o', color='black')

m1=simple_linear_raw.coef_
b1=simple_linear_raw.intercept_
print("Raw ",(.25-b1)/m1)


m2=simple_linear_logit.coef_
b2=simple_linear_logit.intercept_

print("Logit", (.25-b2)/m2)


print("logistic",logistic.predict(.25))