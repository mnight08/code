# -*- coding: utf-8 -*-
"""
Created on Tue May 30 19:25:44 2017

@author: vpx365
"""
from PyPDF2 import PdfFileReader
import os
import sys
def cut(filename, startpg, endpg):
    RESOURCE_ROOT = os.path;
    with open(os.path.join(RESOURCE_ROOT, 'grescores.pdf'), 'rb') as inputfile:
        # Load PDF file from file
        ipdf = PdfFileReader(inputfile)
        ipdf_p1 = ipdf.getPage(0)
