#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
class job
{
public:
	int startime;
	int endtime;
	bool operator <(job a){return this->startime<a.startime;}


};


int main()
{
	job a,b,c,d,e,f,g,h,i,j;
	a.startime=13, a.endtime=42;
	b.startime=2, b.endtime=24;
	c.startime=6, c.endtime=23;
	d.startime=65, d.endtime=80;
	e.startime=5, e.endtime=6;
	f.startime=4, f.endtime=9;
	g.startime=9, g.endtime=23;
	h.startime=3, h.endtime=6;
	i.startime=9, i.endtime=17;
	j.startime=32, j.endtime=40;
	vector<job> joblist;
	joblist.push_back(a);
	joblist.push_back(b);
	joblist.push_back(c);
	joblist.push_back(d);
	joblist.push_back(e);
	joblist.push_back(f);
	joblist.push_back(g);
	joblist.push_back(h);
	joblist.push_back(i);
	joblist.push_back(j);
	
	sort(joblist.begin(),joblist.end());

	vector<job> machine1;
	vector<job> machine2;
	machine1.push_back(joblist[0]);
	//if schedule is doable, then this will work( ie, only 2 intersections
	for(int i=1;i<joblist.size();i++)
	{
		if(joblist[i].startime<machine1.back().endtime)
			machine2.push_back(joblist[i]);
		else machine1.push_back(joblist[i]);

	}

	return 0;
}