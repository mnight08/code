%take a sampled square version of our operator, and the number of steps to
%iterate, the vector y to act on, and the identity and several terms in the series
function Y = NeumannSeries(X,k,S,I)
%NEUMANNSERIES Summary of this function goes here
%   Detailed explanation goes here
%the idea is that 1+x+x^2+...x^((2^(k+1))-1)=(1+x)(1+x^2)(1+x^4)...(1+x^(2^(k)))
%the order we multiply these terms does not matter, since the only two
%elements we are dealing with x and 1 are commutative with each other

%recursive version
if k~=0
    Y=NeumannSeries(X^2,k-1,(I+X)*S,I);
else 
    Y=S;

end

