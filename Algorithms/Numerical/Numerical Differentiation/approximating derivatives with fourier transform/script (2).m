close all;
clear all;
testfunc=@airy;
x=0:.1:2*pi;

f=testfunc(x);

plot(x,f);
title('original function');

figure;
fp=approximate_derivative(f,x,x);


plot(x,real(fp));
title('functions derivative');

fpp=approximate_derivative(fp,x,x);
figure

plot(x,real(fpp));
title('functions second derivative');