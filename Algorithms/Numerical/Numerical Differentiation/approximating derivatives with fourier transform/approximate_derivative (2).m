function Derivative=approximate_derivative(f,x,z)
%approximate the  sampled function f using fourier transform

Derivative=zeros(size(f));
D=zeros(size(f));
N=size(f);
N=max(N);
M=size(z);
M=max(M);
w=-1000:1:1000;
R=size(w);
R=max(R);
K=zeros(size(w));
i=1;
while i<=M
    j=1;
    while j<=N
        k=1;
        while k<=R
        K(k)=exp(-1i*w(k)*(x(i)-z(j)))*(-1i*w(k));
        k=k+1;
        end
        D(j)=trapz(K,w);
        j=j+1;
    end
    Derivative(i)=trapz(D.*f,x);
    i=i+1;

end



