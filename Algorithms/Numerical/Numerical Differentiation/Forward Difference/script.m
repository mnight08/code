clear all;
close all;
h=.01;
a=-1.5;
b=1.5;
sample_set=a:h:b;
f=test_function(sample_set);
f_prime=differentiate(f,sample_set,1);

plot(sample_set,f);
figure;
plot(sample_set,f_prime);
figure;
