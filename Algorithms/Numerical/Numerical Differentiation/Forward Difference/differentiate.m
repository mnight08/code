function derivative = differentiate( f,x,n )
%forward difference for the first N-1 elements,
%backward difference for last element
N=max(size(f));
k=1;
derivative=f;
dx=diff(x);
while k<=n
dy=diff(derivative);
derivative(1:N-1)=dy./dx;
derivative(N)=(derivative(N)-derivative(N-1))/(x(N)-x(N-1));
k=k+1;
end

end