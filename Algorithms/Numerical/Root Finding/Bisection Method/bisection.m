function out=bisection(a,b,f,relerror)
%this is a comment, to set f in the command line we write
%f=inline('x^-1-2^x','x'), then call bisection(0,1,f)
x=0;
while abs(f(b)-f(a)) > relerror*abs(b)
    x = (a + b)/2;
    if sign(f(x)) == sign(f(b))
        b = x;
    else
        a = x;
    end
end
out=x;
end