function [out, steps] = newtonsmethod( f,fprime,xprev,tol)
%takes in a function, its derivative, and a guess, and xprev
%to get out put set, [x,y]=newtonsmethod(f,fprime,guess)
k=0;
x=xprev - f(xprev)/fprime(xprev);
while abs(f(x) - f(xprev)) > tol*abs(x)
xprev = x;
x = xprev - f(xprev)/fprime(xprev);
k=k+1;
end
out=x;
steps=k;
end

