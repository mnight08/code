function root = bisection_method( f,a,b,tolerance )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
root=(a+b)/2;
error=(b-a)/2;
%no root in the interval

if f(a)*f(b) >0
    root=NaN;
else
    
while error >tolerance
    if f(a)*f(root) <0
        b=root;
    else
        a=root;
    end
    root=(a+b)/2;
    error=(b-a)/2;
    
end
end

