import "vector.py"
import <cmath>

def class Matrix(self,vector):

    def __init__(self,int m, int n)
        self.num_rows=m;
        self.num_cols=n;
        self.entry=[[complex(0,0) for j in range(0,n)] for i in range(0,m)]
    
    def __add__(self,Matrix B):
        s.num_rows=self.num_rows
        s.num_cols=self.num_cols
        for i in range(0,self.num_rows):
            for j in range(0,self. num_cols):
                s.entry[i][j]=self.entry[i][j]+B.entry[i][j]
        return s;

    #might be worth doing strassen here
    def __mul__(self,Matrix B):
        for i in range(0,self.num_rows):
            for j in range(0,B.num_cols):
                self[i][j]=self[i][j]+B[i][j]
    def __rmul__(self,):


        
    def __sub__(self,Matrix B):
        s.num_rows=self.num_rows
        s.num_cols=self.num_cols
        for i in range(0,self.num_rows):
            for j in range(0,self. num_cols):
                s.entry[i][j]=self.entry[i][j]-B.entry[i][j]
        return s;

    def transpose(self):
        s.num_rows=self.num_rows
        s.num_cols=self.num_cols
        for i in range(0,self.num_rows):
            for j in range(0,self. num_cols):
                s.entry[i][j]=self.entry[j][i]
        return s;

    def conjugate(self):
        s.num_rows=self.num_rows
        s.num_cols=self.num_cols
        for i in range(0,self.num_rows):
            for j in range(0,self. num_cols):
                s.entry[i][j]=self.entry[i][j].real-self.entry[i][j].imag*1j
        return s;

    def conjugate_transpose(self):
        return self.transpose().conjugate();
    def print(self):
        pass
    #concatenate two matrices.
    def concat(self,over="col", Matrix b):
        pass

    def split(self,over="col", i ):
        #split over row or col depending on over. 
        #By default i is the position between col i-1 and i
        #that will matrix will be split over.
        if(over =="col"):
            return [A,B]
        pass



    def swap_rows(self,i,j):
        pass
    def swap_cols(self,i,j):
        pass
    def replace_row(rowi, rowj):
        pass
    def col(self,j):
        pass
    def row(self,i):
        pass

    def decomposeLU(self):




        return [L,U]
    def decomposeCholesky(self):
        pass

    #Return the least square solution to Self*x=b
    def leastSquares(self, x, b):
        pass
    def pseudoInverse(self):
        pass
    def inverse(self):
        pass;

    
    def minor(self, i, j):
        pass

    #return a list of the eigen values 
    def eigenvalues(self):
        pass

    def trace(self):
        pass
    def decomposeQR(self):
        pass
    
    def gaussJordan(self):
        pass


    def gauss(self,Matrix A, Vector b):
        #Pick the pivot, then use it to eliminate each row below it 
        #by multiplying pivot row by leading coefficient, and 
        #the row being eliminated by the leading coeficient fo the pivot
        Ag=A.concat(b);

        #Eliminate
        for pivot in range(0,A.num_rows):
            j=1;
            #Find a row with nonzero value in pivot position
            #and swap with pivot row.  If one is not found, 
            #then move pivot.
            while Ag(pivot,pivot)==0 and j!=Ag.num_rows: 
                if(Ag(pivot+j,pivot) !=0)
                    Ag.swap_rows(pivot,pivot+j)     

            #eliminate the variable in column j from the rows below 
            #the pivot row.
            for row in range(pivot, Ag.num_rows):
                #replace the ith row with the vector 
                #A(self,pivot,j)*A.row(pivot)-A(rowi,j)*A.row(pivot)
                if(Ag(row,pivot)!=0):
                    Ag.replace(row, Ag(pivot,pivot)*Ag.row(row)-Ag(row,pivot)*Ag.row(pivot))
    

        x=Ag.col(A.num_cols)
        
        #Solve for x.
        #Assume a square matrix.
        for row in range(Ag.num_rows, 0):
            x(row)=x(row)/Ag(row,)



        
        
        #At this point, A is in row reduced echelon form. 
        return [Ainv, x]



    def 



