#include <iostream>
#include <string>
using namespace std;


//of course if the size entered is larger than the size of int64, this will fail badly
_int64 countones(_int64 number, _int64 ones=0)
{
	if (number==0)
	return ones;
	else 
		countones(number/2, ones+number%2);
}

int main()
{
	string exit="";
	_int64 number=0;
	const string bye="no";
	while(exit!=bye)
	{
		
		cout<<"enter a number:";
		//attempt to catch a few errors, not perfect, but should stop the program sooner than normal.
		if(!cin>>number){
			cout<<endl<<"REALLY?"<<endl;
			int i=0;
			string swap="";
			cout<<"FAIL!!"<<endl;
			::exit(1);
						
		}
		cin>>number;

		//:: displays global functions, saves time with typing
		cout<<"there are "<<::countones(number)<<" ones in the binary representation of that number"<<endl;
		cout<<"would you like to continue?(yes/no):"<<endl;
		cin>>exit;
	}

	
}