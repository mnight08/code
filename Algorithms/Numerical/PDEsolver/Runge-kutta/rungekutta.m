function rk= rungekutta(y0, t0, ft, h)
t=t0;
i=1;
total= (ft-t0)/h;
y = zeros(total,3);
%plot3 = zeros(1,total);
while ft>t
t=t+h;
y(i,1)=y0(1); y(i,2)=y0(2);y(i,3)=y0(3);
k1=h*lorenzatt(y0);
k2=h*lorenzatt(y0+k1/2);
k3=h*lorenzatt(y0+k2/2);
k4=h*lorenzatt(y0+k3);
y0=y0+(k1+2*k2+2*k3+k4)/6
i=i+1;
end
plot3(y(:,1),y(:,2),y(:,3));
end
