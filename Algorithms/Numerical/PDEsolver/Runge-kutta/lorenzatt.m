function dy = lorenzatt(y)
    alpha = 28; sigma = 10; beta = 8/3;
    dy = [0 0 0];
    dy(1) = sigma*(y(2) - y(1));
    dy(2) = y(1)*(alpha - y(3)) - y(2);
    dy(3) = y(1)*y(2) - beta*y(3);
    return
end