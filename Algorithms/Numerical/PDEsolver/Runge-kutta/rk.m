function y=rk(f1,f2,f3,t0,y10,y20,y30,h,tf)

tn=t0;
a=1;
numberofpoints=(tf-t0)/h;
plotme1=zeros(1,numberofpoints);
plotme2=zeros(1,numberofpoints);
plotme3=zeros(1,numberofpoints);
times=zeros(1,numberofpoints);
y=[y10,y20,y30];
ynp=[y10,y20,y30];
k1=[f1(tn,y), f2(tn,y),f3(tn,y)];
k2=[f1(tn+(1/2)*h,y+(1/2)*h*k1),f2(tn+(1/2)*h,y+(1/2)*h*k1),f3(tn+(1/2)*h,y+(1/2)*h*k1)];
k3=[f1(tn+(1/2)*h,y+(1/2)*h*k2),f2(tn+(1/2)*h,y+(1/2)*h*k2),f3(tn+(1/2)*h,y+(1/2)*h*k2)];
k4=[f1(tn+h,y+h*k3),f2(tn+h,y+h*k3),f3(tn+h,y+h*k3)];
while tn < tf
    plotme1(a)=y(1);times(a)=tn;plotme2(a)=y(2);plotme3(a)=y(3);
    ynp=y;
    y=ynp+(1/6)*h*(k1+2*k2+2*k3+k4);
    tn=tn+h;
k1=[f1(tn,y), f2(tn,y),f3(tn,y)];
k2=[f1(tn+(1/2)*h,y+(1/2)*h*k1),f2(tn+(1/2)*h,y+(1/2)*h*k1),f3(tn+(1/2)*h,y+(1/2)*h*k1)];
k3=[f1(tn+(1/2)*h,y+(1/2)*h*k2),f2(tn+(1/2)*h,y+(1/2)*h*k2),f3(tn+(1/2)*h,y+(1/2)*h*k2)];
k4=[f1(tn+h,y+h*k3),f2(tn+h,y+h*k3),f3(tn+h,y+h*k3)];
if(y-ynp==0)
    break;
end
a=a+1;
end

figure;
plot(plotme1,times);
figure;
plot(plotme2,times);
figure;
plot(plotme3,times);
figure;
plot(plotme1,plotme3);
figure;
plot(plotme2,plotme3);
figure;
plot(plotme1,plotme2);
plot3(plotme1,plotme2,plotme3);
end