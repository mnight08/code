function plot_origional_image(image_width,image_height,image_resolution,imaging_grid,reflectivity)
Origional_image=zeros(image_width/image_resolution,image_height/image_resolution,3);
z1=1;
while z1<=image_width/image_resolution
    z2=1;
    while z2<=image_width/image_resolution
       Origional_image(z1,z2,1)=norm(reflectivity(z1,z2));
       Origional_image(z1,z2,2)=Origional_image(z1,z2,1);
       Origional_image(z1,z2,3)=Origional_image(z1,z2,1);
       z2=z2+1; 
    end
    
    z1=z1+1;
end
plot_image(Origional_image,image_width,image_height,image_resolution,imaging_grid);


colorbar
title('origional image');
figure;

end