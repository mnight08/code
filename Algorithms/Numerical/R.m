function [sol,ratio]= R(tol, p ,f,fprime,xprev )
%RN Summary of this function goes here
%   Detailed explanation goes here
%tol is the tolerance, p is the power of the denominator when computing the
%ratio, f, is the function, fprime is its derivatrive, and xprev is an
%initial guess  t
x=xprev - f(xprev)/fprime(xprev);
while abs(f(x) - f(xprev)) > tol*abs(x)
xprev = x;
x = xprev - f(xprev)/fprime(xprev);


sol=x;
xnout=(log(2)-x);
xn1out=((log(2) - xprev)^p);
%stops if either is too small, 
if(xnout==0||xn1out==0)
    break;
end
%needs to be abs of bottom otherwise it gives a complex number for
%non-integer powers.
ratio=(log(2)-x)/((abs(log(2) - xprev))^p)

end
end

