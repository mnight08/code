#include <iostream>
#include <vector>
using namespace std;

class point
{
	public:
	double x;
	double y;
	point(double tx, double ty)
	{
		setPoint(tx,ty);
	}
	point()
	{
		x=y=0;
	}
	void setPoint(double tx, double ty)
	{
		x=tx;
		y=ty;
	}
};

class quadtree{
	//this is a region. Each region holds points in its list until it 
	//has too many. Then it spreads them among
	//its sub regions.

	private:
	bool isParent;
	double capacity;
	double width;
	double length;
	point center;
	vector<point> points;
	quadtree* subregions;

	public:
	bool parent()
	{
		return isParent;
	}
	quadtree(){
		isParent=false;
		subregions=NULL;
	}

	quadtree(double x,double y,double l,double w,int c){
		setCenter(x,y);
		setDimension(w,l);
		capacity=c;
		subregions=NULL;
		isParent=false;
	}
	void splitTree()
	{
		point subcenter;
		isParent=true;
		subregions=new quadtree[4];
		//create the children as new quadtrees
		for(int quadrant=0;quadrant<4;quadrant++){
			subcenter=getCenterOfQuadrant(quadrant);
			subregions[quadrant].initializeQuadtree(subcenter.x,subcenter.y,length/2,width/2,capacity);
		}
		//Go through the points and redistribute into subregions
		//We could deallocate the array here, or only push the new points down
		//the tree, but that makes pairing leaves more complicated.
		point t;
		while(!points.empty()){
			t=points.back();
			points.pop_back();
			subregions[getDirection(center,t)].insert(t);
		}

	}
	void initializeQuadtree(double x,double y,double l,double w,int c){
		setCenter(x,y);
		setDimension(w,l);
		capacity=c;
		subregions=NULL;
		isParent=false;

	}

	void setCenter(double x, double y){
		center.x=x;
		center.y=y;
	}
	
	//width is horizontal length
	//length is the vertical length
	void setDimension(double w, double l){
		width=w;
		length=l;
	}

	int getDepth(){
		
		if(isParent)
		{
			int depth0=subregions[0].getDepth();
			int depth1=subregions[1].getDepth();
			int depth2=subregions[2].getDepth();
			int depth3=subregions[3].getDepth();
			return 1+max(max(depth0,depth1),max(depth2,depth3));

		}
		else return 0;
	}
	int getSize(){
		if(!isParent){
			return points.size();
		}
		else {
			int size0=subregions[0].getSize();
			int size1=subregions[1].getSize();
			int size2=subregions[2].getSize();
			int size3=subregions[3].getSize();
			return size0+size1+size2+size3;
		}
	}
	point getCenterOfQuadrant(int quadrant){
		double absoluteoffsetx=length/4;
		double absoluteoffsety=width/4;
		//nw
		if(quadrant==0){
			return point(center.x-absoluteoffsetx,center.y+absoluteoffsety);
		//ne	
		}else if (quadrant==1) {
			return point(center.x+absoluteoffsetx,center.y+absoluteoffsety);
		//sw	
		}else if (quadrant==2) {
			return point(center.x-absoluteoffsetx,center.y-absoluteoffsety);
		//se	
		}else if (quadrant==3) {
			return point(center.x+absoluteoffsetx,center.y-absoluteoffsety);
		}
	}

	//insert the point.  returns false if unable to insert
	//does not check handle repeated points yet, at least not beyond 
	//capacity.  Double points will lead to a stack overflow probably.
	bool insert(point &p){
		//check if is a parent 
		if(isParent){
		//insert the new point.
			return subregions[getDirection(center,p)].insert(p);
		}else if (points.size()<capacity) {
			points.push_back(p);
			return true;
		}else{
			splitTree();
			return subregions[getDirection(center,p)].insert(p);		
		}	
	}

	//the direction p is from q
	int getDirection(point& q,point& p){
		//nw
		if(q.x>=p.x&&q.y<p.y)
			return 0;
		//ne
		else if((q.x<p.x&&q.y<=p.y)||(q.x==p.x&&q.y==p.y))
			return 1;
		//sw
		else if(q.x>p.x&&q.y>=p.y)
			return 2;
		//se
		else if(q.x<=p.x&&q.y>p.y)
			return 3;
		else {

			cout<<"not found!!!"<<"p is" <<" ("<<p.x<<","<<p.y<<") "<< "and q is"
				<<" ("<<q.x<<","<<q.y<<") "<<endl;
		return -1;
		}
	}

	//This could be sped up by returning a pointer instead.
	quadtree* getSubtree(int quadrant){
		if(isParent)
			return &subregions[quadrant];

	}

	void display(){
		if (isParent) 
			for( int quadrant=0;quadrant<4;quadrant++)
				subregions[quadrant].display();
		else {	
			for(int i=0;i<points.size();i++)
				cout<<" ("<<points[i].x
					<<","<<points[i].y<<") ";
		}
	}
};
