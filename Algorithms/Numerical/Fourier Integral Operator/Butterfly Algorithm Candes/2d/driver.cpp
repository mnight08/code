#include <iostream>
#include <string>
#include "quadtree.h"
#include "input.h"
double phi(double x1,double x2, double y1,double y2);
double f(double y1, double y2);
double psi(double x1,double x2, double y1,double y2)
//The point of this program is to implement the fast fourier integral
//using the butterfly algorithm.  This is following the work of 
//Candes et. al in 2011
//The problem we are evaluating is 
//I(x)=sum_{Omega} exp(2pi i phi(x,k))f(k) 
//for all x\in X, where Omega={(k1,k2),-N/2\le k1,k2<N/2}, and 
//X={(i1/N,i2/N),0\le i1,i2<N}.

	
int main(int argc, char *argv)
{
	//The interval endpoints for the x domain
	double A=-10;
	double B=10;
	
	//the interval endpoints for the y(integration domain)
	double C=-10;
	double D=10;

	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//by default the function is not sampled.  If the function is 
	//already sampled, then we do not need to sample, and need the 
	//sample points.  Right now this part will not function 
	//since I have not thought it out enough. !!!!!!!!!!!!!!!!!!!
	bool Sampled=false;

	//First we need to discretize the integral.  For simplicity we
	//will use uniform samples in the integration variable.
	//We also need to pick the points we will evaluate
	//the integral at in the evaluation domain.
	//number of sample points.  Both the integration and
	//evaluation domain will be 
	//sampled with the same number of points.  This might be altered
	//latter
	int N=256;
	
	//Sample Positions(in integration domain).  This
	//is with uniform samples at the left end point.
	double *Xs;
	Xs=new double[N];
	for(int i=0;i<N;i++){
		Xs[i]=A+i*(B-A)/N;
	}

	//Sample Positions(in Evaluation domain).  This
	//is with uniform samples at the left end point.
	double *Ys;
	Ys=new double[N];
	for(int i=0;i<N;i++){
		Ys[i]=C+i*(D-C)/N;
	}


	if (!sampled) {
		
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
	//At this point we need to decide whether N is too large to 
	//sample the functions at.  If N is too large, we
	//will just make calls to the functions as we need them.
	//If not, then we will create an array for the samples
	double *Fs=new double[N];
	for(i=0;i<N;i++){
		Fs[i]=F(Xs[i]);
	}
	//We may never need to sample the amplitude and phase
	//since the algorithm may require evaluation away from 
	//sample points.  But if we need to apply the operator to a 
	//large number of different functions, then sampling the 
	//phase and amplitude may lead to a significant speed up,
	//since there would not be any redundant computations.  
	//If a point does not lie on a sample points, then we could
	//always interpolate, but that might be a bit subtle to handle
	double as[N][N];
	double phis[N][N];
	}

	//The algorithm is as follows:
	//0.)Convert to polar coordinates.
	//1.)Create a dyadic partitioning of the domain and range 
	//recursively until the width is less than 1/N.  We should have two
	//trees at this point.  Tx and Tp are the trees in evaluation and integrations domains.
	//2.)Starting at the root of Tx Compute the equivalent weights for the entire evaluation
	//domain paired with each leaf in the integration domain.  There should be N sets of weights.  This can be computed 
	//directly.
	//3.)Recursively compute the weights for the next levels(down) in Tx paired with the next levels(up) 
	//in Tp.
	//4.)When we are at the bottom of Tx, we are approximating the FIO using the entire integration domain, for only a small number
	//of points in the evaluation domain with each set of weights.  
	
	//Here is what we need to store.  
	//1.) At each level we need for each pair q weights.  
	//This gives a total of qN numbers.
	//2.) To compute the weigths at any level, we need the weigths from the previous level.  
	//This means we need 2qN numbers.


	//take the sorted list Xs and create a balanced binary tree. 
	//!!What we need from the tree is a spot to store an equivalent weight for 
	//each node.
	QuadTree Tx(Xs);
	
	
	//The number of levels in the tree.
	L=log2(N);


	for(i=0;i<L;i++)
	
	
	
	
	
	
	
	
	
	
	
	return 0;




}
