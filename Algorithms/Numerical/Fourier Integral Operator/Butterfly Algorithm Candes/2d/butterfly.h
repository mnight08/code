#include "input.h"
#include <iostream>

//The t-th Lagrange basis polynomial for the grid evaluated at the point x
inline double L1d(double *grids ,int q,int l, int Box, int t,double x){
	double product=1;
	int other_grid_point_index;
	int this_basis_grid_point_index=getGridIndex(l,B,q,t);
	for(int j=1;j<t;j++){
		other_grid_point_index=getGridIndex(l,B,q,j);
		product=product*(x-grid[other_grid_point_index])/(grid[this_basis_grid_point_index]-grid[other_grid_point_index]);
	}
	//skip j=t
	j++;
	for(;j<q;j++){
		product=product*(x-grid[other_grid_point_index])/(grid[this_basis_grid_point_index]-grid[other_grid_point_index]);
	}
	return product;
}

//Tensor interpolation L_t1(y.x)*L_t2(y.y).  Can be sped up using ISAT, or just storing high res copy of L, and interpolating(spline or linear).
//Assumes that t goes up to q^2, and that B goes to M^2, so that t1, t2 are between 0, and q-1,
//and B1, and B2 are between 0, and M-1
inline double L(double *grids, int q, int t, int B,int M, int l,point y){
	int t1=t/q;
	int t2=t%q;
        int B1=B/M;
	int B2=B%M;
        return L1d(grids,q,l,B1,t1,y.x)*L1d(grids,q,l,B2,t2,y.y);
	return 0;
}

inline  void swapweights(complex<double>* current, complex<double>* previous){
	complex<double>* temp;
	temp=previous;
	previous=current;
	current=temp;	
}

inline void zeroweights(complex<double> * weights,int M){
	for(int n=0;n<M;n++)weights[n]=0;
}


//return the center of the box from the left at level level. 
inline double center1d(int level, int box){
	double scale=pow(2,(double)-level);
	return (int)(scale*((double)box+0.5));
}

inline point center(int l, int box, int M){
	b1=box/M;
	b2=box%M;
	return point(center1d(l,b1),center1d(l,b2));
}


//We need to enumerate the boxes, and pairings we are working with.  We need to be consistent.  At scale/depth l in X
//and L-l in Y, there are 4^l boxes in X, and 4^{L-l} in Y.  There are 
//4^L=N^2 pairings for each level.  Given a number n=0,...N^2-1, we identify it at level l, with the
//pair (A,B) with A=0,...,4^l-1, and B=0,...,4^{L-l}-1 by the map n->(n/4^l-1,4n^l)
//This amounts to taking the first 2l bits of n to be the box A, and the last 2(L-l) bits to
//be B.
//there 
//We always count from right to left, top down, so that we would start around (0,0), and end around
//(1,1) 
inline int getA(int n, int l){
	return n>>2*l;

}

//shift n to the left, so that only the last 2*(L-l) digits are non zero, then shift back so that the first 2l bits are zero.
inline int getB(int n, int l, int L){
	return (n<<2*(L-l))>>2*(L-l);
}

inline int getParent(int A){
	return A>>2;
}

inline int getChild(int B,int l,int c){
    return B<<2+c
}

inline point getGridPoint(double * grids, int l, int B, int M, int q, int t ){
	int t1=t/q;
	int t2=t%q;
	int B1=B/M;
	int B2=B%M;
	point x;
        x.setPoint(grids[getGridIndex(l,B1,q,t1)] ,grids[getGridIndex(l,B2,q,t2)])
        return x;
}
 //for 1d grid
inline int getGridIndex(int l, int B,int q,int t){
	return ((pow(2.0,l)-1)+B)*q+t
}

//return the first index of a grid at the scale given for 1d
inline int getGridIndex(int B,int q, int l){
	return 	getGridIndex(B,q,l,0);
}

//There are N*r_eps weights to index.  Here, B needs to have at most 2l bits 
inline int getWeightIndex(int A, int B, int l, int r_eps, int t){
	return ((A<<2*l)+B)*r_eps+t;
}

//If we assume that
//the region we are integrating over is [0,1].  If we are in the left half of the x axis , then the first bit after the decimal point of the x component of the
//point must be zero, otherwise this is 1.  Similarly if we are in the right half, the bit must be 1.  
//To see which half of the half we are in, we look at the next bit.  Basically, this means that the box that a point is in is completely determined by
//the first L bits of its components.  We can always do a substitution so that the integration is over [0,1].
//for all t,  and A with A at coarsest(only one A)
//assuming x, and y are in [0,1].  L is the depth of tree.
inline int getBox(point p, int L){
	//the number of columns at the finest level
	int S=pow(2.0,(double) L);
	//shifts x, and y left by L bits by multiplying by 2^L.
	int column =(int) p.x*S;
	int row    =(int) p.y*S;
	//this keeps the convention that the point at (0,0) would be box 0, and (1,1) would be box S^2
	return row*S+column;
}

//Fills grids with all the 1d grids for [0,1] we will need. There are 2N-1 grids, and each has q entries.
//The function getGridIndex returns the appropriate index.  We use a tensor product to form higher dimensional grids.
//If this is slow, then you should rewrite this part, it
//can be made faster.
inline void buildGrids(double *grids,int L, int N,int q){
	grids= new double[(2*N-1)*q];
	//make the first q entries the chebyshev grid adapted to [0,1].
	protogrid(grids,q);
	int grid_index=0;
	double * current_grid=grids+q;
	//go through each scale
	for( int l=0;l<L;l++){
		//go through each offset at that scale.
		for(int B=0;B<(int)(pow(2.0,l)-1);B++){
			current_grid=grids+q;
			getGrid(current_grid,q,B,l);
		}
	}
}

//give the chebyshev grid for [0,1] with q points, sorted from left to right.  
inline void protogrid(double *fillme, int q){
	for(int t=0;t<q;t++){
		fillme[t]=cos((q-1-t)*pi/(q-1))+.5;
	}
}

//fill in the array with the chebyshev grid adapted to box b at level l.
//Assumes that protogrid is already allocated.  b runs from 0 to 2^level-1
inline void getGrid(double *protogrid, int q, int b, int level){
	double scale=pow(2,(double)-level);
	for(int t=0;t<q;t++){
		//shift protogrid by scale, to get grid from 0 to b.  Then shift by b*scale for the 
		//corresponding box.
		protogrid[t]=scale*(protogrid[t]+b+1);
	}
}

//Integrate over Y subset [0,1]^2 exp(iMphi(x,y))a(x,y)f(y).  
//The functions phi(x,y), f(y), and a(x,y) need to be defined in
//input.h. We order from the (0,1) left, then down to (1,0).  
//We follow the algorithm developed in:
//"A butterfly algorithm for synthetic aperture radar imaging"
//At each level there are Nq^2 weights to compute, and we need the ones from the previous level as well.
//At each level there are N^2 sets of weights we need to compute.  Each set of weights contains q elements.
//If N is a power of 2, then the depth is log(N) In order to compute the weights for a 
//given level, we use the weights from the previous level.   We only need the weights from two consecutive levels.	
//We have computed the weights for the pairs at bottom of T_x and top of T_y.
complex<double> *butterfly(int N, point *X, point *Y){

	point xt(0,0), yt(0,0), xs(0,0), ys(0,0), x0(0,0), y0(0,0);
	int ApBc=0, Ap=0, Bc=0, A=0, B=0, l=0, L=(int)log2((double) N)/2, int q=16, s=0, t=0, n=0, M=(int)sqrt(N), weight_index=0, previous_weight_index=0, r_eps=q*q;

	//differential.  If the points are uniform, then each represents a region of approx size dy.
	double dy=1.0/(double)N;
	//This is what we will return, the sum of the functions evaluated over the grid given.
	complex<double> *SUM=new complex<double>[N];
	const complex<double> i(0,1);
	point x(0,0), y(0,0);

        complex<double> *previous_weights[N*r_eps];
	complex<double> *current_weights[N*r_eps];
	previous_weights= new complex<double>[N*r_eps];
	current_weights = new complex<double>[N*r_eps];

	//Compute all the 1-d grids we will need.  These will be passed to functions as needed.
	double *grids;
	buildGrids(grids,L,N);

	//The algorithm consist of 5 steps.
	//Step 1: Initialize the weights  
	A=0; 
	l=0;
    	zeroweights(current_weights,N,r_eps);
 	for(n=0;n<N;n++){
        	y.setPoint(Y[n]);
		if(f(y)!=0){
			//get the box y is in.
			B=getBox(y,L);
			//center of B
			x0.setPoint(getCenter(B,L));
			for(t=0;t<r_eps;t++)
			{
				weight_index=getWeightIndex(A,B,l,r_eps,t);		
				yt.setPoint(getGridPoint(grids,L,B,q,t));
				//this moves the multiplication by exp(-iMph(x_0,y_t)) inside the sum in (13).  Combine the exponentials also
				currentweights[weight_index]+=exp(-i*M*(phi(x_0,y_t)-phi(x_0,y)))*L(grids,q,t,B,L-l,y)*f(y);
			}
		}
	}
	swapweights(previousweights,currentweights);
	
	//Step 2: for l=1,..., L/2-1  Compute the next weights from previous weights using the fact that
	//diam(B)<1/N
	//handle l=1,...L/2.  That is, we are in coarse scale in X		
	for(l=1;l<=L/2;l++){
		zeroweights(currentweights,M, q);
		//go through all the pairs of boxes in T_X, and T_Y at level l, and L-l respectively.  There are N^2.
		for(n=0;n<N;n++){
			A=getA(n,l);
			B=getB(n,l,L);
			Ap=getParent(A,l);
			//go through the children of B paired with the parent of A to compute weights
			//the center of box a.
			x0.set(getCenter(A,l));
			for(t=0;t<r_eps;t++){
				//the grid in the box B
				yt.setPoint(getGridPoint(grids,L-l,B,q,t));
				weight_index=getWeightIndex(A,B,l,r_eps,t);
				for(int c=0;c<4;c++){
					Bc=getChild(B,L-l,c);
					//for each child parent pair
					for(tc=0;tc<r_eps;tc++){
						previous_weight_index=getIndexForPair(Ap,Bc,tc);       
						if(previous_weights[previous_weight_index]!=0){
							ytc.setPoint(getGridPoint(grids,L-l+1,Bc,q,tc));					 
							//this moves the multiplication by exp(-iMphi(x_0,y_t)) inside the sum in (13)
							current_weights[weight_index]+=L(grids,q, t, B,L-l,ytc)*exp(i*M*phi(x_0,ytc))*previous_weights[previous_weight_index];
						}	
					}	
				}
				currentweights[index]=currentweights[index]*exp(-i*M *phi(x0,yt));
			}
		}
		swapweights(previousweights,currentweights);
	}
	
	
	zeroweights(currentweights,M, q);
	//Step 3: for l=L/2 Do switch
	//handle switch.
	for(n=0;n<N;n++)
		//adapt the grids to their respective boxes.
		A=getA(n,l);
		B=getB(n,l,L);
		for(t=0;t<r_eps;t++){
			weight_index=getWeightIndex(A,B,L/2,r_eps,t);
			previous_weight_index=getIndexForPair(A,B,t);
			xt.setPoint(getGridPoint(grids,L/2,A,q,t));
			for(s=0;s<q;s++){
				previous_weight_index=getWeightIndex(A,B,L/2,r_eps,s);
				if(previous_weights[previous_weight_index]!=0){
					ys.setPoint(getGridPoint(grids,L/2,B,q,s));
					current_weights[weight_index]=current_weights[weight_index]
					+a(xt,ys)*exp(i*M*phi(xt,ys))*previous_weights[previous_weight_index];
				}
			}
		}
	}
	swapweights(previousweights,currentweights);

	//Step 4: for l=L/2+1,...,L Compute the next weights from previous weights using the fact that
	//diam(A)<1/M
	//handle l=L/2+1,... L
       	for(;l<=L;l++){
		zeroweights(currentweights,M, q);
		for(n=0;n<N;n++){
			A=getA(n,l);
			B=getB(n,l,L);
			Ap=getParent(A,l);
			xt.setPoint(getGridPoint(grids,l,A,q,t));
			//set grid for current box b.
			for(t=0;t<r_eps;t++){
				weight_index=getWeightIndex(A,B,L/2,r_eps,t);
				for(int c=0;c<4;c++){
					Bc=getChild(B,L-l,c);
					y0c.set(getCenter(Bc,L-l+1));
					//for each child parent pair
					for(tc=0;tc<r_eps;tc++){
						//The indices for the pairings are given by
						previous_weight_index=getIndexForPair(Ap,Bc,tc);
						if(previous_weights[previous_weight_index]!=0){
							xtp.setPoint(getGridPoint(grids,l-1,Ap,q,tc));
							current_weights[weight_index]=current_weights[weight_index]
								+L(grids,q, tc, Ap,l+1,xt)*exp(-i*M*phi(xtp,y0c))*previous_weights[previous_weight_index];
						}
					}
					currentweights[weight_index]=exp(i*M*phi(xt,y0c))*currentweights[weight_index];
				}
			}
		}
		swapweights(previousweights,currentweights);
	}
	//undo the last swap since we need the latest computed weights.
	swapweights(previousweights,currentweights);

	//Step 5: use equivalent weights to compute final sum.
	//We can now use these as equivalent sources to complete the sum.  
	A=0;
	for(n=0;n<N;n++){
		SUM[n]=0;
		B=getB(n,l,L);
		x.setPoint(X[n]);                        
		//here n is the box in T_y, and 
		y0.set(getCenter(n,L));
		for(t=0;t<q;t++){
			weight_index=getWeightIndex(A,B,L,r_eps,t);
			SUM[n]=SUM[n]+L(grids,q, t, A,l+1,xt)*exp(-i*M*phi(xt,y0))*current_weights[weight_index];
		}
		SUM[x]=exp(i*M*phi(X[x],y0))*SUM[x];
	}
	//account for the differential in the Riemann sum
	for(n=0;n<M;n++){
		SUM[n]=dy*SUM[n];
	}
	//clean up memory
	delete [] current_weights;
        delete [] previous_weights;

	return SUM;
}
