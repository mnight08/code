#include "input.h"
#include <iostream>

//The t-th lagrange basis polynomial for the grid grid 
//evaluated at the point x
double L(double *grid ,int q, double x, int t){
	double product=1;
	for(int j=1;j<t;j++){
		
		product=product*(x-grid[j])/(grid[t]-grid[j]);
	}
	//skip j=t
	j++;
	for(;j<q;j++){
		product=product*(x-grid[j])/(grid[t]-grid[j]);
	}
	return product;
}


void swapweights(complex<double>** current, complex<double>** previous){
	complex<double>** temp;
	temp=previous;
	previous=current;
	current=temp;	
}


//give the chebyshev grid for [-1/2,1/2] with q points, sorted from left to right.  
void protogrid(double *fillme, int q){
	for(int t=0;t<q;t++){
		fillme[t]=cos((q-1-t)*pi/(q-1));
	}
}

//fill in the array with the chebyshev grid adapted to box b at level l.
//Assumes that protogrid is already allocated.  b runs from 0 to 2^level-1
void grid(double *protogrid, int q, int b, int level){
	double scale=pow(2,(double)-level);
	for(int t=0;t<q;t++){
		//shift protogrid by scale, to get grid from 0 to b.  Then shift by b*scale for the 
		//corresponding box.
		protogrid[t]=scale*(protogrid[t]+b+1);
	}
}

//return the center of the box from the left at level level. 
double center(int level, int box){
	double scale=pow(2,(double)-level);
	return scale*(box+1/2);
}

//The array of weights is a set of all pairings, so it functions like a 2d array.
//For level l the dimensions of this 2-d pairing change, but the total number of 
//pairings remains constant.
//number of boxes in T_X is 2^l, and the number in
//T_Y is 2^{L-l}
int findpair(int a, int b, int l, int L){
	return a*pow(2.0,l)+b;
}

//integrate over [0,1]
////we will use lexiographic ordering for (a,b) pairs.  In one dimension, the 
//index for the pair (a,b) at level l is given by $a*2^(L-l)+b$  Effectively, this
//is a dynamic 2-d array stored in a 1-d array.  
//At each level, the dimensions of the 2-d array change, but the product of dimensions is 
//constant at N.
complex<double> *butterfly(int N, double * X, double *Y){
	double dy=(Y[N-1]-Y[0])/(double)N;
	complex<double> * SUM= new complex<double>[N];
	complex<double> i(0,1);
	int x=0, y=0;
	double xt=0, yt=0, xs=0, ys=0, x0=0, y0=0;
	int apbc1=0, apbc2=0, ap=0, bc1=0, bc2=0, a=0, b=0, index=0, l=0, L=(int)log2((double) N), int q=16, s=0, t=0;

	double *grida=new double[q];
	double *gridb=new double[q];
	double *childgrid1=new double[q];
	double *childgrid2=new double[q];
	double *parentgrid=new double[q];

	//At each level there are N weights we need to compute.  In order to compute the weights for a 
	//given level, we use the weights from the previous level.
	complex<double> **previousweights[N][q];
	complex<double> **currentweights[N][q];
	previousweights= new complex<double>*[N];
	currentweights = new complex<double>*[N];
	for(index=0;index<N;index++){
		previousweights[index]=new complex<double>[q];
		currentweights[index]=new complex<double>[q];
	}
	
	//Initialize equivalent weights.  
	protogrid(gridb,q);
	grid(gridb, q, 0, 0);
	x0=center(0,0);
	for(b=0;b<N,b++){
		for(t=0;t<q;t++){
			previousweights[b][t]=0;

			//sum over all the points in B
			for(y=0;y<N;y++)
				previousweights[b][t]=previousweights[b][t]+L(gridb,q,Y[y],t)*exp(i*N *phi(x0,Y[y]))*f(Y(y));
			yt=gridb[t];
			//Should do the array swap with a pointer.
			previousweights[b][t]=exp(-i*N *phi(x0,yt))*previousweights[b][t];
			currentweights[b][t]=previousweights[b][t];
		}
	}	

	//handle l=1,...L/2	
	for(l=1;l<=L/2;l++){
		swapweights(previousweights,currentweights);
		//go through all the pairs of boxes in T_X, and T_Y.  There are N.
		for(a=0;a<(int)pow(2.0,l),a++){
			//the center of box a.
			x0=center(l,a);
			for(b=0;b<(int)pow(2.0,L-l),b++)
			{
				index=findpair(a,b,l,L);

				//set grid for current box b.
				protogrid(gridb,q);
				grid(gridb, q, b,L-l);
				//compute weights
				for(t=0;t<q;t++)
				{
					//The index for a's parent is 
					ap=a/2;
		
					//the children of b
					bc1=2*b;
					bc2=2*b+1;
					
					//go through the two children of b paired with the parent of a to compute weights
					//The indices for the pairings are given by
					apbc1=findpair(ap,bc1,l-1,L);
					apbc2=findpair(ap,bc2,l-1,L);

					//initialize current weights to zero so we can do the sum.
					currentweights[index][t]=0;

					//make the grid for the first child parent pair
					protogrid(childgrid,q);
					grid(childgrid, q, L-l+1, bc1);
					
					//for the first child parent pair
					for(r=0;r<q;r++){
						yt=childgrid[r];
						currentweights[index][t]=currentweights[index][t]
							+L(gridb,q,yt,t)*exp(i*N*phi(x0,yt))*previousweights[apbc1][r];	
					}	
					//make the grid for the second child parent pair
					protogrid(childgrid,q);
					grid(childgrid, q, L-l+1, bc2);
					
					//for the second child parent pair
					for(r=0;r<q;r++){
						yt=childgrid[r];
						currentweights[index][t]=currentweights[index][t]
							+L(gridb,q,yt,t)*exp(i*N*phi(x0,yt))*previousweights[apbc2][r];	
					}
					yt=gridb[t];
					currentweights[index][t]=currentweights[index][t]*exp(-i*N *phi(x0,yt));
				}
			}
		}
	}

	//handle switch.
	//copy current weights to previous weights
	swapweights(previousweights,currentweights);

	//go through all the pairs of boxes in T_X, and T_Y.  There are N.
	//There needs to be two grids, one for A, and one for B.
	for(a=0;a<(int)pow(2.0,L/2),a++){
		for(b=0;b<(int)pow(2.0,L/2),b++){
			//adapt the grids to their respective boxes.
			protogrid(grida,q);
			grid(grida, q, L/2,a);
			protogrid(gridb,q);
			grid(gridb, q, L/2,b)
			
			index=findpair(a,b,L/2,L);
			for(t=0;t<q;t++){
				currentweights[index][t]=0;
				xt=grida[t]	
				for(s=0;s<q;s++){
					ys=gridb[t];	
					currentweights[index][t]=currentweights[index][t]
						+a(xt,ys)*exp(i*N*phi(xt,ys))*previousweights[index][s];
				}
			}
		}
	}
	
	//handle l=L/2+1,... L
       	for(;l<=L;l++){
		swapweights(previousweights,currentweights);
		for(a=0;a<(int)pow(2.0,l),a++){
			for(b=0;b<(int)pow(2.0,L-l),b++){
				index=findpair(a,b,l,L);
				y0=center(L-l,b);
		
				//set grid for current box b.
				protogrid(grida,q);
				grid(grida, q, a,l);
				for(t=0;t<q;t++){
					ap=a/2;
					bc1=2*b;
					bc2=2*b+1;

					//The indices for the pairings are given by
					apbc1=findpair(ap,bc1,l-1,L);
					apbc2=findpair(ap,bc2,l-1,L);

					//initialize current weights to zero so we can do the sum.
					currentweights[index][t]=0;

					protogrid(parentgrid,q);
					grid(parentgrid, q, L-l+1, bc1);
					//for the first child parent pair
					y0=center(L-l+1,bc1);
					xt=grida[t];
					for(s=0;s<q;s++){
						xs=parentgrid[s];
						currentweights[index][t]=currentweights[index][t]
							+L(parentgrid,q,xt,s)*exp(-i*N*phi(xs,y0))*previousweights[apbc1][s];	
					}
					//for the second child parent pair
					currentweights[index][t]=exp(i*N*phi(xt,y0))*currentweights[index][t];
					y0=center(L-l+1,bc2);	
					for(s=0;s<q;s++){
						xs=parentgrid[s];
						currentweights[index][t]=currentweights[index][t]
							+L(parentgrid,q,xt,s)*exp(-i*N*phi(xs,y0))*previousweights[apbc2][s];	
					}
					yt=gridb[t];
					currentweights[index][t]=exp(i*N*phi(xt,y0))*currentweights[index][t];
				}
			}
		}
	}

	//We have computed the weights for the pairs at bottom of T_x and top of T_y.
	//We can now use these as equivalent sources to complete the sum.  
	for(x=0;x<N;x++){
		SUM[x]=0;
		y0=center(L,x);
		for( t=0;t<q;t++){
			SUM[x]=SUM[x]+exp(-i*N*phi(xt,y0))*currentweights[x][t]	;
		}
		SUM[x]=exp(i*N *phi(X[x],y0))*SUM[x];
	}
	//account for the differential in the Riemann sum
	for(index=0;index<N;index++){
		SUM[index]=dy*SUM[index];
	}
	return SUM;
}
