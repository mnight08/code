%This is a script which will plot the output in matlab
%file will plot the data.  The first column of the file will
%be the x values, and the second the sum.
load naive.data;
x=output(:,1);%first column
y=output(:,2);%second column
plot(x,y);
