#include "naive.h"
#include "butterfly.h"

//The point of this program is to implement the fast fourier integral
//using the butterfly algorithm.  This is following the work of 
//Candes et. al in 2011
//For simplicity we work in one dimension here.	

//The problem is to compute osillatory integrals
//of the form int_a^b a(x,y)exp(2 pi i phi(x,y)) f(y) dy for all x
//in some interval $[c,d]$.  We call phi(x,y)
//the phase function, and a(x,y) the amplitude.  We call
//the interval [a,b] is called the integration domain, while 
//the interval [c,d] is called the evaluation domain.

//This input to this problem are then the intervals, [a,b], and
//[c,d], as well as the functions f(y), phi(x,y), and a(x,y)
//The computational problem will be to compute the 
//N point left aligned Riemann sum approximating the integral. 
//Since one can often compute the Fourier transform exactly
//I will treat F(y) as the input instead.

//Since C++ does not support anonymous functions we have
//a function prototype for the inputs.  We have the option to
//give sampled input as well.  
	
int main()
{

 	//The interval endpoints for the x domain
 	double c=0;
 	double d=1;
  	
  	//the interval endpoints for the y(integration domain)
  	double a=-1;
  	double b=1;
 

	int N=256;
	
	double *X= new double[N];
	double *Y= new double[N];
	//used to normalize the integral
	double *TN= new double[N];
	double *XN= new double[N];
	double dy=(b-a)/(double)N;
	double dx=(d-c)/(double)N;
	double dt=1/(double)N;
	for (int k=0;k<N;k++) {
		Y[k]=a+k*dy;
	}
	
	for (int j=0;j<N;j++) {
		X[j]=c+j*dx;
	}
	for (int j=0;j<N;j++) {
		T[j]=j*dt;
	}

	
	//use direct method
	complex<double> *SUM=naive(N,X,Y);
 	
	//output the computed sum
	ofstream output;
	output.open("naive.data");
	for (int j=0;j<N;j++) {
		//for testing purposes, I have used the charactaristic function
		//and the fourier phase which produces 2sinc(x) which is real.
		//For this reason, I am only outputing the real part
		output<<X[j]<<" "<<SUM[j].real()<<endl;
	}
	output.close();
  	
	//delete computed sum from direct method
	delete[] SUM;


	//use butterfly
	SUM=butterfly(N,X,Y);

	//output the computed sum
	output.open("butterfly.data");
	for (int j=0;j<N;j++) {
		output<<X[j]<<" "<<SUM[j].real<<endl;
	}
	output.close();


	return 0;


}
