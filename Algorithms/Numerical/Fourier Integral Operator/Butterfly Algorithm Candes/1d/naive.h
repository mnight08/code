#include "input.h"
//We are integrating over a to b.  Do uniform partition and Riemann sum.
//Evaluate it uniformly from c to d.
complex<double> *naive(int N, double * X, double *Y){
	double dy=1/(double)N;

	complex<double> * SUM= new complex<double>[N];
	for(int j=0;j<N;j++){
		SUM[j]=0;
		for(int k=0;k<N;k++){
			//This can be sped up probably since 
			//the exponential and complex arithmetic may 
			//not be so efficient.
			SUM[j]=SUM[j]+integrand(X[j],Y[k]);
		}
		SUM[j]=dy*SUM[j];
	}
	return SUM;
}
