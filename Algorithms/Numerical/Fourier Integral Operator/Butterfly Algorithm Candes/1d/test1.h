#include <complex>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <cstring>
#include <fstream>
#ifndef INPUT
#define INPUT
using namespace std;

//amplitude and phase should be real valued functions of real variables, or a function of 
//vectors
double psi(double x,double y){
	
	return 100.0*x*y;
};


double a(double x,double y){
	return 1.0;
};

//It is assumed supported in [0,1]
complex<double> f(double y){
	else return 1.0;
};




//normalized phase domain
double phi(double x,double y){
	
	return psi(x,y);
};


//normalized amplitude domain
double an(double t1,double t2){
	
	return 100*3.14*x*y;
};

//normalized function domain
double fn(double x,double y){
	
	return x*y;
};





//The function we are integrating.
complex<double> kernel(double t1, double t2)
{

	return a(t1,t2)*exp(I*phi(t1,t2));
}

//The function we are integrating.  This should be normalized, and so a function
//supported in [0,1]x[0,1]
complex<double> integrand(double t1, double t2)
{

	return kernel(t1,t2)*fn(t2);
}
#endif
