close all;
clear all;
Numberofsamples=4096;
%B=100000;
%w_0=0;
%frequencysamples=w_0-B/2:1/Numberofsamples:w_0+B/2-1/Numberofsamples;

sampling_space=1/2^12;
timesamples=0:sampling_space:sampling_space*Numberofsamples-sampling_space;

%1-d case
sampledfunction=exp(-timesamples.*timesamples);

plot(timesamples,sampledfunction);
figure;
frequencysamples=-1/(2*sampling_space):1/(Numberofsamples*sampling_space):1/(2*sampling_space)-1/(Numberofsamples*sampling_space);
fouriertransform=fftshift(fft(sampling_space*sampledfunction));
plot(frequencysamples,fouriertransform);
originalfunction=ifft(fouriertransform);
figure;

recoveredfunction=ifft(fft(sampledfunction));
plot(timesamples,recoveredfunction);