close all;
clear all;
% Numberofsamples=512;
% B=100000;
% w_0=0;
% frequencysamples=w_0-B/2:1/Numberofsamples:w_0+B/2-1/Numberofsamples;
% % 
% % sampling_space=1/2^12;
% % timesamples=0:sampling_space:sampling_space*Numberofsamples-sampling_space;
% % 
% % %1-d case
% % sampledfunction=1/exp(-timesamples.*timesamples);
% % 
% % plot(timesamples,sampledfunction);
% % figure;
% % frequencysamples=-1/(2*sampling_space):1/(Numberofsamples*sampling_space):1/(2*sampling_space)-1/(Numberofsamples*sampling_space);
% % fouriertransform=fftshift(fft(sampling_space*sampledfunction));
% % plot(frequencysamples,fouriertransform);
% % originalfunction=ifft(fouriertransform);
% % figure;
% % 
% % recoveredfunction=ifft(fft(sampledfunction));
% % plot(timesamples,recoveredfunction);
% 
% % 
% % im=imread('EmissionNebula_NGC6357.jpg');
% % fouriertransform1=fft2(im(:,:,1));
% % fouriertransform2=fft2(im(:,:,2));
% % fouriertransform3=fft2(im(:,:,3));
% % 
% % recoveredimage1=ifft2(fouriertransform1);
% % recoveredimage2=ifft2(fouriertransform2);
% % recoveredimage3=ifft2(fouriertransform3);
% % 
% % 
% % recoveredimage(:,:,1)=abs(recoveredimage1);
% % recoveredimage(:,:,2)=abs(recoveredimage2);
% % recoveredimage(:,:,3)=abs(recoveredimage3);
% % max1=max(max(recoveredimage(:,:,1)));
% % min1=min(min(recoveredimage(:,:,1)));
% % 
% % max2=max(max(recoveredimage(:,:,2)));
% % min2=min(min(recoveredimage(:,:,2)));
% % 
% % max3=max(max(recoveredimage(:,:,3)));
% % min3=min(min(recoveredimage(:,:,3)));
% % 
% % 
% % recoveredimage(:,:,1)=(min1+recoveredimage(:,:,1))/(min1+max1);
% % recoveredimage(:,:,2)=(min2+recoveredimage(:,:,2))/(min2+max2);
% % recoveredimage(:,:,3)=(min3+recoveredimage(:,:,3))/(min3+max3);
% % 
% %set up grid
 N=1024;
 spacing=1/2^8;
 grid=-N*spacing/2:spacing:N*spacing/2-spacing;
% 
 w=10^8;
 c_0=299792458;
% %set up greens function
% %!!!!!!!!!!!!!11
G=zeros(N,N);
i=1;
while i<=N
    j=1;
    while j<=N
        if(norm([grid(i),grid(j)])~=0)
            G(i,j)=exp(-1i*w*norm([grid(i),grid(j)])/c_0)/(4*pi*norm([grid(i),grid(j)]));
        end
        j=j+1;
    end
    i=i+1;
end
% imagesc(abs(G));
% %set up reflectivity function
% %!!!!!1
 V=zeros(N,N);
 % k=1;
 % while k<=N/4
 % V(N/2,N/4+k)=-.001;
 % k=k+1;
 % end
 V(N/2,N/2)=10000;
 
 %normalize V
  vmin=min(min(V));
  vmax=max(max(V));
  V=(vmin+V)/(vmin+vmax);
 % V=.001*imread('Copy_of_test.bmp');
% %  
  imagesc(V);
  figure;
  antennaposition=[0,0,100];
%  % %set up field
  U=zeros(N,N);
%  
i=1;
while i<=N
   j=1;
   while j<=N
       if(norm([grid(i),grid(j),0]-antennaposition)~=0)
           U(i,j)=exp(-1i*w*norm([grid(i),grid(j),0]-antennaposition)/c_0)/(4*pi*norm([grid(i),grid(j),0]-antennaposition));
       end
       j=j+1;
   end
   i=i+1;
end

% %     1;
% % 
% % 
% %compute scattered field
 scatteredfield=fftshift(ifft2(fft2(G).*fft2(V.*U)));
 
%secondscatteredfield=(ifft2(fft2(G).*fft2(V.*scatteredfield)));
%  %imagesc(abs(scatteredfield));
%  %figure;
%  %imagesc(abs(secondscatteredfield));
%  %figure;
%  %imagesc(abs(U));
%  
%  
%  recoveredimage=(fft2(scatteredfield)./fft2(G))./abs(U);
%  
%  

imagesc(abs(scatteredfield));
 %image(abs(fftshift(fft(imread('test.bmp')))))
% imread('EmissionNebula_NGC6357')
% 
% image(recoveredimage);