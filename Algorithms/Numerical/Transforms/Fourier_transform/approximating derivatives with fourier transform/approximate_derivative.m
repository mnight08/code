function Derivative=approximate_derivative(f,x,n)
%approximate the  sampled function f using fourier transform
N=max(size(f));
if mod(N,2)== 0
    First_Order_Symbol=1i*(2*pi/N)*[0:N/2-1,0,-N/2+1:-1];
else
    First_Order_Symbol=1i*(2*pi/N)*[0:(N-1)/2,-(N-1)/2:-1];
end
%k=1;
%while k<N/2+1
%   First_Order_Symbol(k)=((2*pi*1i)/(N+1))*k;
%   k=k+1; 
%end
%while k<N+1
%    First_Order_Symbol(k)=((2*pi*1i)/(N+1))*(k-N);
%    k=k+1;
%end

Symbol=First_Order_Symbol.^n;
Derivative=ifft(Symbol.*fft(f));
band_filter(Derivative,-1,1);

%Derivative=ifft(1i*W.*fft(f));

%D=zeros(size(f));
%N=size(f);
%N=max(N);
%M=size(z);
%M=max(M);
%w=-1000:1:1000;
%R=size(w);
%R=max(R);
%K=zeros(size(w));
% i=1;
% while i<=M
%     j=1;
%     while j<=N
%         k=1;
%         while k<=R
%         K(k)=exp(-1i*w(k)*(x(i)-z(j)))*(-1i*w(k));
%         k=k+1;
%         end
%         D(j)=trapz(K,w);
%         j=j+1;
%     end
%     Derivative(i)=trapz(D.*f,x);
%     i=i+1;
% 
% end


end


