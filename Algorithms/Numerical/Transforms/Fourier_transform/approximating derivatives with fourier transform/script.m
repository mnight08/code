close all;
clear all;
testfunc=@test_function;
h=.01;
larger_sample_set=.1:h:50+h;
sample_set=.1:h:50;
sampled_function=testfunc(sample_set);
simple_derivative=diff(testfunc(larger_sample_set))./diff(larger_sample_set);

plot(sample_set,sampled_function);
title('original function');
figure;

sampled_derivative=approximate_derivative(sampled_function,sample_set,1);


plot(sample_set,(1/(h)).*real(sampled_derivative));
title('derivative of fuction using fourier method');
figure

plot(sample_set,real(simple_derivative));
title('derivative of fuction using difference method');

figure

plot(sample_set,abs((1/(h)).*real(sampled_derivative)-real(simple_derivative)))
%fpp=approximate_derivative(fp,x,x);
%figure

%plot(x,real(fpp));
%title('functions second derivative');