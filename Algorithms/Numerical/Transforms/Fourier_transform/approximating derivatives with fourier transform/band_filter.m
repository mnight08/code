function filtered_function = band_filter( f,a,b )
%Take the function f and if it is not between a and b then 
%restricts range of f to the interval [a,b]
%consider it a type of singularity and remove it in some way
%
%
N=max(size(f));
i=1;
while i<=N
   if f(i)<a || f(i)>b
        f(i)=0;
   end
   i=i+1; 
end

size(f)
end

