function H=faster_hartley_transform1d(f,x,u)
N=size(x);

N=max(N);
H=zeros(size(u));

i=1;
while i<=N
H(i)=trapz(cas(2*pi*u(i)*x).*f);
i=i+1;
end

end