%The idea here is to interpolate our given hartley data and compare its
%reconstruction to using the raw data by itself

clear all;
close all;
hold on;
c_0=1;%3*10^8;
u=-1:.0001:1;
x=-100:.1:100;
f=@testfunc;

N=size(x);
M=size(u);
H=zeros(M(2),1);
%res is the number of points between our given hartley data that we will
%interpolate
res=50;
xi=zeros(M(2)+res*(M(2)-1),1);

fx=zeros(size(x));
i=1;
i=1;
while i <= N(2)
    fx(i)=f(x(i));
    i=i+1;
end
    
    
%original functions
plot(x,fx,'k');
%approximate hartley transform
H=DHT_hartley_transform1d(fx,x,u);
%plot approximated hartley transform
figure
plot(u,H,'b');

%approximate hartley transform applied to hartley trasnform using only given hartley data.  This should
%be approximately the original function
HH=DHT_hartley_transform1d(H,u,x);
%plot estimate of original function using only given hartley data.  I did
%not keep scaling factor for approximated integral, so the inversion will
%be slightly off by a constant factor
figure
plot(x,HH/10^4,'r');



