%The idea here is to interpolate our given hartley data and compare its
%reconstruction to using the raw data by itself

clear all;
close all;
hold on;
c_0=1;%3*10^8;
u=-10:.1:10;
v=-10:.1:10;
x=-5:.1:5;
y=-5:.1:5;
f=@testfunc;

N_1=size(u);
N_1=max(N_1);

N_2=size(v);
N_2=max(N_2);

fxy=sample2d(f,x,y);
%original functions
surf(x,y,fxy);
%approximate hartley transform
%H=hartley_transform2d(fxy,x,y,u,v);
%plot approximated hartley transform
%figure
%surf(u,v,H);

%approximate hartley transform applied to hartley trasnform using only given hartley data.  This should
%be approximately the original function
%HH=hartley_transform2d(H,u,v,x,y);
%plot estimate of original function using only given hartley data.  I did
%not keep scaling factor for approximated integral, so the inversion will
%be slightly off by a constant factor
%figure
%surf(x,y,HH/10^4);



