function fxy=sample2d(f,x,y)

M_1=size(x);
M_1=M_1(2);
M_2=size(y);
M_2=M_2(2);

fxy=zeros(M_1,M_2);
i=1;
while i <= M_1
    j=1;
    while j <= M_2
        fxy(i,j)=f(x(i),y(j));
        j=j+1;
    end
    i=i+1;
end

end