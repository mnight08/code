function interpolation_matrix=BarycentricInterpolationMatrix(interpolation_grid,sample_grid,weights)
interpolation_matrix=zeros(max(size(interpolation_grid)),max(size(sample_grid)));
k=1;
while k<=max(size(interpolation_grid))
interpolation_matrix(k,:)=weights./(interpolation_grid(k).*ones(size(sample_grid))-sample_grid);
k=k+1;
end
end