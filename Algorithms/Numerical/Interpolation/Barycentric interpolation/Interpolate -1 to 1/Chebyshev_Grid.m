function grid = Chebyshev_Grid(N)
%returns a Chebyshev grid of the first kind on [-1,1].  That is, it returns an array
%of points given by x_j=cos((2j+1)/(2n+2))
grid=zeros(N,1);
j=1;
while j<=N
    grid(j)=cos(((2*j+1)*pi)/(2*N+2));
   j=j+1; 
end

end

