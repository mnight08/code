function weights=GeneralBarycentricWeights(sample_grid)

weights=zeros(size(sample_grid));
i=1;
while i<= max(size(sample_grid))
    j=1;
    weights(i)=1;
    while j <= max(size(sample_grid))
        if i ~= j
            weights(i)=weights(i)*(sample_grid(i)-sample_grid(j));
        end
        j=j+1; 
    end
    i=i+1;
end
weights=1./weights;



end