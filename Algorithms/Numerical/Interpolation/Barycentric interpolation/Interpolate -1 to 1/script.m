%This file will test our interpolation methods.  
%first lets set up a grid.
clear all;
close all;
sample_grid=-1:.1:1;
better_sample_grid=-1:.001:1;
interpolation_grid=-1:.001:1;

samples=test_function(sample_grid);
figure('Name','samples function plot');
plot(sample_grid,samples);

figure('Name','better function plot');
plot(better_sample_grid,test_function(better_sample_grid));



%First we compute  the weights.
%If we know the grid spacing is of particular type, then we can compute the
%weights more efficiently.
weights=GeneralBarycentricWeights(sample_grid);


%after computing weights we must calculate the rescaled weights for each
%interpolation point.  wj/(x-xj).
%THis is not necessary with the interpolation matrix.
%rescaled_weights=zeros(max(size(interpolation_grid)),max(size(sample_grid)));

%basically, there is an interpolation matrix that needs to be computed.
%this is independent of actual samples, but depends on the sample points
%and the number of interpolation points.
interpolation_matrix=BarycentricInterpolationMatrix(interpolation_grid,sample_grid,weights);

figure('Name','interpolated function plot using uniform samples');
plot(interpolation_grid,(interpolation_matrix*samples')./(interpolation_matrix*ones(size(samples))'));

figure('Name','error plot')
plot(interpolation_grid,abs(((interpolation_matrix*samples')./ ...
    (interpolation_matrix*ones(size(samples))'))'- ...
    (test_function(better_sample_grid))));

%Do interpolation wiht Chebychev grid.
sample_grid=Chebyshev_Grid(21);

weights=GeneralBarycentricWeights(sample_grid);
interpolation_matrix=BarycentricInterpolationMatrix(interpolation_grid,sample_grid,weights);
samples=test_function(sample_grid);


figure('Name','interpolated function plot using chebyshev samples');
plot(interpolation_grid,(interpolation_matrix*samples)./(interpolation_matrix*ones(size(samples))));

figure('Name','error using cheby grid plot')
plot(interpolation_grid,abs(((interpolation_matrix*samples)./ ...
    (interpolation_matrix*ones(size(samples))))'- ...
    (test_function(better_sample_grid))));



figure('Name','samples on cheby grid function plot');
plot(sample_grid,samples);
