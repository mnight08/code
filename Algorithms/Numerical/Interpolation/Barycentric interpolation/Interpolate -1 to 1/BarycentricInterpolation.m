function f= BarycentricInterpolationChebyshevGridTypeTwo( samples, evaluation_grid)
%BARYCENTRICINTERPOLATION
%perform the barycentric interpolation from [-1,1] cheby grid of type 2
%onto the evaluation grid
%this is based on 4.2 in Barycentric lagrange interpolation by Berute and
%Trefethen
%The weights are given in the paper.
%If we know the grid spacing is of particular type, then we can compute the
%weights more efficiently.
N=max(size(sample_grid));
M=max(size(evaluation_grid));

f=zeros(M,1);
sample_grid=chebgrid2template(N);
weights=zeros(N+1);
weights(1)=.5;
weights(N+1)=(-1)^(N)*.5;
i=2;
while i<=N
    weights(i)=(-1)^(i-1)*1;
end
i=1;
while i<=M
    f(i)=


end

