#include <iostream>
#include <algorithm>
using namespace std;

int knapsack(int *a,int n,int s,int *subset,int &size_of_subset)
{
	int** table;
	table= new int*[n+1];
	for(int i=0;i<n+1;i++)
		table[i]=new int[s+1];

	for( int i=0;i<s+1;i++)
		table[0][i]=0;

	for(int i=0;i<n+1;i++)
		table[i][0]=0;

	for(int i=1;i<n+1;i++)
	{
		for(int j=1;j<s+1;j++)
			if(a[i]>j)
			{
				table[i][j]=table[i-1][j];
			}
			else 
			{
				table[i][j]=max(table[i-1][j],a[i-1]+table[i-1][j-a[i-1]]);
			}

	}
	return table[n][s];
}

int main()
{
	int n= 28;
	int s=28;
	int *a;
	int *subset;
	a= new int[n];
	subset=new int[n];
	int size_of_subset=0;

	a[0]=5;
	a[1]=23;
	a[2]=27;
	a[3]=37;
	a[4]=48;
	a[5]=51;
	a[6]=63;
	a[7]=67;
	a[8]=71;
	a[9]=75;
	a[10]=79;
	a[11]=83;
	a[12]=89;
	a[13]=91;
	a[14]=101;
	a[15]=112;
	a[16]=121;
	a[17]=132;
	a[18]=137;
	a[19]=141;
	a[20]=143;
	a[21]=147;
	a[22]=153;
	a[23]=159;
	a[24]=171;
	a[25]=181;
	a[26]=190;
	a[27]=191;
	
	cout<<knapsack(a,n,s,subset,size_of_subset);


	return 0;
}

