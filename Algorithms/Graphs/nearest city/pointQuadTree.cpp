#include "pointQuadTree.h"	

pointQuadTree::pointQuadTree()
{
	head=NULL;
}

//query will be a pointer to iterate thru sub trees
//list will be passed to have the cities inserted into it
//r is a constant range to be passed, wont change
//c will be a city to pass to find out range you are at, wont change
//this is built off of search function
void pointQuadTree::recinrange(city* query,std::list<city>*&list, int r, city c)
{
	if(query==NULL)
		return;
	else 
	{
		//check to see if current city is in range, if it is not, we are able to drop off one of its children, 
		//if it is, then it is possible for a child that is in range to exist in each subtree
		if(!(c.distancetocity(query)>r))
		{
			list->push_back(*query);
			recinrange(query->northeast,list,r,c);
			recinrange(query->northwest,list,r,c);
			recinrange(query->southwest,list,r,c);
			recinrange(query->southeast,list,r,c);
		}
		else if(c.north(*query))
		{		
			//north west
			if(c.west(*query))
			{
				recinrange(query->northeast,list,r,c);
				recinrange(query->northwest,list,r,c);
				recinrange(query->southwest,list,r,c);
			}

			//directly north || north east
			else 
			{
				recinrange(query->northeast,list,r,c);
				recinrange(query->northwest,list,r,c);
				recinrange(query->southeast,list,r,c);
			}
		}
		else if(c.south(*query))
		{
			//south west
			if(c.west(*query))
			{
				recinrange(query->northwest,list,r,c);
				recinrange(query->southeast,list,r,c);
				recinrange(query->southwest,list,r,c);
			}

			//south east|| directly south
			else 
			{	
				recinrange(query->northeast,list,r,c);
				recinrange(query->southeast,list,r,c);
				recinrange(query->southwest,list,r,c);
			}
		}

		//directly east/west
		else
		{
			//direclty west
			if(c.west(*query)||c.samecity(*query))
			{	
				recinrange(query->northwest,list,r,c);
				recinrange(query->southwest,list,r,c);
			}
			//directly east
			else if(c.east(*query))
			{
				recinrange(query->northeast,list,r,c);
				recinrange(query->southeast,list,r,c);
			}
		}	



	}
}

std::list<city>* pointQuadTree::in_range(int a , int b, int c)
{
	city query(a,b,"");

	//the function returns a close city to the point if it doesnt find it
	//city* found=searchcity(query);
	std::list<city>* list=new std::list<city>;
	recinrange(head,list, c, query);
	return list;
}


city pointQuadTree::nearestCity(int a, int b)
{
	return *this->nearestCity(head,a,b);
}


//if a city is not found, then it will return the next closest city
city* pointQuadTree::searchcitytree(city * tree, city a)
{
	city* tmp=&a;
	city* itr=tree;
	//this should never happen, but probably will
	if(itr==NULL)
	{
		//i forgot this gets called each time nearest city is used
		/*
		cout<<endl<<"!!please dont try so hard to screw this up !!"<<endl;
		cout<<"this will crash if you try and access whatever you gave this pointer, enjoy."<<endl;*/
		return itr;
	}
	for(;itr!=NULL;)
	{
		if(itr->samecity(*tmp))
		{
			return itr;
		}
		//north 
		else if(tmp->north(*itr))
		{
			//north west
			if(tmp->west(*itr))
			{
				if(itr->northwest==NULL)
					return itr;
				else
					itr=itr->northwest;
			}
			//north east//directly north
			else
			{
				if(itr->northeast==NULL)
					return itr;
				else
					itr=itr->northeast;		
			}
		}
		else if(tmp->south(*itr))
		{
			//south west
			if(tmp->east(*itr))
			{
				if(itr->southeast==NULL)
					return itr;
				else 
					itr=itr->southeast;
			}
			//south west//directly south
			else 
			{
				if(itr->southwest==NULL)
					return itr;
				else
					itr=itr->southwest;
			}
		}
		//direclty west/east
		else if(tmp->west(*itr)||tmp->east(*itr))
		{
			if(tmp->west(*itr))
			{
				if(itr->northwest==NULL)
					return itr;
				else
					itr=itr->northwest;			
			}
			else
			{
				if(itr->southeast==NULL)
					return itr;
				else 
					itr=itr->southeast;		
			}
		}
		else 
			return itr;
	}
}





//return a pointer to the nearest city, 
city* pointQuadTree::nearestCity(city *tree, int a, int b)
{
	city query(a,b,"");

	//this should limit the range to a reasonable value.  
	city* northeast=searchcitytree(tree->northeast,query);
	city* northwest=searchcitytree(tree->northwest,query);
	city* southwest=searchcitytree(tree->southwest,query);
	city* southeast=searchcitytree(tree->southeast,query);

	int range;
	range =min(min(query.distancetocity(northeast),query.distancetocity(northeast)),min(query.distancetocity(southeast),query.distancetocity(southwest)));
	std::list<city>* clist;
	clist=this->in_range(a,b,range);	//should get me a small list of cities to compare

	city * closest=new city(clist->front().x,clist->front().y,clist->front().name);
	for(list<city>::iterator itr=clist->begin();itr!=clist->end();itr++)
	{

		city* compare=new city(itr->x,itr->y,itr->name);
		if(query.closercity(compare,closest)==compare)
		{
			city* tmp=closest;
			closest=compare;
			delete tmp;
		}
		else
		{
			delete compare;
		}
	}
	return closest;


}

city* pointQuadTree::searchcity(city a)
{
	return this->searchcitytree(head, a);
}

void pointQuadTree::inserttotree(city * tree,city *parent, city b, int whichchild=5)
{
	if(tree==NULL)
	{
		tree=new city(b.x,b.y,b.name);
		//NULL happens for a parent if tree is top of all trees
		if(parent==NULL)
		{
			tree->parent=NULL;
			this->head=tree;
		}
		switch(whichchild)
		{
			//north west case
		case 0:
			parent->northwest=tree;
			break;

		case 1:
			parent->northeast=tree;
			break;

		case 2:
			parent->southwest=tree;
			break;

		case 3:
			parent->southeast=tree;
			break;

		default:
			return;
		}
		return;
	}
	else 
	{
		parent=this->searchcitytree(tree, b);
		if(b.north(*parent))
		{
			//north west
			if(b.west(*parent))
				inserttotree(parent->northwest, parent, b, 0);

			//directly north || north east
			else 
				inserttotree(parent->northeast, parent, b, 1);
		}
		else if(b.south(*parent))
		{
			//south west
			if(b.west(*parent))
				inserttotree(parent->southwest, parent, b, 2);

			//south east|| directly south
			else 
				inserttotree(parent->southeast, parent, b, 3);
		}

		//directly east/west
		else
		{
			//direclty west
			if(b.west(*parent)||b.samecity(*parent))
				inserttotree(parent->northwest, parent, b, 0);

			//directly east
			else if(b.east(*parent))
				inserttotree(parent->southwest, parent, b, 2);

		}	
	}
}

//this essentially treats first city as origin
void pointQuadTree::insert(int a, int b, std::string s)
{
	inserttotree(head,NULL,city(a,b,s));
}
