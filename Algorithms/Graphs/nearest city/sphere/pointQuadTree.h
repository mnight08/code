#include "city.h"
//apparently if i dont include these in each header, when i try and split the file into .cpp, and .h, i will get a redefinition error
#include <iostream>
#include <string>
#include <list>
using namespace std;

//this is a lot less ungeneralized than i would like.
//there is no delete tree defined, which could be useful, but doesnt seem to be needed
class pointQuadTree 
{
private:

	//the top of the tree
	city* head;

	//this will check if a given node is in range, and if it is, then it push into a list, and check if its children are in range.  
	//if it is not in range though, it only ever neeeds to call three children, cause at least one is further than the current node.
	void recinrange(city* query,std::list<city>*&list, int r, city c);

	//this uses the search to find the parent of the item to be inserted into the tree, then gives it a token to tell what 
	//direction it is in, and makes a call this function again , forcing the base case, kinda pointless to use recursion, but it looks nice
	//i got rid of old method, might help a bit
	void insert(city);

	//this will search either for the tree, or the parent of the tree.  this is used insertion
	city* searchcitytree(city *tree, city a);

	//makes a list of only a few cities, those that actually have a chance to be  closer than the parent of a given city
	void nearestCity(city*,std::list<city>*&, city b);
public:

	pointQuadTree();
	//kinda dangerous to allow a returning to a pointer in the city, because
	//it gives a bit much control to the user
	
	//returns a pointer to the parent
	city* searchcity(city a);

	//calls insert(city)
	void insert(int a, int b, string s);
	city nearestCity(int a, int b);
	std::list<city> * in_range(int a, int b, int c);

};
