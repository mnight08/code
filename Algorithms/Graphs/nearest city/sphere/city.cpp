#include "city.h"

//assuming that if two cities are in the same spot, that they must be the same city, regardless of what they are called.
bool city::samecity(city a)
{
	if(x==a.x&&y==a.y)
	{
		return true;
	}
	else
		return false;
}


//is the current city north of a
bool city::north(city a)
{
	if(y>a.y) return true;else return false;
}


//is the current city south of a
bool city::south(city a)
{
	if(y<a.y) return true;else  return false;
}



//is the current city east of a
bool city::east(city a)
{
	if(x>a.x) return true;else return false;
}

//is the current city west of a
bool city::west(city a)
{
	if(x<a.x) return true;else return false;

}


//output the name, x, and y
ostream& operator <<(ostream &out, const city & a)
{
	out<<a.name<<" "<<"("<<a.x<<","<<a.y<<")"<<endl;
	return out;
}

city::city()
{
	x=-1, y=-1, name="";
	northwest=southwest=southeast=northeast=NULL;
}

//constructor that is being used the most
city::city(int a, int b, std::string s)
{
	name=s;
	x=a;
	y=b;
	northwest=southwest=southeast=northeast=NULL;
}

//distance from current city to another
int city::distancetocity(city *a)
{
	//make sure if it is null, that it gets a really large distance
	//to be honest i havent worked on this ina  few days, so im probably creating more errors now
	//i redid pretty much everything, so not sure if this is even needed anymore
	if(a==NULL)
		return 10000000;	//the furthest distance on map from two points is sqrt(xbound^2+ybound^2)=sqrt(20000000)<10000000
	//a bunch of typecasting using distance to two points on a grid squrt((x2-x1)^2+(y2-y1)^2))
	return int(sqrt(double((pow(double((this->x)-(a->x)),2.0)+pow((double(this->y)-(a->y)),2.0)))));
}

//closer of two cities to current city
city* city::closercity(city *a, city *b)
{
	int x, y;
	if(a==NULL&&b==NULL)
		return NULL;
	else if(a==NULL&&b!=NULL)
		return b;
	else if(a!=NULL&&b==NULL)
		return a;
	else{
		x=distancetocity(a);
		y=distancetocity(b);

		if(x<y)
		{
			return a;
		}
		else 
		{
			return b;
		}
		//x is same distance as y, then the city returned doesnt  matter
	}
}
