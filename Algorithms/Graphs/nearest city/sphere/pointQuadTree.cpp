#include "pointQuadTree.h"	

pointQuadTree::pointQuadTree()
{
	head=NULL;
}

//query will be a pointer to iterate thru sub trees
//list will be passed to have the cities inserted into it
//r is a constant range to be passed, wont change
//c will be a city to pass to find out range you are at, wont change
//this is built off of search function
void pointQuadTree::recinrange(city* query,std::list<city>*&list, int r, city c)
{
	if(query==NULL)
		return;
	else 
	{
		//check to see if current city is in range, if it is not, we are able to drop off one of its children, 
		//if it is, then it is possible for a child that is in range to exist in each subtree
		if(!(c.distancetocity(query)>r))
		{
			list->push_back(*query);
			recinrange(query->northeast,list,r,c);
			recinrange(query->northwest,list,r,c);
			recinrange(query->southwest,list,r,c);
			recinrange(query->southeast,list,r,c);
		}
		else if(c.north(*query))
		{		
			//north west
			if(c.west(*query))
			{
				recinrange(query->northeast,list,r,c);
				recinrange(query->northwest,list,r,c);
				recinrange(query->southwest,list,r,c);
			}

			//directly north || north east
			else 
			{
				recinrange(query->northeast,list,r,c);
				recinrange(query->northwest,list,r,c);
				recinrange(query->southeast,list,r,c);
			}
		}
		else if(c.south(*query))
		{
			//south west
			if(c.west(*query))
			{
				recinrange(query->northwest,list,r,c);
				recinrange(query->southeast,list,r,c);
				recinrange(query->southwest,list,r,c);
			}

			//south east|| directly south
			else 
			{	
				recinrange(query->northeast,list,r,c);
				recinrange(query->southeast,list,r,c);
				recinrange(query->southwest,list,r,c);
			}
		}

		//directly east/west
		else
		{
			//direclty west
			if(c.west(*query)||c.samecity(*query))
			{	
				recinrange(query->northwest,list,r,c);
				recinrange(query->southwest,list,r,c);
			}
			//directly east
			else if(c.east(*query))
			{
				recinrange(query->northeast,list,r,c);
				recinrange(query->southeast,list,r,c);
			}
		}	
	}
}

//this is inclusive i think(has cities on range = c too)
std::list<city>* pointQuadTree::in_range(int a , int b, int c)
{
	city query(a,b,"");

	//the function returns a close city to the point if it doesnt find it
	//city* found=searchcity(query);
	std::list<city>* tlist=new std::list<city>;
	recinrange(head,tlist, c, query);


	//temporary fix til i can find whats wrong with the logic of this, essentially cleanup.  
	std::list<city>* flist=new std::list<city>;
	
	//iterate through tlist, to make sure every item is within the range, if it is not, then do not copy it
	for(std::list<city>::iterator itr=tlist->begin();itr!=tlist->end();itr++)
	{
		//there is probably a cleaner way to do this, but i am not very comfortable with using the list template.
		city* compare=new city(itr->x,itr->y,itr->name);
		if(query.distancetocity(compare)<c)
			flist->push_back(*compare);
		delete compare;
	}
	delete tlist;
	return flist;
}


city pointQuadTree::nearestCity(int a, int b)
{
	city query(a,b,"");

	std::list<city>* tlist=new std::list<city>;
	nearestCity(head,tlist,city(a,b,""));
	
	city* closest;
	closest=new city(tlist->front().x,tlist->front().y,tlist->front().name);

	//iterate through tlist, to make sure every item is within the range, if it is not, then do not copy it
	for(std::list<city>::iterator itr=tlist->begin();itr!=tlist->end();itr++)
	{
		//there is probably a cleaner way to do this, but i am not very comfortable with using the list template.
		city* compare=new city(itr->x,itr->y,itr->name);
		if(query.distancetocity(compare)<query.distancetocity(closest))
		{
			city* temp;
			temp=closest;
			closest =compare;
			delete temp;
		}
		else
		delete compare;
	}	
	delete tlist;
	return *closest;
}


//if a city is not found, then it will return the next closest city
city* pointQuadTree::searchcitytree(city * tree, city a)
{
	city* tmp=&a;
	city* itr=tree;
	//this should never happen, but probably will
	if(itr==NULL)
	{
		//i forgot this gets called each time nearest city is used
		/*
		cout<<endl<<"!!please dont try so hard to screw this up !!"<<endl;
		cout<<"this will crash if you try and access whatever you gave this pointer, enjoy."<<endl;*/
		return itr;
	}
	for(;itr!=NULL;)
	{
		if(itr->samecity(*tmp))
		{
			return itr;
		}
		//north 
		else if(tmp->north(*itr))
		{
			//north west
			if(tmp->west(*itr))
			{
				if(itr->northwest==NULL)
					return itr;
				else
					itr=itr->northwest;
			}
			//north east//directly north
			else
			{
				if(itr->northeast==NULL)
					return itr;
				else
					itr=itr->northeast;		
			}
		}
		else if(tmp->south(*itr))
		{
			//south west
			if(tmp->east(*itr))
			{
				if(itr->southeast==NULL)
					return itr;
				else 
					itr=itr->southeast;
			}
			//south west//directly south
			else 
			{
				if(itr->southwest==NULL)
					return itr;
				else
					itr=itr->southwest;
			}
		}
		//direclty west/east
		else if(tmp->west(*itr)||tmp->east(*itr))
		{
			if(tmp->west(*itr))
			{
				if(itr->northwest==NULL)
					return itr;
				else
					itr=itr->northwest;			
			}
			else
			{
				if(itr->southeast==NULL)
					return itr;
				else 
					itr=itr->southeast;		
			}
		}
		else 
			return itr;
	}
}





//return a pointer to the nearest city, this could be more efficient, but would make project larger
void pointQuadTree::nearestCity(city *tree, std::list<city>*&list,city c)
{

	if(tree==NULL)
		return;
	else 
	{
		//push current city into a list, then drop of one child
		list->push_back(*tree);
	
		if(c.north(*tree))
		{		
			//north west
			if(c.west(*tree))
			{
				nearestCity(tree->northeast,list,c);
				nearestCity(tree->northwest,list,c);
				nearestCity(tree->southwest,list,c);
			}

			//directly north || north east
			else 
			{
				nearestCity(tree->northeast,list,c);
				nearestCity(tree->northwest,list,c);
				nearestCity(tree->southeast,list,c);
			}
		}
		else if(c.south(*tree))
		{
			//south west
			if(c.west(*tree))
			{
				nearestCity(tree->northwest,list,c);
				nearestCity(tree->southeast,list,c);
				nearestCity(tree->southwest,list,c);
			}

			//south east|| directly south
			else 
			{	
				nearestCity(tree->northeast,list,c);
				nearestCity(tree->southeast,list,c);
				nearestCity(tree->southwest,list,c);
			}
		}

		//directly east/west
		else
		{
			//direclty west
			if(c.west(*tree)||c.samecity(*tree))
			{	
				nearestCity(tree->northwest,list,c);
				nearestCity(tree->southwest,list,c);
			}
			//directly east
			else if(c.east(*tree))
			{
				nearestCity(tree->northeast,list,c);
				nearestCity(tree->southeast,list,c);
			}
		}	



	}

}

city* pointQuadTree::searchcity(city a)
{
	return this->searchcitytree(head, a);
}

void pointQuadTree::insert( city b)
{

	if(this->head==NULL)
	{
		head=new city(b.x,b.y,b.name);
	}
	else 
	{
		city* temp=new city(b.x,b.y,b.name);
		city* parent;
		parent=this->searchcitytree(head, b);
		if(b.north(*parent))
		{
			//north west
			if(b.west(*parent))
				parent->northwest=temp;

			//directly north || north east
			else 
				parent->northeast= temp;
		}
		else if(b.south(*parent))
		{
			//south west
			if(b.west(*parent))
				parent->southwest=temp;

			//south east|| directly south
			else 
				parent->southeast=temp;
		}

		//directly east/west
		else
		{
			//direclty west
			if(b.west(*parent)||b.samecity(*parent))
				parent->northwest=temp;

			//directly east
			else if(b.east(*parent))
				parent->southwest=temp;

		}	
	}
}

//this essentially treats first city as origin
void pointQuadTree::insert(int a, int b, std::string s)
{
	insert(city(a,b,s));
}
