#include <iostream>
#include <string>
#include <list>

using namespace std;
#include "math.h"

//i wanted to define this as a base class, but causes a bit of trouble later on
class city
{
public:
	//didnt originally have x/y and name as public
	int x,y;
	string name;

	//returns the closer of two cities to a given city.  would probably be able to do this without pointers, but these 
	//may make it more efficient.
	city *closercity(city *a, city *b);

	//this is so i dont have to type out the formula each time
	int distancetocity(city *a);

	//there are a few explicit calls made to this constuctor to save a bit of typing
	city( int a, int b, string s);
	city();

	//might save a bit of time later
	//these are really just to make this more readable
	bool north(city);
	bool south(city);
	bool east(city);
	bool west(city);
	bool samecity(city);

	//saves search queries, if i ever need to know how to step back up,  i dont think i actually used it though
	//city *parent;

	//pointers to the next trees
	city *northwest;
	city *northeast;
	city *southwest;
	city *southeast;

	//overloaded, but not really needed
	friend ostream &operator<<( ostream &out, const city&);

};

