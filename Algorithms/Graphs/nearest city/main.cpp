#include "pointQuadTree.h"

void print_citylist (std::list<city> * plist, std::string message)
{
	//std::cout << "" << std::endl;
	std::cout << "--------- " << message << " BEGIN ---------" << std::endl;
	for (std::list<city>::iterator it = plist->begin(); it != plist->end(); it ++) {
		std::cout << "City: '" << (*it).name << "'\t(x=" << (*it).x << ", y=" << (*it).y << ")" << std::endl;
	}
	std::cout << "--------- " << message << " END ---------" << std::endl;
}

//it seems it may be possible to get the nearest city to only have to compare log(base 4/3)(N)items if it is working witha  balanced tree,
// but keeping track of them has become a problem, and it would look like the nearest city algorith would just be a watered down version of 
//finding cities within a given range.  by doing a search for the parent, it seems this range can be sufficiently limited
//to provide a reasonable searchtime.

//the search function was built to return the parent of an item if it is not in the list,  these comparisions are based soley on cooridinates
//because two cities in the same spot are the same city.  the reason for the return of the parent by the search function was
//to be able to insert as cleanly as possible.
int main()
{
	pointQuadTree citytree;

	citytree.insert ( 100, 100, "Chicago");
	citytree.insert ( 25, 125, "McAllen");
	citytree.insert ( 50, 150, "Los Angeles");
	citytree.insert ( 75, 130, "Detroit");
	citytree.insert ( 150,  50, "New York");
	citytree.insert ( 10,  40, "Austin");

	city* found=citytree.searchcity(city(100,100,""));
	cout<<*found;
	city closestCity = citytree.nearestCity (63, 84);
	cout << "The closest city is " << closestCity.name << endl;

	double x = 50;
	double y = 95;
	double r = 100;

	std::list<city> * closeCities = citytree.in_range (x, y, r);

	if (NULL == closeCities) {
		return 0;
	}

	print_citylist (closeCities, "city list");
	delete closeCities;
	return 0;
}
