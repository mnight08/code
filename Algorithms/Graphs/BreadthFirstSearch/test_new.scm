;start C:\bloo36a\bin\bigloo.exe "hellO";take a list, and push an item onto it
(define push
  (lambda (lst item)
     (if (list? item) (append lst item)(push lst (list item)))
  )
)

;take a list, pop element off of the list in fifo style, and return the removed element.
(define get
  (lambda (lst)
    (car lst)
  )
)

;take a list, and pop an elemen off of the list in fifo style returning new list
(define pop
  (lambda (lst)
     (list-tail lst 1)
  )
)

;normal exponentiation
(define ^
  (lambda (x y)
    (expt x y)
  )
)

;swap the pos1, and pos2 digits of a number  starting from 0 counting from least significant digit
(define swap
 (lambda (num pos1 pos2)
    (define i pos1)
    (define j pos2)
    (define a (^ 10 i))
    (define b (^ 10 j))
    (define d1 (modulo (quotient num a) 10))
    (define d2 (modulo (quotient num b) 10))
    (+ (+ (- (- num (* d1 a)) (* d2 b)) (* d1 b)) (* d2 a))
  )
)

(define isempty null?)

(define add4children 
  (lambda (current pos option)
    ( cond ((and (= pos 8) (= option 2) )(swap current 8 7)) 
	   ((and (= pos 8) (= option 3) )(swap current 8 7))
	   ((and (= pos 7) (= option 2) )(swap current 7 6))
	   ((and (= pos 7) (= option 3) )(swap current 7 4))
	   ((and (= pos 7) (= option 4) )(swap current 7 8))
	   ((and (= pos 6) (= option 3) )(swap current 6 3))
	   ((and (= pos 6) (= option 4) )(swap current 6 7))
	   ((and (= pos 5) (= option 1) )(swap current 5 8))
	   ((and (= pos 5) (= option 2) )(swap current 5 4))
	   ((and (= pos 5) (= option 3) )(swap current 5 2))
	   ((and (= pos 4) (= option 1) )(swap current 4 7))
	   ((and (= pos 4) (= option 2) )(swap current 4 3))
	   ((and (= pos 4) (= option 3) )(swap current 4 1))
	   ((and (= pos 4) (= option 4) )(swap current 4 5))
	   ((and (= pos 3) (= option 1) )(swap current 3 2))
	   ((and (= pos 3) (= option 3) )(swap current 3 0))
	   ((and (= pos 3) (= option 4) )(swap current 3 4))
	   ((and (= pos 2) (= option 1) )(swap current 2 5))
	   ((and (= pos 2) (= option 2) )(swap current 2 1))
	   ((and (= pos 1) (= option 1) )(swap current 1 4))
	   ((and (= pos 1) (= option 2) )(swap current 1 0))
	   ((and (= pos 1) (= option 4) )(swap current 1 2))
	   ((and (= pos 0) (= option 1) )(swap current 0 3))
	   ((and (= pos 0) (= option 4) )(swap current 0 1))
	   (else current)
	)
  )
)

(define output 
  (lambda (start lst)
    (
     	
	(define current (get lst))
	(define str (number->string current))
	(define pos (blankpos current))
	(set! queue (pop lst))
	 	      
	(if (= pos 8) (display " ") (display (string-ref str 0)))
	(display " ")
	(if (= pos 7) (display " ") (display (string-ref str 1)))
	(display " ")
	(if (= pos 6) (display " ") (display (string-ref str 2)))
	(display " ")
	(newline)

	(if (= pos 5) (display " ") (display (string-ref str 3)))
	(display " ")	
	(if (= pos 4) (display " ") (display (string-ref str 4)))
	(display " ")	
	(if (= pos 3) (display " ") (display (string-ref str 5)))
	(display " ")	
	(newline)
	
	(if (= pos 2) (display " ") (display (string-ref str 6)))
	(display " ")	
	(if (= pos 1) (display " ") (display (string-ref str 7)))
	(display " ")	
	(if (= pos 0) (display " ") (display (string-ref str 8)))
	(display " ")	
	(newline)
	(newline)
	)
  )
)

(define blankpos
  (lambda (current pos)
	(if (= (modulo current 10) 0) pos (blankpos (quotient current 10) (+ pos 1)))
  )
)

(define addchildrenqueue
  (lambda (current chkdlst queue)
    (display current)
    (define l (list ))
    (define c1 (add4children current (blankpos current 0) 1))
    (define c2 (add4children current (blankpos current 0) 2))
    (define c3 (add4children current (blankpos current 0) 3))
    (define c4 (add4children current (blankpos current 0) 4))
    (if (and (not (member c1 queue)) (and (not (member c1 l)) (and (not (= current c1)) (not (member c1 chkdlst)))))
      	(set! l (push l c1)))
    (if (and (not (member c2 queue)) (and (not (member c2 l)) (and (not (= current c2)) (not (member c2 chkdlst)))))
      	(set! l (push l c2)))
    (if (and (not (member c3 queue)) (and (not (member c3 l)) (and (not (= current c3)) (not (member c3 chkdlst)))))
     	(set! l (push l c3)))
    (if (and (not (member c4 queue)) (and (not (member c4 l)) (and (not (= current c4)) (not (member c4 chkdlst)))))
      	(push l c4) l)
   )
 )
(define addchildrenbacktracking
  (lambda (current chkdlst queue tocurrent)
    (define l (list ))
    (define c1 (add4children current (blankpos current 0) 1))
    (define c2 (add4children current (blankpos current 0) 2))
    (define c3 (add4children current (blankpos current 0) 3))
    (define c4 (add4children current (blankpos current 0) 4))
    (set! tocurrent (cons current tocurrent))
    (if (and (not (member c1 queue)) (and (not (member c1 l)) (and (not (= current c1)) (not (member c1 chkdlst)))))
      	(set! l (push l tocurrent)))
    (if (and (not (member c2 queue)) (and (not (member c2 l)) (and (not (= current c2)) (not (member c2 chkdlst)))))
      	(set! l (push l tocurrent)))
    (if (and (not (member c3 queue)) (and (not (member c3 l)) (and (not (= current c3)) (not (member c3 chkdlst)))))
      	(set! l (push l tocurrent)))
    (if (and (not (member c4 queue)) (and (not (member c4 l)) (and (not (= current c4)) (not (member c4 chkdlst)))))
      	(push l  tocurrent) l)
   )
 )

(define bfs
  (lambda (queue start stop chkdlst backtracking tocurrent)
    (define current (get queue))
    (display current)(newline)
    (define back (get backtracking))
    (set! backtracking (pop backtracking))
    (set! queue (pop queue))
    (if (and ( not (member  current chkdlst)) (not (member current queue)))
      (begin
       	 (set! queue  (push queue (addchildrenqueue current chkdlst queue)))
	 (set! backtracking (push backtracking (addchildrenbacktracking current chkdlst queue tocurrent)))
         (set! chkdlst (append chkdlst (list current)))
       )
     )
    (if (and (not (isempty queue)) (not (eqv?  current stop )))
      (bfs queue start stop chkdlst backtracking back)(begin (display "final stop is")(output start backtracking)(newline)))
  )
 )

(define bfs2 
  (lambda (start stop)
	(define queue (list))
	(define chkdlst (list))
	(define backtracking (list start))
	(set! queue (push queue start))
	(bfs queue start stop chkdlst backtracking (list))
  )
)

(begin
    (bfs2 123456789 213456789)
	(newline)
)


