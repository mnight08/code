
/************************************
*  Alex Martinez
*  SID:  10308748
*  mnight08@yahoo.com
************************************/
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>

struct message
{
 long type;
 char data[128];
};

int send(struct message *msg_info,int s,int d);
int recv(struct message *msg_info,int s,int d);
void routing(int G[][5],char flag);

int main(int argc,char *argv[])
{
 int G[5][5]={{0,1,-1,-1,3},{1,0,4,-1,-1},{-1,4,0,2,1},{-1,-1,2,0,9},{3,-1,1,9,0}};                 //define a Graph, '-1' represents endless
 int G1[7],G2[7];              //G1 is used for identifying the neighbor nodes
                               //G2 is used for sending local node map
 int i,j,s,t,change=1; 
 int tm;
 char flag=argv[1][0];
 s=(int)(flag-'A');
 struct message msg_info;
 msg_info.type=100*(s+1);

for(i=0;i<5;i++)              //initialize G1
  G1[i]=G[s][i];
for(i=0;i<5;i++)
  for(j=0;j<5;j++)
    if(i!=s)
     G[i][j]=-1;
  
tm=msg_info.type;           //tm is used for counting the number of messages

while(1)
{ 
  for(i=0;i<5&&change==1;i++)
   if(G1[i]!=-1&&i!=s)      // send messages to neighbor nodes
     {  
        G2[5]=s;            // s is used for identifying the sender node
        G2[6]=i;            // i is used for identifying the receiver node
        for(j=0;j<5;j++)
          G2[j]=G[s][j];       
        for(j=0;j<7;j++)
           msg_info.data[j]=G2[j];  //G2 is the message need to be sent
        tm++;                       //increase the counter
        msg_info.type=tm;
        send(&msg_info,G2[5],G2[6]);
      }
   
   change=0;

   
   for(i=0;i<5;i++)
     {
       msg_info.type=0;
       if(i!=s&&G1[i]!=-1)
       {

        recv(&msg_info,i,s);       //receive messages from neighbor nodes
        if(msg_info.type!=0)
         { 
           change=1;

           for(j=0,t=0;j<5;j++)
             if(G[i][j]!=msg_info.data[j])
               { 
                 G[i][j]=msg_info.data[j];       //update graph
                 t=1;
		 
               }
           if(t==1)	
	   {
           routing(G,flag);}            //calculate the lesat cost of the path between two nodes
         }
       }
     }
  
          
}
return 0;
}




int send(struct message *msg_info,int s,int d)  
{                                     //this function is used for sending messages
 int msg_id;
 int n=9100;
 n+=s*5+d;
 msg_id=msgget((key_t)n,IPC_CREAT|0666);
 //printf("msg send %d -> %d\n",s,d);
 if(msg_id==-1)
 {
  perror("msgget:");
 }
 if(msgsnd(msg_id,msg_info,128,0)==-1)

  perror("msgsnd:");
  return 1;
}

int recv(struct message *msg_info,int s,int d)
{                                      //this function is used for receiving messages
 int msg_id;
 int n=9100;
 n+=s*5+d;
// printf("msg recv %d -> %d\n",s,d);
 msg_id=msgget((key_t)n,IPC_CREAT|0666);
 if(msgrcv(msg_id,msg_info,128,0,0)==-1)
  perror("msgrcv:"); 
 return 1;
}


void routing(int G[][5],char flag)
{                                 //this function is used for calculating the lesat cost of the path between two nodes

/*
*   You need to complete this part so that the program could calculate the value of each 
*   path.  In this function, G[][5] is the graph G, which needs to be calculated. "flag"  
*   is used for identifying the local node, for example 'C' represents node C. Attention, the 
*   type of "flag" is char, you may need to change it into "int" type when use it.
*/

	/*
		Because the message passing above only passes information about the source nodes row to its direct neighbors, 
		G will never completely be filled out.  If a node does not have another node as a neighbor, then that other node will
		never get updated information about that row passed to it.  In the graph we have, when router is run on node A, it will 
		send messages to only B, and E.  It will also only recieve messages from B, and E.  This means that router A will never
		recieve updated information about rows C, and D.  The same thing happens with some of the other processes, leaving a 
		table that has rows filled with -1s.

	*/


	//compute the approximate minimum distance from self to each other node
	for(i=0;i<5;i++)
	{
		//set min to the current minimum
		min=G[s][i];
		//get minimum distance
		for( j=0;j<5;j++)
		{
			//check to make sure that the intermediate can be reached from source, and destination can be reached from intermediate node
			if(G[s][j]!=-1&&G[j][i]!=-1)
				if(min==-1||G[s][j] +G[j][i]<min)
					min=G[s][j] +G[j][i];
		}
		G[s][i]=min;	
	}


        printf("\n");           //print the updated graph
        int i,j=0;
	int min=-1;
	for( i=0;i<5;i++)
           {for(j=0;j<5;j++)
             printf("%d ",G[i][j]);

            printf("\n");}
	
	int s=(int)(flag-'A');

}
