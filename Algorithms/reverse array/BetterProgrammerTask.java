/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

public class BetterProgrammerTask {

    public static Object[] reverseArray(Object[] a) {
        /*
          Please implement this method to
          return a new array where the order of elements has been reversed from the original
          array.
         */
        Object[] swap=a.clone();
         for(int i=0;i<a.length;i++)
         {
             a[i]=swap[swap.length-1];
            }
        return a;
      
       
    }
}
