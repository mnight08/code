#include <fstream>
#include<string>
#include<iostream>
#include <algorithm>
using namespace std;
//this is the heap designed in class
class minheap
{
public:
	minheap(int max);

	void initializeheap(int* itemlist, int newsize);

	void insert(int s);
	int extractMin();

	//test function
	void display();
	int getsize();
private:
	int parent(int i);
	int lchild(int i);
	int rchild(int i);

	int size;  //number of items in heap
	int * heap;

}heapsortobject(1000000);

int minheap::getsize()
{	
	return size;
}


//private helper functions

int minheap::parent(int i)
{
	return (i-1)/2;
}

int minheap::lchild(int i)
{
	return (i*2) + 1;
}

int minheap::rchild(int i)
{
	return (i+1)*2;
}


//public functions

minheap::minheap(int max)
{
	heap = new int[max];
	size = 0;
}

//dumb test function
void minheap::display()
{
	for(int i=0; i<size; i++)
		cout << heap[i] << endl;
}

void minheap::insert(int s)
{
	//put new item at end of heap
	heap[size] = s;
	size++;

	int current = size-1;
	while( heap[parent(current)] > heap[current] )
	{
		//bubble current item up
		swap( heap[current], heap[parent(current)] );
		current = parent(current);
	}
}


int minheap::extractMin()
{
	swap(heap[size-1],heap[0]);
	int temp=heap[size-1];
	size--;
	int current=0;
	while(current<size&&( (lchild(current)<size+1)&&(heap[lchild(current)] < heap[current] )||((rchild(current)<size+1)&&heap[rchild(current)]<heap[current])))
	{
		//bubble current item up
		if(heap[lchild(current)]<=heap[rchild(current)])
		{
			swap( heap[current], heap[lchild(current)]);
			current=lchild(current);
		}
		else if(heap[lchild(current)]>heap[rchild(current)])
		{
			swap( heap[current], heap[rchild(current)]);
			current = rchild(current);
		}	
	}
	if(temp==-842150451)
	{
		std::cout<<current;
	}

	return temp;
}

void minheap::initializeheap(int *itemlist,int newsize)
{
	heap=new int[size];
	size=newsize;
	for(int i=0;i<size;i++)
		heap[i]=itemlist[i];
	for(int i=0;i<size;i++)
	{
		int current = size-i-1;
		while( heap[parent(current)] > heap[current] )
		{
		//bubble current item up
		swap( heap[current], heap[parent(current)] );
		current = parent(current);
		}
	}

}

void heapsort(fstream &load)
{
	int temp=0;
	//a lazy way to do heapsort.  i could have done this by just initializing it then extracting, but its still nlogn
	while(load)
	{
		load>>temp;
		if(temp==-842150451)
			cout<<"errror!!!";
		heapsortobject.insert(temp);
	}
	load.close();
	fstream save;
	string savename("heapsort.txt");
	//savename=savename+char(60+rand()%26);

	save.open(savename.c_str(),ios::out);
	while(heapsortobject.getsize()>0)
		save<<heapsortobject.extractMin()<<endl;
	
	save.close();
}