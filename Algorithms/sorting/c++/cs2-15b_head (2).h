#ifndef studenttypeclass
#define studenttypeclass
#include "headers.h"
using namespace std;

class personType
{


public:	
	friend ostream& operator<<(ostream&, const personType&);

	bool operator==(const personType &test);
	bool operator> (const personType &test);
	bool operator <=(const personType &test);
	bool operator< (const personType &test);

	personType();
	personType(string n, int i, double g);
	string name;
	int id;			//in reality, would you ever allow someone to set their own id number,  in a system that is only using integers?
	double grade;
	~personType();
};

//needs to be defined to avoid errors.
personType::~personType()
{

}

personType::personType()
{
	name="-99";
	id=-99;
	grade=-99;
}

personType::personType(std::string n, int i, double g)
{
	name=n;
	id=i;
	grade=g;
}

//overload operator<<, which is not a member of personType, but is a friend to it.
ostream& operator<<(ostream& os, const personType& person)
{
	os.setf(ios::left, ios::adjustfield);
	
	string n = person.name; 
	int i = person.id;
	double g=person.grade;
	os<<n + "  " << i<< " " <<g <<endl;		//this endl is making the extra spaces, but extra spaces are less anoying than all in one line
	return os;
}


bool personType::operator ==(const personType &test)
{
	if(this->name!=test.name)
		return false;
	else if(this->id!=test.id)
		return false;
	else if(this->grade!=test.grade)
		return false;
	else return true;

}


//this needed to be overloaded for the template in lab 18 to work
bool personType::operator >(const personType &test)
{
	if(this->name<=test.name)
		return false;
	else if(this->id<=test.id)
		return false;
	else if(this->grade<=test.grade)
		return false;
	else return true;

}

bool personType::operator <(const personType &test)
{
	if(this->name>=test.name)
		return false;
	else if(this->id>=test.id)
		return false;
	else if(this->grade>=test.grade)
		return false;
	else return true;

}
//this seems to fail with some of the personType, sorting against what i thought it would. 
//the good thing is, that because according to itself, even if it sorts in a way i see wrong,
//the binary search should see no problems, because it uses the same idea of <= when 
//comparing two personTypes

//i dont see any trouble if i leave it like, but i would like to find out why the strange sorting
//*update-it is not a strange order, it is the order i wanted, but i did not realize the setup of this was screwing with me


bool personType::operator <=(const personType &test)
{
	if(this->name>test.name)
		return false;
	else if(this->id>test.id)
		return false;
	else if(this->grade>test.grade)
		return false;
	else return true;


}
#endif