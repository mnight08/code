// *****************************************************************
// Author:	alex martinez
// Class:	CSCI/CMPE 2380, Spring 2009
// Lab 25:	Quick Sort
// Date:	January 17, 2009
// Comment: The code here is meant to be revised.
//
//-----------------------------------------------------------------
//
// Description: This file contains the prototype of the class arrayListType with 
// two versions of quick-sort algorithm: the recursive one and the iterative one. 
// Simple sorting algorithms of insertion sort, selection sort and bubble sort
// are included. Two versions (recursive and iterative) merge sort algorithms are 
// also included. 
//
// Since we have implemented the array based list, we can choose to add a new set of 
// searching and sorting methods into it:
//	
// Search methods include linear search, randomized search, and binary search.
//
// sorting methods include bubble sort, selection sort, insertion sort, 
// two versions of merge sort (the recrusive one and the iterative one), 
// and two versions of quick sort (the recrusive one and the iterative one).
// 
// Compile and run your program. When everything is fine,
// print your .h and .cpp files and turn them to me or the TA.
// *****************************************************************
#include "headers.h"


using namespace std; 

int main()
{
	/**********************************************************
	 Part: Some practice
	 **********************************************************/
	//var decarations
	unsigned int seed;					//a random seed
	int num;							//to hold an int value
	arrayListType<int> list(1000);		//create a list of int type
	char tag; 
	
	cout<<"loading the list with random numbers between 0 and 666 ... "<<endl; 

	cout<<"Enter a random seed => ";	//get the random seed
	cin >> seed; 
	srand(seed);						//set the randon seed

	for (int i=0; i <500 ; i++)			//insert 50 random numbers into the list
	{
		num = rand()%501; 
		list.insertLast(num);
	}
	list.print(); 
	cout<<endl; 
	
	//list.insertionSort(); 
	//list.bubbleSort(); 
	//list.selectionSort(); 
	
/*
	cout<<"Want to use recursive quick Sort (r) or iterative quick sort (i) => "; 
	cin>>tag; 
	switch(tag)
	{
	case 'r':
	case 'R':	cout<<"calling recursive quick sort .... "<<endl; 
				list.recursiveMedian3QuickSort();
				break; 
	case 'i':
	case 'I':
				cout<<"calling iterative quick sort ....."<<endl; 
				list.iterativeMedian3QuickSort(); 
				break; 
	default: 
				cout<<"entered a wrong choice"<<endl;
	}

	cout<<"after sorting, the list is ....."<<endl; 
	cout<<list<<endl; 


	/***************************************************************
	 Part Two: Try to create a list of strings and practice 
	 both recursive quick sort and the iterative quick sort.
	 ***************************************************************/
	/*
	//add your code here

	
	string swap;
	int character;
	arrayListType<string> strings(100);
	strings.insertFirst("abc");
	strings.insertLast("xyz");
	cout<<"the list is"<<endl;
	strings.print();
	cout<<endl;
	//insert some random 5 charactar strings into list
	for(int i=50; i>0;i--)
	{
		swap="";
		for(int j=5;j>0;j--)
		{
			srand(i+j);
			character=(rand()%27)+65;
			swap=swap+char(character);
		}
		character=rand()%1000;
		if(character%2==0)
			strings.insertFirst(swap);
		else
			strings.insertLast(swap);
	}
	cout<<"now the list is"<<endl;
	strings.print();
	cout<<endl;
		cout<<"Want to use recursive quick Sort (r) or iterative quick sort (i) => "; 
	cin>>tag; 
	switch(tag)
	{
	case 'r':
	case 'R':	cout<<"calling recursive quick sort .... "<<endl; 
				strings.recursiveMedian3QuickSort();
				break; 
	case 'i':
	case 'I':
				cout<<"calling iterative quick sort ....."<<endl; 
				strings.iterativeMedian3QuickSort(); 
				break; 
	default: 
				cout<<"entered a wrong choice"<<endl;
	}

	cout<<"after sorting, the list is ....."<<endl; 
	cout<<strings<<endl; 
	/***************************************************************
	 Part Three: First define a personType class with three data members 
	 (string name, int ID, and double grade). Second, create a list
	 of personType. Third, both recursive quick sort and the iterative quick sort.
	 ***************************************************************/
	
/*	//add your code here

	//test to see if operator overload worked
	personType me("alex", 0, 99);
	personType otherme("alex", 0, 99);
	me==otherme?   cout<<"yes":cout<<"no";

	cout<<endl;

	//make up some people to insert
	personType person1("anonymous", 1, 100);
	personType person2("anyone", 2, 80);
	personType person3("enoyna", 3, 96);
	personType person4("soumynona", 4, 98);


	//would have to actually do the same with any list, doesn seem to be safe guard against duplication
	//i think i need to edit list template, to make sure that an object being inserted does not have the same id, and an object already there
	arrayListType<personType> people(10);
	people.insertFirst(me);
	people.insertFirst(person1);
	people.insertFirst(person2);	
	people.insertFirst(person3);	
	people.insertFirst(person4);

	people.print();

		cout<<"Want to use recursive quick Sort (r) or iterative quick sort (i) => "; 
	cin>>tag; 
	switch(tag)
	{
	case 'r':
	case 'R':	cout<<"calling recursive quick sort .... "<<endl; 
				people.recursiveMedian3QuickSort();
				break; 
	case 'i':
	case 'I':
				cout<<"calling iterative quick sort ....."<<endl; 
				people.iterativeMedian3QuickSort(); 
				break; 
	default: 
				cout<<"entered a wrong choice"<<endl;
	}

	cout<<"after sorting, the list is ....."<<endl; 
	cout<<people<<endl; 

	//complete the program
	*/return 0; 
}