int bubblesort(int *a, int size)
{
	for(int i=0;i<size-1;i++){
		for(int k=i;k<size-1;k++)
			if(a[k]>a[k+1]){	
				int temp=a[k];
				a[k]=a[k+1];
				a[k+1]=temp;
			}
	}
	return 0;
}