#include <fstream>
#include <string>
#include <algorithm>
using namespace std;

void recquicksort(int *& tobesorted, int begin, int end)
{	
	if(end<=begin||begin>=end||end==begin)
		return;
	else
	{

		int leftcount=0;
		int swapsize=end-begin+1;
		int *swapint;
		swapint=new int[swapsize];
		int pivot;

		int pivotindex=rand()%end;
		
		//put pivot at back
		swap(tobesorted[end],tobesorted[pivotindex]);
		pivot=tobesorted[end];
		//move all the stuff to left of pivot to a swap table
		for(int i=begin;i<end;i++)
		{
			if(tobesorted[i]<pivot)
			{
				leftcount++;
				swapint[leftcount-1]=tobesorted[i];
				
			}
		}
		
		//put pivot in the table
		swapint[leftcount]=pivot;
		int pposition=leftcount+begin;
		int rightcount=0;

		//fill the rest of the table with the elements to the right of the pivot, and the pivot
		for(int i=begin;i<=end;i++)
		{
			if(tobesorted[i]>=pivot)
			{
				rightcount++;
				swapint[leftcount+rightcount]=tobesorted[i];
				
				
			}

		}

		//move the changes into old table
		for(int i=begin;i<=end;i++)
		{
			tobesorted[i]=swapint[i-begin];
		}
		
	//	delete[] swapint;//memory leak, says heap is corrupted if i try to delete.
		if(leftcount>1)
		recquicksort(tobesorted,begin,pposition-1);
		//+2 because +1 would be pivot position
		if(rightcount>1)
		recquicksort(tobesorted,pposition+1,end);

	}
}

//load the list
void quicksort(fstream &load)
{
	//max size i expect to be given.
	int size=100000;
	int *table;
	table=new int[size];
	int temp;
	int i=0;
	for(;load;i++)
	{
		load>>temp;
		table[i]=temp;
	}

	//i should be number of items +1, so last index is i-2
	recquicksort(table,0,i-1);
	
	string savename("quicksort");
	savename=savename+char(60+rand()%26);
	fstream save;
	save.open(savename.c_str(),ios::out);
	for(int j=0;j<i;j++)
	{	
		if(table[j]<0)
		{
			cout<<table[j]<<endl<<"error"<<endl;
		}
		save<<table[j]<<endl;
	}
//	delete[] table;  //forces crash with large input

}