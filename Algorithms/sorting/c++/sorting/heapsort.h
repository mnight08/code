#include <fstream>
#include<string>
#include<iostream>
#include <algorithm>
using namespace std;
//this is the heap designed in class
class minheap
{
public:
	minheap(int max);

	void insert(int s);
	int extractMin();

	//test function
	void display();
	int getsize();
private:
	int parent(int i);
	int lchild(int i);
	int rchild(int i);

	int size;  //number of items in heap
	int * heap;

}heapsortobject(10000000);

int minheap::getsize()
{	
	return size;
}

//private helper functions

int minheap::parent(int i)
{
	return (i-1)/2;
}

int minheap::lchild(int i)
{
	return (i*2) + 1;
}

int minheap::rchild(int i)
{
	return (i+1)*2;
}


//public functions
minheap::minheap(int max)
{
	heap = new int[max];
	size = 0;
}

//dumb test function
void minheap::display()
{
	for(int i=0; i<size; i++)
		cout << heap[i] << endl;
}

void minheap::insert(int s)
{
	//put new item at end of heap
	heap[size] = s;
	size++;

	int current = size-1;
	while( heap[parent(current)] > heap[current] )
	{
		//bubble current item up
		swap( heap[current], heap[parent(current)] );
		current = parent(current);
	}
}

//i cannot find the error here
int minheap::extractMin()
{
	swap(heap[size-1],heap[0]);
	int temp=heap[size-1];
	size--;
	int current=0;
	while(current<size)
	{
		if((lchild(current)<size)&&(heap[lchild(current)] < heap[current])||((rchild(current)<size)&&heap[rchild(current)]<heap[current]))
		{
			if(rchild(current)<size)
			//bubble current item up
			if(heap[lchild(current)]<=heap[rchild(current)])
			{
				swap(heap[current], heap[lchild(current)]);
				current=lchild(current);
			}
			else
			{
				swap(heap[current], heap[rchild(current)]);
				current = rchild(current);
			}	
			else 
			{
				swap(heap[current], heap[lchild(current)]);
				current=lchild(current);
			}

		}
		else break;
	}
	return temp;
}

void heapsort(int* a,int siz)
{
	for( int i=0;i<siz;i++)
		heapsortobject.insert(a[i]);
	for(	int i=0;i<siz;i++)
		a[i]=heapsortobject.extractMin();
}