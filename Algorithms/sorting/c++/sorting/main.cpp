//#include "radix sort.h"
//#include "quick sort.h"
#include "heapsort.h"
#include "bubblesort.h"
#include "generatearray.h"
#include "mergesort.h"

#include <iostream>
#include <fstream>
#include <time.h>
int main()
{

	clock_t begin, end;
	int *temp;
	
	int size=10;
/*
	temp=genarray(size);
	begin=clock()*CLK_TCK;
	heapsort(temp,size);
	end=clock()*CLK_TCK;
	cout<<(double(end-begin)/1000)<<endl;
	delete[] temp;
	
	temp=genarray(size);
	begin=clock()*CLK_TCK;
	bubblesort(temp,size);
	end=clock()*CLK_TCK;
	cout<<(double(end-begin)/1000)<<endl;
	delete[] temp;

	temp=genarray(size);
	begin=clock()*CLK_TCK;
	mergesort(temp,size);
	end=clock()*CLK_TCK;
	cout<<(double(end-begin)/1000)<<endl;
	delete[] temp;

	size=100;

	temp=genarray(size);
	begin=clock()*CLK_TCK;
	heapsort(temp,size);
	end=clock()*CLK_TCK;
	cout<<(double(end-begin)/1000)<<endl;
	delete[] temp;
	
	temp=genarray(size);
	begin=clock()*CLK_TCK;
	bubblesort(temp,size);
	end=clock()*CLK_TCK;
	cout<<(double(end-begin)/1000)<<endl;
	delete[] temp;

	temp=genarray(size);
	begin=clock()*CLK_TCK;
	mergesort(temp,size);
	end=clock()*CLK_TCK;
	cout<<(double(end-begin)/1000)<<endl;
	delete[] temp;





	size=1000;

	temp=genarray(size);
	begin=clock()*CLK_TCK;
	heapsort(temp,size);
	end=clock()*CLK_TCK;
	cout<<(double(end-begin)/1000)<<endl;
	delete[] temp;
	
	temp=genarray(size);
	begin=clock()*CLK_TCK;
	bubblesort(temp,size);
	end=clock()*CLK_TCK;
	cout<<(double(end-begin)/1000)<<endl;
	delete[] temp;

	temp=genarray(size);
	begin=clock()*CLK_TCK;
	mergesort(temp,size);
	end=clock()*CLK_TCK;
	cout<<(double(end-begin)/1000)<<endl;
	delete[] temp;


	size=10000;

	temp=genarray(size);
	begin=clock()*CLK_TCK;
	heapsort(temp,size);
	end=clock()*CLK_TCK;
	cout<<(double(end-begin)/1000)<<endl;
	delete[] temp;
	
	temp=genarray(size);
	begin=clock()*CLK_TCK;
	bubblesort(temp,size);
	end=clock()*CLK_TCK;
	cout<<(double(end-begin)/1000)<<endl;
	delete[] temp;

	temp=genarray(size);
	begin=clock()*CLK_TCK;
	mergesort(temp,size);
	end=clock()*CLK_TCK;
	cout<<(double(end-begin)/1000)<<endl;
	delete[] temp;
	



	size=100000;

	temp=genarray(size);
	begin=clock()*CLK_TCK;
	heapsort(temp,size);
	end=clock()*CLK_TCK;
	cout<<(double(end-begin)/1000)<<endl;
	delete[] temp;
	
	temp=genarray(size);
	begin=clock()*CLK_TCK;
	bubblesort(temp,size);
	end=clock()*CLK_TCK;
	cout<<(double(end-begin)/1000)<<endl;
	delete[] temp;

	temp=genarray(size);
	begin=clock()*CLK_TCK;
	mergesort(temp,size);
	end=clock()*CLK_TCK;
	cout<<(double(end-begin)/1000)<<endl;
	delete[] temp;



	size=1000000;

	temp=genarray(size);
	begin=clock()*CLK_TCK;
	heapsort(temp,size);
	end=clock()*CLK_TCK;
	cout<<(double(end-begin)/1000)<<endl;
	delete[] temp;
	
	temp=genarray(size);
	begin=clock()*CLK_TCK;
	bubblesort(temp,size);
	end=clock()*CLK_TCK;
	cout<<(double(end-begin)/1000)<<endl;
	delete[] temp;

	temp=genarray(size);
	begin=clock()*CLK_TCK;
	mergesort(temp,size);
	end=clock()*CLK_TCK;
	cout<<(double(end-begin)/1000)<<endl;
	delete[] temp;
*/



	size=400000;
/*
	temp=genarray(size);
	begin=clock()*CLK_TCK;
	heapsort(temp,size);
	end=clock()*CLK_TCK;
	cout<<(double(end-begin)/1000)<<endl;
	delete[] temp;
*/	
	temp=genarray(size);
	begin=clock()*CLK_TCK;
	bubblesort(temp,size);
	end=clock()*CLK_TCK;
	cout<<(double(end-begin)/1000)<<endl;
	delete[] temp;
/*
	temp=genarray(size);
	begin=clock()*CLK_TCK;
	mergesort(temp,size);
	end=clock()*CLK_TCK;
	cout<<(double(end-begin)/1000)<<endl;
	delete[] temp;
*/
}