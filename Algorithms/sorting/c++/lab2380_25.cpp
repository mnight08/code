// *****************************************************************
// Author:	alex martinez
// Class:	CSCI/CMPE 2380, Spring 2009
// hwk:		 6
// Date:	January 17, 2009
// Comment: The code here is meant to be revised.
//
//-----------------------------------------------------------------
//
// Description: This file contains the prototype of the class arrayListType with 
// two versions of quick-sort algorithm: the recursive one and the iterative one. 
// Simple sorting algorithms of insertion sort, selection sort and bubble sort
// are included. Two versions (recursive and iterative) merge sort algorithms are 
// also included. 
//
// Since we have implemented the array based list, we can choose to add a new set of 
// searching and sorting methods into it:
//	
// Search methods include linear search, randomized search, and binary search.
//
// sorting methods include bubble sort, selection sort, insertion sort, 
// two versions of merge sort (the recrusive one and the iterative one), 
// and two versions of quick sort (the recrusive one and the iterative one).
// 
// Compile and run your program. When everything is fine,
// print your .h and .cpp files and turn them to me or the TA.
//
//this is a modification of lab 25, that adds two parameters to auxillary functions,
//to allow counting of insertions, and swaps in each function.  for insertion sort, i counted how many times 
//an item is inserted, this should be  a close approximation to swaps, the same goes for merge sort.
//
//the copy list function had to be reworked to allow to actually use in the constructor, deleting an uninitiallized pointer
//give trouble.
// *****************************************************************
#include "headers.h"

//if you call delete on a NULL pointer, will it still be attempted to be deleted?
using namespace std; 

int main()
{

	//i stuck to 100 items, more would be a bit much to display, and probalby would not let me print
	/**********************************************************
	Part: Some practice
	**********************************************************/
	//var decarations
	unsigned int seed;					//a random seed
	int num;							//to hold an int value
	arrayListType<int> list(100);		//create a list of int type
	//char tag; 

	cout<<"loading the list with random numbers between 0 and 666 ... "<<endl; 

	cout<<"Enter a random seed => ";	//get the random seed
	cin >> seed; 
	srand(seed);						//set the randon seed

	//generate list
	for (int i=0; i <100 ; i++)			//insert 50 random numbers into the list
	{
		num = rand()%501; 
		list.insertLast(num);
	}
	//copy list twice
	arrayListType<int> list2(list);
	arrayListType<int> list3(list);

	cout<<"the list before being sorted is"<<endl;
	//display list presorted
	list.print(); 
	cout<<endl; 
	/*//this makes it a bit messy, i think only one display is needed
	list2.print();
	cout<<endl;
	list3.print();
	cout<<endl;
	*/
	//sort the list, then display
	list.insertionSort();
	list.print(); 
	cout<<endl; 

	list2.iterativeMergeSort();
	list2.print();
	cout<<endl;

	//this fails when tested with 10k integers, not sure why, might be a memory leak, it is writing out of heap.  might just be using too much resources
	//with 10k it will finish sorting, but will return an error, if number is too large, the program will crash
	list3.iterativeMedian3QuickSort();
	list3.print();
	cout<<endl;



	return 0; 
}