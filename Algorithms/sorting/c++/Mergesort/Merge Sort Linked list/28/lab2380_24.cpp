// *****************************************************************
// Author:	alex martinez
// Class:	CSCI/CMPE 2380, Spring 2009
// Lab 24:	Merge Sort
// Date:	January 17, 2009
// Comment: The code here is meant to be revised.
//
//-----------------------------------------------------------------
//
// Description: This file contains the prototype of the class arrayListType with 
// two versions of merge-sort algorithm: the recursive one and the iterative one. 
// simple sorting algorithms of insertion sort, selection sort and bubble sort
// also included. 
//
// Since we have implemented the array based list, we can choose to add a new set of 
// searching and sorting methods into it:
//	
// Search methods include linear search, randomized search, and binary search.
//
// sorting methods include bubble sort, selection sort, insertion sort, 
// and two versions of merge sort (the recrusive one and the iterative one). 
// 
// Compile and run your program. When everything is fine,
// print your .h and .cpp files and turn them to me or the TA.
// *****************************************************************
#include "headers.h"

using namespace std; 

int main()
{
	/**********************************************************
	 Part: Some practice
	 **********************************************************/
	//var decarations
	unsigned int seed;					//a random seed
	int num;							//to hold an int value
	arrayListType<int> list(100);		//create a list of int type
	char tag; 
	
	cout<<"loading the list with random numbers between 0 and 666 ... "<<endl; 

	cout<<"Enter a random seed => ";	//get the random seed
	cin >> seed; 
	srand(seed);						//set the randon seed

	for (int i=0; i < 45; i++)			//insert 50 random numbers into the list
	{
		num = rand()%41; 
		list.insertLast(num);
	}
	list.print(); 
	cout<<endl; 
	
	//list.insertionSort(); 
	//list.bubbleSort(); 
	//list.selectionSort(); 
	
	cout<<"Want to use recursive merge Sort (r) or iterative merge sort (i) => "; 
	cin>>tag; 
	switch(tag)
	{
	case 'r':
	case 'R':	cout<<"calling recusing merge sort .... "<<endl; 
				list.recursiveMergSort();
				break; 
	case 'i':
	case 'I':
				cout<<"calling iterative merge sort ....."<<endl; 
				list.iterativeMergeSort(); 
				break;
	default: 
				cout<<"entered a wrong choice"<<endl;
	}

	cout<<"after sorting, the list is ....."<<endl; 
	cout<<list<<endl; 

	/*while(true)
	{
		cout<<"Enter a number to search or stop when -99 is entered => "; 
		cin >> num;
		if(num == -99)					//break when the event/sentenel -99 happened
			break; 
		list.binSearch(num);			//do binary search
	} 
	*/
	/***************************************************************
	 Part Two: Try to create a list of strings and practice 
	 both recursive merge sort and the iterative merge sort.
	 ***************************************************************/
	
	//add your code here
	
	string swap;
	int character;
	arrayListType<string> strings(100);
	strings.insertFirst("abc");
	strings.insertLast("xyz");
	cout<<"the list is"<<endl;
	strings.print();
	cout<<endl;
	//insert some random 5 charactar strings into list
	for(int i=50; i>0;i--)
	{
		swap="";
		for(int j=5;j>0;j--)
		{
			srand(i+j);
			character=(rand()%27)+65;
			swap=swap+char(character);
		}
		character=rand()%1000;
		if(character%2==0)
			strings.insertFirst(swap);
		else
			strings.insertLast(swap);
	}
	cout<<"now the list is"<<endl;
	strings.print();
	cout<<endl;
	/*while(swap!="-99")
	{
		cout<<"enter a string to search for, or enter -99 to exit :";
		cin>>swap;

		if(strings.search(swap))
			cout<<"the string "<<swap<<" was found"<<endl;
		else
			cout<<"the string "<<swap<<" was not found"<<endl;

	}
	*/
	/*
	strings.deleteItem("abc");
	cout<<"now abc is no longer in the list"<<endl;
	strings.print();
	cout<<endl;
	strings.destroyList();
	cout<<"now the list is destroyed";
	strings.print();
	cout<<endl;
	*/
		cout<<"Want to use recursive merge Sort (r) or iterative merge sort (i) => "; 
	cin>>tag; 
	switch(tag)
	{
	case 'r':
	case 'R':	cout<<"calling recusing merge sort .... "<<endl; 
				strings.recursiveMergSort();
				break; 
	case 'i':
	case 'I':
				cout<<"calling iterative merge sort ....."<<endl; 
				strings.iterativeMergeSort(); 
				break;
	default: 
				cout<<"entered a wrong choice"<<endl;
	}

	cout<<"after sorting, the list is ....."<<endl; 
	cout<<strings<<endl; 

	/***************************************************************
	 Part Three: First define a personType class with three data members 
	 (string name, int ID, and double grade). Second, create a list
	 of personType. Third, both recursive merge sort and the iterative merge sort.
	 ***************************************************************/
	
	//add your code here
	//test to see if operator overload worked
	personType me("alex", 0, 99);
	personType otherme("alex", 0, 99);
	me==otherme?   cout<<"yes":cout<<"no";

	cout<<endl;

	//make up some people to insert
	personType person1("anonymous", 1, 100);
	personType person2("anyone", 2, 80);
	personType person3("enoyna", 3, 96);
	personType person4("soumynona", 4, 98);


	//would have to actually do the same with any list, doesn seem to be safe guard against duplication
	//i think i need to edit list template, to make sure that an object being inserted does not have the same id, and an object already there
	arrayListType<personType> people(10);
	people.insertFirst(me);
	people.insertFirst(person1);
	people.insertFirst(person2);	
	people.insertFirst(person3);	
	people.insertFirst(person4);

	people.print();
	string quit="100";
		cout<<"Want to use recursive merge Sort (r) or iterative merge sort (i) => "; 
	cin>>tag; 
	switch(tag)
	{
	case 'r':
	case 'R':	cout<<"calling recusing merge sort .... "<<endl; 
				people.recursiveMergSort();
				break; 
	case 'i':
	case 'I':
				cout<<"calling iterative merge sort ....."<<endl; 
				people.iterativeMergeSort(); 
				break;
	default: 
				cout<<"entered a wrong choice"<<endl;
	}

	cout<<"after sorting, the list is ....."<<endl; 
	cout<<people<<endl; 
	int id;
	double grade;
/*	while(quit!="-99")
	{
		//create a person to search for
		cout<<"enter the name id and grade or person you are looking for :";
		cin>>swap>>id>>grade;
		personType search(swap, id, grade);
		
		//search
		//there is no error checking for correct type yet in the == operator
		people.search(search)? cout<<"found":cout<<"not found";
		cout<<endl;
		//flag
		cout<<"enter 0 to continue, or -99 to quit :";
		cin>>quit;
	}

	people.deleteItem(me);
	
	cout<<"alex is not longer in the list"<<endl;
	people.print();
	cout<<endl;
	people.destroyList();
	people.print();
	cout<<endl;
	cout<<"the list is gone now"<<endl;
	*/

	//complete the program
	return 0; 
}