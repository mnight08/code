#include <iostream>
#include <string>
#include <fstream>
#include "math.h"
using namespace std;

struct linkedtable
{
	int data;
	linkedtable *next;
	linkedtable *previous;
	linkedtable();
};

linkedtable::linkedtable()
{
	next=NULL;
	previous=NULL;
}
//a haphazardly put together double linked list.
class linkedlistoftables
{
public:
	linkedtable*head;
	linkedtable*tail;
	void insert(int x);
	linkedlistoftables();
	
};

linkedlistoftables::linkedlistoftables()
{
	head=NULL;
	tail=NULL;
}

void linkedlistoftables::insert(int x)
{
	if(head==NULL)
	{
		head=new linkedtable;
		head->data=x;
		head->previous=NULL;
		tail=head;
		head->next=NULL;
	}
	else
	{


		linkedtable* temp;
		temp=tail;
		tail=new linkedtable;
		
		tail->data=x;
		tail->previous=temp;
		tail->next=NULL;

		if(temp!=NULL)
		{
			temp->next=tail;
			
		}
	}
}

bool radixsort(fstream &load, int range, int base)
{
	int temp=0;
	linkedlistoftables* table=NULL;
	linkedlistoftables* swaptable=NULL;
	linkedlistoftables* swappointer=NULL;


	double power=0;
	int stop= int(log(double(range))/log(double(base)))+1;

	table=new linkedlistoftables[base];


	//load the items, first iteration of sort
	for(;load;)
	{
		load>>temp;
		table[temp%base].insert(temp);
	}

	power++;
	double dividebyme;
	for(;power<stop;power++)
	{	
		swaptable=new linkedlistoftables[base];
		dividebyme=int(pow(double (base),power));
		//put numbers in the buckets
		for(int i=0;i<base;i++)
		{
			for(linkedtable *current=table[i].head;current!=NULL;current=current->next)
			{
				temp=current->data;
				swaptable[int(temp/dividebyme)%base].insert(temp);

			}
		}
		swappointer=table;
		table=swaptable;
		swaptable=swappointer;
		delete[] swappointer;
		
		
		//cout<<power<<endl;
	}

	string storagename;
	storagename="radixsort.txt";
	//storagename=storagename+char(32+range%base);
	
	fstream store;
	store.open(storagename.c_str(), ios::out);

	for(int i=0;i<base;i++)
	{
		for(linkedtable* current=table[i].head;current!=NULL;current=current->next)
			store<<current->data<<endl;
	}
	return true;
}

