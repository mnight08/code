#pragma once
#include "dictionarynode.h"
#include "checklist.h"
#include "onqueue.h"
#include <list>
#include <string>
#include <queue>
#include <vector>
#include <fstream>
#include "edgesexist.h"
#include <algorithm>
using namespace std;
//The problem is to find the shortest path between words when the edges are signified by a letter shift. Breadth first or depth first search 
//works if we can find all the edges from a given word fast. Comparing all words to check if there is an edge is slow.
//My solution to this problem was to create a tree that has a node for each letter that shows up in the dictionary.  Each 
//word gives a path through this tree. I believe this is called a radix tree. We note that a word is in the dictionary
//by marking the end of the path with true.  Once this tree is built, we can find all the words that are next to a given word
//by considering all possible single letter shifts. If the longest word in the dictionary has K letters, and there are M 
//characters in the alphabet
class dictionary
{
public:
	
	//a count of how many words are in the dictionary
	int wordcount;
	int hashletter(string&,int);
	int search(dictionarynode*,string);
	//datastructures that will be used for the actual use of the graph, specifically edgefinding
	//the base of the tree that will be used storing each word based on the letter first letter in a string.
	//based off of this tree, i can find the edges in about (length of string)^2*(26)  this is not too large, and i only have to do it once with the table
	
	dictionarynode * head;
	
	//this is a memoization table, kinda, essentially check if an answer to a question exist, if it does, then you have it very fast, else, 
	//find the answer, and store it. the question is:  what are the edges to this word.
	//the list is the list of keys refering to the words that are stored in a table
	vector<edgesexist> edges;

	//a very basic mapping setup just gives each string a hash value, allows for constant time lookup to check if edges are already discovered, or 
	//to keep track of if they are in the queue, or have been checked, if key is known,  takes logn to find key
	vector<keys> map;

	//return key that is associated with string.  this is binary search, when the word is found, then return the key,  the list is presorted, so no sorting is neccessary
	int search(string &data,int leftbound, int rightbound);

	//methods for graph building
	void inserttotree(dictionarynode *current,string & tobeinserted, int& id);
	void insert(keys &key);
	void load(ifstream&);

	//the rest is pretty much for breadth first search
	//stores if a vertex has been checked, or queued yet
	//if i stay consistent with mapping, this just needs to store bool values
	//this only need to be present in shortest path algorithm
//	vector<checklist> used;

	//methods for use after graph is built
	//check if edges vector has a list at position keys.key if not , then use algorithm to find them, and store at that position.  that way other algorithms can use edges
	//this is the only function left to check
	void getedges(int&);
	//methods for breadth first search

		
	//not sure how to track paths
	bool shortestpath(string& source , string &destination);
	//find the edges to the hashed value, and then put in memoization table
	
	void putedgesonqueue(int&, queue<onqueue> &tocheck,vector<checklist>&);

	//general class stuff needed
	dictionary(void);
	~dictionary(void);
};
