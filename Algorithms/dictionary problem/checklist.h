#pragma once
#include "word.h"
#include <vector>
using namespace std;
class checklist
{
public:
	vector<bool> checked;
	//each item must have unique key for this to be useful
	void checkitem(int key);
	bool isitemchecked(int key);

	checklist(int size);
	~checklist(void);
};
