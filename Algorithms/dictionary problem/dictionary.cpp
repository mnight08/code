#include "dictionary.h"
//this would be awesome if it worked.  binary search in two lines
int dictionary::search(string& query,int leftbound, int rightbound)
{
	int mid =(leftbound+rightbound)/2;
	if(rightbound<leftbound) return -1;else	if(map[mid].data==query) return mid; else if(query<map[mid].data)return search(query,leftbound, mid-1);else return search(query, mid+1, rightbound);
}

bool dictionary::shortestpath(string &source, string &destination)
{	
	int s=search(source,0,wordcount-1);
	int d=search(destination,0,wordcount-1);
	if(s<0||d<0||map[s].data.size()!=map[s].data.size())
		return false;
	vector<checklist> used;
	used.resize(wordcount);
	queue<onqueue> tocheck;
	tocheck.push(onqueue(s,-1));
	used[s].queued=true;
	vector<onqueue> pathtracking;
	pathtracking.resize(wordcount);
	for(;!tocheck.empty();)
	{
		if(used[tocheck.front().me].checked==true)
			tocheck.pop();
		else
		{
			pathtracking[tocheck.front().me]=tocheck.front();
			if(tocheck.front().me==d)
			{
				for(int i=tocheck.front().me;i!=-1;i=pathtracking[i].parent)
					cout<<map[i].data<<endl;
				return true;
			}
			else
			{
				used[tocheck.front().me].checked=true;
				putedgesonqueue(tocheck.front().me,tocheck,used);
				tocheck.pop();
			}
		}
	}
	return false;
}

//i need to remember that th is should start from the back of the word, when traversing the tree.
//this is going to be my most complicated function, so i will do it last, mostly cause its gonna cause crazy errors
void dictionary::getedges(int & k)
{
	//there is an error i cant see here, but it probably has to do with j, too tired to find right now.
	//iterate thru all the positions, that the wildcard can be in
	for(int i=0;i<map[k].data.size();i++)
	{
		string swap(map[k].data);
		
		//something is wrong around here, and i cant find it
		//go as far as you can til you hit wild card
		dictionarynode* current=head;
		for(int j=0;j!=i;j++){
			current=current->next[hashletter(swap,swap.size()-1)];
			swap.erase(swap.size()-1);
		}

		swap.erase(swap.size()-1);
		int temp;
		//check if the path remaining is true for the wildcard varying thru every character. if it is, then the word is in the dictionary, and it is an edge to k
		for(int j=0;j<69;j++){
			temp=search(current->next[j],swap);
			if(temp>-1)
				edges[k].edges.insertItem(temp);
		}

	}
}

int dictionary::search(dictionarynode* current, string query)
{
	if(current==NULL)
		return -1;
	else if(query=="")
		return current->key;
	else
	{
		int temp=hashletter(query,query.size()-1);
		query.erase(query.size()-1);
		return search(current->next[temp],query);
	}

}

void dictionary::putedgesonqueue(int& k,queue<onqueue> &tocheck,vector<checklist>& used)
{
	//this is only useful if there are edges from the given vertex
	if(!edges[k].exist)
	{
		getedges(k);
		edges[k].exist=true;
	}
	edges[k].edges.loadqueue(tocheck,used,k);	
}

void dictionary::load(ifstream& input)
{
	string swap;
	keys temp;
	list<keys> inserted;
	for(int i=0;!input.eof();i++)
	{
		input>>swap;
		if(swap!=""&&swap!="stopallplease"){	
			temp.data=swap;temp.key=i;
			inserted.push_back(temp);
			insert(temp);   
		}
	}

	input.close();

	edges.resize(wordcount);
	map.resize(wordcount);		
	for(int i=0;!inserted.empty();i++)
	{
		temp=inserted.front();
		map[temp.key]=temp;
		inserted.pop_front();
	}
}

void dictionary::insert(keys& tobeinserted)
{
	inserttotree(head,tobeinserted.data,tobeinserted.key);
}

//there is an error in here somewhere//i dont know if i fixed this yet
void dictionary::inserttotree(dictionarynode* current, string &tobeinserted,int& id)
{	
	//if current == null, then there is a screwup
	if(current==NULL)
		return;
	else if(tobeinserted=="")
	{
		current->present=true;
		current->key=id;
		wordcount++;
		return;		
	}
	else {
		int pos=tobeinserted.size()-1;
		int hash=hashletter(tobeinserted,pos);
		if(current->next[hash]==NULL)
		{
			current->next[hash]=new dictionarynode;
			current->next[hash]->letter=tobeinserted[pos];
		}

		current=current->next[hash];
		tobeinserted.erase(tobeinserted.end()-1);
		inserttotree(current,tobeinserted,id);
		//should work with lowercase ascii values
		//inserttotree(current->next[int(tobeinserted[tobeinserted.size()])-97],tobeinserted.erase(tobeinserted.end())));
	}
}

dictionary::dictionary(void)
{
	wordcount=0;
	head=new dictionarynode;
}

dictionary::~dictionary(void)
{

}

//this depends on the characters that appear
//lazy way to fix radix sort
int dictionary::hashletter(std::string& hashme, int pos)
{
	switch(hashme[pos])
	{
	case -32:
		return 0;
	case -31:
		return 1;
	case -30:
		return 2;
	case -28:
		return 3;
	case -27:
		return 4;
	case -25:
		return 5;
	case -24:
		return 6;
	case -23:
		return 7;
	case -22:
		return 8;
	case -19:
		return 9;
	case -18:
		return 10;
	case -17:
		return 11;
	case -15:
		return 12;
	case -13:
		return 13;
	case -12:
		return 14;
	case -10:
		return 15;
	case -8:
		return 16;
	case -7:
		return 17;
	case -6:
		return 18;
	case -5:
		return 19;
	case -4:
		return 20;
	case 33:
		return 21;
	case 39:
		return 22;
	case 45:
		return 23;
	case 65:
		return 24;
	case 66:
		return 25;
	case 67:
		return 26;
	case 68:
		return 27;
	case 69:
		return 28;
	case 71:
		return 29;
	case 72:
		return 30;
	case 73:
		return 31;
	case 74:
		return 32;
	case 75:
		return 33;
	case 76:
		return 34;
	case 78:
		return 35;
	case 80:
		return 36;
	case 82:
		return 37;
	case 83:
		return 38;
	case 84:
		return 39;
	case 85:
		return 40;
	case 86:
		return 41;
	case 97:
		return 42;
	case 98:
		return 43;
	case 99:
		return 44;
	case 100:
		return 45;
	case 101:
		return 46;
	case 102:
		return 47;
	case 103:
		return 48;
	case 104:
		return 49;
	case 105:
		return 50;
	case 106:
		return 51;
	case 107:
		return 52;
	case 108:
		return 53;
	case 109:
		return 54;
	case 110:
		return 55;
	case 111:
		return 56;
	case 112:
		return 57;
	case 113:
		return 58;
	case 114:
		return 59;
	case 115:
		return 60;
	case 116:
		return 61;
	case 117:
		return 62;
	case 118:
		return 63;
	case 119:
		return 64;
	case 120:
		return 65;
	case 121:
		return 66;
	case 122:
		return 67;
	case -1:
		return 68;
	default:
		cout<<"fail"<<" "<<hashme<<endl;
		return -1;
	}
}
