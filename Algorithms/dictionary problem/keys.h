#pragma once

#include <string>
using namespace std;

//this is to get perfect hashing.  take in one word, and give that whatever order it came in at.
class keys
{
public:
	int key;
	string data;
	keys(void);
	keys(int i, string s);
	~keys(void);
	int hash(){return key;}

};
