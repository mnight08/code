#include "dictionary.h"
#include <ifstream>

//This program loads a dictionary and efficiently finds the shortest path between two words by changing a single character at a 
//time.
int main(){

	//Read in dictionary
	ifstream input;
	input.open("largedictionary.txt");
	dictionary words;
	words.load(input);

	//Test the data structure. Verifies that a path exist. Does not verify that it is the shortest.
	string temp3, temp4;
	while(true)
	{	
		cout<<"enter word 1:";
		cin>>temp3;
		cout<<"enter word 2:";
		cin>>temp4;
		cout<<words.shortestpath(temp3,temp4)<<endl;
	}
	return 0;
}
