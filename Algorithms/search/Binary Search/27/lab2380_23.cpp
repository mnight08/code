// *****************************************************************
// Author:	alex martinez
// Class:	CSCI/CMPE 2380, Spring 2009
// Lab 23:	Binary Searching
// Date:	January 17, 2009
// Comment: The code here is meant to be revised.
//
//-----------------------------------------------------------------
//
// This lab exercise is to implement binary search.
//
// Since we have already implemented derived the ordered array-based listed
// from the array based list, we can simply add the additional method binary 
// search into the ordered array-based list.
//
// Compile and run your program. When everything is fine,
// print your .h and .cpp files and turn them to me or the TA.
// *****************************************************************
#include "headers.h" 

using namespace std; 

int main()
{
	//a note:  insertFirst, and insertLast should be overwritten when the ordered array is derived.  
	//they defeat the purpose of it being ordered, because you can insert out of order with them


	/**********************************************************
	 Part: Some practice
	 **********************************************************/
	//var decarations
	unsigned int seed;					//a random seed
	int num;							//to hold an int value
	orderedArrayListType<int> list(100);//create a list of int type
	
	cout<<"loading the list with random numbers between 0 and 666 ... "<<endl; 

	cout<<"Enter a random seed => ";	//get the random seed
	cin >> seed; 
	srand(seed);						//set the randon seed

	for (int i=0; i < 100; i++)			//insert 50 random numbers into the list
	{
		num = rand()%667; 
		list.insertNode(num);
	}
	list.print(); 
	cout<<endl; 

	while(true)
	{
		cout<<"Enter a number to search or stop when -99 is entered => "; 
		cin >> num;
		if(num == -99)					//break when the event/sentenel -99 happened
			break; 
		list.binSearch(num);			//do binary search
	} 
	
	/***************************************************************
	 Part Two: Try to create a list of strings and practice 
	 binary search
	 ***************************************************************/
	
	//add your code here


	
	string swap;
	int character;
	orderedArrayListType<string> strings(100);
	strings.insertNode("abc");
	strings.insertNode("xyz");
	cout<<"the list is"<<endl;
	strings.print();
	cout<<endl;
	//insert some random 5 charactar strings into list
	for(int i=50; i>0;i--)
	{
		swap="";
		for(int j=5;j>0;j--)
		{
			srand(i+j);
			character=(rand()%27)+65;
			swap=swap+char(character);
		}
		character=rand()%1000;
		if(character%2==0)
			strings.insertNode(swap);
		else
			strings.insertNode(swap);
	}
	cout<<"now the list is"<<endl;
	strings.print();
	cout<<endl;
	while(swap!="-99")
	{
		cout<<"enter a string to search for, or enter -99 to exit :";
		cin>>swap;

		if(strings.binSearch(swap))
			cout<<"the string "<<swap<<" was found"<<endl;
		else
			cout<<"the string "<<swap<<" was not found"<<endl;

	}
	
	strings.deleteNode("abc");
	cout<<"now abc is no longer in the list"<<endl;
	strings.print();
	cout<<endl;
	strings.destroyList();
	cout<<"now the list is destroyed";
	strings.print();
	cout<<endl;
	/***************************************************************
	 Part Three: First define a personType class with three data members 
	 (string name, int ID, and double grade). Second, create a list
	 of personType. Third, pratice binary search
	 ***************************************************************/
	
	//add your code here

	//test to see if operator overload worked
	personType me("alex", 0, 99);
	personType otherme("alex", 0, 99);
	me==otherme?   cout<<"yes":cout<<"no";

	cout<<endl;

	//make up some people to insert
	personType person1("anonymous", 1, 100);
	personType person2("anyone", 2, 80);
	personType person3("enoyna", 3, 96);
	personType person4("soumynona", 4, 98);


	//would have to actually do the same with any list, doesn seem to be safe guard against duplication
	//i think i need to edit list template, to make sure that an object being inserted does not have the same id, and an object already there
	orderedArrayListType<personType> people(10);
	people.insertNode(me);
	people.insertNode(person1);
	people.insertNode(person2);	
	people.insertNode(person3);	
	people.insertNode(person4);

	people.print();
	string quit="100";
	int id;
	double grade;
	while(quit!="-99")
	{
		//create a person to search for
		cout<<"enter the name id and grade or person you are looking for :";
		cin>>swap>>id>>grade;
		personType search(swap, id, grade);		//i might wanna change this name,  it already confused me.  search is not a function here, it is only passed as a parameter
		
		//search
		//there is no error checking for correct type yet in the == operator
		people.binSearch(search)? cout<<"found":cout<<"not found";
		cout<<endl;
		//flag
		cout<<"enter 0 to continue, or -99 to quit :";
		cin>>quit;
	}

	people.deleteNode(me);
	
	cout<<"alex is not longer in the list"<<endl;
	people.print();
	cout<<endl;
	people.destroyList();
	people.print();
	cout<<endl;
	cout<<"the list is gone now"<<endl;
	


	//complete the program
	return 0; 
}