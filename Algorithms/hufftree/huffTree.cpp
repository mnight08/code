#include "huffTree.h"

huffTree::huffTree()
{
	children[0]=NULL;
	children[1]=NULL;
	sequence=NULL;
	value=0;
}
void huffTree::makeReport(ofstream& output)
{
	string label="";

	if(this->children[0])
	{
		label+="0";
		recreport(output, label,this->children[0]);

		label.erase(label.end()-1);
	}
	if(this->children[1])
	{	
		label+="1";
		recreport(output, label,this->children[1]);
	}
}

void huffTree::recreport(ofstream &output, string pathlabel, huffTree *current)
{
	if(current==NULL)
		return;

	if(current->children[0]==NULL)
	{
		//pathlabel+='0';
		//this is stupid, but << would not work directly on string
		output<<current->sequence<<" "<<pathlabel.c_str()<<endl;
		return;
	}

/*	else if(current->children[1]==NULL)
	{
		//pathlabel+='1';
		output<<current->sequence<<" "<<pathlabel.c_str()<<endl;
	}
*/
	else
	{
		pathlabel+='0';
		recreport(output, pathlabel,current->children[0]);
		pathlabel.erase(pathlabel.end()-1);

		
		pathlabel+='1';
		recreport(output, pathlabel,current->children[1]);
		return;		
	}

}

huffTree::huffTree(int x,char a)
{
	value=x;
	sequence=a;
	children[0]=NULL;
	children[1]=NULL;
}

huffTree::huffTree(huffTree* a, huffTree* b)
{
	value=a->value+b->value;
	sequence=NULL;
	children[0]=a;
	children[1]=b;
}