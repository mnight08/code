
#include <algorithm>
#include "minHeap.h"
using namespace std;

int minHeap::getSize()
{
	return size;
}

//private helper functions

int minHeap::parent(int i)
{
	return (i-1)/2;
}

int minHeap::lchild(int i)
{
	return (i*2) + 1;
}

int minHeap::rchild(int i)
{
	return (i+1)*2;
}


//public functions

minHeap::minHeap(int max)
{
	heap = new huffTree*[max];
	for(int i=0;i<max;i++)
		heap[i]=NULL;
	size = 0;
}

//dumb test function
void minHeap::display()
{
	//didnt want to fix this right now, its not really needed.
	/*for(int i=0; i<size; i++)
		cout << heap[i] << endl;
*/
}

void minHeap::insert(huffTree *s)
{
	//put new item at end of heap
	heap[size] = s;
	size++;

	int current = size-1;
	while( heap[parent(current)]->value > heap[current]->value )
	{
		//bubble current item up
		swap( heap[current], heap[parent(current)] );
		current = parent(current);
	}
}

huffTree* minHeap::extractMin()
{
	if(size<1)
	{
		cout<<"empty heap"<<endl;	
		return NULL;
	}
	swap(heap[size-1],heap[0]);
	huffTree* temp=heap[size-1];
	heap[size-1]=NULL;
	size--;
	int current=0;
	while( (rchild(current)<size&&lchild(current)<size)&&(heap[lchild(current)]->value < heap[current]->value ||heap[rchild(current)]->value<heap[current]->value))
	{
		//bubble current item up
		if(heap[lchild(current)]->value<heap[rchild(current)]->value)
		{
			swap( heap[current], heap[lchild(current)]);
			current=lchild(current);
		}
		else
		{
			swap( heap[current], heap[rchild(current)]);
			
			current = rchild(current);
		}
	}

	//i hope this is correct
	if(!(rchild(current)<size&&lchild(current)<size))
	{
		if(rchild(current)<size)
		{
			if(heap[rchild(current)]->value<heap[current]->value)
				swap(heap[current],heap[rchild(current)]);
		}
		
		if(lchild(current)<size)
		{
			if(heap[lchild(current)]->value<heap[current]->value)
				swap(heap[current],heap[lchild(current)]);
		}
	}
	return temp;
}

/*
void minHeap::initializeheap(huffTree *itemlist,int newsize)
{
	heap=new huffTree[size];
	size=newsize;
	for(int i=0;i<size;i++)
		heap[i]=itemlist[i];
	for(int i=0;i<size;i++)
	{
		int current = size-i-1;
		while( heap[parent(current)].value > heap[current].value )
		{
		//bubble current item up
		swap( heap[current], heap[parent(current)] );
		current = parent(current);
		}
	}

}
*/