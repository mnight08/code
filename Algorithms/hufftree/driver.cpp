#include <fstream>
#include "AVLTree.h"
#include "minHeap.h"
#include "huffTree.h"
using namespace std;
 
void readAndInsert(AVLTree &avltree, ifstream & ifile)
{
	//what about file not existing?  this would be infinite loop
      while( ! ifile.eof()  )
      {
            char tmp = ifile.get();
            avltree.insertItem( tmp );
      }
}
 
int main()
{
	AVLTree tree;
      ifstream ifile;
      ofstream ofile;
 
      //open input and output files
      ifile.open("story.txt");
      ofile.open("report.txt");
 
      //read in each character of input file, insert into a BST
      AVLTree avltree;
      readAndInsert(avltree, ifile);
 
 
      //For each node in the BST, create a huffman tree,
      //and insert into a min heap
      minHeap heap(1000);
      avltree.loadHeap(heap);
 
      //while the heap has more than 1 huffTree, get 2 smallest,
      //merge them together, and put merged tree back in heap
      huffTree * h1;
      huffTree * h2;
 
      while( heap.getSize() > 1 )
      {
            h1 =heap.extractMin();
            h2 =heap.extractMin();
            heap.insert(new huffTree(h1,h2));
      }
 
      //take remaining huffTree, use to create a table of
      //codes for each char, write to output file
      huffTree * finalTree = heap.extractMin();
      finalTree->makeReport(ofile);
 
      //close files
      ifile.close();
      ofile.close();
     
 
      return 0;
}

 