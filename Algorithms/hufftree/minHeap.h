#include "huffTree.h"
#include<string>
#include<iostream>
using namespace std;
#ifndef minheap
#define minheap
class minHeap
{
public:
	minHeap(int max);

	//void initializeheap(huffTree** itemlist, int newsize);

	void insert(huffTree *s);
	huffTree *extractMin();

	//test function
	void display();
	int getSize();
private:
	int parent(int i);
	int lchild(int i);
	int rchild(int i);

	int size;  //number of items in heap
	//i need an pointer to pointers.  this is gonna be annoying to fix
	huffTree ** heap;

};

#endif