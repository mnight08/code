#include <fstream>
using namespace std;
#ifndef HUFFTREE
#define HUFFTREE
class huffTree  
{
public:
	int value;
	char sequence;//this should be a char*, for alphabets that try to group words.
	huffTree* children[2];//two children for each huff tree.
	huffTree(int,char);
	huffTree(huffTree *a, huffTree *b);

	void makeReport(ofstream& output);
	void recreport(ofstream& output, string pathlabel, huffTree* current);
	huffTree();
};

#endif