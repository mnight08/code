
#include <iostream>
#include <string>
#include "minHeap.h"
using namespace std;
#ifndef ALVTREE
#define AVLTREE
class node
{
public:
	node()
	{
		right = NULL;
		left = NULL;
		height = 0;
		frequency=0;
	}

	char data;
	node * right;
	node * left;
	int frequency;
	int height;
};


class AVLTree
{
public:
	AVLTree();

	void insertItem(char s);
    void display();
	void loadHeap(minHeap &heap);
	//string find(string s);

private:
	void rightRotation( node * & r );
	void leftRotation( node * & r );
	void doubleRotationRight( node * & r );
	void doubleRotationLeft( node * & r );

	int height(node *);
	void recinsertItem(char s, node * & root);
	void recDisplay(node * r);
	node * root;
	void recloadheap(node*, minHeap&);
};

#endif