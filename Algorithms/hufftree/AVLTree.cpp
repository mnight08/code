#include "AVLTree.h"
void AVLTree::loadHeap(minHeap &heap)
{
	recloadheap(root,heap);
}

void AVLTree::recloadheap(node* current, minHeap& heap)
{
	if(current==NULL)
		return;
	else 
	{
		//this line wont work for all cases, maybe...im confused a bit now
		heap.insert(new huffTree(current->frequency+1,current->data));
		recloadheap(current->left,heap);
		recloadheap(current->right,heap);
	}	
}

AVLTree::AVLTree()
{
	root = NULL;
}

int AVLTree::height(node * p)
{
	if( p == NULL )
		return -1;
	else
		return p->height;
}


void AVLTree::rightRotation( node * & r )
{
	node * a = r;
	node * b = r->right;
	node * bleft = b->left;

	r = b;
	a->right = bleft;
	b->left = a;

	a->height = max( height(a->right), height(a->left) ) +1;
	b->height = max( height(b->right), height(b->left) ) +1;
}


void AVLTree::leftRotation( node * & r )
{
	node * a = r;
	node * b = r->left;
	node * bright = b->right;

	r = b;
	a->left = bright;
	b->right = a;

	a->height = max( height(a->right), height(a->left) ) +1;
	b->height = max( height(b->right), height(b->left) ) +1;
}
void AVLTree::doubleRotationRight( node * & r )
{
	leftRotation( r->right );
	rightRotation( r );
}
void AVLTree::doubleRotationLeft( node * & r )
{
	rightRotation( r->left );
	leftRotation( r );
}




void AVLTree::recinsertItem(char s, node * & r)
{
	if( r == NULL )  //base case: empty tree
	{
		r = new node;
		r->data = s;
		r->height = 0;
	}
	else if( s < r->data  ) //insertItem left
	{
		recinsertItem(s, r->left);

		if( height(r->left) - height(r->right) > 1 )
		{  //violation!
			//do some type of rotation
			if( s < r->left->data )
				leftRotation(r);
			else //its a double
				doubleRotationLeft(r);
		}

		r->height = max( height(r->left), height(r->right) ) +1;
	}
	else if(s==r->data)
	{
		r->frequency++;
	}
	//insertItem right, if greater or equal to root
	else

	{
		recinsertItem(s, r->right);

		if( height(r->right) - height(r->left) > 1 )
		{
			if( s > r->right->data )
				rightRotation(r);
			else
				doubleRotationRight(r);
		}

		r->height = max( height(r->left), height(r->right) ) +1;
	}
}

void AVLTree::insertItem(char s)
{
	recinsertItem(s, root);
}

void AVLTree::recDisplay(node * r)
{
	if( r != NULL )
	{
		cout << r->data << endl;
		recDisplay(r->left);
		recDisplay(r->right);
	}
}

void AVLTree::display()
{
	recDisplay(root);
}