-- MySQL dump 10.11
--
-- Host: localhost    Database: afmprojdb
-- ------------------------------------------------------
-- Server version	5.0.51a-24+lenny2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `comments` (
  `comment_id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `data` text,
  `rating_id` int(11) NOT NULL,
  PRIMARY KEY  (`comment_id`),
  KEY `fk_comments_users1` (`user_id`),
  KEY `fk_comments_ratings1` (`rating_id`),
  CONSTRAINT `fk_comments_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_comments_ratings1` FOREIGN KEY (`rating_id`) REFERENCES `ratings` (`rating_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `engine_components`
--

DROP TABLE IF EXISTS `engine_components`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `engine_components` (
  `engine_component_id` int(11) NOT NULL,
  `game_engine_id` int(11) NOT NULL,
  `name` varchar(45) default NULL,
  PRIMARY KEY  (`engine_component_id`),
  KEY `fk_engine_components_game_engines1` (`game_engine_id`),
  CONSTRAINT `fk_engine_components_game_engines1` FOREIGN KEY (`game_engine_id`) REFERENCES `game_engines` (`game_engine_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `engine_components`
--

LOCK TABLES `engine_components` WRITE;
/*!40000 ALTER TABLE `engine_components` DISABLE KEYS */;
INSERT INTO `engine_components` VALUES (41,11,'moving');
/*!40000 ALTER TABLE `engine_components` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_engines`
--

DROP TABLE IF EXISTS `game_engines`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `game_engines` (
  `game_engine_id` int(11) NOT NULL,
  `rating_id` int(11) NOT NULL,
  `name` varchar(45) default NULL,
  PRIMARY KEY  (`game_engine_id`,`rating_id`),
  KEY `fk_game_engines_ratings1` (`rating_id`),
  CONSTRAINT `fk_game_engines_ratings1` FOREIGN KEY (`rating_id`) REFERENCES `ratings` (`rating_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `game_engines`
--

LOCK TABLES `game_engines` WRITE;
/*!40000 ALTER TABLE `game_engines` DISABLE KEYS */;
INSERT INTO `game_engines` VALUES (11,11,'The Fighting Mongooses');
/*!40000 ALTER TABLE `game_engines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `groups` (
  `group_id` int(11) NOT NULL default '1',
  `name` varchar(45) NOT NULL default 'guest',
  `rating_id` int(11) NOT NULL,
  `creation` varchar(10) default NULL,
  PRIMARY KEY  (`group_id`,`rating_id`),
  KEY `fk_groups_ratings1` (`rating_id`),
  CONSTRAINT `fk_groups_ratings1` FOREIGN KEY (`rating_id`) REFERENCES `ratings` (`rating_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (3,'GUEST',2,'12/16/11');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups_has_objects`
--

DROP TABLE IF EXISTS `groups_has_objects`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `groups_has_objects` (
  `group_id` int(11) NOT NULL,
  `object_id` int(11) NOT NULL,
  `priv` int(11) default NULL,
  PRIMARY KEY  (`group_id`,`object_id`),
  KEY `fk_groups_has_objects_objects1` (`object_id`),
  KEY `fk_groups_has_objects_groups1` (`group_id`),
  CONSTRAINT `fk_groups_has_objects_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_groups_has_objects_objects1` FOREIGN KEY (`object_id`) REFERENCES `objects` (`object_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `groups_has_objects`
--

LOCK TABLES `groups_has_objects` WRITE;
/*!40000 ALTER TABLE `groups_has_objects` DISABLE KEYS */;
/*!40000 ALTER TABLE `groups_has_objects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups_has_users`
--

DROP TABLE IF EXISTS `groups_has_users`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `groups_has_users` (
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY  (`group_id`,`user_id`),
  KEY `fk_groups_has_users_users1` (`user_id`),
  KEY `fk_groups_has_users_groups1` (`group_id`),
  CONSTRAINT `fk_groups_has_users_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_groups_has_users_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `groups_has_users`
--

LOCK TABLES `groups_has_users` WRITE;
/*!40000 ALTER TABLE `groups_has_users` DISABLE KEYS */;
INSERT INTO `groups_has_users` VALUES (3,3);
/*!40000 ALTER TABLE `groups_has_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `objects`
--

DROP TABLE IF EXISTS `objects`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `objects` (
  `object_id` int(11) NOT NULL,
  `tool_id` int(11) NOT NULL,
  `creation` varchar(10) default NULL,
  `creator_group_id` int(11) NOT NULL,
  `data` mediumtext,
  `name` varchar(45) default NULL,
  PRIMARY KEY  (`object_id`),
  KEY `fk_objects_tools1` (`tool_id`),
  KEY `fk_objects_groups1` (`creator_group_id`),
  CONSTRAINT `fk_objects_tools1` FOREIGN KEY (`tool_id`) REFERENCES `tools` (`tool_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_objects_groups1` FOREIGN KEY (`creator_group_id`) REFERENCES `groups` (`group_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `objects`
--

LOCK TABLES `objects` WRITE;
/*!40000 ALTER TABLE `objects` DISABLE KEYS */;
/*!40000 ALTER TABLE `objects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `objects_has_tags`
--

DROP TABLE IF EXISTS `objects_has_tags`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `objects_has_tags` (
  `object_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY  (`object_id`,`tag_id`),
  KEY `fk_objects_has_tags_tags1` (`tag_id`),
  KEY `fk_objects_has_tags_objects1` (`object_id`),
  CONSTRAINT `fk_objects_has_tags_objects1` FOREIGN KEY (`object_id`) REFERENCES `objects` (`object_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_objects_has_tags_tags1` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`tag_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `objects_has_tags`
--

LOCK TABLES `objects_has_tags` WRITE;
/*!40000 ALTER TABLE `objects_has_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `objects_has_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ratings`
--

DROP TABLE IF EXISTS `ratings`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ratings` (
  `rating_id` int(11) NOT NULL,
  `rating` int(11) default NULL,
  `count` int(11) default NULL,
  PRIMARY KEY  (`rating_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `ratings`
--

LOCK TABLES `ratings` WRITE;
/*!40000 ALTER TABLE `ratings` DISABLE KEYS */;
INSERT INTO `ratings` VALUES (1,3,0),(2,3,0),(11,3,0),(16,3,0);
/*!40000 ALTER TABLE `ratings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `tags` (
  `tag_id` int(11) NOT NULL,
  `data` tinytext,
  PRIMARY KEY  (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tools`
--

DROP TABLE IF EXISTS `tools`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `tools` (
  `tool_id` int(11) NOT NULL,
  `rating_id` int(11) NOT NULL,
  `engine_component_id` int(11) NOT NULL,
  `name` varchar(45) default NULL,
  PRIMARY KEY  (`tool_id`),
  KEY `fk_tools_ratings1` (`rating_id`),
  KEY `fk_tools_engine_components1` (`engine_component_id`),
  CONSTRAINT `fk_tools_ratings1` FOREIGN KEY (`rating_id`) REFERENCES `ratings` (`rating_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tools_engine_components1` FOREIGN KEY (`engine_component_id`) REFERENCES `engine_components` (`engine_component_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `tools`
--

LOCK TABLES `tools` WRITE;
/*!40000 ALTER TABLE `tools` DISABLE KEYS */;
INSERT INTO `tools` VALUES (1,16,41,'pathmaking.epanel.class');
/*!40000 ALTER TABLE `tools` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tools_has_tags`
--

DROP TABLE IF EXISTS `tools_has_tags`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `tools_has_tags` (
  `tool_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY  (`tool_id`,`tag_id`),
  KEY `fk_tools_has_tags_tags1` (`tag_id`),
  KEY `fk_tools_has_tags_tools1` (`tool_id`),
  CONSTRAINT `fk_tools_has_tags_tools1` FOREIGN KEY (`tool_id`) REFERENCES `tools` (`tool_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tools_has_tags_tags1` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`tag_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `tools_has_tags`
--

LOCK TABLES `tools_has_tags` WRITE;
/*!40000 ALTER TABLE `tools_has_tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `tools_has_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL auto_increment,
  `username` varchar(45) default NULL,
  `email` varchar(45) default NULL,
  `created` varchar(10) default NULL,
  `number_of_post` int(11) default NULL,
  `rating_id` int(11) NOT NULL,
  `user_key` blob,
  `name` varchar(45) default NULL,
  PRIMARY KEY  (`user_id`,`rating_id`),
  KEY `fk_users_ratings1` (`rating_id`),
  CONSTRAINT `fk_users_ratings1` FOREIGN KEY (`rating_id`) REFERENCES `ratings` (`rating_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (3,'GUEST','awesomo@awesomeproductions.net','12/16/11',0,1,'�;��-���X/\r�\'lnO,(�����`<��l���w)!�,	?�������]a(��H0�+=�6H�K)�O�F/p���d|����I��I��*M�x�Y/|��N$8۷J�','guesty');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2011-12-17  3:41:40
