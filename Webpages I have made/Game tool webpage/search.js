function startsearch(){
	//add autocomplete to the search box.
	updateautocomplete();	
	updateresults();
}

//make ajax call, get top level of accodion rebuild accordion.  cant start using accordion then search box.
//put requirement to prefix searches, just put the query into session really
function updateresults(){
	var searchme=$("#searchbox").val();
	//just set the post variables properly, and you should be fine
	$.post('searchservice.php',{level:"0",query:searchme},function(data)
					{
						//alert(data);
						$("#searchtree").html(data);
					}
			);

	$(".accordion").click(updateaccordion);
	$(".accordion").accordion({collapsible:true,active:false});
}

//make ajax call, generate next level of accordion.
function updateaccordion()
{
	currentaccordion=this;
	var searchme=$("#searchbox").val();
	var parentname=$(currentaccordion).attr("name");
	//get the current level
	var depth=$(currentaccordion).attr("value")+1;
	$.post("searchservice.php",{level:depth,query:searchme,parent:parentname},function(data)
					{
						$(currentaccordion).html(data);
						$("#searchtree").html(data);
					});

	$(".accordion").click(updateaccordion);
	$(".accordion").accordion();
}

//load dictionary from server and make it a source for the autocomplete list 
function updateautocomplete()
{
	$.post("getdictionary.php",function(data){
									var dictionary=$.makeArray($(data).filter('.term').text(function (index,data){ return data}).map(function(){return $(this).text()}));
									$("#searchbox").autocomplete(
																	{
																		source:dictionary
																	}
																)
								}
			)
}