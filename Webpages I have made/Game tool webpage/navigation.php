<?
	require_once("functiondefinitions.php");
	session_start();
	error_reporting(false);	
	//if the user goes through the navigation page they get a session flag saying that until they are finished using the navigation page.
	//this means that if the user accesses one of the pages, and the flag is not set, they did not go through this page.
	startnavigation();
	setsessionflags();
	//include the required page
	loadpage();
	endnavigation();
?>

