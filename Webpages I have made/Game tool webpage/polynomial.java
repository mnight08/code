package pathmaking;
import Jama.*;

import java.lang.Math.*;
public class polynomial 
{
	int degree;
	double[] coefficients;
	

	polynomial()
	{
		degree=-1;
		coefficients=null;
	}
	
	polynomial( double[] coef)
	{
		degree=coef.length;
		coefficients=coef;
	
	}

        //return a polynomial that interpolatesthe points given
        polynomial (vector times, vector positions,int size)
        {
            if(times.dimension!=positions.dimension)
            {
                System.out.println("wrong sized vectors to interpolate");
            }
            else
            {
                //make matrix with coefficients row one is 1, x1, x1^2... x1^n
                Matrix m= new Matrix(size,size);
                Matrix in=new Matrix(size,1);
                for(int i=0;i<size;i++)
                {
                   // System.out.println(i);
                    in.set(i, 0, positions.v[i]);

                }
                coefficients= new double[size];
                vector out;
                for(int i=0;i<size;i++)
                {
                    for(int j=0; j<size;j++)
                    {
                        m.set(i,j,Math.pow(times.v[i],j));
                    }
                }

           //     for(int i=0;i<size;i++)
           //     {
            //        for(int j=0; j<size;j++)
            //        {
                      //  System.out.print(m.get(i,j)+" ");
            //        }
            //        System.out.println(" ");
           //     }

                out=new vector( m.solve(in));
             //   System.out.println(out);
                for(int i=0;i<out.dimension;i++)
                {
                    coefficients[i] = out.v[i];
                }
                //set degree to length of coeeficients
                degree=out.dimension;
            }
        }

	double evaluate(double t)
	{
		double eval=0;
		for(int i=0; i<degree;i++)
		{
			eval=eval+coefficients[i]*Math.pow( t ,(double) i );
		
		}
                return eval;
	}
}