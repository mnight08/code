/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pathmaking;

/**
 *
 * @author afmartinez4
 */
import javax.swing.ImageIcon;
import java.awt.*;
import java.awt.event.*;
import java.applet.*;
import java.net.*;
import java.io.OutputStream;

public class epanel extends Applet implements MouseListener {

    path p;
    double time;
    vector times;
    vector xpositions;
    vector ypositions;
    int count;
    polynomial x, y;
    double timelength;
    updaterthread ut;
    private static final String Boundary = "--7d021a37605f0";

    private void submitobject() throws Exception {
        URL url = new URL("http://red1.cs.panam.edu/3342/afmartinez4/pots/storageservice.php");
        System.out.println(1);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

         System.out.println(2);
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");
         System.out.println(3);
        connection.setUseCaches(false);
        connection.setChunkedStreamingMode(1024);
        connection.setFollowRedirects(true);

        connection.addRequestProperty("object","object");
        connection.connect();

         System.out.println(5);
    }

    public void mousePressed(MouseEvent e) {
    }
    public void mouseReleased(MouseEvent e) {
    }
    public void mouseEntered(MouseEvent e) {
    }
    public void mouseExited(MouseEvent e) {
    }
    public void mouseClicked(MouseEvent e) {
        xpositions.v[count] = e.getPoint().x;
        ypositions.v[count] = e.getPoint().y;
        count++;
        for (int i = 0; i < count; i++) {
            times.v[i] = ((double) i) * (timelength / count);
        }
        if (count > 1) {
            x = new polynomial(times, xpositions, count);
            y = new polynomial(times, ypositions, count);
            p = new path(x, y);
       ///*****************************************************************************//////////
            if(count>6)
        {
            System.out.println("outputting object");
            try{
                submitobject();

            }catch(Exception ex){System.out.println("failed to output object");}
            
        }
        }
    }

    public void init() {
    }

    public void stop() {
        try {
            submitobject();
        } catch (Exception e) {
        }
    }

    public epanel() {
        times = new vector(100);
        xpositions = new vector(100);
        ypositions = new vector(100);
        ut = new updaterthread(this);
        String loadobject = "";
        try{
        if (this.getParameter("loadobject") !=null) {
            loadobject = this.getParameter("loadobject");



        if (loadobject.compareTo("nothing123321") != 0) {
            String deliminator = "[#]+";
            System.out.println(loadobject+"##########################################################################");
            String points[] = loadobject.split(deliminator);
            deliminator="[,]+";
            String xpoints[] = points[0].split(deliminator);
            String ypoints[] = points[1].split(deliminator);
            for (int i = 0; i < xpoints.length; i++) {
                xpositions.v[i] = Double.parseDouble(xpoints[i]);
                ypositions.v[i] = Double.parseDouble(ypoints[i]);
            }
            count = xpoints.length;
            }
        } else {
            count = 0;
        } 
        }catch(Exception e){}
        timelength = 12;




      time = 0;
        addMouseListener(this);
       ut.start();

    }

    public void draw(Graphics g) {
        //g.setColor(Color.black);
        g.drawImage((new ImageIcon(this.getClass().getResource("maze.jpg"))).getImage(), 0, 0, 800, 600, this);

        //    System.out.println("this is me");

       // g.setColor(Color.blue);

        if (count > 0) {
            g.drawImage((new ImageIcon(this.getClass().getResource("sinistar.gif"))).getImage(), p.getposition(time).x, p.getposition(time).y, 30, 30, this);
            if (time > timelength) {
                time = 0;
            } else {
                time = time + 1.0 / 30;
            }
        }
    }

    public void paint(Graphics g) {
        draw(g);

    }
}
