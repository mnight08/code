package pathmaking;
import Jama.*;
public class vector {
    double[] v;
    int dimension;
    vector(int n)
    {
        dimension=n;
        v=new double[dimension];


    }
    vector(Matrix m)
    {
        dimension=m.getRowDimension();
        v=new double[dimension];
        for(int i=0;i<dimension;i++)
        {
            v[i]=m.get(i, 0);
        }


    }
    public Matrix toMatrix()
    {
     //  System.out.println(dimension);
        Matrix m= new Matrix(dimension,1);
        for(int i=0;i<dimension;i++)
        {
            m.set(i, 0, v[i]);
        }
        return m;
    }
    public String toString()
    {
        String s="";
        for( int i=0; i<v.length;i++)
        {
            s = s +" "+ v[i];
            
        }
        return s;
    }
}
