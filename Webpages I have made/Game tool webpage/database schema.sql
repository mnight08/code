SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `afmprojdb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE `afmprojdb` ;

-- -----------------------------------------------------
-- Table `afmprojdb`.`ratings`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `afmprojdb`.`ratings` (
  `rating_id` INT NOT NULL ,
  `rating` INT NULL ,
  `count` INT NULL ,
  PRIMARY KEY (`rating_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `afmprojdb`.`groups`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `afmprojdb`.`groups` (
  `group_id` INT NOT NULL DEFAULT 1 ,
  `name` VARCHAR(45) NOT NULL DEFAULT 'guest' ,
  `rating_id` INT NOT NULL ,
  `creation` VARCHAR(10) NULL ,
  PRIMARY KEY (`group_id`, `rating_id`) ,
  INDEX `fk_groups_ratings1` (`rating_id` ASC) ,
  CONSTRAINT `fk_groups_ratings1`
    FOREIGN KEY (`rating_id` )
    REFERENCES `afmprojdb`.`ratings` (`rating_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `afmprojdb`.`game_engines`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `afmprojdb`.`game_engines` (
  `game_engine_id` INT NOT NULL ,
  `rating_id` INT NOT NULL ,
  `name` VARCHAR(45) NULL ,
  PRIMARY KEY (`game_engine_id`, `rating_id`) ,
  INDEX `fk_game_engines_ratings1` (`rating_id` ASC) ,
  CONSTRAINT `fk_game_engines_ratings1`
    FOREIGN KEY (`rating_id` )
    REFERENCES `afmprojdb`.`ratings` (`rating_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `afmprojdb`.`engine_components`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `afmprojdb`.`engine_components` (
  `engine_component_id` INT NOT NULL ,
  `game_engine_id` INT NOT NULL ,
  `name` VARCHAR(45) NULL ,
  PRIMARY KEY (`engine_component_id`) ,
  INDEX `fk_engine_components_game_engines1` (`game_engine_id` ASC) ,
  CONSTRAINT `fk_engine_components_game_engines1`
    FOREIGN KEY (`game_engine_id` )
    REFERENCES `afmprojdb`.`game_engines` (`game_engine_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `afmprojdb`.`tools`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `afmprojdb`.`tools` (
  `tool_id` INT NOT NULL ,
  `rating_id` INT NOT NULL ,
  `engine_component_id` INT NOT NULL ,
  `name` VARCHAR(45) NULL ,
  PRIMARY KEY (`tool_id`) ,
  INDEX `fk_tools_ratings1` (`rating_id` ASC) ,
  INDEX `fk_tools_engine_components1` (`engine_component_id` ASC) ,
  CONSTRAINT `fk_tools_ratings1`
    FOREIGN KEY (`rating_id` )
    REFERENCES `afmprojdb`.`ratings` (`rating_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tools_engine_components1`
    FOREIGN KEY (`engine_component_id` )
    REFERENCES `afmprojdb`.`engine_components` (`engine_component_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `afmprojdb`.`objects`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `afmprojdb`.`objects` (
  `object_id` INT NOT NULL ,
  `tool_id` INT NOT NULL ,
  `creation` VARCHAR(10) NULL ,
  `creator_group_id` INT NOT NULL ,
  `data` MEDIUMTEXT NULL ,
  `name` VARCHAR(45) NULL ,
  PRIMARY KEY (`object_id`) ,
  INDEX `fk_objects_tools1` (`tool_id` ASC) ,
  INDEX `fk_objects_groups1` (`creator_group_id` ASC) ,
  CONSTRAINT `fk_objects_tools1`
    FOREIGN KEY (`tool_id` )
    REFERENCES `afmprojdb`.`tools` (`tool_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_objects_groups1`
    FOREIGN KEY (`creator_group_id` )
    REFERENCES `afmprojdb`.`groups` (`group_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `afmprojdb`.`users`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `afmprojdb`.`users` (
  `user_id` INT NOT NULL AUTO_INCREMENT ,
  `username` VARCHAR(45) NULL ,
  `email` VARCHAR(45) NULL ,
  `created` VARCHAR(10) NULL ,
  `number_of_post` INT NULL ,
  `rating_id` INT NOT NULL ,
  `user_key` BLOB NULL ,
  `name` VARCHAR(45) NULL ,
  PRIMARY KEY (`user_id`, `rating_id`) ,
  INDEX `fk_users_ratings1` (`rating_id` ASC) ,
  CONSTRAINT `fk_users_ratings1`
    FOREIGN KEY (`rating_id` )
    REFERENCES `afmprojdb`.`ratings` (`rating_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `afmprojdb`.`comments`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `afmprojdb`.`comments` (
  `comment_id` INT NOT NULL AUTO_INCREMENT ,
  `user_id` INT NOT NULL ,
  `data` TEXT NULL ,
  `rating_id` INT NOT NULL ,
  PRIMARY KEY (`comment_id`) ,
  INDEX `fk_comments_users1` (`user_id` ASC) ,
  INDEX `fk_comments_ratings1` (`rating_id` ASC) ,
  CONSTRAINT `fk_comments_users1`
    FOREIGN KEY (`user_id` )
    REFERENCES `afmprojdb`.`users` (`user_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_comments_ratings1`
    FOREIGN KEY (`rating_id` )
    REFERENCES `afmprojdb`.`ratings` (`rating_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `afmprojdb`.`groups_has_users`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `afmprojdb`.`groups_has_users` (
  `group_id` INT NOT NULL ,
  `user_id` INT NOT NULL ,
  PRIMARY KEY (`group_id`, `user_id`) ,
  INDEX `fk_groups_has_users_users1` (`user_id` ASC) ,
  INDEX `fk_groups_has_users_groups1` (`group_id` ASC) ,
  CONSTRAINT `fk_groups_has_users_groups1`
    FOREIGN KEY (`group_id` )
    REFERENCES `afmprojdb`.`groups` (`group_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_groups_has_users_users1`
    FOREIGN KEY (`user_id` )
    REFERENCES `afmprojdb`.`users` (`user_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `afmprojdb`.`groups_has_objects`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `afmprojdb`.`groups_has_objects` (
  `group_id` INT NOT NULL ,
  `object_id` INT NOT NULL ,
  `priv` INT NULL ,
  PRIMARY KEY (`group_id`, `object_id`) ,
  INDEX `fk_groups_has_objects_objects1` (`object_id` ASC) ,
  INDEX `fk_groups_has_objects_groups1` (`group_id` ASC) ,
  CONSTRAINT `fk_groups_has_objects_groups1`
    FOREIGN KEY (`group_id` )
    REFERENCES `afmprojdb`.`groups` (`group_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_groups_has_objects_objects1`
    FOREIGN KEY (`object_id` )
    REFERENCES `afmprojdb`.`objects` (`object_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `afmprojdb`.`tags`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `afmprojdb`.`tags` (
  `tag_id` INT NOT NULL ,
  `data` TINYTEXT NULL ,
  PRIMARY KEY (`tag_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `afmprojdb`.`objects_has_tags`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `afmprojdb`.`objects_has_tags` (
  `object_id` INT NOT NULL ,
  `tag_id` INT NOT NULL ,
  PRIMARY KEY (`object_id`, `tag_id`) ,
  INDEX `fk_objects_has_tags_tags1` (`tag_id` ASC) ,
  INDEX `fk_objects_has_tags_objects1` (`object_id` ASC) ,
  CONSTRAINT `fk_objects_has_tags_objects1`
    FOREIGN KEY (`object_id` )
    REFERENCES `afmprojdb`.`objects` (`object_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_objects_has_tags_tags1`
    FOREIGN KEY (`tag_id` )
    REFERENCES `afmprojdb`.`tags` (`tag_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `afmprojdb`.`tools_has_tags`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `afmprojdb`.`tools_has_tags` (
  `tool_id` INT NOT NULL ,
  `tag_id` INT NOT NULL ,
  PRIMARY KEY (`tool_id`, `tag_id`) ,
  INDEX `fk_tools_has_tags_tags1` (`tag_id` ASC) ,
  INDEX `fk_tools_has_tags_tools1` (`tool_id` ASC) ,
  CONSTRAINT `fk_tools_has_tags_tools1`
    FOREIGN KEY (`tool_id` )
    REFERENCES `afmprojdb`.`tools` (`tool_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tools_has_tags_tags1`
    FOREIGN KEY (`tag_id` )
    REFERENCES `afmprojdb`.`tags` (`tag_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
