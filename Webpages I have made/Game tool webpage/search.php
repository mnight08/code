<?
	require_once("functiondefinitions.php");
	error_reporting(0);
	//redirect to navigation page. 
	if(invalidloginpageuse())
	{
		setintrouble();		
	}


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
		<script type="text/javascript" src="search.js"></script>
		<script>	
		$(document).ready(function(){
			$("#logout").mousedown(function(){$(this).attr("src","logoutbuttonsec.png")});
			$("#logout").mouseup(function(){$(this).attr("src","logoutbuttonprim.png")});
			
			$("#search").mousedown(function(){$(this).attr("src","searchbuttonsec.png")});
			$("#search").mouseup(function(){$(this).attr("src","searchbuttonprim.png")});
		
			$("html").css('background-image','url("matrix-animated-image.gif")');
			$("html").css('color','#00FF00');
			
			startsearch();
			
			$("#searchbox").keydown(updateresults);
			$("#accordion").click(updateaccordion);

		})
		</script>
	</head>
	
	<body align="center">
		<h1>
			Welcome to the online game development toolkit
			<h2>
				Please find what you are looking for by traversing the tree.  
				<h3>
					The tree will be updated as you type to narrow down results.
				</h3>
			</h2>
		</h1>
		<p>
		<form method="post" action="navigation.php">
			Search <input type="text" name="query" id="searchbox"><br>
			<div id="searchtree">

			</div>
	

			<input type="image" src="logoutbuttonprim.png" name="logout" id="logout" value="logout"><br>
			<input type="submit" name="takemetodefaulttool" value="just give me a tool">
		</form>
		</p>
	</body>
</html>

