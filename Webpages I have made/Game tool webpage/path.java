package pathmaking;


public class path
{
	
	polynomial x;
	polynomial y;

        path(polynomial xt, polynomial yt)
        {
            x=xt;y=yt;

        }

	point getposition(double t)
        {
            return new point( (int)(x.evaluate(t)), (int)(y.evaluate(t)));
        }
        public String toString()
        {
            String out="";
            for(int i=0;i<x.degree-1;i++)
            {
                out=out+x.coefficients[i]+",";

            }
            out=out+x.coefficients[x.degree-1];
            out=out+"#";
            for(int i=0;i<y.degree-1;i++)
            {
                 out=out+y.coefficients[i]+",";

            }
            out=out+y.coefficients[y.degree-1];
            return out;

        }
}