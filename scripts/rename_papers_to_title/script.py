#vim: set ts=4 expandtab shiftwidth=4 softtabstop=4
import shutil, os, sys
import PyPDF2
def isPdf(filename):
    return filename.endswith(".pdf")
def xstr(s):
    if s is None:
        return ''
    return str(s)

#safey on means do not do renames, simply print out to log file or terminal
def pdfNamesToTitles(directory,flag="r",safety="on", mode="w"):
    logfilename="log.txt"
    with open(logfilename,mode) as log:
        #log.write("Backing up directory: " + directory+"\n")
        #backup directory
        
        #shutil.copytree(directory,"Backup/pdfNamesToTitles/")   
        #rename all files in the directory, then recursively do this to subfolders
        for foldername, subfolders, filenames in os.walk(directory):
            log.write("Current folder is:" +foldername+"\n")
            for filename in filenames:
                log.write("Checking file extension for file:" + filename+"\n")
                #Check if the file isa  pdf and if so rename it to its title.
                if(isPdf(filename)):
                    log.write("File has correct extension"+"\n")

#		    #open pdf
                    try:
                        reader= PyPDF2.PdfFileReader(foldername+filename)
                        log.write("PDF reader created"+"\n")


		        #get pdf document info
                        docinfo=reader.getDocumentInfo()
                    
                        log.write("Title:" +xstr(docinfo.title)+"\n")
                        log.write("Author :"+xstr(docinfo.author)+"\n")
                        log.write("Creator:"+xstr(docinfo.creator)+"\n")
                        log.write("Producer:"+xstr(docinfo.producer)+"\n")
                        log.write("Subject:"+xstr(docinfo.subject)+"\n")



                        #get the title
                        if docinfo != None:
                            title=docinfo.title
                            if title!=None:

                                if(safety=="on"):
                                    #rename files in directory to document title with appropriate extension
                                    log.write("File is to be named "+xstr(title)+".pdf"+"\nTurn safety off to complete action"+"\n\n")
                                elif(safety=="off"):
		                    #rename files in directory to document title with appropriate extension
                                    log.write("File "+filename+" renamed "+xstr(title)+".pdf"+"\n\n")
                                    shutil.move(filename,xstr(title)+".pdf")
                            if(flag=="r"):
                                #recursively rename pdfss.
                                for subfolder in subfolders:
                                    pdfNamesToTitles(subfolder, flag, safety,mode="a")
                        else:
                            log.write(filename + " is does not have .pdf extension"+"\n\n")
                    except:
                        log.write("Unexpected error: "+ xstr(sys.exc_info()[0])+"\n")
                        
pdfNamesToTitles("C:/cygwin64/home/ez pawn/Documentation, Reading, and learning/",flag="r",safety="on",mode="w")
