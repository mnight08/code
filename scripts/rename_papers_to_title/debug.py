#vim: set ts=8 expandtab shiftwidth=4 softtabstop=4
import shutil, os
def isPdf(filename):
    pass


#safey on means do not do renames, simply print out to log file or terminal
def pdfNamesToTitles(directory,flag="r",safety="on"):
    logfilename="log.txt"

    with open(logfilename, 'w') as log:
	#log.write("Backing up directory")
        #backup directory
        shutil.copy(directory,"Backup/pdfNamesToTitles/"    
        #rename all files in the directory, then recursively do this to subfolders
	for folderName, subfolders, filenames in os.walk(directory):
	    for filename in filenames:
	        #Check if the file isa  pdf and if so rename it to its title.
		if(isPdf(filename)):
		    #open pdf
		    reader= PdfFileReader(filename)
		    #get pdf document info
		    docinfo=reader.getDocumentInfo()
		    #get the title
		    if docinfo!= NONE:
		        title=docinfo.title
                        if(safety=="on"):
                            #rename files in directory to document title with appropriate extension
		            log.write("File is to be named "+title+".pdf"+"Turn safety off to complete action")
		        else:
		            #rename files in directory to document title with appropriate extension
		            log.write("File "+filename+" renamed "+title+".pdf")
	                    shutil.move(filename,title+".pdf")
	                    if(flag=="r"):
			    #recursively rename pdfss.
			    for subfolder in subfolders:
		                pdfNamesTotitles(subfolder)
    
