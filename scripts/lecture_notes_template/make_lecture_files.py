from pathlib import Path

Num_Lectures=28

for i in range(1,Num_Lectures+1):
	file_name="Lecture_"+str(i)+".tex"
	Path(file_name).touch()
	print("\\input{lectures\\"+file_name+"}")


