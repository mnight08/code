# -*- coding: utf-8 -*-
"""
Created on Sun Jan 28 00:08:58 2018

@author: vpx365
"""
import itertools
import pandas as pd


path="C:/Users/vpx365/Desktop/PersonalData/"
filename="2017CapitaloneBankTransactions.txt"



transactions=[]

with open(path+filename) as f:
    colums=f.readline()
    for line1,line2 in itertools.zip_longest(*[f]*2):
#        print(line1,line2)
        #if("deposit" in line2):   
        transactions.append((line1,line2))
                
columns=(colums+" DETAILS").split(" ")

#Use the following key words to categorize the transaction.
#multiple key words can be used.
key_words={}
key_words['deposit']=["deposit"]
key_words['withdrawal']=["withdrawal"]
key_words['purchase']=["purchase"]
key_words['transfer']=["transfer"]
    
categories=['deposit','withdrawal','purchase','transfer']

#Each transaction is a tuple, with information in first element
#and detailed description in second.
for transaction in transactions:
    #first 5 characters are date 
    date=transaction[0][:5]
    details=deposit[1]
    #description is next
    description=transaction[0][5:].split("$")[0]
    

    
    category=categorizeTransaction(details)

    amount=float(deposit[0][5:].split("$")[1].rstrip().replace(',',''))
    balance=float(deposit[0][5:].split("$")[2].rstrip().replace(',',''))

    #print([date,description,category,amount,balance,details])
    data.append([date,description,category,amount,balance,details])


df=pd.DataFrame(data,columns=columns)


#returns true if the details contain one of the keywords
def flagTransaction(details,key_words):
    if any(x in details for x in key_words):
        return True
    else:
        return False
    

def categorizeTransaction(details):
    category="none"
    for category in categories:
        if flagTransaction(details,key_words[category]):
            return category        
    return category


def getTransactionByCategory(df, category):
    return df[df['CATEGORY']==category]
    

def getDeposits(df):
    return getTransactionByCategory(df,'deposit')
    

def getTransfers(df):
    return getTransactionByCategory(df,'transfer')
    

def getWithdrawals(df):
    return getTransactionByCategory(df,'withdrawal')
    
def getPurchases(df):
    return getTransactionByCategory(df,'purchase')
    
    
def getUncategorized(df):
    return getTransactionByCategory(df,'none')


def searchSubstringInDetails(df,string):
    return df[df['DETAILS'].str.contains(string)]
    

    
    