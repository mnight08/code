# -*- coding: utf-8 -*-
"""
Created on Sat Jan 27 23:25:31 2018

@author: vpx365
"""

This is a collection of classes for interacting with my bank transaction data.  
The format of the data expected is 
DATE DESCRIPTION CATEGORY AMOUNT BALANCE
JAN15Capital One $400.00$80.72
Transaction Type:	Electronic transferAppears on statement as: ACH Withdrawal CAPITAL ONE ONLINE PMT ...2149MARTINEZALEJ ...0511

Every 2 lines after the columns is a transaction.  Outgoing money is denoted by
the second line starting with "Transaction Type:"
