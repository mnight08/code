function value=signal(w,t)
    value=exp(1i*w*t);
end