function value=scattered_field(y,f,t,image_width,image_height,image_resolution,imaging_grid,reflectivity )

c_0=3*10^8;
i=1;
value=0;
while i<=image_width/image_resolution
    j=1;
    while j<=image_height/image_resolution
        value=value-(2*pi*f)^2*reflectivity(i,j)*exp(2*pi*1i*(t-2*norm(y-imaging_grid(:,i,j))/c_0))/((4*pi)^2*norm(y-imaging_grid(:,i,j))^2);
        j=j+1;
    end
    i=i+1;
end

end
