%create the vertices we want to work with
%x positions
x=[1,2];

%y positions
y=[1,2];

%z positions for the pairs [x(i),y(j)]
z(1,1)=5;
z(1,2)=1;
z(2,1)=-5;
z(2,2)=10;
%make a  wireframe mesh that goes through the values z(1,1)...Z(2,2)
mesh(x,y,z);


%make surface going over those points.
surf(x,y,z);