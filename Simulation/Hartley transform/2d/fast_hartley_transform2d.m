function H=fast_hartley_trasnform2d(f,x,y,u,v)
%in this function I will 
%takes a 2d function f sampled at points x,y and computes its 2d hartley transform
%at the poitns u,v
M_1=size(f);
M_1=M_1(1);
M_2=size(f);
M_2=M_2(2);

N_1=size(u);
N_1=max(N_1);

N_2=size(v);
N_2=max(N_2);
H=zeros(N_1,N_2);

i_1=1;
while i_1<=N_1
    i_2=1;
    while i_2<=N_2 
        j_1=1;
        while  j_1<=M_1
            j_2=1;
            while j_2<=M_2
                H(i_1,i_2)=H(i_1,i_2)+cas(2*pi*(u(i_1)*x(j_1)+v(i_2)*y(j_2)))*f(j_1,j_2);
                j_2=j_2+1;
            end
                j_1=j_1+1;
        end
        i_2=i_2+1;
    end
    i_1=i_1+1;
end



end