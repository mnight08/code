function value=testfunc(x,y)
if x==0 ||y==0
    value=1;
end
value=sinc((x*x+y*y)^.5);
end