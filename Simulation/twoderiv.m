% Author: Guillermo Garza, ggarza23@gmail.com
% Created: Thu 14 Oct 2010 04:56:41 PM CDT
% Modified: Wed 03 Nov 2010 02:47:36 PM CDT

%----------------------------------------------------------------------------
% This script is a test to find the second derivative of a function.
%----------------------------------------------------------------------------

clear all; close all; clc; 

samplesize = 0.1;

p = @(t) t.^5;
array = -4:samplesize:4;

zerothderiv = p(array);
figure;
plot(array,zerothderiv);

firstderiv = diff(zerothderiv)/(samplesize);
figure;
plot(firstderiv);

secondderiv = diff(firstderiv)/samplesize;
figure;
plot(secondderiv);

alpha = 30;
F0 = 30e6;
Tp = 1e-3;
p = @(t) cos(2*pi*alpha*t.^2+2*pi*F0*t).*(t<Tp).*(t>0);

samplesize = 1e-6;
array = 0:samplesize:Tp;

zerothderiv = p(array);
figure;
plot(array,zerothderiv);

firstderiv = diff(zerothderiv)/(samplesize);
figure;
plot(firstderiv);

secondderiv = diff(firstderiv)/samplesize;
figure;
plot(secondderiv);
