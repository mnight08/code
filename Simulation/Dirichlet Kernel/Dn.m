function D=Dn(t,N)
M=max(size(t));
D=zeros(M,1);
m=1;
    while m<=M
        if t(m)==0
            D(m)=2*N+1;
        else
            D(m)=sin((N+.5)*t(m))./((2*pi)*sin(.5*t(m)));
        end
        m=m+1;
    end
end