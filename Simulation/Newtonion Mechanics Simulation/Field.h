#pragma once
#include "Set.h"

template <class type>
class Field
{
public:
	Set<type> F;
	virtual type operator +(type a);
	virtual type operator *(type a);

	virtual Field(void);
	virtual ~Field(void);
};
