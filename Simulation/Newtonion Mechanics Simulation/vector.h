#pragma once
#include <math.h>
#include <iostream>
using namespace std;
template <class field> class vector;
template <class field>
ostream & operator << (ostream &os,vector<field> a);

template <class field>
class vector
{
public:
	int dimensions;
	field* basis;
	vector<field>();
	vector<field>(int dim);
	vector<field>(const vector<field>&);
	vector<field>(int dim,field a);
	vector<field> operator -(vector);
	vector<field> operator +(vector);
	field operator *(vector);
	vector<field> operator *(field);
	vector<field> operator /(field);
	field& operator [](int);
	bool operator==(vector<field>);
	vector<field> operator =(vector<field>);
	void resize(int n);
//	vector<field> operator -();
	friend ostream& operator << <>(ostream &,vector<field>);

	//if i define these functions, then i think i need to make wrapper classes for any field used
	//field norm();
	//field innerproduct();
	//vector<field> crossproduct(vector);
	~vector(void);
};

template <class field>
bool vector<field>::operator==(vector<field> a)
{
	if(dimensions!=a.dimensions)
		return false;
	else if((*this)[0]!=a[0]||(*this)[1]!=a[1]||(*this)[2]!=a[2])
		return false;
	else
		return true;
		
}

template <class field>
void vector<field>::resize(int n)
{
	if(n<=0)return;
	else 
	{
		basis= new field[n];
		dimensions=n;
	}
}
template <class field>
vector<field>::vector()
{
	dimensions=0;
	basis=NULL;
}

/*this template will only really work for classes that are fields, that is, for this function we need the additive inverse of the multiplicitive identity
//or at the very least we need to know what the additive identity is.
template <class field>
vector<field> vector<field>::operator-()
{
	return 
}
*/

template <class field>
vector<field>::vector(const vector<field> &a)
{
	basis= new field[a.dimensions];
	dimensions=a.dimensions;
	for(int i=0;i<dimensions;i++)
	{
		basis[i]=a.basis[i];
	}
}

template <class field>
ostream &operator <<(ostream &os,vector<field> a)
{

	for(int i=0;i<a.dimensions;i++)
	{
		os<<a.basis[i]<<" ";
	}
	os<<endl;
	return os;
}

/*
template <class field>
vector<field> vector<field>::crossproduct(vector a)
{//this is still wrong
	//return norm(a)*norm(b)*vector(field.identity);
}
*/

template <class field>
vector<field> vector<field>::operator=(vector<field> a)
{

	if(this->dimensions!=a.dimensions)
		return *this;
		else
		{
			for(int i=0;i<dimensions;i++)
				(*this)[i]=a[i];
		}
	return *this;
}
template <class field>
vector<field>::vector(int dim, field a)
{
	
	dimensions=dim;
	//this can probably be made much more general, to a template, that takes in a particular field
	//and makes an array of size dim of that field
	basis= new field[dimensions];
	for(int i=0;i<dim;i++)
	{
		basis[i]=a;
	}

}


template <class field>
vector<field>::vector(int dim)
{
	dimensions=dim;
	//this can probably be made much more general, to a template, that takes in a particular field
	//and makes an array of size dim of that field
	basis= new field[dimensions];
}

//vector subtraction
template <class field>
vector<field> vector<field>::operator -(vector a)
{
	if(this->dimensions!=a.dimensions)
		return *this;
			else
		{
			vector<field> temp(dimensions);
			for(int i=0;i<dimensions;i++)
			{
				temp[i]=(*this)[i]-a[i];
			}
			return temp;
		}
}

//vector addition
template <class field>
vector<field> vector<field>::operator +(vector a)
{
		if(this->dimensions!=a.dimensions)
		return *this;
		else
		{
			vector<field> temp(dimensions);
			for(int i=0;i<dimensions;i++)
			{
				temp[i]=(*this)[i]+a[i];
			}
			return temp;
		}
}
template <class field>
field& vector<field>::operator[](int i)
{
	if(!(i<dimensions)) 
		return (*this).basis[0];
	else
	{
		return ((*this).basis[i]);
	}
}

//scalar multiplication
template <class field>
vector<field> vector<field>::operator *(field a)
{
	vector<field> temp(dimensions);
		if(this->dimensions==0)
			return *this;
		else
		{

			for(int i=0;i<dimensions;i++)
				temp[i]=a*(*this)[i];
		}
		return temp;
}

//dot product
template <class field>
field vector<field>::operator *(vector<field> a)
{
	field sum;
	
	if(dimensions>0&&dimensions==a.dimensions)
		sum=(*this)[0]*a[0];
	for(int i=1;i<dimensions;i++)
		sum+=this->basis[i]*a.basis[i];

	return sum;

}

template <class field>
vector<field>::~vector(void)
{
	delete []basis;
}
