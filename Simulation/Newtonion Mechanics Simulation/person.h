#pragma once
#include "vector.h"
#include <list>
#include <string>
class person
{
public:
	string type;
	vector<double> pos;
	vector<double> vel;
	vector<double> acc;
	double mass;
	vector<double> range;
	person(vector<double> startposition, vector<double> startvelocity, vector<double> startacceleration);
	~person(void);
	vector<double> displacement(double time,list<person> &);
	vector<double> velocity( double time,list<person> &);
	vector<double> acceleration(list<person >&forces);
//	vector<double> inertia(person a);
	void update(double,list<person > &forces);
	bool operator ==(person a);
	bool isinrange(person a);
};
/*
vector<double> person::inertia(person a)
{
	vector<double> temp(3);
	temp[0]=temp[1]=temp[2]=0;
	if(type!="person"||a.type!="person")
		return temp;
	else 
	{
		a.acc*a.mass
	}

}
*/
bool person::operator==(person a)
{
	if(pos==a.pos&&vel==a.vel&&acc==a.acc&&mass==a.mass&&range==a.range&&type==a.type)
		return true;
	else return false;
}
bool person::isinrange(person a)
{
	vector<double> closest(3);
	closest=(a.pos-pos);
	//if((closest[0]-pos[0])*(closest[0]-pos[0])+(closest[1]-pos[1])*(closest[1]-pos[1])+(closest[2]-pos[2])*(closest[2]-pos[2])<=range*range/*&&(closest[0]-a.pos[0])*(closest[0]-a.pos[0])+(closest[1]-a.pos[1])*(closest[1]-a.pos[1])+(closest[2]-a.pos[2])*(closest[2]-a.pos[2])<=a.range*a.range*/)
	//if(	closest*closest<=(a.range+range)*(a.range+range))
	if()
	return true;
	else return false;
}

person::~person()
{

}
void person::update(double dt,list<person> &forces)
{
	forces.remove(*this);	
	acc=acceleration(forces);	
	vel=velocity(dt,forces);
	pos=pos+displacement(dt,forces);


	forces.push_back(*this);
	
}
person::person(vector<double> startposition, vector<double> startvelocity, vector<double> startacceleration)
{
	type= "person";
	pos.resize(3);
	vel.resize(3);
	acc.resize(3);
	pos=startposition;vel=startvelocity;acc=startacceleration;
	range.resize(3);
	range[0]=range[1]=range[2]=0;
}
vector<double> person::displacement(double time, list<person> &forces)
{
	return  velocity(time,forces)*time ;
}

vector<double> person::velocity( double time,list<person> & forces)
{
	return vel+ acceleration(forces)*time;
}

vector<double> person::acceleration(list<person > &forces)
{
	list<person> reactancelist;
	vector<double> force(3);
	force[0]=force[1]=force[2]=0;
	//sum all the forces
	//should probably also compensate for gravity between two objects
	for(list<person >::iterator it = forces.begin();it!=forces.end();it++)
	{
		if((*it).isinrange(*this))
		{
			
			force[0]+=(*it).acc[0]*(*it).mass;//+(*it).inertia(*this)[0];
			force[1]+=(*it).acc[1]*(*it).mass;//+(*it).inertia(*this)[1];
			force[2]+=(*it).acc[2]*(*it).mass;//+(*it).inertia(*this)[2];
			acc=acc*-1;
			type="reactance";
			reactancelist.push_back(*this);
			type="person";
			acc=acc*-1;
		}
	}
	for(list<person >::iterator it = reactancelist.begin();it!=reactancelist.end();it++)
	{
		forces.push_back(*it);
	}
	return force*(1/mass);
}

