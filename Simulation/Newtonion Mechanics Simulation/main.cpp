//#include "vector.h"
#include "person.h"
#include <iostream>
#include <math.h>
#include <windows.h>								// Header File For Windows
#include <gl\gl.h>								// Header File For The OpenGL32 Library
#include <gl\glu.h>								// Header File For The GLu32 Library
#include <gl\glut.h>
#include <time.h>
#include <stdio.h>
#include <list>

void drawRect(vector<double>,double,double);
void display(void);
void idle(void);
void init(void);
void reshape(int , int );
void keyboard(unsigned char, int, int);
void mouse(int, int, int, int);
void motion(int x, int y);
void trimforces(list<person> &f);



vector<double> pos(3);
vector<double> vel(3);
vector<double> acc(3);
person a(pos,vel,acc);
person b(pos,vel,acc);
person c(pos,vel,acc);
//if we let mass=1, then force= acceleration, with range r
list<person> forces;
list<person> removelist;
clock_t startt=clock();
clock_t endt=clock();

void drawRect(vector<double> start,double height, double width)
{
	glBegin(GL_POLYGON);
		glVertex3f(start[0],start[1],start[2]);
		glVertex3f(start[0]+width,start[1],start[2]);
		glVertex3f(start[0]+width,start[1]-height,start[2]);
		glVertex3f(start[0],start[1]-height,start[2]);
	glEnd();
}

void trimforces(list<person> &f)
{
	removelist.clear();
	for(list<person>::iterator it=f.begin();it!=f.end();it++)
	{
		if((*it).type=="reactance")
			removelist.push_back((*it));
	}
	for(list<person>::iterator it= removelist.begin();it!=removelist.end();it++)
	{
		forces.remove(*it);
	}
}
void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(10.0,0,0);
	drawRect(a.pos,10,10);
	glColor3f(0,20.0,0);
	drawRect(b.pos,10,10);
	glColor3f(0,0,30.0);
	drawRect(c.pos,10,10);
	glFlush();

}
void idle(void)
{
	endt=clock();

	int q=0;
	cout<<"system is idle, and redrawing at "<<a.pos<<endl;
	trimforces(forces);
	cout<<"change in time is"<<endt-startt<<endl;
	a.update((double)((endt-startt)/CLOCKS_PER_SEC),forces);
	b.update((double)((endt-startt)/CLOCKS_PER_SEC),forces);
	c.update((double)((endt-startt)/CLOCKS_PER_SEC),forces);
	glutPostRedisplay();
	startt=endt;
	//cin>>q;
//	time+=1;
}

void init(void)
{
	glClearColor(0.0,0.0,0.0,0.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-1000.0,1000.0,-1000.0,1000.0,-1000.0,1000.0);
		
	a.pos[0]=-100;
	a.pos[1]=100;
	a.pos[2]=0;
	
	a.vel[0]=0;
	a.vel[1]=0;
	a.vel[2]=0;

	a.acc[0]=0;
	a.acc[1]=0;
	a.acc[2]=0;

	a.mass=10;
	a.range=5;

	b.pos[0]=-100;
	b.pos[1]=-100;
	b.pos[2]=0;
	
	b.vel[0]=0;
	b.vel[1]=0;
	b.vel[2]=0;

	b.acc[0]=0;
	b.acc[1]=0;
	b.acc[2]=0;
	b.mass=2000;
	b.range=5;

	c.pos[0]=0;
	c.pos[1]=0;
	c.pos[2]=0;
	
	c.vel[0]=0;
	c.vel[1]=0;
	c.vel[2]=0;

	c.acc[0]=0;
	c.acc[1]=0;
	c.acc[2]=0;
	c.mass=100;
	c.range=5;
	vector<double> force(3);
	vector<double> p(3);
	vector<double> r(3);

	
	force[0]=0;force[1]=-9.8;force[2]=0;
	
	pos[0]=0;pos[1]=100;pos[2]=0;	
	r[0]=0;r[1]=0;r[2]=0;
	person f(pos,r,force);
	f.type="force";
	f.mass=1;
	f.range=100;
	forces.push_back(f);
	
	force[0]=0;force[1]=9.8;force[2]=0;
	pos[0]=0;pos[1]=-100;pos[2]=0;
	
	f.pos=pos;
	f.acc=force;
	
	forces.push_back(f);

	force[0]=9.8;force[1]=0;force[2]=0;
	pos[0]=-100;pos[1]=0;pos[2]=0;
	f.pos=pos;
	f.acc=force;
	
	forces.push_back(f);

	
	force[0]=-9.8;force[1]=0;force[2]=0;
	pos[0]=100;pos[1]=0;pos[2]=0;
	f.pos=pos;
	f.acc=force;
	
	forces.push_back(f);
	forces.push_back(a);
	forces.push_back(b);
	forces.push_back(c);
	/*
	force=a.acc*a.mass;
	pos=a.pos;
	r=a.range;
	f[0]=force;f[1]=pos;f[2]=r;
	forces.push_back(f);

	force=b.acc*b.mass;
	pos=b.pos;
	r=b.range;
	f[0]=force;f[1]=pos;f[2]=r;
	forces.push_back(f);
	*/

}

void reshape( int width, int height)
{

}
void keyboard(unsigned char key, int x, int y)
{

}
void mouse( int button, int state, int x, int y)
{

}
void motion( int x, int y)
{

}
using namespace std;
int main(int argc,char** argv)
{


	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_SINGLE|GLUT_RGB);
	glutInitWindowSize(600,600);
	glutInitWindowPosition(0,0);
	glutCreateWindow("my first window without a bunch of crap");
	init();
	glutDisplayFunc(display);
	glutIdleFunc(idle);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);
	glutMainLoop();
	


	return 0;
}