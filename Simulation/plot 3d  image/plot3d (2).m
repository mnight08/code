function plot3d( X,Y,Z,I )
%plot3d function to plot image in 
%
%Usage
%   plot3d(X,Y,Z,I,Size);
%
%   This function will plot the values at  I(i,j,k) at the point X(i),Y(j),Z(k) as a  
%   cube of size Size(i,j,k)\.  X,Y,Z, I,
%    must all be 3d matrices of the same size.  the use of this function is meant to be 
%   [X,Y,Z]=meshgrid([a:b],[c,d],[e,f]), where I 
MatrixDimensions=size(X);
L=MatrixDimensions(1);
N=MatrixDimensions(2);
M=MatrixDimensions(3);


Max=max(max(max(I)));
Min=min(min(min(I)));
add_this_to_min_so_it_is_zero=-Min;

colors=colormap;
Size0fColors=size(colors);
NumberOfColors=Size0fColors(1);
i=1;
cube_size=[1,1,1];
while i<=L
    j=1;
    while j<=N
        k=1;
        while k<=M
		%should map the values in the intensity matrix between 0, when it is minimum, and .99 when it is maximum
        ScaledValue=(I(i,j,k)+add_this_to_min_so_it_is_zero)/(Max+add_this_to_min_so_it_is_zero+1);
        %the index in the list of colors is then the number of colors times the value +1, so that it is the closest integer, and greater than or
        %equal to 1.
%         add_this_to_min_so_it_is_zero
%         ScaledValue
%         NumberOfColors
%         ScaledValue*NumberOfColors
%         floor(ScaledValue*NumberOfColors)
%         floor(ScaledValue*NumberOfColors)+1
        color=colors(floor(ScaledValue*NumberOfColors)+1);
        alpha=(floor(ScaledValue*NumberOfColors))/(NumberOfColors);
        voxel([X(i,j,k),Y(i,j,k),Z(i,j,k)],cube_size,color,alpha);
        k=k+1;
        end
        j=j+1;
    end
    i=i+1;
end





end

