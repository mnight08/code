%create a mesh grid to plot points on
[X,Y,Z]=meshgrid(-4*pi:pi:4*pi,-4*pi:pi:4*pi,1:1.1);
s=size(X);
colormap(bone);
%fill in values for image
I=zeros(size(X));
%I(2,2,2)=10000;
    i=1;
    while i<=s(1)
        j=1;
        while j<=s(2)
            k=1;
            while k<=s(3)
                %first test-looked like a plane wave
                %I(i,j,k)=sin(X(i,j,k));
                %second
                I(i,j,k)=sin(X(i,j,k))*sin(Y(i,j,k));
                %fourth
                %I(i,j,k)=sin(X(i,j,k))*sin(X(i,j,k));
                k=k+1;
            end
            j=j+1;
        end
        i=i+1;
    end


    %fill in the sizes for the image


plot3d( X,Y,Z,I );
set(gcf, 'Renderer', 'opengl');