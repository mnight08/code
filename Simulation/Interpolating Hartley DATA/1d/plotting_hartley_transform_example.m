%The idea here is to interpolate our given hartley data and compare its
%reconstruction to using the raw data by itself

clear all;
close all;
hold on;
c_0=1;%3*10^8;
u=-20:.1:20;
x=-10:.005:10;
f=@airy;

N=size(x);
M=size(u);
H=zeros(M(2),1);
%res is the number of points between our given hartley data that we will
%interpolate
res=5;
xi=zeros(M(2)+res*(M(2)-1),1);


i=1;

%original functions
plot(x,f(x),'k');

%approximate hartley transform
H=hartley_transform1d(f,u);
%plot approximated hartley transform
figure
plot(u,H,'b');

%approximate hartley transform applied to hartley trasnform using only given hartley data.  This should
%be approximately the original function
HH=zeros(N(2),1);
i=1;
HH=
%plot estimate of original function using only given hartley data.  I did
%not keep scaling factor for approximated integral, so the inversion will
%be slightly off by a constant factor
figure
plot(x,HH/10^4,'r');


%generate the interpolation points
j=1;
while j<=(M(2)-1)
    k=1;
    xi(1+(res+1)*(j-1))=u(j);
    while k<=res 
        xi(1+(res+1)*(j-1)+k)=u(j)+k*(u(j+1)-u(j))/res;
        k=k+1;
    end
    j=j+1;
end
xi(j)=u(j);

%use the interpolated data to estimate hartley transform.
HInterp=interp1(u,H,xi);
HHInterp=zeros(N(2),1); 
i=1;
while i<=N(2)
    j=1;
    while  j<=M(2)+res*(M(2)-1)
        HHInterp(i)=HHInterp(i)+cas(2*pi*x(i)*xi(j))*HInterp(j);
    j=j+1;
    end
    i=i+1;
end

figure
plot(x,HHInterp/10^4,'r');



