%The idea here is to interpolate our given hartley data and compare its
%reconstruction to using the raw data by itself

clear all;
close all;
hold on;
c_0=1;%3*10^8;
u=-10:.01:10;
x=-10:.05:10;
f=@testfunc;

N=size(x);
M=size(u);
H=zeros(M(2),1);
%res is the number of points between our given hartley data that we will
%interpolate
res=50;
xi=zeros(M(2)+res*(M(2)-1),1);

fx=zeros(size(x));
i=1;
i=1;
while i < N(2)
    fx(i)=f(x(i));
    i=i+1;
end
    
    
%original functions
plot(x,fx,'k');
i=1;
%approximate hartley transform
while i<=M(2)
    j=1;
    while  j<=N(2)
        H(i)=H(i)+cas(2*pi*u(i)*x(j))*f(x(j));
    j=j+1;
    end
    i=i+1;
    N(2)-i;
end
%plot approximated hartley transform
figure
plot(u,H,'b');

%approximate hartley transform applied to hartley trasnform using only given hartley data.  This should
%be approximately the original function
HH=zeros(N(2),1);
i=1;
while i<=N(2)
    j=1;
    while  j<=M(2)
        HH(i)=HH(i)+cas(2*pi*x(i)*u(j))*H(j);
    j=j+1;
    end
    i=i+1;
    
end
%plot estimate of original function using only given hartley data.  I did
%not keep scaling factor for approximated integral, so the inversion will
%be slightly off by a constant factor
figure
plot(x,HH/10^4,'r');


%generate the interpolation points
j=1;
while j<=(M(2)-1)
    k=1;
    xi(1+(res+1)*(j-1))=u(j);
    while k<=res 
        xi(1+(res+1)*(j-1)+k)=u(j)+k*(u(j+1)-u(j))/res;
        k=k+1;
    end
    j=j+1;
end
xi(j)=u(j);

%use the interpolated data to estimate hartley transform.
HInterp=interp1(u,H,xi);
HHInterp=zeros(N(2),1); 
i=1;
while i<=N(2)
    j=1;
    while  j<=M(2)+res*(M(2)-1)
        HHInterp(i)=HHInterp(i)+cas(2*pi*x(i)*xi(j))*HInterp(j);
    j=j+1;
    end
    i=i+1;
end

figure
plot(x,HHInterp/10^4,'r');



