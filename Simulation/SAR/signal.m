%simple linear signal.  it is zero at start, and stop at stop.  zero if t is outside of start<t<stop
function y= signal(f)
    w_0=1e4;
    y=exp(-1i*(f-w_0)^2);
end

