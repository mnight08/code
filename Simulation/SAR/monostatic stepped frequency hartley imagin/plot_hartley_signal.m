clear all;
close all;
hold on;
c_0=1;%3*10^8;
u=1;
t=0:.0001:7;
y1=cas(u*t);

plot(t,y1,'r');
u=2;
y1=cas(pi*c_0*u*t);
plot(t,y1,'b');

u=10;
y1=cas(pi*c_0*u*t);
plot(t,y1,'g');