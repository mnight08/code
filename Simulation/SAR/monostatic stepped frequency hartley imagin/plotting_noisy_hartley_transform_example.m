clear all;
close all;
hold on;
c_0=1;%3*10^8;
u=-10:.05:10;
x=-20:.01:20;
f=@airy;
SNR=1000;
N=size(x);
M=size(u);
H=zeros(M(2),1);
Hf=zeros(N(2),1);
i=1;


plot(x,f(x),'k');
j=1;
while  j<=N(2)
        Hf(j)=f(x(j));
    j=j+1;
end


while i<=M(2)
    j=1;
    while  j<=N(2)
        H(i)=H(i)+cas(2*pi*u(i)*x(j))*f(x(j));
    j=j+1;
    end
    i=i+1;
    N(2)-i;
end
H=awgn(H,SNR,'measured');
figure
plot(u,H,'b');
HH=zeros(N(2),1);
i=1;
while i<=N(2)
    j=1;
    while  j<=M(2)
        HH(i)=HH(i)+cas(2*pi*x(i)*u(j))*H(j);
    j=j+1;
    end
    i=i+1;
    
end
figure
plot(x,HH/10^4,'r');



