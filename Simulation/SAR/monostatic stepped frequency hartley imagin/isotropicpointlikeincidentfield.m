'building image grid'
%make the grid to place the image on
%scene dimensions in meters
image_width=3000000000;
image_height=3000000000;
%meters per pixel
image_resolution=100000000;
image_center=[0,0]';
N1=image_width/image_resolution;
N2=image_width/image_resolution;
%gives the positions of the i, j th 
imaging_grid=zeros(2,N1,N2);
c_0=3*10^8;
k1=1;
while k1<=N1
    k2=1;
    while k2<=N2
        imaging_grid(1,k1,k2)=image_center(1)-image_width/2+(k1-1)*image_resolution+image_resolution/2;
        imaging_grid(2,k1,k2)=image_center(2)-image_height/2+(k2-1)*image_resolution+image_resolution/2;  
        k2=k2+1;
    end
    k1=k1+1;
end

incident_field=zeros(N1,N2);
y=[0,0]';
t=1;
k1=1;
while k1<=N1
    k2=1;
    while k2<=N2
        incident_field(k1,k2)=cas(t-norm(imaging_grid(:,k1,k2)-y)/c_0)/(4*pi*norm(imaging_grid(:,k1,k2)-y));
        k2=k2+1;
    end
    k1=k1+1;
end
Reconstructed_image=zeros(N1,N2,3);
k1=1;
while k1<=N1
    k2=1;
    while k2<=N2
       Reconstructed_image(k1,k2,1)=incident_field(k1,k2);
       Reconstructed_image(k1,k2,2)=Reconstructed_image(k1,k2,1);
       Reconstructed_image(k1,k2,3)=Reconstructed_image(k1,k2,1);
       k2=k2+1; 
    end
    
    N1-k1
    k1=k1+1;
end

%convert reflectivity into image
plot_image(Reconstructed_image,image_width,image_height,image_resolution,imaging_grid)
colormap(hot);
set(gca,'YDir','normal');
colorbar