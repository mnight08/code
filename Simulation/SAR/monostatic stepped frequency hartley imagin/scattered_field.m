function value=scattered_field(y,u,t,imaging_grid,reflectivity,N1,N2 )
    c_0=3*10^8;
    k1=1;
    value=0;
    constant_factors=-(c_0*u)^2/16;
    while k1<=N1
        k2=1;
        while k2<=N2
            value=value+reflectivity(k1,k2)*signal(u,(t-2*norm(y-imaging_grid(:,k1,k2))/c_0))/(norm(y-imaging_grid(:,k1,k2))^2);
            k2=k2+1;
        end
        k1=k1+1;
    end
	value=constant_factors*value;

end
