function value=signal(u,t)
	c_0=3*10^8;
   	value=cos(pi*u*c_0*t)+sin(pi*u*c_0*t);
end