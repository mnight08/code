clear all
close all
warning off

%define constants
c_0=3*10^8;


'defining parameters'
%define parameters
range=1000000000000;
number_of_time_samples=10;
number_of_antenna_locations=30;
number_of_azimuth_samples=number_of_antenna_locations;%/2;
number_of_elevation_samples=1;%number_of_antenna_locations/2;
number_of_frequencies=200;
frequency_spacing=.05;
time_spacing=.0000000000000001;
time_start=0;
frequency_start=.2;
u=frequency_start:frequency_spacing:frequency_start+(number_of_frequencies-1)*frequency_spacing;
time_samples=time_start:time_spacing:time_start+(number_of_time_samples-1)*time_spacing;

'generating antenna locations'
%make set of directions at given range
azimuth_angles=0:2*pi/number_of_azimuth_samples:2*pi-2*pi/number_of_azimuth_samples;
elevation_angles=pi/2.5;%0:(pi/2)/number_of_elevation_samples:pi/2-(pi/2)/number_of_elevation_samples;
antenna_directions=zeros(3,number_of_antenna_locations);
n=1;
i=1;
while i<=number_of_azimuth_samples
    j=1;
    while j<=number_of_elevation_samples
        antenna_directions(1,n)=range*sin(elevation_angles(j))*cos(azimuth_angles(i));
        antenna_directions(2,n)=range*sin(elevation_angles(j))*sin(azimuth_angles(i));
        antenna_directions(3,n)=range*cos(elevation_angles(j));
        n=n+1;
        j=j+1;
    end
    i=i+1;
end


'building image grid'
%make the grid to place the image on
%scene dimensions in meters
image_width=3000000000;
image_height=3000000000;
%meters per pixel
image_resolution=100000000;
image_center=[0,0]';
N1=image_width/image_resolution;
N2=image_width/image_resolution;
%gives the positions of the i, j th 
imaging_grid=zeros(2,N1,N2);
k1=1;
while k1<=N1
    k2=1;
    while k2<=N2
        imaging_grid(1,k1,k2)=image_center(1)-image_width/2+(k1-1)*image_resolution+image_resolution/2;
        imaging_grid(2,k1,k2)=image_center(2)-image_height/2+(k2-1)*image_resolution+image_resolution/2;  
        k2=k2+1;
    end
    k1=k1+1;
end

'defining reflectivity'
%define the reflectivity function
reflectivity=zeros(N1,N2);



%hardcoded 10x10 smiley face reflectivity function is below, 
% reflectivity(2,3)=10^-9;
% reflectivity(2,6)=10^-9;
% 
% reflectivity(3,2)=10^-9;
% reflectivity(3,3)=10^-9;
% reflectivity(3,4)=10^-9;
% reflectivity(3,7)=10^-9;
% 
% reflectivity(4,3)=10^-9;
% reflectivity(4,8)=10^-9;
% 
% reflectivity(5,8)=10^-9;
% 
% reflectivity(6,8)=10^-9;
% 
% reflectivity(7,3)=10^-9;
% reflectivity(7,8)=10^-9;
% 
% reflectivity(8,2)=10^-9;
% reflectivity(8,3)=10^-9;
% reflectivity(8,4)=10^-9;
% reflectivity(8,7)=10^-9;
% 
% reflectivity(9,3)=10^-9;
% reflectivity(9,6)=10^-9;

% reflectivity((image_width/image_resolution)/2-2,(image_height/image_resolution)/2)=-10^3;
% reflectivity((image_width/image_resolution)/2+2,(image_height/image_resolution)/2)=-10^3;
%point target
reflectivity((image_width/image_resolution)/2,(image_height/image_resolution)/2)=-10^3;


'simulating data'
%forward model
DATA=zeros(number_of_antenna_locations,number_of_frequencies,number_of_time_samples);
k=1;
while k<=number_of_time_samples
    i=1;
    while i<=number_of_antenna_locations
        j=1;
        while j<=number_of_frequencies
            DATA(i,j,k)=scattered_field(u(j),antenna_directions(i),time_samples(k),imaging_grid,reflectivity,N1,N2);        
        	j=j+1;
        end
        i=i+1; 
        number_of_antenna_locations-i
    end
    k=k+1;
end


%average the hartley data
'generating hartley data'
AVERAGED_Hartley_DATA=zeros(2*number_of_antenna_locations,number_of_frequencies);
A=zeros(2,2);
b=zeros(2,1);
h=zeros(2,1);

%recover hartley data two points at a time
dt=diff(time_samples);
dy=zeros(1,number_of_time_samples-1);
i=1;
while i<=number_of_antenna_locations
	j=1;
	while j<=number_of_frequencies	
		%matlab sucks, so the below line does not work, have to store the
		%diff in 1 x number of time samples matrix
        %derivative=diff(DATA(i,j,:))./dt;
        dy=diff(DATA(i,j,:));
        
		scaling_factor=-norm(antenna_directions(:,i))^2*16/(c_0^2*u(j)^2);
		k=1;
		while k<=number_of_time_samples-1
			derivative=dy(k)/dt(k);
            tau=pi*c_0*u(j)*time_samples(k)-2*pi*u(j)*norm(antenna_directions(:,i));
			A(1,1)=cos(tau);
			A(1,2)=-sin(tau);
			A(2,1)=sin(tau);
			A(2,2)=cos(tau);
			b(1)=DATA(i,j,k);
			b(2)=derivative/(pi*c_0*u(j));
			h=A*b;
			AVERAGED_Hartley_DATA(i,j)=AVERAGED_Hartley_DATA(i,j)+h(1);
			AVERAGED_Hartley_DATA(number_of_antenna_locations+i,j)=AVERAGED_Hartley_DATA(number_of_antenna_locations+i,j)+h(2);
			k=k+1;
		end
		AVERAGED_Hartley_DATA(i,j)=AVERAGED_Hartley_DATA(i,j)*scaling_factor;
		AVERAGED_Hartley_DATA(number_of_antenna_locations+i,j)=AVERAGED_Hartley_DATA(number_of_antenna_locations+i,j)*scaling_factor;
		j=j+1;
    end
    number_of_antenna_locations-i
    i=i+1;

end

'ploting origional image'
plot_origional_image(image_width,image_height,image_resolution,imaging_grid,reflectivity);

%image reconstruction
'applying inverse hartley transform and recovering image'
recovered_reflectivity=zeros(N1,N2);
%make a note sometime on numerical integration. should be integrating over -infinity to infinity, so i should make a 
%a change of variables to make this proper.
%just directly evaluate the integral by summation
%for each fixed point on our image grid, we evaluate the Hartley inverse of
%our 3-d data
%the reflectivity at a point is given by the Hartley transform integral
%over volume cas( 2 * pi * x dot u ) V'( u ) du 
k1=1;
%for each pixel in the image 
while k1<=N1
	k2=1;
	while k2<=N2
			%add up all the Hartley data on the positive real line
			i=1;
			while i <= number_of_antenna_locations
				j=1;
				while j<= number_of_frequencies
					recovered_reflectivity(k1,k2)=recovered_reflectivity(k1,k2)+cas(2*pi*u(j)*2/c_0*(imaging_grid(1,k1,k2)*antenna_directions(1,i)+imaging_grid(2,k1,k2)*antenna_directions(2,i)))*AVERAGED_Hartley_DATA(i,j);
					j=j+1;
                end
                i=i+1;
            end

    			%add up all the Hartley data on the negative real line

            while i <= 2*number_of_antenna_locations
				j=1;
				while j<= number_of_frequencies
					recovered_reflectivity(k1,k2)=recovered_reflectivity(k1,k2)+cas(2*pi*(-u(j))*2/c_0*(imaging_grid(1,k1,k2)*antenna_directions(1,i-number_of_antenna_locations)+imaging_grid(2,k1,k2)*antenna_directions(2,i-number_of_antenna_locations)))*AVERAGED_Hartley_DATA(i,j);
					j=j+1;
                end
				i=i+1;
            end
		k2=k2+1;
    	end
    N1-k1
    k1=k1+1;
    
end










%convert reflectivity into image.  Each entry is a rgb triple, where each entry is between 0 and 1 
'now building image'
Reconstructed_image=zeros(N1,N2,3);
k1=1;
while k1<=N1
    k2=1;
    while k2<=N2
       Reconstructed_image(k1,k2,1)=norm(recovered_reflectivity(k1,k2));
       Reconstructed_image(k1,k2,2)=Reconstructed_image(k1,k2,1);
       Reconstructed_image(k1,k2,3)=Reconstructed_image(k1,k2,1);
       k2=k2+1; 
    end
    
    N1-k1
    k1=k1+1;
end

%convert reflectivity into image
plot_image(Reconstructed_image,image_width,image_height,image_resolution,imaging_grid)
colormap(hot);
set(gca,'YDir','normal');
colorbar


