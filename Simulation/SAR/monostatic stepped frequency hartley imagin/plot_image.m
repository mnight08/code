function plot_image(given_image,image_width,image_height,image_resolution,imaging_grid)

Max=max(max(max(given_image)));
Min=min(min(min(given_image)));

%normalize and shift data
z1=1;
while z1<=image_width/image_resolution
    z2=1;
    while z2<=image_height/image_resolution
       given_image(z1,z2,1)=(given_image(z1,z2,1)-Min)/abs(Max+Min);
       given_image(z1,z2,2)=given_image(z1,z2,1);
       given_image(z1,z2,3)=given_image(z1,z2,1);
       z2=z2+1; 
    end
    
    z1=z1+1;
end

%convert reflectivity into image
imagesc(imaging_grid(1,:,1),imaging_grid(2,1,:),given_image);
colormap(hot);
set(gca,'YDir','normal');
end