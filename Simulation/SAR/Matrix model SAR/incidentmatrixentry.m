function entry = incidentmatrixentry( transmiters,recievers,cubes, L, j, k, t,scalingfactor )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    c_0=3*10^8;
    entry=0;
    l=1;
    x=0;y=0;z=0;
    while l<=L

        integrand=@(x,y,z) (((transmiters(l).carrier)^2)*transmiters(l).signal(t*ones(size(x'))-(normofvectors(ones(size(x'))*transmiters(l).position-[x',y*ones(size(x')),z*ones(size(x'))])+normofvectors(ones(size(x'))*recievers(j).position-[x',y*ones(size(x')),z*ones(size(x'))]))/c_0).*exp(-1i*(transmiters(l).carrier/c_0)*(t*ones(size(x'))-(normofvectors(ones(size(x'))*transmiters(l).position-[x',y*ones(size(x')),z*ones(size(x'))])+normofvectors(ones(size(x'))*recievers(j).position-[x',y*ones(size(x')),z*ones(size(x'))]))/c_0))./(normofvectors(ones(size(x'))*transmiters(l).position-[x',y*ones(size(x')),z*ones(size(x'))]).*normofvectors(ones(size(x'))*recievers(j).position-[x',y*ones(size(x')),z*ones(size(x'))])))';
        integral=scalingfactor*triplequad(integrand,cubes(k).xmin,cubes(k).xmax,cubes(k).ymin,cubes(k).ymax,cubes(k).zmin,cubes(k).zmax,.001)
        entry=entry+integral
        l=l+1;
        
    end

    'entry is:'
    entry=(-(1/4*pi)^2)*entry
end

