function correcteddata= CorrectData(data,antennapositions,v, grid,frequencysamples,NumberScatteringTimes,ps,spacing)
%GENERATE_DATA Summary of this function goes here
%   Detailed explanation goes here
   % %set up greens function
    % %!!!!!!!!!!!!!11
    c_0=299792458;
    NumberOfFrequencies=max(size(frequencysamples));
    NumberOfAntennaPositions=max(size(antennapositions));
    correcteddata=data;
    M=min(size(grid));
    N=max(size(grid));
    G=zeros(M,N);
    uin=zeros(M,N);
    m=1;
    while m<=NumberOfFrequencies
        w=frequencysamples(m);
        
        %*********************************************************
        %GENERERATE GREENS FUCNTION.  Only changes with frequency
        %*********************************************************
        i=1;
        while i<=M
        j=1;
            while j<=N
                if(norm([grid(i),grid(j)])~=0)
                    G(i,j)=exp(-1i*w*norm([grid(i),grid(j)])/c_0)/(4*pi*norm([grid(i),grid(j)]));
                end
                j=j+1;
            end
            i=i+1;
        end
        %*********************************************************
        
        %*********************************************************
        %generate data for each antenna position at the given frequency
        %*********************************************************
        n=1;
        while n<= NumberOfAntennaPositions
            %*********************************************************
            %Set up antenna position
            %*********************************************************
            antennaposition=[0,0,0];
            antennaposition(1)=antennapositions(n,1);
            antennaposition(2)=antennapositions(n,2);
            antennaposition(3)=0;
            %*********************************************************
            
            %*********************************************************
            %Generate Incident field.  Depends on antenna position and
            %frequency
            %*********************************************************
            i=1;
            while i<=M
                j=1;
                while j<=N
                   if(norm([grid(i),grid(j),0]-antennaposition)~=0)
                       uin(i,j)=ps(m)*exp(-1i*w*norm([grid(i,j,1),grid(i,j,2),0]-antennaposition)/c_0)...
                       /(4*pi*norm([grid(i,j,1),grid(i,j,2),0]-antennaposition));
                   end
                   j=j+1;
               end
               i=i+1;
            end
            %*********************************************************
            
            %*********************************************************
            %estimate high order scattering
            high_order_scattering_estimate=generate_data(v, grid,antennapositions,frequencysamples,NumberScatteringTimes,spacing,ps);
            low_scattering=generate_data(v, grid,antennapositions,frequencysamples,1,spacing,ps);
            high_order_scattering_estimate=high_order_scattering_estimate-low_scattering;

            correcteddata(m,n)=correcteddata(m,n)-high_order_scattering_estimate(m,n);
            %*********************************************************

        
        
            n=n+1;
        end
        m=m+1;
        %*********************************************************
    end
end