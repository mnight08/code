function subfield = generateIncidentField( sub_grid, antennaposition,frequencysamples,frequencyindex,ps)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
            A=1;
            M=max(size(sub_grid));

            subfield=zeros(M,1);
            i=1;
            while i<=M
                subfield(i)=A*ps(frequencyindex)*GreensFunction([sub_grid(i,1),sub_grid(i,2),0]-antennaposition,frequencysamples(frequencyindex));
                i=i+1;
            end
              
            
end

