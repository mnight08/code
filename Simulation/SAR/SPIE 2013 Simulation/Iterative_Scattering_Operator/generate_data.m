function data = generate_data(v, grid,antennapositions,frequencysamples,NumberScatteringTimes,spacing,ps)
%GENERATE_DATA Summary of this function goes here
%   Detailed explanation goes here
   % %set up greens function
    % %!!!!!!!!!!!!!11
    NumberOfFrequencies=max(size(frequencysamples));
    NumberOfAntennaPositions=max(size(antennapositions));
    data=zeros(NumberOfFrequencies,NumberOfAntennaPositions);
    antennaposition=[0,0,0];
    
    %count the number of non  zero points in v
    count=0;
       

    %it might be slightly faster to pass around indices, and grid, since it
    %is not a hard copy unless I try to change grid.  I will not just yet,
    %since it will make latter code slightly ugly.
    M=max(size(v));
    n=1;
    while n<=M
        m=1;
        while m<=M
            if v(m,n) ~= 0 
                count=count+1;
            end
            m=m+1;
        end
        n=n+1;
    end
    
    subv=zeros(count,1);
    sub_grid=zeros(count,2);
    
    
    n=1;
    s=1;
    while n<=M
        m=1;
        while m<=M
            if v(m,n) ~= 0 
                subv(s)=v(m,n);
                sub_grid(s,:)=grid(m,n,:);
                s=s+1;
            end
            m=m+1;
        end
        n=n+1;
    end
    
    
    
    m=1;
    while m<=NumberOfFrequencies
        w=frequencysamples(m);
        
        %*********************************************************
        %GENERERATE GREENS FUCNTION.  Only changes with frequency
        %*********************************************************
        %G=generateGreensFunction(grid,w);
        %*********************************************************
        
        %*********************************************************
        %generate data for each antenna position at the given frequency
        %*********************************************************
        n=1;
        while n<= NumberOfAntennaPositions
            %*********************************************************
            %Set up antenna position
            %*********************************************************

            antennaposition(1)=antennapositions(n,1);
            antennaposition(2)=antennapositions(n,2);
            %*********************************************************
            
            %*********************************************************
            %Generate Incident field.  Depends on antenna position and
            %frequency
            %*********************************************************
            subuin=generateIncidentField(sub_grid, antennaposition,frequencysamples,m,ps);

            %*********************************************************
            
            %imagesc(real(uin));
            %figure 
            %*********************************************************
            %Get data for given frequency and antenna position
            %*********************************************************
            data(m,n)=GenerateMultiscatteringData(subuin, subv, sub_grid,antennaposition, w,NumberScatteringTimes,spacing);
            %*********************************************************
            
            n=n+1;
        end
        m=m+1;
        %*********************************************************
    end
end