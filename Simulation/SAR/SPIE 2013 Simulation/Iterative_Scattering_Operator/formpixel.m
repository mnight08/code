function pixel = formpixel(x,subdata,subindices,  antennapositions,frequencysamples,ps )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
c_0=299792458;
%assuming the antennas lie on a circle.  

antennaposition=[0,0];
M=max(size(subdata));
numberofantennapositions=max(size(antennapositions));
%numberoffrequencies=max(size(frequencysamples));


%integrand1=zeros(numberoffrequencies,1);
%integrand2=zeros(max(size(antennaposition)),1);
%fixed distance for antenna since it is on a circle
antennadistance=norm(antennapositions(1));
dtheta=pi/numberofantennapositions;
dw=frequencysamples(2)-frequencysamples(1);
pixel=0;
k=1;
%I should modify this to use trapz in the other dim as well.  I have not
%yet, maybe after conference
while k<= M
   %n=1;
   %we are assuming x is 2d, and the antenna is on the ground. This needs
   %to change to get it to work in 3d
   m=subindices(k,1);
   n=subindices(k,2);
   antennaposition(1)=antennapositions(m,1);
   antennaposition(2)=antennapositions(m,2);
   antennadirection=antennaposition/antennadistance;
   
   
   %fill in the integrand for dw.  then use trapz to compute the
   %integral.  
   %use trapezoid rule for computing the integral in w domain.
   %while n<= numberoffrequencies
   
   w=frequencysamples(n);
   %   integrand1(n)=0;
       if ps(n) ~=0 && subdata(k) ~=0
            rp=real(ps(n));
            ip=imag(ps(n));
            rd=real(subdata(k));
            id=imag(subdata(k));
            d=(-2*w*(antennadirection*x'+antennadistance))/c_0;
    %        integrand1(n)=(cos(d)*(rp*rd+ip*id)+sin(d)*(rd*ip-rp*id))/(w*(rp^2+ip^2));
       end
   %    n=n+1;
   %end
   %integrand2(m)=trapz(integrand1);
   pixel=pixel+(cos(d)*(rp*rd+ip*id)+sin(d)*(rd*ip-rp*id))/(w*(rp^2+ip^2));
   k=k+1;
end
    pixel=(-16)*dw*dtheta*pixel*((antennadistance/c_0)^2/pi);
end

