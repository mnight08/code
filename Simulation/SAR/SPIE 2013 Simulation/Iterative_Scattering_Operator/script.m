    %*******************************************************    
    close all;
    clear all;
    hold off;
    %*******************************************************
    
    
    %*******************************************************
    %constants and parameters

    tolerance=10^(-4);
    Iterations=1;
    NumberScatteringTimes=1;
    %*******************************************************
    
    
    %*******************************************************
    %RADAR parameters
    numberofrequencysamples=30;
    numberofantennapositions=8;
    w_0=10^12;
    B=2*(w_0-1);
    w_0=50*B/numberofrequencysamples+w_0;
    frequencysamples=w_0-B/2:B/numberofrequencysamples:w_0+B/2-1/numberofrequencysamples;
    ps=p(frequencysamples);
    antennapositions=zeros(numberofantennapositions,2);
    distance=1000;
    %*******************************************************
    
    
    %*******************************************************
    %set up antenna positions.  For now, use a circle on the ground.
    %only need semi circle if we assume that the reflectivity is real
    n=1;
    while n<=numberofantennapositions
        angle=(n-1)*(pi)/numberofantennapositions;
        antennapositions(n,1)=distance*(cos(angle));
        antennapositions(n,2)=distance*(sin(angle));
        n=n+1;
    end
    
    
    %*******************************************************
    
    
    
    %*******************************************************
    %reflectivity function should be square
    %set up reflectivity function
    v=.1*imread('test1.bmp');
    %*******************************************************
    %alternative reflectivity function horizontal line
    %v=zeros(N,N);
    % k=1;
    % while k<=N/4
    % v(N/2,N/4+k)=-.001;
    % k=k+1;
    % end
    %v(N/2,N/2)=10000;
    %normalize V
    %vmin=min(min(v));
    %vmax=max(max(v));
    %v=(vmin+v)/(vmin+vmax);
    %v=.001*imread('Copy_of_test.bmp');
    %*******************************************************
    %alternative reflectivity function only two points right now.
    %v(reflectivity_grid_size/2,reflectivity_grid_size/2)=-200;
    %v(reflectivity_grid_size/4,reflectivity_grid_size/4)=-400;
    %*******************************************************
    %alternative reflectivity function line along diagonal
    %  j=reflectivity_grid_size/4;
    %  while j<=reflectivity_grid_size/2
    %     reflectivity.values(j,j)=-20;
    %     j=j+1; 
    %  end
    %*******************************************************
    %show our original reflectivity
    imagesc(v);
    figure;
    colorbar;

    
    %*******************************************************
    %set up grid

    M=max(size(v));
    grid=zeros(M,M,2);
    %total length of the grid.  This determines the distance between
    %targets, since grid_length/grid_size is the length of the reflectivity
    %function
    grid_length=M;
    %either this or grid_length may be preset, but they determine each
    %other grid_length=spacing*N
    spacing=grid_length/M;
    gridx=linspace(-(grid_length/2),grid_length/(2),M);
    gridy=linspace(-(grid_length/2),grid_length/(2),M);
    r=1;
    while r<=M
        c=1;
        while c<=M
            grid(r,c,1)=gridx(c);
            grid(r,c,2)=gridy(r);
            c=c+1;
        end
        r=r+1;
    end
    %*******************************************************
    %we assume that the reflectivity function lies on the right hand side
    %of the grid, and data is collected on the far left side.  No need to
    %extend the reflectivty function.
    
    
    %Generate data.  DATA does not included incident field
    data=generate_data(v,grid,antennapositions,frequencysamples,NumberScatteringTimes,spacing,ps);
    scaling_paramter=10^(-6);
    corrected_data=data;
    k=1;
    while k<=Iterations
        %Image reconstruction 
        %recover reflectivity function
        reflectivity_estimate=FormImage(corrected_data,grid,antennapositions,frequencysamples,ps,tolerance);    
        imagesc(abs(reflectivity_estimate));
        if k~=Iterations
            figure;
        end
        
        %clean up image in the next steps
        %take image estimate, and scale it by scaling parameter.
        reflectivity_estimate=scaling_paramter*reflectivity_estimate;
        %denoise scaled estimate
        %!!!!!!!!!!!!!!!!!!!!not being used yet

        %generate corrected data
        corrected_data=CorrectData(data,antennapositions,reflectivity_estimate, grid,frequencysamples,NumberScatteringTimes,ps,spacing);
        
        k=k+1;
    end
    
    
    
    
%     %do the same thing for the other image
%        v=.001*imread('smallerimage.bmp');
%     
%     
%         data=generate_data(v,grid,antennapositions,frequencysamples,NumberScatteringTimes,spacing,ps);
%     scaling_paramter=10^(-6);
%     corrected_data=data;
%     k=1;
%     while k<=Iterations
%         %Image reconstruction 
%         %recover reflectivity function
%         reflectivity_estimate=FormImage(corrected_data,grid,antennapositions,frequencysamples,ps,tolerance);    
%         imagesc(abs(reflectivity_estimate));
%         if k~=Iterations
%             figure;
%         end
%         
%         %clean up image in the next steps
%         %take image estimate, and scale it by scaling parameter.
%         reflectivity_estimate=scaling_paramter*reflectivity_estimate;
%         %denoise scaled estimate
%         %!!!!!!!!!!!!!!!!!!!!not being used yet
% 
%         %generate corrected data
%         corrected_data=CorrectData(data,antennapositions,reflectivity_estimate, grid,frequencysamples,NumberScatteringTimes,ps,spacing);
%         
%         k=k+1;
%     end
