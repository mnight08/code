function G = generateGreensFunction( grid,w )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    s=size(grid);
    M=s(1);
    N=s(2);
    G=zeros(M,N);
    i=1;
    while i<=M
        j=1;
        while j<=N
            if(norm([grid(i),grid(j)])~=0)
                G(i,j)=GreensFunction([grid(i),grid(j)],w);
            end
            j=j+1;
        end
        i=i+1;
    end
end

