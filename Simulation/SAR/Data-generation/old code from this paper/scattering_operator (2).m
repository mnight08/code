%This function takes a field, an array of scatterers, and scatters the
%field against the scatterers.  It will return the scattered field. For now
%the input reflectivity function must be a 2-d array of reflecitivity
%values.  I want to work towards allowing moving targets which would
%require the use of a frame matrix of some sort.  I want to make a front
%end that allows one to use a simple graphics program to create test
%reflectivity functions.  I will try and follow Gorham's style of using
%datastructures in these types of functions.  moving is a flag to tell
%whether the scence is moving.  If it is moving, then the last dimension of
%the reflectivity functionw will be its time dimension.  The field is
%assumed time varying and its last dimension is its frequency dimension.
%The scattered field will be computed in the frequency domain.  to get 
%the scattered field as a function of time on must fix a point in space
%and compute the fourier transform at that point.  

%****************This only works with stationary targets at the moment.
%It will be difficult to incorporate moving targets
%
%Calculates the scattered field in the frequency domain at a fixed
%frequency
%this is the brute force version.
%evalutation grid is a list of points to evaluate the scattered field.
%!!!!!!!!!!!!!!!!!!!!!
%Structure for field:  may be given an array of these
%field.dimensions.  Not needed right now since field assumed given on
%reflectivity.grid
%field.values
%field.grid  should be the same as reflectivity grid at this point.  I may
%use interpolation latter to get rid of this requirement  this is not used
%at this point
%!!!!!!!!!!!!!!!!!!!!!
%Structure for reflectivity:
%It is assumed that the 2d grid lies on the ground at height 0
%reflectivity.dimensions a 2d vector
%relfectivity.values
%reflectivity.grid
%!!!!!!!!!!!!!!!!!!!!!
%Structure for evaluation_grid:
%evaluation_grid.values. list of 3d points or grid of 2d points.  if it is
%a 2d grid, it is assumed to lie on the ground at 0
%evaluation_grid.dimensions
%evaluation_grid.islist.  flag to say whether it is a list of 3d points of
%a 2d grid
%!!!!!!!!!!!!!!!!!!!!!
%Structure for frequency:
%frequency.values
%frequency.count
function scattered_field=scattering_operator(field,reflectivity,evaluation_grid,frequency,tolerance)
%     %Algorithm outline:
%     %Build a list of non zero points of V, together with value denote
%     %TrimmedV 
%     %TrimmedV will have the indices of reflectity function that are non
%     %zero
%     %count the number of non zero elements of the reflectivity function
     c_0=299792458;
     s=0;
     i=1;
     while i<=reflectivity.dimensions(1)
         j=1;
         while j<=reflectivity.dimensions(2)
             if reflectivity.values(i,j)~=0
                 s=s+1;
             end
             j=j+1;
         end
        i=i+1; 
     end
     
     TrimmedV=zeros(s,2);
     
     i=1;
     s=0;
     while i<=reflectivity.dimensions(1)
         j=1;
         while j<=reflectivity.dimensions(2)
             if reflectivity.values(i,j)~=0
                 s=s+1;
                 TrimmedV(s,:)=[i,j];
             end
             j=j+1;
         end
        i=i+1; 
     end
     
     scattered_field=field;
     k=1;
     while k<=frequency.count
        scattered_field(k).values=zeros(evaluation_grid.dimensions); 
        k=k+1;
     end
     
     m=1;
     while m<=frequency.count
         %for each p in E.G. and not in TrimmedV   
         p1=1;
         while p1<=evaluation_grid.dimensions(1)
            p2=1;
            while p2<=evaluation_grid.dimensions(2)
                 if evaluation_grid.islist
                     p=[evaluation_grid.values(p1,p2,1),evaluation_grid.values(p1,p2,2)];
                 else 
                     p=[evaluation_grid.values(p1,p2,1),evaluation_grid.values(p1,p2,2),0];
                 end
                 %check if p is on TrimmedV.grid.  if it is not
                 %###########################################need to complete
                 %if p(3)~=0
                 %    isongrid=0;
                 %else
                 %    isongrid=0;
                     %I should change this to binary search for p in TrimmedV.
                     %Brute force method right now using linear search.
                     %check if any point in TrimmedV is the point that we are going
                     %to evaluate at.
                 %    k=1;
                 %    while k<=s
                 %        if  isequal(reflectivity.grid(TrimmedV(k,:)),[p(1),p(2)])
                 %            isongrid=1;
                 %            break;
                 %        end
                 %        k=k+1; 
                 %    end
                 %end
                 %##################################
                 %if ~isongrid
                     %evaluate -w^2sum k=1 to size(TrimmedV)TrimmedV.value(k)e^{-1iw|p-TrimmedV.position(k)|/c_0}/(4pi|p-TrimmedV.position(k)|)     
                     k=1;
                     while k<=s
                         %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!incorrect
                         if norm(p-reflectivity.grid(TrimmedV(k,1),TrimmedV(k,2),1))>tolerance
                         scattered_field(m).values(p1,p2)=scattered_field(m).values(p1,p2)...
                             +field(m).values(TrimmedV(k,1),TrimmedV(k,2)).*...
                             reflectivity.values(TrimmedV(k,1),TrimmedV(k,2)).*...
                             exp(-1i*frequency.values(m)*norm(p-[reflectivity.grid(TrimmedV(k,1),...
                             TrimmedV(k,2),1),reflectivity.grid(TrimmedV(k,1),TrimmedV(k,2),2),0])/c_0)./...
                             (4*pi*norm(p-[reflectivity.grid(TrimmedV(k,1),...
                             TrimmedV(k,2),1),reflectivity.grid(TrimmedV(k,1),TrimmedV(k,2),2),0]));
                         end
                          k=k+1; 
                         
                     end
                 %end
                     p2=p2+1; 
            end
            p1=p1+1; 
         end
        m=m+1;
     end
     %This may give me trouble if there are several points nearby that lie
     %on the reflectivity grid
     %for each p in E.G and in TrimmedV, take average near point $p$
     
     
     
     
     %multiply by factors independent of sum
     m=1;
     while m<=frequency.count
        scattered_field(m).values=-frequency.values(m)^2*scattered_field(m).values/(4*pi);
        m=m+1;
     end
     
end
