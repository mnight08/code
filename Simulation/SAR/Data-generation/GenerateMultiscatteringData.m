%no incident field is included
function data=GenerateMultiscatteringData(subu,subv,sub_grid,x,w,NumberScatteringTimes,spacing)
    %does not handle negative order scattering.  Not sure how to make sense
    %of that.  Also does not handle 0 order since it takes in the incident
    %field making it pointless to ask for zeroth order scattering.
    n=1;
    
    if NumberScatteringTimes==1
        data=scattering_operator_sparse(subu,subv,sub_grid,x,w,spacing);
    else 
        
        field=scattering_operator_sparse_ongrid(subu,subv,sub_grid,w,spacing);
    
    %this will not work yet since the scattering operator only returns the
    %scattered field at a point, and we need it over the reflectivities to
    %do multiscattering
    while n<=NumberScatteringTimes-1
        if NumberScatteringTimes==n+1
            field=scattering_operator_sparse(field+subu,subv,sub_grid,x,w,spacing);
        else 
            field=scattering_operator_sparse_ongrid(field+subu,subv,sub_grid,w,spacing);
        end
        n=n+1;
    end
    data=field;
    end
end