%perform integration 
function Image=FormImage(data,grid,antennapositions, frequencysamples,ps,tolerance)
    
    %size of reflectivty function
    M=max(size(grid));
    %dw is const, so it is bettere to figure it out here instead of
    %recompute it.  the change in angle is not const, so we need to compute
    %it each time.
    %not needed if i use trapz
    %dw=frequencysamples(2)-frequencysamples(1);
    x=[0,0];
    Image=zeros(M,M);
    %the index of grid where the points where we are recovering reflectivty
    %start

    numberofantennapositions=max(size(antennapositions));
    numberoffrequencies=max(size(frequencysamples));
    count=0;
    m=1;
    while m<=numberofantennapositions
       n=1;
       while n<=numberoffrequencies
           if abs(data(n,m))>tolerance
              count=count+1; 
           end
           n=n+1;
       end
        m=m+1;
    end
    
    
    
    
    
    %go through data and find non zero enties.
    subdata=zeros(count,1);
    subindices=zeros(count,2);
    m=1;
    while m<=numberofantennapositions
       n=1;
       while n<=numberoffrequencies
           if abs(data(n,m))>tolerance
              count=count+1; 
           end
           n=n+1;
       end
        m=m+1;
    end
    
    s=1;
    m=1;
    while m<=numberofantennapositions
       n=1;
       while n<=numberoffrequencies
           if abs(data(n,m))>tolerance
               subdata(s)=data(n,m);
               subindices(s,:)=[m,n];
               s=s+1; 
           end
           n=n+1;
       end
        m=m+1;
    end
    
    
    
    n=1;
    %this needs to be changed to a FFT or some other fast method when I can
    %figure it out.
    while n<=M
        m=1;
        while m<=M
            x(1)=grid(m,n,1);
            x(2)=grid(m,n,2);
            %evaluate fourier inverse integral using polar coordinates.
            Image(n,m)=formpixel(x,subdata,subindices,antennapositions,frequencysamples,ps);
            m=m+1;
        end
        n=n+1;
    end
%     
%     %is these two steps
%     %recover fourier data of reflectivity at uniform spacing
%     fourier_data=recover_fourier_data(corrected_data,grid,frequencysamples);
%     Image.dimensions=reflectivity_estimate.dimensions;
%     Image.grid=reflectivity_estimate.grid;
%     Image.values=zeros(reflectivity_estimate.dimensions);
%     i=1;
%     while i<=reflectivity_estimate.dimensions(1)
%     j=1;
%     while j<=reflectivity_estimate.dimensions(2)
%         k=1;
%     while k<=data.frequencies.count
%       x_1=1;
%         while x_1<=data.dimensions(1)
%             x_2=1;
%             while x_2<=data.dimensions(2)    
%                 if sin(data.frequencies.values(k))~=0 
%                 Image.values(i,j)=Image.values(i,j)+exp(1i*([Image.grid(i,j,1),Image.grid(i,j,1)]*...
%                     [2*data.frequencies.values(k)*([data.grid(x_1,x_2,1),data.grid(x_1,x_2,2)]...
%                     /norm([data.grid(x_1,x_2,1),data.grid(x_1,x_2,2)]))/c_0]')...
%                     *norm([data.grid(x_1,x_2,1),data.grid(x_1,x_2,2)])^2*...
%                     exp(1i*data.frequencies.values(k)*norm([data.grid(x_1,x_2,1),data.grid(x_1,x_2,2)])*2)*data.values(x_1,x_2,k)/...
%                     (sin(data.frequencies.values(k)*data.frequencies.values(k))*data.frequencies.values(k)^2));
%                 end
%                 x_2=x_2+1;  
%             end
%         x_1=x_1+1;
%         end
%        k=k+1; 
%     end
%     j=j+1;
%     end
%     i=i+1;
%     end
%     Image.values=Image.values*(1/(4*pi)^2);
end