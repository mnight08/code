%This function takes a field, an array of scatterers, and scatters the
%field against the scatterers.  It will return the scattered field. For now
%the input reflectivity function must be a 2-d array of reflecitivity
%values.  I want to work towards allowing moving targets which would
%require the use of a frame matrix of some sort.  I want to make a front
%end that allows one to use a simple graphics program to create test
%reflectivity functions.  I will try and follow Gorham's style of using
%datastructures in these types of functions.  moving is a flag to tell
%whether the scence is moving.  If it is moving, then the last dimension of
%the reflectivity functionw will be its time dimension.  The field is
%assumed time varying and its last dimension is its frequency dimension.
%The scattered field will be computed in the frequency domain.  to get 
%the scattered field as a function of time on must fix a point in space
%and compute the fourier transform at that point.  

%****************This only works with stationary targets at the moment.
%It will be difficult to incorporate moving targets
%
%Calculates the scattered field in the frequency domain at a fixed
%frequency
%this is the brute force version.
%measures scattered field due to an antenna at x at frequency w
function scattered_field=scattering_operator_sparse_ongrid(subu,subv,sub_grid,w,spacing)
    %grid must be at least 2x2
    %First we need to place the product onto a grid
    %product=zeros(size(u));
    M=max(size(sub_grid));
    z=[0,0,0];
    x=[0,0,0];
    n=1;
    scattered_field=zeros(M,1);
    %use trap
    %the old method was highly redundant
    %place reflectivity product onto properly sized grid.
    while n<=M
         x(1)=sub_grid(n,1);
         x(2)=sub_grid(n,2);
         m=1;
        while m<=M
         z(1)=sub_grid(m,1);
         z(2)=sub_grid(m,2);
         
         scattered_field(n)=scattered_field(n)+subv(m)*subu(m)*GreensFunction(x-z,w);
         
        m=m+1;
        end
        n=n+1;
    end
        
        
     scattered_field=-w^2*spacing^2*scattered_field;
    
end
