function data=threshhold(data,tol)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This function performs a threshholding operation.  The               %
% following fields need to be populated:                               %
%                                                                      %
% data.im_final:  The complex image value at each pixel                %
%                                                                      %
% output                                                               %
% data.im_final:  Threshholded image                                   %
% Written by LeRoy Gorham, Air Force Research Laboratory, WPAFB, OH    %
% Email:  leroy.gorham@wpafb.af.mil                                    %
% Date Released:  8 Apr 2010                                           %
%                                                                      %
% Gorham, L.A. and Moore, L.J., "SAR image formation toolbox for       %
%   MATLAB,"  Algorithms for Synthetic Aperture Radar Imagery XVII     %
%   7669, SPIE (2010).                                                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
image_dimensions=size(data.im_final);
width=image_dimensions(1);
height=image_dimensions(2);

i=1;
while i<= width
    j=1;
    while j<=height
        if(abs(data.im_final(i,j))<tol)
           data.im_final(i,j)=0; 
        end
        j=j+1;
        
    end
   i=i+1; 
end


end