%perform integration using fast fourier approximation.  Does not work yet
function Image=FormImageFourier(data,grid,antennapositions, frequencysamples)
    c_0=299792458;   
    %size of reflectivty function
    M=max(size(grid));
    %dw is const, so it is bettere to figure it out here instead of
    %recompute it.  the change in angle is not const, so we need to compute
    %it each time.
    %not needed if i use trapz
    %dw=frequencysamples(2)-frequencysamples(1);
    x=[0,0];
    Image=zeros(M,M);
    antennaposition=[0,0];
    numberofantennapositions=max(size(antennapositions));
    numberoffrequencies=max(size(frequencysamples));
    %assuming the antennas lie on a circle.  
    %angles=linspace(0,pi,numberofantennapositions);
    B=frequencysamples(numberoffrequencies)-frequencysamples(1);
    integrand=zeros(M,M,numberofantennapositions);
    transformme=zeros(numberoffrequencies,1);
    whereweknowfouriertransform=linspace(-pi*numberoffrequencies/B,pi*numberoffrequencies/B,numberoffrequencies);
    %frequencies to interpolate the fourier transform at.
    interpolationgrid=zeros(M*M,1);

    d=1;
    %I should modify this to use trapz in the other dim as well.  I have not
    %yet, maybe after conference
    while d<= numberofantennapositions

   %we are assuming x is 2d, and the antenna is on the ground. This needs
       %to change to get it to work in 3d
       antennaposition(1)=antennapositions(d,1);
       antennaposition(2)=antennapositions(d,2);

        n=1;
        %update interpolation grid.
        while n<=M
            m=1;
            while m<=M
                x(1)=grid(m,n,1);
                x(2)=grid(m,n,2);
                interpolationgrid(n+(m-1)*M)=2*(antennaposition/norm(antennaposition)*x'+norm(antennaposition))/c_0;                
                m=m+1;
            end
            n=n+1;
        end
       
      %use fft to approximate integral
      %set up array.
       n=1;
       while n<= numberoffrequencies
           w=frequencysamples(n);
           transformme(n)=0;
           if p(w)~=0 && w~=0
            transformme(n)=data(n,d)/(p(w)*w);
           end
           n=n+1;
       end
       
       fouriertransform=(fft(transformme));
       interpolatedvalues=interp1(whereweknowfouriertransform,fouriertransform,interpolationgrid,'linear','extrap');
       n=1;
        while n<=M
            m=1;
            while m<=M
                x(1)=grid(m,n,1);
                x(2)=grid(m,n,2);
                %evaluate fourier inverse integral using polar coordinates.
                integrand(n,m,d)=interpolatedvalues(n+(m-1)*M);
                m=m+1;
            end
            n=n+1;
        end
       d=d+1;
    end

        
        %the index of grid where the points where we are recovering reflectivty
        %start
        n=1;
        %this needs to be changed to a FFT or some other fast method when I can
        %figure it out.
        while n<=M
            m=1;
            while m<=M
                x(1)=grid(m,n,1);
                x(2)=grid(m,n,2);
                %evaluate fourier inverse integral using polar coordinates.
                Image(n,m)=trapz(integrand(n,m,:))*pi/numberofantennapositions;
                m=m+1;
            end
            n=n+1;
        end
        
        
        
        
        Image=(-16)*Image/(c_0^2*pi);
    
end