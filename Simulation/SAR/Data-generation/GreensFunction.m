function G= GreensFunction( x,w )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
    c_0=299792458;
    if norm(x)~=0
        G=exp(-1i*w*norm(x)/c_0)/(4*pi*norm(x));
    else
        G=0;
    end
end

