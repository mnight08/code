%This function takes a field, an array of scatterers, and scatters the
%field against the scatterers.  It will return the scattered field. For now
%the input reflectivity function must be a 2-d array of reflecitivity
%values.  I want to work towards allowing moving targets which would
%require the use of a frame matrix of some sort.  I want to make a front
%end that allows one to use a simple graphics program to create test
%reflectivity functions.  I will try and follow Gorham's style of using
%datastructures in these types of functions.  moving is a flag to tell
%whether the scence is moving.  If it is moving, then the last dimension of
%the reflectivity functionw will be its time dimension.  The field is
%assumed time varying and its last dimension is its frequency dimension.
%The scattered field will be computed in the frequency domain.  to get 
%the scattered field as a function of time on must fix a point in space
%and compute the fourier transform at that point.  

%****************This only works with stationary targets at the moment.
%It will be difficult to incorporate moving targets
%
%Calculates the scattered field in the frequency domain at a fixed
%frequency
%this is the brute force version.
function scattered_field=scattering_operator(u,v,G,w,spacing)
    %grid must be at least 2x2
    %First we need to place the product onto a grid
    %!!!!!!!!!!!!!!!!!!
    %
    product=zeros(size(u));
    M=max(size(v));
    N=max(size(u));
    
    m=1;
    %place reflectivity product onto properly sized grid.
    offset=N-M;
    while m<=M
        n=1;
        while n<=M
        product(m,offset+n)=v(m,n)*u(m,offset+n);
        n=n+1;
        
        end
    m=m+1;
    end
    scattered_field=-w^2*spacing^2*fftshift(ifft2(fft2(G).*fft2(product)));
    %scene_dimensions=size(reflectivity.values);
    %field_dimension=size(field.values);
    %scattered_field=field;
    %probably not needed anymore.  this was trying to generate scattered
    %data using riemann sum.
    %     i=1;
    %     while i<=field_dimensions(1)
    %         j=1;
    %         while j<=field_dimensions(2)
    %             k=1;
    %             while k<=scene_dimensions(1)
    %                 l=1;
    %                 while l<=scene_dimensions(2)
    %                     scattered_field.values(i,j)=scattered_field.values(i,j)+(reflectivity.values(k,l)*field.values(k,l))\(norm([scattered_field.x(i),scattered_field.y(j),0]-[])
    %                 l=l+1;
    %                 end 
    %             k=k+1;
    %             end
    %         j=j+1;
    %         end
    %     i=i+1;
    %     end
    

    
    %The assumption right now is that the scene is given on a uniform grid.
    %
    %scattered_field.values=-frequency^2*reflectivity.cell_width*reflectivity.cell_height*scattered_field.values/(4*pi);
end
