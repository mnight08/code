%plot incident field
k=1;
while k<=frequency.count
    imagesc(linspace(-evaluation_grid_length/2,evaluation_grid_length/2,evaluation_grid_size),linspace(-evaluation_grid_length/2,evaluation_grid_length/2,evaluation_grid_size),abs(incident_field(k).values));
    if k~=frequency.count
        figure;
    end
    k=k+1;
end

k=1;
while k<=frequency.count
   imagesc(imag(field(k).values));figure;
   k=k+1;
end


% %compute first term in neumann series
 BornScatteredField=scattering_operator(incident_field,reflectivity,evaluation_grid,frequency,tolerance);
 imagesc(linspace(-evaluation_grid_length/2,evaluation_grid_length/2,evaluation_grid_size),linspace(-evaluation_grid_length/2,evaluation_grid_length/2,evaluation_grid_size),abs(BornScatteredField(1).values));
 
 
 
% %compute the second term in neumann series
% SecondScatteredField=scattering_operator(BornScatteredField,reflectivity,evaluation_grid,frequency,tolerance);
% %figure
% imagesc(linspace(-evaluation_grid_length/2,evaluation_grid_length/2,evaluation_grid_size),linspace(-evaluation_grid_length/2,evaluation_grid_length/2,evaluation_grid_size),abs(SecondScatteredField(10).values));
% imagesc(linspace(-evaluation_grid_length/2,evaluation_grid_length/2,evaluation_grid_size),linspace(-evaluation_grid_length/2,evaluation_grid_length/2,evaluation_grid_size),abs(SecondScatteredField(10).values+BornScatteredField(1).values));
% %generatre some multiscattering data
% %data=GenerateMultiscatteringData(reflectivity,incident_field,evaluation_
% grid,frequency,6);