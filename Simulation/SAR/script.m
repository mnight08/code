%define constants and stuff
c_0=3*10^8;
N_T=5;
N_R=5;
K=1;
N_S=100;
B=100;
f0=550;

w=2*pi*(f0-B/2):2*pi*B/N_S:2*pi*(f0+B/2);
%left corner of image
imageposition=[-50,-50]';
%height, width
imagedimensions=[100,100]';
M1=10;
M2=10;
deltax1=imagedimensions(1)/M1;
deltax2=imagedimensions(2)/M2;
x1=imageposition(1)+deltax1/2:deltax1:imageposition(1)+M1*deltax1-deltax1/2;
x2=imageposition(2)+deltax2/2:deltax2:imageposition(2)+M2*deltax2-deltax2/2;

H=zeros(N_R,N_T*N_S);
P=struct('signal',@signal);
Y=zeros(2,N_R);
X=zeros(2,N_T);
s=1;
l=1;
j=1;
%transmitters
X(:,1)=[-100,100]';
P(1).signal=@signal;

X(:,2)=[-90,100]';
P(2).signal=@signal;


X(:,3)=[-70,100]';
P(3).signal=@signal;


X(:,4)=[-60,100]';
P(4).signal=@signal;


X(:,5)=[-50,100]';
P(5).signal=@signal;

X(:,6)=[-40,100]';
P(6).signal=@signal;


X(:,7)=[-30,100]';
P(7).signal=@signal;


X(:,8)=[-20,100]';
P(8).signal=@signal;

X(:,9)=[-10,100]';
P(9).signal=@signal;

X(:,10)=[0,100]';
P(10).signal=@signal;


%receivers.  only need to define position
Y(:,1)=[0,-100]';
Y(:,2)=[10,100]';
Y(:,3)=[20,100]';
Y(:,4)=[30,100]';
Y(:,5)=[40,100]';
Y(:,6)=[50,100]';
Y(:,7)=[60,100]';
Y(:,8)=[70,100]';
Y(:,9)=[80,100]';
Y(:,10)=[90,100]';
%scatterers
Z=zeros(2,K);
Z(:,1)=[0,0]';
%reflectivities
v=[-1000,-100,-100,-100,-100,-100,-100,-100,-100,-100];
H_f=zeros(N_R,N_T);

m1phaseinteger=10;
m2phaseinteger=10;
m1start=m1phaseinteger*M1;
m2start=m2phaseinteger*M2;

VCShat=zeros(N_R,2*N_S*N_T);
%this is our estimated image
vhat=zeros(M1,M2);
XI1i=m1start:1/(M1*deltax1):m1start+(M1-1)/(M1*deltax1);
XI2i=m2start:1/(M2*deltax2):m2start+(M2-1)/(M2*deltax2);
XI1=zeros(N_R,2*N_S*N_T);
XI2=zeros(N_R,2*N_S*N_T);


%generate sampled channel matrix
%go through every sample frequency
while s<=N_S
    
    %generate the channel for a fixed frequency.
        j=1;
        while j<=N_R
            l=1;
            while l<=N_T
                jlthentry=0;
                k=1;
                while k<=K
                    jlthentry=jlthentry+exp(-1i*w(s)*(norm(Y(:,j)-Z(:,k))+norm(X(:,l)-Z(:,k)))/c_0)*v(k)/(norm(Y(:,j)-Z(:,k))*norm(X(:,l)-Z(:,k)));
                    k=k+1;
                end
                H_f(j,l)=jlthentry;
                l=l+1;
            end
            j=j+1;
        end
    H_f=-H_f/((w(s)^2)*(4*pi)^2);
    
    H(:,((s-1)*N_T+ 1):((s-1)*(N_T)+N_T))=H_f;
    s=s+1;
end

'generated the channel matrix.'
%estimate image
m1=1;
while m1<=M1
    m2=1;
    while m2<=M2
        s=1;
        while s<=N_S
            %remove constant and use it to get V, then estimate v using sum
            j=1;
            while j<=N_R
                l=1;
                    while l<=N_T
                        H_f=H(:,((s-1)*N_T+ 1):((s-1)*(N_T)+N_T));
                        
                        
                        
                        jlthconstant=H_f(j,l);
                        jlthconstant=conj(jlthconstant)*(4*pi)^2*norm(Y(:,j))*norm(X(:,l))*exp(1i*w(s)*(norm(Y(:,j))+norm(X(:,j))))/(w(s))^2;
                        %size(X(:,l)/norm(X(:,l))+Y(:,j)/norm(Y(:,j)))
                        %size([x1(m1),x2(m2)]')
                        vhat(m1,m2)=vhat(m1,m2)+jlthconstant*exp(1i*w(s)/c_0*dot(X(:,l)/norm(X(:,l))+Y(:,j)/norm(Y(:,j)),[x1(m1),x2(m2)]'));
                        l=l+1;
                    end
                j=j+1;
            end
        s=s+1;
        end
        
    m2=m2+1;  
    end
    m1=m1+1;
    M1-m1
end

%extract fourier data while using realness property of v
% s=1;
% while s<=N_S
% 	j=1;
% 		while j<=N_R
% 			l=1;
% 			while l<=N_T
% 			
% 			VCShat(j,N_S*N_R+(s-1)*N_T+l)=conj(H(j,(s-1)*N_T+l))*(4*pi)^2*norm(X(l))*norm(Y(j))*exp(1i*w(s)*(norm(X(l))+norm(Y(j))))/(w(s)^2);
%                 
%             VCShat(j,(s-1)*N_T+l)=conj(VCShat(j,N_S*N_R+(s-1)*N_T+l));
%             XI=w(s)*(X(l)/norm(X(l))+receivers(1,j)/norm(receivers(j)))/c_0;
%             XI1(j,(s-1)*N_T+l)=XI(1);
%             XI2(j,(s-1)*N_T+l)=XI(2);
%                 
%             l=l+1;
% 			end
% 			j=j+1;
% 		end
% 		s=s+1;
% end
% 
% 
% %interpolate VCShat to estimate VD    
% %interpolate complex part;
% VDhat=interp2d(XI1,XI2,real(VCShat),XI1i,XI2i)+1i*interp2d(XI1,XI2,imaginary(VCShat),XI1i,XI2i);
% 
% %recover image using 2d ifft on VDhat
% n1=1;
% while n1<M1
%     n2=1;
%     while n2<M2
%         
%         %for each pixel find perform idft
%         m1=1;
%         while m1<M1
%             m2=1;
%             while m2<M2
%                         vhat(n1,n2)=vhat(n1,n2)+VDhat(m1,m2)*exp(2*pi*1i*(n1*m1/M1+n2*m2/M2));
%                 m2=m2+1;
%             end
%            m1=m1+1; 
%         end
%         n2=n2+1;
%     end
%    n1=n1+1; 
% end




%plot image
imagesc(x1,x2,real(vhat));

xlabel('x1 position (meters)');
ylabel('x2 position');
