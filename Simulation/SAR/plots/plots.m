%takes in an array of antennas, the number of antennas, the center of the
%plot, the time, the size of the plot, and the resolution of the plot.
function plots(antennas, N,center, t,length, res)

x=(center(1)-length/2):res:(center(1)+length/2);
y=(center(2)-length/2):res:(center(2)+length/2);
z=(center(3)-length/2):res:(center(3)+length/2);
I=zeros(size(x),size(y));

i=1;
j=1;
stopi=size(x);
stopj=size(y);
time=0;
 while i<=stopi(2)
     j=1;
     while j<=stopj(2)
   
     I(i,j)=poweratpoint([x(i) y(j) center(3)],t,antennas,N);
       j=j+1;
     end
     
       i=i+1;
       time=time+1;
       if(time>stopi(2)/10)
           percentdone=(i/stopi(2))*100
           time=0;
       end
 end


    imagesc(x,y,(real(I)));
   
    set(gca,'YDir','normal')
   %plot for the complex part of the signal
%    figure
%    imagesc(x,y,(imag(I)));
end