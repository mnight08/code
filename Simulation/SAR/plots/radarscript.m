clear all

hold off
c0=3e8;
t=0;

length=1e-2
resolution=5e-5

center=[0 0 0]

%set the number of antennas
N=10;

numsteps= N*(length/resolution)^2
antennas(N,1)=struct('pos',[0 0 0],'cf',1e9);

%assign values to the ten antennas positions and carrier frequency
antennas(1).pos=[.001 .001 0];
antennas(1).cf=1e12;
 
 antennas(2).pos=[0 -.001 0];
 antennas(2).cf=1e12;
 
 antennas(3).pos=[-.001 -.003 0];
 antennas(3).cf=1e11;
 
 antennas(4).pos=[.002 .002 0];
 antennas(4).cf=1e12;
 
 antennas(5).pos=[0 -.002 0];
 antennas(5).cf=1e12;
 
 antennas(6).pos=[-.002 0 0];
 antennas(6).cf=1e11;
 
 antennas(7).pos=[.002 0 0];
 antennas(7).cf=1e12;
 
 antennas(8).pos=[0 -.003 0];
 antennas(8).cf=1e12;
 
 antennas(9).pos=[-.003 0 0];
 antennas(9).cf=1e11;
 
 antennas(N).pos=[.001 0 0];
 antennas(N).cf=1e11;
 


plots(antennas,N,center, t,length, resolution);
