function incident=incidentfield(antennaarray,numberofantennas,point,time)
c_0=3*10^8;
l=1;
incident=0;
	while l<=numberofantennas
		incident=incident+antennaarray(l).signal(time-norm((antennaarray(l).position-point)/c_0))/(4*pi*norm(antennaarray(l).position-point));
		l=l+1;
	end

end