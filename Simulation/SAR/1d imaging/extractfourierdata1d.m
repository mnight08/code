%take the channel matrix estimates and map them onto a grid for use by
%ifft2.  what we do is get as much fourier data as we can, then we
%interpolate it, and use that to produce the output F.
function F=extractfourierdata1d(transmitters,recievers,N_T,N_R,H,N_S,B,f_c)
F=zeros(N_S*N_T*N_R,1);
W=zeros(N_S*N_T*N_R,1);
c_0=3*10^8;


%interpolate real part

%interpolate complex part

s=1;
while s<=N_S
    j=1;
    f=f_c-B/2+(s-1)*B/N_S;
    while j<=N_R
        l=1;
        while l<=N_T
            W(N_R*N_T*(s-1)+N_T*(j-1)+l)=f*(transmitters(l).position+recievers(j).position)/c_0;
            l=l+1;
        end
        j=j+1;
    end
    s=s+1;
end

    

end