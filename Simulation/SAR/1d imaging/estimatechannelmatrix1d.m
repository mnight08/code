function H=estimatechannelmatrix1d(transmitters,N_T,N_R,data,N_S,B,f_c)
   transmitvector=zeros(N_T,1);

   H=zeros(N_R,N_T*N_S);
   s=1;
   while s<=N_S
        l=1;
        dataforfixedfrequency=data(:,s);
        signalsample=f_c-B/2+(s-1)*B/N_S;
        while l<=N_T
            transmitvector(l)=transmitters.fouriersignal(signalsample);
            l=l+1;
        end
        
        channelforfixedfrequency=dataforfixedfrequency/transmitvector;

        H(:,((s-1)*N_T+ 1):((s-1)*(N_T)+N_T))=channelforfixedfrequency;
        s=s+1;
   end
          
end