function out=derivative(f,x,h,i)
    m=0;
    sum=0;
    while m<=i
        sum=sum+factorial(i)/(factorial(i-m)*factorial(m))*(-1)^(i-m)*f(x+h*m);
        m=m+1;
    end
    out=sum/h^i;
end