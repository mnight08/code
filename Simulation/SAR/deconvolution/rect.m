function out =rect(t)
N=size(t);
N=N(2);
out=zeros(N,1);
i=1;
while i<=N
    
if( t(i)<-1/2 ||t(i)>1/2)
    out(i)=0;
else 
    out(i)=1;
end
i=i+1;
end

end