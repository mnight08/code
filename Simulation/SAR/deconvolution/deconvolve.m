%find approximate deconvolution of sampled function
%assumes equation is of form h=f*g, then returns an approximation 
%of f.  g and h should be functions. t should be a vector of sampled times
%to evaluate f at
%
%

function f=deconvolve(h,g,t)


start=-100000;
stop=100000;

M=100;
N=size(t)
N=N(1);
tau=start:(stop-start)/N:stop-(stop-start)/N;
G=zeros(M*N,N);
H=zeros(M*N,1);
i=1;
while i<=M*N
    j=1;

    H(i)=derivative(h,t(i),.0001,floor(i/N));
    while j<=N
        

    G(i,j)=g(t(i)-tau(j));
    
    j=j+1;
    end
    i=i+1
end



f=G\H;

end
