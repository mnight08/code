function out=triangle(t)
N=size(t);
N=N(2);
out=zeros(N,1);
i=1;
while i<=N
if t(i)>=0
    out(i)=1-t(i);
    
else 
    out(1)=t(i)+1;
    
end
i=i+1;
end
end