clear all
close all
warning off


%define constants
c_0=3*10^8;


'defining parameters'
%define parameters
range=100;
number_of_time_samples=1;
number_of_antenna_locations=20;
number_of_azimuth_samples=number_of_antenna_locations;%/2;
number_of_elevation_samples=1;%number_of_antenna_locations/2;
number_of_frequencies=100;
%B=10^12;
f_i=c_0/4;%3*10^8+B/2+1;

%make set of directions at given range
azimuth_angles=0:2*pi/number_of_azimuth_samples:2*pi-2*pi/number_of_azimuth_samples;
elevation_angles=pi/2.5;%0:(pi/2)/number_of_elevation_samples:pi/2-(pi/2)/number_of_elevation_samples;

antenna_directions=zeros(3,number_of_antenna_locations);
frequencies=f_i:f_i:(number_of_frequencies)*f_i%-B/2+f_c:B/number_of_frequencies:f_c+B/2-B/number_of_frequencies;
time_samples=0:100/number_of_time_samples:100-1/number_of_time_samples;
n=1;
i=1;
while i<=number_of_azimuth_samples
    j=1;
    while j<=number_of_elevation_samples
        antenna_directions(1,n)=range*sin(elevation_angles(j))*cos(azimuth_angles(i));
        antenna_directions(2,n)=range*sin(elevation_angles(j))*sin(azimuth_angles(i));
        antenna_directions(3,n)=range*cos(elevation_angles(j));
        n=n+1;
        j=j+1;
    end
    
    i=i+1;
end

%make the grid to place the image on
%image reconstruction
%scene dimensions in meters
image_width=500;
image_height=500;
%meters per pixel
image_resolution=50;
image_center=[0,0]';

%gives the positions of the i, j th 
imaging_grid=zeros(2,image_width/image_resolution,image_height/image_resolution);
i=1;
while i<=image_width/image_resolution
    j=1;
    while j<=image_height/image_resolution
        imaging_grid(1,i,j)=image_center(1)-image_width/2+(i-1)*image_resolution+image_resolution/2;
        imaging_grid(2,i,j)=image_center(2)-image_height/2+(j-1)*image_resolution+image_resolution/2;  
        j=j+1;
    end
    i=i+1;
end


%define the reflectivity function
reflectivity=zeros(image_width/image_resolution,image_height/image_resolution);



%hardcoded 10x10 smiley face reflectivity function is below, 
% reflectivity(2,3)=10^-9;
% reflectivity(2,6)=10^-9;
% 
% reflectivity(3,2)=10^-9;
% reflectivity(3,3)=10^-9;
% reflectivity(3,4)=10^-9;
% reflectivity(3,7)=10^-9;
% 
% reflectivity(4,3)=10^-9;
% reflectivity(4,8)=10^-9;
% 
% reflectivity(5,8)=10^-9;
% 
% reflectivity(6,8)=10^-9;
% 
% reflectivity(7,3)=10^-9;
% reflectivity(7,8)=10^-9;
% 
% reflectivity(8,2)=10^-9;
% reflectivity(8,3)=10^-9;
% reflectivity(8,4)=10^-9;
% reflectivity(8,7)=10^-9;
% 
% reflectivity(9,3)=10^-9;
% reflectivity(9,6)=10^-9;

% reflectivity((image_width/image_resolution)/2-2,(image_height/image_resolution)/2)=-10^3;
% reflectivity((image_width/image_resolution)/2+2,(image_height/image_resolution)/2)=-10^3;
%point target
reflectivity((image_width/image_resolution)/2,(image_height/image_resolution)/2)=-10^3;


'simulating data'
%forward model
DATA=zeros(number_of_antenna_locations,number_of_frequencies,number_of_time_samples);

k=1;
while k<=number_of_time_samples
    i=1;
    while i<=number_of_antenna_locations
        j=1;
        while j<=number_of_frequencies
            DATA(i,j,k)=scattered_field(frequencies(j),antenna_directions(i),time_samples(k),image_width,image_height,image_resolution,imaging_grid,reflectivity);        
        	j=j+1;
        end
        i=i+1; 
        number_of_antenna_locations-i
    end
k=k+1;
end

%average the fourier data
'averaging data'
AVERAGED_FOURIER_DATA=zeros(number_of_antenna_locations,number_of_frequencies);
k=1;
while k<=number_of_time_samples
		
i=1;
while i<=number_of_antenna_locations
	j=1;
	while j<=number_of_frequencies	AVERAGED_FOURIER_DATA(i,j)=AVERAGED_FOURIER_DATA(i,j)+DATA(i,j,k)*(4*pi)^2*norm(antenna_directions(i))^2*exp(-1i*2*pi*frequencies(j)/c_0*(time_samples(k)-2*norm(antenna_directions(i))));
			AVERAGED_FOURIER_DATA(i,j)=AVERAGED_FOURIER_DATA(i,j)*-(2*pi*frequencies(j))^2/number_of_time_samples;
		j=j+1;
	end
	i=i+1;
end
k=k+1;
end

'ploting origional image'
plot_origional_image(image_width,image_height,image_resolution,imaging_grid,reflectivity);

'reconstructing image'
recovered_reflectivity=zeros(image_width/image_resolution,image_height/image_resolution);
%make a note sometime on numerical integration. should be integrating over -infinity to infinity, so i should make a 
%a change of variables to make this proper.
%just directly evaluate the integral by summation
%for each fixed point on our image grid, we evaluate the Fourier inverse of
%our 3-d data
%the reflectivity at a point is given by the Fourier transform integral
%over volume exp( 2 * pi * z dot w ) V'( w ) dz 
z1=1;
while z1<=image_width/image_resolution
	z2=1;
	while z2<=image_height/image_resolution
			w1=1;
			while w1 <= number_of_antenna_locations
				w2=1;
				while w2<= number_of_frequencies
					%recovered_reflectivity(z1,z2)
					recovered_reflectivity(z1,z2)=recovered_reflectivity(z1,z2)+exp(1i*2*pi*2*frequencies(w2)/c_0*(imaging_grid(1,z1,z2)*antenna_directions(1,w1)+imaging_grid(2,z1,z2)*antenna_directions(2,w1)))*AVERAGED_FOURIER_DATA(w1,w2);
					w2=w2+1;
                end
				w1=w1+1;
            end
    
		z2=z2+1;
    end
    image_width/image_resolution-z1
    z1=z1+1;
    
end

'now building image'
Reconstructed_image=zeros(image_width/image_resolution,image_height/image_resolution,3);
z1=1;
while z1<=image_width/image_resolution
    z2=1;
    while z2<=image_width/image_resolution
       Reconstructed_image(z1,z2,1)=norm(recovered_reflectivity(z1,z2));
       Reconstructed_image(z1,z2,2)=Reconstructed_image(z1,z2,1);
       Reconstructed_image(z1,z2,3)=Reconstructed_image(z1,z2,1);
       z2=z2+1; 
    end
    
    image_width/image_resolution-z1

    z1=z1+1;
end


plot_image(Reconstructed_image,image_width,image_height,image_resolution,imaging_grid)
%convert reflectivity into image
colormap(hot);
set(gca,'YDir','normal');
colorbar


