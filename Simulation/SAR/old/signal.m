function out=signal(t,f)
out=exp(-2*pi*1i*f*t);
end