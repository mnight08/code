%define physical constants
c_0=3*10^8;
res=.2;
center=[0 0 0];
length=30;

x=(center(1)-length/2):res:(center(1)+length/2);
y=(center(2)-length/2):res:(center(2)+length/2);
%z=(center(3)-length/2):res:(center(3)+length/2);




numberofantennas=2;
numberofscatterers=5;
numberofrecievers=1;

antennaarray=struct('signal',@sin,'carrier',0,'position',[0,0,0]);
scattererarray=struct('simga',0,'position',[0,0,0]);
recieverarray=struct('position',[0,0,0]);


%i need to define separate functions for each signal
%first class functions are called function handles in matlab
antennaarray(1).signal=@sin;
antennaarray(1).carrier=10^8;
antennaarray(1).position=[1,100,0];

antennaarray(2).signal=@cos;
antennaarray(2).carrier=10^7;
antennaarray(2).position=[-1,100,0];

scattererarray(1).sigma=1;
scattererarray(1).position=[0,0,0];

scattererarray(2).sigma=1;
scattererarray(2).position=[9,0,0];

scattererarray(3).sigma=1;
scattererarray(3).position=[-9,0,0];

scattererarray(4).sigma=1;
scattererarray(4).position=[0,9,0];

scattererarray(5).sigma=1;
scattererarray(5).position=[0,-9,0];

xsize=size(x);
xsize=xsize(2);
ysize=size(y);
ysize=ysize(2);

Incident=zeros(xsize,ysize);
Scattered=zeros(xsize,ysize);
Total=zeros(xsize,ysize);

i=1;
j=1;
noisescale=20;
stopi=size(x);
stopj=size(y);
time=1;
stepcount=1;
%generate a view of the scattered incident, and total field.
 while i<=stopi(2)
     j=1;
     while j<=stopj(2)
		point=[x(i) y(j) center(3)];
		Incident(i,j)=incidentfield(antennaarray,numberofantennas,point,time);
		Scattered(i,j)=scatteredfield(antennaarray,numberofantennas,scattererarray,numberofscatterers,point,time);
		Total(i,j)=Incident(i,j)+Scattered(i,j);
		j=j+1;
        stepcount=stepcount+1;
     end
     
       i=i+1;

       if(stepcount>stopi(2)/10)
           percentdone=(i/stopi(2))*100
           stepcount=0;
       end
 end


%SampledRecievedData=SimulateAndSampleData(antennaarray,numberofantennas,recieverarray,numberofrecievers,scattererarray,numberofscatterers);


%SampledSignal=SampleSignalArray(antennaarray,numberofantennas, spacesamplepoints, timesamplepoints, spacesamplepoints);



%Estimator=SampledRecievedData*SampledSignal'\(SampledSignal*SampledSignal');


%Use the estimator to recover image using derived form for the channel matrix.  
%ExtractImage returns the average of image values created by 
%ImageArray=ExtractImageArray(Estimator,antennaarray,numberofantennas,recieverarray,numberofrecievers,samplepoints,numberofsamples);
%Image=ExtractImage(ImageArray,antennaarray,numberofantennas,recieverarray,numberofrecievers,samplepoints,numberofsamples);
%plot the recovered image.
%imagesc(Image);
    imagesc(x,y,(real(Incident)));
           xlabel('X (meters)');
    ylabel('Y (meters)');
    title('interference pattern for a set of antennas');
    set(gca,'YDir','normal')
      set(gca,'XDir','normal')
    figure
    imagesc(x,y,real(Scattered));
           xlabel('X (meters)');
    ylabel('Y (meters)');
    title('scattered field');
    set(gca,'YDir','normal')
      set(gca,'XDir','normal')
   figure 
   imagesc(x,y,real(Total));
       xlabel('X (meters)');
    ylabel('Y (meters)');
    title('total field');
    set(gca,'YDir','normal')
      set(gca,'XDir','normal')




