clear all

hold off
c0=3e8;
t=2;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%smiley face
%set the number of antennas
% center=[0 0 0]
% length=1e-2
% resolution=3e-5
% numberincircle=16;
% N=numberincircle+7;
% antennas(N,1)=struct('pos',[0 0 0],'cf',1e9);
% 
% theta=0;
% i=1;
% r=.005;
% h=2*pi/numberincircle;
% while theta<=2*pi
%     antennas(i).pos=[r*cos(theta) r*sin(theta) 0]
%     antennas(i).cf=1e12;
% 
%     i=i+1;
%     theta=theta+h;
% end
% antennas(numberincircle+1).pos=1e-3*[2 -2 0];
% antennas(numberincircle+1).cf=1e12;
% antennas(numberincircle+2).pos=[2e-3 2e-3 0];
% antennas(numberincircle+2).cf=1e12;
% antennas(numberincircle+3).pos=1e-3*[-1 -2 0];
% antennas(numberincircle+3).cf=1e12;
% antennas(numberincircle+4).pos=1e-3*[-2 -1 0];
% antennas(numberincircle+4).cf=1e12;
% antennas(numberincircle+5).pos=1e-3*[-2 0 0];
% antennas(numberincircle+5).cf=1e12;
% antennas(numberincircle+6).pos=1e-3*[-2 1 0];
% antennas(numberincircle+6).cf=1e12;
% antennas(numberincircle+7).pos=1e-3*[-1 2 0];
% antennas(numberincircle+7).cf=1e12;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%smiley face





%%%%%%%%%%%%%%%%%%%%%%%%%line pattern
% center=[0 0 0]
% length=1e-2
% resolution=3e-5
% N=9;
% antennas(N,1)=struct('pos',[0 0 0],'cf',1e9);
% 
% antennas(1).pos=[-.004 0 0];
% antennas(1).cf=1e12;
% 
% antennas(2).pos=[-.003 0 0];
% antennas(2).cf=1e11;
% 
% antennas(3).pos=[-.002 0 0];
% antennas(3).cf=1e12;
%  
%  antennas(4).pos=[-.001 0 0];
% antennas(4).cf=1e11;
% 
% antennas(5).pos=[0 0 0];
% antennas(5).cf=1e12;
% 
% antennas(6).pos=[.001 0 0];
% antennas(6).cf=1e11;
% 
% antennas(7).pos=[.002 0 0];
% antennas(7).cf=1e12;
% 
% antennas(8).pos=[.003 0 0];
% antennas(8).cf=1e11;
% 
% 
% antennas(9).pos=[.004 0 0];
% antennas(9).cf=1e12;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%line pattern

%%%%%%%%%%%%%%%%%%%%%%%%%line pattern raised
% center=[0 0 1000]
% length=1e2
% resolution=3e-1
% N=9;
% antennas(N,1)=struct('pos',[0 0 0],'cf',1e9);
% 
% antennas(1).pos=[-.004 0 0];
% antennas(1).cf=1e12;
% 
% antennas(2).pos=[-.003 0 0];
% antennas(2).cf=1e11;
% 
% antennas(3).pos=[-.002 0 0];
% antennas(3).cf=1e12;
%  
%  antennas(4).pos=[-.001 0 0];
% antennas(4).cf=1e11;
% 
% antennas(5).pos=[0 0 0];
% antennas(5).cf=1e12;
% 
% antennas(6).pos=[.001 0 0];
% antennas(6).cf=1e11;
% 
% antennas(7).pos=[.002 0 0];
% antennas(7).cf=1e12;
% 
% antennas(8).pos=[.003 0 0];
% antennas(8).cf=1e11;
% 
% 
% antennas(9).pos=[.004 0 0];
% antennas(9).cf=1e12;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%line pattern raised


%%%%%%%%%%%%%%%%%%%%%%%%%3 above, 3 below pattern raised
% center=[0 0 0]
% length=1e-2
% resolution=3e-5
% N=6;
% antennas(N,1)=struct('pos',[0 0 0],'cf',1e9);
% 
% antennas(1).pos=[-.001 0 .001];
% antennas(1).cf=1e12;
% 
% antennas(2).pos=[0 0 .001];
% antennas(2).cf=1e12;
% 
% antennas(3).pos=[.001 0 .001];
% antennas(3).cf=1e12;
%  
%  antennas(4).pos=[-.001 0 -.001];
% antennas(4).cf=1e12;
% 
% antennas(5).pos=[0 0 -.001];
% antennas(5).cf=1e12;
% 
% antennas(6).pos=[.001 0 -.001];
% antennas(6).cf=1e12;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%in the middle of a cross pattern below pattern raised
% center=[0 0 0]
% length=4e-3
% resolution=1e-5
% N=6;
% antennas(N,1)=struct('pos',[0 0 0],'cf',1e9);
% 
% antennas(1).pos=[-.001 0 0];
% antennas(1).cf=1e12;
% 
% antennas(3).pos=[.001 0 0];
% antennas(3).cf=1e12;
% 
% antennas(2).pos=[0 0 .001];
% antennas(2).cf=1e12;
% 
% antennas(5).pos=[0 0 -.001];
% antennas(5).cf=1e12;
% 
% antennas(4).pos=[ 0 -.001 0];
% antennas(4).cf=1e12;
% 
% antennas(6).pos=[ 0 .001 0];
% antennas(6).cf=1e12;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%wedge pattern
% center=[0 0 0]
% length=1e-2
% resolution=1e-5
% N=5;
% antennas(N,1)=struct('pos',[0 0 0],'cf',1e9);
% 
% antennas(1).pos=[0 .001 0];
% antennas(1).cf=1e12;
% 
% antennas(3).pos=[0 -.001 0];
% antennas(3).cf=1e12;
% 
% antennas(2).pos=[.001 0 0];
% antennas(2).cf=1e12;
% 
% antennas(4).pos=[ -.001 .0015 0];
% antennas(4).cf=1e12;
% 
% antennas(5).pos=[ -.001 -.0015 0];
% antennas(5).cf=1e12;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%







%%%%%%%%%%%%%%%%%%%%%%%%%box pattern
center=[0 0 0]
length=3e0
resolution=8e-3
N=9;
antennas(N,1)=struct('pos',[0 0 0],'cf',1e9);

antennas(1).pos=[1 -1 0];
antennas(1).cf=1e11;

antennas(2).pos=[1 0 0];
antennas(2).cf=1e11;

antennas(3).pos=[1 1 0];
antennas(3).cf=1e11;
 
 antennas(4).pos=[0 -1 0];
antennas(4).cf=1e11;

antennas(5).pos=[0 0 0];
antennas(5).cf=1e11;

antennas(6).pos=[0 1 0];
antennas(6).cf=1e11;

antennas(7).pos=[-1 -1 0];
antennas(7).cf=1e11;

antennas(8).pos=[-1 0 0];
antennas(8).cf=1e11;

antennas(9).pos=[-1 1 0];
antennas(9).cf=1e11;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%box pattern

numsteps= N*(length/resolution)^2
plots(antennas,N,center, t,length, resolution);
