%1-d imaging using channel matrix model

'defining parameters'
clear all
%define physical constants
c_0=3*10^8;
eps=1e-100;

%bandwidth, center frequency, scaling factor for matrix
B=6.4*10^6;
f_c=10^8;
scalingfactor=10^-8;

%image center,resolution, width, length, height
center=0;
res=.01;
length=10;


%define the number of transmitters, scatterers, and recievers
N_T=1;
K=length/res
N_R=1;
N_S=1000;
transmitters=struct('fouriersignal',@chirp,'carrier',0,'position',[0,0,0]);

'defining transmiters'
%define transmitters
transmitters(1).fouriersignal=@chirp;
transmitters(1).position=1000;

'defining recievers'
%define reciever at same position as transmitter
recievers=struct('position',0);

recievers(1).position=1000;

rangecellcenters=-length/2:res:length/2;

'generating data'
data=generatedata(transmitters,recievers, N_T, N_R, N_S,B,f_c);

%fix some rounding errors by matlab
if (K-ceil(K)) <.01
    K=ceil(K);
else
    K=floor(K);
end

'filling matrix'
H=estimatechannelmatrix1d(transmitters,N_T,N_R,data,N_S,B,f_c);

F=extractfourierdata1d(transmitters,recievers,N_T,N_R,H,N_S);

V=ifft2(fftshift(F));

plot(V);