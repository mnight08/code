%take the channel matrix estimates and map them onto a grid for use by
%ifft2.  what we do is get as much fourier data as we can, then we
%interpolate it, and use that to produce the output F.
function F=extractfourierdata1d(transmitters,receivers,N_T,N_R,H,N_S,B,f_c)
	F=zeros(1,2*N_S*N_T*N_R);
	
	Y=zeros(1,N_S*N_T*N_R);
	X=zeros(1,N_S*N_T*N_R);


	c_0=3*10^8;
	N=N_S*N_R*N_T;

	
	%take the channel matrix real and imaginary parts and put them in vectors RE,IM along with their position 
	%in fourier space in X.  These will be latter used to interpolate the fourier space
	s=1;
	n=1;
	while s<=N_S
		j=1;
		f=f_c-B/2+(s-1)*B/N_S;
		fouriermatrix=scalechannelmatrix(transmitters,receivers,N_T,N_R,H(:,((s-1)*N_T+ 1):((s-1)*(N_T)+N_T)),2*pi*f);
		while j<=N_R
			l=1;
			while l<=N_T
				X(n)=f*(transmitters(l).position+receivers(j).position)/c_0;
				Y(n)=fouriermatrix(j,l);
				
				l=l+1;
				n=n+1;
			end
			j=j+1;
		end
		s=s+1;
    end

    length=max(X)-min(X);
    res=length/N_S*N_R*N_T;
	XI=min(X):res:max(X)-res;
    interpcomplex1d(X,Y,XI);
	%fill in the upper half of the fourier data with the real and complex parts we interpolated.
	F((N_S*N_T*N_R+1):2*N_S*N_T*N_R)=interpcomplex1d(X,Y,XI);
	
	%use the fact that the scene is real to double the fourier coverage by making F(-n)=F(n)
    %ifft tests X to see whether vectors in X along the active dimension
    %are conjugate symmetric. If so, the computation is faster and the output is real. 
    %An N-element vector x is conjugate symmetric if x(i) = conj(x(mod(N-i+1,N)+1)) for each element of x. 
	%see:  http://www.mathworks.com/help/techdoc/ref/ifft.html
    n=1;
	while n<=N
		F(n)=conj(F(mod(2*N-n+1,2*N)+1));
        n=n+1;
	end
	
end