function field=pointscatteredfield1d(transmitters,N_T,x,w)
K=3;
c_0=3*10^8;


%row vector containing the positions of scatterers along the y1 axis
positions=[-20,0,200];
sigma=[-100 -100 -1000];
field=0;
k=1;
while k<=K
    field=field + sigma(k)*w^2*exp(1i*w*(abs(x-positions(k)))/c_0)*incidentfield1d(transmitters,N_T,positions(k),w)/((4*pi)*abs(x-positions(k)));
    k=k+1;
end

field=-field;

end