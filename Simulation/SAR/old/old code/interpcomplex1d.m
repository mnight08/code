%performs complex interpolation for functions that have a real domain
function YI=interpcomplex1d(X,Y,XI)
	%interpolate the fouier data x and generate a sequence of equally spaced points XI of fourier space.
	%see http://www.mathworks.com/help/techdoc/ref/interp1.html for reference
	%interpolate complex part
	r=interp1(X,imag(Y),XI);
	%interp real part
	c=interp1(X,real(Y),XI);
	YI=r+1i*c;

	
	
end