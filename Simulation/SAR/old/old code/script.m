%1-d imaging using channel matrix model

'defining parameters'
clear all
%define physical constants
c_0=3*10^8;
eps=1e-100;

%bandwidth, center frequency, scaling factor for matrix
B=6.4*10^6;
f_c=10^9;
scalingfactor=10^-8;

%image center,resolution, width, length, height
center=0;
res=.01;
length=10;


%define the number of transmitters, scatterers, and recievers
N_T=2;
K=length/res
N_R=2;
N_S=300;
transmitters=struct('fouriersignal',@chirp,'position',0);

'defining transmiters'
%define transmitters
transmitters(1).fouriersignal=@chirp;
transmitters(1).position=367;

transmitters(1).fouriersignal=@chirp;
transmitters(1).position=987;

'defining recievers'
%define reciever at same position as transmitter
recievers=struct('position',0);

recievers(1).position=77;
recievers(1).position=335;

'generating data'
data=generatedata(transmitters,recievers, N_T, N_R, N_S,B,f_c);


'filling matrix'
H=estimatechannelmatrix1d(transmitters,N_T,N_R,data,N_S,B,f_c);

F=extractfourierdata1d(transmitters,recievers,N_T,N_R,H,N_S,B,f_c);

V=ifftn(fftshift(F));
size(V)
plot(V.*conj(V));