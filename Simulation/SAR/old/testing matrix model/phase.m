function out=phase(x)
c0=3*10^8;
  
out=exp((-2*pi*1i*abs(x)/c0)/(4*pi*abs(x)));

end