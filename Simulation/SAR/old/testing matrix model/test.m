function out = test( x,y,z )
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here
k=1;
j=1;
c_0=3*10^8;
t=0;
L=2;
K=1;
J=1;
l=1
transmiters=struct('signal',@sin,'carrier',0,'position',[0,0,0]);
cubes=struct('xmin',0,'xmax',0,'ymin',0,'ymax',0,'zmin',0,'zmax',0);
recievers=struct('position',[0,0,0]);

%i need to define separate functions for each signal
%first class functions are called function handles in matlab
transmiters(1).signal=@sin;
transmiters(1).carrier=10^8;
transmiters(1).position=[1,100,0];

transmiters(2).signal=@cos;
transmiters(2).carrier=10^7;
transmiters(2).position=[-1,100,0];

recievers(1).position=[0,0,1000];

cubes(1).xmin=-1;
cubes(1).xmax=1;
cubes(1).ymin=-1;
cubes(1).ymax=1;
cubes(1).zmin=-1;
cubes(1).zmax=1;


out = transmiters(l).signal(t*ones(size(x'))-(normofvectors(ones(size(x'))*transmiters(l).position-[x',y*ones(size(x')),z*ones(size(x'))])+normofvectors(ones(size(x'))*recievers(j).position-[x',y*ones(size(x')),z*ones(size(x'))]))/c_0).*exp(-1i*(transmiters(l).carrier/c_0)*(t*ones(size(x'))-(normofvectors(ones(size(x'))*transmiters(l).position-[x',y*ones(size(x')),z*ones(size(x'))])+normofvectors(ones(size(x'))*recievers(j).position-[x',y*ones(size(x')),z*ones(size(x'))]))/c_0))./(normofvectors(ones(size(x'))*transmiters(l).position-[x',y*ones(size(x')),z*ones(size(x'))]).*normofvectors(ones(size(x'))*recievers(j).position-[x',y*ones(size(x')),z*ones(size(x'))]))';

end

