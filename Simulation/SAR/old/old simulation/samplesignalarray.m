function sampled=SampleSignalArray(antennaarray,numberofantennas, spacesamplepoints, timesamplepoints,spacesamplepoints)
x=size(timesamplepoints);
x=x(2);

y=spacesamplepoints;
y=y(2);

sampled=zeros(x,y);

end