function sum= poweratpoint(x, t, antennas, N)
    sum=0;
    c0=3e8;
    i=1;
    while i<=N 
       sum = sum+signal(t-norm(x-antennas(i).pos)/c0,antennas(i).cf)/(4*pi*norm(x-antennas(i).pos)); 
       i=i+1;
    end
end