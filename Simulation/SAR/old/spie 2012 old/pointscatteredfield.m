function field=pointscatteredfield(transmitters,N_T,x,w)
K=1;
c_0=3*10^8;
k=1;
field=0;

positions=zeros(3,K);
sigma=zeros(K,1);
'define scatterer positions and reflectivities';
while k<=K
   positions(:,k)=[k,k,0]';
   sigma(k)=-10;
   k=k+1;
end

% 
% positions(:,1)=[-4,-4,0]';
% positions(:,2)=[4,-4,0]';
% positions(:,3)=[-4,4,0]';
% positions(:,4)=[4,4,0]';
% sigma(1)=100;
% sigma(2)=-100;
% sigma(3)=-100;
% sigma(4)=-100;

k=1;
while k<=K
    field=field + sigma(k)*w^2*exp(1i*w*(norm(x-positions(:,k)))/c_0)*incidentfield(transmitters,N_T,positions(:,k),w)/((4*pi)*norm(x-positions(:,k)));
    k=k+1;
end


end