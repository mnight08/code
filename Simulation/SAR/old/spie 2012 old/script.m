'defining parameters'
clear all
%define physical constants
c_0=3*10^8;
eps=1e-100;

%bandwidth, center frequency, scaling factor for matrix
B=6.4*10^6;
f_c=10^8;
scalingfactor=10^-8;
%image center,resolution, width, length, height
center=[0,0,0]';
res=.01;
width=10;
length=10;
height=res;


%define the number of transmitters, scatterers, and recievers
N_T=1;
K=length*width*height/res^3
N_R=10;
N_S=10;
transmitters=struct('fouriersignal',@chirp,'carrier',0,'position',[0,0,0]);

'defining transmiters'
%define transmitters
transmitters(1).fouriersignal=@chirp;
transmitters(1).position=[1,1000,0]';



'defining recievers'
%define recievers
recievers=struct('position',[0,0,0]');
j=1;
while j<=N_R
    recievers(j).position=[1000+j,1000+j,1000+j]';
    j=j+1;
end



[rangecellcenters,indices]=partitionscene(center,width,length,height,res);





'generating data'
data=generatedata(transmitters,recievers, N_T, N_R, N_S,B,f_c);



k=1;
j=1;



if (K-ceil(K)) <.01
    K=ceil(K);
else
    K=floor(K);
end

'filling matrix'
A=zeros(N_R*N_S,K);
s=1;
while s<=N_S
    j=1;
    frequencysamplepoint=f_c-B/2+s*B/N_S;
 while j<=N_R
     k=1;
     while k<=K
        if norm(incidentmatrixentry( transmitters,recievers,rangecellcenters,N_T, j, k, frequencysamplepoint,scalingfactor ))==Inf
        
        'infinity!!!!'
        
        else
         A((s-1)*N_R+j,k)=incidentmatrixentry( transmitters,recievers,rangecellcenters,N_T, j, k, frequencysamplepoint,scalingfactor );
         
        end
        k=k+1;
         
     end
     j=j+1;
 end
 s=s+1;
 N_S-s
end
size(data)
size(A)
'building image'
sigma=10^-2*data\A



'drawing image'

Image=zeros(width/res,length/res);
X=zeros(width/res,1);
Y=zeros(length/res,1);

count=1;
while count<=width*length/(res*res)
    i=indices(:,count);
    i=i(1);
    j=indices(:,count);
    j=j(2);
    Image(i,j)=sigma(count);
    count=count+1;
end   

imagesc(X,Y,real(Image));
