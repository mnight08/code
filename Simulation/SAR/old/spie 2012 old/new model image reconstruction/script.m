'defining parameters'
clear all
%define physical constants
c_0=3*10^8;


%bandwidth, center frequency, scaling factor for matrix
B=6.4*10^6;
f_c=10^8;
scalingfactor=10^-8;
%image center,resolution, width, length, height
center=[0,0,0]';
res=.2;
width=30;
length=30;
height=res;


%define the number of transmitters, scatterers, and recievers
N_T=1;
K=length*width*height/res^3
N_R=8;
N_S=K/N_R;
transmitters=struct('fouriersignal',@chirp,'carrier',0,'position',[0,0,0]);

'defining transmiters'
%define transmitters
transmitters(1).fouriersignal=@chirp;
transmitters(1).position=[1,1000,0]';

% transmitters(2).fouriersignal=@chirp;
% transmitters(2).carrier=10^7;
% transmitters(2).position=[-1,100,0]';

'defining recievers'
%define recievers
recievers=struct('position',[0,0,0]');
j=1;
while j<=N_R
    recievers(j).position=[1000+j,1000+j,1000+j]';
    j=j+1;
end
% recievers(1).position=[0,0,2000]';
% recievers(2).position=[0,10,2000]';
% recievers(3).position=[0,100,2000]';
% recievers(4).position=[0,1000,2000]';
% recievers(5).position=[-500,10,2000]';
% rangecellcenters=partitionscene(center,width,length,height,res);
plotpointscatteredfield(width,length,res,transmitters,N_T);
 
% plotscene(width,length,res);
%  
% figure;
% 'generating data'
% data=generatedata(transmitters,recievers, N_T, N_R, N_S,B,f_c);
% 
% 
% k=1;
% j=1;
% 
% 
% 
% if (K-ceil(K)) <.01
%     K=ceil(K);
% else
%     K=floor(K);
% end
% 
% 'filling matrix'
% A=zeros(N_R*N_S,K);
% s=1;
% while s<=N_S
%     j=1;
%     frequencysamplepoint=f_c-B/2+s*B/N_S;
%  while j<=N_R
%      k=1;
%      while k<=K
%         if norm(incidentmatrixentry( transmitters,recievers,rangecellcenters,N_T, j, k, frequencysamplepoint,scalingfactor ))==Inf
%         
%         'infinity!!!!'
%         
%         else
%          A((s-1)*N_R+j,k)=incidentmatrixentry( transmitters,recievers,rangecellcenters,N_T, j, k, frequencysamplepoint,scalingfactor );
%          
%         end
%         k=k+1;
%          
%      end
%      j=j+1;
%  end
%  s=s+1;
%  N_S-s
% end
% size(data)
% size(A)
% 'building image'
% sigma=10^-2*data\A;
% i=1;
% while i<=K
%     if sigma(i) ~=0
%         rangecellcenters(:,i)
%         
%     end
%    
%     i=i+1
% end
% K
% %sigma=data\A;
% 
% 'drawing image'
% i=1;
% Image=zeros(width/res,length/res);
% X=zeros(width/res,1);
% Y=zeros(length/res,1);
% 
% count=1;
% while i<=width/res
%     X(i)=rangecellcenters(1,floor((i-1)*length/res)+1);
%     j=1;
%     while j<=length/res
%         Y(j)=rangecellcenters(2,j);
%         Image(i,j)=norm(10^-7*data(floor((i-1)*width/res+j)));
%         j=j+1;
%     end
%     i=i+1;
% end
% % sigma
% imagesc(X,Y,Image);
