function plotscene(length,width,res)

X=-width/2:res:width/2;
Y=-length/2:res:length/2;
Image=zeros(width/res,length/res);
i=1;
xcount=size(X);
xcount=xcount(2);
ycount=size(Y);
ycount=ycount(2);
while i<=xcount
    j=1;
    while j<=ycount
        Image(i,j)=reflectivity([X(i),Y(j),0]');
        
        j=j+1;
    end
   i=i+1; 
end
imagesc(X,Y,Image);