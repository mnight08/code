function data=generatedata(transmitters,recievers,samplepoints,K, N_T, N_R, N_S,B,f_c)
data=zeros(N_R*N_S,1);

s=1;
while s<=N_S
    j=1;
    frequencysamplepoint=f_c-B/2+s*B/N_S;
    while j<=N_R

        
        data((s-1)*N_R+j)=scatteredfield(transmitters,N_T,samplepoints,K,recievers(j).position,frequencysamplepoint);
        
        j=j+1;
    end
    s=s+1;
end

end