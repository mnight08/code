'defining parameters'

%define physical constants
c_0=3*10^8;

%define the number of transmitters, scatterers, and recievers
N_T=2;
K=2;
N_R=2;
N_S=10;
%bandwidth, center frequency, scaling factor for matrix
B=6.4*10^6;
f_c=10^8;
scalingfactor=10^10;
%image center,resolution, width, length, height
center=[0,0,0]';
res=20;
width=100;
length=100;
height=20;

transmitters=struct('fouriersignal',@chirp,'carrier',0,'position',[0,0,0]);

'defining transmiters'
%define transmitters
transmitters(1).fouriersignal=@sin;
transmitters(1).carrier=10^8;
transmitters(1).position=[1,100,0]';

transmitters(2).fouriersignal=@cos;
transmitters(2).carrier=10^7;
transmitters(2).position=[-1,100,0]';

'defining recievers'
%define recievers
recievers=struct('position',[0,0,0]');

recievers(1).position=[0,0,20]';
recievers(2).position=[0,10,20]';

rangecellcenters=partitionscene(center,width,length,height,res);
data=generatedata(transmitters,recievers,rangecellcenters,width*length*height/(res*res*res), N_T, N_R, N_S,B,f_c);


k=1;
j=1;
K=length*width*height/res^3
N_R*N_S
A=zeros(N_R*N_S,K);
s=1;
while s<=N_S
    frequencysamplepoint=f_c-B/2+s*B/N_S;
 while j<=N_R
     k=1
     while k<=K
         
         A((s-1)*N_R+j,k)=incidentmatrixentry( transmitters,recievers,rangecells, j, k, frequencysamplepoint,scalingfactor )
         k=k+1;
         
     end
     j=j+1;
 end
        
end
