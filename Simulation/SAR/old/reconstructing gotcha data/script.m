%define physical constants
c_0=3*10^8;
t=0;

L=2;
K=2;
J=2;
scalingfactor=10^10;

transmiters=struct('signal',@sin,'carrier',0,'position',[0,0,0]);
cubes=struct('xmin',0,'xmax',0,'ymin',0,'ymax',0,'zmin',0,'zmax',0);
recievers=struct('position',[0,0,0]);

%i need to define separate functions for each signal
%first class functions are called function handles in matlab
transmiters(1).signal=@sin;
transmiters(1).carrier=10^8;
transmiters(1).position=[1,100,0];

transmiters(2).signal=@cos;
transmiters(2).carrier=10^7;
transmiters(2).position=[-1,100,0];

recievers(1).position=[0,0,1000];
recievers(2).position=[0,100,500];

cubes(1).xmin=-1;
cubes(1).xmax=1;
cubes(1).ymin=-1;
cubes(1).ymax=1;
cubes(1).zmin=-1;
cubes(1).zmax=1;


cubes(2).xmin=1;
cubes(2).xmax=2;
cubes(2).ymin=-1;
cubes(2).ymax=1;
cubes(2).zmin=-1;
cubes(2).zmax=1;

k=1;
j=1;



 A=zeros(J,K)
 while j<=J
     k=1
     while k<=K
         A(j,k)=incidentmatrixentry( transmiters,recievers,cubes, L, j, k, t,scalingfactor )
         k=k+1;
         
     end
     j=j+1;
 end
        