%this script demonstrates the cross coorelation of two sampled single variable signals.
%Matlab has an implemented function that is also used below.  The cross
%coorelation of two functions can be found here
%http://en.wikipedia.org/wiki/Cross-correlation

%first we need to get two signals to cross correlate.  For simplicity we
%use a cos and a sin.  These signals can be changed by changing the right 
%the function name after the @ symbol.  for example, if we have a matlab 
%function called chirp that takes as input a single scalar variable, then
%we can perform the coorelation of it and another signal by setting
%signal_1=@chirp;, and setting signal_2 in a similiar manner.
signal_1=@cos;
signal_2=@sin;
%to do any computation we need to sample our signals.  The number of
%samples for the two signals is denoted number_of_signal_samples
number_of_signal_samples=100;
%the  two vectors contains our sampled signals are denoted by
%sampled_signal_1 and sampled_signal_2.
sampled_signal_1=zeros(number_of_signal_samples,1);
sampled_signal_2=zeros(number_of_signal_samples,1);
signal_start_sample_point=-4*pi;
signal_stop_sample_point=4*pi;
signal_sample_points=signal_start_sample_point:(signal_stop_sample_point-signal_start_sample_point)/number_of_signal_samples:signal_stop_sample_point-(signal_stop_sample_point-signal_start_sample_point)/number_of_signal_samples;

i=1;
while i<=number_of_signal_samples
    sampled_signal_1(i)=signal_1(signal_sample_points(i));
    sampled_signal_2(i)=signal_2(signal_sample_points(i));
    i=i+1;
end

%The cross coorelation of two functions is a function of a single variable
%t.  So we can sample the correlation at different values of t. The number of 
%samples is given by number_of_coorelation_samples
number_of_coorelation_samples=100;
coorelation_start_sample_point=-4*pi;
coorelation_stop_sample_point=4*pi;
coorelation_sample_points=coorelation_start_sample_point:(coorelation_stop_sample_point-coorelation_start_sample_point)/number_of_coorelation_samples:coorelation_stop_sample_point-(coorelation_stop_sample_point-coorelation_start_sample_point)/number_of_coorelation_samples;
%First we perform the coorelation using the definition given on the wiki
%page, manually.  
%The vector that contains the samples of the cross coorelation function
%computed manually is called cross_coorelation_manual
cross_coorelation_manual=zeros(number_of_coorelation_samples,1);
%we assume that the signals are supported only on the interval that we
%sampled them over.  that is, if n+m > number_of_signal_samples, then      
%sampled_signal_1[n+m]=0 
n=1;
while n<=number_of_coorelation_samples
    m=1;
    while m<number_of_signal_samples
        if ~((n+m)>number_of_signal_samples)
            cross_coorelation_manual(n)=cross_coorelation_manual(n)+conj(sampled_signal_1(m))*sampled_signal_2(n+m);
        end
        m=m+1;
    end
    n=n+1;
end
%Then we use the built in matlab function.  we should get
%the same results.  To compare the two we get
cross_coorelation_matab=xcorr(sampled_signal_1,sampled_signal_2);

%now we plot the two on the same figure to compare them

hold on;
plot(cross_coorelation_manual,'red');
plot(cross_coorelation_matab(number_of_signal_samples:2*number_of_signal_samples-1),'blue');
plot(cross_coorelation_manual+cross_coorelation_matab(number_of_signal_samples:2*number_of_signal_samples-1),'green');
%turns out the manual implementation failed terrible.  dont know why.  

figure;
%the auto coorelation is the coorelation of a function with itself.  for
%example the autocoorelation of cos with itself is 
plot(xcorr(sampled_signal_1,sampled_signal_1),'blue');



%this simulation only covers single variable signals.  It should easily be
%extended to two or more signals in a way similar to how convolution is
%extended to multiple variables.  If i decide to add multi variable
%coorelation I will place it below.