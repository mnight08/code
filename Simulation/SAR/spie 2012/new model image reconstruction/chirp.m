function f=chirp(w)
f=exp(-1i*w^2);
end