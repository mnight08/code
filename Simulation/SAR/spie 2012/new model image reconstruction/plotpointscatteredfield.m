function plotpointscatteredfield(width,length,res,transmitters,N_T)
X=-width/2:res:width/2;
Y=-length/2:res:length/2;
xcount=floor(width/res);
ycount=floor(length/res);
Image=zeros(floor(width/res),floor(length/res));
samplepoints= 
i=1;
while i<=xcount
    j=1;
    while j<=ycount
        Image(i,j)=pointscatteredfield(transmitters,N_T,[X(i),Y(j),0]',10);
        j=j+1;
    end
    i=i+1;
    xcount-i
end
imagesc(X,Y,real(Image));
end