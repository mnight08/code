function rangecellcenters=partitionscene(center,width,length,height,res)
    count=1;
    xcount=width/res;
    ycount=length/res;
    zcount=height/res;
    start=center-[width/2,length/2,height/2]';
    rangecellcenters=zeros(3,xcount*ycount*zcount);
    i=1;
    while i<=xcount
        j=1;
        while j<=ycount
            k=1;
            while k<=zcount
                rangecellcenters(:,count)=start+[(i-1)*res+res/2,(j-1)*res+res/2,(k-1)*res+res/2]';
                count=count+1;
                k=k+1;
            end
            j=j+1;
        end
        i=i+1
    end
end