function field=pointscatteredfield(transmitters,N_T,x,w)
K=10;
c_0=3*10^8;
k=1;
field=0;

positions=zeros(3,K);
sigma=zeros(K,1);

while k<=K
   positions(:,k)=[k,k,k]';
   sigma(k)=-10;
   k=5*k+1;
end


k=1;
while k<=K
    field=field + sigma(k)*w^2*exp(1i*w*(norm(x-positions(:,k)))/c_0)*incidentfield(transmitters,N_T,positions(:,k),w)/((4*pi)*norm(x-positions(:,k)));
    k=k+1;
end


end