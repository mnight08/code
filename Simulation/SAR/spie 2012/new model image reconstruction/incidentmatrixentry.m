function entry = incidentmatrixentry( transmitters,recievers,rangecellcenters,N_T,j, k, w,scalingfactor )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    c_0=3*10^8;
    
    entry=-scalingfactor*w^2*exp(1i*w*norm(recievers(j).position-rangecellcenters(k))/c_0)*incidentfield(transmitters,N_T,rangecellcenters(k),w)/((4*pi)*norm(recievers(j).position-rangecellcenters(k)));
    
end

