function out=cube(pos,res)
%pos should be the center of the cube
    xmin=pos(1)-res/2;
    xmax=pos(1)+res;
    ymin=pos(2)-res/2;
    ymax=pos(2)+res;
    zmin=pos(3)-res/2;
    zmax=pos(3)+res/2;
    out=struct('xmin',xmin,'xmax',xmax,'ymin',ymin,'ymax',ymax,'zmin',zmin,'zmax',zmax,'sigma',0);
end