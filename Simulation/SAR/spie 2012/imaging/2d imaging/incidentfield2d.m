function incident=incidentfield2d(transmiters,N_T,x,w)
c_0=3*10^8;
l=1;
incident=0;
	while l<=N_T
		incident=incident+transmiters(l).fouriersignal(w)*exp(-1i*w*norm((x-transmiters(l).position)/c_0))/(4*pi*norm(x-transmiters(l).position));
		l=l+1;
	end
	
	incident=-1*incident;

end