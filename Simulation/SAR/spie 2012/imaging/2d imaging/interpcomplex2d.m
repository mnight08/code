%performs complex interpolation for functions that have a real domain
%X is a vector of X positions, Y of Y positions, and Z the function value at X,Y.  XI
%YI are the points where ZI will be sampled at
function ZI=interpcomplex1d(X,Y,Z,XI,YI)
	%interpolate the fouier data x and generate a sequence of equally spaced points XI of fourier space.
	%see http://www.mathworks.com/help/techdoc/ref/interp1.html for reference
	%interpolate complex part
	r=interp2(X,Y,imag(Z),XI,YI);
	%interp real part
	c=interp2(X,Y,real(Z),XI,YI);
	
	
	ZI=r+1i*c;
end