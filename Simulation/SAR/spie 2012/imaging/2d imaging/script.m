%2-d imaging using channel matrix model

'defining parameters'
clear all
%define physical constants
c_0=3*10^8;
eps=1e-100;

%bandwidth, center frequency, scaling factor for matrix
B=6.4*10^6;
f_c=10^8;
scalingfactor=10^-8;

%image center,resolution, width, length, height
center=0;
res=.01;
length=10;
widht=10;


%define the number of transmitters, scatterers, and recievers
N_T=1;
K=length*width/(res*res)
N_R=1;
N_S=1000;
transmitters=struct('fouriersignal',@chirp,'position',[0,0]);

'defining transmiters'
%define transmitters
transmitters(1).fouriersignal=@chirp;
transmitters(1).position=[1000,0];

'defining recievers'
%define reciever at same position as transmitter
recievers=struct('position',[0,0]);

recievers(1).position=[1000,0];

'generating data'
data=generatedata(transmitters,recievers, N_T, N_R, N_S,B,f_c);

'filling matrix'
H=estimatechannelmatrix2d(transmitters,N_T,N_R,data,N_S,B,f_c);

F=extractfourierdata2d(transmitters,recievers,N_T,N_R,H,N_S);

%figure out logic for 2d ifft.  I dont think there is an ifft2.
V=ifft2(fftshift(F));

plot(V);