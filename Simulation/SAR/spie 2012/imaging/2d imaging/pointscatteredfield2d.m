function field=pointscatteredfield2d(transmitters,N_T,x,w)
K=5;
c_0=3*10^8;
%row vector containing the positions of scatterers along the y1 axis
positions=[[-10,0], -[5,0], [0,0], [5,0],[10,0]];
sigma=[-1000,-200, -10,-200,-1000];
field=0;
k=1;
while k<=K
    field=field + sigma(k)*w^2*exp(1i*w*(abs(x-positions(k)))/c_0)*incidentfield1d(transmitters,N_T,positions(k),w)/((4*pi)*abs(x-positions(k)));
    k=k+1;
end

field=-field;

end