clear all;
close all;
hold off;
c0=3e8;
t=0;
length=5e-3
resolution=4e-5
numsteps= (length/resolution)^2
center=[0 0 0]
antennas=[struct('pos',[0 1e-3 0],'cf',1e5) struct('pos',[0 (-1e-3) 0],'cf',1e9) struct('pos',[0 0 (-1e-3)],'cf',1e12)];
N=size(antennas);
N=N(2)
plot(antennas,N,center, t,length, resolution);