clear all
warning off

fname = 'trihedral';  % The trihedral facet model is hard-coded in the "RaiderTracer" function.
maxBounce = 2;
nLevels = 3;
B = 2e9;
Nsamp = 256;
fo = 10e9 + linspace(-B/2,B/2,Nsamp).';
phio = 40*pi/180 + linspace(-10*pi/180,10*pi/180,Nsamp);
thetao = 40*pi/180*ones(size(phio));

% Generate data
[data] = RaiderTracer(fname,maxBounce,nLevels,fo,phio,thetao);

% Form SAR image - Provide your own SAR image formation code
ifig = 3;
useTayWt = 1;
osamp = 2;
[f,phi] = meshgrid(fo,phio);
theta = repmat(thetao.',1,size(f,2));
[x,y,img] = pfAlgorithm2D_exscribe(f,phi,theta,data.',useTayWt,osamp,ifig);
