function MIMORadar
close all;
clear all;

global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global sampNum;
global save_path
global waveform;
global scanSetOrg;
%save_path='E:/workspace/program/MIMO Radar/data';
savePath='E:/Document/lzxu/my program/MIMO Radar/data/'
%path(path,save_path);

txAntNum=10;
rxAntNum=10;
txDist=0.5*ones(1,txAntNum-1);%[0.5 1. 1.5 4];%
rxDist=0.5*ones(1,rxAntNum-1);%[0.5 1. 1.5 4];%
sampNum=256;
betaTrue=[4 4 1];
DOATrue=[-35 -25 -20];
compNum=length(DOATrue);
scanSetOrg=[-60:0.05:60];
SNRSetdB=[-40:5: 30]; %dB
SNRSet=10.^(SNRSetdB/20);
snrNum=length(SNRSetdB);

MonteCarloNum=500;

JammerAmp=1000; %dB
DOAJammer=0;

% waveform: hardmard codes
temp=[1];
for kk=1:ceil(log2(sampNum))
  temp=[temp temp; temp -1*temp];
end;
waveformSet=temp;
waveform=(waveformSet(1:txAntNum, 1:sampNum)+j*waveformSet(txAntNum+1:2*txAntNum, 1:sampNum))/sqrt(2);
%waveform=(( 2*rand(txAntNum, sampNum) -1) + j*(2*rand(txAntNum, sampNum) -1) )/sqrt(2);

% source generator
%------------------------------------
tmp=0.03*(randn(txAntNum, compNum) +j*randn(txAntNum, compNum) )/sqrt(2);
txTmp=cumsum([0 txDist]);
txStv=exp(-j*2*pi*txTmp' * sin(DOATrue*pi/180) );%  + 0.1*(randn(txAntNum, compNum) +j*randn(txAntNum, compNum) )/sqrt(2);
rxTmp=cumsum([0 rxDist]);
rxStv=exp(-j*2*pi*rxTmp' * sin(DOATrue*pi/180) );%  + 0.1*(randn(txAntNum, compNum) +j*randn(txAntNum, compNum) )/sqrt(2);

signalPure=(rxStv+tmp)*diag(betaTrue)*((txStv+tmp).'*waveform);

 % correlation matrix 
  Q_struct=zeros(rxAntNum,rxAntNum);
   for row_no=1:rxAntNum
      for col_no=1:rxAntNum
          Q_struct(row_no,col_no)=0.9^abs(col_no-row_no)*exp(j*pi/2*(col_no-row_no));
      end;
    end;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 AmpBiasDAS=zeros(snrNum,compNum);
 AmpBiasCapon=zeros(snrNum,compNum);
 AmpBiasAPES=zeros(snrNum,compNum);
 AmpBiasDGC=zeros(snrNum,compNum);    
  
 AmpMSEDAS=zeros(snrNum,compNum);
 AmpMSECapon=zeros(snrNum,compNum);
 AmpMSEAPES=zeros(snrNum,compNum);
 AmpMSEDGC=zeros(snrNum,compNum);
 for snrno=1:snrNum
     SNR=SNRSet(snrno);
     
     for mont=1:MonteCarloNum
         signal=signalPure +  sqrt(Q_struct)*(randn(rxAntNum, sampNum) +j *randn(rxAntNum, sampNum))/SNR/sqrt(2) ;
    % generator 
    rxTmp=cumsum([0 rxDist]);
    rxStvJammer=exp(-j*2*pi*rxTmp' * sin(DOAJammer*pi/180) );
    jammerWaveform=((2*randint(1,sampNum)-1)+j*(2*randint(1,sampNum)-1))/sqrt(2);
    signal=signal+rxStvJammer*jammerWaveform*JammerAmp;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % MIMO signal processing
    [AmpErr]=dasEst(signal,txStv,rxStv,betaTrue.');
    AmpBiasDAS(snrno,:)=AmpBiasDAS(snrno,:)+AmpErr;
    AmpMSEDAS(snrno,:)=AmpMSEDAS(snrno,:)+abs(AmpErr).^2;
    
    [AmpErr]=RobcaponEst(signal,txStv,rxStv,betaTrue.');
     AmpBiasCapon(snrno,:)=AmpBiasCapon(snrno,:)+AmpErr;
     AmpMSECapon(snrno,:)=AmpMSECapon(snrno,:)+abs(AmpErr).^2;
     
    [AmpErr]=RobAPESEst(signal,txStv,rxStv,betaTrue.');
     AmpBiasAPES(snrno,:)=AmpBiasAPES(snrno,:)+AmpErr;
     AmpMSEAPES(snrno,:)=AmpMSEAPES(snrno,:)+abs(AmpErr).^2;
    
    [AmpErr]=RobDGCEst(signal,txStv,rxStv,betaTrue.');
       AmpBiasDGC(snrno,:)=AmpBiasDGC(snrno,:)+AmpErr;
     AmpMSEDGC(snrno,:)=AmpMSEDGC(snrno,:)+abs(AmpErr).^2;
     
   end;
end;
AmpBiasDAS=real(AmpBiasDAS)/MonteCarloNum;
AmpBiasCapon=real(AmpBiasCapon)/MonteCarloNum;
AmpBiasAPES=real(AmpBiasAPES)/MonteCarloNum;
AmpBiasDGC=real(AmpBiasDGC)/MonteCarloNum;

AmpMSEDAS=AmpMSEDAS/MonteCarloNum;
AmpMSECapon=AmpMSECapon/MonteCarloNum;
AmpMSEAPES=AmpMSEAPES/MonteCarloNum;
AmpMSEDGC=AmpMSEDGC/MonteCarloNum;
 
  
  for k=1:compNum
         figure;
  plot( SNRSetdB,AmpBiasDAS(:,k),'--S',...
            SNRSetdB,AmpBiasCapon(:,k),'-->',...
            SNRSetdB,AmpBiasAPES(:,k), '--o',...
            SNRSetdB,AmpBiasDGC(:,k),'-');
      
  myboldify1;
  xlabel('SNR (dB)');
  ylabel('Bias')
  legend('LS','Capon','APES','AML',0);
  %axis([SNR_set_dB(1),SNR_set_dB(end),1e-5,1e-1]);    
  myboldify1;
   print('-deps',[savePath 'fig_BiasvsSNR_comp' num2str(k) '.eps']);
end;

 for k=1:compNum
      figure;
      semilogy( SNRSetdB,AmpMSEDAS(:,k),'--S',...
            SNRSetdB,AmpMSECapon(:,k),'-->',...
            SNRSetdB,AmpMSEAPES(:,k),'--o',...
            SNRSetdB,AmpMSEDGC(:,k),'-');
      
  myboldify1;
  xlabel('SNR (dB)');
  ylabel('MSE')
  legend('LS','Capon','APES','DGC-AML',0);
  %axis([SNR_set_dB(1),SNR_set_dB(end),1e-5,1e-1]);
  myboldify1;  
  print('-deps',[savePath 'fig_MSEvsSNR_' num2str(k) '.eps']);
end;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [AmpErr]=dasEst(signal,txStv,rxStv,beta)
global waveform;
        
compNum=length(beta);
AmpErr=zeros(1,compNum);
for compNo=1:compNum
        
 a=rxStv(:,compNo);
 s=txStv(:,compNo).'*waveform;
        
 xBar=signal*s';
 betaTmp=a'*xBar/(norm(s)^2)/(norm(a)^2);
 
  AmpErr(compNo)=betaTmp-beta(compNo);
end;

          
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [AmpErr]=caponEst(signal,txStv,rxStv,beta)
global waveform;


compNum=length(beta);
AmpErr=zeros(1,compNum);
for compNo=1:compNum
        
 a=rxStv(:,compNo);
 s=txStv(:,compNo).'*waveform;
        
 xBar=signal*s';
   
 R=signal*signal';
 
 
       
 Rinv=pinv(R);
 betaTmp=a'*Rinv*xBar/(a'*Rinv*a)/(norm(s)^2);
  AmpErr(compNo)=betaTmp-beta(compNo);
end;
     
     
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [AmpErr]=APESEst(signal,txStv,rxStv,beta)
global waveform;


compNum=length(beta);
AmpErr=zeros(1,compNum);
for compNo=1:compNum
        
 a=rxStv(:,compNo);
 s=txStv(:,compNo).'*waveform;
        
 xBar=signal*s';
   
 T=signal*signal'-xBar*xBar'/( norm(s)^2);
       
 Tinv=pinv(T);
 betaTmp=a'*Tinv*xBar/(a'*Tinv*a)/(norm(s)^2);
       
  AmpErr(compNo)=betaTmp-beta(compNo);
end;
      
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function  [AmpErr]=DGCEst(signal,txStv,rxStv,beta)
global waveform;

compNum=length(beta);
AmpErr=zeros(1,compNum);

A=rxStv;
S=txStv.'*waveform;

% GTmp=signal*S'; 
% T=signal*signal'-GTmp*pinv(S*S')*GTmp';
GTmp=signal*waveform'; 
T=signal*signal'-GTmp*pinv(waveform*waveform')*GTmp';
       
 Tinv=pinv(T);
 betaTmp=(  (A'*Tinv*A).* ((S*S').') )\diag(A'*Tinv*signal*S');
        
 AmpErr=(betaTmp-beta).'; 
        
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [AmpErr]=RobcaponEst(signal,txStv,rxStv,beta)
global waveform;
epsilon=0.1;

compNum=length(beta);
AmpErr=zeros(1,compNum);
for compNo=1:compNum
        
 a=rxStv(:,compNo);
 s=txStv(:,compNo).'*waveform;
        
 xBar=signal*s';
   
 R=signal*signal';
 
 [a] = RCB_func(R,a,epsilon);
        
 Rinv=pinv(R);
 betaTmp=a'*Rinv*xBar/(a'*Rinv*a)/(norm(s)^2);
  AmpErr(compNo)=betaTmp-beta(compNo);
end;


     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [AmpErr]=RobAPESEst(signal,txStv,rxStv,beta)
global waveform;
epsilon=0.1;

compNum=length(beta);
AmpErr=zeros(1,compNum);
for compNo=1:compNum
        
 a=rxStv(:,compNo);
 s=txStv(:,compNo).'*waveform;
       
 R=signal*signal';
  [a] = RCB_func(R,a,epsilon);
 
 xBar=signal*s';
   
 T=signal*signal'-xBar*xBar'/( norm(s)^2);
       
 Tinv=pinv(T);
 betaTmp=a'*Tinv*xBar/(a'*Tinv*a)/(norm(s)^2);
       
  AmpErr(compNo)=betaTmp-beta(compNo);
end;

   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function  [AmpErr]=RobDGCEst(signal,txStv,rxStv,beta)
global waveform;
epsilon=0.1;

compNum=length(beta);
AmpErr=zeros(1,compNum);

R=signal*signal';
A=[];
for k=1:compNum
    [a] = RCB_func(R,rxStv(:,k),epsilon);
    A=[A,a];
end;
S=txStv.'*waveform;

% GTmp=signal*S'; 
% T=signal*signal'-GTmp*pinv(S*S')*GTmp';
GTmp=signal*waveform'; 
T=signal*signal'-GTmp*pinv(waveform*waveform')*GTmp';
       
 Tinv=pinv(T);
 betaTmp=(  (A'*Tinv*A).* ((S*S').') )\diag(A'*Tinv*signal*S');
        
 AmpErr=(betaTmp-beta).';       