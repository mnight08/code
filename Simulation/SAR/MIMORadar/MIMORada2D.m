function MIMORadar
close all;
clear all;

global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global save_path

%save_path='E:/workspace/program/MIMO Radar/data';
savePath='C:/myprogram/MIMO Radar/dataImage/'
saveName='image_Conf1';
%path(path,save_path);

txAntNum=10;
rxAntNum=10;
txDist=5*ones(1,txAntNum-1);%[0.5 1. 1.5 4];%
rxDist=0.5*ones(1,rxAntNum-1);%[0.5 1. 1.5 4];%
sampNum=256;

% target parameters
rangeNum=64; targetNum=10;
loadFlag=1;
%if loadFlag==1
    load([savePath  'Radar2DImage_Parameters1.mat']);
    DOATrue(1)=19; DOATrue(2)=-15; DOATrue(3)=1; DOATrue(5)=17;  DOATrue(6) =-17; 
    rangeTrue(1)=34; rangeTrue(2)=51; rangeTrue(3)=57; rangeTrue(10)=17; rangeTrue(6)=10;%DOATrue(5)=-16; DOATrue(6)=5; DOATrue(8)=-18; DOATrue(9)=-25
    save([savePath  'Radar2DImage_Parameters1.mat'], 'betaTrue','DOATrue','rangeTrue');
% else 
%    betaTrue= rand(1,targetNum)*10;
%    targetNum=length(betaTrue);
%    DOATrue= round((2*rand(1, targetNum)-1)*28); %linspace(-25, 25, targetNum);%
%    rangeTrue= round( rand(1,targetNum)*50+5); %
%    save([savePath  'Radar2DImage_Parameters1.mat'], 'betaTrue','DOATrue','rangeTrue');
% end;
betaGround=20;
GroundDOA=-10;
GroundRange=1:64; betaTrue=ones(1, targetNum);
betaTrue=[betaTrue betaGround*ones(1, rangeNum)];
DOATrue=[DOATrue GroundDOA*ones(1, rangeNum)]
rangeTrue=[rangeTrue GroundRange];
targetNum=length(rangeTrue);

scanSet=[-30:1:30];
ASNRdB=40; %dB
SNRAmp=sqrt(10.^(ASNRdB/10)./rxAntNum./txAntNum);

jammerDOA=-5;
AINRdB=100;
jammerPower=10.^((AINRdB-ASNRdB)./10)/rxAntNum;
JammerNum=length(jammerDOA);

% waveform: hardmard codes
temp=[1];
for kk=1:ceil(log2(sampNum))
  temp=[temp temp; temp -1*temp];
end;
waveformSet=temp;
waveform=(waveformSet(1:txAntNum, 1:sampNum)+j*waveformSet(txAntNum+1:2*txAntNum, 1:sampNum))/sqrt(2);

mSeq=mseq(2,8,0,1);
waveform=waveform.*(ones(txAntNum,1)* [mSeq' 1]) ./sqrt(sampNum) ;



% source generator
%------------------------------------
signalPure=zeros(rxAntNum, sampNum+rangeNum);
for k=1:targetNum
   txTmp=cumsum([0 txDist]);
   v=exp(-j*2*pi*txTmp' * sin(DOATrue(k)*pi/180) );%  
   rxTmp=cumsum([0 rxDist]);
   rxStv=exp(-j*2*pi*rxTmp' * sin(DOATrue(k)*pi/180) );% 

   signalPure(:, (1:sampNum)+rangeTrue(k))=signalPure(:, (1:sampNum)+rangeTrue(k)) + rxStv*diag(betaTrue(k))*(v.'*waveform);
end;

signal=signalPure;

 % correlation matrix 
%   Q_struct=zeros(rxAntNum,rxAntNum);
%    for row_no=1:rxAntNum
%       for col_no=1:rxAntNum
%           Q_struct(row_no,col_no)=0.9^abs(col_no-row_no)*exp(j*pi/2*(col_no-row_no));
%       end;
%     end;
   Q_struct=eye(rxAntNum,rxAntNum)
signal=signal +  (Q_struct^0.5)*(randn(rxAntNum, sampNum+rangeNum) +j *randn(rxAntNum, sampNum+rangeNum))/SNRAmp/sqrt(2) ;

% jammer 
rxTmp=cumsum([0 rxDist]);
rxStv=exp(-j*2*pi*rxTmp' * sin(jammerDOA*pi/180) );
jammerWaveform=((2*randint(JammerNum,sampNum+rangeNum)-1)+j*(2*randint(JammerNum,sampNum+rangeNum)-1))/sqrt(2);
signal=signal+rxStv*jammerWaveform.*sqrt(jammerPower);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MIMO signal processing
loadFlag=0;
if loadFlag==1
    load([savePath saveName '_DAS.mat']);
else 
    [imageDAS]=fun_DAS_RangeCompression(signal, waveform, rangeNum, scanSet);
    save([savePath saveName '_DAS.mat'], 'imageDAS','betaTrue','DOATrue','rangeTrue');
end;

if loadFlag==1
    load([savePath saveName '_RCB.mat']);
else 
    [imageRCB]=fun_RCB_RangeCompression(signal, waveform, rangeNum, scanSet);
    save([savePath saveName '_RCB.mat'], 'imageRCB','betaTrue','DOATrue','rangeTrue');
end;

if loadFlag==1
    load([savePath saveName '_Capon.mat']);
else 
    [imageCapon]=fun_Capon(signal, waveform, rangeNum, scanSet);
    save([savePath saveName '_Capon.mat'], 'imageCapon','betaTrue','DOATrue','rangeTrue');
end;

if loadFlag==1
    load([savePath saveName '_Apes.mat']);
else 
    [imageApes]=fun_Apes(signal, waveform, rangeNum, scanSet);
    save([savePath saveName '_Apes.mat'], 'imageApes','betaTrue','DOATrue','rangeTrue');
end;

if loadFlag==1
    load([savePath saveName '_GLRT.mat']);
else 
    [imageGLRT]=fun_GLRT(signal, waveform, rangeNum, scanSet);
    save([savePath saveName '_GLRT.mat'], 'imageGLRT','betaTrue','DOATrue','rangeTrue');
end;

loadFlag=0;
if loadFlag==1
    load([savePath saveName '_CAPESGLRT.mat']);
else 
    [imageCAPESGLRT]=fun_CAPESGLRT(imageCapon, imageApes, imageGLRT); 
    save([savePath saveName '_CAPESGLRT.mat'], 'imageCAPESGLRT');
end;
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure;
imagescdb1(imageDAS,70, 1:rangeNum,scanSet, 1,0);
drawnow;
hold on
thetaSet=linspace(-pi, pi, 100);
xSet=2*cos(thetaSet); ySet=2*sin(thetaSet);
for k=1:10%targetNum
    plot(xSet+DOATrue(k), ySet+rangeTrue(k)+1, 'y');
end;
print('-depsc',[savePath saveName '_DAS.eps']);

%------------
figure;
imagescdb1(imageRCB,70, 1:rangeNum,scanSet, 1,0);
drawnow;
hold on
thetaSet=linspace(-pi, pi, 100);
xSet=2*cos(thetaSet); ySet=2*sin(thetaSet);
for k=1:10%targetNum
    plot(xSet+DOATrue(k), ySet+rangeTrue(k)+1, 'y');
end;
print('-depsc',[savePath saveName '_RCB.eps']);
 
%--------------
figure;
imagescdb1(imageCapon,70, 1:rangeNum,scanSet, 1,0);
drawnow;
%set(hb,'Position',  [0.8 0.42 0.02 0.2])
%text(63.2,-1.8,'(dB)')
hold on
thetaSet=linspace(-pi, pi, 100);
xSet=2*cos(thetaSet); ySet=2*sin(thetaSet);
for k=1:10%targetNum
    plot(xSet+DOATrue(k), ySet+rangeTrue(k)+1, 'y');
end;
print('-depsc',[savePath saveName '_Capon.eps']);

%------------
figure;
imagescdb1(imageApes,70, 1:rangeNum,scanSet, 1,0);
drawnow;
%set(hb,'Position',  [0.8 0.42 0.02 0.2])
%text(63.2,-1.8,'(dB)')
hold on
thetaSet=linspace(-pi, pi, 100);
xSet=2*cos(thetaSet); ySet=2*sin(thetaSet);
for k=1:10%targetNum
    plot(xSet+DOATrue(k), ySet+rangeTrue(k)+1, 'y');
end;
print('-depsc',[savePath saveName '_Apes.eps']);
 
%------------
figure;
imagescdb1(imageGLRT,1, 1:rangeNum,scanSet, 0,0);
drawnow;
%set(hb,'Position',  [0.8 0.42 0.02 0.2])
%text(63.2,-1.8,'(dB)')
hold on
thetaSet=linspace(-pi, pi, 100);
xSet=2*cos(thetaSet); ySet=2*sin(thetaSet);
for k=1:10%targetNum
    plot(xSet+DOATrue(k), ySet+rangeTrue(k)+1, 'y');
end;
print('-depsc',[savePath saveName '_GLRT.eps']);
 
%------------
figure;
imagescdb1(imageCAPESGLRT,70, 1:rangeNum,scanSet, 1,0);
drawnow;
%set(hb,'Position',  [0.8 0.42 0.02 0.2])
%text(63.2,-1.8,'(dB)')
hold on
thetaSet=linspace(-pi, pi, 100);
xSet=2*cos(thetaSet); ySet=2*sin(thetaSet);
for k=1:10%targetNum
    plot(xSet+DOATrue(k), ySet+rangeTrue(k)+1, 'y');
end;
print('-depsc',[savePath saveName '_CAPESGLRT.eps']);
 
 
      
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [imageEst]=fun_Capon(signal, waveform, rangeNum, scanSet)
global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global save_path

angleNum=length(scanSet);
sampNum=size(waveform,2);
 txTmp=cumsum([0 txDist]);
 rxTmp=cumsum([0 rxDist]);

R=signal*signal'/sampNum;
Rinv=pinv(R);
imageEst=zeros( rangeNum, angleNum);
for k=1:rangeNum
    waveformTmp=zeros(txAntNum, sampNum+rangeNum);
    waveformTmp(:, (1:sampNum)+k-1)=waveform;   
    for n=1:angleNum
        DOATmp=scanSet(n);      
        v=exp(-j*2*pi*txTmp' * sin(DOATmp*pi/180) );
        a=exp(-j*2*pi*rxTmp' * sin(DOATmp*pi/180) );
        s=v.'*waveformTmp;
        xBar=signal*s'./sampNum;
        betaTmp= sampNum*a'*Rinv*xBar/(a'*Rinv*a)/(norm(s)^2);
          
        imageEst(k, n)=betaTmp;
    end;
end;

        
    
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [imageEst]=fun_Apes(signal, waveform, rangeNum, scanSet)
global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global save_path

angleNum=length(scanSet);
sampNum=size(waveform,2);
 txTmp=cumsum([0 txDist]);
 rxTmp=cumsum([0 rxDist]);

R=signal*signal'/sampNum;
Rinv=pinv(R);
imageEst=zeros( rangeNum, angleNum);
for k=1:rangeNum
    waveformTmp=zeros(txAntNum, sampNum+rangeNum);
    waveformTmp(:, (1:sampNum)+k-1)=waveform;
    for n=1:angleNum
        DOATmp=scanSet(n);      
        v=exp(-j*2*pi*txTmp' * sin(DOATmp*pi/180) );
        a=exp(-j*2*pi*rxTmp' * sin(DOATmp*pi/180) );
        s=v.'*waveformTmp;
        xBar=signal*s'/sampNum;
        
        Rinv_xBar=Rinv*xBar;
        Tinv=Rinv - Rinv_xBar*Rinv_xBar'./(xBar'*Rinv_xBar - norm(s).^2/sampNum);
        betaTmp=sampNum*a'*Tinv*xBar/(a'*Tinv*a)/(norm(s)^2);
          
        imageEst(k, n)=betaTmp;
    end;
end;
     
 
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [imageEst]=fun_GLRT(signal, waveform, rangeNum, scanSet)
global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global save_path

angleNum=length(scanSet);
sampNum=size(waveform,2);
 txTmp=cumsum([0 txDist]);
 rxTmp=cumsum([0 rxDist]);

R=signal*signal'/sampNum;
Rinv=pinv(R);
imageEst=zeros( rangeNum, angleNum);
for k=1:rangeNum
    waveformTmp=zeros(txAntNum, sampNum+rangeNum);
    waveformTmp(:, (1:sampNum)+k-1)=waveform;
    for n=1:angleNum
        DOATmp=scanSet(n);      
        v=exp(-j*2*pi*txTmp' * sin(DOATmp*pi/180) );
        a=exp(-j*2*pi*rxTmp' * sin(DOATmp*pi/180) );
        s=v.'*waveformTmp;
        xBar=signal*s'/sampNum;
        
        Rinv_xBar=Rinv*xBar;
         Tinv=Rinv - Rinv_xBar*Rinv_xBar'./(xBar'*Rinv_xBar - norm(s).^2/sampNum);
        GLR=1 - (a'*Rinv*a)./(a'*Tinv*a);
          
        imageEst(k, n)=GLR;
    end;
end;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%      
function [imageCAPESGLRT]=fun_CAPESGLRT(imageCapon, imageApes, imageGLRT) 

global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global save_path
GLRThreshold=0.2;
[rangeNum scanNum]=size(imageCapon);
imageCapon=abs(imageCapon);

imagePeak=(imageCapon>[zeros(rangeNum, 1) imageCapon(:,1:end-1)]) ...
          +  (imageCapon>[ imageCapon(:,2:end) zeros(rangeNum, 1)])...
          + (imageCapon>[zeros(1, scanNum); imageCapon(1:end-1,:)]) ...
          + (imageCapon> [imageCapon(2:end,:); zeros(1, scanNum)]);
imagePeak=(imagePeak>=4);

imageCAPESGLRT=imageApes.*imagePeak.*(imageGLRT>=GLRThreshold);
          
          
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [imageEst]=fun_DAS_RangeCompression(signal, waveform, rangeNum, scanSet)
global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global save_path

angleNum=length(scanSet);
sampNum=size(waveform,2);
 txTmp=cumsum([0 txDist]);
 rxTmp=cumsum([0 rxDist]);

R_Phi=waveform*waveform';
R_Phi_sqrtm=sqrtm(R_Phi);
imageEst=zeros( rangeNum, angleNum);
for k=1:rangeNum
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%
    % Range Compression
    signalTmp=signal(:, (1:sampNum)+k-1)*waveform'*pinv(R_Phi_sqrtm);
    waveformTmp=R_Phi_sqrtm;
    sampNumTmp=txAntNum;
    R=signalTmp*signalTmp'/sampNum;
    Rinv=pinv(R);
    for n=1:angleNum
        DOATmp=scanSet(n);      
        v=exp(-j*2*pi*txTmp' * sin(DOATmp*pi/180) );
        a=exp(-j*2*pi*rxTmp' * sin(DOATmp*pi/180) );
        s=v.'*waveformTmp;
        betaTmp= a'*signalTmp*s'/(a'*a)/norm(s).^2;         
        imageEst(k, n)=betaTmp;
    end;
end;

        
    
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [imageEst]=fun_RCB_RangeCompression(signal, waveform, rangeNum, scanSet)
global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global save_path
epsilon=0.1;

angleNum=length(scanSet);
sampNum=size(waveform,2);
 txTmp=cumsum([0 txDist]);
 rxTmp=cumsum([0 rxDist]);

R_Phi=waveform*waveform';
R_Phi_sqrtm=sqrtm(R_Phi);
imageEst=zeros( rangeNum, angleNum);
for k=1:rangeNum
    signalTmp=signal(:, (1:sampNum)+k-1)*waveform'*pinv(R_Phi_sqrtm);
    waveformTmp=R_Phi_sqrtm;
    sampNumTmp=txAntNum;
    R=signalTmp*signalTmp'/sampNum;
    Rinv=pinv(R);
    for n=1:angleNum
        DOATmp=scanSet(n);      
        v=exp(-j*2*pi*txTmp' * sin(DOATmp*pi/180) );
        a=exp(-j*2*pi*rxTmp' * sin(DOATmp*pi/180) );
        s=v.'*waveformTmp;
        xBar=signalTmp*s'/sampNumTmp;
        Rinv_xBar=Rinv*xBar;
        [a_hat] = RCB_func(R,a,epsilon);
        betaTmp=sampNumTmp*a_hat'*Rinv*xBar/(a_hat'*Rinv*a_hat)/(norm(s)^2);
        imageEst(k, n)=betaTmp;
    end;
end;
     
 