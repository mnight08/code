function [a_hat1,W_DCRCB] = DCRCB_func(R,m,a_bar,epsilon,d)

%%%%%% input
%% a_bar is the assumed steering vector,
%% d is the intersensorspacing with respect to wavelength (usually d=0.5)
%% m is the number of array elements
%% R is the array covariance matrix
%% epsilon is the parameter for the uncertainty set, i.e., the norm square of (a - a_bar)

%%%%%% output
%% a_hat1 is the steering vector calculated
%% W_DCRCB is the weigth vector calculated

[U,Gamma,V]=svd(R);
Gamma=real(Gamma);
Gamma_vec=diag(Gamma);
inv_Gamma_vec=ones(m,1)./Gamma_vec;

z=U'*a_bar;

a_tilde1= U(:,1)*sqrt(m);
a_tilde1= a_tilde1*exp(j*(angle(a_tilde1'*a_bar))); %remove the phaseeffect

a_tilde2= -a_tilde1;

if (norm(a_tilde1 - a_bar)^2 <= norm(a_tilde2 - a_bar)^2)
  a_tilde = a_tilde1;
else
  a_tilde = a_tilde2;
end

rou=m/(m-epsilon/2)^2;

if (norm(a_tilde - a_bar)^2 <= epsilon)
  a_hat1 = a_tilde;
  disp('Exact RCB case 1');

else
  %%%%% Using the half interval method for the optimal delta

  xL1= -inv_Gamma_vec(1)-1e-12;
  xR1= (inv_Gamma_vec(m) - sqrt(m*rou)*inv_Gamma_vec(1))/(sqrt(m*rou) -1);

  f_xL1=Exact_RCB_func(z,inv_Gamma_vec,xL1,m,rou);
  f_xR1=Exact_RCB_func(z,inv_Gamma_vec,xR1,m,rou);

  if(f_xL1*f_xR1>0)
  disp('error!');
  pause;
  end

  while(1)
  f_xL1=Exact_RCB_func(z,inv_Gamma_vec,xL1,m,rou);
  f_xR1=Exact_RCB_func(z,inv_Gamma_vec,xR1,m,rou);

  x2=(xL1+xR1)/2;
  f_x2=Exact_RCB_func(z,inv_Gamma_vec,x2,m,rou);
  %f_x2

  if(abs(f_x2)<1e-10)
    break;
  end

  if(f_x2<0)
    xL2=xL1;
    xR2=x2;
  elseif (f_x2>0)
    xL2=x2;
    xR2=xR1;
  else
    break;
    disp('product is 0');
  end

  xL1=xL2;
  xR1=xR2;

  end

  lambda_opt= x2;

  DCRCB_load_level=1/lambda_opt;

  mu = real((m - epsilon/2)/(a_bar'*R*inv(eye(m)+lambda_opt*R)*a_bar));

  %%% DCRCB
  a_hat1 = mu*R*inv(eye(m)+lambda_opt*R)*a_bar;
  
end

 %a_hat1=sqrt(m)*a_hat1./norm(a_hat1);
  W_DCRCB=inv(R)*a_hat1*inv(a_hat1'*inv(R)*a_hat1)*1;




function g = Exact_RCB_func(z,inv_Gamma_vec,delta,m,rou)

g1=sum(abs(z).^2./(inv_Gamma_vec + delta*ones(m,1)).^2);
g=  g1/(sum(abs(z).^2./(inv_Gamma_vec + delta*ones(m,1))))^2 - rou;