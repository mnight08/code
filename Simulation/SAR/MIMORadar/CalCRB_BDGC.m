%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [CRBDOA, CRBAmp]=CalCRB_BDGC(txAntNum, txDist, rxAntNum, rxDist, B,DOATrue,waveform,Q)

sampNum=size(waveform, 2);
txSubArrayNum=length(txAntNum);
rxSubArrayNum=length(rxAntNum);
totalTxAntNum=sum(txAntNum);
totalRxAntNum=sum(rxAntNum);
targetNum=length(DOATrue);
betaNum=txSubArrayNum*rxSubArrayNum;

% construct the receiving steering matrix
Ar=zeros(totalRxAntNum, rxSubArrayNum, targetNum);
Ar_driv=zeros(totalRxAntNum, rxSubArrayNum, targetNum);
for k=1:targetNum
    DOAtmp=DOATrue(k);
    
    Atmp=zeros(totalRxAntNum, rxSubArrayNum);
    A_driv_tmp=zeros(totalRxAntNum, rxSubArrayNum);
    index=1;
    for m=1:rxSubArrayNum
         distTmp=rxDist(m, 1:(rxAntNum(m)-1) ); distTmp=cumsum([0 distTmp]);
         stvTmp=  exp(-j*2*pi*distTmp' * sin(DOAtmp*pi/180) );
         Atmp(index:(index+rxAntNum(m)-1), m)=stvTmp;
         
         %
         driv_Tmp=  stvTmp .* (-j *2*pi*distTmp'* cos(DOAtmp*pi/180) .* pi/180);
         A_driv_tmp(index:(index+rxAntNum(m)-1), m)=driv_Tmp;
         index=index+rxAntNum(m);
     end;
     
      Ar(:,:,k)=Atmp;  Ar_driv(:,:,k)=A_driv_tmp;
 end;
     clear stvTmp, driv_Tmp;
     clear Atmp, A_driv_tmp;
     
% construct the transmtting steering matrix
At=zeros(totalTxAntNum, txSubArrayNum, targetNum);
At_driv=zeros(totalTxAntNum, txSubArrayNum, targetNum);
for k=1:targetNum
    DOAtmp=DOATrue(k);
    
    Atmp=zeros(totalTxAntNum, txSubArrayNum);
    A_driv_tmp=zeros(totalTxAntNum, txSubArrayNum);
    index=1;
    for m=1:txSubArrayNum
         distTmp=txDist(m, 1:(txAntNum(m)-1) ); distTmp=cumsum([0 distTmp]);
         stvTmp=  exp(-j*2*pi*distTmp' * sin(DOAtmp*pi/180) );
         Atmp(index:(index+txAntNum(m)-1), m)=stvTmp;
         
         %
         driv_Tmp=  stvTmp .* (-j *2*pi*distTmp.'* cos(DOAtmp*pi/180) .* pi/180);
         A_driv_tmp(index:(index+txAntNum(m)-1), m)=driv_Tmp;
         index=index+txAntNum(m);
     end;
     
      At(:,:,k)=Atmp;  At_driv(:,:,k)=A_driv_tmp;
 end;
         
%%%%%%%%
Qinv=pinv(Q);
Rxx=waveform*waveform';

%calculate F11
F11=zeros(targetNum, targetNum);
for m=1:targetNum
    for n=1:targetNum
        Ar_m=squeeze( Ar(:,:,m)); 
        Ar_n=squeeze( Ar(:,:,n)); 
        Ar_driv_m=squeeze( Ar_driv(:,:,m));
        Ar_driv_n=squeeze( Ar_driv(:,:,n));
        
        At_m=squeeze( At(:,:,m)); 
        At_n=squeeze( At(:,:,n)); 
        At_driv_m=squeeze( At_driv(:,:,m));
        At_driv_n=squeeze( At_driv(:,:,n));
        
        B_m=squeeze( B(:,:,m));
        B_n=squeeze( B(:,:,n));
        
        temp=trace( (Ar_driv_m'*Qinv*Ar_driv_n)*(B_n*At_n.'*Rxx*conj(At_m)*B_m') )...
            +trace( (Ar_driv_m'*Qinv*Ar_n*B_n)*(At_driv_n.'*Rxx*conj(At_m)*B_m') ) ...
            +trace( (B_m'*Ar_m'*Qinv*Ar_driv_n)*(B_n*At_n.'*Rxx*conj(At_driv_m)))...
            +trace( (B_m'*Ar_m'*Qinv*Ar_n*B_n)*(At_driv_n.'*Rxx*conj(At_driv_m)));
        
        F11(m,n)=trace(temp);
    end;
end;

% calculate F21
betaNum=txSubArrayNum*rxSubArrayNum;
F21=zeros(targetNum*betaNum, targetNum );
index=1;
for m=1:targetNum
    for n=1:targetNum
        
        Ar_m=squeeze( Ar(:,:,m)); 
        Ar_n=squeeze( Ar(:,:,n)); 
        Ar_driv_m=squeeze( Ar_driv(:,:,m));
        Ar_driv_n=squeeze( Ar_driv(:,:,n));
        
        At_m=squeeze( At(:,:,m)); 
        At_n=squeeze( At(:,:,n)); 
        At_driv_m=squeeze( At_driv(:,:,m));                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
        At_driv_n=squeeze( At_driv(:,:,n));
        
        B_m=squeeze( B(:,:,m));
        B_n=squeeze( B(:,:,n));
        
        temp=(Ar_m'*Qinv*Ar_driv_n)*(B_n*At_n.'*Rxx*conj(At_m))...
            +(Ar_m'*Qinv*Ar_n*B_n)*(At_driv_n.'*Rxx*conj(At_m));
        
        F21(index:index+betaNum-1, n)=temp(:);
    end;
    index=index+betaNum;
end;

% calculate F22
betaNum=txSubArrayNum*rxSubArrayNum;
F22=zeros(targetNum*betaNum, targetNum*betaNum);
index1=1;
for m=1:targetNum
    index2=1;
    for n=1:targetNum
        
        Ar_m=squeeze( Ar(:,:,m)); 
        Ar_n=squeeze( Ar(:,:,n)); 
        
        At_m=squeeze( At(:,:,m)); 
        At_n=squeeze( At(:,:,n)); 
        
        temp1=(At_m'*Rxx*At_n);
        temp2=(Ar_m'*Qinv*Ar_n);
        F22(index1:index1+betaNum-1, index2:index2+betaNum-1)=kron(temp1, temp2);
                
        index2=index2+betaNum;
    end;
    index1=index1+betaNum;
end;

%%Calculate CRB
F22_inv=inv(F22);
CRBDOA=0.5*inv( real( F11-F21'*F22_inv*F21   )  ); 
CRBAmp=F22_inv+F22_inv*F21*CRBDOA*F21'*F22_inv;