function MIMORadar
close all;
clear all;

global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global sampNum;
global save_path
global waveform;
global scanSetOrg;
%save_path='E:/workspace/program/MIMO Radar/data';
savePath='E:/Document/lzxu/my program/MIMO Radar/ParameterIdentifiablity/'
saveName=['Spectral_16Target_5by5'] 
    
%path(path,save_path);

txAntNum=5;
rxAntNum=5; 
targetNum=16;

txDist=5*0.5*ones(1,txAntNum-1);%[0.5 1. 1.5 4];%
rxDist=0.5*ones(1,rxAntNum-1);%[0.5 1. 1.5 4];%
sampNum=256;
betaTrue=ones(targetNum,1);
DOASet=[8:8:80; -8:-8:-80]; DOASet=[0; DOASet(:)].'; DOATrue=DOASet(1:targetNum);
%DOASet=[-60:8:70];DOATrue=DOASet(1:targetNum);
compNum=length(DOATrue);
scanSetOrg=[-90:0.1:90];
SNRdB=20; %dB
SNR=10.^(SNRdB/20);

INRdB=SNRdB; JammerAmp=10.^((INRdB-SNRdB)/20);
DOAJammer=[45];
JammerNum=length(DOAJammer);

% waveform: hardmard codes
temp=[1];
for kk=1:ceil(log2(sampNum))
  temp=[temp temp; temp -1*temp];
end;
waveformSet=temp;
waveform=(waveformSet(1:txAntNum, 1:sampNum)+j*waveformSet(txAntNum+1:2*txAntNum, 1:sampNum))/sqrt(2)/sqrt(txAntNum);
%waveform=(( 2*rand(txAntNum, sampNum) -1) + j*(2*rand(txAntNum, sampNum) -1) )/sqrt(2);

% source generator
%------------------------------------
txTmp=cumsum([0 txDist]);
txStv=exp(-j*2*pi*txTmp' * sin(DOATrue*pi/180) );%  + 0.1*(randn(txAntNum, compNum) +j*randn(txAntNum, compNum) )/sqrt(2);
rxTmp=cumsum([0 rxDist]);
rxStv=exp(-j*2*pi*rxTmp' * sin(DOATrue*pi/180) );%  + 0.1*(randn(txAntNum, compNum) +j*randn(txAntNum, compNum) )/sqrt(2);

signalPure=rxStv*diag(betaTrue)*(txStv.'*waveform);

 % correlation matrix 
%   Q_struct=zeros(rxAntNum,rxAntNum);
%    for row_no=1:rxAntNum
%       for col_no=1:rxAntNum
%           Q_struct(row_no,col_no)=0.9^abs(col_no-row_no)*exp(j*pi/2*(col_no-row_no));
%       end;
%     end;
   Q_struct=eye(rxAntNum,rxAntNum)
signal=signalPure +  (Q_struct^0.5)*(randn(rxAntNum, sampNum) +j *randn(rxAntNum, sampNum))/SNR/sqrt(2) ;

% generator 
rxTmp=cumsum([0 rxDist]);
rxStv=exp(-j*2*pi*rxTmp' * sin(DOAJammer*pi/180) );
jammerWaveform=((2*randint(JammerNum,sampNum)-1)+j*(2*randint(JammerNum,sampNum)-1))/sqrt(2);
signal=signal+rxStv*jammerWaveform.*JammerAmp;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MIMO signal processing
[patternEstDAS,scanSetOrgDAS]=das(signal);
%[patternEstCAPON,CorCoeffEstCapon,scanSetOrgCAPON]=capon(signal);
% [patternEstAPES,CorCoeffEstAPES,scanSetOrgAPES]=APES(signal);
%  [patternEstAPES,GLR,scanSetOrgAPES]=GLR(signal);
 
% [patternEstCAPES,scanSetOrgCAPES]=CAPES(patternEstAPES, [DOATrue]);
 
%  [patternEstCAML,scanSetOrgCAML]=CAML(signal, [DOATrue ]);
 
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   figure;
%  patternTrue=scanSetOrgDAS-scanSetOrgDAS;
%  for k=1:compNum
%      index=find(scanSetOrgDAS==DOATrue(k));
%      patternTrue(index)= betaTrue(k);
% end;
% index=find(scanSetOrgDAS==DOAJammer);
% patternTrue(index)=JammerAmp;
% plot(scanSetOrgDAS, abs(patternTrue));
%  xlabel('Angle (deg)');
%  ylabel('Modulus of Complex Amplitude');
%  % legend('DAS','Capon', 'APES');
%  axis([min(scanSetOrgDAS) max(scanSetOrgDAS) 0 5]);%max(abs(patternEstAPES))]*1.2);
%  myboldify1;
%  print('-deps',[savePath saveName '_true' '.eps']);
 
 figure;
 plot(scanSetOrgDAS,abs(patternEstDAS),'-b');
 hold on;
compNum=length(betaTrue);
for compNo=1:compNum
    plot([DOATrue(compNo) DOATrue(compNo)], [0 betaTrue(compNo) ], ':k');
    hold on;
end;
 xlabel('Angle (deg)');
 ylabel('Modulus of Complex Amplitude');
 zoom on
% legend('DAS','Capon', 'APES');
 axis([min(scanSetOrgDAS) max(scanSetOrgDAS) 0 2]);%max(abs(patternEstAPES))]*1.2);
 myboldify1;
 print('-depsc',[savePath saveName  '_SNR' num2str(SNRdB) '_INR' num2str(INRdB)  '_LS' '.eps']);
 
% grid on;
 %figure;
 plot(scanSetOrgCAPON,abs(patternEstCAPON));
 hold on;
compNum=length(betaTrue);
for compNo=1:compNum
    plot([DOATrue(compNo) DOATrue(compNo)], [0 betaTrue(compNo) ], ':');
    hold on;
end;
 xlabel('DOA (deg)');
 ylabel('Modulus of Complex Amplitude');
  zoom on
% legend('DAS','Capon', 'APES');
 axis([min(scanSetOrgCAPON) max(scanSetOrgCAPON) 0 5]);%max(abs(patternEstAPES))]*1.2);
 myboldify1;
 print('-deps',[savePath saveName '_CAPON' '.eps']);
 
 
 % grid on;
 figure;
 plot(scanSetOrgAPES,abs(patternEstAPES));
 hold on;
compNum=length(betaTrue);
for compNo=1:compNum
    plot([DOATrue(compNo) DOATrue(compNo)], [0 betaTrue(compNo) ], ':');
    hold on;
end;
 xlabel('DOA (deg)');
 ylabel('Modulus of Complex Amplitude');
 %legend('DAS','Capon', 'APES');
 axis([min(scanSetOrgAPES) max(scanSetOrgAPES) 0 5]);%max(abs(patternEstAPES))]*1.2);
 myboldify1;
 print('-deps',[savePath saveName '_APES' '.eps']);
 
%  
 % grid on;
 figure;
 plot(scanSetOrgCAPES,abs(patternEstCAPES));
 hold on;
compNum=length(betaTrue);
for compNo=1:compNum
    plot([DOATrue(compNo) DOATrue(compNo)], [0 betaTrue(compNo) ], ':');
    hold on;
end;
 xlabel('DOA (deg)');
 ylabel('Modulus of Complex Amplitude');
 %legend('DAS','Capon', 'APES');
 axis([min(scanSetOrgAPES) max(scanSetOrgAPES) 0 5]);%max(abs(patternEstAPES))]*1.2);
 myboldify1;
 print('-deps',[savePath saveName '_CAPES' '.eps']);
 
  % grid on;
 figure;
 plot(scanSetOrgCAML,abs(patternEstCAML));
 hold on;
compNum=length(betaTrue);
for compNo=1:compNum
    plot([DOATrue(compNo) DOATrue(compNo)], [0 betaTrue(compNo) ], ':');
    hold on;
end;
 xlabel('DOA (deg)');
 ylabel('Modulus of Complex Amplitude');
 %legend('DAS','Capon', 'APES');
 axis([min(scanSetOrgAPES) max(scanSetOrgAPES) 0 5]);%max(abs(patternEstAPES))]*1.2);
 myboldify1;
 print('-deps',[savePath saveName '_CAML' '.eps']);

 
 
figure;
 plot(scanSetOrgCAML,abs(CorCoeffEstCapon));
 hold on;

 xlabel('DOA (deg)');
 ylabel('Correlation Coefficient');
 %legend('DAS','Capon', 'APES');
 axis([min(scanSetOrgAPES) max(scanSetOrgAPES) 0 1.2]);%max(abs(patternEstAPES))]*1.2);
 myboldify1;
 print('-deps',[savePath saveName '_Capon_correlationcoeff' '.eps']);
 

 figure;
 plot(scanSetOrgCAML,abs(CorCoeffEstAPES));
 hold on;

 xlabel('DOA (deg)');
 ylabel('Correlation Coefficient');
 %legend('DAS','Capon', 'APES');
 axis([min(scanSetOrgAPES) max(scanSetOrgAPES) 0 1.2]);%max(abs(patternEstAPES))]*1.2);
 myboldify1;
 print('-deps',[savePath saveName '_APES_correlationcoeff' '.eps']);
 
 
  figure;
 plot(scanSetOrgCAML, GLR);
 hold on;

 xlabel('DOA (deg)');
 ylabel('GLR');
 %legend('DAS','Capon', 'APES');
 axis([min(scanSetOrgAPES) max(scanSetOrgAPES) 0 1.2]);%max(abs(patternEstAPES))]*1.2);
 myboldify1;
 print('-deps',[savePath saveName '_GLR' '.eps']);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [patternEst,scanSetOrg]=das(signal)

global save_path

global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global sampNum;
global waveform;
global scanSetOrg;


DOAEst=[];
betaEst=[];


scanSet=scanSetOrg; 
patternNum=length(scanSet);
patternEst=zeros(patternNum,1);
 
       
       for DOAScan=scanSet
        DOATmp=[DOAScan];
        
        txTmp=cumsum([0 txDist]);
        txStv=exp(-j*2*pi*txTmp' * sin(DOATmp*pi/180) );
        rxTmp=cumsum([0 rxDist]);
        rxStv=exp(-j*2*pi*rxTmp' * sin(DOATmp*pi/180) );
        
        a=rxStv;
        s=txStv.'*waveform;
        
        xBar=signal*s';
        betaTmp=a'*xBar/(norm(s)^2)/(norm(a)^2);
                      
        index=find(scanSet==DOAScan);
        patternEst(index)= betaTmp(end);
    end;
    
%         figure;
%         plot(scanSetOrg, abs(patternEst));
%         grid on;
        
%         DOAEstTmp=input('The peak DOA:')
%         DOAEst=[DOAEst, DOAEstTmp];
%         index=find(scanSet==DOAEstTmp);
%         scanSet=[scanSet(1:index-1) scanSet(index+1:end)];
        
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [patternEst,CorCoeffEst,scanSetOrg]=capon(signal);

global save_path

global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global sampNum;
global waveform;
global scanSetOrg;


DOAEst=[];
betaEst=[];


scanSet=scanSetOrg; 
patternNum=length(scanSet);
patternEst=zeros(patternNum,1);
CorCoeffEst=zeros(patternNum,1);
       
       for DOAScan=scanSet
        DOATmp=[DOAScan];
        
        txTmp=cumsum([0 txDist]);
        txStv=exp(-j*2*pi*txTmp' * sin(DOATmp*pi/180) );
        rxTmp=cumsum([0 rxDist]);
        rxStv=exp(-j*2*pi*rxTmp' * sin(DOATmp*pi/180) );
        
        a=rxStv;
        s=txStv.'*waveform;
        
        xBar=signal*s'/sampNum;
   
        R=signal*signal'/sampNum;
       
        Rinv=pinv(R);
        betaTmp=sampNum*a'*Rinv*xBar/(a'*Rinv*a)/(norm(s)^2);
               
        index=find(scanSet==DOAScan);
        patternEst(index)= betaTmp(end);
        
        
        s_est=a'*Rinv*signal/(a'*Rinv*a)/(norm(s)^2);
        temp=s_est*s'/norm(s_est)/norm(s);
        CorCoeffEst(index)=temp;
        
    end;
%     
%         figure;
%         plot(scanSetOrg, abs(patternEst));
%         grid on;
        
%         DOAEstTmp=input('The peak DOA:')
%         DOAEst=[DOAEst, DOAEstTmp];
%         index=find(scanSet==DOAEstTmp);
%         scanSet=[scanSet(1:index-1) scanSet(index+1:end)];
        
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     
     
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [patternEst,CorCoeffEst,scanSetOrg]=APES(signal);

global save_path

global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global sampNum;
global waveform;
global scanSetOrg;


DOAEst=[];
betaEst=[];


scanSet=scanSetOrg; 
patternNum=length(scanSet);
patternEst=zeros(patternNum,1);
CorCoeffEst=zeros(patternNum,1);
       
       for DOAScan=scanSet
        DOATmp=[DOAScan];
        
        txTmp=cumsum([0 txDist]);
        txStv=exp(-j*2*pi*txTmp' * sin(DOATmp*pi/180) );
        rxTmp=cumsum([0 rxDist]);
        rxStv=exp(-j*2*pi*rxTmp' * sin(DOATmp*pi/180) );
        
        a=rxStv;
        s=txStv.'*waveform;
        
        xBar=signal*s';
   
        T=signal*signal'-xBar*xBar'/( norm(s)^2);
       % T=signal*signal'-(signal*waveform')*inv(waveform*waveform')*(signal*waveform')';
        
        Tinv=pinv(T);
        betaTmp=a'*Tinv*xBar/(a'*Tinv*a)/(norm(s)^2);
               
        index=find(scanSet==DOAScan);
        patternEst(index)= betaTmp(end);
        
        
        s_est=a'*Tinv*signal/(a'*Tinv*a)/(norm(s)^2);
        temp=s_est*s'/norm(s_est)/norm(s);
        CorCoeffEst(index)=temp;
    end;
    
%         figure;
%         plot(scanSetOrg, abs(patternEst));
%         grid on;
        
%         DOAEstTmp=input('The peak DOA:')
%         DOAEst=[DOAEst, DOAEstTmp];
%         index=find(scanSet==DOAEstTmp);
%         scanSet=[scanSet(1:index-1) scanSet(index+1:end)];
        
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
      
function [patternEst,scanSetOrg]=CAPES(patternEstAPES, DOAEst)

global save_path

global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global sampNum;
global waveform;
global scanSetOrg;

compNum=length(DOAEst);

betaEst=[];

scanSet=scanSetOrg; 
patternNum=length(scanSet);
patternEst=zeros(patternNum,1);


for k=1:compNum
    DOATmp=DOAEst(k);
    
    index=find(scanSet==DOATmp);
    patternEst(index)= patternEstAPES(index);
end;

%%%%%%%%%%%%%%%%%%%%%%%%%%
function [patternEst,scanSetOrg]=CAML(signal, DOAEst)

global save_path

global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global sampNum;
global waveform;
global scanSetOrg;

compNum=length(DOAEst);

betaEst=[];

txTmp=cumsum([0 txDist]);
txStv=exp(-j*2*pi*txTmp' * sin(DOAEst*pi/180) );% 
rxTmp=cumsum([0 rxDist]);
rxStv=exp(-j*2*pi*rxTmp' * sin(DOAEst*pi/180) );%
        
 A=rxStv;
 S=txStv.'*waveform;
        
GTmp=signal*S';
   
 T=signal*signal'-GTmp*pinv(S*S')*GTmp';
       
Tinv=pinv(T);
betaTmp=(  (A'*Tinv*A).* ((S*S').') )\diag(A'*Tinv*GTmp);
        
scanSet=scanSetOrg; 
patternNum=length(scanSet);
patternEst=zeros(patternNum,1);

for k=1:compNum
    DOATmp=DOAEst(k);
    
    index=find(scanSet==DOATmp);
    patternEst(index)= betaTmp(k);
end;


   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [DOAEst, betaEst, patternEst]=DGCProc(signal);

global save_path

global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global sampNum;
global waveform;

maxCompNum=3;

DOAEst=[];
betaEst=[];

scanSetOrg=[-90:90];
scanSet=scanSetOrg; 
patternNum=length(scanSet);
patternEst=zeros(patternNum,1);
for k=1:maxCompNum
    
       
       for DOAScan=scanSet
        DOATmp=[DOAEst DOAScan];
        
        txStv=exp(-j*2*pi*txDist*(0:txAntNum-1)' * sin(DOATmp*pi/180) );
        rxStv=exp(-j*2*pi*rxDist*(0:rxAntNum-1)' * sin(DOATmp*pi/180) );
        
        A=rxStv;
        S=txStv.'*waveform;
        
        GTmp=signal*S';
   
        T=signal*signal';%-GTmp*pinv(S*S')*GTmp';
       
        Tinv=pinv(T);
        betaTmp=(  (A'*Tinv*A).* ((S*S').') )\diag(A'*Tinv*GTmp);
        
        index=find(scanSet==DOAScan);
        patternEst(index)= betaTmp(end);
    end;
    
        figure;
        plot(scanSetOrg, abs(patternEst));
%         
%         DOAEstTmp=input('The peak DOA:')
%         DOAEst=[DOAEst, DOAEstTmp];
%         index=find(scanSet==DOAEstTmp);
%         scanSet=[scanSet(1:index-1) scanSet(index+1:end)];
        
    end;
    
     DOATmp=[-45 -30 10];
     txStv=exp(-j*2*pi*txDist*(0:txAntNum-1)' * sin(DOATmp*pi/180) );
     rxStv=exp(-j*2*pi*rxDist*(0:rxAntNum-1)' * sin(DOATmp*pi/180) );
     
     A=rxStv;
     S=txStv.'*waveform;
        
     GTmp=signal*S';
   
     T=signal*signal'-GTmp*pinv(S*S')*GTmp';
       
     Tinv=pinv(T);
     betaTmp=(  (A'*Tinv*A).* ((S*S').') )\diag(A'*Tinv*GTmp)
     
     
     
     function [patternEst,GLREst,scanSetOrg]=GLR(signal);

global save_path

global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global sampNum;
global waveform;
global scanSetOrg;


DOAEst=[];
betaEst=[];


scanSet=scanSetOrg; 
patternNum=length(scanSet);
patternEst=zeros(patternNum,1);
CorCoeffEst=zeros(patternNum,1);
       
       for DOAScan=scanSet
        DOATmp=[DOAScan];
        
        txTmp=cumsum([0 txDist]);
        txStv=exp(-j*2*pi*txTmp' * sin(DOATmp*pi/180) );
        rxTmp=cumsum([0 rxDist]);
        rxStv=exp(-j*2*pi*rxTmp' * sin(DOATmp*pi/180) );
        
        a=rxStv;
        s=txStv.'*waveform;
        
        xBar=signal*s';
   
        R=signal*signal'/sampNum;
        T=(signal*signal'-xBar*xBar'/( norm(s)^2) )/sampNum;
       % T=signal*signal'-(signal*waveform')*inv(waveform*waveform')*(signal*waveform')';
        
        Tinv=pinv(T);
        betaTmp=a'*Tinv*xBar/(a'*Tinv*a)/(norm(s)^2);
               
        index=find(scanSet==DOAScan);
        patternEst(index)= betaTmp(end);
        
        
       Q_hat = 1/sampNum * (signal - a*betaTmp*s)*(signal - a*betaTmp*s)';
       % GLRTmp=-sampNum*log(det(Q_hat))-sampNum*rxAntNum;
        %GLRTmp=-log(det(Q_hat));
       
       GLRTmp=(a'*inv(T)*xBar)*(xBar'*inv(R)*a)./(a'*inv(T)*a)/(norm(s)^2)/sampNum;
        
      %  GLRTmp=1-det(Q_hat)./det(R);
        GLREst(index)=GLRTmp;
    end;
    
    
       function [patternEst,GLREst,scanSetOrg]=concentrateLF(signal);

global save_path

global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global sampNum;
global waveform;
global scanSetOrg;


DOAEst=[];
betaEst=[];


scanSet=scanSetOrg; 
patternNum=length(scanSet);
patternEst=zeros(patternNum,1);
CorCoeffEst=zeros(patternNum,1);
       
       for DOAScan=scanSet
        DOATmp=[DOAScan];
        
        txTmp=cumsum([0 txDist]);
        txStv=exp(-j*2*pi*txTmp' * sin(DOATmp*pi/180) );
        rxTmp=cumsum([0 rxDist]);
        rxStv=exp(-j*2*pi*rxTmp' * sin(DOATmp*pi/180) );
        
        a=rxStv;
        s=txStv.'*waveform;
        
        xBar=signal*s';
   
        R=signal*signal'/sampNum;
        T=(signal*signal'-xBar*xBar'/( norm(s)^2) )/sampNum;
       % T=signal*signal'-(signal*waveform')*inv(waveform*waveform')*(signal*waveform')';
        
        Tinv=pinv(T);
        betaTmp=a'*Tinv*xBar/(a'*Tinv*a)/(norm(s)^2);
               
        index=find(scanSet==DOAScan);
        patternEst(index)= betaTmp(end);
        
        
       Q_hat = 1/sampNum * (signal - a*betaTmp*s)*(signal - a*betaTmp*s)';
       % GLRTmp=-sampNum*log(det(Q_hat))-sampNum*rxAntNum;
       GLRTmp=-log(det(Q_hat));
       
      % GLRTmp=(a'*inv(T)*xBar)*(xBar'*inv(R)*a)./(a'*inv(T)*a)/(norm(s)^2)/sampNum;
        
      %  GLRTmp=1-det(Q_hat)./det(R);
        GLREst(index)=GLRTmp;
    end;
    