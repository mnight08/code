function MIMORadar
close all;
clear all;

global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global sampNum;
global save_path
global waveform;
global scanSetOrg;
global epsilon;

savePath='E:/Document/lzxu/my program/MIMO Radar/data/';
%path(path,save_path);
epsilon=0.1;

txAntNum=10;
rxAntNum=10;
txDist=0.5*ones(1,txAntNum-1);%[0.5 1. 1.5 4];%
rxDist=0.5*ones(1,rxAntNum-1);%[0.5 1. 1.5 4];%
sampNum=256;
betaTrue=[4  4  1];
DOATrue=[-40 -25 -10 ];
compNum=length(DOATrue);
scanSetOrg=[-60:0.05:60];
SNRdB=30; %dB
SNR=10.^(SNRdB/20);

JammerAmp=1000; %dB
DOAJammer=0;

% waveform: hardmard codes
temp=[1];
for kk=1:ceil(log2(sampNum))
  temp=[temp temp; temp -1*temp];
end;
waveformSet=temp;
waveform=(waveformSet(1:txAntNum, 1:sampNum)+j*waveformSet(txAntNum+1:2*txAntNum, 1:sampNum))/sqrt(2);
%waveform=(( 2*rand(txAntNum, sampNum) -1) + j*(2*rand(txAntNum, sampNum) -1) )/sqrt(2);

% source generator
%------------------------------------
tmp=sqrt(0.005)*(randn(txAntNum, compNum) +j*randn(txAntNum, compNum) )/sqrt(2);
rxTmp=cumsum([0 rxDist]);
rxStv=exp(-j*2*pi*rxTmp' * sin(DOATrue*pi/180) )  + tmp; rxStv=rxStv./norm(rxStv)*sqrt(rxAntNum);
txTmp=cumsum([0 txDist]);
txStv=exp(-j*2*pi*txTmp' * sin(DOATrue*pi/180) ) + tmp; txStv=txStv./norm(txStv)*sqrt(txAntNum);

signalPure=rxStv*diag(betaTrue)*(txStv.'*waveform);

 % correlation matrix 
  Q_struct=zeros(rxAntNum,rxAntNum);
   for row_no=1:rxAntNum
      for col_no=1:rxAntNum
          Q_struct(row_no,col_no)=0.9^abs(col_no-row_no)*exp(j*pi/2*(col_no-row_no));
      end;
    end;
  %  Q_struct=eye(rxAntNum,rxAntNum)
signal=signalPure +  sqrt(Q_struct)*(randn(rxAntNum, sampNum) +j *randn(rxAntNum, sampNum))/SNR/sqrt(2) ;

% generator 
rxTmp=cumsum([0 rxDist]);
rxStv=exp(-j*2*pi*rxTmp' * sin(DOAJammer*pi/180) );
jammerWaveform=((2*randint(1,sampNum)-1)+j*(2*randint(1,sampNum)-1))/sqrt(2);
signal=signal+rxStv*jammerWaveform*JammerAmp;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MIMO signal processing
[patternEstDAS,scanSetOrgDAS]=das(signal);
[patternEstCAPON,CorCoeffEstCapon,scanSetOrgCAPON]=CAPON(signal);
 [patternEstAPES,CorCoeffEstAPES,scanSetOrgAPES]=APES(signal);
  [patternEstRCB,CorCoeffEstRCB,scanSetOrgRCB]=RCB(signal);
  [patternEstDCRCB,CorCoeffEstDCRCB,scanSetOrgDCRCB]=DCRCB(signal);
 

  
%     figure;
%  plot(scanSetOrgDAS,(abs(patternEstDAS)));
%  hold on;
% compNum=length(betaTrue);
% for compNo=1:compNum
%     plot([DOATrue(compNo) DOATrue(compNo)], [0 (betaTrue(compNo)) ], ':');
%     hold on;
% end;
%  xlabel('DOA (deg)');
%  ylabel('Modulus of Complex Amplitude ');
% %5 legend('DAS','Capon', 'APES');
%  axis([min(scanSetOrgCAPON) max(scanSetOrgCAPON) 0 5]);%max(abs(patternEstAPES))]*1.2);
%  myboldify1;
%  print('-deps',[savePath 'Fig_stverr_DAS' '.eps']);
%  
% % grid on;
%  figure;
%  plot(scanSetOrgCAPON,(abs(patternEstCAPON)));
%  hold on;
% compNum=length(betaTrue);
% for compNo=1:compNum
%     plot([DOATrue(compNo) DOATrue(compNo)], [0 (betaTrue(compNo)) ], ':');
%     hold on;
% end;
%  xlabel('DOA (deg)');
%  ylabel('Modulus of Complex Amplitude');
% % legend('DAS','Capon', 'APES');
%  axis([min(scanSetOrgCAPON) max(scanSetOrgCAPON) 0 5]);%max(abs(patternEstAPES))]*1.2);
%  myboldify1;
%  print('-deps',[savePath 'Fig_stverr_CAPON' '.eps']);
%  
%  
%  % grid on;
%  figure;
%  plot(scanSetOrgAPES,(abs(patternEstAPES)));
%  hold on;
% compNum=length(betaTrue);
% for compNo=1:compNum
%     plot([DOATrue(compNo) DOATrue(compNo)], [0 (betaTrue(compNo)) ], ':');
%     hold on;
% end;
%  xlabel('DOA (deg)');
%  ylabel('Modulus of Complex Amplitude');
%  %legend('DAS','Capon', 'APES');
% axis([min(scanSetOrgAPES) max(scanSetOrgAPES) 0 5]);%max(abs(patternEstAPES))]*1.2);
% myboldify1;
%  print('-deps',[savePath 'Fig_stverr_APES' '.eps']);
%  
%   % grid on;
%  figure;
%  plot(scanSetOrgRCB,(abs(patternEstRCB)));
%  hold on;
% compNum=length(betaTrue);
% for compNo=1:compNum
%    plot([ DOATrue(compNo) DOATrue(compNo)], [0 (betaTrue(compNo)) ], ':');
%     hold on;
% end;
%  xlabel('DOA (deg)');
%  ylabel('Modulus of Complex Amplitude ');
%  zoom on
%  %legend('DAS','Capon', 'APES');
%  axis([min(scanSetOrgAPES) max(scanSetOrgAPES) 0 5]);
%  myboldify1;
%  print('-deps',[savePath 'Fig_stverr_RCB' '.eps']);
%  
%    % grid on;
%  figure;
%  plot(scanSetOrgDCRCB, (abs(patternEstDCRCB)));
%  hold on;
% compNum=length(betaTrue);
% for compNo=1:compNum
%     plot([DOATrue(compNo) DOATrue(compNo) ], [0 (betaTrue(compNo)) ], ':');
%     hold on;
% end;
%  xlabel('DOA (deg)');
%  ylabel('Modulus of Complex Amplitude');
%  zoom on
%  %legend('DAS','Capon', 'APES');
%  axis([min(scanSetOrgAPES) max(scanSetOrgAPES) 0 5]);
%  myboldify1;
%  print('-deps',[savePath 'Fig_stverr_DCRCB' '.eps'])
  
  
 
  
  figure;
 plot(scanSetOrgDAS,20*log10(abs(patternEstDAS)));
 hold on;
compNum=length(betaTrue);
for compNo=1:compNum
    plot([DOATrue(compNo)], [20*log10(betaTrue(compNo)) ], 'o');
    hold on;
end;
 xlabel('DOA (deg)');
 ylabel('Modulus of Complex Amplitude (dB)');
%5 legend('DAS','Capon', 'APES');
 axis([min(scanSetOrgCAPON) max(scanSetOrgCAPON) -80 20]);%max(abs(patternEstAPES))]*1.2);
 myboldify1;
 print('-deps',[savePath 'Fig_stverr_DAS' '.eps']);
 
% grid on;
 figure;
 plot(scanSetOrgCAPON,20*log10(abs(patternEstCAPON)));
 hold on;
compNum=length(betaTrue);
for compNo=1:compNum
    plot([DOATrue(compNo) DOATrue(compNo)], [-80 20*log10(betaTrue(compNo)) ], ':');
    hold on;
end;
 xlabel('DOA (deg)');
 ylabel('Modulus of Complex Amplitude (dB)');
% legend('DAS','Capon', 'APES');
 axis([min(scanSetOrgCAPON) max(scanSetOrgCAPON) -80 20]);%max(abs(patternEstAPES))]*1.2);
 myboldify1;
 print('-deps',[savePath 'Fig_stverr_CAPON' '.eps']);
 
 
 % grid on;
 figure;
 plot(scanSetOrgAPES,20*log10(abs(patternEstAPES)));
 hold on;
compNum=length(betaTrue);
for compNo=1:compNum
    plot([DOATrue(compNo) DOATrue(compNo)], [-80 20*log10(betaTrue(compNo)) ], ':');
    hold on;
end;
 xlabel('DOA (deg)');
 ylabel('Modulus of Complex Amplitude (dB)');
 %legend('DAS','Capon', 'APES');
axis([min(scanSetOrgAPES) max(scanSetOrgAPES) -80 20]);%max(abs(patternEstAPES))]*1.2);
myboldify1;
 print('-deps',[savePath 'Fig_stverr_APES' '.eps']);
 
  % grid on;
 figure;
 plot(scanSetOrgRCB,20*log10(abs(patternEstRCB)));
 hold on;
compNum=length(betaTrue);
for compNo=1:compNum
    plot([ DOATrue(compNo) DOATrue(compNo)], [-80 20*log10(betaTrue(compNo)) ], ':');
    hold on;
end;
 xlabel('DOA (deg)');
 ylabel('Modulus of Complex Amplitude (dB)');
 zoom on
 %legend('DAS','Capon', 'APES');
 axis([min(scanSetOrgAPES) max(scanSetOrgAPES) -80 20]);
 myboldify1;
 print('-deps',[savePath 'Fig_stverr_RCB' '.eps']);
 
   % grid on;
 figure;
 plot(scanSetOrgDCRCB,20*log10(abs(patternEstDCRCB)));
 hold on;
compNum=length(betaTrue);
for compNo=1:compNum
    plot([DOATrue(compNo) DOATrue(compNo)], [-80 20*log10(betaTrue(compNo)) ], ':');
    hold on;
end;
 xlabel('DOA (deg)');
 ylabel('Modulus of Complex Amplitude (dB)');
 zoom on
 %legend('DAS','Capon', 'APES');
 axis([min(scanSetOrgAPES) max(scanSetOrgAPES) -80 20]);
 myboldify1;
 print('-deps',[savePath 'Fig_stverr_DCRCB' '.eps'])

 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 figure;
 plot(scanSetOrgCAPON,abs(CorCoeffEstCapon));
 hold on;

 xlabel('DOA (deg)');
 ylabel('GLR');
 %legend('DAS','Capon', 'APES');
 axis([min(scanSetOrgAPES) max(scanSetOrgAPES) 0 1.2]);%max(abs(patternEstAPES))]*1.2);
 myboldify1;
 print('-deps',[savePath  'Fig_stverr_Capon_correlationcoeff' '.eps']);
 

 figure;
 plot(scanSetOrgAPES,(CorCoeffEstAPES));
 hold on;

 xlabel('DOA (deg)');
 ylabel('GLR');
 %legend('DAS','Capon', 'APES');
 axis([min(scanSetOrgAPES) max(scanSetOrgAPES) 0 1.2]);%max(abs(patternEstAPES))]*1.2);
 myboldify1;
 print('-deps',[savePath  'Fig_stverr_APES_GLR' '.eps']);
 
 
 
  figure;
 plot(scanSetOrgRCB,abs(CorCoeffEstRCB));
 hold on;

 xlabel('DOA (deg)');
 ylabel('GLR');
 %legend('DAS','Capon', 'APES');
 axis([min(scanSetOrgAPES) max(scanSetOrgAPES) 0 1.2]);%max(abs(patternEstAPES))]*1.2);
 myboldify1;
 print('-deps',[savePath  'Fig_stverr_RCB_GLR' '.eps']);
 

 figure;
 plot(scanSetOrgDCRCB,abs(CorCoeffEstDCRCB));
 hold on;

 xlabel('DOA (deg)');
 ylabel('GLR');
 %legend('DAS','Capon', 'APES');
 axis([min(scanSetOrgAPES) max(scanSetOrgAPES) 0 1.2]);%max(abs(patternEstAPES))]*1.2);
 myboldify1;
 print('-deps',[savePath 'Fig_stverr_DCRCB_GLR' '.eps']);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [patternEst,scanSetOrg]=das(signal)

global save_path

global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global sampNum;
global waveform;
global scanSetOrg;


DOAEst=[];
betaEst=[];


scanSet=scanSetOrg; 
patternNum=length(scanSet);
patternEst=zeros(patternNum,1);
 
       
       for DOAScan=scanSet
        DOATmp=[DOAScan];
        
        txTmp=cumsum([0 txDist]);
        txStv=exp(-j*2*pi*txTmp' * sin(DOATmp*pi/180) );
        rxTmp=cumsum([0 rxDist]);
        rxStv=exp(-j*2*pi*rxTmp' * sin(DOATmp*pi/180) );
        
        a=rxStv;
        s=txStv.'*waveform;
        
        xBar=signal*s';
        betaTmp=a'*xBar/(norm(s)^2)/(norm(a)^2);
                      
        index=find(scanSet==DOAScan);
        patternEst(index)= betaTmp(end);
    end;
    
%         figure;
%         plot(scanSetOrg, abs(patternEst));
%         grid on;
        
%         DOAEstTmp=input('The peak DOA:')
%         DOAEst=[DOAEst, DOAEstTmp];
%         index=find(scanSet==DOAEstTmp);
%         scanSet=[scanSet(1:index-1) scanSet(index+1:end)];
        
 function [patternEst,CorCoeffEst,scanSetOrg]=CAPON(signal);

global save_path

global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global sampNum;
global waveform;
global scanSetOrg;


DOAEst=[];
betaEst=[];


scanSet=scanSetOrg; 
patternNum=length(scanSet);
patternEst=zeros(patternNum,1);
CorCoeffEst=zeros(patternNum,1);
       
       for DOAScan=scanSet
        DOATmp=[DOAScan];
        
        txTmp=cumsum([0 txDist]);
        txStv=exp(-j*2*pi*txTmp' * sin(DOATmp*pi/180) );
        rxTmp=cumsum([0 rxDist]);
        rxStv=exp(-j*2*pi*rxTmp' * sin(DOATmp*pi/180) );
        
        a=rxStv;
        s=txStv.'*waveform;
        
        xBar=signal*s';
   
        R=signal*signal';
       
        Rinv=pinv(R);
        betaTmp=a'*Rinv*xBar/(a'*Rinv*a)/(norm(s)^2);
               
        index=find(scanSet==DOAScan);
        patternEst(index)= betaTmp(end);
        
        
        s_est=a'*Rinv*signal/(a'*Rinv*a)/(norm(s)^2);
        temp=s_est*s'/norm(s_est)/norm(s);
        CorCoeffEst(index)=temp;
        
    end;
%         figure;
%         plot(scanSetOrg, abs(patternEst));
%         grid on;
        
%         DOAEstTmp=input('The peak DOA:')
%         DOAEst=[DOAEst, DOAEstTmp];
%         index=find(scanSet==DOAEstTmp);
%         scanSet=[scanSet(1:index-1) scanSet(index+1:end)];
        
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     
     
         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [patternEst,GLREst,scanSetOrg]=APES(signal);

global save_path

global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global sampNum;
global waveform;
global scanSetOrg;


DOAEst=[];
betaEst=[];


scanSet=scanSetOrg; 
patternNum=length(scanSet);
patternEst=zeros(patternNum,1);
CorCoeffEst=zeros(patternNum,1);
       
       for DOAScan=scanSet
        DOATmp=[DOAScan];
        
        txTmp=cumsum([0 txDist]);
        txStv=exp(-j*2*pi*txTmp' * sin(DOATmp*pi/180) );
        rxTmp=cumsum([0 rxDist]);
        rxStv=exp(-j*2*pi*rxTmp' * sin(DOATmp*pi/180) );
        
        a=rxStv;
        s=txStv.'*waveform;
        
        xBar=signal*s';
   
        R=signal*signal'/sampNum;
        T=(signal*signal'-xBar*xBar'/( norm(s)^2))./sampNum;
       % T=signal*signal'-(signal*waveform')*inv(waveform*waveform')*(signal*waveform')';
        
        Tinv=pinv(T);
        betaTmp=a'*Tinv*xBar/(a'*Tinv*a)/(norm(s)^2);
               
        index=find(scanSet==DOAScan);
        patternEst(index)= betaTmp(end);
        
        
       GLRTmp=(a'*inv(T)*xBar)*(xBar'*inv(R)*a)./(a'*inv(T)*a)/(norm(s)^2)/sampNum;

        GLREst(index)=GLRTmp;
    end;
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [patternEst,CorCoeffEst,scanSetOrg]=RCB(signal);

global save_path

global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global sampNum;
global waveform;
global scanSetOrg;
global epsilon;

DOAEst=[];
betaEst=[];


scanSet=scanSetOrg; 
patternNum=length(scanSet);
patternEst=zeros(patternNum,1);
CorCoeffEst=zeros(patternNum,1);
       
   for DOAScan=scanSet
        DOATmp=[DOAScan];
        
        txTmp=cumsum([0 txDist]);
        txStv=exp(-j*2*pi*txTmp' * sin(DOATmp*pi/180) );
        rxTmp=cumsum([0 rxDist]);
        rxStv=exp(-j*2*pi*rxTmp' * sin(DOATmp*pi/180) );
        
        a=rxStv;
       % s=txStv.'*waveform;
                        
        %xBar=signal*s';
   
        R=signal*signal'/sampNum;
        %R=signal*signal'-xBar*xBar'/( norm(s)^2);
        
        [a] = RCB_func(R,a,epsilon);
        
        
      %  txStv=a;
        s=txStv.'*waveform;
        xBar=signal*s';
                       
        Rinv=pinv(R) ;
        betaTmp=a'*Rinv*xBar/(a'*Rinv*a)/(norm(s)^2);
               
        index=find(scanSet==DOAScan);
        patternEst(index)= betaTmp(end);
        
     %  s_est=a'*Rinv*signal/(a'*Rinv*a)/(norm(s)^2);
      %  temp=s_est*s'/norm(s_est)/norm(s);
       % CorCoeffEst(index)=temp;
       
         Q_hat = 1/sampNum * (signal - a*betaTmp*s)*(signal - a*betaTmp*s)';
         GLRTmp=det(R)./det(Q_hat);
         CorCoeffEst(index)=GLRTmp;
    end;
    CorCoeffEst=1-1./CorCoeffEst;
    
%         figure;
%         plot(scanSetOrg, abs(patternEst));
%         grid on;
        
%         DOAEstTmp=input('The peak DOA:')
%         DOAEst=[DOAEst, DOAEstTmp];
%         index=find(scanSet==DOAEstTmp);
%         scanSet=[scanSet(1:index-1) scanSet(index+1:end)];
        
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
     
              %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [patternEst,CorCoeffEst,scanSetOrg]=DCRCB(signal);

global save_path

global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global sampNum;
global waveform;
global scanSetOrg;
global epsilon;

%epsilon=0.1
DOAEst=[];
betaEst=[];


scanSet=scanSetOrg; 
patternNum=length(scanSet);
patternEst=zeros(patternNum,1);
CorCoeffEst=zeros(patternNum,1);
 
       
   for DOAScan=scanSet
        DOATmp=[DOAScan];
        
        txTmp=cumsum([0 txDist]);
        txStv=exp(-j*2*pi*txTmp' * sin(DOATmp*pi/180) );
        rxTmp=cumsum([0 rxDist]);
        rxStv=exp(-j*2*pi*rxTmp' * sin(DOATmp*pi/180) );
        
        a=rxStv;
%         s=txStv.'*waveform;
%                         
%         xBar=signal*s';
   
        R=signal*signal'/sampNum;
        %R=signal*signal'-xBar*xBar'/( norm(s)^2);
         [a,W_DCRCB] = DCRCB_func(R,rxAntNum,a,epsilon,rxDist(1));
       
       % txStv=a;
        s=txStv.'*waveform;
        xBar=signal*s'; 
         
        Rinv=pinv(R) ;
        betaTmp=a'*Rinv*xBar/(a'*Rinv*a)/(norm(s)^2);
               
        index=find(scanSet==DOAScan);
        patternEst(index)= betaTmp(end);
        
        Q_hat = 1/sampNum * (signal - a*betaTmp*s)*(signal - a*betaTmp*s)';
       % GLRTmp=-sampNum*log(det(Q_hat))-sampNum*rxAntNum;
        GLRTmp=det(R)./det(Q_hat);
        
      %   s_est=a'*Rinv*signal/(a'*Rinv*a)/(norm(s)^2);
      %  temp=s_est*s'/norm(s_est)/norm(s);
        CorCoeffEst(index)=GLRTmp;
    end;
         CorCoeffEst=1-1./CorCoeffEst;

   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [DOAEst, betaEst, patternEst]=DGCProc(signal);

global save_path

global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global sampNum;
global waveform;

maxCompNum=3;

DOAEst=[];
betaEst=[];

scanSetOrg=[-90:90];
scanSet=scanSetOrg; 
patternNum=length(scanSet);
patternEst=zeros(patternNum,1);
for k=1:maxCompNum
    
       
       for DOAScan=scanSet
        DOATmp=[DOAEst DOAScan];
        
        txStv=exp(-j*2*pi*txDist*(0:txAntNum-1)' * sin(DOATmp*pi/180) );
        rxStv=exp(-j*2*pi*rxDist*(0:rxAntNum-1)' * sin(DOATmp*pi/180) );
        
        A=rxStv;
        S=txStv.'*waveform;
        
        GTmp=signal*S';
   
        T=signal*signal';%-GTmp*pinv(S*S')*GTmp';
       
        Tinv=pinv(T);
        betaTmp=(  (A'*Tinv*A).* ((S*S').') )\diag(A'*Tinv*GTmp);
        
        index=find(scanSet==DOAScan);
        patternEst(index)= betaTmp(end);
    end;
    
        figure;
        plot(scanSetOrg, abs(patternEst));
%         
%         DOAEstTmp=input('The peak DOA:')
%         DOAEst=[DOAEst, DOAEstTmp];
%         index=find(scanSet==DOAEstTmp);
%         scanSet=[scanSet(1:index-1) scanSet(index+1:end)];
        
    end;
    
     DOATmp=[-45 -30 10];
     txStv=exp(-j*2*pi*txDist*(0:txAntNum-1)' * sin(DOATmp*pi/180) );
     rxStv=exp(-j*2*pi*rxDist*(0:rxAntNum-1)' * sin(DOATmp*pi/180) );
     
     A=rxStv;
     S=txStv.'*waveform;
        
     GTmp=signal*S';
   
     T=signal*signal'-GTmp*pinv(S*S')*GTmp';
       
     Tinv=pinv(T);
     betaTmp=(  (A'*Tinv*A).* ((S*S').') )\diag(A'*Tinv*GTmp);