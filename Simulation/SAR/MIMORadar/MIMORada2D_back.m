function MIMORadar
close all;
clear all;

global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global save_path

%save_path='E:/workspace/program/MIMO Radar/data';
savePath='E:/Document/lzxu/my program/MIMO Radar/data/'
saveName='image_Conf1';
%path(path,save_path);

txAntNum=10;
rxAntNum=10;
txDist=0.5*ones(1,txAntNum-1);%[0.5 1. 1.5 4];%
rxDist=0.5*ones(1,rxAntNum-1);%[0.5 1. 1.5 4];%
sampNum=256;

% target parameters
rangeNum=128;
betaTrue=[1 1 4 5 3 4 5 1 10 10]%randn(1,targetNum);
targetNum=length(betaTrue);
DOATrue= round((2*rand(1, targetNum)-1)*50); %linspace(-25, 25, targetNum);%
rangeTrue= round( rand(1,targetNum)*63+32); %

scanSet=[-60:1:60];
ASNRdB=40; %dB
SNRAmp=10.^(ASNRdB/20)./rxAntNum.^2;

JammerAmp=0 ; %
DOAJammer=[0];
JammerNum=length(DOAJammer);

% waveform: hardmard codes
temp=[1];
for kk=1:ceil(log2(sampNum))
  temp=[temp temp; temp -1*temp];
end;
waveformSet=temp;
waveform=(waveformSet(1:txAntNum, 1:sampNum)+j*waveformSet(txAntNum+1:2*txAntNum, 1:sampNum))/sqrt(2);

mSeq=mseq(2,8,0,1);
waveform=waveform.*(ones(txAntNum,1)* [mSeq' 1]) ./sqrt(sampNum) ;



% source generator
%------------------------------------
signalPure=zeros(rxAntNum, sampNum+rangeNum);
for k=1:targetNum
   txTmp=cumsum([0 txDist]);
   v=exp(-j*2*pi*txTmp' * sin(DOATrue(k)*pi/180) );%  
   rxTmp=cumsum([0 rxDist]);
   rxStv=exp(-j*2*pi*rxTmp' * sin(DOATrue(k)*pi/180) );% 

   signalPure(:, (1:sampNum)+rangeTrue(k))=signalPure(:, (1:sampNum)+rangeTrue(k)) + rxStv*diag(betaTrue(targetNum))*(v.'*waveform);
end;

signal=signalPure;

 % correlation matrix 
%   Q_struct=zeros(rxAntNum,rxAntNum);
%    for row_no=1:rxAntNum
%       for col_no=1:rxAntNum
%           Q_struct(row_no,col_no)=0.9^abs(col_no-row_no)*exp(j*pi/2*(col_no-row_no));
%       end;
%     end;
   Q_struct=eye(rxAntNum,rxAntNum)
signal=signal +  (Q_struct^0.5)*(randn(rxAntNum, sampNum+rangeNum) +j *randn(rxAntNum, sampNum+rangeNum))/SNRAmp/sqrt(2) ;

% jammer 
rxTmp=cumsum([0 rxDist]);
rxStv=exp(-j*2*pi*rxTmp' * sin(DOAJammer*pi/180) );
jammerWaveform=((2*randint(JammerNum,sampNum+rangeNum)-1)+j*(2*randint(JammerNum,sampNum+rangeNum)-1))/sqrt(2)./sqrt(sampNum+rangeNum);
signal=signal+rxStv*jammerWaveform.*JammerAmp;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MIMO signal processing
loadFlag=0;
if loadFlag==1
    load([savePath saveName '_Capon.mat']);
else 
    [imageCapon]=fun_Capon(signal, waveform, rangeNum, scanSet);
    save([savePath saveName '_Capon.mat'], 'imageCapon','betaTrue','DOATrue','rangeTrue');
end;

if loadFlag==1
    load([savePath saveName '_Apes.mat']);
else 
    [imageApes]=fun_Apes(signal, waveform, rangeNum, scanSet);
    save([savePath saveName '_Apes.mat'], 'imageApes','betaTrue','DOATrue','rangeTrue');
end;

if loadFlag==1
    load([savePath saveName '_GLRT.mat']);
else 
    [imageGLRT]=fun_GLRT(signal, waveform, rangeNum, scanSet);
    save([savePath saveName '_GLRT.mat'], 'imageGLRT','betaTrue','DOATrue','rangeTrue');
end;

% if loadFlag==1
%     load([savePath saveName '_CAPESGLRT.mat']);
% else 
%     [imageCAPESGLRT]=fun_CAPESGLRT(imageCapon, imageApes, imageGLRT); 
%     save([savePath saveName '_CAPESGLRT.mat'], 'imageCAPESGLRT');
% end;
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure;
imagescdb1(imageCapon,20, 1:rangeNum,scanSet, 1,0);
drawnow;
hb=colorbar;
set(hb,'Position',  [0.8 0.42 0.02 0.2])
text(63.2,-1.8,'(dB)')
hold on
thetaSet=linspace(-pi, pi, 100);
xSet=2*cos(thetaSet); ySet=2*sin(thetaSet);
for k=1:targetNum
    plot(xSet+DOATrue(k), ySet+rangeTrue(k), 'r');
end;
print('-depsc',[savePath saveName '_Capon.eps']);

%------------
figure;
imagescdb1(imageApes,20, 1:rangeNum,scanSet, 1,0);
drawnow;
hb=colorbar;
set(hb,'Position',  [0.8 0.42 0.02 0.2])
text(63.2,-1.8,'(dB)')
hold on
thetaSet=linspace(-pi, pi, 100);
xSet=2*cos(thetaSet); ySet=2*sin(thetaSet);
for k=1:targetNum
    plot(xSet+DOATrue(k), ySet+rangeTrue(k), 'r');
end;
print('-depsc',[savePath saveName '_Apes.eps']);
 
%------------
figure;
imagescdb1(imageGLRT,1, 1:rangeNum,scanSet, 0,0);
drawnow;
hb=colorbar;
set(hb,'Position',  [0.8 0.42 0.02 0.2])
%text(63.2,-1.8,'(dB)')
hold on
thetaSet=linspace(-pi, pi, 100);
xSet=2*cos(thetaSet); ySet=2*sin(thetaSet);
for k=1:targetNum
    plot(xSet+DOATrue(k), ySet+rangeTrue(k), 'r');
end;
print('-depsc',[savePath saveName '_GLRT.eps']);
 
%------------
% figure;
% imagescdb1(imageCAPESGLRT,60, 1:rangeNum,scanSet, 1,0);
% drawnow;
% hb=colorbar;
% set(hb,'Position',  [0.8 0.42 0.02 0.2])
% text(63.2,-1.8,'(dB)')
% hold on
% thetaSet=linspace(-pi, pi, 100);
% xSet=2*cos(thetaSet); ySet=2*sin(thetaSet);
% for k=1:targetNum
%     plot(xSet+DOATrue(k), ySet+rangeTrue(k), 'r');
% end;
% print('-depsc',[savePath saveName '_CAPESGLRT.eps']);
 
 
      
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [imageEst]=fun_Capon(signal, waveform, rangeNum, scanSet)
global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global save_path

angleNum=length(scanSet);
sampNum=size(waveform,2);
 txTmp=cumsum([0 txDist]);
 rxTmp=cumsum([0 rxDist]);

imageEst=zeros( rangeNum, angleNum);
for k=1:rangeNum
   signalTmp=signal(:, (1:sampNum)+k-1); 
   R=signalTmp*signalTmp'/sampNum;
   Rinv=pinv(R);
    for n=1:angleNum
        DOATmp=scanSet(n);      
        v=exp(-j*2*pi*txTmp' * sin(DOATmp*pi/180) );
        a=exp(-j*2*pi*rxTmp' * sin(DOATmp*pi/180) );
        s=v.'*waveform;
        xBar=signalTmp*s'./sampNum;
        betaTmp= sampNum*a'*Rinv*xBar/(a'*Rinv*a)/(norm(s)^2);
          
        imageEst(k, n)=betaTmp;
    end;
end;

        
    
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [imageEst]=fun_Apes(signal, waveform, rangeNum, scanSet)
global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global save_path

angleNum=length(scanSet);
sampNum=size(waveform,2);
 txTmp=cumsum([0 txDist]);
 rxTmp=cumsum([0 rxDist]);

imageEst=zeros( rangeNum, angleNum);
for k=1:rangeNum
   signalTmp=signal(:, (1:sampNum)+k-1); 
   R=signalTmp*signalTmp'/sampNum;
   Rinv=pinv(R);
    for n=1:angleNum
        DOATmp=scanSet(n);      
        v=exp(-j*2*pi*txTmp' * sin(DOATmp*pi/180) );
        a=exp(-j*2*pi*rxTmp' * sin(DOATmp*pi/180) );
        s=v.'*waveform;
        xBar=signalTmp*s'/sampNum;
        
        Rinv_xBar=Rinv*xBar;
        Tinv=Rinv - Rinv_xBar*Rinv_xBar'./(xBar'*Rinv_xBar - norm(s).^2/sampNum);
        betaTmp=sampNum*a'*Tinv*xBar/(a'*Tinv*a)/(norm(s)^2);
          
        imageEst(k, n)=betaTmp;
    end;
end;
     
 
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [imageEst]=fun_GLRT(signal, waveform, rangeNum, scanSet)
global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global save_path

angleNum=length(scanSet);
sampNum=size(waveform,2);
 txTmp=cumsum([0 txDist]);
 rxTmp=cumsum([0 rxDist]);

imageEst=zeros( rangeNum, angleNum);
for k=1:rangeNum
   signalTmp=signal(:, (1:sampNum)+k-1); 
   R=signalTmp*signalTmp'/sampNum;
   Rinv=pinv(R);
    for n=1:angleNum
        DOATmp=scanSet(n);      
        v=exp(-j*2*pi*txTmp' * sin(DOATmp*pi/180) );
        a=exp(-j*2*pi*rxTmp' * sin(DOATmp*pi/180) );
        s=v.'*waveform;
        xBar=signalTmp*s'/sampNum;
        
        Rinv_xBar=Rinv*xBar;
         Tinv=Rinv - Rinv_xBar*Rinv_xBar'./(xBar'*Rinv_xBar - norm(s).^2/sampNum);
        GLR=1 - (a'*Rinv*a)./(a'*Tinv*a);
          
        imageEst(k, n)=GLR;
    end;
end;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%      
function [patternEst,scanSetOrg]=CAPES(patternEstAPES, DOAEst)

global save_path

global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global sampNum;
global waveform;
global scanSetOrg;

compNum=length(DOAEst);

betaEst=[];

scanSet=scanSetOrg; 
patternNum=length(scanSet);
patternEst=zeros(patternNum,1);


for k=1:compNum
    DOATmp=DOAEst(k);
    
    index=find(scanSet==DOATmp);
    patternEst(index)= patternEstAPES(index);
end;


 