function [lambda_new,error]=RCB_Newton(lambda,z,Gamma_vec,m,epsilon)

f=sum(abs(z).^2./(lambda*Gamma_vec + ones(m,1)).^2)-epsilon;
f_derivative= -2*sum(abs(z).^2.*Gamma_vec./(lambda*Gamma_vec + ones(m,1)).^3);

lambda_new=lambda-(f/f_derivative);
    
error=sum(abs(z).^2./(lambda_new*Gamma_vec + ones(m,1)).^2)-epsilon;


