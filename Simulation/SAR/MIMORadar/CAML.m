function [AmpEst]=CAML(signal, waveform, txDist, rxDist,  DOAEst)

[rxAntNum, sampNum]=size(signal);
txAntNum=length(txDist)+1;

betaEst=[];
 

txTmp=cumsum([0 txDist]);
txStv=exp(-j*2*pi*txTmp' * sin(DOAEst*pi/180) );% 
rxTmp=cumsum([0 rxDist]);
rxStv=exp(-j*2*pi*rxTmp' * sin(DOAEst*pi/180) );%
        
 A=rxStv;
 S=txStv.'*waveform;
        
GTmp=signal*S';
   
 T=signal*signal'-GTmp*pinv(S*S')*GTmp';
       
Tinv=pinv(T);
AmpEst=(  (A'*Tinv*A).* ((S*S').') )\diag(A'*Tinv*GTmp);
        
