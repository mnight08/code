 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 function [CRBDOA, CRBAmp]=calCRB(txDist,rxDist,beta,DOA,waveform,Q)
 % txDist: tx antenna locations relative to the 1st element.
 % rxDist: rx antenna locations relative to the 1st element
 % beta: amplitude
 % DOA: DOA in degree
 % waveform: 
 % Q: covariance matrix of noise and interference
 
 txAntNum=length(txDist)+1;
 rxAntNum=length(rxDist)+1;
 compNum=length(DOA);
 sampNum=size(waveform,2);
 
txTmp=cumsum([0 txDist]);
txStv=exp(-j*2*pi*txTmp' * sin(DOA*pi/180) );
txStv_Deff=(-j*2*pi*txTmp'*cos(DOA*pi/180)*pi/180) .* txStv;
rxTmp=cumsum([0 rxDist]);
rxStv=exp(-j*2*pi*rxTmp' * sin(DOA*pi/180) );
rxStv_Deff=(-j*2*pi*rxTmp'*cos(DOA*pi/180)*pi/180) .* rxStv;

Qinv=inv(Q);
Rss=waveform*waveform'; Rss=conj(Rss);

F11=(rxStv_Deff'*Qinv*rxStv_Deff).*(txStv'*Rss*txStv)...
   +(rxStv_Deff'*Qinv*rxStv).*(txStv'*Rss*txStv_Deff)...
    +(rxStv'*Qinv*rxStv_Deff).*(txStv_Deff'*Rss*txStv)...
    +(rxStv'*Qinv*rxStv).*(txStv_Deff'*Rss*txStv_Deff);
F11=F11.*(conj(beta.')*beta);

F12=(rxStv_Deff'*Qinv*rxStv).*(txStv'*Rss*txStv).*(conj(beta.')*ones(1,compNum))...
    +(rxStv'*Qinv*rxStv).*(txStv_Deff'*Rss*txStv).*(conj(beta.')*ones(1,compNum));
F21=F12';
F22=(rxStv'*Qinv*rxStv).*(txStv'*Rss*txStv);
F22inv=inv(F22);

CRBDOA=0.5*inv( real (F11-F12*F22inv*F21 ));
CRBAmp=F22inv +F22inv*F21*CRBDOA*F12*F22inv;


