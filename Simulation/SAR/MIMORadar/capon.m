function [patternEst, DOAEst, AmpEst]=capon(signal, waveform, txDist, rxDist, scanSet, compNum, DOATrue)

[rxAntNum, sampNum]=size(signal);
txAntNum=length(txDist)+1;
compNum=length(DOATrue);


patternNum=length(scanSet);
patternEst=zeros(patternNum,1);
 
       
  for DOAScan=scanSet
        DOATmp=[DOAScan];
        
        txTmp=cumsum([0 txDist]);
        txStv=exp(-j*2*pi*txTmp' * sin(DOATmp*pi/180) );
        rxTmp=cumsum([0 rxDist]);
        rxStv=exp(-j*2*pi*rxTmp' * sin(DOATmp*pi/180) );
        
        a=rxStv;
        s=txStv.'*waveform;
        
        xBar=signal*s';
   
        R=signal*signal';
       
        Rinv=pinv(R);
        betaTmp=a'*Rinv*xBar/(a'*Rinv*a)/(norm(s)^2);
               
        index=find(scanSet==DOAScan);
        patternEst(index)= betaTmp(end);
    end;
    
    cost=abs(patternEst);
    DOAEst=zeros(compNum,1);
    AmpEst=zeros(compNum,1);
    for k=1:compNum
        if k==1
            index_lower=1;
        else
            [tmp index_lower]=min( abs(scanSet - mean(DOATrue(k-1:k))) );
        end;
        
        if k==compNum
            index_upper=length(scanSet);
        else 
             [tmp index_upper]=min( abs(scanSet - mean(DOATrue(k:k+1))) );
         end;
         
         
        [peak index]=max(cost(index_lower:index_upper));
        DOAEst(k)=scanSet(index+index_lower-1);
        AmpEst(k)=patternEst(index+index_lower-1);
    end;