function MIMORadarML
close all;
clear all;
%save_path='E:/workspace/program/MIMO Radar/data';
savePath='E:/Document/lzxu/my program/MIMO Radar/dataCRB/'

global trialNum;
global AmpTrue;


trialNum=500;
AmpTrue=[4 4 1];
DOATrue=[-35 -25 -15];

SNRdBSet=[-30:5:10]; %dB
sampNum=128;


tic


saveName=[savePath 'MSEvsL_3comp_SampNum128'];

%----------------------------
loadflag=1;
saveNameTmp=[saveName '_SIMO.mat']
if loadflag==0
    disp('*****************SIMO*****************')
    txAntNum=1;
    rxAntNum=10;
    [CRBDOA1,CRBAmp1, MSEDOA1, MSEAmp1, biasDOA1, biasAmp1]=SIMORadarML_SNR(rxAntNum,SNRdBSet,sampNum);
    save(saveNameTmp,  'CRBDOA1','CRBAmp1', 'MSEDOA1', 'MSEAmp1','biasDOA1', 'biasAmp1');
   else 
       load(saveNameTmp);
end;


%------------------------------------
loadflag=1;
saveNameTmp=[saveName '_MIMOPara.mat']
if loadflag==0
%      disp('*****************MIMO*****************')
    txAntNum=10;
    rxAntNum=10;
    [CRBDOA_Para,CRBAmp_Para, MSEDOA_Para, MSEAmp_Para, MSEDOACapon_Para, MSEAmpCAML_Para]=MIMORadarML_Parallel_SNR(txAntNum,rxAntNum,SNRdBSet,sampNum);
% 
     save(saveNameTmp, 'CRBDOA_Para', 'CRBAmp_Para', 'MSEDOA_Para', 'MSEAmp_Para','MSEDOACapon_Para', 'MSEAmpCAML_Para');
 else load(saveNameTmp);
 end;
 
 %-------------------------------
 loadflag=1;
saveNameTmp=[saveName '_MIMOSerial.mat']
if loadflag==0
          %      disp('*****************MIMO*****************')
    txAntNum=10;
    rxAntNum=10;
    [CRBDOA_Seri,CRBAmp_Seri, MSEDOA_Seri, MSEAmp_Seri]=MIMORadarML_serial_SNR(txAntNum,rxAntNum,SNRdBSet,sampNum);
% 
     save(saveNameTmp, 'CRBDOA_Seri', 'CRBAmp_Seri', 'MSEDOA_Seri', 'MSEAmp_Seri');
                    
 else 
     load(saveNameTmp);
 end;
 
    
compNum=size(CRBDOA_Seri,1);


for compNo=1:compNum
    figure;
   % semilogy(SNRdBSet, sqrt(MSEDOA1(compNo,:)),'--+',SNRdBSet, sqrt(CRBDOA1(compNo,:)),'--'); hold on;
     semilogy(SNRdBSet, sqrt(MSEDOACapon_Para(compNo,:)),'-*',SNRdBSet, sqrt(MSEDOA_Para(compNo,:)),'-.o');
     hold on;
     semilogy(SNRdBSet, sqrt(MSEDOA_Seri(compNo,:)),'-.>', SNRdBSet, sqrt(CRBDOA_Seri(compNo,:)),'-');     
     legend('Capon','Para. AML',  'Serial AML', 'CRB');
     xlabel('SNR (dB)');
     ylabel('RMSE');
     axis([min(SNRdBSet) max(SNRdBSet) 1e-3 10]);
     myboldify1;
     
     tmp=[saveName 'CRB_SNRvsRMSE_DOA' num2str(compNo)];
     print('-deps',[tmp '.eps']);
     
%      figure;
%     % semilogx(sampNumSet, biasDOA1(compNo,:),'--'); hold on;
%      semilogx(sampNumSet, biasDOA2(compNo,:),'-'); 
%      legend('SIMO Bias','MIMO Bias');
%      xlabel('Number of Samples');
%      ylabel('Bias');
%      
%      tmp=['AMLCRB_bias_DOA_' num2str(compNo)];
%      print('-deps',[savePath tmp '.eps']);
 end;
 
for compNo=1:compNum
    figure;
   %  semilogy(SNRdBSet, sqrt(MSEAmp1(compNo,:)),'--+',SNRdBSet, sqrt(CRBAmp1(compNo,:)),'--'); hold on;
     semilogy(SNRdBSet, sqrt(MSEAmpCAML_Para(compNo,:)), '-*', SNRdBSet, sqrt(MSEAmp_Para(compNo,:)),'-.o'); 
     hold on;
     semilogy(SNRdBSet, sqrt(MSEAmp_Seri(compNo,:)),'-.>', SNRdBSet, sqrt(CRBAmp_Seri(compNo,:)),'-');     
     legend('CAML', 'Para. AML', 'Serial AML', 'CRB');
     xlabel('SNR (dB)');
     ylabel('RMSE');
     axis([min(SNRdBSet) max(SNRdBSet) 1e-3 10]);
     myboldify1;
     
     tmp=[saveName 'CRB_SNRvsRMSE_Amp_' num2str(compNo)];
     print('-deps',[tmp '.eps']);
     
     
%          figure;
%      semilogx(sampNumSet, real(biasAmp1(compNo,:)),'--'); hold on;
%    %   semilogx(sampNumSet, biasAmp2(compNo,:),'--');
%     % semilogy( sampNumSet, sqrt(MSEAmp2(compNo,:)),'-o', sampNumSet, sqrt(CRBAmp2(compNo,:)),'-x');
%      legend('SIMO Bias','MIMO Bias');
%      xlabel('Number of Samples');
%      ylabel('Bias');
%      
%      tmp=['AMLCRB_bias_Amp_' num2str(compNo)];
%      print('-deps',[savePath tmp '.eps']);
 end;
 

  %%%%%%%%%%%%%%%%%%%%%%%
function [CRBDOA,CRBAmp, MSEDOA, MSEAmp,biasDOA, biasAmp]=SIMORadarML_SNR(rxAntNum,SNRdBSet,sampNum)
global trialNum;
global AmpTrue;
global DOATrue;

txAntNum=1;

txDist=0.5*ones(1,txAntNum-1);
rxDist=0.5*ones(1,rxAntNum-1);%[0.5 1. 1.5 4];%

compNum=length(DOATrue);

% JammerdB=60; %dB
% DOAJammer=5;

SNRNum=length(SNRdBSet);

% waveform: hardmard codes
temp=[1];
for kk=1:ceil(log2(sampNum))
  temp=[temp temp; temp -1*temp];
end;
waveformSet=temp;

%waveform=(randn(txAntNum,sampNum)+j*randn(txAntNum, sampNum) )/sqrt(2)./sqrt(txAntNum);
% correlation matrix 
  Q_struct=zeros(rxAntNum,rxAntNum);
   for row_no=1:rxAntNum
      for col_no=1:rxAntNum
          Q_struct(row_no,col_no)=0.9^abs(col_no-row_no)*exp(j*pi/2*(col_no-row_no));
      end;
    end;
 % Q_struct=eye(rxAntNum);
  
  %%%%%%%%%%%%%%%%%%%%  
 CRBDOA=[];
 CRBAmp=[];
 MSEDOA=[];
 MSEAmp=[];
 biasDOA=[];
 biasAmp=[];
 
  for SNRIndex= 1:SNRNum
     SNRdB=SNRdBSet(SNRIndex);
      Q=Q_struct./(10.^(SNRdB/10) );
      
      waveform=(waveformSet(1:txAntNum, 1:sampNum)+j*waveformSet(txAntNum+1:2*txAntNum, 1:sampNum))/sqrt(2)./sqrt(txAntNum);
      
      toc 
      SNRIndex
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % calculate CRB
    [CRBDOATmp, CRBAmpTmp]=calCRB_serial(txDist,rxDist,AmpTrue,DOATrue,waveform,Q);
    CRBDOATmp=diag(CRBDOATmp);
    CRBAmpTmp=diag(CRBAmpTmp);
    
    CRBDOA=[CRBDOA,CRBDOATmp];
    CRBAmp=[CRBAmp, CRBAmpTmp];
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % do ML estimator
    DOASearchGrid=sqrt(CRBDOATmp)/5;
    
    SqrErrDOA=zeros(compNum,1);
    SqrErrAmp=zeros(compNum,1);
    ErrDOA=zeros(compNum,1);
    ErrAmp=zeros(compNum,1);
    
    for trialNo=1:trialNum
        
        % source generator
        signal=sourceGen(txDist,rxDist,AmpTrue,DOATrue,waveform,Q);
    
        % ML estimator
        DOAInit=DOATrue+(2*rand(1,compNum)-1)*2.5;
        [DOAEst, AmpEst]=AML_Serial(signal, txDist, rxDist, waveform, DOASearchGrid, DOAInit);
  
       SqrErrDOA=SqrErrDOA+(abs(DOAEst-DOATrue.')).^2;
       SqrErrAmp=SqrErrAmp+(abs(AmpEst-AmpTrue.')).^2;
       ErrDOA=ErrDOA+(DOAEst-DOATrue.');
       ErrAmp=ErrAmp+(AmpEst-AmpTrue.');
   end;
   
   MSEDOA=[MSEDOA, SqrErrDOA/trialNum];
   MSEAmp=[MSEAmp, SqrErrAmp/trialNum];
   biasDOA=[biasDOA, ErrDOA/trialNum];
   biasAmp=[biasAmp, ErrAmp/trialNum];
    
 end;
 
 
 

 
%%%%%%%%%%%%%%%%%%%%%%%
function [CRBDOA,CRBAmp, MSEDOA, MSEAmp, MSEDOACapon, MSEAmpCAML]=MIMORadarML_Parallel_SNR(txAntNum,rxAntNum,SNRdBSet,sampNum)
global trialNum;
global AmpTrue;
global DOATrue;
txDist= 0.5*ones(1,txAntNum-1);%
rxDist=0.5*ones(1,rxAntNum-1);%[0.5 1. 1.5 4];%

compNum=length(DOATrue);


% JammerdB=60; %dB
% DOAJammer=5;

SNRNum=length(SNRdBSet);

% waveform: hardmard codes
temp=[1];
for kk=1:ceil(log2(sampNum))
  temp=[temp temp; temp -1*temp];
end;
waveformSet=temp;

%waveform=(randn(txAntNum,sampNum)+j*randn(txAntNum, sampNum) )/sqrt(2)./sqrt(txAntNum);
% correlation matrix 
  Q_struct=zeros(rxAntNum,rxAntNum);
   for row_no=1:rxAntNum
      for col_no=1:rxAntNum
          Q_struct(row_no,col_no)=0.9^abs(col_no-row_no)*exp(j*pi/2*(col_no-row_no));
      end;
    end;

  %%%%%%%%%%%%%%%%%%%%  
 CRBDOA=[];
 CRBAmp=[];
 MSEDOA=[];
 MSEAmp=[];
 MSEDOACapon=[];
 MSEAmpCAML=[];
 
  for SNRIndex= 1:SNRNum
      SNRdB=SNRdBSet(SNRIndex);
      Q=Q_struct./(10.^(SNRdB/10) );
      waveform=(waveformSet(1:txAntNum, 1:sampNum)+j*waveformSet(txAntNum+1:2*txAntNum, 1:sampNum))/sqrt(2)./sqrt(txAntNum);
      
      toc 
      SNRIndex
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % calculate CRB
    [CRBDOATmp, CRBAmpTmp]=calCRB(txDist,rxDist,AmpTrue,DOATrue,waveform,Q);
    CRBDOATmp=diag(CRBDOATmp);
    CRBAmpTmp=diag(CRBAmpTmp);
    
    CRBDOA=[CRBDOA,CRBDOATmp];
    CRBAmp=[CRBAmp, CRBAmpTmp];
    
       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % do ML estimator
    DOASearchGrid=sqrt(CRBDOATmp)/5;
    
    ErrDOA=zeros(compNum,1);
    ErrAmp=zeros(compNum,1);
    
    ErrDOACapon=zeros(compNum,1);
    ErrAmpCAML=zeros(compNum,1);
    
    for trialNo=1:trialNum
        
        % source generator
        signal=sourceGen(txDist,rxDist,AmpTrue,DOATrue,waveform,Q);
    
        % capon estimator
        scanSet=[];
        for k=1:compNum
          scanSet=[scanSet (DOATrue(k)-2: DOASearchGrid(k):DOATrue(k)+2)];
      end;
      
        [patternEstCapon, DOAEstCapon, AmpEstCapon]=capon(signal, waveform, txDist, rxDist, scanSet, compNum, DOATrue);
        
        % CAML
        [AmpEstCAML]=CAML(signal, waveform, txDist, rxDist,  DOAEstCapon.');
        
        % ML estimator
        [DOAEst, AmpEst]=AML_Parallel(signal, txDist, rxDist, waveform, DOASearchGrid, DOAEstCapon.');
        
       ErrDOA=ErrDOA+(abs(DOAEst-DOATrue.')).^2;
       ErrAmp=ErrAmp+(abs(AmpEst-AmpTrue.')).^2;
       
       ErrDOACapon=ErrDOACapon+(abs(DOAEstCapon-DOATrue.')).^2;
       ErrAmpCAML=ErrAmpCAML+(abs(AmpEstCAML-AmpTrue.')).^2;
   end;
   
   MSEDOA=[MSEDOA, ErrDOA/trialNum];
   MSEAmp=[MSEAmp, ErrAmp/trialNum];
   MSEDOACapon=[MSEDOACapon, ErrDOACapon/trialNum];
   MSEAmpCAML=[MSEAmpCAML, ErrAmpCAML/trialNum];
   
   
    
 end;
 
 
 %%%%%%%%%%%%%%%%%%%%%%%
function [CRBDOA,CRBAmp, MSEDOA, MSEAmp]=MIMORadarML_serial_SNR(txAntNum,rxAntNum,SNRdBSet,sampNum)
global trialNum;
global AmpTrue;
global DOATrue;
txDist= 0.5*ones(1,txAntNum-1);%
rxDist=0.5*ones(1,rxAntNum-1);%[0.5 1. 1.5 4];%

compNum=length(DOATrue);

% JammerdB=60; %dB
% DOAJammer=5;

SNRNum=length(SNRdBSet);

% waveform: hardmard codes
temp=[1];
for kk=1:ceil(log2(sampNum))
  temp=[temp temp; temp -1*temp];
end;
waveformSet=temp;

%waveform=(randn(txAntNum,sampNum)+j*randn(txAntNum, sampNum) )/sqrt(2)./sqrt(txAntNum);
% correlation matrix 
  Q_struct=zeros(rxAntNum,rxAntNum);
   for row_no=1:rxAntNum
      for col_no=1:rxAntNum
          Q_struct(row_no,col_no)=0.9^abs(col_no-row_no)*exp(j*pi/2*(col_no-row_no));
      end;
    end;

  %%%%%%%%%%%%%%%%%%%%  
 CRBDOA=[];
 CRBAmp=[];
 MSEDOA=[];
 MSEAmp=[];
 
  for SNRIndex= 1:SNRNum
      SNRdB=SNRdBSet(SNRIndex);
      Q=Q_struct./(10.^(SNRdB/10) );
      waveform=(waveformSet(1, 1:sampNum)+j*waveformSet(txAntNum+1, 1:sampNum))/sqrt(2)./sqrt(txAntNum);
      
      toc 
      SNRIndex
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % calculate CRB
    [CRBDOATmp, CRBAmpTmp]=calCRB_serial(txDist,rxDist,AmpTrue,DOATrue,waveform,Q);
    CRBDOATmp=diag(CRBDOATmp);
    CRBAmpTmp=diag(CRBAmpTmp);
    
    CRBDOA=[CRBDOA,CRBDOATmp];
    CRBAmp=[CRBAmp, CRBAmpTmp];
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % do ML estimator
    DOASearchGrid=sqrt(CRBDOATmp)/5;
    
    ErrDOA=zeros(compNum,1);
    ErrAmp=zeros(compNum,1);
    
    for trialNo=1:trialNum
        
        % source generator
        signal=sourceGen_serial(txDist,rxDist,AmpTrue,DOATrue,waveform,Q);
    
        % ML estimator
        % capon estimator
        scanSet=[];
        for k=1:compNum
          scanSet=[scanSet (DOATrue(k)-2: DOASearchGrid(k):DOATrue(k)+2)];
       end;
      
    %   [patternEstCapon, DOAEstCapon, AmpEstCapon]=capon(sum(signal,3), waveform, txDist, rxDist, scanSet, compNum, DOATrue);
        DOAInit=DOATrue+(rand(1,compNum)*2-1)*2.5;
        [DOAEst, AmpEst]=AML_Serial(signal, txDist, rxDist, waveform, DOASearchGrid, DOAInit);
        
       ErrDOA=ErrDOA+(abs(DOAEst-DOATrue.')).^2;
       ErrAmp=ErrAmp+(abs(AmpEst-AmpTrue.')).^2;
   end;
   
   MSEDOA=[MSEDOA, ErrDOA/trialNum];
   MSEAmp=[MSEAmp, ErrAmp/trialNum];
    
 end;
 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% source generateor
function signal=sourceGen(txDist,rxDist,betaTrue,DOATrue,waveform,Q)
sampNum=size(waveform,2);
rxAntNum=length(rxDist)+1;
%------------------------------------
txTmp=cumsum([0 txDist]);
txStv=exp(-j*2*pi*txTmp' * sin(DOATrue*pi/180) );% 
rxTmp=cumsum([0 rxDist]);
rxStv=exp(-j*2*pi*rxTmp' * sin(DOATrue*pi/180) );%  

signalPure=rxStv*diag(betaTrue)*(txStv.'*waveform);

signal=signalPure +  Q^(0.5)*(randn(rxAntNum, sampNum) +j *randn(rxAntNum, sampNum))/sqrt(2) ;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% source generateor
function signal=sourceGen_serial(txDist,rxDist,betaTrue,DOATrue,waveform,Q)
sampNum=size(waveform,2);
rxAntNum=length(rxDist)+1;
txAntNum=length(txDist)+1;
%------------------------------------
txTmp=cumsum([0 txDist]);
txStv=exp(-j*2*pi*txTmp' * sin(DOATrue*pi/180) );% 
rxTmp=cumsum([0 rxDist]);
rxStv=exp(-j*2*pi*rxTmp' * sin(DOATrue*pi/180) );% 

signal=zeros(rxAntNum,sampNum,txAntNum);
for tranNo=1:txAntNum
    signalPure=rxStv*diag(betaTrue)*(txStv(tranNo,:).'*waveform);

    signal(:,:,tranNo)=signalPure +  Q^(0.5)*(randn(rxAntNum, sampNum) +j *randn(rxAntNum, sampNum))/sqrt(2) ;
end;


%%%%%%%%%%%%%%%%%%%%5
%ML estimator for parallelly transmitting MIMO Radar
function [DOAEst, AmpEst]=AML_Parallel(signal, txDist, rxDist, waveform, DOASearchGrid, DOAInit);

compNum=length(DOAInit);
iterNum=100;
searchNum=100;

txTmp=cumsum([0 txDist]);
rxTmp=cumsum([0 rxDist]);

G=signal*waveform';
Rss=waveform*waveform';
T=signal*signal'-G*(Rss\G');
Tinv=pinv(T);



% Cyclic maximiazation

DOAEst=DOAInit;
At=exp(-j*2*pi*txTmp' * sin(DOAInit*pi/180) );
Ar=exp(-j*2*pi*rxTmp' * sin(DOAInit*pi/180) );
for iterNo=1:iterNum
    exitflag=1;
   for compNo=1:compNum
       
       DOACandidate=(-1*searchNum:searchNum)*DOASearchGrid(compNo) + DOAEst(compNo);
       costfunction=[];
       for DOATmp=DOACandidate
           at=exp(-j*2*pi*txTmp' * sin(DOATmp*pi/180) );
           ar=exp(-j*2*pi*rxTmp' * sin(DOATmp*pi/180) );
           
           At(:,compNo)=at;
           Ar(:,compNo)=ar;
           
%            temp1=G*conj(At);
%            temp2=At.'*Rss*conj(At);
%            T=signal*signal'- temp1*(temp2\temp1');
%            Tinv=inv(T);
           
           tmp1=diag(Ar'*Tinv*G*conj(At));
           tmp2=(Ar'*Tinv*Ar).*(At'*Rss*At);
           costTmp=tmp1'*(tmp2\tmp1);
           
           costfunction=[costfunction costTmp];
       end;
       
       [peak index]=max(real(costfunction));
       exitflag=exitflag&(index==(searchNum+1));
       
       DOATmp=DOACandidate(index);
       at=exp(-j*2*pi*txTmp' * sin(DOATmp*pi/180) );
       ar=exp(-j*2*pi*rxTmp' * sin(DOATmp*pi/180) );
           
       At(:,compNo)=at;
       Ar(:,compNo)=ar;
       
       DOAEst(compNo)=DOATmp;
   end;
   
   if exitflag==1
       break;
   end;
end;

tmp1=diag(Ar'*Tinv*G*conj(At));
tmp2=(Ar'*Tinv*Ar).*(At'*Rss*At);
AmpEst=tmp2\tmp1;
DOAEst=DOAEst.';


%ML estimator for parallelly transmitting MIMO Radar
function [DOAEst, AmpEst]=AML_Serial(signal, txDist, rxDist, waveform, DOASearchGrid, DOAInit);

[rxAntNum, sampNum, txAntNum]=size(signal);
compNum=length(DOAInit);
iterNum=100;
searchNum=100;

txTmp=cumsum([0 txDist]);
At=exp(-j*2*pi*txTmp' * sin(DOAInit*pi/180) );
rxTmp=cumsum([0 rxDist]);
Ar=exp(-j*2*pi*rxTmp' * sin(DOAInit*pi/180) );

G=zeros(rxAntNum, txAntNum);
R=zeros(rxAntNum,rxAntNum);
for k=1:txAntNum
    temp=squeeze(signal(:,:,k));
    G(:,k)=temp*waveform';
    
    R=R+temp*temp';
end;

rss=waveform*waveform';

T=R-G*G'/rss;
Tinv=pinv(T);

DOAEst=DOAInit;
for iterNo=1:iterNum
    exitflag=1;
   for compNo=1:compNum
       
       DOACandidate=(-1*searchNum:searchNum)*DOASearchGrid(compNo) + DOAEst(compNo);
       costfunction=[];
       for DOATmp=DOACandidate
           at=exp(-j*2*pi*txTmp' * sin(DOATmp*pi/180) );
           ar=exp(-j*2*pi*rxTmp' * sin(DOATmp*pi/180) );
           
           At(:,compNo)=at;
           Ar(:,compNo)=ar;
           
          
           tmp1=diag(Ar'*Tinv*G*conj(At));
           tmp2=(Ar'*Tinv*Ar).*(At'*At)*rss;
           costTmp=tmp1'*(tmp2\tmp1);
           
           costfunction=[costfunction costTmp];
       end;
       
       [peak index]=max(real(costfunction));
       exitflag=exitflag&(index==(searchNum+1));
       
       DOATmp=DOACandidate(index);
       at=exp(-j*2*pi*txTmp' * sin(DOATmp*pi/180) );
       ar=exp(-j*2*pi*rxTmp' * sin(DOATmp*pi/180) );
           
       At(:,compNo)=at;
       Ar(:,compNo)=ar;
       
       DOAEst(compNo)=DOATmp;
   end;
   
   if exitflag==1
       break;
   end;
end;

tmp1=diag(Ar'*Tinv*G*conj(At));
tmp2=(Ar'*Tinv*Ar).*(At'*rss*At);
AmpEst=tmp2\tmp1;
DOAEst=DOAEst.';



