function MIMORadar
close all;
clear all;

global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global sampNum;
global save_path
global waveform;
global scanSetOrg;
%save_path='E:/workspace/program/MIMO Radar/data';
savePath='E:/Document/lzxu/my program/MIMO Radar/data/'
%path(path,save_path);

txAntNum=10;
rxAntNum=10;
txDist=0.5*ones(1,txAntNum-1);%[0.5 1. 1.5 4];%
rxDist=0.5*ones(1,rxAntNum-1);%[0.5 1. 1.5 4];%
% sampNum=256;
betaTrue=[4 4 1];
DOATrue=[-35 -25 -15 ];
compNum=length(DOATrue);
scanSetOrg=[-60:0.05:60];
SNRdB=-10;
SNR=10.^(SNRdB/20)
%SNRSetdB=[-20:5: 30]; %dB
%SNRSet=10.^(SNRSetdB/20);
%snrNum=length(SNRSetdB);
sampNumSet=[16 32 64 128 256 512 1024];
testPoiNum=length(sampNumSet);

MonteCarloNum=5;

JammerAmp=0; %dB
DOAJammer=5;

% waveform: hardmard codes

% source generator
%------------------------------------
    txTmp=cumsum([0 txDist]);
    txStv=exp(-j*2*pi*txTmp' * sin(DOATrue*pi/180) );%  + 0.1*(randn(txAntNum, compNum) +j*randn(txAntNum, compNum) )/sqrt(2);
    rxTmp=cumsum([0 rxDist]);
    rxStv=exp(-j*2*pi*rxTmp' * sin(DOATrue*pi/180) );%  + 0.1*(randn(txAntNum, compNum) +j*randn(txAntNum, compNum) )/sqrt(2);

 % correlation matrix 
  Q_struct=zeros(rxAntNum,rxAntNum);
   for row_no=1:rxAntNum
      for col_no=1:rxAntNum
          Q_struct(row_no,col_no)=0.9^abs(col_no-row_no)*exp(j*pi/2*(col_no-row_no));
      end;
    end;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 AmpBiasDAS=zeros(testPoiNum,compNum);
 AmpBiasCapon=zeros(testPoiNum,compNum);
 AmpBiasAPES=zeros(testPoiNum,compNum);
 AmpBiasDGC=zeros(testPoiNum,compNum);    
  
 AmpMSEDAS=zeros(testPoiNum,compNum);
 AmpMSECapon=zeros(testPoiNum,compNum);
 AmpMSEAPES=zeros(testPoiNum,compNum);
 AmpMSEDGC=zeros(testPoiNum,compNum);
 for testpoino=1:testPoiNum
     sampNum=sampNumSet(testpoino);
     
     temp=[1];
    for kk=1:ceil(log2(sampNum))
        temp=[temp temp; temp -1*temp];
    end;
    waveformSet=temp;
    waveform=waveformSet(1:txAntNum, 1:sampNum);
    %waveform=(( 2*rand(txAntNum, sampNum) -1) + j*(2*rand(txAntNum, sampNum) -1) )/sqrt(2);

    

signalPure=rxStv*diag(betaTrue)*(txStv.'*waveform);

     
     for mont=1:MonteCarloNum
         signal=signalPure +  sqrt(Q_struct)*(randn(rxAntNum, sampNum) +j *randn(rxAntNum, sampNum))/SNR/sqrt(2) ;
    % generator 
    rxTmp=cumsum([0 rxDist]);
    rxStvJammer=exp(-j*2*pi*rxTmp' * sin(DOAJammer*pi/180) );
    jammerWaveform=((2*randint(1,sampNum)-1)+j*(2*randint(1,sampNum)-1))/sqrt(2);
    signal=signal+rxStvJammer*jammerWaveform*JammerAmp;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % MIMO signal processing
    [AmpErr]=dasEst(signal,txStv,rxStv,betaTrue.');
    AmpBiasDAS(testpoino,:)=AmpBiasDAS(testpoino,:)+AmpErr;
    AmpMSEDAS(testpoino,:)=AmpMSEDAS(testpoino,:)+abs(AmpErr).^2;
    
    [AmpErr]=caponEst(signal,txStv,rxStv,betaTrue.');
     AmpBiasCapon(testpoino,:)=AmpBiasCapon(testpoino,:)+AmpErr;
     AmpMSECapon(testpoino,:)=AmpMSECapon(testpoino,:)+abs(AmpErr).^2;
     
    [AmpErr]=APESEst(signal,txStv,rxStv,betaTrue.');
     AmpBiasAPES(testpoino,:)=AmpBiasAPES(testpoino,:)+AmpErr;
     AmpMSEAPES(testpoino,:)=AmpMSEAPES(testpoino,:)+abs(AmpErr).^2;
    
    [AmpErr]=DGCEst(signal,txStv,rxStv,betaTrue.');
       AmpBiasDGC(testpoino,:)=AmpBiasDGC(testpoino,:)+AmpErr;
     AmpMSEDGC(testpoino,:)=AmpMSEDGC(testpoino,:)+abs(AmpErr).^2;
     
   end;
end;
AmpBiasDAS=real(AmpBiasDAS)/MonteCarloNum;
AmpBiasCapon=real(AmpBiasCapon)/MonteCarloNum;
AmpBiasAPES=real(AmpBiasAPES)/MonteCarloNum;
AmpBiasDGC=real(AmpBiasDGC)/MonteCarloNum;

AmpMSEDAS=AmpMSEDAS/MonteCarloNum;
AmpMSECapon=AmpMSECapon/MonteCarloNum;
AmpMSEAPES=AmpMSEAPES/MonteCarloNum;
AmpMSEDGC=AmpMSEDGC/MonteCarloNum;
 
  
  for k=1:compNum
         figure;
  plot( sampNumSet,AmpBiasDAS(:,k),'--S',...
            sampNumSet,AmpBiasCapon(:,k),'-->',...
            sampNumSet,AmpBiasAPES(:,k),'--o',...
            sampNumSet,AmpBiasDGC(:,k),'-');
      
  myboldify1;
  xlabel('number of snapshots');
  ylabel('Bias')
  legend('DAS','Capon','APES','DGC',0);
  %axis([SNR_set_dB(1),SNR_set_dB(end),1e-5,1e-1]);    
   print('-deps',[savePath 'fig_BiasvsSNR_comp' num2str(k) '.eps']);
end;

 for k=1:compNum
      figure;
      semilogy(sampNumSet,AmpMSEDAS(:,k),'--S',...
            sampNumSet,AmpMSECapon(:,k),'-->',...
            sampNumSet,AmpMSEAPES(:,k),'--o',...
            sampNumSet,AmpMSEDGC(:,k),'-');
      
  myboldify1;
  xlabel('number of snapshots');
  ylabel('MSE')
  legend('DAS','Capon','APES','DGC',0);
  %axis([SNR_set_dB(1),SNR_set_dB(end),1e-5,1e-1]);
  print('-deps',[savePath 'fig_MSEvsSNR_' num2str(k) '.eps']);
end;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [AmpErr]=dasEst(signal,txStv,rxStv,beta)
global waveform;
        
compNum=length(beta);
AmpErr=zeros(1,compNum);
for compNo=1:compNum
        
 a=rxStv(:,compNo);
 s=txStv(:,compNo).'*waveform;
        
 xBar=signal*s';
 betaTmp=a'*xBar/(norm(s)^2)/(norm(a)^2);
 
  AmpErr(compNo)=betaTmp-beta(compNo);
end;

          
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [AmpErr]=caponEst(signal,txStv,rxStv,beta)
global waveform;


compNum=length(beta);
AmpErr=zeros(1,compNum);
for compNo=1:compNum
        
 a=rxStv(:,compNo);
 s=txStv(:,compNo).'*waveform;
        
 xBar=signal*s';
   
 R=signal*signal';
       
 Rinv=pinv(R);
 betaTmp=a'*Rinv*xBar/(a'*Rinv*a)/(norm(s)^2);
  AmpErr(compNo)=betaTmp-beta(compNo);
end;
     
     
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [AmpErr]=APESEst(signal,txStv,rxStv,beta)
global waveform;


compNum=length(beta);
AmpErr=zeros(1,compNum);
for compNo=1:compNum
        
 a=rxStv(:,compNo);
 s=txStv(:,compNo).'*waveform;
        
 xBar=signal*s';
   
 T=signal*signal'-xBar*xBar'/( norm(s)^2);
       
 Tinv=pinv(T);
 betaTmp=a'*Tinv*xBar/(a'*Tinv*a)/(norm(s)^2);
       
  AmpErr(compNo)=betaTmp-beta(compNo);
end;
      
        

   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function  [AmpErr]=DGCEst(signal,txStv,rxStv,beta)
global waveform;

compNum=length(beta);
AmpErr=zeros(1,compNum);
        
A=rxStv;
S=txStv.'*waveform;
        
GTmp=signal*S';
   
T=signal*signal'-GTmp*pinv(S*S')*GTmp';
       
 Tinv=pinv(T);
 betaTmp=(  (A'*Tinv*A).* ((S*S').') )\diag(A'*Tinv*GTmp);
        
 AmpErr=(betaTmp-beta).';       