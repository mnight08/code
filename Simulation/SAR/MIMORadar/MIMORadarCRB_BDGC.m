function MIMORadarCRBVsK
close all;
clear all;
%save_path='E:/workspace/program/MIMO Radar/data';
savePath='E:/Document/lzxu/my program/MIMORadar_BDEG/data/'
dataName='data';

SNRdB=20; SNR=10.^(SNRdB/20);
INRdB=20; JammerAmp=10.^((INRdB-SNRdB)/20);
DOAJammer=45; 

K=3;
DOATrue=[-40 -20 -8];
SignalPwr=[4 4 1];


% 10 x 10
txAntNum1=[10]; rxAntNum1=10;
txDist1= 0.5*ones(2,txAntNum1-1); rxDist1= 0.5*ones(1,rxAntNum1-1);
% [5 5] x 10
txAntNum2=[5 5]; rxAntNum2=10;
txDist2= 0.5*ones(2,txAntNum2-1); rxDist2= 0.5*ones(1,rxAntNum2-1);

sampNum=256;trialNum=1000;

loadFlag=0;
if loadFlag==1
    load([savePath dataName '_K' num2str(2) '_SNR' num2str(SNRdB)  '.mat']);
else 
    [CRBDOA1 CRBAmp1]=funCalCRB(txAntNum1, txDist1, rxAntNum1, rxDist1, SignalPwr,DOATrue,JammerAmp, DOAJammer, SNR, sampNum,trialNum);
    [CRBDOA2 CRBAmp2]=funCalCRB(txAntNum2, txDist2, rxAntNum2, rxDist2, SignalPwr,DOATrue,JammerAmp, DOAJammer, SNR, sampNum,trialNum);
    save([savePath dataName '_K' num2str(2) '_SNR' num2str(SNRdB)  '.mat'], 'CRBDOA1', 'CRBAmp1','CRBDOA2', 'CRBAmp2' );
end;

 %%%%%%%%%%%%%%%%%%%%%%%%%%%
 % plot CRB of angle
 for targetNo=1:K
 
     temp=[CRBDOA1(:,targetNo) CRBDOA2(:,targetNo)];
     [cdf, CRB]=hist(temp, 1000);  
     cdf=cumsum(cdf)/trialNum;
     figure;
     plot(CRB, cdf(:,1), '-b', ...
               CRB, cdf(:,2), '--r');
     legend('Conf. 1', 'Conf. 2');
     xlabel('CRB'); ylabel('cdf');
    % title('CRB of DOA1')
      myboldify1
   %   axis([1 12 1e-5 10]);
      tmp=['CRB_DOA_target' num2str(targetNo) ];
     print('-deps',[savePath tmp '.eps']);
 end;   
%    
%  % plot CRB of amplitude
%  betaNum=
%  for targetNo=1:K
%     for 
%      temp=[CRBAmp1(:,targetNo) CRBAmp2(:,targetNo)];
%      [cdf, CRB]=hist(temp);   cdf=cumsum(cdf)/trialNum;
%      figure;
%      semiplotx(CRB, cdf(:,1), '-', ...
%                CRB, cdf(:,2), '-0');
%      legend('Conf. 1', 'Conf. 2');
%      xlabel('CRB'); ylabel('cdf');
%     % title('CRB of DOA1')
%       myboldify1
%    %   axis([1 12 1e-5 10]);
%       tmp=['CRB_Amp_target' num2str(targetNo) ];
%      print('-deps',[savePath tmp '.eps']);
%  end; 
 

 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [CRBDOA, CRBAmp]=funCalCRB(txAntNum, txDist, rxAntNum, rxDist, signalPwr,DOATrue,JammerAmp, DOAJammer,SNR, sampNum, trialNum)

txSubArrayNum=length(txAntNum);
rxSubArrayNum=length(rxAntNum);
totalTxAntNum=sum(txAntNum);
totalRxAntNum=sum(rxAntNum);
targetNum=length(DOATrue);
betaNum=txSubArrayNum*rxSubArrayNum;

% waveform: hardmard codes
temp=[1];
for kk=1:ceil(log2(sampNum))
  temp=[temp temp; temp -1*temp];
end;
waveformSet=temp;
totalTxAntNum=sum(txAntNum);
waveform=(waveformSet(1:totalTxAntNum, 1:sampNum)+j*waveformSet(totalTxAntNum+1:2*totalTxAntNum, 1:sampNum))/sqrt(2)/sqrt(totalTxAntNum); 


% correlation matrix 
  Q_struct=zeros(totalRxAntNum,totalRxAntNum);
   for row_no=1:totalRxAntNum
      for col_no=1:totalRxAntNum
          Q_struct(row_no,col_no)=0.9^abs(col_no-row_no)*exp(j*pi/2*(col_no-row_no));
      end;
    end;
 Q=Q_struct./(SNR.^2) ;
    
rxStvJammer=[];   
for rxArNo=1:length(rxAntNum)
   rxTmp=cumsum([0 rxDist(rxArNo, 1:(rxAntNum(rxArNo)-1) )]);
   rxStvJammer= [rxStvJammer; exp(-j*2*pi*rxTmp' * sin(DOAJammer*pi/180) ) *exp(j*2*pi*rand(1))];
end;  
Q_tilde = Q + rxStvJammer*diag(JammerAmp.*conj(JammerAmp))*rxStvJammer';

%
CRBDOA=zeros(targetNum, trialNum);
CRBAmp=zeros(targetNum*betaNum, trialNum);
for trialNo=1:trialNum
    
    % generate the reflection coeffcient
    B=zeros(rxSubArrayNum, txSubArrayNum, targetNum);
    for k=1:targetNum
        tmp=(randn(rxSubArrayNum, txSubArrayNum)+randn(rxSubArrayNum, txSubArrayNum)*j)./sqrt(2) * sqrt(signalPwr(k));
        B(:,:,k)=tmp;
    end;
    
    [CRBDOAtmp, CRBAmptmp]=CalCRB_BDGC(txAntNum, txDist, rxAntNum, rxDist, B,DOATrue,waveform,Q);
    
    CRBDOA(:,trialNo)=diag(CRBDOAtmp);
    CRBAmp(:,trialNo)=diag(CRBAmptmp);
end;




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [CRBDOA, CRBAmp]=CalCRB_BDGC(txAntNum, txDist, rxAntNum, rxDist, B,DOATrue,waveform,Q)

sampNum=size(waveform, 2);
txSubArrayNum=length(txAntNum);
rxSubArrayNum=length(rxAntNum);
totalTxAntNum=sum(txAntNum);
totalRxAntNum=sum(rxAntNum);
targetNum=length(DOATrue);
betaNum=txSubArrayNum*rxSubArrayNum;

% construct the receiving steering matrix
Ar=zeros(totalRxAntNum, rxSubArrayNum, targetNum);
Ar_driv=zeros(totalRxAntNum, rxSubArrayNum, targetNum);
for k=1:targetNum
    DOAtmp=DOATrue(k);
    
    Atmp=zeros(totalRxAntNum, rxSubArrayNum);
    A_driv_tmp=zeros(totalRxAntNum, rxSubArrayNum);
    index=1;
    for m=1:rxSubArrayNum
         distTmp=rxDist(m, 1:(rxAntNum(m)-1) ); distTmp=cumsum([0 distTmp]);
         stvTmp=  exp(-j*2*pi*distTmp' * sin(DOATrue(k)*pi/180) );
         Atmp(index:(index+rxAntNum(m)-1), m)=stvTmp;
         
         %
         driv_Tmp=  stvTmp .* (-j *2*pi*distTmp.'* cos(DOATrue(k)*pi/180) .* pi/180);
         A_driv_tmp(index:(index+rxAntNum(m)-1), m)=stvTmp;
         index=index+rxAntNum(m);
     end;
     
      Ar(:,:,k)=Atmp;  Ar_driv(:,:,k)=A_driv_tmp;
 end;
       
% construct the transmtting steering matrix
At=zeros(totalTxAntNum, txSubArrayNum, targetNum);
At_driv=zeros(totalTxAntNum, txSubArrayNum, targetNum);
for k=1:targetNum
    DOAtmp=DOATrue(k);
    
    Atmp=zeros(totalTxAntNum, txSubArrayNum);
    A_driv_tmp=zeros(totalTxAntNum, txSubArrayNum);
    index=1;
    for m=1:txSubArrayNum
         distTmp=txDist(m, 1:(txAntNum(m)-1) ); distTmp=cumsum([0 distTmp]);
         stvTmp=  exp(-j*2*pi*distTmp' * sin(DOATrue(k)*pi/180) );
         Atmp(index:(index+txAntNum(m)-1), m)=stvTmp;
         
         %
         driv_Tmp=  stvTmp .* (-j *2*pi*distTmp.'* cos(DOATrue(k)*pi/180) .* pi/180);
         A_driv_tmp(index:(index+txAntNum(m)-1), m)=stvTmp;
         index=index+txAntNum(m);
     end;
     
      At(:,:,k)=Atmp;  At_driv(:,:,k)=A_driv_tmp;
 end;
         
%%%%%%%%
Qinv=pinv(Q);
Rxx=waveform*waveform';

%calculate F11
F11=zeros(targetNum, targetNum);
for m=1:targetNum
    for n=1:targetNum
        Ar_m=squeeze( Ar(:,:,m)); 
        Ar_n=squeeze( Ar(:,:,n)); 
        Ar_driv_m=squeeze( Ar_driv(:,:,m));
        Ar_driv_n=squeeze( Ar_driv(:,:,n));
        
        At_m=squeeze( At(:,:,m)); 
        At_n=squeeze( At(:,:,n)); 
        At_driv_m=squeeze( At_driv(:,:,m));
        At_driv_n=squeeze( At_driv(:,:,n));
        
        B_m=squeeze( B(:,:,m));
        B_n=squeeze( B(:,:,n));
        
        temp=trace( (Ar_driv_m'*Qinv*Ar_driv_n)*(B_n*At_n.'*Rxx*conj(At_m)*B_m') )...
            +trace( (Ar_driv_m'*Qinv*Ar_n*B_n)*(At_n.'*Rxx*conj(At_m)*B_m') ) ...
            +trace( (B_m'*Ar_m'*Qinv*Ar_driv_n)*(B_n*At_n.'*Rxx*conj(At_m)))...
            +trace( (B_m'*Ar_m'*Qinv*Ar_n*B_n)*(At_n.'*Rxx*conj(At_m)));
        
        F11(m,n)=trace(temp);
    end;
end;

% calculate F21
betaNum=txSubArrayNum*rxSubArrayNum;
F21=zeros(targetNum*betaNum, targetNum );
index=1;
for m=1:targetNum
    for n=1:targetNum
        
        Ar_m=squeeze( Ar(:,:,m)); 
        Ar_n=squeeze( Ar(:,:,n)); 
        Ar_driv_m=squeeze( Ar_driv(:,:,m));
        Ar_driv_n=squeeze( Ar_driv(:,:,n));
        
        At_m=squeeze( At(:,:,m)); 
        At_n=squeeze( At(:,:,n)); 
        At_driv_m=squeeze( At_driv(:,:,m));
        At_driv_n=squeeze( At_driv(:,:,n));
        
        B_m=squeeze( B(:,:,m));
        B_n=squeeze( B(:,:,n));
        
        temp=(Ar_m'*Qinv*Ar_driv_n)*(B_n*At_n.'*Rxx*conj(At_m))...
            +(Ar_m'*Qinv*Ar_n*B_n)*(At_driv_n.'*Rxx*conj(At_m));
        
        F21(index:index+betaNum-1, n)=temp(:);
    end;
    index=index+betaNum;
end;

% calculate F22
betaNum=txSubArrayNum*rxSubArrayNum;
F22=zeros(targetNum*betaNum, targetNum*betaNum);
index1=1;
for m=1:targetNum
    index2=1;
    for n=1:targetNum
        
        Ar_m=squeeze( Ar(:,:,m)); 
        Ar_n=squeeze( Ar(:,:,n)); 
        Ar_driv_m=squeeze( Ar_driv(:,:,m));
        Ar_driv_n=squeeze( Ar_driv(:,:,n));
        
        At_m=squeeze( At(:,:,m)); 
        At_n=squeeze( At(:,:,n)); 
        At_driv_m=squeeze( At_driv(:,:,m));
        At_driv_n=squeeze( At_driv(:,:,n));
        
        B_m=squeeze( B(:,:,m));
        B_n=squeeze( B(:,:,n));
        
        temp1=(At_n.'*Rxx*At_m).';
        temp2=(Ar_m'*Qinv*Ar_n);
        F22(index1:index1+betaNum-1, index2:index2+betaNum-1)=kron(temp1, temp2);
                
        index2=index2+betaNum;
    end;
    index1=index1+betaNum;
end;

%%Calculate CRB
F22_inv=pinv(F22);
CRBDOA=0.5*pinv( real( F11-F21'*F22_inv*F21   )  );
CRBAmp=F22_inv+F22_inv*F21*CRBDOA*F21'*F22_inv;