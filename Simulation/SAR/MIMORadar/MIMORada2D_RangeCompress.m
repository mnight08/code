function MIMORadar
close all;
clear all;

global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global save_path

%save_path='E:/workspace/program/MIMO Radar/data';
savePath='E:/Document/lzxu/my program/MIMO Radar/dataImage/'
saveName='image_Conf1';
%path(path,save_path);

txAntNum=10;
rxAntNum=10;
txDist=2*ones(1,txAntNum-1);%[0.5 1. 1.5 4];%
rxDist=0.5*ones(1,rxAntNum-1);%[0.5 1. 1.5 4];%
sampNum=256;

% target parameters
rangeNum=64; targetNum=10;
loadFlag=1;
if loadFlag==1
    load([savePath  'Radar2DImage_Parameters.mat']);
    DOATrue(1)=9; DOATrue(4)=10; DOATrue(6)=5; DOATrue(8)=-18; DOATrue(9)=-25
    save([savePath  'Radar2DImage_Parameters.mat'], 'betaTrue','DOATrue','rangeTrue');
else 
   betaTrue= rand(1,targetNum)*10;
   targetNum=length(betaTrue);
   DOATrue= round((2*rand(1, targetNum)-1)*28); %linspace(-25, 25, targetNum);%
   rangeTrue= round( rand(1,targetNum)*50+5); %
   save([savePath  'Radar2DImagee_Parameters1.mat'], 'betaTrue','DOATrue','rangeTrue');
end;
betaGround=20;
GroundDOA=-5;
GroundRange=1:64; betaTrue=ones(1, targetNum);
betaTrue=[betaTrue betaGround*ones(1, rangeNum)];
DOATrue=[DOATrue GroundDOA*ones(1, rangeNum)]
rangeTrue=[rangeTrue GroundRange];
targetNum=length(rangeTrue);

scanSet=[-30:1:30];
ASNRdB=40; %dB
SNRAmp=sqrt(10.^(ASNRdB/10)./rxAntNum./txAntNum);

JammerAmp=1000 ; %
DOAJammer=[0];
JammerNum=length(DOAJammer);

% waveform: hardmard codes
temp=[1];
for kk=1:ceil(log2(sampNum))
  temp=[temp temp; temp -1*temp];
end;
waveformSet=temp;
waveform=(waveformSet(1:txAntNum, 1:sampNum)+j*waveformSet(txAntNum+1:2*txAntNum, 1:sampNum))/sqrt(2);

mSeq=mseq(2,8,0,1);
waveform=waveform.*(ones(txAntNum,1)* [mSeq' 1]) ./sqrt(sampNum) ;


% source generator
%------------------------------------
signalPure=zeros(rxAntNum, sampNum+rangeNum);
for k=1:targetNum
   txTmp=cumsum([0 txDist]);
   v=exp(-j*2*pi*txTmp' * sin(DOATrue(k)*pi/180) );%  
   rxTmp=cumsum([0 rxDist]);
   rxStv=exp(-j*2*pi*rxTmp' * sin(DOATrue(k)*pi/180) );% 

   signalPure(:, (1:sampNum)+rangeTrue(k))=signalPure(:, (1:sampNum)+rangeTrue(k)) + rxStv*diag(betaTrue(targetNum))*(v.'*waveform);
end;

signal=signalPure;

 % correlation matrix 
%   Q_struct=zeros(rxAntNum,rxAntNum);
%    for row_no=1:rxAntNum
%       for col_no=1:rxAntNum
%           Q_struct(row_no,col_no)=0.9^abs(col_no-row_no)*exp(j*pi/2*(col_no-row_no));
%       end;
%     end;
   Q_struct=eye(rxAntNum,rxAntNum)
signal=signal +  (Q_struct^0.5)*(randn(rxAntNum, sampNum+rangeNum) +j *randn(rxAntNum, sampNum+rangeNum))/SNRAmp/sqrt(2) ;

% jammer 
rxTmp=cumsum([0 rxDist]);
rxStv=exp(-j*2*pi*rxTmp' * sin(DOAJammer*pi/180) );
jammerWaveform=((2*randint(JammerNum,sampNum+rangeNum)-1)+j*(2*randint(JammerNum,sampNum+rangeNum)-1))/sqrt(2)./sqrt(sampNum+rangeNum);
signal=signal+rxStv*jammerWaveform.*JammerAmp;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MIMO signal processing
loadFlag=0;
if loadFlag==1
    load([savePath saveName '_DAS.mat']);
else 
    [imageCapon]=fun_DAS_RangeCompression(signal, waveform, rangeNum, scanSet);
    save([savePath saveName '_DAS.mat'], 'imageCapon','betaTrue','DOATrue','rangeTrue');
end;

if loadFlag==1
    load([savePath saveName '_RCB.mat']);
else 
    [imageApes]=fun_RCB_RangeCompression(signal, waveform, rangeNum, scanSet);
    save([savePath saveName '_RCB.mat'], 'imageApes','betaTrue','DOATrue','rangeTrue');
end;

 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure;
imagescdb1(imageCapon,30, 1:rangeNum,scanSet, 1,0);
drawnow;
hold on
thetaSet=linspace(-pi, pi, 100);
xSet=2*cos(thetaSet); ySet=2*sin(thetaSet);
for k=1:10%targetNum
    plot(xSet+DOATrue(k), ySet+rangeTrue(k), 'y');
end;
print('-depsc',[savePath saveName '_DAS.eps']);

%------------
figure;
imagescdb1(imageApes,30, 1:rangeNum,scanSet, 1,0);
drawnow;
hold on
thetaSet=linspace(-pi, pi, 100);
xSet=2*cos(thetaSet); ySet=2*sin(thetaSet);
for k=1:10%targetNum
    plot(xSet+DOATrue(k), ySet+rangeTrue(k), 'y');
end;
print('-depsc',[savePath saveName '_RCB.eps']);
 

 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [imageEst]=fun_DAS_RangeCompression(signal, waveform, rangeNum, scanSet)
global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global save_path

angleNum=length(scanSet);
sampNum=size(waveform,2);
 txTmp=cumsum([0 txDist]);
 rxTmp=cumsum([0 rxDist]);

R_Phi=waveform*waveform';
R_Phi_sqrtm=sqrtm(R_Phi);
imageEst=zeros( rangeNum, angleNum);
for k=1:rangeNum
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%
    % Range Compression
    signalTmp=signal(:, (1:sampNum)+k-1)*waveform'*pinv(R_Phi_sqrtm);
    waveformTmp=R_Phi_sqrtm;
    sampNumTmp=txAntNum;
    R=signalTmp*signalTmp'/sampNum;
    Rinv=pinv(R);
    for n=1:angleNum
        DOATmp=scanSet(n);      
        v=exp(-j*2*pi*txTmp' * sin(DOATmp*pi/180) );
        a=exp(-j*2*pi*rxTmp' * sin(DOATmp*pi/180) );
        s=v.'*waveformTmp;
        betaTmp= a'*signalTmp*s'/(a'*a)/norm(s).^2;         
        imageEst(k, n)=betaTmp;
    end;
end;

        
    
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [imageEst]=fun_RCB_RangeCompression(signal, waveform, rangeNum, scanSet)
global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global save_path
epsilon=0.1;

angleNum=length(scanSet);
sampNum=size(waveform,2);
 txTmp=cumsum([0 txDist]);
 rxTmp=cumsum([0 rxDist]);

R_Phi=waveform*waveform';
R_Phi_sqrtm=sqrtm(R_Phi);
imageEst=zeros( rangeNum, angleNum);
for k=1:rangeNum
    signalTmp=signal(:, (1:sampNum)+k-1)*waveform'*pinv(R_Phi_sqrtm);
    waveformTmp=R_Phi_sqrtm;
    sampNumTmp=txAntNum;
    R=signalTmp*signalTmp'/sampNum;
    Rinv=pinv(R);
    for n=1:angleNum
        DOATmp=scanSet(n);      
        v=exp(-j*2*pi*txTmp' * sin(DOATmp*pi/180) );
        a=exp(-j*2*pi*rxTmp' * sin(DOATmp*pi/180) );
        s=v.'*waveformTmp;
        xBar=signalTmp*s'/sampNumTmp;
        Rinv_xBar=Rinv*xBar;
        [a_hat] = RCB_func(R,a,epsilon);
        betaTmp=sampNumTmp*a_hat'*Rinv*xBar/(a_hat'*Rinv*a_hat)/(norm(s)^2);
        imageEst(k, n)=betaTmp;
    end;
end;
     
 