 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 function [CRBDOA, CRBAmp]=calCRB_serial(txDist,rxDist,beta,DOA,waveform,Q)
 
 txAntNum=length(txDist)+1;
 rxAntNum=length(rxDist)+1;
 compNum=length(DOA);
 sampNum=size(waveform,2);
 
txTmp=cumsum([0 txDist]);
txStv=exp(-j*2*pi*txTmp' * sin(DOA*pi/180) );%  + 0.1*(randn(txAntNum, compNum) +j*randn(txAntNum, compNum) )/sqrt(2);
txStv_Deff=(-j*2*pi*txTmp'*cos(DOA*pi/180)*pi/180) .* txStv;
rxTmp=cumsum([0 rxDist]);
rxStv=exp(-j*2*pi*rxTmp' * sin(DOA*pi/180) );%  + 0.1*(randn(txAntNum, compNum) +j*randn(txAntNum, compNum) )/sqrt(2);
rxStv_Deff=(-j*2*pi*rxTmp'*cos(DOA*pi/180)*pi/180) .* rxStv;

Qinv=inv(Q);
rss=waveform*waveform'; 

F11=(rxStv_Deff'*Qinv*rxStv_Deff).*(txStv'*txStv)...
   +(rxStv_Deff'*Qinv*rxStv).*(txStv'*txStv_Deff)...
    +(rxStv'*Qinv*rxStv_Deff).*(txStv_Deff'*txStv)...
    +(rxStv'*Qinv*rxStv).*(txStv_Deff'*txStv_Deff);
F11=F11.*(conj(beta.')*beta)*rss;

F12=(rxStv_Deff'*Qinv*rxStv).*(txStv'*txStv).*(conj(beta.')*ones(1,compNum))*rss...
    +(rxStv'*Qinv*rxStv).*(txStv_Deff'*txStv).*(conj(beta.')*ones(1,compNum))*rss;
F21=F12';
F22=(rxStv'*Qinv*rxStv).*(txStv'*txStv)*rss;
F22inv=inv(F22);

CRBDOA=0.5*inv( real (F11-F12*F22inv*F21 ));
CRBAmp=F22inv +F22inv*F21*CRBDOA*F12*F22inv;
end;

