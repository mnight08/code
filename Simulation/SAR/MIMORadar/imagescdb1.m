function Logimage=imagescdb1(image,level,xx,yy,scale_flag, dbflag)
%% scale_flag==1, use dB scale
%% dbflag==1 means power image.

if dbflag==1
    aa=10;
else
    aa=20;
end

%% Level in dB

image=abs(image);

if scale_flag==1
    image=image./max(max(image));
    Logimage=aa*log10(abs(image));
    pmax=max(max(Logimage));
    Logimage(Logimage<(pmax-level))=(pmax-level);
    %h0=axes;
    imagesc(yy,xx,Logimage); 
    %colormap(flipud(gray));
    %colormap(hot);
    h3=colorbar;
else
    imagesc(yy,xx,image); 
    %colormap(flipud(gray));
    %colormap(hsv);
    colorbar;
end


h1=ylabel('Range'); 
h2=xlabel('Angle (deg)');
set(gca,'FontSize',14);
set(h1,'Fontsize',14);
set(h2,'Fontsize',14);
%set(h3,'Fontsize',14);





%axis('equal')
%boldify1;

