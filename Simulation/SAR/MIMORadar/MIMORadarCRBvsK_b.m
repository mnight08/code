function MIMORadarCRBVsK
close all;
clear all;
%save_path='E:/workspace/program/MIMO Radar/data';
savePath='E:/Document/lzxu/my program/MIMO Radar/ParameterIdentifiablity/'

SNRdB=20; SNR=10.^(SNRdB/10);
INRdB=20; JammerAmp=10.^((INRdB-SNRdB)/20);
DOAJammer=45; 

MaxK=16;
%DOASet=[0 10 -10 20 -20 30 -30 40 -40 50 -50 60 -60 70 -70 80 -80];
DOASet=[8:8:80; -8:-8:-80]; DOASet=[0; DOASet(:)].';
%DOASet=[0 10 -10 20 -20 30 -40  60  -70  80 -85];
%DOASet=[-70:8:70]
betaSet=ones(MaxK,1);

txAntNum=5;
rxAntNum=5;
txDist= 5*0.5*ones(1,txAntNum-1);
rxDist= 0.5*ones(1,rxAntNum-1);
sampNum=256;

% waveform: hardmard codes
temp=[1];
for kk=1:ceil(log2(sampNum))
  temp=[temp temp; temp -1*temp];
end;
waveformSet=temp;%2*randint(2*txAntNum, sampNum)-1; %
waveform=(waveformSet(1:txAntNum, 1:sampNum)+j*waveformSet(txAntNum+1:2*txAntNum, 1:sampNum))/sqrt(2)/sqrt(txAntNum);
  %%%%%%%%%%%%%%%%%%%%  
 
 CRBDOA=zeros(MaxK);
 CRBAmp=zeros(MaxK);
 for k=1:MaxK;
     DOATrue=DOASet(1:k);
     betaTrue=betaSet(1:k).'; 
     
     Q=eye(rxAntNum)./SNR ;
     rxTmp=cumsum([0 rxDist]);
     rxStvJammer=exp(-j*2*pi*rxTmp' * sin(DOAJammer*pi/180) );
     Q = Q + rxStvJammer*diag(JammerAmp.*conj(JammerAmp))*rxStvJammer';
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % calculate CRB
    [CRBDOATmp, CRBAmpTmp]=calCRB(txDist,rxDist,betaTrue,DOATrue,waveform,Q);
   
     CRBDOA(1:k,k)=[diag(CRBDOATmp)];
     CRBAmp(1:k,k)=[diag(CRBAmpTmp)];
 end;

 %%%%%%%%%%%%%%%%%%%%%%
 %SIMO
txAntNumSIMO=1;
rxAntNumSIMO=5;
txDistSIMO= 0.5*ones(1,txAntNumSIMO-1);
rxDistSIMO= 0.5*ones(1,rxAntNumSIMO-1);
 
waveform=(waveformSet(1:txAntNumSIMO, 1:sampNum)+j*waveformSet(txAntNumSIMO+1:2*txAntNumSIMO, 1:sampNum))/sqrt(2)/sqrt(txAntNumSIMO);
 CRBDOASIMO=zeros(MaxK);
 CRBAmpSIMO=zeros(MaxK);
 for k=1:MaxK;
     DOATrue=DOASet(1:k);
     betaTrue=betaSet(1:k).'; 
     
     Q=eye(rxAntNumSIMO)./SNR ;
     rxTmp=cumsum([0 rxDistSIMO]);
     rxStvJammer=exp(-j*2*pi*rxTmp' * sin(DOAJammer*pi/180) );
     Q = Q + rxStvJammer*diag(JammerAmp.*conj(JammerAmp))*rxStvJammer';
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % calculate CRB
    [CRBDOATmp, CRBAmpTmp]=calCRB(txDistSIMO,rxDistSIMO,betaTrue,DOATrue,waveform,Q);
   
     CRBDOASIMO(1:k,k)=[diag(CRBDOATmp)];
     CRBAmpSIMO(1:k,k)=[diag(CRBAmpTmp)];
 end;
 
 
    figure;
    semilogy(1:4, abs(CRBDOASIMO(1,1:4)), '--r', 1:MaxK, abs(CRBDOA(1,:)), '-b');  
     xlabel('K');
     ylabel('CRB');
    % grid on;  
      tmp=['CRB_DOA_' num2str(txAntNum) 'by' num2str(rxAntNum) '_SNR' num2str(SNRdB) '_INR' num2str(INRdB) '_DOA1' ];
     legend('Phased-Array', 'MIMO Radar')
    % title('CRB of DOA1')
      myboldify1
      axis([1 16 1e-5 10]);
     print('-depsc',[savePath tmp '.eps']);
     
    figure;
   semilogy(1:4, max( abs(CRBDOASIMO(:,1:4)), [], 1), '--r', 1:MaxK, max( abs(CRBDOA), [], 1),'-b' );  
   legend('Phased-Array', 'MIMO Radar')
     xlabel('K');
     ylabel('CRB');
    % grid on;  
       tmp=['CRB_DOA_' num2str(txAntNum) 'by' num2str(rxAntNum) '_SNR' num2str(SNRdB) '_INR' num2str(INRdB) '_WorstDOA' ];
     %title('CRB of DOA 1')
      myboldify1
     axis([1 16 1e-5 10]);
     print('-depsc',[savePath tmp '.eps']);

%          figure;
%     semilogy(1:MaxK, CRBAmp(1,:));  
%      xlabel('K');
%      ylabel('CRB');
%      %grid on;  
%      tmp=['CRB_Amp 1'];
%      title('CRB of Amp1')
%       myboldify1
%      print('-deps',[savePath tmp '.eps']);
%      
%     figure;
%     semilogy(1:MaxK, max(CRBDOA, [], 1) );  
%      xlabel('K');
%      ylabel('CRB');
%      %grid on;  
%      tmp=['CRB_Amp_worst'];
%      %title('CRB of DOA 1')
%       myboldify1
%      print('-deps',[savePath tmp '.eps']);
 
