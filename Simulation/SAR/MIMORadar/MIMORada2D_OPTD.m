function MIMORadar
close all;
clear all;

global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global save_path

%save_path='E:/workspace/program/MIMO Radar/data';
savePath='E:/Document/lzxu/my program/MIMO Radar/dataImage/'
saveName='image_Conf3';
%path(path,save_path);

txAntNum=10;
rxAntNum=10;
txDist=0.5*ones(1,txAntNum-1);%[0.5 1. 1.5 4];%
rxDist=2*ones(1,rxAntNum-1);%[0.5 1. 1.5 4];%
sampNum=256;

% target parameters
rangeNum=64; targetNum=10;
loadFlag=1;
if loadFlag==1
    load([savePath  'Radar2DImage_Parameters.mat']);
    DOATrue(1)=9; DOATrue(4)=10; DOATrue(6)=5; DOATrue(8)=-18; DOATrue(9)=-25
    save([savePath  'Radar2DImage_Parameters.mat'], 'betaTrue','DOATrue','rangeTrue');
else 
   betaTrue= rand(1,targetNum)*10;
   targetNum=length(betaTrue);
   DOATrue= round((2*rand(1, targetNum)-1)*28); %linspace(-25, 25, targetNum);%
   rangeTrue= round( rand(1,targetNum)*50+5); %
   save([savePath  'Radar2DImagee_Parameters1.mat'], 'betaTrue','DOATrue','rangeTrue');
end;
betaGround=20;
GroundDOA=-5;
GroundRange=1:64; betaTrue=ones(1, targetNum);
betaTrue=[betaTrue betaGround*ones(1, rangeNum)];
DOATrue=[DOATrue GroundDOA*ones(1, rangeNum)]
rangeTrue=[rangeTrue GroundRange];
targetNum=length(rangeTrue);

scanSet=[-30:1:30];
ASNRdB=40; %dB
SNRAmp=sqrt(10.^(ASNRdB/10)./rxAntNum./txAntNum);

JammerAmp=1000 ; %
DOAJammer=[0];
JammerNum=length(DOAJammer);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% waveform: hardmard codes
temp=[1];
for kk=1:ceil(log2(sampNum))
  temp=[temp temp; temp -1*temp];
end;
waveformSet=temp;
waveform=(waveformSet(1:txAntNum, 1:sampNum)+j*waveformSet(txAntNum+1:2*txAntNum, 1:sampNum))/sqrt(2);

mSeq=mseq(2,8,0,1);
waveform=waveform.*(ones(txAntNum,1)* [mSeq' 1]) ./sqrt(sampNum) ;

 %[CRBDOA, CRBAmp, R_Phi]=fun_CBROPT(txDist,rxDist,betaTrue,DOATrue,Q, Energy, method)

% source generator
%------------------------------------
signalPure=zeros(rxAntNum, sampNum+rangeNum);
for k=1:targetNum
   txTmp=cumsum([0 txDist]);
   v=exp(-j*2*pi*txTmp' * sin(DOATrue(k)*pi/180) );%  
   rxTmp=cumsum([0 rxDist]);
   rxStv=exp(-j*2*pi*rxTmp' * sin(DOATrue(k)*pi/180) );% 

   signalPure(:, (1:sampNum)+rangeTrue(k))=signalPure(:, (1:sampNum)+rangeTrue(k)) + rxStv*diag(betaTrue(targetNum))*(v.'*waveform);
end;

signal=signalPure;

 % correlation matrix 
%   Q_struct=zeros(rxAntNum,rxAntNum);
%    for row_no=1:rxAntNum
%       for col_no=1:rxAntNum
%           Q_struct(row_no,col_no)=0.9^abs(col_no-row_no)*exp(j*pi/2*(col_no-row_no));
%       end;
%     end;
   Q_struct=eye(rxAntNum,rxAntNum)
signal=signal +  (Q_struct^0.5)*(randn(rxAntNum, sampNum+rangeNum) +j *randn(rxAntNum, sampNum+rangeNum))/SNRAmp/sqrt(2) ;

% jammer 
rxTmp=cumsum([0 rxDist]);
rxStv=exp(-j*2*pi*rxTmp' * sin(DOAJammer*pi/180) );
jammerWaveform=((2*randint(JammerNum,sampNum+rangeNum)-1)+j*(2*randint(JammerNum,sampNum+rangeNum)-1))/sqrt(2)./sqrt(sampNum+rangeNum);
signal=signal+rxStv*jammerWaveform.*JammerAmp;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MIMO signal processing
loadFlag=0;
if loadFlag==1
    load([savePath saveName '_Capon.mat']);
else 
    [imageCapon]=fun_Capon(signal, waveform, rangeNum, scanSet);
    save([savePath saveName '_Capon.mat'], 'imageCapon','betaTrue','DOATrue','rangeTrue');
end;

if loadFlag==1
    load([savePath saveName '_Apes.mat']);
else 
    [imageApes]=fun_Apes(signal, waveform, rangeNum, scanSet);
    save([savePath saveName '_Apes.mat'], 'imageApes','betaTrue','DOATrue','rangeTrue');
end;

if loadFlag==1
    load([savePath saveName '_GLRT.mat']);
else 
    [imageGLRT]=fun_GLRT(signal, waveform, rangeNum, scanSet);
    save([savePath saveName '_GLRT.mat'], 'imageGLRT','betaTrue','DOATrue','rangeTrue');
end;

loadFlag=0;
if loadFlag==1
    load([savePath saveName '_CAPESGLRT.mat']);
else 
    [imageCAPESGLRT]=fun_CAPESGLRT(imageCapon, imageApes, imageGLRT); 
    save([savePath saveName '_CAPESGLRT.mat'], 'imageCAPESGLRT');
end;
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure;
imagescdb1(imageCapon,30, 1:rangeNum,scanSet, 1,0);
drawnow;
%set(hb,'Position',  [0.8 0.42 0.02 0.2])
%text(63.2,-1.8,'(dB)')
hold on
thetaSet=linspace(-pi, pi, 100);
xSet=2*cos(thetaSet); ySet=2*sin(thetaSet);
for k=1:10%targetNum
    plot(xSet+DOATrue(k), ySet+rangeTrue(k), 'y');
end;
print('-depsc',[savePath saveName '_Capon.eps']);

%------------
figure;
imagescdb1(imageApes,30, 1:rangeNum,scanSet, 1,0);
drawnow;
%set(hb,'Position',  [0.8 0.42 0.02 0.2])
%text(63.2,-1.8,'(dB)')
hold on
thetaSet=linspace(-pi, pi, 100);
xSet=2*cos(thetaSet); ySet=2*sin(thetaSet);
for k=1:10%targetNum
    plot(xSet+DOATrue(k), ySet+rangeTrue(k), 'y');
end;
print('-depsc',[savePath saveName '_Apes.eps']);
 
%------------
figure;
imagescdb1(imageGLRT,1, 1:rangeNum,scanSet, 0,0);
drawnow;
%set(hb,'Position',  [0.8 0.42 0.02 0.2])
%text(63.2,-1.8,'(dB)')
hold on
thetaSet=linspace(-pi, pi, 100);
xSet=2*cos(thetaSet); ySet=2*sin(thetaSet);
for k=1:10%targetNum
    plot(xSet+DOATrue(k), ySet+rangeTrue(k), 'y');
end;
print('-depsc',[savePath saveName '_GLRT.eps']);
 
%------------
figure;
imagescdb1(imageCAPESGLRT,30, 1:rangeNum,scanSet, 1,0);
drawnow;
%set(hb,'Position',  [0.8 0.42 0.02 0.2])
%text(63.2,-1.8,'(dB)')
hold on
thetaSet=linspace(-pi, pi, 100);
xSet=2*cos(thetaSet); ySet=2*sin(thetaSet);
for k=1:10%targetNum
    plot(xSet+DOATrue(k), ySet+rangeTrue(k), 'y');
end;
print('-depsc',[savePath saveName '_CAPESGLRT.eps']);
 
 
      
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [imageEst]=fun_Capon(signal, waveform, rangeNum, scanSet)
global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global save_path

angleNum=length(scanSet);
sampNum=size(waveform,2);
 txTmp=cumsum([0 txDist]);
 rxTmp=cumsum([0 rxDist]);

R=signal*signal'/sampNum;
Rinv=pinv(R);
imageEst=zeros( rangeNum, angleNum);
for k=1:rangeNum
    waveformTmp=zeros(txAntNum, sampNum+rangeNum);
    waveformTmp(:, (1:sampNum)+k-1)=waveform;   
    for n=1:angleNum
        DOATmp=scanSet(n);      
        v=exp(-j*2*pi*txTmp' * sin(DOATmp*pi/180) );
        a=exp(-j*2*pi*rxTmp' * sin(DOATmp*pi/180) );
        s=v.'*waveformTmp;
        xBar=signal*s'./sampNum;
        betaTmp= sampNum*a'*Rinv*xBar/(a'*Rinv*a)/(norm(s)^2);
          
        imageEst(k, n)=betaTmp;
    end;
end;

        
    
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [imageEst]=fun_Apes(signal, waveform, rangeNum, scanSet)
global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global save_path

angleNum=length(scanSet);
sampNum=size(waveform,2);
 txTmp=cumsum([0 txDist]);
 rxTmp=cumsum([0 rxDist]);

R=signal*signal'/sampNum;
Rinv=pinv(R);
imageEst=zeros( rangeNum, angleNum);
for k=1:rangeNum
    waveformTmp=zeros(txAntNum, sampNum+rangeNum);
    waveformTmp(:, (1:sampNum)+k-1)=waveform;
    for n=1:angleNum
        DOATmp=scanSet(n);      
        v=exp(-j*2*pi*txTmp' * sin(DOATmp*pi/180) );
        a=exp(-j*2*pi*rxTmp' * sin(DOATmp*pi/180) );
        s=v.'*waveformTmp;
        xBar=signal*s'/sampNum;
        
        Rinv_xBar=Rinv*xBar;
        Tinv=Rinv - Rinv_xBar*Rinv_xBar'./(xBar'*Rinv_xBar - norm(s).^2/sampNum);
        betaTmp=sampNum*a'*Tinv*xBar/(a'*Tinv*a)/(norm(s)^2);
          
        imageEst(k, n)=betaTmp;
    end;
end;
     
 
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [imageEst]=fun_GLRT(signal, waveform, rangeNum, scanSet)
global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global save_path

angleNum=length(scanSet);
sampNum=size(waveform,2);
 txTmp=cumsum([0 txDist]);
 rxTmp=cumsum([0 rxDist]);

R=signal*signal'/sampNum;
Rinv=pinv(R);
imageEst=zeros( rangeNum, angleNum);
for k=1:rangeNum
    waveformTmp=zeros(txAntNum, sampNum+rangeNum);
    waveformTmp(:, (1:sampNum)+k-1)=waveform;
    for n=1:angleNum
        DOATmp=scanSet(n);      
        v=exp(-j*2*pi*txTmp' * sin(DOATmp*pi/180) );
        a=exp(-j*2*pi*rxTmp' * sin(DOATmp*pi/180) );
        s=v.'*waveformTmp;
        xBar=signal*s'/sampNum;
        
        Rinv_xBar=Rinv*xBar;
         Tinv=Rinv - Rinv_xBar*Rinv_xBar'./(xBar'*Rinv_xBar - norm(s).^2/sampNum);
        GLR=1 - (a'*Rinv*a)./(a'*Tinv*a);
          
        imageEst(k, n)=GLR;
    end;
end;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%      
function [imageCAPESGLRT]=fun_CAPESGLRT(imageCapon, imageApes, imageGLRT) 

global txAntNum;
global rxAntNum;
global txDist;
global rxDist;
global save_path
GLRThreshold=0.2;
[rangeNum scanNum]=size(imageCapon);
imageCapon=abs(imageCapon);

imagePeak=(imageCapon>[zeros(rangeNum, 1) imageCapon(:,1:end-1)]) ...
          +  (imageCapon>[ imageCapon(:,2:end) zeros(rangeNum, 1)])...
          + (imageCapon>[zeros(1, scanNum); imageCapon(1:end-1,:)]) ...
          + (imageCapon> [imageCapon(2:end,:); zeros(1, scanNum)]);
imagePeak=(imagePeak>=4);

imageCAPESGLRT=imageApes.*imagePeak.*(imageGLRT>=GLRThreshold);
          
          

 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%55
function [CRBDOA, CRBAmp, R_Phi]=fun_CBROPT(txDist,rxDist,beta,DOA,Q, Energy, method)
 
 txAntNum=length(txDist)+1;
 rxAntNum=length(rxDist)+1;
 compNum=length(DOA);
 
txTmp=cumsum([0 txDist]); txTmp=txTmp-mean(txTmp);
txStv=exp(-j*2*pi*txTmp' * sin(DOA*pi/180) );
txStv_Deff=(-j*2*pi*txTmp'*cos(DOA*pi/180)) *pi./180.* txStv;   norm_txStv_Deff=norm(txStv_Deff).^2;
rxTmp=cumsum([0 rxDist]); rxTmp=rxTmp-mean(rxTmp);
rxStv=exp(-j*2*pi*rxTmp' * sin(DOA*pi/180) );
rxStv_Deff=(-j*2*pi*rxTmp'*cos(DOA*pi/180)) *pi./180.* rxStv;

Qinv=inv(Q);
 
 % Set optmization variable
 Lambda=sdpvar(2*compNum, 2*compNum, 'hermitian','complex');
 Lambda11=Lambda(1:compNum, 1:compNum);
 Lambda12=Lambda(1:compNum, (1:compNum)+compNum);
 Lambda21=Lambda((1:compNum)+compNum, 1:compNum);
 Lambda22=Lambda((1:compNum)+compNum, (1:compNum)+compNum);
 
% set constraint
B=diag(beta);

R_V=txStv'*txStv; R_dot_V=txStv_Deff'*txStv_Deff;

F11=(rxStv_Deff'*Qinv*rxStv_Deff).*(R_V* Lambda11*R_V)...
   +(rxStv_Deff'*Qinv*rxStv).*(R_V*Lambda12*R_dot_V)...
    +(rxStv'*Qinv*rxStv_Deff).*(R_dot_V*Lambda21*R_V)...
    +(rxStv'*Qinv*rxStv).*(R_dot_V*Lambda22*R_dot_V);
F11=F11.*(conj(beta.')*beta);

F12=(rxStv_Deff'*Qinv*rxStv).*(R_V*Lambda11*R_V).*(conj(beta.')*ones(1,compNum))...
    +(rxStv'*Qinv*rxStv).*(R_dot_V*Lambda21*R_V).*(conj(beta.')*ones(1,compNum));
F21=F12';
F22=(rxStv'*Qinv*rxStv).*(R_V*Lambda11*R_V);

FIM=2*[real(F11)  real(F12)   -1*imag(F12) ; ...
        real(F21) real(F22)  -1*imag(F22);...
        imag(F21) imag(F22)  real(F22)];


F= set( trace(Lambda11)*txAntNum+ trace(Lambda22)*norm_txStv_Deff<= Energy); % keep total transmitting Energy
F=F + set(Lambda>=0);

ops = sdpsettings('solver','sedumi','sedumi.eps',0);


switch lower(method)
    case{'doa'}
        t=sdpvar(compNum,1);
        I_matrix=eye(3*compNum);
        for k=1:compNum
            eTmp=I_matrix(:,k);
            temp=[FIM eTmp; eTmp' t(k)];
            F=F+set(temp>=0);
        end;
        solvesdp(F, sum(t));

     case{'amp'}
        t=sdpvar(2*compNum,1);
        I_matrix=eye(3*compNum);
        for k=1:2*compNum
            eTmp=I_matrix(:,k+compNum);
            temp=[FIM eTmp; eTmp' t(k)];
            F=F+set(temp>=0);
        end;
        solvesdp(F, sum(t) );
    case{'trace'}
         t=sdpvar(3*compNum,1);
         I_matrix=eye(3*compNum);
        for k=1:3*compNum
            eTmp=I_matrix(:,k);
            temp=[FIM eTmp; eTmp' t(k)];
            F=F+set(temp>=0);
        end;
        solvesdp(F,  sum(t));
     case{'norm'}
         t=sdpvar(1, 1);
         F=F+set( FIM-t*eye(3*compNum)  >= 0);
         solvesdp(F, -1*t);
     case{'det', 'determinant'}
         solvesdp(F, -1*geomean(FIM));
     otherwise
         disp('Unknown criterion');
 end;

CRB=pinv(double(FIM));
CRBDOA=  CRB(1:compNum,1:compNum);
CRBAmp=CRB((1:compNum)+compNum, (1:compNum)+compNum)+ CRB((1:compNum)+2*compNum, (1:compNum)+2*compNum)+...
       j*CRB((1:compNum)+2*compNum, (1:compNum)+compNum) -j* CRB((1:compNum)+compNum, (1:compNum)+2*compNum);
Lambda=double(Lambda);
R_Phi=conj([txStv txStv_Deff])*conj(Lambda)*[txStv txStv_Deff].';
