function [a_hat] = RCB_func(R,a_bar,epsilon)

%%%%%% input
%% a_bar is the assumed steering vector,
%% d is the intersensorspacing with respect to wavelength (usually d=0.5)
%% m is the number of array elements
%% R is the array covariance matrix
%% epsilon is the parameter for the uncertainty set, i.e., the norm square of (a - a_bar)

%%%%%% output
%% a_hat1 is the steering vector calculated
%% W_DCRCB is the weigth vector calculated
m=length(a_bar);

[U,Gamma,V]=svd(R);
Gamma=real(Gamma);
Gamma_vec=diag(Gamma);

lambda_max=max(diag(Gamma));
lambda_min=min(diag(Gamma));

% use Newton's method here for RCB 
z=U'*a_bar;
    
error=10; % initially set a large number for the error

% lower bound for the optimal solution of lambda
LowerBound=(norm(a_bar)/sqrt(epsilon)-1)/lambda_max;
lambda_n=LowerBound;	
        
while (abs(error)>1e-6) 
         [lambda_m,error]=RCB_Newton(lambda_n,z,Gamma_vec,m,epsilon);
         lambda_n=lambda_m;	
end

% get the optimal solution for lambda
lambda_Newton=lambda_n; 

% get the estimated steering vector
a_hat=a_bar-inv(eye(m,m)+lambda_Newton*R)*a_bar; 

a_hat=sqrt(m)*a_hat/norm(a_hat);
