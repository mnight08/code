function MIMORadarCRB
%close all;
%clear all;
%save_path='E:/workspace/program/MIMO Radar/data';
savePath='E:/Document/lzxu/my program/MIMO Radar/data/'

SNRdBSet=[-20:2:0]; %dB

txAntNum=10;
rxAntNum=10;
[CRBDOA3,CRBAmp3]=MIMOCRBVaryAnt(txAntNum,rxAntNum,SNRdBSet);

compNum=size(CRBDOA1,1);

for compNo=1:compNum
    figure;
         semilogy(SNRdBSet, (CRBDOA1(compNo,:)),'--b',...
         SNRdBSet, (CRBDOA3(compNo,:)),'-r');
     legend('SIMO: 1 x 10', 'MIMO: 10 x 10');
%      semilogy(SNRdBSet, (CRBDOA1(compNo,:)),':',SNRdBSet, (CRBDOA2(compNo,:)),'--',...
%          SNRdBSet,(CRBDOA2a(compNo,:)),'-.',SNRdBSet, (CRBDOA3(compNo,:)),'-');
%      legend('1 x 10', '1 x 15', '1 x 20','10 x 10');

     xlabel('SNR (dB)');
     ylabel('CRB of \theta_3');
        % grid on;
     tmp=['MSE_DOA_' num2str(compNo)];
     myboldify1
     print('-deps',[savePath tmp '.eps']);
 end;
 
 for compNo=1:compNum
    figure;
    semilogy(SNRdBSet, (CRBAmp1(compNo,:)),':',SNRdBSet, (CRBAmp2(compNo,:)),'--',...
        SNRdBSet, (CRBAmp2a(compNo,:)),'-.',SNRdBSet, (CRBAmp3(compNo,:)),'-');
     legend('1 x 10', '1 x 15','1 x 20', '10 x 10');
     xlabel('SNR (dB)');
     ylabel('CRB');
     %grid on;
     
     tmp=['RMSE_Amp_' num2str(compNo)];
      myboldify1
     print('-deps',[savePath tmp '.eps']);
 end;
 


%%%%%%%%%%%%%%%%%%%%%%%
function [CRBDOA,CRBAmp]=MIMOCRBVaryAnt(txAntNum,rxAntNum,SNRdBSet);

txDist= 0.5*ones(1,txAntNum-1);%
rxDist=0.5*ones(1,rxAntNum-1);%[0.5 1. 1.5 4];%
sampNum=128;
%betaTrue=[1 1]; 
betaTrue=[ -0.0046 + 1.1164i, 0.8954 - 0.9620i]
DOATrue=[10 8 ];
compNum=length(DOATrue);
scanSetOrg=[-60:0.05:60];
SNRSet=10.^(SNRdBSet/20);
SNRNum=length(SNRdBSet);

JammerAmp=0; %dB
DOAJammer=0;

% waveform: hardmard codes
temp=[1];
for kk=1:ceil(log2(sampNum))
  temp=[temp temp; temp -1*temp];
end;
waveformSet=temp;
waveform=(waveformSet(1:txAntNum, 1:sampNum)+j*waveformSet(txAntNum+1:2*txAntNum, 1:sampNum))/sqrt(2)/sqrt(txAntNum);
%waveform=(randn(txAntNum,sampNum)+j*randn(txAntNum, sampNum) )/sqrt(2)./sqrt(txAntNum);
% correlation matrix 
  Q_struct=zeros(rxAntNum,rxAntNum);
   for row_no=1:rxAntNum
      for col_no=1:rxAntNum
          Q_struct(row_no,col_no)=0.9^abs(col_no-row_no)*exp(j*pi/2*(col_no-row_no));
      end;
    end;
 
rxTmp=cumsum([0 rxDist]);
rxstvJammer=exp(-j*2*pi*rxTmp' * sin(DOAJammer*pi/180) );

  %%%%%%%%%%%%%%%%%%%%  
 CRBDOA=[];
 CRBAmp=[];
 
 
  for snrno=1:SNRNum;
%       SNR=SNRSet(snrno);
%       Q=Q_struct./(SNR.^2) ;%+ rxstvJammer*rxstvJammer'*10^(JammerdB/10);
%       
%      rxTmp=cumsum([0 rxDist]);
%      rxStvJammer=exp(-j*2*pi*rxTmp' * sin(DOAJammer*pi/180) );
%      Q = Q + rxStvJammer*diag(JammerAmp.*conj(JammerAmp))*rxStvJammer';

Q=[ 0.1000             0.0000 + 0.0900i  -0.0810 + 0.0000i  -0.0000 - 0.0729i   0.0656 - 0.0000i   0.0000 + 0.0590i  -0.0531 + 0.0000i  -0.0000 - 0.0478i   0.0430 - 0.0000i   0.0000 + 0.0387i
   0.0000 - 0.0900i   0.1000             0.0000 + 0.0900i  -0.0810 + 0.0000i  -0.0000 - 0.0729i   0.0656 - 0.0000i   0.0000 + 0.0590i  -0.0531 + 0.0000i  -0.0000 - 0.0478i   0.0430 - 0.0000i
  -0.0810 - 0.0000i   0.0000 - 0.0900i   0.1000             0.0000 + 0.0900i  -0.0810 + 0.0000i  -0.0000 - 0.0729i   0.0656 - 0.0000i   0.0000 + 0.0590i  -0.0531 + 0.0000i  -0.0000 - 0.0478i
  -0.0000 + 0.0729i  -0.0810 - 0.0000i   0.0000 - 0.0900i   0.1000             0.0000 + 0.0900i  -0.0810 + 0.0000i  -0.0000 - 0.0729i   0.0656 - 0.0000i   0.0000 + 0.0590i  -0.0531 + 0.0000i
   0.0656 + 0.0000i  -0.0000 + 0.0729i  -0.0810 - 0.0000i   0.0000 - 0.0900i   0.1000             0.0000 + 0.0900i  -0.0810 + 0.0000i  -0.0000 - 0.0729i   0.0656 - 0.0000i   0.0000 + 0.0590i
   0.0000 - 0.0590i   0.0656 + 0.0000i  -0.0000 + 0.0729i  -0.0810 - 0.0000i   0.0000 - 0.0900i   0.1000             0.0000 + 0.0900i  -0.0810 + 0.0000i  -0.0000 - 0.0729i   0.0656 - 0.0000i
  -0.0531 - 0.0000i   0.0000 - 0.0590i   0.0656 + 0.0000i  -0.0000 + 0.0729i  -0.0810 - 0.0000i   0.0000 - 0.0900i   0.1000             0.0000 + 0.0900i  -0.0810 + 0.0000i  -0.0000 - 0.0729i
  -0.0000 + 0.0478i  -0.0531 - 0.0000i   0.0000 - 0.0590i   0.0656 + 0.0000i  -0.0000 + 0.0729i  -0.0810 - 0.0000i   0.0000 - 0.0900i   0.1000             0.0000 + 0.0900i  -0.0810 + 0.0000i
   0.0430 + 0.0000i  -0.0000 + 0.0478i  -0.0531 - 0.0000i   0.0000 - 0.0590i   0.0656 + 0.0000i  -0.0000 + 0.0729i  -0.0810 - 0.0000i   0.0000 - 0.0900i   0.1000             0.0000 + 0.0900i
   0.0000 - 0.0387i   0.0430 + 0.0000i  -0.0000 + 0.0478i  -0.0531 - 0.0000i   0.0000 - 0.0590i   0.0656 + 0.0000i  -0.0000 + 0.0729i  -0.0810 - 0.0000i   0.0000 - 0.0900i   0.1000          ]
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % calculate CRB
   [CRBDOATmp, CRBAmpTmp]=calCRB(txDist,rxDist,betaTrue,DOATrue,waveform,Q);
    
   
     CRBDOA=[CRBDOA,diag(CRBDOATmp)];
     CRBAmp=[CRBAmp, diag(CRBAmpTmp)];
 end;

% figure;
%  plot(SNRdBSet, CRBDOA(1,:),'-',SNRdBSet, CRBDOA(2,:),'--',SNRdBSet, CRBDOA(3,:),'-.');
% legend('Comp 1', 'Comp 2', 'Comp 3');
%  figure;
%  plot(SNRdBSet, CRBAmp(1,:),'-',SNRdBSet, CRBAmp(2,:),'--',SNRdBSet, CRBAmp(3,:),'-.');
%   legend('Comp 1', 'Comp 2', 'Comp 3');
 
