function coefficients=fractional_polynomial_interpolation(function_values,sample_points,powers)
if ~( size(function_values)~=size(sample_points)|| size(sample_points)~= size(powers))
    n=size(function_values);
    A=zeros(n,n);
    i=1;
    while i<=n
        j=1;
        while j<=M
            A(i,j)=sample_points(i)^powers(j);
            j=j+1;
        end
        i=i+1;
    end
    coefficients=A\function_values;
end

end