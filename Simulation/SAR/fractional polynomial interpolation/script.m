%Fractional polynomial interpolation
%An nth order polynomial is defined as a function of the form
%p(x)=sum i=0 to n a_i x^i.  To evaluate a polynomial we need 
%a sequence a_i of coeffiients. a more general class of functions 
%can be defined as p(x)=integral 
%
%
%
%
powers=[1,2,3,4.5,6];
sample_points=[0,2,3,7,9];
function_values=sample_points
fractional_polynomial_interpolation(function_values,sample_points,powers)