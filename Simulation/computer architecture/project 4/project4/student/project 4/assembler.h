#include "configuration.h"


class Assembler{
public:
	void translate_instruction(string instr, string& machine_instr);
	string get_register_bits_by_name(string register_name);
	int convert_string_to_int(const string& s);
	string convert_int_to_string(const int& i);
	string twos_complement_for_integer(int num, int bit_length);
};