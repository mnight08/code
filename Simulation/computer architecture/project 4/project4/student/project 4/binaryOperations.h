#include "configuration.h"

class BinaryOperations{
	bool overflow;
public:
	bool bit_addition(bool op1, bool op2, bool carry, bool& sum);
	bool unsigned_addition(Word op1, Word op2, Word& sum);
	unsigned int word_to_unsigned_int(Word bin);
	bool unsigned_decimal_to_word(unsigned int d, Word& bin);
	bool decimal_to_twos_compliment(int d, Word& bin);
	bool twos_compliment_addition(Word op1, Word op2, Word& sum);
	bool twos_compliment_negation(Word& bin);
	int twos_compliment_to_decimal(Word bin);
	//void copy_word(Word in, Word& out);
	void and(Word op1, Word op2, Word& result);
	void or(Word op1, Word op2, Word& result);
	bool subtraction(Word op1, Word op2, Word& result);
	bool set_less_than(Word op1, Word op2);
	bool shit_left_logical(Word op1, int d,Word& op2);
};