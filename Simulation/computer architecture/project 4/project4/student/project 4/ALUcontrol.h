#include "configuration.h"

class ALUcontrol{
	Word input_value;
	bool ALUop0;
	bool ALUop1;
	AluControl control;
public:
	void set_input_value(Word instruction);
	void set_ALUop0(bool b);
	void set_ALUop1(bool b);
	void execute();
	void get_output(AluControl& c);
};