#include "configuration.h"

class Memory{
	Word mem[NUMBER_OF_WORDS]; 
	Word input_address;
	Word output_data;
	bool mem_read;
	bool mem_write;
	Word input_data;
	int number_of_data;
	int starting_address_of_data;
public:
	Memory();
	void set_input_address(Word address);
	void set_input_data(Word data);
	void set_mem_read(bool read);
	void set_mem_write(bool write);
	void execute();
	void get_output_data(Word& data);
	void Memory::display_data();
};