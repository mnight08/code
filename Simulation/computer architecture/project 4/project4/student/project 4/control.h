#include "configuration.h"

class Control{
	Word input_value;
	bool regDst;
	bool branch;
	bool branchNE;
	bool jump;
	bool jumpLink;
	bool jumpRegister;
	bool memRead;
	bool memToReg;
	bool memWrite;
	bool ALUsrc;
	bool regWrite;
	bool ALUop0;
	bool ALUop1;
	bool shift;
public:
	void set_input_value(Word instruction);
	void execute();
	bool get_output(string name);
};