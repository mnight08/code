#include "memory.h"
#include "control.h"

void Control::set_input_value(Word instr){
	input_value=instr;
}

void Control::execute(){
	outFile<<"/////Control Execution/////"<<endl;
	if(!input_value[WORD_WIDTH-1]&&!input_value[WORD_WIDTH-2]&&!input_value[WORD_WIDTH-3]){
		/* first row of the top table in Figure 2.19 on page 135  (000)*/
		if(!input_value[WORD_WIDTH-4]&&!input_value[WORD_WIDTH-5]&&!input_value[WORD_WIDTH-6]){
			/* R-format (000)*/
			outFile<<"(Control) R-format"<<endl;
			regDst=true;
			ALUsrc=false;
			memToReg=false;
			regWrite=true;
			memRead=false;
			memWrite=false;
			branch=false;
			branchNE=false;
			jump=false;
			jumpLink=false;
			jumpRegister=false;
			ALUop0=false;
			ALUop1=true;
			shift=false;
			if(!input_value[5]&&!input_value[4]&&input_value[3]&&!input_value[2]&&!input_value[1]&&!input_value[0]){
				outFile<<"(Control)        R-foramt jr"<<endl;
				jumpRegister=true;
				regWrite=false;
			}
			if(!input_value[5]&&!input_value[4]&&!input_value[3]&&!input_value[2]&&!input_value[1]&&!input_value[0]){
				outFile<<"(Control)        R-foramt shift"<<endl;
				shift=true;
			}
		}else if(!input_value[WORD_WIDTH-4]&&input_value[WORD_WIDTH-5]&&!input_value[WORD_WIDTH-6]){
			/* jump (010)*/
			outFile<<"(Control) j"<<endl;
			regDst=false;
			ALUsrc=false;
			memToReg=false;
			regWrite=false;
			memRead=false;
			memWrite=false;
			branch=false;
			branchNE=false;
			jump=true;
			jumpLink=false;
			jumpRegister=false;
			ALUop0=false;
			ALUop1=false;
			shift=false;
		}else if(!input_value[WORD_WIDTH-4]&&input_value[WORD_WIDTH-5]&&input_value[WORD_WIDTH-6]){
			/* jump and link(011)*/
			outFile<<"(Control) jal"<<endl;
			regDst=true;
			ALUsrc=false;
			memToReg=false;
			regWrite=true;
			memRead=false;
			memWrite=false;
			branch=false;
			branchNE=false;
			jump=true;
			jumpLink=true;
			jumpRegister=false;
			ALUop0=false;
			ALUop1=false;
			shift=false;
		}else if(input_value[WORD_WIDTH-4]&&!input_value[WORD_WIDTH-5]&&!input_value[WORD_WIDTH-6]){
			/* beq (100)*/
			outFile<<"(Control) beq"<<endl;
			regDst=true;
			ALUsrc=false;
			memToReg=false;
			regWrite=false;
			memRead=false;
			memWrite=false;
			branch=true;
			branchNE=false;
			jump=false;
			jumpLink=false;
			jumpRegister=false;
			ALUop0=true;
			ALUop1=false;
			shift=false;
		}else if(input_value[WORD_WIDTH-4]&&!input_value[WORD_WIDTH-5]&&input_value[WORD_WIDTH-6]){
			/* bne (101)*/
			outFile<<"(Control) bne"<<endl;
			regDst=true;
			ALUsrc=false;
			memToReg=false;
			regWrite=false;
			memRead=false;
			memWrite=false;
			branch=false;
			branchNE=true;
			jump=false;
			jumpLink=false;
			jumpRegister=false;
			ALUop0=true;
			ALUop1=false;
			shift=false;
		}
	}else if(!input_value[WORD_WIDTH-1]&&!input_value[WORD_WIDTH-2]&&input_value[WORD_WIDTH-3]){
		/* second row of the top table in Figure 2.19 on page 135  (001)*/
		if(!input_value[WORD_WIDTH-4]&&!input_value[WORD_WIDTH-5]&&!input_value[WORD_WIDTH-6]){
			/* addi(000) */
			outFile<<"(Control) addi"<<endl;
			regDst=false;
			ALUsrc=true;
			memToReg=false;
			regWrite=true;
			memRead=false;
			memWrite=false;
			branch=false;
			branchNE=false;
			jump=false;
			jumpLink=false;
			jumpRegister=false;
			ALUop0=false;
			ALUop1=false;
			shift=false;
		}else if(!input_value[WORD_WIDTH-4]&&input_value[WORD_WIDTH-5]&&!input_value[WORD_WIDTH-6]){
			/* stli(010)*/
			outFile<<"(Control) slti"<<endl;
			regDst=false;
			ALUsrc=true;
			memToReg=false;
			regWrite=true;
			memRead=false;
			memWrite=false;
			branch=false;
			branchNE=false;
			jump=false;
			jumpLink=false;
			jumpRegister=false;
			ALUop0=true;
			ALUop1=true;
			shift=false;
		}
	}else if (input_value[WORD_WIDTH-1]&&!input_value[WORD_WIDTH-2]&&!input_value[WORD_WIDTH-3]&&!input_value[WORD_WIDTH-4]&&input_value[WORD_WIDTH-5]&&input_value[WORD_WIDTH-6]){
		/* lw (100011)*/
		outFile<<"(Control) lw"<<endl;
		regDst=false;
		ALUsrc=true;
		memToReg=true;
		regWrite=true;
		memRead=true;
		memWrite=false;
		branch=false;
		branchNE=false;
		jump=false;
		jumpLink=false;
		jumpRegister=false;
		ALUop0=false;
		ALUop1=false;
		shift=false;
	}else if (input_value[WORD_WIDTH-1]&&!input_value[WORD_WIDTH-2]&&input_value[WORD_WIDTH-3]&&!input_value[WORD_WIDTH-4]&&input_value[WORD_WIDTH-5]&&input_value[WORD_WIDTH-6]){
		/* sw (101011)*/
		outFile<<"(Control) sw"<<endl;
		regDst=true;
		ALUsrc=true;
		memToReg=false;
		regWrite=false;
		memRead=false;
		memWrite=true;
		branch=false;
		branchNE=false;
		jump=false;
		jumpLink=false;
		jumpRegister=false;
		ALUop0=false;
		ALUop1=false;
		shift=false;
	}
}

bool Control::get_output(string name){
	if(name=="regDst")
		return regDst;
	else if(name=="branch")
		return branch;
	else if(name=="branchNE")
		return branchNE;
	else if(name=="memRead")
		return memRead;
	else if(name=="memToReg")
		return memToReg;
	else if(name=="ALUsrc")
		return ALUsrc;
	else if(name=="memWrite")
		return memWrite;
	else if(name=="ALUop0")
		return ALUop0;
	else if(name=="ALUop1")
		return ALUop1;
	else if(name=="regWrite")
		return regWrite;
	else if(name=="jump")
		return jump;
	else if(name=="jumpLink")
		return jumpLink;
	else if(name=="jumpRegister")
		return jumpRegister;
	else if(name=="shift")
		return shift;
	return false;
}