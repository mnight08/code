.data
integer: .word 4 5 3 4 -2

.text
lw $s0, integer # initialize $s0, address of current integer
add $s1, $zero, $zero #initialize sum $S1 to zero
add $s2, $zero, $zero #initialize counter to zero
lw $s3, 4($t0)#load the number of digits to count
addi $s0,$s0, 4 #skip to the first number in the list to add
SUM:			#loop through the numbers, and add the positive ones.
	beq $s2,$s3,DONE#check if you are done with the numbers, if so, exit loop
	lw $t5, 0($s0)#load the current number
	addi $s0, $s0,4#move pointer to next number
	addi $s2, $sw, 1#increment counter
	slt $t2,$zero,$t5#check if the number is greater than zero
	beq $t2,$zero, ADDIT#if it is go add it to the sum. otherwise continue loop
	j SUM
	
ADDIT:	
	add $s1, $s1,$t5#add the current number to the sum
	j SUM #continue loop
	
	
DONE:	
li $v0, 1
add $a0, $zero, $s1
syscall
