#include "configuration.h"
#include "binaryOperations.h"
#include "assembler.h"
#define INSTRUCTION_LENGTH 32
Word ONE_W;
Word ZERO_W;

int test_assembler();
int test_binaryOperations();

int main(){

	/* constant 1 */
	ONE_W.reset();
	ONE_W[0]=1;

	test_assembler();
	int doge;
	cin>>doge;
	return 1;
}

int test_assembler()
{
	ifstream assemb_prog;
	ofstream machine_prog;
	string assemb_instr, machine_instr;
	Assembler assembler;

	assemb_prog.open("assembly_sample.txt");
	machine_prog.open("machine_program.txt");

	while(!assemb_prog.eof())
	{
		getline(assemb_prog,assemb_instr);
		machine_instr.assign(INSTRUCTION_LENGTH,'0');
		assembler.translate_instruction(assemb_instr,machine_instr);
		machine_prog<<machine_instr<<endl;
	}
	assemb_prog.close();
	machine_prog.close();
	return 0;
}

int test_binaryOperations(){
	ifstream inFile;
	ofstream outFile;
	string str1, str2;
	bool carry,b;
	BinaryOperations binOp;
	int d;
	Word w;


	outFile.open(".\\testData\\binaryOperations\\test_result_data.txt");

	inFile.open(".\\testData\\binaryOperations\\unsigned_addition_test_data.txt");
	outFile<<"Now testing addition of unsigned binary words"<<endl;
	while(!inFile.eof()){
		carry=false;
		inFile>>str1>>str2;
		Word word1(str1), word2(str2),sum;
		carry=binOp.unsigned_addition(word1,word2,sum);
		outFile<<carry<<" "<<sum<<endl;  //output the carry bit followed by the sum
	}
	inFile.close();
	inFile.clear();

	inFile.open(".\\testData\\binaryOperations\\unsigned_decimal_to_word_test_data.txt");
	outFile<<endl<<"Now testing conversion of unsigned integer to binary word"<<endl;
	while(!inFile.eof()){
		inFile>>d;
		b=binOp.unsigned_decimal_to_word(d,w);
		if(!b)
			outFile<<d<<" is out of range"<<endl;
		else{
			outFile<<w<<endl;  //output the binary word
		}
	}
	inFile.close();
	inFile.clear();


	inFile.open(".\\testData\\binaryOperations\\word_to_unsigned_int_test_data.txt");
	outFile<<endl;
	outFile<<"Now testing conversion of binary word to unsigned integer"<<endl;
	while(!inFile.eof()){
		inFile>>str1;
		Word bin(str1);
		d=binOp.word_to_unsigned_int(bin);
		outFile<<d<<endl;  //output the unsigned integer
	}
	inFile.close();
	inFile.clear();

	inFile.open(".\\testData\\binaryOperations\\decimal_to_twos_complement_test_data.txt");
	outFile<<endl<<"Now testing conversion of integers to binary word(2's complement representation)"<<endl;
	while(!inFile.eof()){
		inFile>>d;
		w.reset();
		b=binOp.decimal_to_twos_compliment(d,w);
		if(b)
			outFile<<w<<endl;  //output the binary word(2's complement)
		else
			outFile<<d<<" is out of range"<<endl;
	}
	inFile.close();
	inFile.clear();

	inFile.open(".\\testData\\binaryOperations\\twos_complement_to_decimal_test_data.txt");
	outFile<<endl<<"Now testing conversion of binary word(2's complement representation) to integer"<<endl;
	while(!inFile.eof()){
		inFile>>str1;
		Word bin(str1);
		d=binOp.twos_compliment_to_decimal(bin);
		outFile<<d<<endl;  //output the binary word(2's complement)
	}
	inFile.close();
	inFile.clear();

	inFile.open(".\\testData\\binaryOperations\\twos_complement_negation_test_data.txt");
	outFile<<endl<<"Now testing negation of binary word(2's complement representation)"<<endl;
	while(!inFile.eof()){
		inFile>>str1;
		Word bin(str1);
		Word& r=bin;
		b=binOp.twos_compliment_negation(r);
		if(b)
			outFile<<r<<endl;  //output the binary word(2's complement)
		else
			outFile<<str1<<" is out of range for negation"<<endl;
	}
	inFile.close();
	inFile.clear();

	inFile.open(".\\testData\\binaryOperations\\twos_complement_addition_test_data.txt");
	outFile<<endl<<"Now testing addition of binary words(2's complement representatin)"<<endl;
	while(!inFile.eof()){
		inFile>>str1>>str2;
		Word word1(str1), word2(str2),sum;
		b=binOp.twos_compliment_addition(word1,word2, sum);
		if(b)
			outFile<<sum<<endl;  //output the binary word(2's complement)
	}
	inFile.close();
	inFile.clear();

	outFile.close();
	return 1;
}