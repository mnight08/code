#ifndef CAT
#define CAT
#include "assembler.h"

#define INSTRUCTION_LENGTH 32
#define OP_CODE_START_BIT 0
#define OP_CODE_BIT_NUMBER 6
#define RS_START_BIT 6
#define RS_END_BIT 10
#define RT_START_BIT 11
#define RT_END_BIT 15
#define RD_START_BIT 16
#define RD_END_BIT 20
#define SHAMT_START_BIT 21
#define SHAMT_BIT_NUMBER 5
#define SHAMT_DEFAULT "00000"
#define FUNC_START_BIT 26
#define FUNC_BIT_NUMBER 6
#define IMMEDIATE_START_BIT 16
#define IMMEDIATE_BIT_NUMBER 16
#define ADDRESS_START_BIT 6
#define ADDRESS_BIT_NUMBER 26
#define REGISTER_BIT_NUMBER 5
#define OP_CODE_R_FORMAT "000000"
#define FUNC_CODE_ADD "100000"
#define FUNC_CODE_SLT "101010"
#define FUNC_CODE_SLL "000000"
#define FUNC_CODE_JR "001000"
#define FUNC_CODE_AND "100100"
#define FUNC_CODE_NOR "100111"
#define FUNC_CODE_OR "100101"
#define FUNC_CODE_SLTU "101011"
#define FUNC_CODE_SRL "000010"
#define FUNC_CODE_SUB "100010"
#define FUNC_CODE_ADDU "100001"
#define FUNC_CODE_SUBU "100011"
#define OP_CODE_BEQ "000100"
#define OP_CODE_BNE "000101"
#define OP_CODE_LW "100011"
#define OP_CODE_SW "101011"
#define OP_CODE_ADDI "001000"
#define OP_CODE_SLTI "001010"
#define OP_CODE_JAL "000011"
#define OP_CODE_J "000010"
#define OP_CODE_ADDI "001000"
#define OP_CODE_ADDIU "001001"
#define OP_CODE_ANDI "001100"
#define OP_CODE_LBU "100100"
#define OP_CODE_LHU "100101"
#define OP_CODE_LL "110000"
#define OP_CODE_LUI "001111"
#define OP_CODE_ORI "001101"
#define OP_CODE_SLTIU "001011"
#define OP_CODE_SB "101000"
#define OP_CODE_SC "111000"
#define OP_CODE_SH "101001"
#define OP_CODE_ADDIU "001001"


/* translate assembly instruction to machine instruction 
   Sample Input: add $s2 $a0 $zero
   Sample Output: 00000000100000001001000000100000
*/
void Assembler::translate_instruction(string instr, string& machine_instr)
{
	string operation,rs,rt,rd;
	istringstream instream;
	int immediate_number,target_address,shamt;
	string str;

	/* change string instr to a istringstream, which can be used to extract different fields in str */
	instream.clear();
	instream.str(instr);
	instream>>operation; // extract operation field.

	/* translate R-format instructions here */
	if(operation=="add" || operation=="slt"|| operation=="addu"|| operation=="and"|| operation=="jr"|| operation=="nor"|| operation=="or"|| operation=="sltu"|| operation=="sll"|| operation=="srl"|| operation=="sub"|| operation=="subu")
	{

		machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_R_FORMAT); //replace the bits for opcode
		instream>>rd>>rs>>rt; //extract three operands(registers).		
		if(operation!="jr")
		{
			/* replace the bits of register rs */
		machine_instr.replace(RS_START_BIT,REGISTER_BIT_NUMBER,get_register_bits_by_name(rs));
		/* replace the bits of register rt */
		if(rt[0]=='$')
			machine_instr.replace(RT_START_BIT,REGISTER_BIT_NUMBER,get_register_bits_by_name(rt));
		else shamt=convert_string_to_int(rt);
		/* replace the bits of register rd */
		machine_instr.replace(RD_START_BIT,REGISTER_BIT_NUMBER,get_register_bits_by_name(rd));
		/* replace the bits of func field for addition */
		}
		if(operation=="add")
		{
			machine_instr.replace(FUNC_START_BIT,FUNC_BIT_NUMBER,FUNC_CODE_ADD); //replace the bits of funcion_code for add
			machine_instr.replace(SHAMT_START_BIT,SHAMT_BIT_NUMBER,SHAMT_DEFAULT);
		}
		else if(operation=="slt")
		{
			machine_instr.replace(FUNC_START_BIT,FUNC_BIT_NUMBER,FUNC_CODE_SLT); //replace the bits for funcion_code for slt
			machine_instr.replace(SHAMT_START_BIT,SHAMT_BIT_NUMBER,SHAMT_DEFAULT);
		}
		else if(operation=="addu")
		{
			machine_instr.replace(FUNC_START_BIT,FUNC_BIT_NUMBER,FUNC_CODE_ADDU);
			machine_instr.replace(SHAMT_START_BIT,SHAMT_BIT_NUMBER,SHAMT_DEFAULT);
		}
		else if(operation=="and")
		{
			machine_instr.replace(FUNC_START_BIT,FUNC_BIT_NUMBER,FUNC_CODE_AND);
			machine_instr.replace(SHAMT_START_BIT,SHAMT_BIT_NUMBER,SHAMT_DEFAULT);
		}
		else if(operation=="jr")
		{
			machine_instr.replace(FUNC_START_BIT,FUNC_BIT_NUMBER,FUNC_CODE_JR);
			machine_instr.replace(SHAMT_START_BIT,SHAMT_BIT_NUMBER,SHAMT_DEFAULT);
			rs=rd;
			machine_instr.replace(RS_START_BIT,REGISTER_BIT_NUMBER,get_register_bits_by_name(rs));
		}
		else if(operation=="nor")
		{
			machine_instr.replace(FUNC_START_BIT,FUNC_BIT_NUMBER,FUNC_CODE_NOR);
			machine_instr.replace(SHAMT_START_BIT,SHAMT_BIT_NUMBER,SHAMT_DEFAULT);
		}
		else if(operation=="or")
		{
			machine_instr.replace(FUNC_START_BIT,FUNC_BIT_NUMBER,FUNC_CODE_OR);
			machine_instr.replace(SHAMT_START_BIT,SHAMT_BIT_NUMBER,SHAMT_DEFAULT);
		}
		else if(operation=="sltu")
		{
			machine_instr.replace(FUNC_START_BIT,FUNC_BIT_NUMBER,FUNC_CODE_SLTU);
			machine_instr.replace(SHAMT_START_BIT,SHAMT_BIT_NUMBER,SHAMT_DEFAULT);
		}
		else if(operation=="sll")
		{
			machine_instr.replace(FUNC_START_BIT,FUNC_BIT_NUMBER,FUNC_CODE_SLL);
			str=twos_complement_for_integer(convert_string_to_int(rt), SHAMT_BIT_NUMBER+1);
			str=str.substr(1,str.size()-1);
			machine_instr.replace(SHAMT_START_BIT,SHAMT_BIT_NUMBER,str);
		}
		else if(operation=="srl")
		{
			machine_instr.replace(FUNC_START_BIT,FUNC_BIT_NUMBER,FUNC_CODE_SRL);
			str=twos_complement_for_integer(convert_string_to_int(rt), SHAMT_BIT_NUMBER+1);
			str=str.substr(1,str.size()-1);
			machine_instr.replace(SHAMT_START_BIT,SHAMT_BIT_NUMBER,str);
		}
		else if(operation=="sub")
		{
			machine_instr.replace(FUNC_START_BIT,FUNC_BIT_NUMBER,FUNC_CODE_SUB);
			machine_instr.replace(SHAMT_START_BIT,SHAMT_BIT_NUMBER,SHAMT_DEFAULT);
		}
		else if(operation=="subu")
		{
			machine_instr.replace(FUNC_START_BIT,FUNC_BIT_NUMBER,FUNC_CODE_SUBU);
			machine_instr.replace(SHAMT_START_BIT,SHAMT_BIT_NUMBER,SHAMT_DEFAULT);
		}
	}
	//I format
	else if(operation=="addi"||operation=="addiu"||operation=="andi"||operation=="beq"||operation=="bne"||operation=="lbu"||operation=="lhu"||operation=="ll"||operation=="lui"||operation=="lw"||operation=="ori"||operation=="slti"||operation=="sltiu"||operation=="sb"||operation=="sc"||operation=="sh"||operation=="sw")
	{
		instream>>rs>>rt>>immediate_number;
		if(rt[0]!='$')
		{	int i=0;
			for(;i<rt.size();i++)
			{
				if(rt[i]=='(')break;
			}
			str=twos_complement_for_integer(convert_string_to_int(rt.substr(0,i)),IMMEDIATE_BIT_NUMBER+1);
			str=str.substr(1,str.size()-1);
			rt=rt.substr(i+1,3);
		} 		
		else 
		{
			machine_instr.replace(RT_START_BIT,REGISTER_BIT_NUMBER,get_register_bits_by_name(rt));
			str=twos_complement_for_integer(immediate_number, IMMEDIATE_BIT_NUMBER);
		}
		/* replace the bits of register rs */
		machine_instr.replace(RS_START_BIT,REGISTER_BIT_NUMBER,get_register_bits_by_name(rs));
		/* replace the bits of register rt */


		/* replace the bits for immediate*/
		machine_instr.replace(IMMEDIATE_START_BIT,IMMEDIATE_BIT_NUMBER,str);
		/* replace bits for opcode*/
		if(operation=="addi")
			machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_ADDI);
		else if(operation=="addiu")
			machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_ADDIU);
		else if(operation=="andi")
			machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_ANDI);
		else if(operation=="beq")
			machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_BEQ);
		else if(operation=="bne")
			machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_BNE);
		else if(operation=="lbu")
			machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_LBU);
		else if(operation=="lhu")
			machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_LHU);
		else if(operation=="ll")
			machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_LL);
		else if(operation=="lui")
			machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_LUI);
		else if(operation=="lw")
			machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_LW);
		else if(operation=="ori")
			machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_ORI);
		else if(operation=="slti")
			machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_SLTI);
		else if(operation=="sltiu")
			machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_SLTIU);
		else if(operation=="sb")
			machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_SB);
		else if(operation=="sc")
			machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_SC);
		else if(operation=="sh")
			machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_SH);
		else if(operation=="sw")
			machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_SW);
	}
	//J format
	else if(operation=="j"||operation=="jal")
	{
		/* insert address bits */
		instream>>target_address;
		str=twos_complement_for_integer(target_address, ADDRESS_BIT_NUMBER+1);
		str=str.substr(1,str.size()-1);
		machine_instr.replace(ADDRESS_START_BIT,ADDRESS_BIT_NUMBER,str);
		
		/* insert opcode*/
		if(operation=="j")
			machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_J);
		else if(operation=="jal")
			machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_JAL);
	}
}

/* get the 2's complement representation (bit_length bits) for an integer
   Sample Input: -5, 16
   Sample Output: 1111111111111010
*/
string Assembler::twos_complement_for_integer(int num, int bit_length)
{
	int max_pos=(1<<(bit_length-1))-1;  //maximum positive integer for IMMEDIATE_BIT_NUMBER bits
	string bits;
	int i;

	bits.resize(bit_length);
	if(num>max_pos||num<-(max_pos+1))
	{
		cout<<"ERROR! Integer "<<num<<" is too big for "<<bit_length<<" bits!"<<endl;
		exit(0);
	}
	if(num==-(max_pos+1))
	{
		bits=string(bit_length,'0');
		bits.replace(0,1,"1");
	}else
	{
		int temp=abs(num);
		/* get the binary reprsentation of number num */
		for(i=0;i<bit_length;i++)
		{
			if(temp%2)
				bits.replace(bit_length-i-1,1,"1");
			else
				bits.replace(bit_length-i-1,1,"0");
			temp>>=1;
		}
		if(num<0)
		{
			for(i=0;i<bit_length;i++)
				bits.replace(i,1,(bits[i]=='0')?"1":"0");
			i=bit_length-1;
			while(bits[i]=='1') i--; //find the rightmost position which is '0'
			bits.replace(i,1,"1"); //set this bit to 1
			if(i<bit_length-1)
				bits.replace(i+1,bit_length-i-1,string(bit_length-i-1,'0')); //replace the bits to the right with all zeros
		}
	}
	return bits;
}


/* get the integer number from the register name 
   Sample Input: t8
   Sample Output: 11000
*/
string Assembler::get_register_bits_by_name(string register_name)
{
	string register_bits;
	if(register_name=="$zero")
		register_bits="00000";
	else if(register_name=="$at")
		register_bits="00001";
	else if(register_name=="$v0")
		register_bits="00010";
	else if(register_name=="$v1")
		register_bits="00011";
	else if(register_name=="$a0")
		register_bits="00100";
	else if(register_name=="$a1")
		register_bits="00101";
	else if(register_name=="$a2")
		register_bits="00110";
	else if(register_name=="$a3")
		register_bits="00111";
	else if(register_name=="$t0")
		register_bits="01000";
	else if(register_name=="$t1")
		register_bits="01001";
	else if(register_name=="$t2")
		register_bits="01010";
	else if(register_name=="$t3")
		register_bits="01011";
	else if(register_name=="$t4")
		register_bits="01100";
	else if(register_name=="$t5")
		register_bits="01101";
	else if(register_name=="$t6")
		register_bits="01110";
	else if(register_name=="$t7")
		register_bits="01111";
	else if(register_name=="$s0")
		register_bits="10000";
	else if(register_name=="$s1")
		register_bits="10001";
	else if(register_name=="$s2")
		register_bits="10010";
	else if(register_name=="$s3")
		register_bits="10011";
	else if(register_name=="$s4")
		register_bits="10100";
	else if(register_name=="$s5")
		register_bits="10101";
	else if(register_name=="$s6")
		register_bits="10110";
	else if(register_name=="$s7")
		register_bits="10111";
	else if(register_name=="$t8")
		register_bits="11000";
	else if(register_name=="$t9")
		register_bits="11001";
	else if(register_name=="$k0")
		register_bits="11010";
	else if(register_name=="$k1")
		register_bits="11011";
	else if(register_name=="$gp")
		register_bits="11100";
	else if(register_name=="$sp")
		register_bits="11101";
	else if(register_name=="$fp")
		register_bits="11110";
	else if(register_name=="$ra")
		register_bits="11111";
	else
	{
		cout<<"ERROR! Incorrect Register Name!"<<endl;
		exit(0);
	}
	return register_bits;
}

/* convert a string to an integer.
   Sample Input: 53(string).
   Sample Output: 53(integer).
*/
int Assembler::convert_string_to_int(const string& s)
{
     istringstream stream (s);
     int t;
     stream >> t;
     return t;
}
/* convert an integer to a string.
   Sample Input: 53(integer).
   Sample Output: 53(string).
*/
string Assembler::convert_int_to_string(const int& i)
{
	stringstream ss;
	ss<<i;
	return ss.str();
}

#endif