#include "binaryOperations.h"
//for debugging purposes
#include <iostream>
/* calcuate the binary bit addition sum=op1+op2+carry with carry bit c returned
 */
bool BinaryOperations::bit_addition(bool op1, bool op2, bool c, bool& sum){
	if((!op1&&!op2)&&!c)
	{
		sum=false;
		return false;
	}
	else if((!op1&&!op2)&&c)
	{
		sum=true;
		return false;
	}
	else if((!op1&&op2)&&!c)
	{
		sum=true;
		return false;
	}
	else if((!op1&&op2)&&c)
	{
		sum=false;
		return true;
	}
	else if((op1&&!op2)&&!c)
	{
		sum=true;
		return false;
	}
	else if((op1&&!op2)&&c)
	{
		sum=false;
		return true;
	}
	else if((op1&&op2)&&!c)
	{
		sum=false;
		return true;
	}
	else if((op1&&op2)&&c)
	{
		sum=true;
		return true;
	}
}

/* calculate the binary addition sum=op1+op2 with carry bit returned
 */
bool BinaryOperations::unsigned_addition(Word op1, Word op2, Word& sum){
	bool carry=false;
	bool swap=false;
	for(int i=0;i<WORD_WIDTH;i++)
	{	
		swap=sum[i];
		carry=bit_addition(op1[i],op2[i],carry,swap);
		sum[i]=swap;
	}
	return carry;
}

/* convert a binary word to an unsigned integer and return the integer
 */
unsigned int BinaryOperations::word_to_unsigned_int(Word bin){
	int weight=1;
	int integer=0;
	for(int i=0;i<WORD_WIDTH;i++)
	{
		for(int j=1;j<=i;j++)
		{
			weight=2*weight;
		}
		if(bin[i])	integer=integer+weight;
		weight=1;
	}
	return integer;
}

/* convert an unsigned integer to a binary word(unsigned representation)
   return FALSE if the integer is out of range
   return TRUE otherwise
 */
bool BinaryOperations::unsigned_decimal_to_word(unsigned int d, Word& bin){
	unsigned int limit=1;
	Word max;
	for(int i=0;i<WORD_WIDTH;i++)
	{
		max[i]=1;
	}
	limit=word_to_unsigned_int(max);
	if(d>limit)return false;
	else 
	{
		bin=ZERO_W;
		for( int i=0;i<WORD_WIDTH && d!=0;i++)
		{
			bin[i]= d%2;
			d=d/2;
		}
		return true;
	}
}

/* convert an integer to a binary word(2's complement representation)
   return FALSE if the number is out of range
   return TRUE otherwise
 */
bool BinaryOperations::decimal_to_twos_compliment(int d, Word& bin){
		int lowerlimit=-1;
		if(d<0){
			
			for(int i=1;i<WORD_WIDTH;i++)
			{
				lowerlimit=lowerlimit*2;
			}
			if(d<lowerlimit)
				return false;
			else{
				d=-d;
				unsigned_decimal_to_word(d,bin);
				std::cout<<"this is the number you are testing";
				for(int i=0;i<WORD_WIDTH;i++)
				{
					
					bin[i]=!bin[i];std::cout<<bin[i];
				}
				std::cout<<"this is d"<<d<<endl;
				unsigned_addition(bin,ONE_W,bin);
				return true;
			}
		}
		else{
			Word temp;
			for(int i=0;i<WORD_WIDTH-1;i++)
				temp[i]=1;
			int max=word_to_unsigned_int(temp);
			if(d>max)return false;
			else return unsigned_decimal_to_word(d,bin);
		}
}

/* calculate sum=op1+op2, all have 2's complement representation
 */
bool BinaryOperations::twos_compliment_addition(Word op1, Word op2, Word& sum){
	Word temp;

	unsigned_addition(op1,op2,temp);
	if((op1[WORD_WIDTH-1]&&op2[WORD_WIDTH-1])&&!temp[WORD_WIDTH-1])return false;
	else if((!op1[WORD_WIDTH-1]&&!op2[WORD_WIDTH-1])&&temp[WORD_WIDTH-1])return false;
	else unsigned_addition(op1,op2,sum);
	return true;
	
}

/* find the negation of a binary word 
   return FALSE if no negation exists
   return TRUE otherwise
 */
bool BinaryOperations::twos_compliment_negation(Word& bin){
	//this seemed to be the easiest way to do the job  below is a 
	//more complicated route I origionally took, but the logic here is much
	//simpler.  all of the error handling is already taken care of by 
	//decimal_to_twos_compliment

	int d=twos_compliment_to_decimal(bin);
	d=-d;
	return decimal_to_twos_compliment(d,bin);
	
	/*
	//bin is negative
	if(bin[WORD_WIDTH-1])
	{

		Word negative_one;
		for(int i=0;i<WORD_WIDTH;i++)
		{
			negative_one[i]=1;
		}
		
		unsigned_addition(bin,negative_one,bin);
		
		for(int i=0;i<WORD_WIDTH;i++)
		{
			bin[i]=!bin[i];
		}
		
	}
	//bin is positive
	else{
	
		for(int i=0;i<WORD_WIDTH;i++)
		{
			bin[i]=!bin[i];
		}
		unsigned_addition(bin,ONE_W,bin);
		return true;
	}
	*/
}

unsigned int power(unsigned int base, unsigned int exponent)
{
	unsigned int out=1;
	for( unsigned int i=0;i<exponent;i++)
		out=out*base;
	return out;
}

/* convert a binary word(2's complement representation) to an integer and return the integer
 */
int BinaryOperations::twos_compliment_to_decimal(Word bin){
	int value=0;
	value=power(2,WORD_WIDTH-1)*bin[WORD_WIDTH-1];
	value=-value;
	for(int i=0;i<WORD_WIDTH-1;i++)
	{
		value=value+power(2,i)*bin[i];
	}
	return value;
}