#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <bitset>
#include <algorithm>

using namespace std;

#define WORD_WIDTH 32
#define REGISTER_FIELD 5
#define NUMBER_OF_REGISTERS 33  //including pc as the last register
#define INSTRUCTION_LENGTH 32
#define NUMBER_OF_WORDS 400
#define ALU_CONTROL_BITS 4


typedef bitset<WORD_WIDTH> Word;
typedef bitset<REGISTER_FIELD> RegisterNumber;
typedef bitset<ALU_CONTROL_BITS> AluControl;

extern Word ONE_W,ZERO_W,FOUR_W;  //constant 1, 0 and 4 in the format of Word. 
extern ofstream outFile;




#endif