#include "register.h"
#include "binaryOperations.h"

/* constructor which initialize all registers from values in register_ini.txt */
Register::Register(){
	Word w=ZERO_W;
	BinaryOperations binOp;
	ifstream inFile;
	int i,d;

	inFile.open("register_ini.txt");

	regWrite=0;		//read by default
	for(i=0;i<NUMBER_OF_REGISTERS;i++){
		inFile>>d;
		binOp.decimal_to_twos_compliment(d,w);
		registers[i]=w;
	}
	inFile.close();
}

/* set input value for register rs
 */
void Register::set_input_rs(RegisterNumber rs){
	this->rs=rs;
}

void Register::set_input_rt(RegisterNumber rt){
	this->rt=rt;
}

void Register::set_input_rd(RegisterNumber rd){
	this->rd=rd;
}

void Register::set_input_read(RegisterNumber rs, RegisterNumber rt){
	this->rs=rs;
	this->rt=rt;
}

void Register::set_input_data(Word data){
	writeData=data;
}

void Register::set_write_control(bool write){
	regWrite=write;
}

void Register::execute(){
	BinaryOperations binOp;
	/* read data from rs and rt */
		out1=registers[registerNumber_to_int(rs)];
		out2=registers[registerNumber_to_int(rt)];
	if(regWrite){
		/* write data to rd */
		registers[registerNumber_to_int(rd)]=writeData;
		outFile<<"(Register Write) Number---"<<registerNumber_to_int(rd)<<":"<<binOp.twos_compliment_to_decimal(writeData)<<endl;
	}
}

void Register::get_output_rs(Word& rs){
	rs=out1;
}

void Register::get_output_rt(Word& rt){
	rt=out2;
}

int Register::registerNumber_to_int(RegisterNumber reg){
	int i, weight=1,sum=0;
	for(i=0;i<REGISTER_FIELD;i++){
		sum+=reg[i]*weight;
		weight<<=1;
	}
	return sum;
}

void Register::read_pc(Word& p){
	p=registers[NUMBER_OF_REGISTERS-1];
}
