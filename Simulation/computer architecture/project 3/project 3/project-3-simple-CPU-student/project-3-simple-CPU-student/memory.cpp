#include "memory.h"
#include "binaryOperations.h"

/* constructor which initialize memory from memory_ini.txt */
Memory::Memory(){
	Word w=ZERO_W;
	BinaryOperations binOp;
	ifstream inFile;
	int i,d,starting_address,number_of_instructions;

	inFile.open("memory_ini.txt");

	inFile>>starting_address; // get the starting address of instructions
	inFile>>number_of_instructions; //get the number of instructions

	mem_read=1;		//read by default
	mem_write=0;
	/* load instructions */
	for(i=0;i<number_of_instructions;i++){
		inFile>>mem[4*i+starting_address];
	}
	/* load data */
	inFile>>starting_address_of_data; // get the starting address of data
	inFile>>number_of_data;	  // get the number of data
	/* load data */
	for(i=0;i<number_of_data;i++){
		inFile>>d;
		binOp.decimal_to_twos_compliment(d,w);
		mem[4*i+starting_address_of_data]=d;
	}
}

void Memory::set_input_address(Word address){
	input_address=address;
}

void Memory::set_input_data(Word data){
	input_data=data;
}

void Memory::set_mem_read(bool read){
	mem_read=read;
}

void Memory::set_mem_write(bool write){
	mem_write=write;
}

void Memory::execute(){
	BinaryOperations binOp;
	if(mem_read)
		//read first
		output_data=mem[binOp.word_to_unsigned_int(input_address)];
	if(mem_write){
		outFile<<"(Memory Write) Address---"<<binOp.word_to_unsigned_int(input_address)<<":"<<binOp.twos_compliment_to_decimal(input_data)<<endl;
		mem[binOp.word_to_unsigned_int(input_address)]=input_data;
	}
}

void Memory::get_output_data(Word& data){
	data=output_data;
	//cout<<"output_data:"<<data<<"\t data:"<<data<<endl;
}

void Memory::display_data(){
	BinaryOperations binOp;
	int i;
	cout<<"-----------Now display data in memory-----------"<<endl;
	for(i=0;i<number_of_data;i++){
		outFile<<"address "<<starting_address_of_data+i<<":"<<binOp.twos_compliment_to_decimal(mem[i+starting_address_of_data])<<endl;
	}
}