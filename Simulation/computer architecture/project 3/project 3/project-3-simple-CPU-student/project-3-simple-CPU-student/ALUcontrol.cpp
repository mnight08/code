#include "memory.h"
#include "ALUcontrol.h"

void ALUcontrol::set_input_value(Word instr){
	input_value=instr;
}

void ALUcontrol::set_ALUop0(bool b){
	ALUop0=b;
}

void ALUcontrol::set_ALUop1(bool b){
	ALUop1=b;
}

/* set ALU control bits according to Figure 4.12 on page 317 */
void ALUcontrol::execute(){
	if(ALUop1 && ALUop0){
		/* stli(11) */
		/* operations: 0111(set on less than) */	
			control[3]=false;
			control[2]=true;
			control[1]=true;
			control[0]=true;		
	}else if(ALUop1 && !ALUop0){
		/* R-format */
		if(input_value[3]&&!input_value[2]&&!input_value[1]&&!input_value[0]){
			/* default operation for jr: 1111(do nothing) */
			control[3]=true;
			control[2]=true;
			control[1]=true;
			control[0]=true;
		}else if(input_value[3]){
			/* operations: 0111(set on less than) */	
			control[3]=false;
			control[2]=true;
			control[1]=true;
			control[0]=true;
		}else{
			if(input_value[2]){
				if(input_value[0]){
					/* operation: 0001(or) */
					control[3]=false;
					control[2]=false;
					control[1]=false;
					control[0]=true;
				}else{
					/* operation: 0000(and) */
					control[3]=false;
					control[2]=false;
					control[1]=false;
					control[0]=false;
				}
			}else{
				if(input_value[1]){
					/* operation: 0110(subtract) */
					control[3]=false;
					control[2]=true;
					control[1]=true;
					control[0]=false;
				}else{
					if(input_value[5]){
						/* operation: 0010(addition) */
						control[3]=false;
						control[2]=false;
						control[1]=true;
						control[0]=false;
					}else{
						/* operation: 0011(shift left logical) */
						control[3]=false;
						control[2]=false;
						control[1]=true;
						control[0]=true;
					}
				}
			}
		}
	}else if(!ALUop1 && ALUop0){
		/* branch(01) */
		/* 01---> operation: 0110(subtract) */
		control[3]=false;
		control[2]=true;
		control[1]=true;
		control[0]=false;
	}else{
		/* lw/sw(00) */
		/* 00---> operation: 0010(add) */
		control[3]=false;
		control[2]=false;
		control[1]=true;
		control[0]=false;
	}
	cout<<"(ALU Conrol) alu op bits---"<<ALUop1<<ALUop0<<endl;
	cout<<"(ALU Conrol) control bits---"<<control[3]<<control[2]<<control[1]<<control[0]<<endl;
}

void ALUcontrol::get_output(AluControl& c){
	c=control;
}