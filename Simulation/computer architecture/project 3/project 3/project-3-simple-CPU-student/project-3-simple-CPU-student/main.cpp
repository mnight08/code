#include "configuration.h"
#include "binaryOperations.h"
#include "register.h"
#include "memory.h"
#include "ALU.h"
#include "control.h"
#include "ALUcontrol.h"

Word ONE_W,ZERO_W,FOUR_W,PROGRAM_EXIT;
ofstream outFile;

void simple_CPU();


int main(){

	/* constant 0 */
	ZERO_W.reset();

	/* constant 1 */
	ONE_W.reset();
	ONE_W[0]=true;

	/*constant 4*/
	FOUR_W.reset();
	FOUR_W[2]=true;

	outFile.open("output.txt");

	simple_CPU();
	
	outFile.close();

	return 1;
}


void simple_CPU(){
	ALU adder4, adderBranch,alu;
	Memory mem;
	Register reg;
	Control control;
	ALUcontrol aluControl;
	Word pc,nextPC,branchAddr,jumpAddr,instr,aluResult,rsData,rtData,rdData,memoryOutData;
	RegisterNumber rs,rt,rd, ra;
	Word shiftAmount;
	BinaryOperations binOp;
	bool regDst,branch,branchNE,memRead,memToReg,ALUop0,ALUop1,memWrite,ALUsrc,regWrite,jump,jumpLink,jumpRegister,zero,aluZero,shift;
	AluControl aluCon;
	int i,number_of_instructions=16;

	ra.set(); //register $ra
	PROGRAM_EXIT.set();
	reg.read_pc(pc);



	//set up adder4 to add 4 to its first argument.  this does not need to change each iteration in loop, so i moved it out.
	aluControl.set_ALUop0(false);
	aluControl.set_ALUop1(false);
	//set nextPc to hold add instruction
	//0 is the right most bit of a bitset
	Word ADD;
	binOp.unsigned_decimal_to_word(0x80000000,ADD);
	aluControl.set_input_value(ADD);
	aluControl.execute();
	aluControl.get_output(aluCon);
	adder4.set_control(aluCon);
	adder4.set_input_value2(FOUR_W);

	adderBranch.set_control(aluCon);

	do{
		/* fetch instruction */
		outFile<<"now executing instruction at address: "<<binOp.word_to_unsigned_int(pc)<<endl;
		mem.set_mem_read(true);
		mem.set_mem_write(false);
		mem.set_input_address(pc);
		mem.execute();
		mem.get_output_data(instr);

				
		/* get pc<---pc+4 */
		//i didnt write pc back to reg file because then it would not get the proper output.
		adder4.set_input_value1(pc);
		adder4.execute();
		adder4.get_output_data(pc,aluZero);


		/* generate control signal */
		control.set_input_value(instr);
		control.execute();
		ALUsrc=control.get_output("ALUsrc");
		memToReg=control.get_output("memToReg");
		regDst=control.get_output("regDst");
		regWrite=control.get_output("regWrite");
		memRead=control.get_output("memRead");
		memWrite=control.get_output("memWrite");
		branch=control.get_output("branch");
		branchNE=control.get_output("branchNE");
		jump=control.get_output("jump");
		jumpLink=control.get_output("jumpLink");
		jumpRegister=control.get_output("jumpRegister");		
		ALUop0=control.get_output("ALUop0");		
		ALUop1=control.get_output("ALUop1");		
		shift=control.get_output("shift");


		/* decoding get rs, rt, rd, and shiftAmount */
		rs.set(4,instr[25]);
		rs.set(3,instr[24]);
		rs.set(2,instr[23]);
		rs.set(1,instr[22]);
		rs.set(0,instr[21]);
										
		rt.set(4,instr[20]);
		rt.set(3,instr[19]);
		rt.set(2,instr[18]);
		rt.set(1,instr[17]);
		rt.set(0,instr[16]);

				
		rd.set(4,instr[15]);
		rd.set(3,instr[14]);
		rd.set(2,instr[13]);
		rd.set(1,instr[12]);
		rd.set(0,instr[11]);
		
		
		branchAddr[15]=instr[15];
		branchAddr[14]=instr[14];
		branchAddr[13]=instr[13];
		branchAddr[12]=instr[12];
		branchAddr[11]=instr[11];
		branchAddr[10]=instr[10];
		branchAddr[9]=instr[9];
		branchAddr[8]=instr[8];
		branchAddr[7]=instr[7];
		branchAddr[6]=instr[6];
		branchAddr[5]=instr[5];
		branchAddr[4]=instr[4];
		branchAddr[3]=instr[3];
		branchAddr[2]=instr[2];
		branchAddr[1]=instr[1];
		branchAddr[0]=instr[0];

		//sign extending
		for( int i=1;i<16;i++)
		{
			branchAddr[15+i]=branchAddr[15];
		}

		//initialize shiftAmount to zer0;
		binOp.unsigned_decimal_to_word(0,shiftAmount);
		//load shiftAmount
		shiftAmount.set(4,instr[10]);
		shiftAmount.set(3,instr[9]);
		shiftAmount.set(2,instr[8]);
		shiftAmount.set(1,instr[7]);
		shiftAmount.set(0,instr[6]);

		
		/* read registers */
		reg.set_input_rs(rs);
		if(regDst)
		{
			reg.set_input_rd(rd);
		}else
		{
			reg.set_input_rd(rt);

		}
		reg.set_input_rt(rt);
		reg.set_write_control(false);
		reg.execute();
		reg.get_output_rs(rsData);
		reg.get_output_rt(rtData);



		/* get control signals for ALU */
		aluControl.set_input_value(instr);
		aluControl.set_ALUop0(ALUop0);
		aluControl.set_ALUop1(ALUop1);
		aluControl.execute();
		aluControl.get_output(aluCon);

		/* execute instruction */
		alu.set_input_value1(rsData);
		if(ALUsrc)
			alu.set_input_value2(branchAddr);
		else
			alu.set_input_value2(rtData);
		alu.set_control(aluCon);
		alu.execute();
		alu.get_output_data(aluResult,aluZero);
	

		/* calcuate the branch address */
		//had to load the branch address, and sign extend it before the call to execute the instruction in order check ALUsrc.
		//shift left 2
		binOp.shit_left_logical(branchAddr,2,branchAddr);


		/* add pc+4 with shifted branchAddr */
		adderBranch.set_input_value1(pc);
		adderBranch.set_input_value2(branchAddr);
		adderBranch.execute();
		adderBranch.get_output_data(branchAddr,zero);


		/* calcuate nextPC after considering beq instructions */
		//pc=pc+4+branchAddr
		if(branch&&aluZero)
		{
			nextPC=branchAddr;
		}
		//pc=pc+4
		else
		{
			nextPC=pc;
		}

		
		/* read/write memory if necessary */
		mem.set_input_data(rtData);
		mem.set_mem_read(memRead);
		mem.set_mem_write(memWrite);
		mem.set_input_address(aluResult);
		mem.execute();
		mem.get_output_data(memoryOutData);

		/* write register if necessary */
		reg.set_write_control(regWrite);
		if(memToReg)
		{
			reg.set_input_data(memoryOutData);
		}
		else
		{
			reg.set_input_data(aluResult);
		}
		reg.execute();
		

		/* set pc */
		pc=nextPC;

		outFile<<endl;
		number_of_instructions--;
	}while(number_of_instructions>0);
	int k ;
	std::cin>>k;
	std::cout<<"End of Program Execution!"<<endl;
}

