#include "configuration.h"

class BinaryOperations{
	bool overflow;
public:
	bool bit_addition(bool op1, bool op2, bool carry, bool& sum);
	bool unsigned_addition(Word op1, Word op2, Word& sum);
	unsigned int word_to_unsigned_int(Word bin);
	bool unsigned_decimal_to_word(unsigned int d, Word& bin);
	bool BinaryOperations::decimal_to_twos_compliment(int d, Word& bin);
	bool BinaryOperations::twos_compliment_addition(Word op1, Word op2, Word& sum);
	bool BinaryOperations::twos_compliment_negation(Word& bin);
	int BinaryOperations::twos_compliment_to_decimal(Word bin);
};