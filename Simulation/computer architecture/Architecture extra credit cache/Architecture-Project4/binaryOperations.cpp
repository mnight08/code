#include "binaryOperations.h"

/* calcuate the binary bit addition sum=op1+op2+carry with carry bit c returned
 */
bool BinaryOperations::bit_addition(bool op1, bool op2, bool carry, bool& sum){
	bool c;
	switch(carry){
		case false:
			if(op1){
				c=op2;
				sum=!op2;
			}else{
				c=false;
				sum=op2;
			}
			break;
		default:
			if(op1){
				c=true;
				sum=op2;
			}else{
				c=op2;
				sum=!op2;
			}
			break;
	}
	return c;
}

/* calculate the binary addition sum=op1+op2 with carry bit returned
 */
bool BinaryOperations::unsigned_addition(Word op1, Word op2, Word& sum){
	int i;
	bool carry=false;
	bool s=false;
	bool& rs=s;
	overflow=false;
	for(i=0;i<WORD_WIDTH;i++){
		carry=BinaryOperations::bit_addition(op1[i],op2[i],carry,rs);
		sum[i]=rs;
	}
	overflow=carry;

	return carry;
}

/* convert a binary word to an unsigned integer and return the integer
 */
unsigned int BinaryOperations::word_to_unsigned_int(Word bin){
	int i;
	unsigned int u=0, weight=1;
	for(i=0;i<WORD_WIDTH;i++){
		if(bin[i])
			u+=weight;
		weight=weight<<1;
	}
	return u;
}

/* convert an unsigned integer to a binary word(unsigned representation)
   return FALSE if the integer is out of range
   return TRUE otherwise
 */
bool BinaryOperations::unsigned_decimal_to_word(unsigned int d, Word& bin){
	Word w;
	int i=0;

	w.set(); //all bits are one.
	bin.reset();
	if(d>w.to_ulong()){
		//cout<<"the integer is too big"<<endl;
		return false;
	}
	do{
		bin[i++]=d%2;  //remainder of mod 2
		d=d/2;  //integral quotient of divided by 2
	}while(d);
	return true;
}

/* convert an integer to a binary word(2's complement representation)
   return FALSE if the number is out of range
   return TRUE otherwise
 */
bool BinaryOperations::decimal_to_twos_compliment(int d, Word& bin){
	bool b;

	if(d<0){
		b=BinaryOperations::unsigned_decimal_to_word(d+(1<<(WORD_WIDTH-1)), bin);
		if(bin[WORD_WIDTH-1])
			b=false;	//MSB is 1, out of range for non-negative integers
		bin[WORD_WIDTH-1]=1;
	}else{
		b=BinaryOperations::unsigned_decimal_to_word(d, bin);
		if(bin[WORD_WIDTH-1])
			b=false;	//MSB is 1, out of range for non-negative integers
	}
	return b;
}

/* calculate sum=op1+op2, all have 2's complement representation
 */
bool BinaryOperations::twos_compliment_addition(Word op1, Word op2, Word& sum){
	int i;
	bool s=false,carry=false;
	bool& rs=s;
	
	for(i=0;i<WORD_WIDTH;i++){
		carry=BinaryOperations::bit_addition(op1[i],op2[i],carry,rs);
		sum[i]=rs;
	}
	return true;
}

/* find the negation of a binary word 
 */
bool BinaryOperations::twos_compliment_negation(Word& bin){
	bool msb=bin[WORD_WIDTH-1];

	bin.flip();
	BinaryOperations::twos_compliment_addition(bin,ONE_W,bin);
	/* the smallest negative integer has no negation */
	if(msb && bin[WORD_WIDTH-1])
		return false;
	else
		return true;
}

/* convert a binary word(2's complement representation) to an integer and return the integer
 */
int BinaryOperations::twos_compliment_to_decimal(Word bin){
	if(!bin[WORD_WIDTH-1]){
		/* positive number */
		return static_cast<int>(BinaryOperations::word_to_unsigned_int(bin));
	}else{
		/* negative number */
		bin[WORD_WIDTH-1]=0;   //reset the sign bit to be 0
		int u=BinaryOperations::word_to_unsigned_int(bin); //get the partial sum without the weight of the sign bit
		bin.reset();
		bin[WORD_WIDTH-1]=1;     
		u-=bin.to_ulong();         //subtract the weight of the sign bit
		return u;
	}
}

/*void BinaryOperations::copy_word(Word in, Word& out){
	int i;
	for(i=0;i<WORD_WIDTH;i++){
		out[i]=in[i];
	}
}*/

void BinaryOperations::and(Word op1, Word op2, Word& result){
	int i;
	for(i=0;i<WORD_WIDTH;i++){
		result[i]=op1[i]&op2[i];
	}
}

void BinaryOperations::or(Word op1, Word op2, Word& result){
	int i;
	for(i=0;i<WORD_WIDTH;i++){
		result[i]=(op1[i]|op2[i]);
	}
}

bool BinaryOperations::subtraction(Word op1, Word op2, Word& result){
	if(!twos_compliment_negation(op2))
		return false;	//return false if op2 is out of range after negation
	return twos_compliment_addition(op1,op2,result);
}

bool BinaryOperations::set_less_than(Word op1, Word op2){
	int i;
	if(op1[WORD_WIDTH-1]){
		/* op1 is negative */
		if(op2[WORD_WIDTH-1]){
			/* op1<0 and op2 <0 */
			for(i=WORD_WIDTH-2;i>=0;i--){
				if(op1[i]<op2[i])
					return true;
				else if(op1[i]>op2[i])
					return false;
			}
			return false;
		}else{
			/* op1<0 and op2>=0 */
			return true;
		}
	}else{
		/* op1 is non-negative */
		if(op2[WORD_WIDTH-1]){
			/* op1>=0 and op2<0 */
			return false;
		}else{
			/* op1>=0 and op2>=0 */
			for(i=WORD_WIDTH-2;i>=0;i--){
				if(op1[i]<op2[i])
					return true;
				else if(op1[i]>op2[i])
					return false;
			}
			return false;
		}
	}
}

bool BinaryOperations::shit_left_logical(Word op1, int d, Word& op2){
	int i;
	if(d>WORD_WIDTH)
		return false;
	op2.reset();
	for(i=d;i<WORD_WIDTH;i++){
		op2[i]=op1[i-d];
	}
	return true;
}