#include "assembler.h"

#define INSTRUCTION_LENGTH 32
#define OP_CODE_START_BIT 0
#define OP_CODE_BIT_NUMBER 6
#define RS_START_BIT 6
#define RS_END_BIT 10
#define RT_START_BIT 11
#define RT_END_BIT 15
#define RD_START_BIT 16
#define RD_END_BIT 20
#define SHAMT_START_BIT 21
#define SHAMT_BIT_NUMBER 5
#define FUNC_START_BIT 26
#define FUNC_BIT_NUMBER 6
#define IMMEDIATE_START_BIT 16
#define IMMEDIATE_BIT_NUMBER 16
#define ADDRESS_START_BIT 6
#define ADDRESS_BIT_NUMBER 26
#define REGISTER_BIT_NUMBER 5
#define OP_CODE_R_FORMAT "000000"
#define FUNC_CODE_ADD "100000"
#define FUNC_CODE_SLT "101010"
#define FUNC_CODE_SLL "000000"
#define FUNC_CODE_JR "001000"
#define OP_CODE_BEQ "000100"
#define OP_CODE_BNE "000101"
#define OP_CODE_LW "100011"
#define OP_CODE_SW "101011"
#define OP_CODE_ADDI "001000"
#define OP_CODE_SLTI "001010"
#define OP_CODE_JAL "000011"
#define OP_CODE_J "000010"

/* translate assembly instruction to machine instruction 
   Sample Input: add $s2 $a0 $zero
   Sample Output: 00000000100000001001000000100000
*/
void Assembler::translate_instruction(string instr, string& machine_instr)
{
	string operation,rs,rt,rd;
	istringstream instream;
	int immediate_number,target_address,shamt;
	string str;

	/* change string instr to a istringstream, which can be used to extract different fields in str */
	instream.clear();
	instream.str(instr);
	instream>>operation; // extract operation field.

	/* translate R-format instructions here */
	if(operation=="add" || operation=="slt")
	{
		machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_R_FORMAT); //replace the bits for opcode
		instream>>rd>>rs>>rt; //extract three operands(registers).		
		/* replace the bits of register rs */
		machine_instr.replace(RS_START_BIT,REGISTER_BIT_NUMBER,get_register_bits_by_name(rs));
		/* replace the bits of register rt */
		machine_instr.replace(RT_START_BIT,REGISTER_BIT_NUMBER,get_register_bits_by_name(rt));
		/* replace the bits of register rd */
		machine_instr.replace(RD_START_BIT,REGISTER_BIT_NUMBER,get_register_bits_by_name(rd));
		/* replace the bits of func field for addition */
		if(operation=="add")
			machine_instr.replace(FUNC_START_BIT,FUNC_BIT_NUMBER,FUNC_CODE_ADD); //replace the bits of funcion_code for add
		else if(operation=="slt")
			machine_instr.replace(FUNC_START_BIT,FUNC_BIT_NUMBER,FUNC_CODE_SLT); //replace the bits for funcion_code for slt
	}else if(operation=="sll")
	{
		machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_R_FORMAT); //replace the bits for opcode
		instream>>rd>>rt>>shamt; //extract three operands(registers).		
		/* replace the bits of shamt */
		str=twos_complement_for_integer(shamt,SHAMT_BIT_NUMBER);
		machine_instr.replace(SHAMT_START_BIT,SHAMT_BIT_NUMBER,str);
		/* replace the bits of register rt */
		machine_instr.replace(RT_START_BIT,REGISTER_BIT_NUMBER,get_register_bits_by_name(rt));
		/* replace the bits of register rd */
		machine_instr.replace(RD_START_BIT,REGISTER_BIT_NUMBER,get_register_bits_by_name(rd));
	}else if(operation=="jr")
	{
		machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_R_FORMAT); //replace the bits for opcode
		instream>>rs;
		machine_instr.replace(FUNC_START_BIT,FUNC_BIT_NUMBER,FUNC_CODE_JR); //replace the bits for funcion_code for jr
		/* replace the bits of register rs */
		machine_instr.replace(RS_START_BIT,REGISTER_BIT_NUMBER,get_register_bits_by_name(rs));
	}
	/* translate I-format instructions here */
	if(operation=="addi" ||   operation=="slti")
	{
		instream>>rt>>rs>>immediate_number;
		/* replace the bits of register rs */
		machine_instr.replace(RS_START_BIT,REGISTER_BIT_NUMBER,get_register_bits_by_name(rs));
		/* replace the bits of register rt */
		machine_instr.replace(RT_START_BIT,REGISTER_BIT_NUMBER,get_register_bits_by_name(rt));
		/* replace the bits of immediate number */
		machine_instr.replace(IMMEDIATE_START_BIT,IMMEDIATE_BIT_NUMBER,twos_complement_for_integer(immediate_number,IMMEDIATE_BIT_NUMBER));
		if(operation=="addi")
			machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_ADDI); //replace the bits for opcode addi
		else if(operation=="slti")
			machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_SLTI); //replace the bits for opcode slti
	}else if(operation=="beq" || operation=="bne"){
		instream>>rs>>rt>>immediate_number;
		/* replace the bits of register rs */
		machine_instr.replace(RS_START_BIT,REGISTER_BIT_NUMBER,get_register_bits_by_name(rs));
		/* replace the bits of register rt */
		machine_instr.replace(RT_START_BIT,REGISTER_BIT_NUMBER,get_register_bits_by_name(rt));
		/* replace the bits of immediate number */
		machine_instr.replace(IMMEDIATE_START_BIT,IMMEDIATE_BIT_NUMBER,twos_complement_for_integer(immediate_number,IMMEDIATE_BIT_NUMBER));
		if(operation=="beq")
			machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_BEQ); //replace the bits for opcode beq
		else if(operation=="bne")
			machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_BNE); //replace the bits for opcode bne
	}
	else if(operation=="sw" || operation=="lw")
	{
		if(operation=="sw")
			machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_SW); //replace the bits for opcode sw
		else if(operation=="lw")
			machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_LW); //replace the bits for opcode lw
		instream>>rt>>immediate_number>>rs;
		/* replace the bits of register rs */
		machine_instr.replace(RS_START_BIT,REGISTER_BIT_NUMBER,get_register_bits_by_name(rs.substr(1,rs.size()-2)));// use -2 to remove paranthesis
		/* replace the bits of register rt */
		machine_instr.replace(RT_START_BIT,REGISTER_BIT_NUMBER,get_register_bits_by_name(rt)); 
		/* replace the bits of immediate number */
		machine_instr.replace(IMMEDIATE_START_BIT,IMMEDIATE_BIT_NUMBER,twos_complement_for_integer(immediate_number,IMMEDIATE_BIT_NUMBER));
	}
	/* translate J-format instruction here */
	if(operation=="jal" || operation=="j")
	{
		if(operation=="jal")
			machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_JAL); //replace the bits for opcode jal
		else if(operation=="j")
			machine_instr.replace(OP_CODE_START_BIT,OP_CODE_BIT_NUMBER,OP_CODE_J); //replace the bits for opcode j
		instream>>target_address;
		/* replace the bits of immediate number */
		machine_instr.replace(ADDRESS_START_BIT,ADDRESS_BIT_NUMBER,twos_complement_for_integer(target_address,ADDRESS_BIT_NUMBER));
	}
}

/* get the 2's complement representation (bit_length bits) for an integer
   Sample Input: -5, 16
   Sample Output: 1111111111111010
*/
string Assembler::twos_complement_for_integer(int num, int bit_length)
{
	int max_pos=(1<<(bit_length-1))-1;  //maximum positive integer for IMMEDIATE_BIT_NUMBER bits
	string bits;
	int i;

	bits.resize(bit_length);
	if(num>max_pos||num<-(max_pos+1))
	{
		cout<<"ERROR! Integer "<<num<<" is too big for "<<bit_length<<" bits!"<<endl;
		exit(0);
	}
	if(num==-(max_pos+1))
	{
		bits=string(bit_length,'0');
		bits.replace(0,1,"1");
	}else
	{
		int temp=abs(num);
		/* get the binary reprsentation of number num */
		for(i=0;i<bit_length;i++)
		{
			if(temp%2)
				bits.replace(bit_length-i-1,1,"1");
			else
				bits.replace(bit_length-i-1,1,"0");
			temp>>=1;
		}
		if(num<0)
		{
			for(i=0;i<bit_length;i++)
				bits.replace(i,1,(bits[i]=='0')?"1":"0");
			i=bit_length-1;
			while(bits[i]=='1') i--; //find the rightmost position which is '0'
			bits.replace(i,1,"1"); //set this bit to 1
			if(i<bit_length-1)
				bits.replace(i+1,bit_length-i-1,string(bit_length-i-1,'0')); //replace the bits to the right with all zeros
		}
	}
	return bits;
}


/* get the integer number from the register name 
   Sample Input: t8
   Sample Output: 11000
*/
string Assembler::get_register_bits_by_name(string register_name)
{
	string register_bits;
	if(register_name=="$zero")
		register_bits="00000";
	else if(register_name=="$at")
		register_bits="00001";
	else if(register_name=="$v0")
		register_bits="00010";
	else if(register_name=="$v1")
		register_bits="00011";
	else if(register_name=="$a0")
		register_bits="00100";
	else if(register_name=="$a1")
		register_bits="00101";
	else if(register_name=="$a2")
		register_bits="00110";
	else if(register_name=="$a3")
		register_bits="00111";
	else if(register_name=="$t0")
		register_bits="01000";
	else if(register_name=="$t1")
		register_bits="01001";
	else if(register_name=="$t2")
		register_bits="01010";
	else if(register_name=="$t3")
		register_bits="01011";
	else if(register_name=="$t4")
		register_bits="01100";
	else if(register_name=="$t5")
		register_bits="01101";
	else if(register_name=="$t6")
		register_bits="01110";
	else if(register_name=="$t7")
		register_bits="01111";
	else if(register_name=="$s0")
		register_bits="10000";
	else if(register_name=="$s1")
		register_bits="10001";
	else if(register_name=="$s2")
		register_bits="10010";
	else if(register_name=="$s3")
		register_bits="10011";
	else if(register_name=="$s4")
		register_bits="10100";
	else if(register_name=="$s5")
		register_bits="10101";
	else if(register_name=="$s6")
		register_bits="10110";
	else if(register_name=="$s7")
		register_bits="10111";
	else if(register_name=="$t8")
		register_bits="11000";
	else if(register_name=="$t9")
		register_bits="11001";
	else if(register_name=="$k0")
		register_bits="11010";
	else if(register_name=="$k1")
		register_bits="11011";
	else if(register_name=="$gp")
		register_bits="11100";
	else if(register_name=="$sp")
		register_bits="11101";
	else if(register_name=="$fp")
		register_bits="11110";
	else if(register_name=="$ra")
		register_bits="11111";
	else
	{
		cout<<"ERROR! Incorrect Register Name!"<<endl;
		exit(0);
	}
	return register_bits;
}

/* convert a string to an integer.
   Sample Input: 53(string).
   Sample Output: 53(integer).
*/
int Assembler::convert_string_to_int(const string& s)
{
     istringstream stream (s);
     int t;
     stream >> t;
     return t;
}
/* convert an integer to a string.
   Sample Input: 53(integer).
   Sample Output: 53(string).
*/
string Assembler::convert_int_to_string(const int& i)
{
	stringstream ss;
	ss<<i;
	return ss.str();
}