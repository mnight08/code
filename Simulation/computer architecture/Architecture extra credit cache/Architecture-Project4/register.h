#include "configuration.h"

class Register{
	RegisterNumber rs;	//source register 1
	RegisterNumber rt;	//source register 2
	RegisterNumber rd;	//destination register
	Word writeData;		//data written to the destination register
	Word out1;			//output value 1
	Word out2;			//output value 2
	bool regWrite;		//write control: 1-- write, 0--read
	Word registers[NUMBER_OF_REGISTERS];			//registers
	int registerNumber_to_int(RegisterNumber reg);	//convert register number(bit set) to an integer
public:
	Register();
	void set_input_rs(RegisterNumber rs);						//set register number(bit set) for source register 1
	void set_input_rt(RegisterNumber rt);						//set register number(bit set) for source register 2
	void set_input_rd(RegisterNumber rd);						//set register number(bit set) for destination register
	void set_input_read(RegisterNumber rs, RegisterNumber rd);	//set register number(bit set) for two source registers
	void set_input_data(Word data);								//set input data)(bit set) to write
	void set_write_control(bool write);							//set control signal: 1--write, 0--read
	void execute();												//if control signla is to write(1), write the data into the destination register. Otherwise, read the two output values for the two input registers.
	void get_output_rs(Word& rs);								//get the output value into rs
	void get_output_rt(Word& rt);								//get the output value into rt
	void read_pc(Word& p);                                      //read pc register
};