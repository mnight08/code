#include "configuration.h"

class ALU{
	Word input_value1;
	Word input_value2;
	Word output_value;
	bool zero;
	AluControl control;
public:
	void set_input_value1(Word w);
	void set_input_value2(Word w);
	void set_control(AluControl c);
	void execute();
	void get_output_data(Word& data, bool& zero);
};