#include "configuration.h"

class Memory
{
	//this is in here to define the cache in mem as an array of cache blocks.
	class Cacheblock
	{
		bool valid;
		Word tag;
		Word data[BLOCK_SIZE];
		int lastuse;
	public:
		Cacheblock()
		{
			valid = false;
			//data=0;
			tag=0;
			lastuse=0;
		}
		void set_data(Word d,int offset)
		{
			data[offset]=d;
		}
		void set_tag(Word t)
		{
			tag = t;
		}
		void set_valid()
		{
			valid = true;
		}
		bool isValid()
		{
			return valid;
		}
		Word get_data(int offset)
		{
			return data[offset];
		}
		Word get_tag()
		{
			return tag;
		}
		void set_last_use(int time)
		{
			lastuse=time;
		}
		int get_last_use()
		{
			return lastuse;
		}
	};//dont forget semicolons

	Word mem[NUMBER_OF_WORDS];
	//cache
	Cacheblock cache[CACHE_SIZE];
	//cache
	Word input_address;
	Word output_data;
	bool mem_read;
	bool mem_write;
	Word input_data;
	int number_of_data;
	int starting_address_of_data;
	// cache ways-way associative
	int ways;
	int tagbitssize;
	int offsetbitssize;
	int setbitssize;
	int set;
	int offset;
	int time;
	int setsize;
	bool hit;
		
public:
	Memory();
	void set_input_address(Word address);
	void set_input_data(Word data);
	void set_mem_read(bool read);
	void set_mem_write(bool write);
	void execute();
	void get_output_data(Word& data);
	void Memory::display_data();
	void flush_cache();
};
