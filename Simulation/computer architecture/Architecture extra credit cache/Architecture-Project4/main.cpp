#include "configuration.h"
#include "binaryOperations.h"
#include "assembler.h"
#include "register.h"
#include "memory.h"
#include "ALU.h"
#include "control.h"
#include "ALUcontrol.h"

Word ONE_W,ZERO_W,FOUR_W,PROGRAM_EXIT; //PROGRAM_EXIT is the address to terminate the execution of program
ofstream outFile;

void simple_CPU();
void test_ALUcontrol();
void test_control();
void test_ALU();
void test_memory();
void test_register();
int test_assembler();
int test_binaryOperations();

int main(){

	/* constant 0 */
	ZERO_W.reset();

	/* constant 1 */
	ONE_W.reset();
	ONE_W[0]=true;

	/*constant 4*/
	FOUR_W.reset();
	FOUR_W[2]=true;

	outFile.open("output.txt");
	simple_CPU();


	outFile.close();
	return 1;
}


void simple_CPU(){
	ALU adder4, adderBranch,alu;
	Memory mem;
	Register reg;
	Control control;
	ALUcontrol aluControl;
	Word pc,nextPC,branchAddr,jumpAddr,instr,aluResult,rsData,rtData,rdData,memoryOutData;
	RegisterNumber rs,rt,rd, ra;
	Word shiftAmount;
	BinaryOperations binOp;
	bool regDst,branch,branchNE,memRead,memToReg,ALUop0,ALUop1,memWrite,ALUsrc,regWrite,jump,jumpLink,jumpRegister,zero,aluZero,shift;
	AluControl aluCon;
	int i;
	ra.set(); //register $ra
	PROGRAM_EXIT.set();  //set the address to terminate execution of the program
	reg.read_pc(pc); // sets pc from register
	do{
		outFile<<"********now executing instruction at address: "<<binOp.word_to_unsigned_int(pc)<<"********"<<endl;;

		// from memory mem //
		// set controls
		mem.set_mem_read(true);
		mem.set_mem_write(false);
		// give memory the address of instruction
		mem.set_input_address(pc);
		// execute it
		mem.execute();
		// set instr to the instruction
		mem.get_output_data(instr);


		/* get pc<---pc+4 */

		// set up aluCon for addition
		aluCon[0] = false;
		aluCon[1] = true;

		adder4.set_control( aluCon );
		adder4.set_input_value1(pc);
		adder4.set_input_value2( FOUR_W );

		// use adder4 to do the addition
		adder4.execute();
		adder4.get_output_data(pc, aluZero);


		/* generate control signals */
		// pass the instruction to the control
		control.set_input_value(instr);
		control.execute();

		// begin setting the control signals
		regDst = control.get_output("regDst");
		branch = control.get_output("branch");
		branchNE = control.get_output("branchNE");
		memRead = control.get_output("memRead");
		memToReg = control.get_output("memToReg");
		ALUop0 = control.get_output("ALUop0");
		ALUop1 = control.get_output("ALUop1");
		memWrite = control.get_output("memWrite");
		ALUsrc = control.get_output("ALUsrc");
		regWrite = control.get_output("regWrite");
		jump = control.get_output("jump");
		jumpLink = control.get_output("jumpLink");
		jumpRegister = control.get_output("jumpRegister");
		shift = control.get_output("shift");
		// end setting control signals


		/* decoding get rs, rt, rd, and shiftAmount */
		// set rd from instruction
		rd.set(0, instr[11]);
		rd.set(1, instr[12]);
		rd.set(2, instr[13]);
		rd.set(3, instr[14]);
		rd.set(4, instr[15]);
		// set rt from instruction
		rt.set(0, instr[16]);
		rt.set(1, instr[17]);
		rt.set(2, instr[18]);
		rt.set(3, instr[19]);
		rt.set(4, instr[20]);
		// set rs from instruction
		rs.set(0, instr[21]);
		rs.set(1, instr[22]);
		rs.set(2, instr[23]);
		rs.set(3, instr[24]);
		rs.set(4, instr[25]);

		// initialize shiftAmount to zero
		shiftAmount = ZERO_W;

		// load shiftAmount
		shiftAmount.set(0, instr[6]);
		shiftAmount.set(1, instr[7]);
		shiftAmount.set(2, instr[8]);
		shiftAmount.set(3, instr[9]);
		shiftAmount.set(4, instr[10]);

		// setting branch address
		for( int i = 0; i<16; i++ )
		{
			branchAddr[i] = instr[i];
		}
		for( int i = 16; i<32; i++ )
		{ // set the rest of branch address by sign extension
			branchAddr[i] = branchAddr[15];
		}

		// set jump address
		for(int j=0; j<26; j++)
		{
			jumpAddr[j] = instr[j];
		}
		// multiply by 4 by shifting left
		binOp.shit_left_logical(jumpAddr, 2, jumpAddr);
		// first four bits are from pc+4
		jumpAddr[28] = pc[28];
		jumpAddr[29] = pc[29];
		jumpAddr[30] = pc[30];
		jumpAddr[31] = pc[31];

		/* read registers */
		reg.set_input_rs( rs );
		// set output
		if( !regDst )
			reg.set_input_rd( rt );
		else
		{
			// if jumplinking set rd to register 31(RA) for writing at end of loop
			if(jumpLink)
				for(int k=0; k<5; k++)
					rd[k] = 1;
			reg.set_input_rd( rd );
		}
		//
		reg.set_input_rt( rt );
		reg.set_write_control( false );
		reg.execute(); // register execution
		reg.get_output_rs( rsData );
		reg.get_output_rt( rtData );

		if( jumpRegister )
		{
			jumpAddr = rsData;
		}

		/* get control signals for ALU */
		aluControl.set_input_value( instr );
		aluControl.set_ALUop0( ALUop0 );
		aluControl.set_ALUop1( ALUop1 );

		aluControl.execute(); // ALU control execution
		aluControl.get_output( aluCon );


		/* execute instruction */

		if( ALUsrc )
		{
			alu.set_input_value1( rsData );
			alu.set_input_value2( branchAddr );
		}
		else if( shift )
		{
			alu.set_input_value1( rtData );
			alu.set_input_value2( shiftAmount );
		}
		else
		{
			alu.set_input_value1( rsData );
			alu.set_input_value2( rtData );
		}

		alu.set_control( aluCon );
		alu.execute();
		alu.get_output_data( aluResult, aluZero );


		/* calcuate the branch address */
		// branch address was set above
		// shift left by 2
		binOp.shit_left_logical( branchAddr, 2, branchAddr );


		/* add pc+4 with shifted branchAddr */
		adderBranch.set_input_value1( pc );
		adderBranch.set_input_value2( branchAddr );
		// set aluCon for addition
		aluCon[0] = false;
		aluCon[1] = true;
		aluCon[2] = false;
		aluCon[3] = false;
		adderBranch.set_control(aluCon);
		adderBranch.execute();
		adderBranch.get_output_data( branchAddr, zero );

		//TODO:  Fix bne instruction.  it is broken right now.
		/* calcuate nextPC after considering bne, beq, and jump instructions */
		if( (branchNE && !aluZero) || (branch && aluZero) )
			nextPC = branchAddr;
		else if( jump )
		{
			nextPC = jumpAddr;
		}
		else if( jumpRegister )
		{ // set next pc to jump register
			nextPC = jumpAddr;
		}
		else
		{
			nextPC = pc;
		}


		/* read/write memory if necessary */
		mem.set_input_data( rtData );
		mem.set_mem_read( memRead );
		mem.set_mem_write( memWrite );
		mem.set_input_address( aluResult );

		mem.execute();

		mem.get_output_data( memoryOutData );


		/* write register if necessary */
		reg.set_write_control( regWrite );
		if( memToReg )
			reg.set_input_data( memoryOutData );
		else
		{
			// if jumplinking need to set register to pc
			if(jumpLink)
				reg.set_input_data( pc );
			else
				reg.set_input_data( aluResult );
		}

		reg.execute();



		/* set pc */
		pc=nextPC;

		outFile<<endl;
	}while(pc!=PROGRAM_EXIT);
	mem.flush_cache();
	mem.display_data();
	outFile<<"End of Program Execution!"<<endl;
}

