#include "ALU.h"
#include "binaryOperations.h"

void ALU::set_input_value1(Word w){
	input_value1=w;
}

void ALU::set_input_value2(Word w){
	input_value2=w;
}

void ALU::set_control(AluControl c){
	control=c;
}

void ALU::execute(){
	BinaryOperations binOp;
	Word w;

	outFile<<"/////ALU Execution/////"<<endl;
	if(control[3] && control[2] && control[1] && control[0]){
		/* for jr */
		output_value=input_value1;
		outFile<<"(ALU Execution) operations---for jr"<<endl;
		return;   
	}
	if(control[2]){
		if(control[0]){
			/* operation: set on less than */
			outFile<<"(ALU Execution) operations---slt"<<endl;
			zero=binOp.set_less_than(input_value1,input_value2);
			if(zero)
				output_value=ONE_W;	//output value is 1 is input_value1<input_value2
			else
				output_value=ZERO_W;
		}else{
			/* operation: subtract */
			outFile<<"(ALU Execution) operations---subtraction"<<endl;
			binOp.subtraction(input_value1,input_value2,output_value);
			if(output_value==ZERO_W)
				zero=true;
			else
				zero=false;
		}
	}else{
		if(control[1]){
			if(control[0]){
				/* operation: sll */
				outFile<<"(ALU Execution) operations---sll"<<endl;
				binOp.shit_left_logical(input_value1,binOp.word_to_unsigned_int(input_value2),output_value);
			}else{
				/* operation: add */
				outFile<<"(ALU Execution) operations---addition"<<endl;
				binOp.twos_compliment_addition(input_value1,input_value2,output_value);
			}
		}else{
			if(control[0]){
				/* operation: or */
				outFile<<"(ALU Execution) operations---or"<<endl;
				binOp.or(input_value1,input_value2,output_value);
			}else{
				/* operation: and */
				outFile<<"(ALU Execution) operations---and"<<endl;
				binOp.and(input_value1,input_value2,output_value);
			}
		}
	}
}


void ALU::get_output_data(Word& data, bool& z){
	data=output_value;
	z=zero;
}
