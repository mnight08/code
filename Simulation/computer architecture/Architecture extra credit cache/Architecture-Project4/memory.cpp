#include <math.h>
#include "memory.h"
#include "binaryOperations.h"
using namespace std;

/* constructor which initialize memory from memory_ini.txt */
Memory::Memory(){
	Word w=ZERO_W;
	BinaryOperations binOp;
	ifstream inFile;
	int i,d,starting_address,number_of_instructions;
	
	//cache stuff
	tagbitssize=32;
	offsetbitssize = log((double)BLOCK_SIZE)/log((double)2);
	setbitssize = log((double)CACHE_SIZE/WAYS)/log(2.0);
	set=0;
	offset=0;
	ways = WAYS;
	time=0;
	//number of words in a set
	setsize=WAYS*BLOCK_SIZE;
	hit=false;
		
	//cache stuff


	inFile.open("memory_ini.txt");

	inFile>>starting_address; // get the starting address of instructions
	inFile>>number_of_instructions; //get the number of instructions

	mem_read=1;		//read by default
	mem_write=0;
	/* load instructions */
	for(i=0;i<number_of_instructions;i++){
		inFile>>mem[4*i+starting_address];
	}
	/* load data */
	inFile>>starting_address_of_data; // get the starting address of data
	inFile>>number_of_data;	  // get the number of data
	/* load data */
	for(i=0;i<number_of_data;i++){
		inFile>>d;
		binOp.decimal_to_twos_compliment(d,w);
		mem[4*i+starting_address_of_data]=d;
	}

}

void Memory::set_input_address(Word address){
	input_address=address;
}

void Memory::set_input_data(Word data){
	input_data=data;
}

void Memory::set_mem_read(bool read){
	mem_read=read;
}

void Memory::set_mem_write(bool write){
	mem_write=write;
}

void Memory::execute()
{
	outFile<<"/////Memory Execution/////";

	BinaryOperations binOp;
	
	time+=1;
	int hitblock=0;
	int oldestblock=0;
	Word temp;
	offset=0;

	//get offset and set
	//get offset
	temp=ZERO_W;
	for(int i=0;i<offsetbitssize;i++)
	{
		temp[i]=input_address[i];
	}
	offset=binOp.word_to_unsigned_int(temp);
	//get set
	temp=ZERO_W;
	for(int i=0;i<setbitssize;i++)
	{
		temp[i]=input_address[offsetbitssize+i];
	}
	set=binOp.word_to_unsigned_int(temp);
	
	// check if in cache and find the oldest block in the set in case there is a miss
	//check each word in the set
	hit=false;
	for(int i=0;i<setsize;i++)
	{
		//find oldest element in the set in case it needs to be replaced
		if(cache[set*WAYS+i].get_last_use()<cache[set*WAYS+oldestblock].get_last_use())
		{
			oldestblock=i;
		}
		//found block in cache with value
		if(binOp.word_to_unsigned_int(cache[set*WAYS+i].get_tag())/BLOCK_SIZE==binOp.word_to_unsigned_int(input_address)/BLOCK_SIZE&&cache[set*WAYS+i].isValid())
		{
			hitblock=i;
			hit=true;
		}
	}

	if(hit)
		outFile<<"hit"<<endl;
	else outFile<<"miss"<<endl;
	if(!mem_read&&!mem_write)
		return;
	//handle reads first
	if(mem_read)
	{
		//read first
		//make sure cache has data that is needed
		//if found data in cache, then good job, we are done.
		// if not get from memory, and cache it
		//replace old block in cache
		if(!hit)
		{
			hitblock=oldestblock;
			//if write back mode, then replace the proper block in memory, and replace the cache block
			if(WRITE_BACK&&cache[set*WAYS+hitblock].isValid())
			{
				//find block in given set to replace
				//this is done no matter what above when checking if it is a cache hit
				//update memory with cache block
				//get old block from cache.

				//load old block into memory
				for( int i=0;i<BLOCK_SIZE;i++)
				{	
					//I think this is correct
					mem[((binOp.word_to_unsigned_int(cache[set*WAYS+hitblock].get_tag())/BLOCK_SIZE)<<offsetbitssize)+i]=cache[set*WAYS+hitblock].get_data(i);
				}
			}

			//replace cache block
			//pull new block from memory
			//load new block into cache
			for( int i=0;i<BLOCK_SIZE;i++)
			{	
				cache[set*WAYS+hitblock].set_data(mem[((binOp.word_to_unsigned_int(input_address)/BLOCK_SIZE)<<offsetbitssize)+i],i);
			}
			cache[set*WAYS+hitblock].set_tag(input_address);
		}
		//pull item from cache.
		//this happens whether there is a hit or not.  If there is a hit, then the item is in cache, so
		//the output is just the item in cache.  if not, then the spot it will replace will be the offset
		//of the oldest item in cache.
		output_data=cache[set*WAYS+hitblock].get_data(offset);
	}

	if(mem_write)
	{

		if(hit)
		{
			//cache
			//handle cache writes
			//if the cache is set to write back use writeback method.  write to cache and write back latter
			//if writeback and hit, then simply write to the  cache
			cache[set*WAYS+hitblock].set_data(input_data,offset);
			cache[set*WAYS+hitblock].set_last_use(time);
			//otherwise write through to memory immediatly after writing to cache
			//if cache hit, and write through, then write to cache, and memory.
			if(!WRITE_BACK)
			{				
				mem[((binOp.word_to_unsigned_int(input_address)/BLOCK_SIZE)<<offsetbitssize)+offset]=cache[set*WAYS+hitblock].get_data(offset);
			}
			//cache
		}
		else if(!hit)
		{

			//if block isnt in cache, then pull into cache first.  this means if we are doing writeback, that we need to replace the block in memory before hand
			hitblock=oldestblock;
			//if doing writeback mode, then we need to replace the block in cache back into memory
			if(WRITE_BACK&&cache[set*WAYS+hitblock].isValid())
			{
				//load old block into memory
				for( int i=0;i<BLOCK_SIZE;i++)
				{	
					//I think this is correct
					mem[((binOp.word_to_unsigned_int(cache[set*WAYS+hitblock].get_tag())/BLOCK_SIZE)<<offsetbitssize)+i]=cache[set*WAYS+hitblock].get_data(i);
				}

			}
			//fetch new block from memory into cache.
			for( int i=0;i<BLOCK_SIZE;i++)
			{	
				cache[set*WAYS+hitblock].set_data(mem[((binOp.word_to_unsigned_int(input_address)/BLOCK_SIZE)<<offsetbitssize)+i],i);
			}


			//write to cache
			cache[set*WAYS+hitblock].set_data(input_data,offset);
			cache[set*WAYS+hitblock].set_tag(input_address);

			//if write through just write through to memory
			if(!WRITE_BACK)
			{
				//write through to memory
				mem[((binOp.word_to_unsigned_int(input_address)/BLOCK_SIZE)<<offsetbitssize)+offset]=cache[set*WAYS+hitblock].get_data(offset);
			}

		}
		cache[set*WAYS+hitblock].set_last_use(time);
		cache[set*WAYS+hitblock].set_valid();
		outFile<<"(Memory Write) Address---"<<binOp.word_to_unsigned_int(input_address)<<":"<<binOp.twos_compliment_to_decimal(input_data)<<endl;
	}
}

void Memory::flush_cache()
{
	BinaryOperations binOp;
	for(int block=0;block<CACHE_SIZE;block++)
	{
		//load old block into memory
		for( int word=0;word<BLOCK_SIZE;word++)
		{	
			//I think this is correct
			mem[((binOp.word_to_unsigned_int(cache[block].get_tag())/BLOCK_SIZE)<<offsetbitssize)+word]=cache[block].get_data(word);
		}
	}

}

void Memory::get_output_data(Word& data){
	data=output_data;
	//cout<<"output_data:"<<data<<"\t data:"<<data<<endl;
}

void Memory::display_data(){
	BinaryOperations binOp;
	int i;
	outFile<<"-----------Now display data in memory-----------"<<endl;
	for(i=0;i<number_of_data;i++){
		outFile<<"address "<<starting_address_of_data+i*4<<":"<<binOp.twos_compliment_to_decimal(mem[i*4+starting_address_of_data])<<endl;
	}
}