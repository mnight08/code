function H = channelforfixedfrequency( transmitters,receivers,N_T,N_R,w )
    %define scatterers
    %number of scatterers
    K=10;
    %speed of light
    c_0=3*10^8;

    %row vector containing the positions of scatterers along the x1 axis
    positions=[-100, -60,-20,-50,-9,-5, 0,9,100,300];
    %reflectivities
    %sigma=[-1,-2, -3,-4,-5,-6,-7, -8,-9,-10];
    sigma=[-100,-100, -100,-100,-0,-0,-0, -100,-100,-100];

    
    H=zeros(N_R,N_T);
    

    j=1;
    while j<=N_R
        l=1;
        while l<=N_T
            entry=0;
            k=1;
            while k<=K
                entry=entry+exp(1i*w*abs(receivers(j)-positions(k))+abs(transmitters(l).position-positions(k))/c_0)*sigma(k)/(abs(receivers(j)-positions(k))*abs(transmitters(l).position-positions(k)));
                k=k+1;
            end
            H(j,l)=entry;
            l=l+1;
        end
        j=j+1;
    end
    H=H*(w^2)*(4*pi)^2;
    
end

