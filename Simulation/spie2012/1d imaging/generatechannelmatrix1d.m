function H=generatechannelmatrix1d(transmitters,receivers,N_T,N_R,N_S,start,stop)
    H=zeros(N_R,N_T*N_S);
    
    %go through every sample frequency
    s=1;
    while s<=N_S
        w=start+((stop-start)/N_S)*(s-1);
        H(:,((s-1)*N_T+ 1):((s-1)*(N_T)+N_T))=channelforfixedfrequency( transmitters,receivers,N_T,N_R,w );
        s=s+1;
    end
    
end