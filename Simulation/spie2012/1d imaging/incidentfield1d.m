function incident=incidentfield1d(transmiters,N_T,x,w,start,stop)
    c_0=3*10^8;
    l=1;
    incident=0;
    while l<=N_T
		incident=incident+transmiters(l).fouriersignal(w,start,stop)*exp(-1i*w*abs((x-transmiters(l).position)/c_0))/(4*pi*abs(x-transmiters(l).position));
		l=l+1;
    end
	incident=-1*incident;
end