function out=scalechannelmatrix(transmitters,receivers,N_T,N_R,H,w)
out=zeros(N_R,N_T);
j=1;
while j<=N_R
   l=1;
   while l<=N_T
       out(j,l)=H(j,l)*(4*pi)^2*norm(transmitters(l).position)*norm(receivers(j))*exp(-1i*w*(norm(transmitters(l).position+norm(receivers(j)))))/(w^2);
       l=l+1;
   end
    j=j+1;
end

end
