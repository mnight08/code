%simple linear signal.  it is zero at start, and stop at stop.  zero if t is outside of start<t<stop
function y= signal( t,start, stop )
    if( t<=stop) && (t>=start)
        y=t;
    else 
        y=0;
    end
end

