%graph the phase in the polar fourier recon
hold on;
N=1/.01+1
n=1;
while n<=N
x=1-1/n;
y=0:.01:1;
s=0:.01:1;
r=10;
fs=x.*cos(2*pi*s)+y.*sin(2*pi*s)-r;
plot3(fs,y,s);
n=n+1;
end
