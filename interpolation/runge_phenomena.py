# -*- coding: utf-8 -*-
"""
Created on Fri Apr 14 12:18:48 2017

@author: vpx365
"""
import matplotlib.pyplot as plt;
import scipy.special as sp;
def f(x):
    return 1.0/(4+x**2)
    

#return an interpolating function of Y on X.
#assume equally spaced X with unit spacing.
def interpolate(X,Y):
    n=len(X)
    print(n)
    def interpolant(t):
        interp=0;
        for k in range(0,n):
            dq=0;
            for x in X[0:k]: 
                dq=dq+(-1)**(x+k)*sp.binom(k,x)*f(x)
                print(k)
                print(x)
                print(dq)
            interp=interp+dq*sp.binom(t,k)   
        return interp
    return interpolant
#interpolate f(x) at -10,-9,..., 9, 10
X=[i for i in range(-10,11)];

Y=[f(x) for x in X]

plt.plot(X,Y, 'ro')


interpolant=interpolate(X[15:19],Y[15:19])
plt.plot(X[15:19],interpolant(X[15:19]))

#Construct interpolants taking different slices of data
#for j in range(-10,10):
 #   sliceObj=slice(j,len(X))
 #   interpolant=interpolate(X[sliceObj],Y[sliceObj])
 #   plt.plot(X,interpolant(X))