from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt
import numpy as np
import copy
import decimal

def nChoosej(n,j):
    product=1;
    i=1
    while i<j:
        product=product*(n+1-i)/i
        i=i+1
    return product
        

#Lemma 1
def zeta_m(m,k):
    sum=0.0
    for j in range(1,m+1):
        sum=sum+1/j**k
    return sum;
    
    
def binomial_alt_m(m,k):
    sum=decimal.Decimal(0)
    for j in range(1,m+1):
        try:
            sum=decimal.Decimal(nChoosej(m,j))*decimal.Decimal((-1)**(j-1))/decimal.Decimal(j)**decimal.Decimal(k)
        except OverflowError:
            print(m,k,j, sum)
    return sum;
    
    

#compare the two functions.
#print("Oh noooo, the conjectured lemma is wrong!");

decimal.getcontext().prec = 10000

N=200
fig1 = plt.figure()
ax = fig1.gca(projection='3d')
X = np.arange(2, N+2, 1)
Y = np.arange(2, N+2, 1)
X, Y = np.meshgrid(X, Y)

#initialize Z.
##Z=np.zeros((N,N))
##for m in range(2,N+2):
##    for k in range(2, N+2):
##        Z[m-2][k-2]=zeta_m(m,k)
##        #print(zeta_m(m,k))
##surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.coolwarm,
##        linewidth=0, antialiased=False)
##ax.set_zlim(.9, 3)
##
##ax.zaxis.set_major_locator(LinearLocator(10))
##ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
##
##fig1.colorbar(surf, shrink=0.5, aspect=5)
##
##plt.show()

fig2 = plt.figure()
ax = fig2.gca(projection='3d')
X = np.arange(2, N+2, 1)
Y = np.arange(2, N+2, 1)
X, Y = np.meshgrid(X, Y)

#initialize Z.
Z=np.zeros((N,N))
for m in range(2,N+2):
    for k in range(2, N+2):
        Z[m-2][k-2]=binomial_alt_m(m,k)
        #print(zeta_m(m,k))
surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.coolwarm,
        linewidth=0, antialiased=False)
ax.set_zlim(0, 1)

ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

fig2.colorbar(surf, shrink=0.5, aspect=5)

plt.show()
