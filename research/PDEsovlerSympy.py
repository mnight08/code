# -*- coding: utf-8 -*-
"""
Created on Wed Mar  8 13:06:10 2017

@author: vpx365
"""

from sympy import init_session
from sympy.solvers.pde import pdsolve
from sympy import Function, diff, Eq
from sympy.abc import x,y



init_session()
f=Function('f')
u=f(x,y)
uxx=u.diff(x,2)

uyy=u.diff(y,2)

eq=Eq(uyy)

pdsolve(eq)