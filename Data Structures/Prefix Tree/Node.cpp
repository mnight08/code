#include <iostream>

template <class type>
//this is a general Node class that has an array of node pointers.  kind of a 
//generalization of a binary tree.
class Node
{
	public:
		//string key;
		type *data;	//if data is NULL, then no item ends here
		Node<type> **children;
		int count;
		Node(int);
};

template<class type>
Node<type>::Node(int numchildren)
{
	data= NULL;count=0;
	children=new Node<type>*[numchildren];
	for(int i=0;i<numchildren;i++)
	{
		children[i]=NULL;
	}
}
