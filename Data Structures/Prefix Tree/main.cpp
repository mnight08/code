#include <iostream>
#include <string>
#include <fstream>
#include <time.h>

#include "tree.h"
using namespace std;

#include <set>
#include <algorithm>


int main()
{
	alphabet a;
	a.letters="0123456789abcdefghijklmnopqrstuvwxyz";
	tree<string> t(a);
	string key;

	key="10acde34";
	t.insert(key,key);

	key="cat";
	t.insert(key,key);
	
	key="3567";
	t.insert(key,key);
	
	key="87665";
	t.insert(key,key);

	t.list();
	return 0;
}
