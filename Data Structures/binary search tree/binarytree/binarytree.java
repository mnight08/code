package binarytree;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author afmartinez4
 */
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
public class binarytree {

    node head;

    public class node {

        int count;
        String word;
        node left;
        node right;

        public String toString() {
            return word + " " + count + "\n";
        }
    }
    public void buildtree()
    {
     try {

            String temp = this.getClass().getResource("story.txt").getFile().replaceAll("%20", " ");
           
            Scanner is = new Scanner(new FileReader(temp));//"/Users/mnight08/Desktop/java/NetBeansProjects/fucking with files/src/fuckingwithfiles/test.txt"));
            while(is.hasNext())
            this.insert(is.next());

        } catch (Exception e) {
            System.out.println("file not found");
        }
    }

    public void insert(String item) {
        head=recinsert(head, item);
    }

    public node recinsert(node tree, String tobeinserted) {
        if (tree == null) {
            tree = new node();
            tree.count = 1;
            tree.word = tobeinserted;
           return tree;
         
        } else if (tobeinserted.compareTo(tree.word) < 0) {
            tree.left= recinsert(tree.left, tobeinserted);
            return tree;
        } else if (tobeinserted.compareTo(tree.word) > 0) {
            tree.right= recinsert(tree.right, tobeinserted);
            return tree;
        } else {
            tree.count++;
            return null;
        }
    }

    public String toString() {
        if (head != null) {
            return toString(head.left, "") + " " + head + " " + toString(head.right, "");
        } else {
            return "empty";
        }
    }

    public String toString(node tree, String output) {
        if (tree == null) {
            if(output!="")
                output=output+"\n";
            return output ;

        } else {
            return toString(tree.left, output) + tree + toString(tree.right, output);
        }


    }
    public void writetofile()
    {

                   FileOutputStream out; // declare a file output object
                PrintStream p; // declare a print stream object

                try
                {
                        // Create a new file output stream
                        // connected to "myfile.txt"
                        out = new FileOutputStream("output.txt");

                        // Connect print stream to the output stream
                        p = new PrintStream( out );

                        p.print(toString());

                        p.close();
                }
                catch (Exception e)
                {
                        System.err.println ("Error writing to file");
                }
        
  }
}