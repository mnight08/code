/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author afmartinez4
 */
import java.io.*;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
public class binarytree {

    node head;

    public class node {

        int count;
        String word;
        node left;
        node right;

        public String toString() {
            return word + " " + count + "";
        }
    }
    public void buildtree()
    {
        try {
            head = null;
            FileInputStream in = new FileInputStream(getClass().getResource("story.txt").getFile());
           // File out = new File("output.txt");
            Scanner scan = new Scanner(in);
            while(scan.hasNext())
            {
              insert(scan.next());
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(binarytree.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void insert(String item) {
        head=recinsert(head, item);
    }

    public node recinsert(node tree, String tobeinserted) {
        if (tree == null) {
            tree = new node();
            tree.count = 1;
            tree.word = tobeinserted;
           return tree;
         
        } else if (tobeinserted.compareTo(tree.word) < 0) {
            tree.left= recinsert(tree.left, tobeinserted);
            return tree;
        } else if (tobeinserted.compareTo(tree.word) > 0) {
            tree.right= recinsert(tree.right, tobeinserted);
            return tree;
        } else {
            tree.count++;
            return null;
        }
    }

    public String toString() {
        if (head != null) {
            return toString(head.left, "") + " " + head + " " + toString(head.right, "");
        } else {
            return "empty";
        }
    }

    public String toString(node tree, String output) {
        if (tree == null) {
            return output;

        } else {
            return toString(tree.left, output) + tree + toString(tree.right, output);
        }


    }
}
