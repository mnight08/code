#include <iostream>
#include "bst.h"
using namespace std;


int main()
{
	bst tree;

	tree.insert("meowers");
	tree.insert("grafield");
	tree.insert("abra");
	tree.insert("cadabra");
	tree.insert("heathcliff");
	tree.insert("fluffers");

	tree.display();


	return 0;
}