#include "bst.h"



bst::bst()
{
	root = NULL;
}

void bst::recInsert(string s, node * & r)
{
	if( r == NULL )  //base case: empty tree
	{
		r = new node;
		r->data = s;
	}
	else if( s < r->data  ) //insert left
	{
		recInsert(s, r->left);
	}
	else//insert right, if greater or equal to root
	{
		recInsert(s, r->right);
	}
}

void bst::insert(string s)
{
	recInsert(s, root);
}

void bst::recDisplay(node * r)
{
	if( r != NULL )
	{
		recDisplay(r->left);
		cout << r->data << endl;
		
		recDisplay(r->right);
	}
}

void bst::display()
{
	recDisplay(root);
}

void bst::printinorder(node * tree)
{
	if(tree!=NULL)
	{
		print(left);
		cout<<data<<endl;
		print(right);
	}
}