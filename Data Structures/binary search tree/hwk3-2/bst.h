#include <iostream>
#include <string>
using namespace std;

class node
{
public:
	node()
	{
		right = NULL;
		left = NULL;
	}

	string data;
	node * right;
	node * left;
};


class bst
{
public:
	bst();

	void insert(string s);
    void display();
	void print(bst*);
	//string find(string s);

private:
	void recInsert(string s, node * & root);
	void recDisplay(node * r);
	node * root;
};
