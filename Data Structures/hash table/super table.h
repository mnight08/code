//this will really only be a method for the user to impliment a filter onto a table datatype.  by doing this, it seems i may be able to use the filter on various datastructures
#include "table.h"
#include "filter.h"
#include <string>
class supertable :public filter
{
private:

public:
	int count;
	//a linked list of tables
	table* head;
	supertable();
	~supertable();
	table* findtable(bool, table* current,unsigned int x);
	void inserttotable(bool outofnumbers,table *& current,unsigned int x, student me); 

	
    //add new student with given id and gpa to data structure           
    void insertStudent( unsigned int id, double gpa ); 
          
     //return the gpa of the student with given id.
     //Return -1 if student is not in data structure     
     double getGPA( unsigned int id );
     //change the gpa of the student with the given id
     //to newGPA.
     void updateGPA( unsigned int id, double newGPA ); 
	 table* findtable(unsigned int id);
};
