//the method i will implement, will consist of k hash tables, of length l.  each hash table will use the same hash function, operating on a different piece of the given data
#ifndef FILTER
#define FILTER
#include "validation table.h"
#include <iostream>
using namespace std;
class filter
{
	//this will probably be best defined as a template, but for now using int type is easiest
	protected:
		//int datasize;
		int hash(unsigned int x);
		bool validatesearch(unsigned int id, validationtable* current);		//if this fails return false,
		bool acknowledgeinsert(validationtable *&current,bool outofnumbers,unsigned int id);
		bool validatesearch(unsigned int id);
		bool validateinsertion(unsigned int id);
		bool validateinsertion(unsigned int id, validationtable* current);
		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		//i need to have something to keep track of all these tables.  i dont have anything yet, cause i havent decided whether to limit the number of permutations in this.  
		//i could use a linked list, but that would be a bit expensive, but not really, depending on the number of permutations required.  with 6 tables, i have the ability to distinguish about 1 million unique items
		//which takes a linked list of 6 tables.  with 100 tables, the ability to detect the presence of 10^100 unique items becomes possible.  if we are careful about how we take the keys, this gives an exact yes/no for
		//figuring whether an item is in a list or not.
		//this might be more useful if this were int value, not only telling you that there exist someitem that has all these digits, but how many have those digits.
		validationtable* head;
	
		filter();
};
#endif