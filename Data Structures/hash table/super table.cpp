#include "super table.h"

table* supertable::findtable(unsigned int id)
{
	if(validatesearch(id))
	return findtable(false,head,id);
	else return NULL;
}
void supertable::updateGPA(unsigned int id, double newGPA)
{
	table* found=findtable(id);
	if(found!=NULL&&found->data!=NULL)
		found->data->gpa=newGPA;
	else
		cout<<"fail!!!"<<endl;

}

double supertable::getGPA(unsigned int id)
{
	table* found=findtable(id);
	if(found!=NULL)
		if(found->data!=NULL)
			if(found->data->gpa!=NULL)
				if(found->data->id!=NULL)
					return found->data->gpa;
				else
					return -1;
			else return -1;
		else
			return-1;
	else return -1;
}


void supertable::insertStudent(unsigned int id, double gpa)
{
	if(validateinsertion(id))
	{

		acknowledgeinsert(filter::head,false,id);
		
		
		this->inserttotable(false,head,id,student(id,gpa));
		
	}
}

supertable::supertable()
{
	head=NULL;
	count=0;

}

//since i dont plan on using more than one table at one time, i wont define this
supertable::~supertable()
{

}

table* supertable::findtable(bool outofnumbers, table* current,unsigned int x)
{

	if(outofnumbers||current==NULL)
		return current;
	else
	{
		if(x<10)
			outofnumbers=true;
	 
		findtable(outofnumbers,current->next[x%10],x/10);
	}

}

void supertable::inserttotable(bool outofnumbers,table *& current,unsigned int x, student me)
{
		if(current==NULL)
			current=new table();
		if(outofnumbers)
		{
			if(current->data==NULL)
			{
				count++;
				current->data=new student(me.id,me.gpa);
				return;
			}
			else
			return;
			
		}
		if(x<10)
			outofnumbers=true;
		 
		inserttotable(outofnumbers,current->next[x%10],x/10,me);
	
}