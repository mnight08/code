/****************************this is not the correct file to derive link list queue from.  the one that should be there is only a copy of the other header file. 
i foudn the file, but the link on the web page is, or was wrong*/

/****************************************************************************************
*  Program Name:		lab2380_22_b_head.h
*  Lab 22:			    Header file for Lab 22
*						- Deriving linked list based queue from linked list
*  Date:				11/16/2009
*
*  Description: This file contains the prototype of the class linkeListQueueType, which 
* is derived from linkedListType
*****************************************************************************************/

#ifndef H_QueueType
#define H_QueueType

#include "headers.h"

using namespace std; 

//derive linkedListQueueType from linkedListType
template <class Type> 
class linkedListQueueType: public linkedListType<Type>
{
	//overload insertion operator<<
	template<class Type>
	friend ostream & operator<<(ostream& os, const linkedListQueueType<Type> & rhs); 

public:
	const linkedListQueueType<Type> & operator=	// overloading assignment operator
					(const linkedListQueueType<Type>&); 
	//void initializeList();					// initialize the list to an empty list
	//bool isEmpty();							// chech whether list is empty
	//int  length();							// read the number of nodes in list
	//void destroyList();						// to delete all nodes from the list
	//Type front();								// return the first element in the list
	//Type back();								// return the last element in the list
	//bool search(const Type & searchItem);		// to determine the searchItem is in the list
												// return true if yes or no otherwise
	//void insertFirst(const Type & newItem);	// insert the newItem to the beginning of the list
	//void insertLast(const Type & newItem);	// insert the newItem to the end of the list
	//void deleteNode(const Type & deleteItem); // delete the node containing the input item from the list
	//void print();								// print the list  
	
	//major stack operations
	void enQueue(Type & item);					// add item to the rear the queue
	Type & deQueue ();							// remove the front item from the queue

	// default constructor
	linkedListQueueType():linkedListType<Type>(){}; 
	// copy constructor 
	linkedListQueueType(const linkedListQueueType<Type>&);
	// the destructor
	~linkedListQueueType(){};						

private: 
	void copyQueue(const linkedListQueueType<Type> & rhs); 
											//copy other list to the invoking list
};

//enQueue: add item to the rear of the stack
template<class Type>
void linkedListQueueType<Type>::enQueue(Type & item)	
{
	assert(!isFull());						//make sure the stach is not full
	
	if(!isFull())
	{
		insertLast(item);					//add to the rear
	}
	else
	{
		cout<<"No push, stack is full."<<endl; 
	}
}

//pop: pop the top item out of the the stack
template<class Type>
Type & linkedListQueueType<Type>::deQueue( )	
{
	assert(!isEmpty());						//make sure the stach is not empty
	
	Type tmp; 
	if(!isEmpty())
	{
		tmp = front();						//get the front node value
		deleteNode(tmp);					//delete the from node
		return tmp; 
	}
	else
	{
		cout<<"No pop, stack is empty."<<endl; 
	}
}	

//overload insertion operator<<
template<class Type>
ostream & operator<<(ostream& os, const linkedListQueueType<Type> & rhs)
{
	nodeType<Type> *current;			//point to current node
	current = rhs.first; 
	int flag = 1; 

	while(current != NULL)
	{
		os<<setw(10) <<left <<current->info<<"  "; 
		if (flag % 5 == 0)				//print five element per line
			os << endl; 
		flag++; 
		current = current->link; 
	}
	os<<endl;

	return os; 
}

// copy constructor 
template <class Type>
linkedListQueueType<Type>::linkedListQueueType(const linkedListQueueType<Type>& rhs)
{
	first = NULL; 
	copyQueue(rhs); 
}

// overloading assignment operator
template <class Type>
const linkedListQueueType<Type>& 
		linkedListQueueType<Type>::operator=(const linkedListQueueType<Type>& rhs)
{
	if (this != & rhs)
	{
		copyQueue(rhs);
	}
	return *this; 
}

//copy stack
template <class Type>
void linkedListQueueType<Type>::copyQueue(const linkedListQueueType<Type> & rhs)
{
	nodeType<Type>	newNode,	//for creating a new node 
					current;	//point to current node
	
	
	if (!isEmpty())				//if list is not empty then destroy it
		destroyList(); 
	
	if (rhs.isEmpty())			//otherList is empty
	{
		first = last = NULL; 
		count = 0; 
		return; 
	}
	count = rhs.count;			//copy the count
	//otherList is not empty. first take care of the first node	
	first = new nodeType<Type>;
	first->info = rhs.first->info; 
	first->link = NULL; 
	last = first; 
	//now take care of additional nodes if any
	current = rhs.first->link;  
	while (current != NULL)
	{
		newNode = new nodeType<Type>; 
		newNode -> info = current->info; 
		newNode -> link = NULL; 
		last->link = newNode; 
		last = newNode; 

		current = current->link; 
	}
}

#endif
