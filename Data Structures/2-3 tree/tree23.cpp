#include "tree23.h"
#include "node.h"
#include <iostream>

node* tree23::getmax(node *input, node *target)
{
	if(target==NULL)
		return NULL;
	if(target->childrencount==2)
	{
		//insert to left node, and shift left to mid
		if(target->leftmax>input->rightmax)
		{
			target->mid=target->left;
			target->left=input;
			target->leftmax=target->rightmax;
			target->midmax=target->mid->rightmax;
		}
		//move right to mid, and move input to right
		else if(target->rightmax<input->rightmax)
		{
			target->mid=target->right;
			target->right=input;
			target->midmax=target->mid->rightmax;
			target->rightmax=target->right->rightmax;
		}
		return NULL;
	}
	else return NULL;
}


int tree23::deletenode(node *parent, node *target)
{
	if(parent==NULL)
	{
		return -1;
	}
	//the only time it should get to a leaf is if the leaf is the only item in the tree
	else if(parent->childrencount==0)
	{
		delete parent;
		root=NULL;
		root->childrencount=0;
		root->leftmax=0;
		root->rightmax=0;
		root->midmax=0;
		root->left=NULL;
		root->right=NULL;
		root->mid=NULL;
		root->parent=NULL;
		return 0;
	}
	else	if(parent->childrencount==2)
	{		
		parent->childrencount--;	
		
		if(parent->parent->left==parent)
		{
			if(parent->parent->right->childrencount==3||(parent->parent->mid!=NULL&&parent->parent->mid->childrencount==3))
			//if mid is present, and one of the brothers has more than two children take mids lowest child if it has more than 2, otherwise, take rights lowest
			if(parent->parent->mid!=NULL)
			{
			
			}
			//take rights lowest child
			else
			{
			}
		}
		else if(parent->parent->right==parent)
		{	
			if(parent->parent->left->childrencount==3||(parent->parent->mid!=NULL&&parent->parent->mid->childrencount==3))
			//if mid is present, and one of the brothers has more than three, then shift right
			if(parent->parent->mid!=NULL)
			{
			}
			else 

		}
		else if(parent->parent->mid==parent)
		{
		
		}
		else return -1;

		//if you cant get a brother, then do this, and call delete node one parent, and self
		if(parent->left->midmax==a)
		{
			delete parent->left;
			parent->left=parent->right;
			parent->right=NULL;
			parent->leftmax=parent->left->midmax;
			parent->midmax=0;
			parent->rightmax=0;
		}
		else 
		{
			delete parent->right;
			parent->right=NULL;	
			parent->rightmax=0;
			parent->midmax=0;
		}

		if(parent->childrencount==1)
		return deletnode(parent->parent,parent);
	}
	//trivial case
	else if(parent->childrencount==3)
	{
		parent->childrencount--;
		//if left, swap with mid, then delete
		if(parent->left->midmax==target)
		{
			delete parent->left;
			parent->left =parent->mid;
			//only one level above actual stored data
			parent->leftmax=parent->left->midmax;
			parent->mid=NULL;
			parent->midmax=0;
		}
		//if right swap with mid, then delete
		else if( parent->right->midmax==target)
		{
			delete parent->right;
			parent->right=parent->mid;
			parent->mid=NULL;
			parent->rightmax=parent->right->midmax;
			parent->midmax=0;
		}
		//if mid, then delete
		else if( parent->mid->midmax==target)
		{
			delete parent->mid;	
			parent->mid=NULL;
			parent->midmax=0;
		}
	}	else return-1;

}

int tree23::deletenode(node *root, int a)
{
	node * temp;
	temp= find(root, a);
	if(temp->leftmax==a)
		return deletenode(temp, temp->left);
	else if(temp->midmax==a)
		return deletenode(temp, temp->mid);
	else if(temp->rightmax==a)
		return deletenode(temp, temp->right);
	else return -1;
}

//actual integer will be stored in leftmax, 
int tree23::insert(node *root, int a)
{
	if(root==NULL)
	{
		root = new node;
		root->leftmax=a;
		root->midmax=a;
		root->rightmax=a;
		root->left=NULL;
		root->right=NULL;
		root->mid=NULL;
	}
	node *temp;
	//check if the item to insert is already in the tree, if it is, dont insert it.  this might conflict with random insertion and deletion of items.
	temp= this->find(root, a);
	if(temp!=NULL)
	{
		node * temp2;
		temp2=new node;
		temp2->leftmax=a;
		temp2->rightmax=a;
		temp2->midmax=a;
		if(temp->childrencount==0)
		{
			node*temp3;
			temp3=new node;
			if(temp2->rightmax>temp->rightmax)
			{
				temp3->left=temp;
				temp3->right=temp2;
				temp3->mid=NULL;
				temp3->leftmax=temp3->left->rightmax;
				temp3->rightmax=temp3->right->rightmax;
				temp3->midmax=0;
			}
			else
			{
				temp3->right=temp;
				temp3->left=temp2;
				temp3->mid=NULL;
				temp3->leftmax=temp3->left->rightmax;
				temp3->rightmax=temp3->right->rightmax;
				temp3->midmax=0;
			}
			return 0;
		}
		else	this->makeson(temp,temp2);
		return 0;
	}
	else return -1;
}

int tree23::makeson(node *target, node *son)
{
	if(target==NULL||son==NULL)
		return -1;
	else 
	{	
		if(target->childrencount==3)
		{
			//then target is root
			if(target->parent==NULL)
			{
				node* temp;
				temp= new node;
				if(son->rightmax>target->rightmax)
				{
					temp->left=target;
					temp->right=son;
				}
				else
				{
					temp->left=son;
					temp->right=target;
				}
				temp->childrencount=2;
				temp->leftmax=temp->left->rightmax;
				temp->rightmax=temp->right->rightmax;
			}
			//origionally i had done this overly complicated//here we just create a new node, and have the middle of target become a part of it, so each node now has 2 children, then we make a son of the parent
			else
			{
				node* temp;
				temp= new node;
				if(target->mid->rightmax>son->rightmax)
				{
					temp->left=son;
					temp->right=target->mid;
				}
				else
				{
					temp->right=son;
					temp->left=target->mid;
				}

				temp->mid=NULL;
				temp->childrencount=2;
				temp->leftmax=temp->left->rightmax;
				temp->rightmax=temp->right->rightmax;
				temp->midmax=0;
				return makeson(target->parent,temp);
			}

		}
		//if this is true, then it is a trivial case, so just put the item in//this is essentially the base case
		else if(target->childrencount==2)
		{
			son->parent=target;
			//should insert from left, then right, then mid
			if(target->mid==NULL)
			{
				if(getmax(target,son)!=NULL)
					return -1;
				else return 0;
			}
			else return -1;
		}
		else return -1;
	}
}

//i think this should be fine for the most part
node* tree23::find(node* root, int a)
{
	if(root==NULL)
		return root;
	//root is a leaf
	if(root->childrencount==0)
	{
		if(root->parent!=NULL)	
			return root->parent;
		else return root;
	}//check if current node only has leafs; essentially a base case
	else if(root->left->left==NULL)
		return root;
	else if(a<=root->leftmax)
		return find(root->left,a);
	else if(root->mid!=NULL&&a<=root->midmax)
		return find(root->mid,a);	
	else
		return find(root->right,a);
}

tree23::tree23(void)
{
}

tree23::~tree23(void)
{
}
