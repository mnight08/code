#pragma once
#include "node.h"

class tree23
{
public:


	//merge in a node, and sort them
	node* getmax(node* input,node* target);
	int makeson(node* target, node* son);
	int insert(node* root, int a);
	int deletenode(node* root, int a);//search for nodes parent, then call other delete
	int deletenode(node* parent, node*target);//a recursive delete
	node* find(node*root, int a);
	
	node* top;

	tree23(void);
	~tree23(void);
};
