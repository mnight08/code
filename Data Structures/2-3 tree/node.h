#pragma once

class node
{
public:
	//should save me some time as opposed to checking each node each time
	int childrencount;

	//there is probably a way to do this without using this extra pointer, but i cant think of  a way that still runs logn
	node* parent;

	node* left;
	node* mid;
	node* right;
	
	int leftmax;
	int midmax;
	int rightmax;

	node(void);
	~node(void);
};
