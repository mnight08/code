
#include<string>
#include<iostream>
using namespace std;

class minheap
{
public:
	minheap(int max);

	void initializeheap(string* itemlist, int newsize);

	void insert(string s);
	string extractMin();

	//test function
	void display();

private:
	int parent(int i);
	int lchild(int i);
	int rchild(int i);

	int size;  //number of items in heap
	string * heap;

};