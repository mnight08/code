
#include "minheap.h"


int main()
{
	minheap h(100);

	h.insert("elk");
	h.insert("rabbit");
	h.insert("toad");
	h.insert("lion");
	h.insert("zebra");
	h.insert("tauren");
	h.insert("squirrel");

	h.display();

	return 0;
}