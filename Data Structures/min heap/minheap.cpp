
#include <algorithm>
#include "minheap.h"
using namespace std;

//private helper functions

int minheap::parent(int i)
{
	return (i-1)/2;
}

int minheap::lchild(int i)
{
	return (i*2) + 1;
}

int minheap::rchild(int i)
{
	return (i+1)*2;
}


//public functions

minheap::minheap(int max)
{
	heap = new string[max];
	size = 0;
}

//dumb test function
void minheap::display()
{
	for(int i=0; i<size; i++)
		cout << heap[i] << endl;
}

void minheap::insert(string s)
{
	//put new item at end of heap
	heap[size] = s;
	size++;

	int current = size-1;
	while( heap[parent(current)] > heap[current] )
	{
		//bubble current item up
		swap( heap[current], heap[parent(current)] );
		current = parent(current);
	}
}


string minheap::extractMin()
{
	swap(heap[size],heap[0]);
	string temp=heap[size];
	size--;
	int current=0;
	while( heap[lchild(current)] < heap[current] ||heap[rchild(current)]<heap[current])
	{
		//bubble current item up
		if(heap[lchild(current)]<heap[rchild(current)])
		{
			swap( heap[current], heap[lchild(current)]);
			current=lchild(current);
		}
		else
		{
			swap( heap[current], heap[rchild(current)]);
		}	
			current = rchild(current);
	}

	return temp;
}

void minheap::initializeheap(string *itemlist,int newsize)
{
	heap=new string[size];
	size=newsize;
	for(int i=0;i<size;i++)
		heap[i]=itemlist[i];
	for(int i=0;i<size;i++)
	{
		int current = size-i-1;
		while( heap[parent(current)] > heap[current] )
		{
		//bubble current item up
		swap( heap[current], heap[parent(current)] );
		current = parent(current);
		}
	}

}