#abstract tree partition. A collection of paritions 
#of a set.  The partitions are quantified over levels.
#Every cell at a level l has a parent at level l-1 unless the cell is the 
#root.  Every cell has a number of children as well such that the union over 
#the children is the parent.  
class Partition(Set):
    #returns the cell that contains x
    def get_cell(x,l):
        pass
    def insert(x):
        pass
    def get_parent(x,l):
        pass

    #returns the parition trees that have the children of 
    def get_children(p,l):
        pass

    


