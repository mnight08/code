#ifndef largeintlargeintoverloading
#define largeintlargeintoverloading
#include "largeint.h"
#include "math.h"
//i should probably define all these operators in thier own headers.//it seems that they dont notice largeint as a class outside of main.
//the first operators i will overload,  i eventually need to do this for every numberic type to be useful
largeint largeint::operator =(largeint n)				//asssignment
{
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//i might be able to speed this up by checking if a certain node is the same in the new list, instead of destroying this one right away.

	//i need to redefine this probably completley//i need to check that if the two items being passed are the sameitem
	//seems to work fine
	nodeType<int>	*newNode;//for creating a new node 
	nodeType<int>	*current;	//point to current node

	//check if its the same list
	if (this==&n)
		return *this;

	if (!this->isEmpty())				//if list is not empty then destroy it
		this->destroyList(); 

	if (n.isEmpty())	//otherList is empty
	{
		first = last = NULL; 
		count = 0; 
		return *this; 
	}
	count =  n.count;	//copy the count
	//otherList is not empty. first take care of the first node	
	first = new nodeType<int>;
	first->info = n.first->info; 
	first->next = NULL;
	first->previous = NULL; 
	last = first; 
	//now take care of additional nodes if any
	current = n.first->next;  
	while (current != NULL)
	{
		newNode = new nodeType<int>; 
		newNode -> info = current->info; 
		newNode -> next = NULL; 
		newNode->previous = last; 
		last->next = newNode; 
		last = newNode; 

		current = current->next; 
	}
	return *this;
}


//this should be the fulll description of addition, but it has dependencies on subtractions, so it will not be completly usable until i have defined 
//subtraction.
//i might be able to pass by refference, if i rework my method, but fine tuning is  for the future
//i decided to not change the this object, of the given object, will slow down performance, but would better conform to other datatypes interface
largeint  largeint::operator +(largeint n)			//addition
{
	largeint temp(*this);
	//compare magnitude of the two numbers
	//just add if they are both have same signs
	if(((temp<0)&&(n<0))||(temp>0&&n>0))
	{	
		bool negative= false;
		if (temp.first->info<0)
			negative=true;


		temp.first->info=abs(temp.first->info);
		n.first->info=abs(n.first->info);
		if (temp>n)
		{
			//this will be defined as a private member, it will sum the list together, puting new list in the first parameter.
			sum(temp, n);
			//will step through each item in list, and make sure each is less than the size of each bit
			carryover(temp);
			if(negative)
				temp.first->info=temp.first->info*-1;

			return temp;

		}

		else if (temp<=n)
		{

			//this will be defined as a private member, it will sum the list together, puting new list in the first parameter.
			sum(n, temp);
			//will step through each item in list, and make sure each is less than the size of each bit
			carryover(n);
			temp=n;
			if(negative)
				temp.first->info=temp.first->info*-1;

			return temp;
		}
	}


	//different signs, then subtract
	else if(((temp<0)&&(n>0))||((temp>0)&&(n<0)))
	{	
		//if i do not do this line first, then i am right back to adding a number, because of the way subtraction is built
		//when you subtract you put  a - sign in front of item. 
		n.first->info=n.first->info*-1;
		temp=temp-n;
		return temp;
	}	

	//n equal zero
	else if(n==0)
	{
		return temp;
	}

	//this number =0
	else if(temp==0)
	{
		temp=n;
		return temp;
	}
}


/****i should make this into a few nested if statements****/

largeint  largeint::operator -(largeint n)				//subtraction
{	
	n.first->info=n.first->info*-1;
	largeint temp(*this);
	//compare magnitude of the two numbers
	//just add if they are both have same signs
	if(((temp<0)&&(n<0))||(temp>0&&n>0))
	{	
		temp=temp+n;
		return temp;
	}
	//different signs, then subtract
	else if(((temp<0)&&(n>0))||((temp>0)&&(n<0)))
	{	

		//if temp is larger than 0
		if(temp<0)
		{

			temp.first->info=temp.first->info*-1;

			//different signs, and same number return 0, x-x=0
			if(temp==n)
			{
				temp.destroyList();
				temp.insertfirst(0);
				return temp;
			}
			//if this is true the answer will be positive.  -y+(y+n)=n>0
			if(temp<n)
			{
				subtract(n, temp);
				carryover(n);
				temp=n;
				return temp;
			}

			//this will be negative if true.  -(y+n)+y=-n<0
			else if(temp>n)
			{
				subtract( temp,n);
				carryover(temp);
				temp.first->info=temp.first->info*-1;
				return temp;
			}
		}

		else if(temp>=0)
		{	
			n.first->info=n.first->info*-1;

			//different signs, and same number return 0, x-x=0
			if(temp==n)
			{
				temp.destroyList();
				temp.insertfirst(0);
				return temp;
			}
			if(temp<n)
			{
				subtract(n, temp);
				carryover(n);
				temp=n;
				temp.first->info=temp.first->info*-1;
				return temp;
			}
			else if(temp>n)
			{
				subtract( temp,n);
				carryover(temp);
				return temp;
			}


		}

	}	


	//move these to describe subtraction.

	//n equal zero
	else if(n==0)
	{
		return temp;
	}

	//this is for subtraction, not addition
	//this number =0
	else if(temp==0)
	{
		temp=n;
		return temp;

	}
}



//i have a few choices on how to do this.  the most straight forward is to add several times.  it would seem more efficient to work in a shift operations, but is a bit confusing
largeint  largeint::operator *(largeint n)				//multiplication
{
	largeint temp(*this);
	int ONE =1;
	largeint one(ONE);
	//if they are the same value
	if(temp==0||n==0)
	{
		temp.destroyList();
		temp.insertfirst(0);
		return temp;
	}

	//compare magnitude of the two numbers
	//answer should be positive if this is true
	else if((n>0&&temp>0)||(n<0&&temp<0))		
	{
		if(n>0&&temp>0)
		{

		largeint temp2(temp);
			while(n>one)
			{
				temp=temp+temp2;
				n=n-one;
			}


		}
		//answer should be positive
		else if(n<0&&temp<0)
		{
			n.first->info=n.first->info*-1;
			temp.first->info=temp.first->info*-1;

			largeint temp2(temp);
			while(n>one)
			{
				temp=temp+temp2;
				n=n-one;
			}

		}
	}

	//answer should be negative
	else if(n>0&&temp<0||n<0&&temp>0)
	{
		if(n>0&&temp<0)
		{
			temp.first->info=temp.first->info*-1;
			largeint temp2(temp);
			while(n>one)
			{
				temp=temp+temp2;
				n=n-one;
			}
		}
		else if(n<0&&temp>0)
		{
			n.first->info=n.first->info*-1;
			largeint temp2(temp);
			while(n>one)
			{
				temp=temp+temp2;
				n=n-one;
			}

		}
		temp.first->info=temp.first->info*-1;
	}

//carryover(temp);
return temp;
}


largeint  largeint::operator /(largeint n)				//division
{
	largeint temp(*this);
	int ONE =1;
	int ZERO=0;
	largeint one(ONE);
	largeint zero(ZERO);
	largeint count(ZERO);
	//if they are the same value
	if(temp==0&&n!=0)
	{
		//default constructor, will return 0 essentially
		return largeint();
	}
	else if(n==0)
	{
		cout<<"undefined, returning 0 instead, please dont divide by zero";
		return largeint();
	}

	//compare magnitude of the two numbers
	//answer should be positive if this is true
	else if((n>0&&temp>0)||(n<0&&temp<0))		
	{
		if(n>0&&temp>0)
		{
			for(;temp>=n;count=count+one, temp=temp-n)
			{

			}

		}
	//answer should be positive
		else if(n<0&&temp<0)
		{
			n.first->info=n.first->info*-1;
			temp.first->info=temp.first->info*-1;

			for(;temp>=n;count=count+one, temp=temp-n)
			{

			}

		}
	}

	//answer should be negative
	else if(n>0&&temp<0||n<0&&temp>0)
	{
		if(n>0&&temp<0)
		{
			temp.first->info=temp.first->info*-1;
			for(;temp>=n;count=count+one, temp=temp-n)
			{

			}
		}
		else if(n<0&&temp>0)
		{
			n.first->info=n.first->info*-1;
			for(;temp>=n;count=count+one, temp=temp-n)
			{

			}

		}
		count.first->info=count.first->info*-1;
	}

//carryover(count);
return count;
}


/*this is a useless operation if i cannot speed up addition/subtraction, so i wont define it till they are faster
//so far this will only work for integer values.
largeint  largeint::operator ^(largeint &n)				//exponential
{

//i could overload iterator, but this shoudl work for now
int x=1;
largeint one(x);

if(n.first->info==0)
return  one;
largeint temp(*this);
else if(n.first->info>0)
{
//i could overload iterator, but this shoudl work for now
for (largeint count(n);count>0;count=count-one)
{
temp=temp*temp;	
}
}


//find nth root.  this is only useful if add 
else if(n.first->info<0)
{

//will be here till i add decimal notation, otherwise, this would only return an integer type value, that would be of little use if the root is transcendental. 
//i would also need to add funcitionality to work with decimal powers.
cout<<"sorry function not added yet";
return this;


}
}
*/
//just an edited copy of / operator definition.  i dont think this should ever return negative
//this is not taking into account a shifting operation to save time
largeint largeint::operator %(largeint n)				//mod
{
	largeint temp(*this);
	int ONE =1;
	int ZERO=0;
	largeint one(ONE);
	largeint zero(ZERO);
	//largeint count(ZERO);
	//if they are the same value
	if(temp==0&&n!=0)
	{
		//default constructor, will return 0 essentially
		return largeint();
	}
	else if(n==0)
	{
		cout<<"undefined, returning 0 instead, please dont divide by zero";
		return largeint();
	}

	//compare magnitude of the two numbers
	//answer should be positive if this is true
	else if((n>0&&temp>0)||(n<0&&temp<0))		
	{
		if(n>0&&temp>0)
		{
			for(;temp>=n; temp=temp-n){}
		}
		//answer should be positive
		else if(n<0&&temp<0)
		{
			n.first->info=n.first->info*-1;
			temp.first->info=temp.first->info*-1;

			for(;temp>=n; temp=temp-n){}
		}
	}

	//
	else if(n>0&&temp<0||n<0&&temp>0)
	{
		if(n>0&&temp<0)
		{
			temp.first->info=temp.first->info*-1;
			for(;temp>=n; temp=temp-n){}
		}
		else if(n<0&&temp>0)
		{
			n.first->info=n.first->info*-1;
			for(;temp>=n; temp=temp-n){}
		}
	}
return temp;
}



#endif