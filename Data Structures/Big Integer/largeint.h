#include "large int headers.h"

#ifndef largeinth//i cant have this with the way i have it being called twice
#define	largeinth



//ideally this should be able to do anythign that an integer can do.

class largeint: protected doubleLinkedListType<int>
{	
private:
	const int bits;

	void carryover(largeint& number);						//loop though list, and carry over each bit, check most left, then add extra bit if needed
	void sum (largeint& largerone,largeint& smallerone);	//add smaller one to larger one, this does the actual addition, but not carryover.

	void subtract(largeint& largerone,largeint& smallerone);//subtract smaller one from smaller one, does the actual subtraction, no carryover. 
	void borrow(nodeType<int> *number);						//do the borrowing 
	void removeleadingzeros(largeint & number);

	void copyList(const doubleLinkedListType<int> & otherList); 	//copy other list to the invoking list
	//i may want to add this
	void magnitude(){if (this->first->info<0) this->first->info= this->first->info*-1; else return;}

	//use for input only so far, but can probably be moded to use with multiplication
	void carryback(int howfar);

public:

	//basic comparison operartions
	bool operator >(largeint n);
	bool operator <(largeint n);
	bool operator >=(largeint n);
	bool operator <=(largeint n);
	bool operator ==(largeint n);
	bool operator !=(largeint n);

	//logic operators to compare to ints, this is needed to compare to zero
	bool operator  >(int n/********this is only useful if you compare to zero, i will define other type later for integers by reference********/){if(first->info>n)return true;else return false;}
	bool operator >=(int n/********this is only useful if you compare to zero, i will define other type later for integers by reference********/){if(first->info>=n)return true;else return false;}
	bool operator  <(int n/********this is only useful if you compare to zero, i will define other type later for integers by reference********/){if(first->info<n)return true;else return false;}
	bool operator <=(int n/********this is only useful if you compare to zero, i will define other type later for integers by reference********/){if(first->info<=n)return true;else return false;}
	bool operator ==(int n/********this is only useful if you compare to zero, i will define other type later for integers by reference********/){if(first->info==n)return true;else return false;}
	bool operator !=(int n/********this is only useful if you compare to zero, i will define other type later for integers by reference********/){if(first->info!=n)return true;else return false;}



	//the first operators i will overload,  i eventually need to do this for every numberic type to be useful
	largeint  operator =(largeint n);				//asssignment
	largeint  operator -(largeint n);				//subtraction
	largeint  operator +(largeint n);				//addition
	largeint  operator *(largeint n);				//multiplication
	largeint  operator /(largeint n);				//division
	largeint  operator ^(largeint n);				//exponential
	largeint  operator %(largeint n);				//mod


	largeint  operator =(int n);				//asssignment
	largeint  operator -(int n);				//subtraction
	largeint  operator +(int n);				//addition
	largeint  operator *(int n);				//multiplication
	largeint  operator /(int n);				//division
	largeint  operator ^(int n);				//exponential
	largeint  operator %(int n);				//mod


	//these will initially typecast to int
	largeint  operator =(double n);				//asssignment
	largeint  operator -(double n);				//subtraction
	largeint  operator +(double n);				//addition
	largeint  operator *(double n);				//multiplication
	largeint  operator /(double n);				//division
	largeint  operator ^(double n);				//exponential
	largeint  operator %(double n);				//mod


	largeint  operator =(long n);				//asssignment
	largeint  operator -(long n);				//subtraction
	largeint  operator +(long n);				//addition
	largeint  operator *(long n);				//multiplication
	largeint  operator /(long n);				//division
	largeint  operator ^(long n);				//exponential
	largeint  operator %(long n);				//mod


	//errors, for using wrong type
	largeint  operator =(char n){cout<<"you cant give a largeint a char value!!!\a"<<endl;return *this;};				//asssignment
	largeint  operator -(char n){cout<<"you cant give a largeint a char value!!!\a"<<endl;return *this;};				//subtraction
	largeint  operator +(char n){cout<<"you cant give a largeint a char value!!!\a"<<endl;return *this;};				//addition
	largeint  operator *(char n){cout<<"you cant give a largeint a char value!!!\a"<<endl;return *this;};				//multiplication
	largeint  operator /(char n){cout<<"you cant give a largeint a char value!!!\a"<<endl;return *this;};				//division
	largeint  operator ^(char n){cout<<"you cant give a largeint a char value!!!\a"<<endl;return *this;};				//exponential
	largeint  operator %(char n){cout<<"you cant give a largeint a char value!!!\a"<<endl;return *this;};				//mod


	//errors, for using wrong type
	largeint  operator =(string n){cout<<"you cant give a largeint a string value!!!\a"<<endl;return *this;};	//asssignment
	largeint  operator -(string n){cout<<"you cant give a largeint a string value!!!\a"<<endl;return *this;};	//subtraction
	largeint  operator +(string n){cout<<"you cant give a largeint a string value!!!\a"<<endl;return *this;};	//addition
	largeint  operator *(string n){cout<<"you cant give a largeint a string value!!!\a"<<endl;return *this;};	//multiplication
	largeint  operator /(string n){cout<<"you cant give a largeint a string value!!!\a"<<endl;return *this;};	//division
	largeint  operator ^(string n){cout<<"you cant give a largeint a string value!!!\a"<<endl;return *this;};	//exponential
	largeint  operator %(string n){cout<<"you cant give a largeint a string value!!!\a"<<endl;return *this;};	//mod


	//it might be easier to not use refference for int
	//largeint(int n);					//i might be able to use this for assigning ints

	largeint();							//default constructor
	largeint(int n);					//this cant be passed by reference, or else i cant typecast for some reason.

	friend ostream & operator <<(ostream &stream, largeint&);
	friend istream & operator >>(istream &stream, largeint&);

	//i dont think i have this defined yet
	largeint( largeint &);				//copy constructor, need to make one with all parameters
	~largeint(){};

};

//can be moded to shift operation easily
void largeint::carryback(int howfar)
{
	if(howfar<1)
		return;

	nodeType<int> *current;	
	int swap;

	int carryright, keepleft;
	current=first;
	carryright=0;

	for(;current;current=current->next)
	{
			keepleft=current->info/int(pow(double(10),howfar));
			swap=(carryright*(bits/pow(double(10), howfar))+keepleft);
			carryright=current->info%int(pow(double(10),howfar));
			current->info=swap;

	}


	//look for a leading zero, there should be one.
	while(first->info==0)
		deleteNode(0);
}

//output the numbers, need to be careful about leading zeros
ostream & operator <<(ostream &stream, largeint&n)
{
	nodeType<int>* current;
	current =n.first->next;
	stream<<n.first->info;

	for(;current!=NULL;current=current->next)
	{
		//this is a more general case for outputing leading zeros,  check if info is the minimun required number to fill a leading bit.  if not, then put a zero in front.
		for(int i =n.bits/10; i>1; i=i/10)
		{
			if (current->info<i)
			{
				stream<<0;

			}
			else 
				break;
		}
		stream<<current->info;

	}
	return stream;

}

//i could copy the entire set of characters into an array, then start copying from back of the array.
istream & operator >>(istream &stream, largeint& n)
{
	bool negative=false;
	int convert=0;
	char gc=' ';
	nodeType<int>* current;

	//check the first character being put in, if it is minus sign, then the number is going to be negative, if it is, we say it is, and ignore the character.
	if(stream.peek()=='-')
	{
		negative =true;
		stream.ignore();
	}

	for(current=n.first;stream&&!(int(stream.peek())<47||int(stream.peek())>57);current=current->next)
	{

		//check to see that we are still getting numbers, and not symbols, letters, etc. if we are, we take what we have and stop.
		if(int(stream.peek())<47||int(stream.peek())>57)
		{
			break;
		}	

		else if(!current)
		{
			n.insertlast(0);
			current=n.last;
		}

		//		n.insertlast(0)/*this is to add a new node, kinda a lazy way though*/,current=n.last, current->info=0;
		for(int i=(n.bits/10);i>=1/*this needs to be something dependent on bits*/;i=i/10)
		{	
			if(int(stream.peek())<47||stream.peek()>57)
			{	
				//	current->info=(current->info/(i*10));
				n.carryback((i/10)+1);

				break;
			}
			stream.get(gc);
			current->info=((int(gc)-48)*i)+current->info;


		}
	}

	if(negative==true)
		n.first->info=n.first->info*-1;
	stream.get(gc);				//get the space, or return that is after the entry of a number
	return stream;

}


//search for leading zeroes and remove them
void largeint::removeleadingzeros(largeint & number)
{
	int temp=0;
	while(number.first->info==0)
	{
		number.deleteNode(temp);
	}

}

//only useful if you are actually subtracting numbers other than 0
void largeint::subtract(largeint& largerone,largeint& smallerone)
{
	nodeType <int> *number1;
	nodeType <int> *number2;
	number1=largerone.last;
	number2=smallerone.last;
	for(;number2!=NULL;)
	{
		//check to borrow
		if(number2->info>number1->info)
			borrow(number1);

		//subtract
		number1->info=number1->info-number2->info;


		//take another step towards most significant bit.
		number1=number1->previous;
		number2=number2->previous;
	}
	//get rid of any leading zeroes
	removeleadingzeros(largerone);

}

//the nodeType i pass cannot be part of a signed list.  the subtraction operator should determine the sign only, then pass abs values here, not this function 
void largeint::borrow(nodeType <int> *number)
{
	nodeType <int> *findabit;
	findabit=number->previous;
	//this is made to work with a function that subtracts a smaller number from a  larger number, like -123123+12314
	//it looks for the first node in the list to the left of this bit to have  a number in it.
	//i dont know why, but when i did this all in the head of the for loop i ran into errors, so i switched to a while loop
	//that uses an embedded if to break.  seems to work fine now.
	while(true)
	{
		if(findabit->info!=0)
			break;
		else 
			findabit=findabit->previous;
	}

	//walk back to number, shifting to the right usually the treck should not be so far. 
	for(;findabit!=number;findabit=findabit->next)
	{
		findabit->info=findabit->info-1;
		findabit->next->info=findabit->next->info+bits;
	}

	//i need to remember to go back after subtraction, and carryover and get rid of empty leading nodes.  
}


largeint::largeint(largeint  &otherList):bits(1000)
{
	//will probably cause errros

	nodeType<int>	*newNode;//for creating a new node 
	nodeType<int>   *current;	//point to current node

	//check if its the same list
	if (this==&otherList)
		return;

	if (!isEmpty())				//if list is not empty then destroy it
		destroyList(); 

	if (otherList.isEmpty())	//otherList is empty
	{
		first = last = NULL; 
		count = 0; 
		return; 
	}
	count = otherList.count;	//copy the count
	//otherList is not empty. first take care of the first node	
	first = new nodeType<int>;
	first->info = otherList.first->info; 
	first->next = NULL;
	first->previous = NULL; 
	last = first; 
	//now take care of additional nodes if any
	current = otherList.first->next;  
	while (current != NULL)
	{
		newNode = new nodeType<int>; 
		newNode -> info = current->info; 
		newNode -> next = NULL; 
		newNode->previous = last; 
		last->next = newNode; 
		last = newNode; 

		current = current->next; 
	}

}

bool largeint::operator >(largeint n)
{
	//fastest way to compare
	if (count<n.count)
		return false;
	else if (count>n.count)
		return true;

	//go through list, and compare each item
	else
	{
		nodeType<int> *thislist=this->first;
		nodeType<int> *otherlist=n.first;
		for(;thislist!=NULL;)
		{
			if(thislist->info>otherlist->info)
				return true;
			else if(thislist->info<otherlist->info)
				return false;
			else 
			{
				thislist=thislist->next;
				otherlist=otherlist->next;
			}
		}

		//if it getshere, they are equal, so false
		return false;
	}
}

bool largeint::operator <(largeint n)
{
	//fastest way to compare
	if (count>n.count)
		return false;
	else if (count<n.count)
		return true;

	//go through list, and compare each item
	else
	{
		nodeType<int> *thislist=this->first;
		nodeType<int> *otherlist=n.first;
		for(;thislist!=NULL;)
		{
			if(thislist->info<otherlist->info)
				return true;
			else if(thislist->info>otherlist->info)
				return false;
			else 
				thislist=thislist->next;
			otherlist=otherlist->next;
		}

		//if it getshere, they are equal, so false
		return false;

	}
}

bool largeint::operator >=(largeint n)
{
	//fastest way to compare
	if (count<n.count)
		return false;
	else if (count>=n.count)
		return true;

	//go through list, and compare each item
	else
	{
		nodeType<int> *thislist=this->first;
		nodeType<int> *otherlist=n.first;
		for(;thislist!=NULL;)
		{
			if(thislist->info>otherlist->info)
				return true;
			else if(thislist->info<otherlist->info)
				return false;
			else 
				thislist=thislist->next;
			otherlist=otherlist->next;
		}

		//if it getshere, they are equal, so false
		return true;
	}
}

bool largeint::operator <=(largeint n)
{
	//fastest way to compare
	if (count>n.count)
		return false;
	else if (count<=n.count)
		return true;

	//go through list, and compare each item
	else
	{
		nodeType<int> *thislist=this->first;
		nodeType<int> *otherlist=n.first;
		for(;thislist!=NULL;)
		{
			if(thislist->info>otherlist->info)
				return false;
			else if(thislist->info<otherlist->info)
				return true;
			else 
				thislist=thislist->next;
			otherlist=otherlist->next;
		}

		//if it getshere, they are equal, so false
		return true;
	}
}

bool largeint::operator ==(largeint n)
{
	//fastest way to compare
	if (count<n.count)
		return false;
	else if (count>n.count)
		return false;

	//go through list, and compare each item
	else
	{
		nodeType<int> *thislist=this->first;
		nodeType<int> *otherlist=n.first;
		for(;thislist!=NULL;)
		{
			if(thislist->info>otherlist->info)
				return false;
			else if(thislist->info<otherlist->info)
				return false;
			else 
				thislist=thislist->next;
			otherlist=otherlist->next;
		}

		//if it getshere, they are equal, so false
		return true;
	}
}

bool largeint::operator !=(largeint n)
{
	//fastest way to compare
	if (count<n.count)
		return true;
	else if (count>n.count)
		return true;

	//go through list, and compare each item
	else
	{

		nodeType<int> *thislist=this->first;
		nodeType<int> *otherlist=n.first;
		for(;thislist!=NULL;)
		{
			if(thislist->info>otherlist->info)
				return true;
			else if(thislist->info<otherlist->info)
				return true;
			else 
				thislist=thislist->next;
			otherlist=otherlist->next;
		}

		//if it getshere, they are equal, so false
		return false;
	}
}






// add the smaller list to larger one
void largeint::sum(largeint& largerone,largeint& smallerone)
{
	nodeType<int>* current;
	nodeType<int>* largerint;
	//add all the nodes of the smaller one to the larger one
	for (current= smallerone.last, largerint=largerone.last;current!=NULL;current=current->previous,largerint=largerint->previous)
	{	
		largerint->info=largerint->info+current->info;

	}

}

//check each bit for size
void largeint::carryover(largeint& number)
{
	nodeType<int> *current;
	current=number.last;

	//set each bit lower than bit size
	for(;current->previous!=NULL;current=current->previous)
	{
		current->previous->info=(current->previous->info)+(current->info/bits);
		current->info=current->info%bits;
	}

	//check last bit for size, if it is too large, insert first with the carryover, then have current equal remainder.
	if(current->info>=bits)
	{
		number.insertfirst(current->info/bits);
		current->info=current->info%bits;

	}
}


largeint::largeint():bits(1000)
{
	this->destroyList();
	insertfirst(0);

}


//this will need to be modified later, raising 10000 to a power gets really large
//i realized that i did not need to do that if i lower temp by dividing by the base number

largeint::largeint(int n):bits(1000)
{
	*this=n;


	this->destroyList();
	if (n==0)
	{
		this->insertfirst(0);
		return;
	}
	int i=1;
	int temp=abs(n);
	for (;temp >=bits;i++)
	{
		this->insertfirst(temp%(bits));
		temp=temp/(int(bits));

	}

	if (n<0&&temp!=0)
	{
		this->insertfirst(temp*-1);

	}
	else if(temp!=0)
		this->insertfirst(temp);
}


#endif