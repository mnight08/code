#include <iostream>
#include <list>
using namespace std;
/**
 *\class Point
 *Represents a point in 2d space.  Coordinates currently supported are: Cartesian.
 *
 */
class Point
{
	public:
		double x;
		double y;

	Point(double tx, double ty)
	{
		setPoint(tx,ty);
	}
	Point()
	{
		x=y=0;
	}
	void setPoint(double tx, double ty)
	{
		x=tx;
		y=ty;
	}
};



/**
 *
 *\class QuadTree
 *Represents a bounded subset of R2. In particular, each region 
 * is given by its boundaries. A region that can be parititioned into four child regions where each subregion has equal area and any region contains at most cap points.
 *Main use cases:
 *Bulk insert of points on creation
 *normalization optimization optimization.  If, we normalize each region, then we may be able 
 *Get all points in a certain region
 *Get all regions at a certain depth.
 *Does not currently support inserting a single point at a time as it is not needed.
 */

class QuadTree{
	//this is a region. Each region holds points in its list until it 
	//has too many. Then it spreads them among
	//its sub regions.

	private:
		bool isParent;
		double left_boundary;
		double right_boundary;
		double top_boundary;
		double bottom_boundary;

		int cap;
		vector<Point> points;
		QuadTree* subregions[4];

	public:

		QuadTree(vector<Point> points, int c){
		
			//get boundaries.
			////!update this to set lb to min x, rb to 
			//max x and so forth.
			double lb=0, rb=1, tb=1, bb=0;
			       
			left_boundary = lb;
			right_boundary = rb;
			top_boundary = tb;
			bottom_boundary = bb;
			cap=c;
			//if too many points, split and distribute points to sub regions.
			if( points.size()>c){
				vector<Point> top_left;
				vector<Point> top_right;				
					
				vector<Point> bottom_left;
				vector<Point> bottom_right;
				Point center=getMidPoint();
				double x_c=center.x;
				double y_c=center.y;

				for( int i =0; i<points.size();i++){
					double x=points[i].x;
					double y=points[i].y;

					if(x>=x_c && y>=y_c){
						top_right.push_back(points[i]);
					}else if (x<x_c && y>=y_c){
						top_left.push_back(points[i]);
					}else if (x<=x_c && y<y_c){
						bottom_left.push_back(points[i]);
					}else if (x>x_c && y<y_c){
						bottom_right.push_back(points[i]);
					}
					
				}


				isParent=true;
				subregions[0]=new QuadTree(top_right,c);
				subregions[1]=new QuadTree(top_left,c);	
				subregions[2]=new QuadTree(bottom_left,c);
				subregions[3]=new QuadTree(bottom_right,c);
			}else{
				isParent=false;
				points=points;
			}

	}


	Point getMidPoint(){
		return Point((left_boundary+right_boundary)/2,(top_boundary+bottom_boundary)/2);
	}

	float getArea(){
		return (right_boundary-left_boundary)*(top_boundary-bottom_boundary);
	}

		
	int getDepth(){
		
		if(isParent)
		{
			int depth0=subregions[0]->getDepth();
			int depth1=subregions[1]->getDepth();
			int depth2=subregions[2]->getDepth();
			int depth3=subregions[3]->getDepth();
			return 1+max(max(depth0,depth1),max(depth2,depth3));

		}
		else return 0;
	}

	int getSize(){
		if(!isParent){
			return points.size();
		}
		else {
			int size0=subregions[0]->getSize();
			int size1=subregions[1]->getSize();
			int size2=subregions[2]->getSize();
			int size3=subregions[3]->getSize();
			return size0+size1+size2+size3;
		}
	}

	void display(){
		if (isParent) 
			for( int quadrant=0;quadrant<4;quadrant++)
				subregions[quadrant]->display();
		else {	
			for(int i=0;i<points.size();i++)
				cout<<" ("<<points[i].x
					<<","<<points[i].y<<") ";
		}
	}

	//Return an iterator of all of the points in this quadtree.
	vector<Point> getPoints()
	{
		if(isParent){
			
			vector<Point> list1=subregions[0]->getPoints();
			vector<Point> list2=subregions[1]->getPoints();
			vector<Point> list3=subregions[2]->getPoints();
			vector<Point> list4=subregions[3]->getPoints();
			//splice these together
		}else{
			return points;
		}
		
	}
};
