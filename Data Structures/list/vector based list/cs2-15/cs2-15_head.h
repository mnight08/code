/****************************************************************************************
*  Program Name:		vectorListType.h
*  Author:				Zhixiang Chen
*  Date:				09/02/2008
*  Course:				CSCI-2380.01
*  Lab:					Five
*
*  Description: This file contains the prototype of the class vectorListType
*****************************************************************************************/

#ifndef H_vectorListType
#define H_vectorListType
#include "headers.h"
using namespace std; 


template <class Type> 
class vectorListType
{
public:
	const vectorListType<Type> & operator=	// overloading assignment operator
				(const vectorListType<Type>&); 
	Type & operator[] (int index);			// index operator overloading
	void initializeList();					// initialize the list to an empty list
	bool isEmpty();							// chech whether list is empty
	bool isFull();							// chech whether list is full
	int  length();							// read the number of nodes in list
	void destroyList();						// to delete all nodes from the list
	Type front();							// return the first element in the list
	Type back();							// return the last element in the list
	bool search(const Type & searchItem);	// to determine the searchItem is in the list
											// return true if yes or no otherwise
	void insertFirst(const Type & newItem);	// insert the newItem to the beginning of the list
	void insertLast(const Type & newItem);	// insert the newItem to the end of the list
	void deleteItem(const Type & deleteItem); // delete the node containing the input item from the list
	void print();							// print the list  
	vectorListType();						// default constructor
	vectorListType(int n);					// another constructor
	vectorListType(const vectorListType<Type>&);// copy constructor 
	~vectorListType();						// the destructor

protected: 
	
	vector<Type> list;								// pointer for dynamic vector

private: 
	void copyList(const vectorListType<Type> & otherList); 
											//copy other list to the invoking list
};


// Overload  vector index operator []
template <class Type>
Type& vectorListType<Type>::operator[](int index)
{
	assert(0<= index && index < list.size());		//check the range of the index
	return list[index];
}

// print the list
template <class Type>
void vectorListType<Type>::print( )
{
	int flag = 1; 
	for (int i = 0; i < list.size(); i++)
	{
		cout << list[i] << "  "; 
		if (flag %5 == 0)
			cout << endl;
		flag++; 
	}
}

// initialize the list to an empty list
template <class Type> 
void vectorListType<Type> :: initializeList()
{
	list.clear(); 
}

// chech whether list is empty
template <class Type> 
bool vectorListType<Type>::isEmpty()
{
	return list.empty(); 
}

// chech whether list is full
template <class Type> 
bool vectorListType<Type>::isFull()
{
	return false;						//assume memory is always available
}

// read the number of nodes in list
template <class Type>
int  vectorListType<Type>::length()
{
	return list.size(); 
}
	
// to delete all nodes from the list
template <class Type>
void vectorListType<Type>::destroyList()
{
	list.clear(); 
}


// return the first element in the list
template <class Type> 
Type vectorListType<Type>::front()
{
	assert(!list.empty());			//to make sure the list is not empty
	return list[0]; 
}

// return the last element in the list
template <class Type> 
Type vectorListType<Type>::back()
{
	assert(!isEmpty());			//to make sure the list is not empty
	return list[list.size() - 1]; 
}

// to determine the searchItem is in the list
// return true if yes or no otherwise
template <class Type> 
bool vectorListType<Type>::search(const Type & searchItem)
{
	assert(!isEmpty());			// make sure the list is not empty
	
	bool found = false;			//flag for founding status
	
	int i = 0;			//point to first
	while (!found && i < list.size())
	{
		if (list[i] == searchItem)
			found = true;		//find the searchItem
		else					//otherwise move to the next node
			i++; 
	}
	return found; 
}

// insert the newItem to the beginning of the list
template <class Type>
void vectorListType<Type>::insertFirst(const Type & newItem)
{
	assert(!isFull());				//make sure the list is not full
    
	list.insert(list.begin(), newItem); 
}


// insert the newItem to the end of the list
template <class Type> 
void vectorListType<Type>::insertLast(const Type & newItem)
{
	assert(!isFull());				//make sure the list is not full
	
	list.push_back(newItem); 
}



// delete the node containing the input item from the list	
template <class Type>
void vectorListType<Type>::deleteItem(const Type & deleteItem)
{
	vector<Type>::iterator iter;	//declare a list iterator 
	int i; 
	bool found = false; 
	iter = list.begin();			//iter points to the beginning of the list
	for (i=0; i<list.size(); i++, iter++)
	{
		if (list[i] == deleteItem)
		{
			found = true; 
			break; 
		}
	}
	if (!found)						//if not found then stop
		return; 
	else							//found then delete
	{
		list.erase(iter);			//remove the element pointed by iter
	}
}

// default constructor	
template <class Type>
vectorListType<Type>::vectorListType()
{
	list.clear();
}

/*
// another constructor	
template <class Type>
vectorListType<Type>::vectorListType(int n)
{
	assert(n>=0);				//make sure n >= 0
	count = 0; 
	maxSize = n;
	list = new Type[n]; 
}
*/

// copy constructor 
template <class Type>
vectorListType<Type>::vectorListType(const vectorListType<Type>& otherList)
{
	copyList(otherList); 
}

//destructor
template <class Type>
vectorListType<Type>::~vectorListType()
{
	list.clear(); 
}

//copy list
template <class Type>
void vectorListType<Type>::copyList(const vectorListType<Type> & otherList)
{
	list.clear(); 
	for (int i=0; i<otherList.size(); i++)
	{
		list.push_back(otherlist[i]); 
	}
}

// overloading assignment operator
template <class Type>
const vectorListType<Type>& 
		vectorListType<Type>::operator=(const vectorListType<Type>& otherList)
{
	if (this != & otherList)
	{
		copyList(otherList);
	}
	return *this; 
}



#endif
