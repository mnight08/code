#ifndef studenttypeclass
#define studenttypeclass
#include "headers.h"
using namespace std;

//person type class
class personType
{
public:	
	friend ostream& operator<<(ostream&, const personType&);

	bool operator==(const personType &test);
	
	//constructors
	personType();
	personType(string n, int i, double g);
	
	//data members
	string name;
	int id;			
	double grade;

	//destructor
	~personType();
};

//needs to be defined to avoid errors.
personType::~personType()
{

}

//default constructor
personType::personType()
{
	name="-99";
	id=-99;
	grade=-99;
}

//other constructor
personType::personType(std::string n, int i, double g)
{
	name=n;
	id=i;
	grade=g;
}

//overload operator<<, which is not a member of personType, but is a friend to it.
ostream& operator<<(ostream& os, const personType& person)
{
	os.setf(ios::left, ios::adjustfield);

	string n = person.name; 
	int i = person.id;
	double g=person.grade;

	os<<n + "  " << i<< " " <<g <<endl; 

	return os;
}

//overload ==
bool personType::operator ==(const personType &test)
{
	if(this->name!=test.name)
		return false;
	else if(this->id!=test.id)
		return false;
	else if(this->grade!=test.grade)
		return false;
	else return true;

}
#endif