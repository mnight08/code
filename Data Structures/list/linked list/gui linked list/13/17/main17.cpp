// *****************************************************************
// Program Name:		doubleLinkedListType_head.h
// Author:				alex martinez
// Class:				CSCI/CMPE 2380.01, spring, 2009
// Lab 13:				Double linked list
// Date:				2/19/09
// Comment: The code here is meant to be revised.
//
//-----------------------------------------------------------------
// Lab Exercise 13: 
//
// This lab exercise is to implement a double linked list template
// 
//
// Compile and run your program. When everything is fine,
// print your .h and .cpp file and turn it to me.
// *****************************************************************
#include "headers.h"

using namespace std; 

int main()
{
	/**********************************************************
	Part: Some practice
	**********************************************************/
	//var decarations
	unsigned int seed;					//a random seed
	int num;							//to hold an int value
	doubleLinkedListType<int> list;		//create a list of int type

	cout<<"Enter a random seed => ";	//get the random seed
	cin >> seed; 
	srand(seed);						//set the randon seed

	for (int i=0; i < 10; i++)			//insert 50 random numbers into the list
	{
		num = rand(); 
		if (i%2==0)
			list.insertFirst(num);
		else 
			list.insertLast(num); 
	}
	list.print(); 

	do
	{
		cout<<"Enter a number to search or stop when -99 is entered => "; 
		cin >> num; 
		if (list.search(num))
		{
			cout << "found " <<num << " in the list. And, delete it. " <<endl; 
			list.deleteNode(num); 
			list.print(); 
		}
		else
			cout << "failed to find " <<num << " in the list. " <<endl; 

	}while (num!=-99); 

	/***************************************************************
	Part Two: Try to create a list of strings and practice 
	the basic list operations: insertion, search, deletion, etc. 
	***************************************************************/

	//add your code here



	string swap;
	int character;

	//declare, and print list wiht two items in it
	doubleLinkedListType<string> strings;
	strings.insertFirst("abc");
	strings.insertLast("xyz");
	cout<<"the list is"<<endl;
	strings.print();
	cout<<endl;

	//insert some random 5 charactar strings into list
	for(int i=50; i>0;i--)
	{
		swap="";
		for(int j=5;j>0;j--)
		{
			srand(i+j);
			character=(rand()%27)+65;
			swap=swap+char(character);
		}
		character=rand()%1000;
		if(character%2==0)
			strings.insertFirst(swap);
		else
			strings.insertLast(swap);
	}
	cout<<"now the list is"<<endl;
	strings.print();
	cout<<endl;
	while(swap!="-99")
	{
		cout<<"enter a string to search for, or enter -99 to exit :";
		cin>>swap;

		if(strings.search(swap))
			cout<<"the string "<<swap<<" was found"<<endl;
		else
			cout<<"the string "<<swap<<" was not found"<<endl;

	}

	//play with whats in the list, deleting, and destroying
	strings.deleteNode("abc");
	cout<<"now abc is no longer in the list"<<endl;
	strings.print();
	cout<<endl;
	strings.destroyList();
	cout<<"now the list is destroyed";
	strings.print();
	cout<<endl;
	/***************************************************************
	Part Three: First define a personType class with three data members 
	(string name, int ID, and double grade). Second, create a list 
	of personType. Third, pratice the basic list operations: 
	insertion, search, deletion, etc. 
	***************************************************************/

	//add your code here


	//test to see if operator overload worked
	personType me("alex", 0, 99);
	personType otherme("alex", 0, 99);
	me==otherme?   cout<<"yes":cout<<"no";

	cout<<endl;

	//make up some people to insert
	personType person1("anonymous", 1, 100);
	personType person2("anyone", 2, 80);
	personType person3("enoyna", 3, 96);
	personType person4("soumynona", 4, 98);


	//i only inserted 5 items into the list because it would 
	//have been a bit of trouble to make a loop to do it.
	//i could probably declare the string generator as a function,
	//then just use random numbers for the other two, that would save a bit of trouble.
	doubleLinkedListType<personType> people;
	people.insertFirst(me);
	people.insertFirst(person1);
	people.insertFirst(person2);	
	people.insertFirst(person3);	
	people.insertFirst(person4);

	people.print();
	string quit="100";
	int id;
	double grade;
	while(quit!="-99")
	{
		//create a person to search for
		cout<<"enter the name id and grade or person you are looking for :";
		cin>>swap>>id>>grade;
		personType search(swap, id, grade);

		//search
		//there is no error checking for correct type yet in the == operator
		people.search(search)? cout<<"found":cout<<"not found";
		cout<<endl;
		//flag
		cout<<"enter 0 to continue, or -99 to quit :";
		cin>>quit;
	}

	//delete item
	people.deleteNode(me);

	//get rid of the list
	cout<<"alex is not longer in the list"<<endl;
	people.print();
	cout<<endl;
	people.destroyList();
	people.print();
	cout<<endl;
	cout<<"the list is gone now"<<endl;

	//complete the program
	return 0; 
}
