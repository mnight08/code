#ifndef H_doubleLinkedListType
#define H_doubleLinkedListType

#include "headers.h"
using namespace std; 

template <class Type> 
struct nodeType
{
	Type info;
	nodeType<Type> *previous;				//to point to previous node
	nodeType<Type> *next;					//to point to next node
};

template <class Type> 
class doubleLinkedListType
{
public:
	const doubleLinkedListType<Type> & operator=// overloading assignment operator
					(const doubleLinkedListType<Type>&); 
	void initializeList();					// initialize the list to an empty list
	bool isEmpty();							// chech whether list is empty
	int  length();							// read the number of nodes in list
	void destroyList();						// to delete all nodes from the list
	Type front();							// return the first element in the list
	Type back();							// return the last element in the list
	bool search(const Type & searchItem);	// to determine the searchItem is in the list
											// return true if yes or no otherwise
	void insertFirst(const Type & newItem);	// insert the newItem to the beginning of the list
	void insertLast(const Type & newItem);	// insert the newItem to the end of the list
	void deleteNode(const Type & deleteItem); // delete the node containing the input item from the list
	void print();							// print the list  
	doubleLinkedListType();					// default constructor
	doubleLinkedListType(const doubleLinkedListType<Type>&);// copy constructor 
	~doubleLinkedListType();				// the destructor

protected: 
	int count;								// store the number of nodes in the list
	nodeType<Type> *first;					// the pointer to the first node
	nodeType<Type> *last;					// the pointer to the last node

private: 
	void copyList(const doubleLinkedListType<Type> & otherList); 
											//copy other list to the invoking list
};

// print the list
template <class Type>
void doubleLinkedListType<Type>::print( )
{
	nodeType<Type> *current;				//point to current node
	current = first; 
	int flag = 1; 

	while(current != NULL)
	{
		cout<<setw(10) <<left <<current->info<<"  "; 
		if (flag % 5 == 0)					//print five element per line
			cout << endl; 
		flag++; 
		current = current->next; 
	}
		
	cout<<endl; 
}

// initialize the list to an empty list
template <class Type> 
void doubleLinkedListType<Type> :: initializeList()
{
	destroyList();							//destroy the list to an empty one
}

// chech whether list is empty
template <class Type> 
bool doubleLinkedListType<Type>::isEmpty()
{
	return count == 0; 
}

// read the number of nodes in list
template <class Type>
int  doubleLinkedListType<Type>::length()
{
	return count; 
}
	
// to delete all nodes from the list
template <class Type>
void doubleLinkedListType<Type>::destroyList()
{
	nodeType<Type> *tmp;			//local node ptr to help to delete a node
	
	while (first!=NULL)				//loop until no more nodes are in the list
	{
		tmp = first;				//get the current node
		first = first->next;		//move to the next node
		delete tmp;					//delete the current node
	}

	first = last = NULL; 
	count = 0; 
}

// return the first element in the list
template <class Type> 
Type doubleLinkedListType<Type>::front()
{
	assert(first!=NULL);			//to make sure the list is not empty
	return first->info; 
}

// return the last element in the list
template <class Type> 
Type doubleLinkedListType<Type>::back()
{
	assert(last!=NULL);				//to make sure the list is not empty
	return last->info; 
}

// to determine the searchItem is in the list
// return true if yes or no otherwise
template <class Type> 
bool doubleLinkedListType<Type>::search(const Type & searchItem)
{
	nodeType<Type> *current;		//ptr to current node 
	bool found = false;				//flag for founding status
	
	current = first;				//point to first
	while (!found && current!=NULL)		
	{
		if (current->info == searchItem)
			found = true;			//find the searchItem
		else						//otherwise move to the next node
			current = current->next; 
	}

	return found; 
}

// insert the newItem to the beginning of the list
template <class Type>
void doubleLinkedListType<Type>::insertFirst(const Type & newItem)
{
		nodeType<Type> *newNode;		//for creating a new new
		newNode = new nodeType<Type>;	//create a new node

		assert(newNode != NULL);		//make sure the new node in indeed created
		
		newNode->info = newItem;		//load info to new node	 
		newNode->next = first;			//add new node the front of the list
		newNode->previous = NULL;		//new node has no previous node
		
		if(first != NULL)				//the list is not empty
			first->previous = newNode;	//current first has a previous node now
		first = newNode;				//reset first 
		count++;						//increase the count 
		if (last == NULL)				//reset last if it is empty
			last = newNode; 
}

//insert the newItem to the end of the list
template <class Type> 
void doubleLinkedListType<Type>::insertLast(const Type & newItem)
{
		nodeType<Type> *newNode;		//for creating a new new
		newNode = new nodeType<Type>;	//create a new node

		assert(newNode != NULL);		//make sure the new node in indeed created
		
		newNode->info = newItem;		//load info to new node	 
		newNode->next = NULL;			//make sure the new node is the last one
		newNode->previous = last;		//new node becomes the last node
		count++;						//increase the count 
		if (first == NULL)				//when the list is empty
		{
			first = last = newNode; 
		}
		else 
		{
			last->next = newNode;		//current last become the previous node of the new node
			last = newNode;
		}
}

// delete the node containing the input item from the list	
template <class Type>
void doubleLinkedListType<Type>::deleteNode(const Type & deleteItem)
{
	nodeType<Type>	*current,			//point to current node
					*before;			//point to next node
	bool found;							//flag for finding the node containing deleteItem
	
	//case 1: the list is empty
	if(isEmpty())
		return; 
	//case 2: the first is to be deleted
	else if (first->info == deleteItem)
	{
		current = first->next;			//get the second node if there is one
		delete first;					//delete the first node
		if (current == NULL)			//there is only one node
		{
			first = last = NULL; 
		}	
		else
		{
			first = current;			//the second node becomes the first
			first->previous = NULL;		
		}
		count--; 
		return; 
	}
	//case 3: find the node after the first one
	else 
	{
		before = first; 
		current = first->next;  
		found = false; 
		while (!found && current!=NULL)
		{
			if (current->info == deleteItem)
			{
				found = true; 
			}
			else
			{
				before = current; 
				current = current->next; 
			}
		}
		if (found)							//need to delete the current node
		{
			before->next = current->next;	//skip the current node
			count--; 
			if (last == current)			//delete the last node
			{
				last = before; 		
			}
			else							//current is the middle of two nodes
			{
				current->next->previous = before; 
			}
			delete current;					//now delete the current node
		}	
	}
}

// default constructor	
template <class Type>
doubleLinkedListType<Type>::doubleLinkedListType()
{
	count = 0; 
	first = last = NULL;
}

// copy constructor 
template <class Type>
doubleLinkedListType<Type>::doubleLinkedListType(const doubleLinkedListType<Type>& otherList)
{
	first = NULL; 
	copyList(otherList); 
}

//destructor
template <class Type>
doubleLinkedListType<Type>::~doubleLinkedListType()
{
	destroyList(); 
}

//copy list
template <class Type>
void doubleLinkedListType<Type>::copyList(const doubleLinkedListType<Type> & otherList)
{
	nodeType<Type>	newNode,	//for creating a new node 
					current;	//point to current node
	
	
	if (!isEmpty())				//if list is not empty then destroy it
		destroyLisy(); 
	
	if (otherList.isEmpty())	//otherList is empty
	{
		first = last = NULL; 
		count = 0; 
		return; 
	}
	count = otherList.count;	//copy the count
	//otherList is not empty. first take care of the first node	
	first = new nodeType<Type>;
	first->info = otherList.first->info; 
	first->next = NULL;
	first->previous = NULL; 
	last = first; 
	//now take care of additional nodes if any
	current = otherList.first->next;  
	while (current != NULL)
	{
		newNode = new nodeType<Type>; 
		newNode -> info = current->info; 
		newNode -> next = NULL; 
		newNode->previous = last; 
		last->next = newNode; 
		last = newNode; 

		current = current->link; 
	}
}

// overloading assignment operator
template <class Type>
const doubleLinkedListType<Type>& 
		doubleLinkedListType<Type>::operator=(const doubleLinkedListType<Type>& otherList)
{
	if (this != & otherList)
	{
		copyList(otherList);
	}
	return *this; 
}

#endif
