package graphiclinkedlist;

import java.awt.*;
import javax.swing.*;
import java.util.Random;
public class Node
{

	String data;
	Node next;
        int x;
        int y;
        int width, height;
        public void setpos(int h, int w)
        {
            Random r; r= new Random();
            x=r.nextInt(w-2*width);
            y=r.nextInt(h-2*height);
        }
        public void setdim(int h, int w)
        {
            width=w;
            height=h;
        }
        public void draw(Graphics g, Component c)
        {
            System.out.print("drawn at x="+(x)+"y="+(y)+"\n");
            g.setColor(Color.blue);
            g.fillRect(x, y, width, height);
            g.setColor(Color.yellow);
            g.fillRect(x+width, y, width/3, height);
            g.setColor(Color.WHITE);
            g.drawString(data, x, y+height/2);
        }

}