package graphiclinkedlist;


import javax.swing.*;

public class Driver
{
	public static void main(String [] args)
	{

		JFrame frame = new JFrame("Linked List GUI");

		LLGUIPanel panel = new LLGUIPanel();
		frame.getContentPane().add(panel);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(600,600);
                panel.setdim(600,600); 
		frame.setVisible(true);

	}
}