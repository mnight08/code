#include <iostream>
template< class t>
class node{
public:
	t data;
	node<t> *prev;
	node<t> *next;
	node();
};

template< class t>
node<t>::node()
{
	next=NULL;
	prev=NULL;
}

template< class t>
class list{
public:
	node<t>* head;
	node<t>* tail;
	void insert(t i);
	bool search(t i);
	void remove(t i);
	list();
	~list();
};

template< class t>
list<t>::list()
{
	head=NULL;
	tail=head;
}

template< class t>
list<t>::~list()
{
	while(head!=NULL)
	this->remove(this->head->data);
}

template< class t>
bool list<t>::search(t i)
{
	node<t>* current;
	current=head;
	while(current!=NULL)
	{
		if(current->data=i)
			return true;
		else current =current->next;

	}
	return false;
}

template< class t>
void list<t>::remove(t i)
{
	node<t>* current;
	current=head;
	while(current!=NULL)
	{
		if(current->data=i)
		{
			node<t>* temp=current->prev;
			temp->next=current->next;
			delete current;
		}
		else current =current->next;

	}
}

template< class t>
void list<t>::insert(t i)
{
	if(head==NULL)
	{
		head=new node<t>;
		head->data=i;
		tail=head;
	}
	else{
	tail->next=new node<t>;
	tail->prev=tail;
	tail=tail->next;
	tail->data=i;
	}
}