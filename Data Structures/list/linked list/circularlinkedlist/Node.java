import java.awt.*;
import java.util.Random;

public class Node extends Rectangle
{
	String data;
	Node next;
        Node prev;
	Node()
	{
		Random dice = new Random();
		x= dice.nextInt(500);
		y= dice.nextInt(500);

		width = 60;
		height = 40;
	}

	//move node so that i, j is at center of node
	public void makeCenter(int i, int j)
	{
		x = i-width/2;
		y = j-height/2;
	}

        public int getnewx(int x, int y, double xtranslation, double ytranslation, double rotation)
        {
            double xt=x-xtranslation;
            double yt=y-ytranslation;
            double xtr= xt*Math.cos(Math.toRadians(rotation))+yt*Math.sin(Math.toRadians(rotation));

            return (int)xtr;
        }

        public int getnewy(int x, int y, double xtranslation, double ytranslation, double rotation)
       {
            double xt=x-xtranslation;
            double yt=y-ytranslation;
            double ytr= -xt*Math.sin(Math.toRadians(rotation))+yt*Math.cos(Math.toRadians(rotation));

            return (int)ytr;
        }
        public void draw(Graphics g, double rotation)
	{
                //make a graphics2d object to rotate cooridinate system
                Graphics2D temp=   (Graphics2D) g;
               
                //shift the cooridinate system to starting at the node
                temp.translate(x+width/2, y+height/2);
                //rotate the system about x,y
                temp.rotate(Math.toRadians(rotation));

                //draw rectangle body of node
		g.setColor(Color.BLACK);
		g.fillRect(-width/2,-height/2, width, height);

		//draw data
		g.setColor(Color.WHITE);
		g.drawString(data,-width/2+width/6,0);
                System.out.println("drawn at"+x+","+y);

                //draw the prev field
                g.setColor(Color.YELLOW);
		g.fillRect(-width/2,-height/2, width/6, height);

                //draw next field
		g.setColor(Color.YELLOW);
		g.fillRect((width*5)/6-width/2,-height/2, width/6, height);

                //
                g.setColor(Color.RED);
                if( next != null )
                    //very dense line, applying the tranlation matrix, then the rotation matrix, then working with those new x, and y to do what i want.  kinda  a mess
                    //starting point seems to work fine
                    g.drawLine(width/2-width/6, 0,
                                //the new x position of the the next node is (int)((next.x-x)*Math.cos(Math.toRadians(rotation)))-(int)((next.y-y)*Math.sin(Math.toRadians(rotation)))
                                getnewx(next.x+next.width/2, next.y+next.height/2,x+width/2,y+height/2,rotation),
                                //the new y position of the next node after matrix transformation
                                getnewy(next.x+next.width/2, next.y+next.height/2,x+width/2,y+height/2,rotation));
		
                g.setColor(Color.green);
                //draw prev line
                if( prev != null )
			g.drawLine(-width/2+width/6, 0, getnewx(prev.x+prev.width/2, prev.y+prev.height/2,x+width/2,y+height/2,rotation),getnewy(prev.x+prev.width/2, prev.y+prev.height/2,x+width/2,y+height/2,rotation));
		
                //undo rotation and translation, so that other images can work with the original coordinate system
                temp.rotate(-Math.toRadians(rotation));
                temp.translate(-x-width/2, -y-height/2);                //draw next line
     
	}
}