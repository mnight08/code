import java.awt.*;

public class LinkedList
{

	Node head;

	LinkedList()
	{
        
	}

	//add new item to front of list
	public void insert(String s)
	{
		Node x = new Node();
		if(head==null)
                {
                    x.next=x;x.prev=x;
                }
                else
                {
                    x.next = head;
                    x.prev = head.prev;
                    x.prev.next=x;
                    head.prev = x;
                    if(head.next==head)
                        head.next=x;
                }
                head=x;
		head.data = s;
	}

        public void removehead()
        {
            if(head!=null)
            {
                if(head.next==head)
                    head=null;
                else
                {
                    head.prev.next=head.next;
                    head.next.prev=head.prev;
                    head=head.next;
                }
            }
        }
        public void shiftheadleft()
        {
            if(head!=null)
          head=head.prev;
        }
        public void shiftheadright()
        {
            if(head!=null)
            head=head.next;
        }
	//finds and removes an item whose toString matches s
	public void removeStringItem(String s)
	{
		Node current=head;
                //empty list
                 if(current==null)
                     return;
                 else 
                 {
                    //if it is the first item in the list
                    if(current.data.compareTo(s)==0)
                    {
                        //only one item in the list
                        removehead();
                    }
                    else
                    {
                        current= current.next;
                        while(current!=head)
                        {
                             if(current.data.compareTo(s)==0)
                            {
                                 Node temp=head;
                                 head=current;
                                 //only one item in the list
                                 removehead();
                                 head=temp;
                             }
                        }
                     }
                 }
        }

	public void draw(Graphics g)
	{
		//identify head node with word "head"
		g.setColor(Color.BLUE);
		if( head != null)
                {
			g.drawString("head", head.x,head.y);
		//draw all the nodes in list
		Node current = head.next;
		while( current != head )
		{
			current.draw(g);
			current = current.next;
		}
                }
	}

	//find a node that contains given point, move node to that point
	public void adjustPosition(int newX, int newY)
	{
		Node current = head;

		while( current != null && ! current.contains(newX, newY) )
		{
			current = current.next;
		}

		if(current != null)
		{
			//center the node at position newX, newY
			current.makeCenter(newX,newY);
		}
	}

	public String toString()
	{
		String output = "";

		Node tmp = head;
                if(head!=null)
                {
                    output=head.data+"\n";
                    tmp=tmp.next;
                    while( tmp != head)
                    {
                    	output += tmp.data + "\n";
			tmp = tmp.next;
                    }
                }
		return output;
	}
        
}