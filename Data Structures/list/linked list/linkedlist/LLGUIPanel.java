import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


public class LLGUIPanel extends JPanel implements ActionListener, MouseListener, MouseMotionListener
{
	LinkedList list;

	JTextField field;
	JTextField removefield;

	LLGUIPanel()
	{
		field = new JTextField("insert");
		removefield = new JTextField("remove");

		list = new LinkedList();

		//add the field
		field.addActionListener(this);
		removefield.addActionListener(this);
		add(field);
		add(removefield);

		//add listener
		addMouseListener(this);
		addMouseMotionListener(this);
	}


	public void paintComponent(Graphics g)
	{
		//clear screen
		super.paintComponent(g);

		//paint linked list
		list.draw(g);

		repaint();
	}


	public void actionPerformed(ActionEvent e)
	{
		if( e.getSource() == field )
		{
			String item = field.getText();
			field.setText("");
			list.insert(item);
		}
		else
		{
			String s = removefield.getText();
			removefield.setText("");
			list.removeStringItem(s);
		}
	}


	//stupid mouse stuff
	public void mouseClicked(MouseEvent e)
	{
	//Invoked when the mouse button has been clicked (pressed and released) on a component.

	}
 	public void mouseEntered(MouseEvent e)
 	{//Invoked when the mouse enters a component.
	}

	public void mouseExited(MouseEvent e)
	{ //Invoked when the mouse exits a component.
	}

	public void mousePressed(MouseEvent e)
	{//Invoked when a mouse button has been pressed on a component.
	}

	public void mouseReleased(MouseEvent e)
	{//Invoked when a mouse button has been released on a component.
	}


	public void mouseDragged(MouseEvent e)
	{//Invoked when a mouse button is pressed on a component and then dragged.
		list.adjustPosition(e.getX(), e.getY());
		System.out.println("hello..");
	}

	public void mouseMoved(MouseEvent e)
	{
		//Invoked when the mouse cursor has been moved onto a component but no buttons have been pushed.
	}

}