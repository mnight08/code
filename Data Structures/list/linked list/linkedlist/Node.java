import java.awt.*;
import java.util.Random;

public class Node extends Rectangle
{
	String data;
	Node next;
        Node prev;
	Node()
	{
		Random dice = new Random();
		x= dice.nextInt(500);
		y= dice.nextInt(500);

		width = 60;
		height = 40;
	}

	//move node so that i, j is at center of node
	public void makeCenter(int i, int j)
	{
		x = i-width/2;
		y = j-height/2;
	}

        public void draw(Graphics g)
	{
		//draw rectangle body of node
		g.setColor(Color.BLACK);
		g.fillRect(x,y, width, height);

		//draw data
		g.setColor(Color.WHITE);
		g.drawString(data,x,y+(height/2));

		//draw next field
		g.setColor(Color.YELLOW);
		g.fillRect(x+(width*2)/3,y, width/3, height);
		g.setColor(Color.RED);
		if( next != null )
			g.drawLine(x+width-10, y+height/2, next.x, next.y +height/2);
		else
			g.drawString("null", x+(width*2)/3, y+height/2);

	}
}