#include<iostream>
#include<iomanip>

#include "linked list.h"

using namespace std;

int main()
{

	
doubleLinkedList lista;
	for( int i = 0; i < 5; i++ )
		lista.addFront( i );

	cout << "Items added to the front of the list: \n";	
	cout << setw(42);
		lista.display();
	cout << endl;

	for( int i = 5; i < 10; i++ )
		lista.addBack( i );

	cout << "Items added to the back: \n";
	cout << setw(42);
		lista.display();
	cout << endl;

	lista.remove( 5 );
	cout <<  "The list after the deletion of 5: \n"; 
	cout << setw(42);
		lista.display();
	cout << endl;

	lista.remove( 4 );
	cout <<  "The list after the deletion of 4, \nwhich was at the begining: \n"; 
	cout << setw(42);
		lista.display();	
	cout << endl;	

	lista.remove( 9 );
	cout <<  "The list after the deletion of 9, \nwhich was at the end: \n"; 
	cout << setw(42);
		lista.display();
	cout << endl;
//	lista.testswap();

	lista.sort();
	cout <<  "The list sorted in ascending order: \n"; 
	cout << setw(42);
		lista.display();
	cout << endl;

	
return 0;
}
