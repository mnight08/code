//Assignment #6:  Due 10/22, beginning of class.
//
// 
//
//Implement a doubly linked list by implementing the public methods for the class listed below.  
//Turn in a printout of your code at the beginning of class.
#include<iostream>

using namespace std;


class doubleLinkedList
{//b dbly linked

private:
      class node
      {//b node

      public:
            int data;
            node * next;
            node * prev;

            node(int x)
            {//b constructor
                data = x;
                next = NULL;
                prev = NULL;

            }//e constructor

      };//e node

      node * head, * tail, *current;

public:

      doubleLinkedList();

      //add item to front of list
      void addFront(int x);

      //add item to end of list
      void addBack(int x); 
	 
      //remove x from list, if it is there.
      //return true if it was in the list, false
      //if x was not in the list.
      bool remove(int x);

	 void swap( node*&, node*& );
      //arrange items into increasing order in list
      void sort();

      //display items in list
      void display();

	// void testswap();


};//e doubly linked


doubleLinkedList::doubleLinkedList()
{//b contructor
	head = NULL;
	tail = NULL;
	current = head;


}//e contructor

void doubleLinkedList::addFront(int x)
{//b addfront

//create new node
	node * newNode = new node(x);
	//check to see if head is NULL
	if(head != NULL)
	{//b if
		head->prev = newNode;
	

	//then set newNode's next pointer
	//equal to head
	newNode->next = head;
	//then set head = to the newNode so 
	//the newNode will be the first node
	head = newNode;
	}//e if
	else if( head == NULL )
{
 head = tail = newNode;
}
	
}//e addfront

void doubleLinkedList::addBack(int x)
{//b add back
	//create a new node
		node * newNode = new node(x);
	
	//check to see if head is equal to NULL
	if(tail == NULL)
	{//b if
	//if yes, set new node equal to head
		head = new node(x);
		tail = head;
	}//e if
	//if head is not null
	else
	{//b else
	//set head's next equal to new node
		tail->next = newNode;
	//set new node's prev to head
		newNode->prev = tail;
	//set tail to new node
		tail = newNode;
	}//e else
}//e add back

bool doubleLinkedList::remove(int x)
{//b remove
	//this makes new node 
	node * p = new node(x);
	//find x
current = head;
	while( current != NULL )
		{//b while
		
		//loop to find appropriate x
		//if pointer current points to x
			if( current->data == x )
			{
			//return pointer current
			//because we want to delete it
				p = current;
				break;
			}
		//else make pointer current
		//equal pointer current's next
		//so we can keep looking
			current = current->next;

		}//e while //continue through loop until the
		 //right current is found
	if ( p == NULL )
		return false;

else
{//b else p is not NULL
//if p is not NULL
//we want to check if:
//1. pointer p's prev is NULL, i.e head
//2. pointer p's next is NULL, i.e tail
//3. pointer p is not NULL
	if ( p->next != NULL )
	{//b if

	//if p's prev equals NULL 
	//we are the front of the list
		if ( p == head )
		{//b nested if
		
		//set  head to p's next to avoid
		//to avoid losing the list
			//no simply said pointer head's next to head
			head = head->next;
			//and pointer head's prev to NULL
			//to reestablish the beginning of the list
			head->prev = NULL;
			p = NULL;
		//delete p
			delete p;
		//return not null
			return true;
		}//e nested if
	

	//else return null and break loop
		//set p prev's next to p's next
			p->prev->next = p->next;
	//set p next's prev to p's prev
			p->next->prev = p->prev;
	//the pointers go around p, so p can safely be 
	//deleted
			delete p;
	//return not null
			return true;
	}//e if p next not null
	//else if p's next equals NULL
	//we are at the end of the list
		else if( p == tail )
		{//b nested else if
		//set tail to p's prev
		//to avoid losing tail's pointer
			//no simply set pointer tail's prev to tail
			tail = tail->prev;
			//and pointer tail's next to NULL
			//to reestablish the end of thelist
			tail->next = NULL;
			//p = NULL;
		//delete p
			delete p;
		//return not null
			return true;
		}//e nested else if
		else
		return false;
	}//
	

}//e remove

void doubleLinkedList::display()
{//b display

node *start = head;
//current = head;

while( start != NULL )
{//b while
cout << start->data << " ";
//traverse the list 
start = start->next;
}//e while

cout << endl;

}//e display

//the problem is head remains unchanged
//so the display is printing the same list
//in display current is set to head, which makes the
//list print incorrectly
//that is an incorrect assumption
//current or p is being deallocated and  the list has been adjusted

//tried simply setting head to next and tail to previous but that doesn't fix the problem
//fixed it yesterday...but now can't remeber what it was 
//working with sort
void doubleLinkedList::swap( node*& first, node*& second )
{//b swap
	//orrr we can just switch the data
	int temp = first->data;
	first->data = second->data;
	second->data = temp;

	////we want first to be second and second to be first
	////make a temp pointer
	////set it equal to first

	//node * temp = first;
	////set temp next to second next
	//temp->next = second->next;
	////set temp previous to second prev
	//temp->prev = second->prev;
	////new node, temp2 
	////equal to second
	//node * temp2 = second;
	////set temp2's next to first's prev's next
	//temp2->next = first->next;
	////set temp2's prev to first's prev
	//temp2->prev = first->prev;
	////then set second to temp and first to temp2
	//second = temp;
	//first = temp2;
	////delete the temporary pointers
	//delete temp, temp2;
//maybe something
	/*temp = first->next
first->next = second->next
second->next = temp
and the same for prev*/

}//e swap

void doubleLinkedList::sort()
{//b sort

//set current pointer to head pointer
	//traverse the list swapping the next if the first is larger
	//at the end of the first traversal the largest item will be at the end
	//but the rest of the list may still remain unsorted
	//if we know how long the list is
	//we can subtract the iteration from the list to exclude the last item 
	//to make the sort shorter each time until it's done

	//1st figure out how long the list is
	//a loop to incriment traversal iteration
	//the inner loop will take care of the swap
	current = head;

	int listCount = 0;
	while( current != NULL )
	{//b while list count

		current = current->next;
		listCount++;
	
	}//e while list count

//reset current to head
	current = head;
//loop for bubble sort
	for( int tIteration = 1; tIteration < listCount; tIteration++ )
	{//b traversal iteration tracker
		for( int listIndex = 0; listIndex < listCount - tIteration; listIndex++ )
		{//b for swap
			if ( current == NULL )
				break;
			else if (current->data >= current->next->data && current->next != NULL)
			{//b if less than next
				swap( current, current->next );
				current = current->next;
			}//e if less than next
			else 
				current = current->next;
		}//e for swap 
		current = head;
	}//e traversal iteration tracker


}//e sort
//
//void doubleLinkedList::testswap()
//{
//swap(tail, head);
//}