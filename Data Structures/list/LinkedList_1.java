/*
 alex martinez
 */
import java.awt.*;

public class LinkedList
{

	Node head;
        int width;
        int height;
	LinkedList()
	{

	}
        public void setdim(int h, int w)
        {
            width=w;
            height=h;
        }
	//add new item to front of list
	public void insert(String s)
	{
		Node x = new Node();x.setdim(50, 50);
                x.setpos(height, width);
		x.next = head;
		head = x;
		head.data = s;
	}

        public void delete(String s)
        {
            Node current=head;
            Node prev=null;
            while(current!=null)
            {
                
                if(s.compareTo(current.data)==0)
                {
                    if(prev==null)
                        head=head.next;
                    else
                    prev.next=current.next;
                    return;
                }
                prev=current;
                current=current.next;
            }
        }

        public void draw(Graphics g, Component c)
        {
            Node current =head;
            Node temp;
            while(current!=null)
            {
                temp=current.next;
                current.draw(g, c);
                if(temp!=null)
                {
                    g.setColor(Color.darkGray);
                    g.drawLine(current.x+current.width+(1/3)*current.width, current.y+current.height/2, temp.x, temp.y);

                }
                current=current.next;
            }
        }
	public String toString()
	{
		String output = "";

		Node tmp = head;
		while( tmp != null )
		{
			output += tmp.data + "\n";
			tmp = tmp.next;
		}

		return output;
	}
}
