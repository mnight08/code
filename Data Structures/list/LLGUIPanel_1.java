/*
 alex martinez
 10308748

 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class LLGUIPanel extends JPanel implements ActionListener {

    LinkedList list;
    JTextField field;
    JTextField delete;
    int height;
    int width;

    LLGUIPanel() {
        field = new JTextField("add list item here");
        delete = new JTextField("remove items here");
        list = new LinkedList();

        field.addActionListener(this);
        delete.addActionListener(this);
        add(field);
        add(delete);

    }

    public void setdim(int h, int w) {
        height = h;
        width = w;
        list.setdim(h, w);
    }

    public void paintComponent(Graphics g) {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, width, height);
        list.draw(g, this);
    }

    public void actionPerformed(ActionEvent e) {
        String item = "";
        if (e.getSource() == field) {

            item = field.getText();
            list.insert(item);
        } else if (e.getSource() == delete) {
            item = delete.getText();

            list.delete(item);
        }
        field.setText("");
        delete.setText("");
        repaint();
        System.out.println(list);
    }


}
