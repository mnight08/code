#ifndef H_orderedDoubleLinkedListType
#define H_orderedDoubleLinkedListType

#include <iostream>
#include <iomanip>
#include <cassert>
#include "doubleLinkedListType_head.h"

using namespace std; 

template <class Type> 
class orderedDoubleLinkedListType: public doubleLinkedListType<Type>
{
public:
	void insertNode(const Type & newItem);		// insert into the list to keep it ordered
	//void deleteNode(const Type & deleteItem);	// delete the given node. The search part is done in binary
	//orderedDoubleLinkedListType(int n):doubleLinkedListType<Type>(n){} //constructor
};


// insert the newItem to the list to keep the list ordered
// Do insertion sort here
template <class Type>
void orderedDoubleLinkedListType<Type>::insertNode(const Type & newItem)
{
	nodeType<Type> *newNode;		//for creating a new node
	nodeType<Type> *tmp;			//for finding the right location for insertion

	newNode = new nodeType<Type>;	//create a new node

	assert(newNode != NULL);		//make sure the new node in indeed created

	newNode->info = newItem;		//load info to new node	 

	count++;						//increase the count 

	//case 1: the list is empty
	if (first == NULL)				//when the list is empty
	{
		first = last = newNode; 
		newNode->next = NULL;			//make sure the new node is the last one
		newNode->previous = NULL;		//new node has no previous node

		return; 
	}

	//case 2: the list is not empty
	//the find the right location for insertion
	tmp = last; 
	while (tmp != NULL)
	{
		if (newItem < tmp->info) 
			tmp = tmp -> previous;
		else 
			break; 
	}
	if (tmp == NULL)				// to insert the node at the beginning
	{
		newNode->previous = NULL; 
		newNode->next = first; 
		first->previous=newNode;
		first = newNode; 
	}
	else if (tmp == last)			//to insert the node at the end
	{
		newNode->previous = last; 
		newNode->next = NULL;		//current last become the previous node of the new node
		last->next=newNode; 
		last = newNode;
	}
	else							//to insert the node between two nodes
	{
		newNode->previous = tmp; 
		newNode->next = tmp->next;
		newNode->previous->next = newNode; 
		newNode->next->previous = newNode; 
	}

}




#endif