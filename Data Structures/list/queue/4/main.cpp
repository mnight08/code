// *****************************************************************
// Author:				alex martinez
// Class:				CSCI/CMPE 2380, Spring 2009
// hwk 5:				queue
// Date:				april 17, 2009
// *****************************************************************

#include "headers.h"
using namespace std;

//i can copy lab 22 for linked list queue
//seems to work fine
double power(int x, double y)
{
	if(x==0)
		return -1;		//just return something to exit
	else if(y==0)
		return 1;
	else if(y>0)
		return power(x, y-1) * x;      
	else if(y<0)
		return power(x, y+1) / x;
}

void menu()
{
	cout<<"what would you like to do?: \n1:enque\n2:deque\n3:printqueue\n4:quit\n=>";

}
//10^308 seems to be the highest a double will let you go, 10^-323 is lowest
int main()
{
	/*
	cout<<power(16, 2)<<endl;
	cout<<power(4, 3)<<endl;
	cout<<power(2, 10)<<endl;
	cout<<power(5, 5)<<endl;
	cout<<power(9, 9)<<endl;
	cout<<power(4, -2)<<endl;
	cout<<power(3, -3)<<endl;
	*/
	linkedListQueueType<int> queue;

	string answer;
	int option=0;

	cout<<"do you want to play with a queue?(yes, or no)=>";
	cin>>answer;
	if(answer=="no")
		return 0;
	while (true)
	{

		menu();
		cin>>option;
		switch(option)
		{
		case 1:
			//	case "enque":
			cout<<"what do you want to enque?=>";
			cin>>option;
			queue.enQueue(option);
			break;
		case 2:
			//	case "deque":
			if(queue.isEmpty())
			{
				cout<<"queue is empty\n";
				break;
			}
			else 
			{
				cout<<queue.deQueue();
				cout<<endl;
			}
			break;
		case 3:
			//	case"printqueue":
			cout<<endl;
			queue.print();
			break;
		case 4:
			{
				cout<<"bye";
				return 0;
			}
		default:
			break;

		}


	}


	return 0;
}
