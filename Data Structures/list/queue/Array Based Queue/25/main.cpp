// *****************************************************************
// Author:	alex martinez
// Class:	CSCI/CMPE 2380, Spring 2009
// Lab 21:	Array based queue
// Date:	January 15, 2009
// Comment: The code here is meant to be revised.
//
//-----------------------------------------------------------------
//
// This lab exercise is to implement an array based queue class template
// Here, the queue is the FIFO (First In First Out) queue.
//
// Since we have already implemented the array-based list, we can simply
// derive the array-based queue from the array-based list
//
// Compile and run your program. When everything is fine,
// print your .h and .cpp files and turn them to me or the TA.
// *****************************************************************
#include <iostream>
#include <cassert>
#include <string> 
#include <cstdlib>

#include "headers.h"

using namespace std; 

int main()
{
	/**********************************************************
	Part: Some practice
	**********************************************************/
	//var decarations
	unsigned int seed;					//a random seed
	int num;							//to hold an int value
	arrayQueueType<int> queue(100);		//create a list of int type
	int cat; 

	cout<<"Enter a random seed => ";	//get the random seed
	cin >> seed; 
	srand(seed);						//set the randon seed

	for (int i=0; i < 20; i++)			//insert 50 random numbers into the list
	{
		num = rand(); 
		if (i%5!=3)
			queue.enQueue(num);
		else 
		{
			cat = queue.deQueue();
			cout<<" just popping " <<cat<<" out of the stack "<<endl; 
		}
	}
	queue.print(); 
	cout<<endl; 

	do
	{
		cout<<"Enter a number to search or stop when -99 is entered => "; 
		cin >> num; 
		if (queue.search(num))
			cout << "found " <<num << " in the list. " <<endl; 
		else
			cout << "failed to find " <<num << " in the list. " <<endl; 

	}while (num!=-99); 

	/***************************************************************
	Part Two: Try to create a queue of strings and practice 
	the basic queue operations: enQueue, search, deQueue, etc. 
	***************************************************************/

	//add your code here

	//add your code here
	string swap;
	int character;
	arrayQueueType<string> strings(100);
	strings.insertFirst("abc");
	strings.insertLast("xyz");
	cout<<"the list is"<<endl;
	strings.print();
	cout<<endl;
	//insert some random 5 charactar strings into list
	for(int i=50; i>0;i--)
	{
		swap="";
		for(int j=5;j>0;j--)
		{
			srand(i+j);
			character=(rand()%27)+65;
			swap=swap+char(character);
		}
		character=rand()%1000;
		if(character%2==0)
			strings.insertFirst(swap);
		else
			strings.insertLast(swap);
	}
	cout<<"now the list is"<<endl;
	strings.print();
	cout<<endl;
	while(swap!="-99")
	{
		cout<<"enter a string to search for, or enter -99 to exit :";
		cin>>swap;

		if(strings.search(swap))
			cout<<"the string "<<swap<<" was found"<<endl;
		else
			cout<<"the string "<<swap<<" was not found"<<endl;

	}

	strings.deleteItem("abc");
	cout<<"now abc is no longer in the list"<<endl;
	strings.print();
	cout<<endl;
	strings.destroyList();
	cout<<"now the list is destroyed";
	strings.print();
	cout<<endl;
	/***************************************************************
	Part Three: First define a personType class with three data members 
	(string name, int ID, and double grade). Second, create a queue 
	of personType. Third, pratice the basic queue operations: 
	enQueue, search, deQueue, etc. 
	***************************************************************/

	//add your code here


	//test to see if operator overload worked
	personType me("alex", 0, 99);
	personType otherme("alex", 0, 99);
	me==otherme?   cout<<"yes":cout<<"no";

	cout<<endl;

	//make up some people to insert
	personType person1("anonymous", 1, 100);
	personType person2("anyone", 2, 80);
	personType person3("enoyna", 3, 96);
	personType person4("soumynona", 4, 98);


	//would have to actually do the same with any list, doesn seem to be safe guard against duplication
	//i think i need to edit list template, to make sure that an object being inserted does not have the same id, and an object already there
	arrayQueueType<personType> people(10);
	people.insertFirst(me);
	people.insertFirst(person1);
	people.insertFirst(person2);	
	people.insertFirst(person3);	
	//people.insertFirst(person4);

	people.print();
	string quit="100";
	int id;
	double grade;
	while(quit!="-99")
	{
		//create a person to search for
		cout<<"enter the name id and grade or person you are looking for :";
		cin>>swap>>id>>grade;
		personType search(swap, id, grade);

		//search
		//there is no error checking for correct type yet in the == operator
		people.search(search)? cout<<"found":cout<<"not found";
		cout<<endl;
		//flag
		cout<<"enter 0 to continue, or -99 to quit :";
		cin>>quit;
	}

	people.deleteItem(me);

	cout<<"alex is not longer in the list"<<endl;
	people.print();
	cout<<endl;
	people.destroyList();
	people.print();
	cout<<endl;
	cout<<"the list is gone now"<<endl;

	//complete the program
	return 0; 
}
