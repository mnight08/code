/****************************************************************************************
*  Program Name:		lab2380_21_head.h (for arrayQueueType.h)
*  Author:				alex martinez
*  Course:				CSCI/CMPE 2380, Spring 2009
*  Lab 21:				Header file for Lab 21
*						- derving array-based queue from array-based list
*  Date:				10/02/2008
*
*  Description: This file contains the prototype of the class arrayQueueType
*  Here, the queue is the FIFO (First In First Out) queue. 
*****************************************************************************************/

#ifndef H_ArrayQueueType
#define H_ArrayQueueType

#include "headers.h"

using namespace std; 


//derive arrayStackType from arrayListType
template <class Type> 
class arrayQueueType: public arrayListType<Type>
{
	//overload insertion operator <<
	friend ostream& operator<<<Type>(ostream& os, const arrayQueueType<Type>& x);  
public:
	const arrayQueueType<Type> & operator=		// overloading assignment operator
				(const arrayQueueType<Type>&); 
	Type & operator[] (int index);				// index operator overloading
	//void initializeList();					// initialize the list to an empty list
	//bool isEmpty();							// chech whether list is empty
	//bool isFull();							// chech whether list is full
	//int  length();							// read the number of nodes in list
	//void destroyList();						// to delete all nodes from the list
	//Type front();								// return the first element in the list
	//Type back();								// return the last element in the list
	//bool search(const Type & searchItem);		// to determine the searchItem is in the list
												// return true if yes or no otherwise
	//void insertFirst(const Type & newItem);	// insert the newItem to the beginning of the list
	//void insertLast(const Type & newItem);	// insert the newItem to the end of the list
	//void deleteItem(const Type & deleteItem); // delete the node containing the input item from the list
	//void print();								// print the list  
	
	//two major stack operation
	Type deQueue();								// enter an element into the queue
	void enQueue(Type & item);					// remove an element from the queue

	arrayQueueType():arrayListType<Type>()		// default constructor
	{
		front = 0;								// front is always at position 0	 
		rear = -1;								// rear varies from -1 (empty queue) to count - 1
	}
	arrayQueueType(int n):arrayListType<Type>(n)// another constructor
	{
		front = 0;								// front is always at position 0	 
		rear = -1;								// rear varies from -1 (empty queue) to count - 1
	}
	arrayQueueType(const arrayQueueType<Type>&);// copy constructor 
	
	~arrayQueueType(){};						// the destructor

private: 
	int front; 
	int rear; 
	void arrayQueueType<Type>::copyQueue(const arrayQueueType<Type> & rhs); //copy stack method
};

//deQueue
template<class Type>
Type arrayQueueType<Type>::deQueue()			//remove the front element
{
	assert(!isEmpty());							//make sure the stack is not empty
	
	if (!isEmpty())
	{
		Type tmp = list[rear]; 
		deleteItem(tmp);
		rear--; 
		return tmp; 
	}
	else
	{
		cout<<"No dequeue, the queue is empty." <<endl; 
	}
}

//push
template<class Type>
void arrayQueueType<Type>::	enQueue(Type & item)//push an item upto stack
{
	assert(!isFull());							//make sure no full
	
	if(!isFull())
	{
		insertLast(item);
		rear++; 
	}
	else 
	{
		cout<<"No enQueue, the queue is full."<<endl; 
	}
}


//overload insertion operator <<
template<class Type>
ostream&  operator<<<>(ostream& os, const arrayQueueType<Type>& x)
{
	int flag = 1; 
	for (int i = 0; i < x.length(); i++)
	{
		os << x[i] << "  "; 
		if (flag %5 == 0)
			os << endl;
		flag++; 
	}
	return os; 
}

// index operator overloading
template<class Type>
Type & arrayQueueType<Type>::operator[] (int index)
{
	assert(0<= index && index < count);		//check the range of the index
	return list[index]; 
}

// overloading assignment operator
template<class Type>
const arrayQueueType<Type> & 
arrayQueueType<Type>::operator=	(const arrayQueueType<Type>& rhs)
{
	if (this != & rhs)
	{
		copyQueue(rhs);
	}
	return *this; 
}

//copy stack
template <class Type>
void arrayQueueType<Type>::copyQueue(const arrayQueueType<Type> & rhs)
{
	count = rhs.count; 
	front = rhs.front; 
	rear = rhs.rear; 
	maxSize = rhs.maxSize; 
	delete[] list; 
	if (rhs.isEmpty())
		list = NULL; 
	else
	{
		list = new Type[maxSize]; 
		for (int i = 0; i<count; i++)
			list[i] = rhs.list[i];
	}
}

//copy constructor
template<class Type>
arrayQueueType<Type>::arrayQueueType(const arrayQueueType<Type>& rhs)	// copy constructor 
{
	front = rhs.front; 
	rear = rhs.rear; 
	count = rhs.count; 
	maxSize = rhs.maxSize; 
	delete[] list; 
	if (rhs.isEmpty())
		list = NULL; 
	else
	{
		list = new Type[maxSize]; 
		for (int i = 0; i<count; i++)
			list[i] = rhs[i];
	}
}

#endif
