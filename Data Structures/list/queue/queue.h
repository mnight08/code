#include<string>

using namespace std;

class queue

{

public:

	//set the array variable 'items' to point to an array of size 'cap'
	queue(int cap);
	//add s to the top of the queue
	void push(string s);

	//remove and return the item at the top of the queue
	//The item removed should be the item that was most recently added to the queue
	string pop(); 

	//return true if queue has no items, false if there are some items in the queue
	bool empty();
	//return true if the queue is full
	bool full();

	//check if queue is less than 25% or greater than 75%
	bool smaller();
	bool bigger();
	//shrink and grow queue
	void shrink();
	void grow();

	int getCap(){ return capacity; };
	int getCurrent() { return queueCount; };

private:

	string * items;  //use a (dynamically allocated) array 'items' to hold the elements of the queue
	//also add whatever other stuff you need here....
	int capacity;  //max the stack can hold
	int pushIndex; //index of pushed items
	int popIndex;	//index of item being returned 
	int queueCount;		//number of items in queue

};

queue::queue(int cap)
{//b queue

	capacity = cap;
	items = new string[cap];
	queueCount = 0;
	pushIndex = 0;
	popIndex = 0;

}//e queue

//if the queue is empty it returns true
//if the queue is NOT empty it returns false
bool queue::empty()
{//b empty

	if( queueCount == 0 )
		return true;
	else 
		return false;

}//e empty

//checks to see if the queue is full
//returns true if it is full
//returns false if it is not full
bool queue::full()
{//b full

	if ( queueCount == capacity )
		return true;
	else 
		return false;

}//e full

//adds a new item to the top of the list
void queue::push(string s)
{//b push

	items[pushIndex] = s;
	pushIndex = ( pushIndex + 1 ) % capacity;
	queueCount++;
	//check if newly added item makes the queue 75+% full
	if ( bigger() )
		grow();
	else 
		return;

}//e push

//removes and deletes the last thing entered into the queue
string queue::pop()
{//b pop

	string output = items[popIndex];
	popIndex = ( popIndex + 1 ) % capacity;
	queueCount --;

	//check to see if queue is under 25% to shrink to half size
	if( smaller() )
		shrink();

	return output;
}//e pop

//returns true if capacity is less than or equal 25%
bool queue::smaller()
{//b smaller

	//casts queueCount to double for proper division comparison
	if( double( queueCount )/capacity <= .25 )
		return true;
	else 
		return false;

}//e smaller

//returns true if capacity is greater than or equal 75%
bool queue::bigger()
{//b bigger

	if ( double( queueCount )/capacity >= .75 )
		return true;
	else 
		return false;

}//e bigger

//shrink queue
void queue::shrink()
{//b shrink

	//capacity is the max of the queue which is being divided
	//by 2 to save memory
	int temp2 = capacity;
	capacity = capacity/2;
	//a temporary queue is being created with the new max capacity
	string * temp = new string[capacity];

	//copy old queue into new queue
	for( int newIndex = 0 ,  temp1=popIndex; newIndex < queueCount; 
		newIndex++, temp1 = ( temp1+1 ) % temp2 )
	{//b for
		temp[newIndex] = items[temp1];
	}//e for
	popIndex=0;
	pushIndex=queueCount;
	//deallocate memory from  original list
	delete [] items;
	//set the items to the new list, which is temp and then deallocate temp
	items = temp;
	temp = NULL;
}//e shrink

void queue::grow()
{//b grow

	//the max is capacity and is being multiplied by 2 
	//and will be allocated to the new queue
	int temp2 = capacity;
	capacity = capacity*2;
	//a temporary queue is being created with the new max capacity
	string * temp = new string[capacity];

	//copy old queue into new queue
	for( int newIndex = 0 ,  temp1=popIndex; newIndex < queueCount; 
		newIndex++, temp1 = ( temp1+1 ) % temp2 )
	{//b for
		temp[newIndex] = items[temp1];
	}//e for
	popIndex=0;
	pushIndex=queueCount;
	//deallocate memory from  original list
	delete [] items;
	//store temp into items and then deallocate temp
	items = temp;
	temp = NULL;
}//e grow