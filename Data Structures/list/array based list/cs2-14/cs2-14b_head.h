#ifndef studenttypeclass
#define studenttypeclass
#include "headers.h"
using namespace std;

class personType
{


public:	
	friend ostream& operator<<(ostream&, const personType&);

	bool operator==(const personType &test);


	personType();
	personType(string n, int i, double g);
	string name;
	int id;			//in reality, would you ever allow someone to set their own id number,  in a system that is only using integers?
	double grade;
	~personType();
};

//needs to be defined to avoid errors.
personType::~personType()
{

}

personType::personType()
{
	name="-99";
	id=-99;
	grade=-99;
}

personType::personType(std::string n, int i, double g)
{
	name=n;
	id=i;
	grade=g;
}

//overload operator<<, which is not a member of personType, but is a friend to it.
ostream& operator<<(ostream& os, const personType& person)
{
	os.setf(ios::left, ios::adjustfield);

	string n = person.name; 
	int i = person.id;
	double g=person.grade;

	os<<n + "  " << i<< " " <<g <<endl; 

	return os;
}


bool personType::operator ==(const personType &test)
{
	if(this->name!=test.name)
		return false;
	else if(this->id!=test.id)
		return false;
	else if(this->grade!=test.grade)
		return false;
	else return true;

}
#endif