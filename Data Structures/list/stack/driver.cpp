#include <iostream>
#include "stack.h"


int main()
{

	stack pancakes;

	pancakes.push("blueberry");
	pancakes.push("choco");
	pancakes.push("raisin");
	pancakes.push("bannana");
	cout << "Eating " << pancakes.pop() << " pancake" << endl;
	cout << "Eating " << pancakes.pop() << " pancake" << endl;
	pancakes.push("sherbert");
	pancakes.push("walnut");
	pancakes.flip();
	while( ! pancakes.empty() )
		cout << "Eating " << pancakes.pop() << " pancake" << endl;

	return 0;
}