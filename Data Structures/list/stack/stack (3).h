#include<string>

using namespace std;

class stack{
public:

      //Constructor: set the array variable 'items' to point to an array of size 'cap'
      stack(int cap);
      //add s to the top of the stack
      void push(string s);

      //remove and return the item at the top of the stack
      //The item removed should be the item that was most recently added to the stack
      string pop(); 

      //return true if stack has no items, false if there are some items in the stack
	  bool empty();
	  //return true if the stack is full
	  bool full();
	
	  //check if stack is less than 25% or greater than 75%
	  bool smaller();
	  bool bigger();
	  //shrink and grow stack
	  void shrink();
	  void grow();

	  int getCap(){ return capacity; };
	  int getCurrent() { return currentIndex; };

private:

      string * items;  //use a (dynamically allocated) array 'items' to hold the elements of the stack
      //also add whatever other stuff you need here....
		int capacity;
		int currentIndex;
};

stack::stack(int cap)
{//b stack

	capacity = cap;
	items = new string[cap];
	currentIndex = 0;

}//e stack

//if the stack is empty it returns true
//if the stack is NOT empty it returns false
bool stack::empty()
{//b empty

	if( currentIndex == 0 )
		return true;
	else 
		return false;

}//e empty

//checks to see if the stack is full
//returns true if it is full
//returns false if it is not full
bool stack::full()
{//b full

	if ( currentIndex == capacity )
		return true;
	else 
		return false;

}//e full

//adds a new item to the top of the list
void stack::push(string s)
{//b push

	items[currentIndex] = s;
	currentIndex++;
	//check if newly added item makes the stack 75+% full
	if ( bigger() )
		grow();
	else 
		return;

}//e push

//removes and deletes the last thing entered into the stack
string stack::pop()
{//b pop

	string output = items[currentIndex-1];
	currentIndex--;
	
	//check to see if stack is under 25% to shrink to half size
	if( smaller() )
		shrink();
		
	return output;
}//e pop

//returns true if capacity is less than or equal 25%
bool stack::smaller()
{//b smaller

	//casts currentIndex to double for proper division comparison
	if( double( currentIndex )/capacity <= .25 )
		return true;
	else 
		return false;

}//e smaller

//returns true if capacity is greater than or equal 75%
bool stack::bigger()
{//b bigger

	if ( double( currentIndex )/capacity >= .75 )
		return true;
	else 
		return false;

}//e bigger

//shrink stack
void stack::shrink()
{//b shrink
	
	//capacity is the max of the stack which is being divided
	//by 2 to save memory
	capacity = capacity/2;
	//a temporary stack is being created with the new max capacity
	string * temp = new string[capacity];

	//copy old stack into new stack
	for( int newIndex = 0; newIndex < currentIndex; newIndex++ )
	{//b for
	temp[newIndex] = items[newIndex];
	}//e for
	
	//deallocate memory from  original list
	delete [] items;
	//set the items to the new list, which is temp and then deallocate temp
	items = temp;
	temp = NULL;
}//e shrink

void stack::grow()
{//b grow
	
	//the max is capacity and is being multiplied by 2 
	//and will be allocated to the new stack
	capacity = capacity * 2;
	//the current top is being allocated to the new top of the new stack
	string * temp = new string[capacity];

	//copy old stack into new stack
	for( int newIndex = 0; newIndex < currentIndex; newIndex++ )
	{//b for
	temp[newIndex] = items[newIndex];
	}//e for
	
	//deallocate memory from  original list
	delete [] items;
	//store temp into items and then deallocate temp
	items = temp;
	temp = NULL;
}//e grow
