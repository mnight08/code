#ifndef STACK_H
#define STACK_H

#include <string>
using namespace std;

class node
{
public:
	string data;
	node * next;
};

class stack
{
public:
	stack();

	void push(string x);
	string pop();
	bool empty();
	void flip(node * flipme=NULL, node *dontforgetme=NULL);
private:
	node * topptr;

};
#endif