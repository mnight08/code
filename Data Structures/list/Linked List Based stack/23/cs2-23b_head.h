/****************************************************************************************
*  Program Name:		lab2380_24_b_head.h
*  Lab 20:				Header file for Lab 20
*						- Deriving linked list based stack from linked list
*  Date:				10/02/2008
*
*  Description: This file contains the prototype of the class linkenListStackType, which 
* is derived from linkedListType
*****************************************************************************************/

#ifndef H_StackType
#define H_StackType

#include "headers.h"

using namespace std; 

//derive linkedListStackType from linkedListType
template <class Type> 
class linkedListStackType: public linkedListType<Type>
{
	//overload insertion operator<<
	template<class Type>
	friend ostream & operator<<(ostream& os, const linkedListStackType<Type> & rhs); 

public:
	const linkedListStackType<Type> & operator=	// overloading assignment operator
					(const linkedListStackType<Type>&); 
	//void initializeList();					// initialize the list to an empty list
	//bool isEmpty();							// chech whether list is empty
	//int  length();							// read the number of nodes in list
	//void destroyList();						// to delete all nodes from the list
	//Type front();								// return the first element in the list
	//Type back();								// return the last element in the list
	//bool search(const Type & searchItem);		// to determine the searchItem is in the list
												// return true if yes or no otherwise
	//void insertFirst(const Type & newItem);	// insert the newItem to the beginning of the list
	//void insertLast(const Type & newItem);	// insert the newItem to the end of the list
	//void deleteNode(const Type & deleteItem); // delete the node containing the input item from the list
	//void print();								// print the list  
	
	//major stack operations
	void push(Type & item);						//push item onto the stack
	Type & pop ();								//pop out the top stack item

	// default constructor
	linkedListStackType():linkedListType<Type>(){}; 
	// copy constructor 
	linkedListStackType(const linkedListStackType<Type>&);
	// the destructor
	~linkedListStackType(){};						

private: 
	void copyStack(const linkedListStackType<Type> & rhs); 
											//copy other list to the invoking list
};

//push: push item onto the stack
template<class Type>
void linkedListStackType<Type>::push(Type & item)	
{
	assert(!isFull());						//make sure the stach is not full
	
	if(!isFull())
	{
		insertFirst(item); 
	}
	else
	{
		cout<<"No push, stack is full."<<endl; 
	}
}

//pop: pop the top item out of the the stack
template<class Type>
Type & linkedListStackType<Type>::pop( )	
{
	assert(!isEmpty());						//make sure the stach is not empty
	
	Type tmp; 
	if(!isEmpty())
	{
		tmp = front();						//get the front node value
		deleteNode(tmp);					//delete the from node
		return tmp; 
	}
	else
	{
		cout<<"No pop, stack is empty."<<endl; 
	}
}	

//overload insertion operator<<
template<class Type>
ostream & operator<<(ostream& os, const linkedListStackType<Type> & rhs)
{
	nodeType<Type> *current;				//point to current node
	current = rhs.first; 
	int flag = 1; 

	while(current != NULL)
	{
		os<<setw(10) <<left <<current->info<<"  "; 
		if (flag % 5 == 0)					//print five element per line
			os << endl; 
		flag++; 
		current = current->link; 
	}
	os<<endl;

	return os; 
}

// copy constructor 
template <class Type>
linkedListStackType<Type>::linkedListStackType(const linkedListStackType<Type>& rhs)
{
	first = NULL; 
	copyStack(rhs); 
}

// overloading assignment operator
template <class Type>
const linkedListStackType<Type>& 
		linkedListStackType<Type>::operator=(const linkedListStackType<Type>& rhs)
{
	if (this != & rhs)
	{
		copyList(rhs);
	}
	return *this; 
}

//copy stack
template <class Type>
void linkedListStackType<Type>::copyStack(const linkedListStackType<Type> & rhs)
{
	nodeType<Type>	newNode,	//for creating a new node 
					current;	//point to current node
	
	
	if (!isEmpty())				//if list is not empty then destroy it
		destroyLisy(); 
	
	if (rhs.isEmpty())			//otherList is empty
	{
		first = last = NULL; 
		count = 0; 
		return; 
	}
	count = rhs.count;			//copy the count
	//otherList is not empty. first take care of the first node	
	first = new nodeType<Type>;
	first->info = rhs.first->info; 
	first->link = NULL; 
	last = first; 
	//now take care of additional nodes if any
	current = rhs.first->link;  
	while (current != NULL)
	{
		newNode = new nodeType<Type>; 
		newNode -> info = current->info; 
		newNode -> link = NULL; 
		last->link = newNode; 
		last = newNode; 

		current = current->link; 
	}
}

#endif
