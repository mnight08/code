/****************************************************************************************
*  Program Name:		orderedArrayListType.h
*  Description: This file contains the prototype of the class orderedArrayListType, 
*  which is derived from the arrayListType class template
*****************************************************************************************/

#ifndef H_orderedArrayListType
#define H_orderedArrayListType

#include "headers.h"

using namespace std; 

template <class Type> 
class orderedArrayListType: public arrayListType<Type>
{
public:
	bool binSearch(const Type & searchItem);	// to determine the searchItem is in the list
												// return true if yes or no otherwise. Here do binary search
	void insertNode(const Type & newItem);		// insert into the list to keep it ordered
	void deleteNode(const Type & deleteItem);	// delete the given node. The search part is done in binary
	orderedArrayListType(int n):arrayListType<Type>(n){} //constructor
};

// to determine the searchItem is in the list
// return true if yes or no otherwise. Since the list is order, need to 
// do binary search
template <class Type> 
bool orderedArrayListType<Type>::binSearch(const Type & searchItem)
{
	assert(!isEmpty());			// make sure the list is not empty
	
	bool found = false;			//flag for founding status
	int beg, end, middle;		//three vars for binary search
	beg = 0; 
	end = count - 1; 
	
	while (!found && beg <= end)
	{
		middle = (beg + end)/2; 
		
		if (list[middle] == searchItem)		//case 1: found at middle
		{
			found = true; 
			break; 
		}
		else if (list[middle] > searchItem)	//case 2: search in the left half
		{
			end = middle - 1; 
		}
		else								//case 3; search in the right half
		{
			beg = middle + 1; 
		}
	}

	return found; 
}

// insert the newItem to the list to keep the list ordered
// Do insertion sort here
template <class Type>
void orderedArrayListType<Type>::insertNode(const Type & newItem)
{
	assert(!isFull());					//make sure the list is not full
	int i; 

	//case 1: list is empty
	if (count == 0)					
	{
		list[0] = newItem; 
		count++; 
		return; 
	}

	//case 2: list is not empty
	for (i = count-1; i>=0; i--)	
	{
		if (list[i] <= newItem)			//find the right location for newItem
		{
			list[i+1] = newItem;		//insert the newItem
			break; 
		}
		else							
		{
			list[i+1]=list[i];			//shift list[i] to right
		}

	}
	if ( i == -1)						//newItem is the samllest
		list[0] = newItem;				//add the new item into the first 

	count++;						    //increase the index
}



// delete the node containing the input item from the list	
template <class Type>
void orderedArrayListType<Type>::deleteNode(const Type & deleteItem)
{
	//first use binary search to find the node
	bool found = false;					//flag for founding status
	int beg, end, middle;				//three vars for binary search
	beg = 0; 
	end = count - 1; 
	
	while (!found && beg <= end)
	{
		middle = (beg + end)/2; 
		
		if (list[middle] == deleteItem)		//case 1: found at middle
		{
			found = true; 
			break; 
		}
		else if (list[middle] > deleteItem)	//case 2: search in the left half
		{
			end = middle - 1; 
		}
		else								//case 3; search in the right half
		{
			beg = middle + 1; 
		}
	}

	//to delete the node
	//note that if found, middle is the location of the node to be deleted
	if (!found)						//if not found then stop
		return; 
	else							//found then delete
	{
		if (middle==count)
			count--; 
		else						//shift elements 
		{
			for (int j = middle+1; j<count; j++)
				list[j-1]=list[j]; 
			count--; 
		}
	}
}


#endif
