/*Assignment #4: Due 10/6/2010

0.1) Read the opening description of the "stack" from wikipedia:

http://en.wikipedia.org/wiki/Stack_%28data_structure%29

0.2) Read the opening description of the "queue" from wikipedia:

http://en.wikipedia.org/wiki/Queue_%28data_structure%29

1) Implement the 'queue' class specified below.

2) Implement the 'stack' class specified below.

3) Write a test program to verify that your classes work correctly.

 

For both the queue and the stack class,  the enqueue and push methods respectively should create a larger array to hold the items in the data structure if the current array is out of room.  Further, the queue and stack data structure should never be less than 25% full (unless there are 0 items in the structure).  If the number of items drops below 25%, shrink the array to a smaller size.  When resizing the array (either increasing or decreasing) try to take into consideration efficiency.
*/
 
class stack
{
public:
      //set the array variable 'items' to point to an array of size 'cap'
      stack(int cap);
 
      //add s to the top of the stack
      void push(string s);
 
      //remove and return the item at the top of the stack
      //The item removed should be the item that was most recently added to the stack
      string pop();
 
      //return true if stack has no items, false if there are some items in the stack
      bool empty();
 
private:
      string * items;  //use a (dynamically allocated) array 'items' to hold the elements of the stack
      //also add whatever other stuff you need here....
	 int max;
	 int count;
	 int lastitem;
	 void shrinkstack();
	 void growstack();
};
 
 
 
class queue
{
public:
      //set the array variable 'items' to point to an array of size 'cap'
      queue(int cap);
 
      //add s to the queue
      void enqueue(string s);
 
      //remove and return the item that was the first in the queue
      //The item removed should be the item that has been in the queue the longest
      string dequeue();
 
      //return true if queue has no items, false if there are some items in the queue
      bool empty();
 
private:
      string * items;  //use a (dynamically allocated) array 'items' to hold the elements of the queue
      //also add whatever other attributes or private helper methods
      //you need to make this work (number of items in the queue, position of first item, resize method....)
	 int max;
	 int count;
	 int lastitem;
	 void shrinkqueue();
	 void growqueue();

};
 
 
 