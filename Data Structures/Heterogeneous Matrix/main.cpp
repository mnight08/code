#include <iostream>
using namespace std;

int main(){
	int N=10;

	//1d array
	int *cat;
	cat= new int [N];

	for(int i=0;i<N;i++)
		cat[i]=0;

	for(int i=0;i<N;i++)
		cout<<cat[i]<<" ";
	cout<<endl;

	//2d staggered array
	int **dog;
	dog= new int*[N];
	for( int i=0;i<N;i++)
	{
		dog[i]=new int[i]; 
	}



	for(int i=0;i<N;i++){
		for(int j=0;j<=i;j++){
			dog[i][j]=0;
		}
	}



	for(int i=0;i<N;i++){
		for(int j=0;j<=i;j++){
			cout<<dog[i][j]<<" ";
		}
		cout<<endl;
	}


	cout<<"3-d case"<<endl;

	int ***bat;
	bat= new int**[N];
	for( int i=0;i<N;i++)
	{
		bat[i]=new int*[N]; 
	}
	
	for( int i=0;i<N;i++)
	{
		for( int j=0;j<N;j++)
			bat[i][j]=new int[N]; 
	}

	for( int i=0;i<N;i++)
	{
		for( int j=0;j<N;j++)
			for( int k=0;k<N;k++)
			bat[i][j][k]=0;
	}

	
	for( int i=0;i<N;i++)
	{
		for( int j=0;j<N;j++)
		{
			for( int k=0;k<N;k++)
			{
				cout<<bat[i][j][k]<<" ";	
			}
			cout<<endl;
		}
		cout<<endl<<endl;
	}






	return 0;
}
