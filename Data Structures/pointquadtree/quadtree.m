%Assumes that we are given 2d coordinate points, 
classdef quadtree
	properties
        root=node();
	end
	methods
        function insertrec(x,y,tree)
            %if the tree is empty, then set the given item to be the root
            if tree.value==0
                tree.value=[x,y];
            else 
                if tree.value==0
                    tree.value=InsertMe;
                else 
                    if x > tree.value(1) && y>=tree.value(2) ||(x == tree.value(1) && y==tree.value(2) )
                        tree.NorthEast=node();
                        insertrec(InsertMe,tree.NorthEast);
                    elseif x <= tree.value(1) && y>tree.value(2)
                        tree.NorthWest=node();
                        insertrec(InsertMe,tree.NorthWest);
                    elseif x < tree.value(1) && y<=tree.value(2)
                        tree.SouthWest=node();
                        insertrec(InsertMe,tree.SouthWest);
                    else
                        tree.SouthEast=node();
                        insertrec(InsertMe,tree.SouthEast);
                    end
                end
            
            end
        end

            
        
        function this=insert(x,y)
            insertrec(x,y,this.root);
            %if the tree is empty, then set the given item to be the root
        end
    end
    
end
