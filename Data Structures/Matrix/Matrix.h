#include <iostream>
#include <Complex.h>
//A general matrix class.  Does not make use of structure of matrix.
class Matrix{
	friend ostream &operator<<(ostream& os,Matrix B)
	{
		for(int i=0;i<M;i++)
			{
			    for(int j=0;j<N;j++)
		               os<<" "<<A[i][j]<<" ";
			    os<<endl;
			}
	}
	
	
	//!!!!!!!!!!!!!!PArameters should probably be by reference.
	public:
		//copy constructor
		Matrix(Matrix B)
		{
			M=B.M;
			N=B.N;
			A=new Complex*[M];
			for(int i=0;i<M;i++)
			   A[i]=new Complex[N];

			for(int i=0;i<M;i++)
			   for(int j=0;j<N;j++)
		               A[i][j]=B.A[i][j];
		}
		//create a zero matrix of size m by n
		Matrix(int m, int n)
		{
			M=m;
			N=n;
			A=new Complex*[M];
			for(int i=0;i<M;i++)
			   A[i]=new Complex[N];

			for(int i=0;i<M;i++)
			   for(int j=0;j<N;j++)
		               A[i][j]=0;
		}
		//Take a 2d array and turn it into a matrix.
		Matrix(double B[][], int m, int n)
		{
			N=n; M=m;
			A=new Complex*[M];
			for(int i=0;i<M;i++)
			   A[i]=new Complex[N];

			for(int i=0;i<M;i++)
			   for(int j=0;j<N;j++)
		               A[i][j]=B[i][j];
		}
		//transpose the current matrix.
		void transpose()
		{
			for(int i=0;i<M;i++)
			   for(int j=0;j<N;j++)
		               A[i][j]=A[j][i];

		}
		//Take the conjugate of the current matrix.
		void conjugate();
		//Take the conjugate transpose(Hermitian conjugate)
		//of the current matrix.
		void conjugate_transpose()
		{
			transpose();
			conjugate();
		}
		//display the current matrix.
		void display()
		{
			std:cout<<*this;
		}
		//Multiply the current matrix with the matrix B.
		//In the future I may include Strassen's algorithm,
		//but for now I will use the definition.  
		Matrix operator *(Matrix B)
		{
			Complexs sum=0;
			Matrix C(M,B.N);
			for(int i=0;i<M;i++)
			{

			    for(int j=0;j<B.N;j++)
			    {
				//!!!!!!!!!!!!!!!!!!!!!
				;
			    }

			}
			
		}
		//Add two matrices.  Return the result
		Matrix operator +(Matrix B);
		//Subtract another matrix from this matrix. Return the
		//result.
		Matrix operator -(Matrix B);
	
		bool operator ==(Matrix B);
		bool operator !=(Matrix B);
	        bool operator =(Matrix B)
		{ 	
			//This might fail.
			//dealocate memory
			this.~Matrix();
			//copy the matrix B
			this(B);
		}




		~Matrix();
	protected:
	//c!!!!!!!!!!!!!!this is wrong, needs to be complex
		Complex **A;
		int M;
		int N;
};
