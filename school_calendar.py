# -*- coding: utf-8 -*-
"""
Created on Tue Jan 24 13:36:08 2017

@author: Alex Martinez


Make the scheduled meeting days for my classes.  For Spring 2017
"""

import calendar as cal
import datetime
import pprint
#@return a list of days of class meetings in chronological order. Assumes that the meeting dates are in the same year.
#@param first- First day of class in datetime format.
#@param last - Last day of class in datetime format.
#@param days - The days of the week that the class meets.  Should be a list of charactars" 'SU', 'M', 'T', 'W' , 'TR', 'F', 'SA' 
#@param holidays- A list of days in datetime format that the class will not meet. 
def get_Meeting_Days(start, end, days=['M', 'W'], holidays=[]):
    class_days=[]
    #set sunday to the first day of the week.
    CALENDAR= cal.Calendar(6)
    #dictionary to convert the days of the week characters into numerical days of the week.  
    weekmap={6:'SU',0:'M',1:'T',2:'W',3:'TR',4:'F',5:'SA'}
    year=start.year;
    #Iterate through each month in the range and got through each day of the month.  
    #The daypair will contain day of the month and day of the week.  If the day of week 
    #corresponds to those listed, then we will append those days to the list of class 
    #days.
    for month in range(start.month,end.month+1):
        for daypair in CALENDAR.itermonthdays2(year, month):
            #if the day of the month is zero, then the date is outside the month
            if daypair[0] !=0:
                date=datetime.date(year,month,daypair[0])
                if weekmap[daypair[1]] in days and \
                          date >= first_day and date <= last_day and\
                          date not in holidays:
                              class_days.append(date)
    return class_days;

#Fall 2017 8/28/2017-12/5/2017.  Holidays are labor day: 9/4/2017, and Thanks giving: 11/23/2017-11/25/2017 
holidays=[datetime.date(2018, 1, 15),
          datetime.date(2018, 3, 12),
          datetime.date(2018, 3, 13), – Mar. 17 (Mon. – Sat.)23),
          datetime.date(2018, 3, 14),
          datetime.date(2018, 3, 15)
          datetime.date(2018, 3, 16)
          datetime.date(2018, 3, 17)
          datetime.date(2018, 3, 15)
          ]Mar. 30 – Mar. 31 (Fri. – Sat.) 
#College Algebra Meet days.  #Monday Wednesday classes. 
first_day=datetime.date(2018, 1, 16)
last_day=datetime.date(2018, 5, 2)
days=['T','TR'];
class_days=get_Meeting_Days(first_day, last_day,days, holidays);
i=2
for date in class_days:
    print("Lecture "+ str(i) + ":"+ str(date))
    i=i+1    


