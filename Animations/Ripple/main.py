



from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt
import numpy as np
from numpy import linalg as lin
f=1

A=1.0
c=(0,0)
s=1

def d(p,q):
    return np.sqrt((p[0]-q[0])**2+(p[1]-q[1])**2)
def n(p):
    return np.sqrt(p[0]**2+p[1]**2)
def dot(p,q):
    return p[0]*q[0]+p[1]*q[1]
def ripple(x,y,t=0):
    return A*np.sin(2*f*np.pi*(d((x,y),c)-t*s))
    
    
                                
def traveling_sin(x,y,t=0):
    p=(x,y)
    v=s*u
    
    A*sin(2*f*dot(u,(p-tv)))


t = np.linspace(0, 2 * np.pi, 200)



fig = plt.figure()
ax = fig.gca(projection='3d')
X = np.linspace(-10, 10, 300)
Y = np.linspace(-10, 10, 300)
X, Y = np.meshgrid(X, Y)

for t in np.linspace(0,10,10):
    Z = ripple(X,Y,t)
    surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)


    ax.zaxis.set_major_locator(LinearLocator(10))
    ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
    ax.view_init(elev=90,azim=0)
    fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.show()

